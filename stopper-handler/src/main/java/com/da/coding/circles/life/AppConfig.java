package com.da.coding.circles.life;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableJms
@ComponentScan(basePackages = "com.da.coding.circles.life")
public class AppConfig {

}
