package com.da.coding.circles.life.init;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

@Configuration
public class AMQConnectionConfig {

	@Value("${amq_broker_url:vm://localhost:61616}")
	private String brokerUrl;

	private static final Logger LOGGER = LoggerFactory.getLogger(AMQConnectionConfig.class);

	@Bean
	public ActiveMQConnectionFactory getConnectionFactory() {
		LOGGER.info("AMQ host name: {}", brokerUrl);
		ActiveMQConnectionFactory activeMQConnectionFactory = null;
		activeMQConnectionFactory = new ActiveMQConnectionFactory();
		activeMQConnectionFactory.setBrokerURL(brokerUrl);

		LOGGER.info("AMQ connection factory created: {}", activeMQConnectionFactory);
		return activeMQConnectionFactory;
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		DefaultJmsListenerContainerFactory listenerContainerFactory = new DefaultJmsListenerContainerFactory();
		listenerContainerFactory.setConnectionFactory(getConnectionFactory());

		return listenerContainerFactory;
	}

}
