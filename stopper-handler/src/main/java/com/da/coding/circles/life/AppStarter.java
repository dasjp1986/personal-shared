package com.da.coding.circles.life;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppStarter {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		context.registerShutdownHook();
		//context.close();
	}
}
