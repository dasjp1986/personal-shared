package com.da.coding.circles.life.handler;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class StopHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(StopHandler.class);

	@JmsListener(destination = "circle.life.ToStop")
	public void process(Message message, Session session) throws JMSException {
		String msgStr = ((TextMessage) message).getText();
		LOGGER.info("Received Message : {} ", msgStr);
	}
}
