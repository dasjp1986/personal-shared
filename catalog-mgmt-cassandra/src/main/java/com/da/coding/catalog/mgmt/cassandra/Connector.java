package com.da.coding.catalog.mgmt.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.Session;

/**
 * Hello world!
 *
 */
public class Connector {

	private static final int MAX_CONNECTIONS = 100;
	private static final int CORE_CONNECTIONS = 25;
	private static final String HOST = "localhost";
	private static final int PORT = 9042;
	private static final String USER_NAME = "cassandra";
	private static final String PASSWORD = "cassandra";
	private static final String CLUSTER_NAME = "easybuy";

	public static Session getSession() {
		PoolingOptions poolingOptions = new PoolingOptions();
		poolingOptions.setMaxConnectionsPerHost(HostDistance.LOCAL, MAX_CONNECTIONS);
		poolingOptions.setCoreConnectionsPerHost(HostDistance.LOCAL, CORE_CONNECTIONS);

		Cluster cluster = Cluster.builder().addContactPoint(HOST).withPort(PORT).withCredentials(USER_NAME, PASSWORD)
				.withPoolingOptions(poolingOptions).withClusterName(CLUSTER_NAME).build();
		return cluster.connect();
	}

	public static void main(String[] args) {

	}
}
