package com.da.coding.apache.storm.windowing;

import org.apache.storm.trident.operation.BaseAggregator;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;

public class ErrorAggregator extends BaseAggregator<ErrorAggregator.Status> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static class Status {
		Long count = 0L;

		public Status() {

		}
	}

	@Override
	public Status init(Object batchId, TridentCollector collector) {
		return new ErrorAggregator.Status();
	}

	@Override
	public void aggregate(Status val, TridentTuple tuple, TridentCollector collector) {
		if (tuple.getString(0).equals("ERROR")) {
			val.count++;
		}
	}

	@Override
	public void complete(Status val, TridentCollector collector) {
		collector.emit(new Values(val.count));
	}
}
