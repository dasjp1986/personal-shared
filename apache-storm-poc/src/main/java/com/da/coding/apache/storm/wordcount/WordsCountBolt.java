package com.da.coding.apache.storm.wordcount;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class WordsCountBolt extends BaseBasicBolt {
	private static final long serialVersionUID = 1L;

	private Map<String, Integer> wordCounts = new ConcurrentHashMap<>();

	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		super.prepare(stormConf, context);
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		String word = input.getStringByField("word");
		Integer count = wordCounts.get(word);
		if (count == null) {
			count = 0;
		}
		wordCounts.put(word, ++count);

		collector.emit(new Values(word, count));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word", "count"));
	}

	@Override
	public void cleanup() {
	}

}
