package com.da.coding.apache.storm.twitter;

public class TwitterWordCount {
	private String word;
	private Integer count;

	public TwitterWordCount(String word, Integer count) {
		super();
		this.word = word;
		this.count = count;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
