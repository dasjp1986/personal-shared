package com.da.coding.apache.storm.faulttolerance;

import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyCounterFailureBolt extends BaseRichBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MyCounterFailureBolt.class);
	private OutputCollector collector;

	/**
	 * define transformation logic need to happen on the tuple
	 */

	@Override
	public void execute(Tuple input) {
		int counter = input.getIntegerByField("counter");
		int bucket = input.getIntegerByField("bucket");

		if (bucket == 1) {
			LOGGER.warn("Bolt Error, counter : " + counter);
			collector.fail(input);
		} else {
			collector.emit(new Values(input.getIntegerByField("counter")));
			collector.ack(input);
		}

	}

	/**
	 * declare the schema for output tuple
	 */

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("even_counter"));
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

}
