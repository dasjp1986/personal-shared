package com.da.coding.apache.storm.trident;

import org.apache.storm.trident.operation.MapFunction;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyMapLowerCaseFunction implements MapFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MyMapLowerCaseFunction.class);

	@Override
	public Values execute(TridentTuple input) {
		String result = input.getStringByField("sentence").toLowerCase();
		LOGGER.info("MyMapFunction : {} ", result);
		return new Values(result);
	}

}
