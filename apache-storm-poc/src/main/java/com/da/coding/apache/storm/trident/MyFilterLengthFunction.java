package com.da.coding.apache.storm.trident;

import org.apache.storm.trident.operation.BaseFilter;
import org.apache.storm.trident.tuple.TridentTuple;

public class MyFilterLengthFunction extends BaseFilter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean isKeep(TridentTuple tuple) {
		String word = tuple.getString(0);
		return word == null ? false : word.length() > 2;
	}

}
