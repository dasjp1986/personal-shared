package com.da.coding.apache.storm.drpc;

import java.util.Map;

import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.DRPCExecutionException;
import org.apache.storm.thrift.TException;
import org.apache.storm.thrift.transport.TTransportException;
import org.apache.storm.utils.DRPCClient;
import org.apache.storm.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyDRPCClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(MyDRPCClient.class);

	public static void main(String[] args) {
		Map<String, Object> config = Utils.readStormConfig();
		try {
			DRPCClient client = new DRPCClient(config, "localhost", 3772);
			for (int i = 1; i <= 10; i++) {
				String result = client.execute("TenMultipier", i + "");
			}
			Thread.sleep(20000);
			client.close();
		} catch (TTransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DRPCExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
