package com.da.coding.apache.storm.grouping;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

public class MyFileSpoutDirectGrouping extends BaseRichSpout {

	private SpoutOutputCollector collector;
	private List<Integer> bolts;
	private BufferedReader reader;
	private boolean isCompleted;

	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
		bolts = context.getComponentTasks("MyFileBolt");
		try {
			this.reader = new BufferedReader(new FileReader(conf.get("fileReadPath").toString()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void nextTuple() {
		String str;
		try {
			if (!isCompleted) {
				str = reader.readLine();
				if (str == null) {
					isCompleted = true;
					reader.close();
				} else {
					Integer bucket = Integer.parseInt(str.split(" ")[2]) % 2;
					collector.emitDirect(bolts.get(bucket), new Values(str.split(" ")));
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("FirstName", "LastName", "Type", "City"));

	}

}
