package com.da.coding.apache.storm.drpc;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.LocalDRPC;
import org.apache.storm.drpc.LinearDRPCTopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DRPCTopology {
	private static final Logger LOGGER = LoggerFactory.getLogger(DRPCTopology.class);

	public static void main(String[] args) {
		LinearDRPCTopologyBuilder builder = new LinearDRPCTopologyBuilder("TenMultipier");
		builder.addBolt(new DRPCBolt());

		// running rpc in local mode
		Config config = new Config();

		LocalDRPC localDRPC = new LocalDRPC();
		LocalCluster localCluster = new LocalCluster();
		localCluster.submitTopology("DRPCTopology", config, builder.createLocalTopology(localDRPC));
		localCluster.submitTopology("DRPCTopology", config, builder.createLocalTopology(localDRPC));

		for (int i = 1; i <= 10; i++) {
			LOGGER.info("result : ", localDRPC.execute("TenMultipier", i + ""));
		}

		try {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} finally {
			localCluster.shutdown();
		}

		// running in remote cluster
//		Config config = new Config();
//		List<String> drpcServers = new ArrayList<>();
//		drpcServers.add("localhost");
//		config.put(Config.DRPC_SERVERS, drpcServers);
//		config.put(Config.DRPC_PORT, 3772);
//
//		try {
//			StormSubmitter.submitTopology("TenMultipier", config, builder.createRemoteTopology());
//		} catch (AlreadyAliveException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InvalidTopologyException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (AuthorizationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}
}
