package com.da.coding.apache.storm.wordcount;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

public class WordCountSorterBolt extends BaseBasicBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, WordCount> wordCounts = new HashMap<>();
	private Calendar calendar = Calendar.getInstance();
	private PrintWriter writer;
	private int top = 0;

	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		super.prepare(stormConf, context);
		top = Integer.parseInt(stormConf.get("top").toString());
		try {
			writer = new PrintWriter(new File(stormConf.get("fileWritePath").toString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		WordCount wordCount = new WordCount(input.getStringByField("word"), input.getIntegerByField("count"));
		wordCounts.put(wordCount.getWord(), wordCount);
		List<WordCount> list = new ArrayList<>(wordCounts.values());
		Collections.sort(list, (WordCount o1, WordCount o2) -> o2.getCount().compareTo(o1.getCount()));
		int rows = Math.min(top, list.size());
		writer.write("\n word count at " + calendar.getTime().toLocaleString());
		for (int i = 0; i < rows; i++) {
			writer.write("\n Hot Word : " + list.get(i).getWord() + " | Frequency : " + list.get(i).getCount());
		}
		writer.write("\n --------------------------------------------------------------");
		writer.flush();
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}

	@Override
	public void cleanup() {
		super.cleanup();
		writer.close();
	}

}
