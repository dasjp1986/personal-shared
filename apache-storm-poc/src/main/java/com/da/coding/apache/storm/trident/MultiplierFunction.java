package com.da.coding.apache.storm.trident;

import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiplierFunction extends BaseFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MultiplierFunction.class);

	@Override
	public void execute(TridentTuple tuple, TridentCollector collector) {
		int result= tuple.getIntegerByField("counter") * 10;
		LOGGER.info("Function sending : {} ",result);
		collector.emit(new Values(result));
	}

}
