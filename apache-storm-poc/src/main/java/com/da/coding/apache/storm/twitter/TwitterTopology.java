package com.da.coding.apache.storm.twitter;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class TwitterTopology {
	public static void main(String[] args) {
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("TwitterSpout", new TwitterSpout());
		builder.setBolt("TwitterSplitWordsBolt", new TwitterSplitWordsBolt()).shuffleGrouping("TwitterSpout");
		builder.setBolt("TwitterWordsCountBolt", new TwitterWordsCountBolt(), 10)
				.fieldsGrouping("TwitterSplitWordsBolt", new Fields("word"));
		builder.setBolt("TwitterWordCountSorterBolt", new TwitterWordCountSorterBolt())
				.shuffleGrouping("TwitterWordsCountBolt");

		Config config = new Config();
		config.setDebug(true);
		config.put("fileWritePath",
				"/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/top-tweets.txt");
		config.put("top", 20);

		LocalCluster cluster = new LocalCluster();
		try {
			cluster.submitTopology("TwitterTopology", config, builder.createTopology());
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			cluster.shutdown();
		}
	}
}
