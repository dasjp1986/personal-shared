package com.da.coding.apache.storm.counter;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class MyFirstTopology {
	public static void main(String[] args) {

		// create topology
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("MyFirstSpout", new MyFirstSpout());
		builder.setBolt("MyFirstBolt", new MyFirstBolt()).shuffleGrouping("MyFirstSpout");

		Config config = new Config();
		config.setDebug(true);

		// create local cluster

		LocalCluster cluster = new LocalCluster();
		try {
			cluster.submitTopology("MyFirstTopology", config, builder.createTopology());
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			cluster.shutdown();
		}

	}

}
