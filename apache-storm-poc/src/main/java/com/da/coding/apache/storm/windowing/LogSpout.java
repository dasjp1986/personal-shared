package com.da.coding.apache.storm.windowing;

import java.util.Map;
import java.util.Random;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

public class LogSpout extends BaseRichSpout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7671952074702343358L;
	private SpoutOutputCollector collector;
	private Random random = new Random();

	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void nextTuple() {
		if (random.nextInt(100) < 80) {
			collector.emit(new Values("SUCCESS"));
		} else {
			collector.emit(new Values("ERROR"));
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("log"));
	}

}
