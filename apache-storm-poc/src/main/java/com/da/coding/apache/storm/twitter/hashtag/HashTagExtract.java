package com.da.coding.apache.storm.twitter.hashtag;

import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;

import twitter4j.HashtagEntity;
import twitter4j.Status;

public class HashTagExtract extends BaseFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5118819999355214291L;

	@Override
	public void execute(TridentTuple tuple, TridentCollector collector) {
		Status status = (Status) tuple.getValueByField("tweet");
		for (HashtagEntity entity : status.getHashtagEntities()) {
			collector.emit(new Values(entity.getText()));
		}
	}

}
