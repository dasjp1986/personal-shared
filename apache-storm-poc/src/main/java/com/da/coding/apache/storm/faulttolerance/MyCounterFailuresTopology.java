package com.da.coding.apache.storm.faulttolerance;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class MyCounterFailuresTopology {
	public static void main(String[] args) {

		// create topology
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("MyCounterSpout", new MyCounterSpout());
		builder.setBolt("MyCounterFailureBolt", new MyCounterFailureBolt(),2).fieldsGrouping("MyCounterSpout",
				new Fields("bucket"));
		Config config = new Config();
		config.setDebug(true);

		// create local cluster

		LocalCluster cluster = new LocalCluster();
		try {
			cluster.submitTopology("MyCounterFailuresTopology", config, builder.createTopology());
			Thread.sleep(50000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			cluster.shutdown();
		}

	}

}
