package com.da.coding.apache.storm.state;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.LocalDRPC;
import org.apache.storm.trident.TridentState;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.operation.builtin.Count;
import org.apache.storm.trident.operation.builtin.MapGet;
import org.apache.storm.trident.testing.FixedBatchSpout;
import org.apache.storm.trident.testing.MemoryMapState;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import com.da.coding.apache.storm.trident.MyFlatMap;
import com.da.coding.apache.storm.trident.MyMapLowerCaseFunction;

public class StateTopology {
	public static void main(String[] args) {
		FixedBatchSpout spout = new FixedBatchSpout(new Fields("sentence"), 3, new Values("Happy Diwali"),
				new Values("Wish you very Happy Diwali"), new Values("Diwali is the biggest Festival in india"));

		spout.setCycle(true);
		LocalDRPC localDRPC = new LocalDRPC();
		TridentTopology tridentTopology = new TridentTopology();

		TridentState tridentState = tridentTopology.newStream("spout", spout).map(new MyMapLowerCaseFunction())
				.flatMap(new MyFlatMap()).groupBy(new Fields("sentence"))
				.persistentAggregate(new MemoryMapState.Factory(), new Count(), new Fields("count"));
		tridentTopology.newDRPCStream("words", localDRPC).stateQuery(tridentState, new Fields("args"), new MapGet(),
				new Fields("count"));

		Config config = new Config();
		config.setDebug(true);

		LocalCluster localCluster = new LocalCluster();
		localCluster.submitTopology("words", config, tridentTopology.build());

		String[] strArray = new String[] { "Diwali", "Happy", "Festival" };
		for (String query : strArray) {
			System.out.println("count for word : " + query + " = " + localDRPC.execute("words", query));
		}

		try {
			Thread.sleep(3000);
			localCluster.shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
