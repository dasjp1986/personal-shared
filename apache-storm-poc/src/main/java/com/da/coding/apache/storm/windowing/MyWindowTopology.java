package com.da.coding.apache.storm.windowing;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.operation.builtin.Debug;
import org.apache.storm.trident.windowing.InMemoryWindowsStoreFactory;
import org.apache.storm.trident.windowing.WindowsStoreFactory;
import org.apache.storm.trident.windowing.config.BaseWindowConfig;
import org.apache.storm.trident.windowing.config.SlidingCountWindow;
import org.apache.storm.trident.windowing.config.TumblingCountWindow;
import org.apache.storm.trident.windowing.config.WindowConfig;
import org.apache.storm.tuple.Fields;

public class MyWindowTopology {
	public static void main(String[] args) {
		WindowsStoreFactory windowStoreFactory = new InMemoryWindowsStoreFactory();

		TridentTopology topology = new TridentTopology();

		//sliding window
//		topology.newStream("log", new LogSpout()).slidingWindow(100, 10, windowStoreFactory, new Fields("log"),
//				new ErrorAggregator(), new Fields("count")).each(new Fields("count"), new Debug());

		//sliding window using config
//		WindowConfig windowConfig = SlidingCountWindow.of(100, 10);
//		topology.newStream("log", new LogSpout())
//				.window(windowConfig, new Fields("log"), new ErrorAggregator(), new Fields("count"))
//				.each(new Fields("count"), new Debug());
		
		//tumbling window using config
				WindowConfig windowConfig = TumblingCountWindow.of(100);
				topology.newStream("log", new LogSpout())
						.window(windowConfig, new Fields("log"), new ErrorAggregator(), new Fields("count"))
						.each(new Fields("count"), new Debug());

		Config config = new Config();
		config.setDebug(true);

		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology("window", config, topology.build());
	}
}
