package com.da.coding.apache.storm.remote;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;

public class MyFirstTopologyRemote {
	public static void main(String[] args) {

		// create topology
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("MyFirstSpout", new MyFirstSpoutRemote());
		builder.setBolt("MyFirstBolt", new MyFirstBoltRemote()).shuffleGrouping("MyFirstSpout");

		Config config = new Config();
		config.setDebug(true);

		// create remote cluster
		try {
			StormSubmitter.submitTopology("MyRemoteTopology", config, builder.createTopology());
		} catch (AlreadyAliveException e) {
			e.printStackTrace();
		} catch (InvalidTopologyException e) {
			e.printStackTrace();
		} catch (AuthorizationException e) {
			e.printStackTrace();
		}

	}

}
