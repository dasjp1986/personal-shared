package com.da.coding.apache.storm.counter;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class MyFirstBolt extends BaseBasicBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * define transformation logic need to happen on the tuple
	 */
	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		collector.emit(new Values(input.getIntegerByField("counter") * 2));
	}

	/**
	 * declare the schema for output tuple
	 */

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("even_counter"));
	}

}
