package com.da.coding.apache.storm.twitter.hashtag;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.operation.builtin.Count;
import org.apache.storm.trident.operation.builtin.Debug;
import org.apache.storm.trident.operation.builtin.FirstN;
import org.apache.storm.trident.testing.MemoryMapState;
import org.apache.storm.tuple.Fields;

import com.da.coding.apache.storm.twitter.TwitterSpout;

public class HashTagTopology {
	public static void main(String[] args) {
		TridentTopology topology = new TridentTopology();
		topology.newStream("tweet", new TwitterSpout())
				.each(new Fields("tweet"), new HashTagExtract(), new Fields("hashtag")).groupBy(new Fields("hashtag"))
				.persistentAggregate(new MemoryMapState.Factory(), new Count(), new Fields("count")).newValuesStream()
				.applyAssembly(new FirstN(10, "count")).each(new Fields("hashtag", "count"), new Debug());

		Config config = new Config();
		config.setDebug(true);

		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology("streams", config, topology.build());

	}
}
