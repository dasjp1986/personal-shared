package com.da.coding.apache.storm.files;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

public class MyFileBolt extends BaseBasicBolt {

	/**
	 * 
	 */

	private PrintWriter writer;

	private static final long serialVersionUID = 1L;

	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		super.prepare(stormConf, context);
		try {
			String fileName = "output-" + context.getThisTaskId() + "-" + context.getThisComponentId();
			writer = new PrintWriter(new File(stormConf.get("WriteDirPath").toString() + fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		writer.write("\nFirstName : " + input.getStringByField("FirstName"));
		writer.write("\nLastName : " + input.getStringByField("LastName"));
		writer.write("\nAge : " + input.getStringByField("Age"));
		writer.write("\nCity : " + input.getStringByField("City"));
		writer.write("\n----------------------------------------------------------");
		writer.flush();
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("line"));
	}

	@Override
	public void cleanup() {
		super.cleanup();
		writer.close();
	}

}
