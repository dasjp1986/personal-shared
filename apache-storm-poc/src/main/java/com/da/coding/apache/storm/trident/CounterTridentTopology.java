package com.da.coding.apache.storm.trident;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.operation.builtin.Count;
import org.apache.storm.tuple.Fields;

import com.da.coding.apache.storm.counter.MyFirstSpout;
import com.da.coding.apache.storm.wordcount.SentenceSpout;

public class CounterTridentTopology {
	public static void main(String[] args) {
		TridentTopology topology = new TridentTopology();
//		topology.newStream("each", new MyFirstSpout()).each(new Fields("counter"), new MultiplierFunction(),
//				new Fields("tens"));
		// topology.newStream("sentence", new SentenceSpout()).map(new MyMapFunction(),
		// new Fields("lowercase"));

		topology.newStream("sentence", new SentenceSpout()).map(new MyMapLowerCaseFunction())
				.flatMap(new MyFlatMap(), new Fields("word")).filter(new MyFilterLengthFunction())
				.aggregate(new Count(), new Fields("wordCount"));

		Config config = new Config();
		config.setDebug(true);
		config.put("fileReadPath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/tweets.txt");

		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology("CounterTridentTopology", config, topology.build());
	}
}
