package com.da.coding.apache.storm.grouping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.CustomStreamGrouping;
import org.apache.storm.task.WorkerTopologyContext;

public class CustomGrouping implements CustomStreamGrouping, Serializable {

	private List<Integer> targetTasks;

	@Override
	public void prepare(WorkerTopologyContext context, GlobalStreamId stream, List<Integer> targetTasks) {
		this.targetTasks = targetTasks;
	}

	@Override
	public List<Integer> chooseTasks(int taskId, List<Object> values) {
		List<Integer> bolts = new ArrayList<>();
		Integer task = Integer.parseInt(values.get(2).toString()) % 2;
		bolts.add(targetTasks.get(task));
		return bolts;
	}

}
