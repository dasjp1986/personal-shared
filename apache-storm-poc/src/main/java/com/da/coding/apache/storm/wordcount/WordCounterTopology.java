package com.da.coding.apache.storm.wordcount;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class WordCounterTopology {

	public static void main(String[] args) {
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("SentenceSpout", new SentenceSpout());
		builder.setBolt("SplitWordsBolt", new SplitWordsBolt(), 5).shuffleGrouping("SentenceSpout");
		builder.setBolt("WordsCountBolt", new WordsCountBolt(), 10).fieldsGrouping("SplitWordsBolt",
				new Fields("word"));
		builder.setBolt("WordCountSorterBolt", new WordCountSorterBolt()).shuffleGrouping("WordsCountBolt");

		Config config = new Config();
		config.setDebug(true);
		config.put("fileReadPath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/tweets.txt");
		config.put("fileWritePath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/hot-tweets.txt");
		config.put("top", 10);
		
		LocalCluster cluster = new LocalCluster();
		try {
			cluster.submitTopology("FileTopology", config, builder.createTopology());
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			cluster.shutdown();
		}
	}

}
