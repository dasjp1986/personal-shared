package com.da.coding.apache.storm.grouping;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class MyFileTopologyGrouping {

	public static void main(String[] args) {
		TopologyBuilder builder = new TopologyBuilder();
		//builder.setSpout("MyFileSpout", new MyFileSpoutGrouping());
		builder.setSpout("MyFileSpout", new MyFileSpoutDirectGrouping());
		// builder.setBolt("MyFileBolt", new
		// MyFileBoltShuffle(),2).shuffleGrouping("MyFileSpout");
		// builder.setBolt("MyFileBolt", new
		// MyFileBoltShuffle(),2).fieldsGrouping("MyFileSpout", new Fields("Type"));
		// builder.setBolt("MyFileBolt", new
		// MyFileBoltGrouping(),2).allGrouping("MyFileSpout");
		//builder.setBolt("MyFileBolt", new MyFileBoltGrouping(), 2).customGrouping("MyFileSpout", new CustomGrouping());

		builder.setBolt("MyFileBolt", new MyFileBoltGrouping(), 2).directGrouping("MyFileSpout");

		Config config = new Config();
		config.setDebug(true);
		config.put("fileReadPath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/bio");
		config.put("WriteDirPath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/");
		config.setMaxTaskParallelism(4);

		LocalCluster cluster = new LocalCluster();
		try {
			cluster.submitTopology("FileTopology", config, builder.createTopology());
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			cluster.shutdown();
		}
	}

}
