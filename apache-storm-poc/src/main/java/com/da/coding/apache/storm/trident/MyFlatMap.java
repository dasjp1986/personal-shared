package com.da.coding.apache.storm.trident;

import java.util.ArrayList;
import java.util.List;

import org.apache.storm.trident.operation.FlatMapFunction;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyFlatMap implements FlatMapFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MyFlatMap.class);

	@Override
	public Iterable<Values> execute(TridentTuple input) {
		String[] tokens = input.getStringByField("sentence").split(" ");
		List<Values> list = new ArrayList<>();
		for (String token : tokens) {
			list.add(new Values(token));
		}
		LOGGER.info("MyFlatMap : {} ", list);
		return list;
	}

}
