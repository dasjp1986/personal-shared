package com.da.coding.apache.storm.files;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class MyFileTopology {

	public static void main(String[] args) {
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("MyFileSpout", new MyFileSpout());
		builder.setBolt("MyFileBolt", new MyFileBolt()).shuffleGrouping("MyFileSpout");

		Config config = new Config();
		config.setDebug(true);
		config.put("fileReadPath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/bio");
		config.put("WriteDirPath", "/Users/dagraw1/Documents/Personal-Useful/my learnings/Apache Storm/");
		config.setMaxTaskParallelism(4);
		
		LocalCluster cluster = new LocalCluster();
		try {
			cluster.submitTopology("FileTopology", config, builder.createTopology());
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			cluster.shutdown();
		}
	}

}
