package com.da.coding.apache.storm.twitter;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterSpout extends BaseRichSpout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TwitterStream twitterStream;
	private LinkedBlockingQueue<Status> queue;
	private SpoutOutputCollector collector;

	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;

		ConfigurationBuilder cfgb = new ConfigurationBuilder();
		cfgb.setDebugEnabled(true);
		cfgb.setOAuthConsumerKey("a6oBl5aQU8ZsTJg9n8ZgtbhRc");
		cfgb.setOAuthConsumerSecret("kS8szcys5Bmp9762yQ9tfwgjEbRJ928gqia16syxAfEevI8sJV");
		cfgb.setOAuthAccessToken("814305216518438912-qDW4Up6wNouG4gdSO1RXWL1gk6JPDoL");
		cfgb.setOAuthAccessTokenSecret("0bDI70AOdbk9Czn6tJJ4gcnb6JsYEsNSABXiDFrS0DT3g");

		twitterStream = new TwitterStreamFactory(cfgb.build()).getInstance();
		queue = new LinkedBlockingQueue<>();

		final StatusListener statusListener = new StatusListener() {

			@Override
			public void onException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStatus(Status status) {
				queue.offer(status);
			}

			@Override
			public void onStallWarning(StallWarning warning) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrubGeo(long userId, long upToStatusId) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
				// TODO Auto-generated method stub

			}
		};

		twitterStream.addListener(statusListener);
		FilterQuery query = new FilterQuery();
		query.track(new String[] { "india" });
		twitterStream.filter(query);
	}

	@Override
	public void nextTuple() {
		Status status = queue.poll();
		if (status == null) {
			Utils.sleep(50);
		} else {
			collector.emit(new Values(status));
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweet"));
	}
	
	@Override
	public void close() {
		super.close();
		twitterStream.shutdown();
	}

}
