package com.da.coding.apache.storm.twitter;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

public class TwitterWordCountSorterBolt extends BaseBasicBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, TwitterWordCount> wordCounts = new HashMap<>();
	private Calendar calendar = Calendar.getInstance();
	private int top = 0;
	private Map stormConf;

	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		super.prepare(stormConf, context);
		top = Integer.parseInt(stormConf.get("top").toString());
		this.stormConf = stormConf;
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		TwitterWordCount wordCount = new TwitterWordCount(input.getStringByField("word"), input.getIntegerByField("count"));
		wordCounts.put(wordCount.getWord(), wordCount);
		List<TwitterWordCount> list = new ArrayList<>(wordCounts.values());
		Collections.sort(list, (TwitterWordCount o1, TwitterWordCount o2) -> o2.getCount().compareTo(o1.getCount()));
		int rows = Math.min(top, list.size());
		try {
			PrintWriter writer = new PrintWriter(new File(stormConf.get("fileWritePath").toString()));
			writer.write("\n word count at " + calendar.getTime().toLocaleString());
			for (int i = 0; i < rows; i++) {
				writer.write("\n Hot Word : " + list.get(i).getWord() + " | Frequency : " + list.get(i).getCount());
			}
			writer.write("\n --------------------------------------------------------------");
			writer.flush();
			writer.close();
		} catch (IOException e) {

		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}

	@Override
	public void cleanup() {
		super.cleanup();
	}

}
