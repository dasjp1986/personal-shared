package com.da.coding.apache.storm.joinstreams;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.trident.Stream;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.Consumer;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

public class StreamsTopology {
	public static void main(String[] args) {
		TridentTopology topology = new TridentTopology();
		Stream source = topology.newStream("pairs", new PairsSpout());
		Stream sumStream = source.each(new Fields("x1", "x2"), new SumFunction(), new Fields("sum"));
		Stream productStream = source.each(new Fields("x1", "x2"), new ProductFunction(), new Fields("sum"));

//		topology.merge(new Fields("calculation"), sumStream, productStream).peek(new Consumer() {
//
//			@Override
//			public void accept(TridentTuple input) {
//				System.out.println("Merge Result : "+input.getInteger(0));
//			}
//		});
		
		topology.join(sumStream, new Fields("x1", "x2"), productStream, new Fields("x1", "x2"), new Fields("sum", "product")).peek(new Consumer() {
			
			@Override
			public void accept(TridentTuple input) {
				System.out.println("Merge Result Sum : "+input.getInteger(0));
				System.out.println("Merge Result Prod : "+input.getInteger(1));
			}
		});

		Config config = new Config();
		config.setDebug(true);

		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology("streams", config, topology.build());
	}
}

class SumFunction extends BaseFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(TridentTuple tuple, TridentCollector collector) {
		collector.emit(new Values(tuple.getInteger(0) + tuple.getInteger(1)));
	}

}

class ProductFunction extends BaseFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(TridentTuple tuple, TridentCollector collector) {
		collector.emit(new Values(tuple.getInteger(0) * tuple.getInteger(1)));
	}

}
