package com.da.coding.intuit.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerMetricsService {
	@RequestMapping("/producer")
	public ResponseEntity<String> produce() {
		System.out.println("produced");
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
