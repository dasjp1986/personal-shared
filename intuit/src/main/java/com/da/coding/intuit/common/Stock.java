package com.da.coding.intuit.common;

public class Stock {
	private long timestamp;
	private String symbol;
	private String price;

	public Stock(long timestamp, String symbol, String price) {
		super();
		this.timestamp = timestamp;
		this.symbol = symbol;
		this.price = price;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}
