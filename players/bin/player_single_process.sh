#!/bin/sh

export JAVA_HOME=`/usr/libexec/java_home -v ‘1.8*’`

echo "Welcome, Starting Players in Single-Process Mode..."

java -jar -Dprocesses=1 -Dinitiator=initiator -Dreceiver=receiver -DisInitiator=true target/players-0.0.1-SNAPSHOT-shaded.jar 

