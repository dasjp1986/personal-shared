#!/bin/sh

export JAVA_HOME=`/usr/libexec/java_home -v ‘1.8*’`

echo "Welcome, Starting Players in Multi-Process Mode..."

java -jar -Dprocesses=2 -Dinitiator=receiver -Dreceiver=initiator -DisInitiator=false target/players-0.0.1-SNAPSHOT-shaded.jar &

echo "Starting Players-Receiver Process..."
sleep 10s

echo "Starting Players-Initiator Process..."

java -jar -Dprocesses=2 -Dinitiator=initiator -Dreceiver=receiver -DisInitiator=true target/players-0.0.1-SNAPSHOT-shaded.jar 
