package com.coding.trade.players;

import com.coding.trade.players.service.IMessageService;

/**
 * 
 * @author Dharmendra Agrawal
 */
public class Player extends AbstractPlayer {

    public Player(String ownerId, String playerId, boolean isInitiator,
        IMessageService messageService) {

        super(ownerId, playerId, isInitiator, messageService);

    }

}
