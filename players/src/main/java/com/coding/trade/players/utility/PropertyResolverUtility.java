package com.coding.trade.players.utility;

/**
 * helps reading java arguments and return them in the expected data type
 * 
 * @author Dharmendra Agrawal
 *
 */
public class PropertyResolverUtility {
    /**
     * reads java argument, convert it to integer and return it
     * 
     * @param propName
     * @return
     */
    public static int getIntProperty(String propName) {
        int intValue = 0;
        try {
            intValue = Integer.parseInt(System.getProperty(propName));
        } catch (NumberFormatException ex) {
            System.out.println(
                "expected numeric value but found alpha numeric : " + propName);
        }

        return intValue;
    }

    /**
     * reads java argument, convert it to boolean and return it
     * 
     * @param propName
     * @return
     */
    public static boolean getBooleanProperty(String propName) {

        return Boolean.parseBoolean(System.getProperty(propName));

    }

    /**
     * reads java argument and return it
     * 
     * @param propName
     * @return
     */

    public static String getStringProperty(String propName) {
        return System.getProperty(propName);
    }
}
