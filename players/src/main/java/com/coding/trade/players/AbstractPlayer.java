package com.coding.trade.players;

import java.text.MessageFormat;

import com.coding.trade.players.common.Constants;
import com.coding.trade.players.common.InfraException;
import com.coding.trade.players.service.CommandLineUserInputService;
import com.coding.trade.players.service.IMessageService;
import com.coding.trade.players.service.IUserInputService;

/**
 * All the players should extend this abstract implementation and override
 * methods as required
 * 
 * @author Dharmendra Agrawal
 *
 */
public abstract class AbstractPlayer {
    /**
     * id to identify a player uniquely
     */
    private String ownerId;

    /**
     * id of the other player- receiver
     */
    private String playerId;

    /**
     * count of messages sent by this player
     */
    private int msgSentCount;

    /**
     * count of messages received by this player
     */
    private int msgReceivedCount;

    /**
     * indicates if this player will be taking input from defined input service
     */
    private boolean isInitiator;

    /**
     * allows player to take input from different sources e.g user terminal
     */
    private IUserInputService userInputService;

    /**
     * defines communication layer between players- e.g in-memory, file systems
     * etc
     */
    private IMessageService messageService;

    public AbstractPlayer(String ownerId, String playerId, boolean isInitiator,
        IMessageService messageService) {
        this.ownerId = ownerId;
        this.playerId = playerId;
        this.isInitiator = isInitiator;
        this.messageService = messageService;

        // setting up the default implementation
        userInputService = CommandLineUserInputService.getInstance();
        messageService.register(this);
    }

    /**
     * sends the message to the other player identified by playerId via
     * communication channel as defined by message service implementation
     * 
     * @param message
     * 
     */
    public void sendMessage(String message) {
        if (message == null || message.isEmpty())
            return;
        try {
            this.setMsgSentCount(this.getMsgSentCount() + 1);
            System.out.println(MessageFormat.format(
                "Player-{0} sending message-{1}", this.getOwnerId(), message));
            messageService.sendMessage(message, this);
        } catch (InfraException e) {
            System.out.println(MessageFormat.format(
                "Player Process:{0} failed to send message...",
                this.getOwnerId()));
            shutDown();
        }

    }

    /**
     * receives a message from sender player via communication channel as
     * defined by message service implementation
     * 
     * receiver can append number to the received message and sends
     * acknowledgement back to the sender player
     * 
     * if received message is an acknowledgement of the previously sent message
     * then player takes necessary actions- checkStopCondition
     * 
     * @param message
     * 
     */
    public void listenMessage(String message) {
        System.out.println(MessageFormat.format(
            "Player-{0} received message-{1}", this.getOwnerId(), message));
        this.setMsgReceivedCount(this.getMsgReceivedCount() + 1);
        try {
            if (this.isInitiator()) {
                checkStopCondition();
                sendMessage(takeUserMsgInput());
            } else {
                String msg = buildMessage(message);
                sendMessage(msg);
                checkStopCondition();
            }
        } catch (InfraException e) {
            e.printStackTrace();
            shutDown();
        }

    }

    /**
     * takes the user input from channel defined by user input service throws
     * InfraException if fails to read message from user
     * 
     * @return
     * @throws InfraException
     * 
     */
    public String takeUserMsgInput() throws InfraException {
        return userInputService.takeUserInput(this.getMsgSentCount() + 1);
    }

    /**
     * checks stop condition based on number of messages sent and received and
     * shuts down the application gracefully
     * 
     * @return
     */
    public boolean checkStopCondition() {
        boolean canStop = this.getMsgReceivedCount() == Constants.MAX_MSG_COUNT
            && this.getMsgSentCount() == Constants.MAX_MSG_COUNT;
        if (canStop) {
            shutDown();
        }
        return canStop;

    }

    private void shutDown() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(
            "\n**********************************************************************\n");
        System.out.println(MessageFormat.format(
            "Player Process:{0} will be stopping gracefully...",
            this.getOwnerId()));
        System.exit(0);
    }

    /**
     * prepares a message for sending acknowledgement by appending number at the
     * end of the message received
     * 
     * @param msg
     * @return
     */
    public String buildMessage(String msg) {
        StringBuilder stringBuilder = new StringBuilder(msg);
        stringBuilder.append(Constants.MSG_SEPARATOR);
        stringBuilder.append(this.getMsgReceivedCount());
        return stringBuilder.toString();
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public int getMsgSentCount() {
        return msgSentCount;
    }

    public void setMsgSentCount(int msgSentCount) {
        this.msgSentCount = msgSentCount;
    }

    public int getMsgReceivedCount() {
        return msgReceivedCount;
    }

    public void setMsgReceivedCount(int msgReceivedCount) {
        this.msgReceivedCount = msgReceivedCount;
    }

    public boolean isInitiator() {
        return isInitiator;
    }

    public void setInitiator(boolean isInitiator) {
        this.isInitiator = isInitiator;
    }

    public IUserInputService getUserInputService() {
        return userInputService;
    }

    public void setUserInputService(IUserInputService userInputService) {
        this.userInputService = userInputService;
    }

    public IMessageService getMessageService() {
        return messageService;
    }

    public void setMessageService(IMessageService messageService) {
        this.messageService = messageService;
    }

}
