package com.coding.trade.players.utility;

import java.util.HashSet;
import java.util.Set;

public class TestString {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add(" ".trim());
        set.add(" ".trim());
        set.add(" ".trim());
        set.add("hi");
        set.add(" ".trim());
        set.add(null);
        set.add(null);

        System.out.println(String.join(",", set));
    }
}
