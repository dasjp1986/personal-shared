package com.coding.trade.players.service;

import com.coding.trade.players.common.InfraException;
/**
 * 
 * @author Dharmendra Agrawal
 *
 */
public interface IUserInputService {
    /**
     * override this method and define implementation to take user inputs from
     * input channel
     * 
     * @param msgNbr
     * @return
     * @throws InfraException
     * 
     */
    String takeUserInput(int msgNbr) throws InfraException;

    /**
     * override this method and define implementation to close the input channel
     * gracefully
     */
    void closeConnection();
}
