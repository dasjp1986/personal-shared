package com.coding.trade.players.service;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.common.InfraException;
import com.coding.trade.players.jobs.MessageFileSystemWatcherJob;
import com.coding.trade.players.utility.FileSystemReadWriteUtility;

/**
 * defines implementation to use file system as message communication channel
 * 
 * @author Dharmendra Agrawal
 *
 */
public class MessageFileSystemService implements IMessageService {

    @Override
    public AbstractPlayer register(AbstractPlayer player) {
        MessageFileSystemWatcherJob messageWatcherJob =
            new MessageFileSystemWatcherJob(player);
        Thread thread = new Thread(messageWatcherJob);
        thread.start();

        return player;
    }

    @Override
    public void sendMessage(String msg, AbstractPlayer player)
        throws InfraException {
        FileSystemReadWriteUtility.write(msg, player.getPlayerId());
    }

}
