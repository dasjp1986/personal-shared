package com.coding.trade.players.service;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.common.InfraException;

/**
 * 
 * @author Dharmendra Agrawal
 *
 */
public interface IMessageService {
    /**
     * override this method to send message to the player via channel defined in
     * the implementation
     * 
     * @param msg
     * @param player
     * @throws InfraException
     * 
     * 
     */
    void sendMessage(String msg, AbstractPlayer player) throws InfraException;

    /**
     * override this method to register player to receive message via implemented
     * message channel
     * 
     * @param player
     */
    AbstractPlayer register(AbstractPlayer player);
}
