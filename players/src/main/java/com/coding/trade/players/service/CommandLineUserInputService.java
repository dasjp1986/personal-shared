package com.coding.trade.players.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;

import com.coding.trade.players.common.InfraException;

/**
 * defines implementation to take user input from command line terminal
 * 
 * @author Dharmendra Agrawal
 *
 */
public class CommandLineUserInputService implements IUserInputService {

    private static volatile CommandLineUserInputService SINGLETON;
    private final BufferedReader reader;

    private CommandLineUserInputService() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * creates singleton instance of this implementation to avoid unnecessary
     * creation of objects
     * 
     * @return UserInputService
     */
    public static IUserInputService getInstance() {
        if (SINGLETON == null) {
            synchronized (CommandLineUserInputService.class) {
                if (SINGLETON == null)
                    SINGLETON = new CommandLineUserInputService();
            }
        }

        return SINGLETON;
    }

    @Override
    public String takeUserInput(int msgNbr) throws InfraException {
        String msg = "";
        try {
            System.out.println();
            System.out.println(MessageFormat
                .format("Please type your Message[msg#:{0}] : ", msgNbr));
            msg = reader.readLine();
        } catch (IOException e) {
            throw new InfraException(
                "Error in reading message from user terminal", e);
        }
        return msg;
    }

    @Override
    public void closeConnection() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
