package com.coding.trade.players.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.Player;

/**
 * defines a factory of players which manages creation of players and store them
 * for future use
 * 
 * @author Dharmendra Agrawal
 *
 */
public class PlayersFactoryService implements IPlayerFactoryService {

    private static volatile IPlayerFactoryService SINGLETON;
    private Map<String, AbstractPlayer> factory;

    private PlayersFactoryService() {
        factory = new ConcurrentHashMap<>();
    }

    public static IPlayerFactoryService getInstance() {
        if (SINGLETON == null) {
            synchronized (PlayersFactoryService.class) {
                if (SINGLETON == null) {
                    SINGLETON = new PlayersFactoryService();
                }
            }
        }
        return SINGLETON;
    }

    @Override
    public AbstractPlayer getPlayer(String ownerId, String playerId,
        boolean isInitiator, IMessageService messageService) {
        AbstractPlayer player = factory.get(ownerId);
        if (player == null) {
            player = new Player(ownerId, playerId, isInitiator, messageService);
            factory.put(ownerId, player);
        }
        return player;
    }

    @Override
    public void cleanUp() {
        factory.clear();
        SINGLETON = null;
    }
}
