package com.coding.trade.players.common;

/**
 * defines a custom exception for all infrastructure related issues
 * 
 * @author Dharmendra Agrawal
 *
 */
public class InfraException extends Exception {

    /**
    	 * 
    	 */
    private static final long serialVersionUID = -6559349206107563917L;

    public InfraException(String msg) {
        super(msg);
    }

    public InfraException(String msg, Throwable ex) {
        super(msg, ex);
    }
}
