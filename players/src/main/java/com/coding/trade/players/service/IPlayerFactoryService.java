package com.coding.trade.players.service;

import com.coding.trade.players.AbstractPlayer;

public interface IPlayerFactoryService {

    /**
     * create or get a player from factory
     * 
     * @param ownerId
     * @param playerId
     * @param isInitiator
     * @param messageService
     * @return
     */
    AbstractPlayer getPlayer(String ownerId, String playerId,
        boolean isInitiator, IMessageService messageService);

    /**
     * cleans up factory of players created
     */
    void cleanUp();

}