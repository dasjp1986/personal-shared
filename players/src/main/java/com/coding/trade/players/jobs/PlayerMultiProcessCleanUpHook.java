package com.coding.trade.players.jobs;

import java.text.MessageFormat;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.utility.FileSystemReadWriteUtility;

/**
 * this class defines a job which will be running the cleanup code when JVM
 * shuts down to release resources created by multiple player process
 * 
 * @author Dharmendra Agrawal
 *
 */
public class PlayerMultiProcessCleanUpHook extends Thread {

    private AbstractPlayer player;

    public PlayerMultiProcessCleanUpHook(AbstractPlayer player) {
        this.player = player;
    }

    @Override
    public void run() {
        System.out.println(MessageFormat.format(
            "Clean up data for Player Process:{0}", player.getOwnerId()));
        FileSystemReadWriteUtility.cleanUp(player.getOwnerId());
        player.getUserInputService().closeConnection();
        System.out.println(MessageFormat.format(
            "Clean up completed for Player Process:{0}", player.getOwnerId()));
    }
}
