package com.coding.trade.players.service;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.common.InfraException;

/**
 * defines implementation to use in-memory message communication channel
 * 
 * @author Dharmendra Agrawal
 */
public class MessageInMemoryService implements IMessageService {

    public MessageInMemoryService() {
    }

    @Override
    public void sendMessage(String msg, AbstractPlayer player)
        throws InfraException {
        AbstractPlayer receiver =
            PlayersFactoryService.getInstance().getPlayer(player.getPlayerId(),
                player.getOwnerId(), !player.isInitiator(), this);
        receiver.listenMessage(msg);
    }

    @Override
    public AbstractPlayer register(AbstractPlayer player) {
        // does not require anything during registration
        return player;
    }

}
