package com.coding.trade.players.common;

/**
 * 
 * @author Dharmendra Agrawal
 *
 */
public interface Constants {

    int MAX_MSG_COUNT = 10;
    int WATCHER_TIMEOUT_SECONDS=120;
    String MSG_SEPARATOR = ":";
    String INITIATOR = "initiator";
    String RECEIVER = "receiver";
    String USER_HOME = "user.home";
    String TEMP_DIR = "temp";
    String IS_INITIATOR = "isInitiator";
    String PROCESSES = "processes";

}
