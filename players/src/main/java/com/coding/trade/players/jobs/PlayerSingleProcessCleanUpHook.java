package com.coding.trade.players.jobs;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.service.PlayersFactoryService;

/**
 * this class defines a job which will be running the cleanup code when JVM
 * shuts down to release resources created by single player process
 * 
 * @author Dharmendra Agrawal
 *
 */
public class PlayerSingleProcessCleanUpHook extends Thread {

    private AbstractPlayer player;

    public PlayerSingleProcessCleanUpHook(AbstractPlayer player) {
        this.player = player;
    }

    @Override
    public void run() {
        System.out.println("Clean up data for Players in-memory");
        PlayersFactoryService.getInstance().cleanUp();
        player.getUserInputService().closeConnection();
        System.out.println("Clean up completed for Players in-memory");
    }
}
