package com.coding.trade.players.init;

import java.text.MessageFormat;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.common.Constants;
import com.coding.trade.players.common.InfraException;
import com.coding.trade.players.jobs.PlayerMultiProcessCleanUpHook;
import com.coding.trade.players.jobs.PlayerSingleProcessCleanUpHook;
import com.coding.trade.players.service.MessageFileSystemService;
import com.coding.trade.players.service.MessageInMemoryService;
import com.coding.trade.players.service.PlayersFactoryService;
import com.coding.trade.players.utility.PropertyResolverUtility;

/**
 * this is the main entry point class, which takes all required parameters as
 * inputs and decides to create players in single or multiple process
 * environment
 * 
 * @author Dharmendra Agrawal
 *
 */
public class AppStarter {

    public static void main(String[] args) {

        int processNum =
            PropertyResolverUtility.getIntProperty(Constants.PROCESSES);

        String ownerId = System.getProperty(Constants.INITIATOR);
        String playerId = System.getProperty(Constants.RECEIVER);
        boolean isInitiator =
            Boolean.parseBoolean(System.getProperty(Constants.IS_INITIATOR));

        switch (processNum) {
        case 1:
            startPlayersSingleProcess(ownerId, playerId, isInitiator);
            break;

        case 2:
            startPlayersMultiProcess(ownerId, playerId, isInitiator);
            break;

        default:
            System.out.println(
                "These Number of processes are not yet supported, processes: "
                    + processNum);
            break;
        }

    }

    private static void startPlayersSingleProcess(String ownerId,
        String playerId, boolean isInitiator) {

        AbstractPlayer initiatorPlayer =
            PlayersFactoryService.getInstance().getPlayer(ownerId, playerId,
                isInitiator, new MessageInMemoryService());
        Runtime.getRuntime().addShutdownHook(
            new PlayerSingleProcessCleanUpHook(initiatorPlayer));
        try {
            initiatorPlayer.sendMessage(initiatorPlayer.takeUserMsgInput());
        } catch (InfraException e) {
            e.printStackTrace();
        }
    }

    private static void startPlayersMultiProcess(String ownerId,
        String playerId, boolean isInitiator) {

        System.out.println(
            MessageFormat.format("initiator:{0},receiver:{1}, isInitiator:{2}",
                ownerId, playerId, isInitiator));
        AbstractPlayer player = PlayersFactoryService.getInstance().getPlayer(
            ownerId, playerId, isInitiator, new MessageFileSystemService());
        Runtime.getRuntime()
            .addShutdownHook(new PlayerMultiProcessCleanUpHook(player));

        if (isInitiator)
            try {
                player.sendMessage(player.takeUserMsgInput());
            } catch (InfraException e) {
                e.printStackTrace();
            }

    }

}
