package com.coding.trade.players.jobs;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

import com.coding.trade.players.AbstractPlayer;
import com.coding.trade.players.common.Constants;
import com.coding.trade.players.common.InfraException;
import com.coding.trade.players.utility.FileSystemReadWriteUtility;

/**
 * this class defines a job which keep watching changes in the file system
 * directory. invokes the player registered as an Observer when file create
 * events occurs
 * 
 * @author Dharmendra Agrawal
 *
 */
public class MessageFileSystemWatcherJob implements Runnable {

    /**
     * holds reference to the observer player object
     */
    private AbstractPlayer player;

    public MessageFileSystemWatcherJob(AbstractPlayer player) {
        this.player = player;
    }

    public void initiate() throws InfraException {
        String dirPath = FileSystemReadWriteUtility.setup(player.getOwnerId());

        try (WatchService watchService =
            FileSystems.getDefault().newWatchService()) {
            Path dir = Paths.get(dirPath);
            dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_DELETE);
            WatchKey watchKey = null;
            while ((watchKey = watchService.poll(
                Constants.WATCHER_TIMEOUT_SECONDS, TimeUnit.SECONDS)) != null) {

                for (WatchEvent<?> event : watchKey.pollEvents()) {
                    @SuppressWarnings("unchecked")
                    WatchEvent<Path> pathEvent = (WatchEvent<Path>) event;
                    Path path = pathEvent.context();

                    if (StandardWatchEventKinds.ENTRY_CREATE == event.kind()) {
                        player.listenMessage(FileSystemReadWriteUtility
                            .read(dirPath + File.separator + path.toString()));
                    }

                }

                // IMPORTANT: The key must be reset after processed
                boolean valid = watchKey.reset();
                if (!valid) {
                    break;
                }
            }
        } catch (InterruptedException | IOException e) {
            throw new InfraException("Error in FileSystem Watcher", e);
        }
    }

    @Override
    public void run() {
        try {
            initiate();
        } catch (InfraException e) {
            e.printStackTrace();
        }
    }

}
