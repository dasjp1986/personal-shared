package com.coding.trade.players.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

import com.coding.trade.players.common.Constants;
import com.coding.trade.players.common.InfraException;

/**
 * defines various methods to read-write data from file system
 * 
 * @author Dharmendra Agrawal
 *
 */
public class FileSystemReadWriteUtility {

    private static final String BASE_DIR =
        System.getProperty(Constants.USER_HOME) + File.separator
            + Constants.TEMP_DIR;

    /**
     * sets up directory from where to read/write data
     * 
     * @param dirName
     * @return
     */
    public static String setup(String dirName) {
        String dirPath = BASE_DIR + File.separator + dirName;
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        return dirPath;
    }

    /**
     * cleans up the directory setup by setup() method
     * 
     * @param id
     */
    public static void cleanUp(String id) {
        File baseDir = new File(BASE_DIR + File.separator + id);
        deleteRecusively(baseDir);
    }

    /**
     * writes the message in new a file with a random file name inside sub
     * directory named- dirName
     * 
     * @param message
     * @param dirName
     * @throws InfraException
     */
    public static void write(String message, String dirName)
        throws InfraException {
        try {
            String dirPath = setup(dirName);
            File file = new File(
                dirPath + File.separator + UUID.randomUUID().toString());
            file.createNewFile();

            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(message);
            writer.close();
        } catch (IOException e) {
            throw new InfraException("error in writing message", e);
        }
    }

    /**
     * reads data from the file at given path
     * 
     * @param filePath
     * @return
     * @throws InfraException
     */
    public static String read(String filePath) throws InfraException {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader =
            new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            throw new InfraException("error in reading message", e);
        }
        return stringBuilder.toString();
    }

    private static void deleteRecusively(File dir) {
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                deleteRecusively(file);
            } else
                file.delete();
        }
        dir.delete();
    }
}
