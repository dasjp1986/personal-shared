package com.coding.neo4j;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.TransactionWork;

public class HelloWorldExample implements AutoCloseable {
	private final Driver driver;

	public HelloWorldExample(String uri, String user, String password) {
		driver = (Driver) GraphDatabase.driver(uri, AuthTokens.basic(user, password));
	}

	public void close() throws Exception {
		driver.close();
	}

	public void printGreeting(final String message) {
		try (Session session = driver.session()) {
			String greeting = session.writeTransaction(new TransactionWork<String>() {

				@Override
				public String execute(Transaction tx) {
					Map<String, Object> parameters= new HashMap<>();
					parameters.put("message", message);
					StatementResult result = tx.run("CREATE (a:Greeting) " + "SET a.message = $message "
							+ "RETURN a.message + ', from node ' + id(a)", parameters);
					return result.single().get(0).asString();
				}
			});
			
			System.out.println(greeting);
		}
		
	}
	
	public static void main(String[] args) {
		HelloWorldExample example= new HelloWorldExample("bolt://localhost:7687", "neo4j", "admin");
		example.printGreeting("Hello, there");
	}

}
