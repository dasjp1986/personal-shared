package com.da.coding.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given an array of integers, return the highest product possible by
 *         multiplying 3 numbers from the array
 * 
 *         Input:
 * 
 *         array of integers e.g {1, 2, 3}
 * 
 *         NOTE: Solution will fit in a 32-bit signed integer
 * 
 *         Example:
 * 
 *         [0, -1, 3, 100, 70, 50]
 * 
 *         => 70*50*100 = 350000
 * 
 * 
 */
public class MaxProduct {

	public int maxp3(int[] A) {
		int n = A.length;
		Integer[] array = new Integer[A.length];
		if (n < 3) {
			return 0;
		}
		for (int i = 0; i < array.length; i++) {
			array[i] = A[i];
		}
		Arrays.sort(array, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}

		});

		return Math.max(array[0] * array[1] * array[2], array[0] * array[n - 2] * array[n - 1]);
	}

}
