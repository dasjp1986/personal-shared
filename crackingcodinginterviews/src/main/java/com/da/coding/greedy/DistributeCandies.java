package com.da.coding.greedy;

/**
 * 
 * @author dagraw1
 *
 *
 *
 * 
 *         There are N children standing in a line. Each child is assigned a
 *         rating value.
 * 
 *         You are giving candies to these children subjected to the following
 *         requirements:
 * 
 *         Each child must have at least one candy. Children with a higher
 *         rating get more candies than their neighbors.
 * 
 *         What is the minimum candies you must give?
 * 
 *         Sample Input :
 * 
 *         Ratings : [1 2]
 * 
 *         Sample Output :
 * 
 *         3
 * 
 *         The candidate with 1 rating gets 1 candy and candidate with rating
 *         cannot get 1 candy as 1 is its neighbor. So rating 2 candidate gets 2
 *         candies. In total, 2+1 = 3 candies need to be given out.
 * 
 */

public class DistributeCandies {

	public int candy(int[] A) {

		int[] candies = new int[A.length];
		for (int i = 0; i < candies.length; i++) {
			candies[i] = 1;
		}

		for (int i = 1; i < A.length; i++) {
			if (A[i] > A[i - 1]) {
				candies[i] = candies[i - 1] + 1;
			}
		}

		int total = candies[A.length - 1];
		for (int i = A.length - 1; i > 0; i--) {
			if (A[i - 1] > A[i] && candies[i - 1] <= candies[i]) {
				candies[i - 1] = candies[i] + 1;
			}
			total += candies[i - 1];
		}

		return total;

	}

	public static void main(String[] args) {
		int[] array = { 1, 2, 3 };
		DistributeCandies distributeCandies = new DistributeCandies();
		System.out.println(distributeCandies.candy(array));
	}

}

class Child {
	Integer rating;
	Integer pos;

	public Child(Integer rating, Integer pos) {
		this.rating = rating;
		this.pos = pos;
	}

	@Override
	public String toString() {
		return "Child [rating=" + rating + "]";
	}

}
