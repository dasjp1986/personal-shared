package com.da.coding.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 
 * @author dagraw1
 *
 *         you are given n activities with their start and finish times. Select
 *         the maximum number of activities that can be performed by a single
 *         person, assuming that a person can only work on a single activity at
 *         a time. Example:
 * 
 *         Example 1 : Consider the following 3 activities sorted by by finish
 *         time. start[] = {10, 12, 20}; finish[] = {20, 25, 30}; A person can
 *         perform at most two activities. The maximum set of activities that
 *         can be executed is {0, 2} [ These are indexes in start[] and finish[]
 *         ]
 * 
 *         Example 2 : Consider the following 6 activities sorted by by finish
 *         time. start[] = {1, 3, 0, 5, 8, 5}; finish[] = {2, 4, 6, 7, 9, 9}; A
 *         person can perform at most four activities. The maximum set of
 *         activities that can be executed is {0, 1, 3, 4} [ These are indexes
 *         in start[] and finish[] ]
 */
public class ActivitySelectionProblem {
	// Time Complexity- O(nlogn)
	public int activities(ActivityTime[] activs) {

		int count = 1;
		Arrays.sort(activs, new Comparator<ActivityTime>() {

			@Override
			public int compare(ActivityTime o1, ActivityTime o2) {
				return o1.finish.compareTo(o2.finish);
			}
		});
		int finish= activs[0].finish;
		for (int i = 1; i < activs.length; i++) {
			if(activs[i].start>=finish){
				count++;
				
				finish= activs[i].finish;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		ActivityTime[] acTimes= new ActivityTime[6];
		acTimes[0]= new ActivityTime(1, 2);
		acTimes[1]= new ActivityTime(2, 6);
		acTimes[2]= new ActivityTime(2, 5);
		acTimes[3]= new ActivityTime(5, 7);
		acTimes[4]= new ActivityTime(8, 9);
		acTimes[5]= new ActivityTime(5, 9);
		
		ActivitySelectionProblem problem= new ActivitySelectionProblem();
		System.out.println(problem.activities(acTimes));
	}
}

class ActivityTime {
	Integer start;
	Integer finish;

	public ActivityTime(Integer start, Integer finish) {
		this.start = start;
		this.finish = finish;
	}
}
