package com.da.coding.greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * 
 * @author dagraw1
 *
 *
 * Input:
 * 1
5 100
100 40
50 40
40 50
60 20
100 50

output:
210
 */
public class FlowersAndFertilizers {
	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int testCases = scn.nextInt();
		for (int t = 0; t < testCases; t++) {
			int n = scn.nextInt();
			int p = scn.nextInt();

			Map<Integer, List<Integer>> map = new TreeMap<>();
			for (int i = 0; i < n; i++) {
				int fl = scn.nextInt();
				int fr = scn.nextInt();

				List<Integer> list = map.get(fr);
				if (list == null) {
					list = new ArrayList<>();
					map.put(fr, list);
				}
				list.add(fl);
			}
			System.out.println(solve(n, p, map));
		}
		scn.close();
	}

	private static int solve(int n, int p, Map<Integer, List<Integer>> map) {
		int max = 0;
		int price = p;
		int profit = 0;
		for (Integer fr : map.keySet()) {
			List<Integer> list = map.get(fr);
			Collections.sort(list);

			for (Integer fl : list) {
				if (price - fr >= 0) {
					profit += fl;
					price -= fr;
				} else {
					profit += price;
					if (profit > max) {
						max = profit;
					}
					price = p;
				}
			}
		}
		return max;
	}
}
