package com.da.coding.greedy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author dagraw1
 *
 *         Given n sorted lists, your task is to merge all n lists into a single
 *         sorted list with minimal operations performed
 * 
 * 
 */
public class MergeNSortedList {
	/**
	 * Merging two lists at a time whose size are the least can give best results
	 */

	public int merge(List<List<Integer>> lists) {
		int count = 0;

		if (lists.size() < 2) {
			return count;
		}

		int first = Integer.MAX_VALUE, second = Integer.MAX_VALUE;
		int firstIndex = 0, secondIndex = 0;

		for (int i = 0; i < lists.size(); i++) {
			List<Integer> list = lists.get(i);
			if (list.size() < first) {
				first = list.size();
				secondIndex = firstIndex;
				firstIndex = i;
			} else if (list.size() < second) {
				second = list.size();
				secondIndex = i;
			}

		}

		count = lists.get(firstIndex).size() + lists.get(secondIndex).size();
		List<Integer> merged = join(lists.get(firstIndex), lists.get(secondIndex));

		if (firstIndex > secondIndex) {
			lists.remove(firstIndex);
			lists.remove(secondIndex);
		} else {
			lists.remove(secondIndex);
			lists.remove(firstIndex);
		}

		lists.add(merged);

		count = count + merge(lists);
		return count;
	}

	private List<Integer> join(List<Integer> list1, List<Integer> list2) {
		int i = 0, j = 0;
		List<Integer> result = new ArrayList<>();
		while (i < list1.size() && j < list2.size()) {
			if (list1.get(i) < list2.get(j)) {
				result.add(list1.get(i));
				i++;
			} else {
				result.add(list2.get(j));
				j++;
			}
		}

		while (i < list1.size()) {
			result.add(list1.get(i++));
		}
		while (j < list2.size()) {
			result.add(list2.get(j++));
		}
		return result;
	}

	public static void main(String[] args) {
		List<List<Integer>> lists = new ArrayList<>();
		//expected- 31
//		lists.add(createList(6));
//		lists.add(createList(5));
//		lists.add(createList(2));
//		lists.add(createList(3));

		//expected- 205
		lists.add(createList(20));
		lists.add(createList(30));
		lists.add(createList(10));
		lists.add(createList(5));
		lists.add(createList(30));
		
		MergeNSortedList merge = new MergeNSortedList();
		System.out.println(merge.merge(lists));

	}

	private static List<Integer> createList(int n) {
		List<Integer> list = new ArrayList<>();
		Random random = new Random();
		for (int i = 0; i < n; i++) {
			list.add(random.nextInt(100));
		}
		return list;
	}
}
