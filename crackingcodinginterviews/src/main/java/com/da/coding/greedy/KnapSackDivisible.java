package com.da.coding.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 
 * @author dagraw1
 *
 * 
 *         Given weights and values of n items, put these items in a knapsack of
 *         capacity W to get the maximum total value in the knapsack. In other
 *         words, given two integer arrays val[0..n-1] and wt[0..n-1] which
 *         represent values and weights associated with n items respectively.
 *         Also given an integer W which represents knapsack capacity, find out
 *         the maximum value subset of val[] such that sum of the weights of
 *         this subset is smaller than or equal to W.
 * 
 * 
 *         You are allowed to break an item.
 *
 */
public class KnapSackDivisible {
	public double maxProfit(int[] values, int[] weights, double capacity) {
		double maxProfit = 0;
		WeightRatio[] wr = new WeightRatio[values.length];
		for (int i = 0; i < wr.length; i++) {
			wr[i] = new WeightRatio(weights[i], (values[i] * 1.0) / weights[i]);
		}

		Arrays.sort(wr, new Comparator<WeightRatio>() {
			@Override
			public int compare(WeightRatio o1, WeightRatio o2) {
				return o2.ratio.compareTo(o1.ratio);
			}

		});

		for (WeightRatio wRatio : wr) {
			double w = wRatio.weight <= capacity ? wRatio.weight : capacity;
			maxProfit = maxProfit + w * wRatio.ratio;
			capacity = capacity - w;
			System.out.println("capacity : " + capacity + ", profit : " + maxProfit);
		}

		return maxProfit;
	}

	public static void main(String[] args) {
		int[] values = { 10, 5, 15, 7, 6, 18, 3 };
		int[] weights = { 2, 3, 5, 7, 1, 4, 1 };
		KnapSackDivisible knDivisible = new KnapSackDivisible();
		System.out.println(knDivisible.maxProfit(values, weights, 15.0));
	}

}

class WeightRatio {
	int weight;
	Double ratio;

	public WeightRatio(int weight, Double ratio) {
		this.weight = weight;
		this.ratio = ratio;
	}

	@Override
	public String toString() {
		return "WeightRatio [weight=" + weight + ", ratio=" + ratio + "]";
	}

}
