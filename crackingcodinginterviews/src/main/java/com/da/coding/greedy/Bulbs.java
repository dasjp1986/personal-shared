package com.da.coding.greedy;

/**
 * 
 * @author dagraw1
 *
 *
 *
 * 
 *         N light bulbs are connected by a wire. Each bulb has a switch
 *         associated with it, however due to faulty wiring, a switch also
 *         changes the state of all the bulbs to the right of current bulb.
 *         Given an initial state of all bulbs, find the minimum number of
 *         switches you have to press to turn on all the bulbs. You can press
 *         the same switch multiple times.
 * 
 *         Note : 0 represents the bulb is off and 1 represents the bulb is on.
 * 
 *         Example:
 * 
 *         Input : [0 1 0 1] Return : 4
 * 
 *         Explanation : press switch 0 : [1 0 1 0] press switch 1 : [1 1 0 1]
 *         press switch 2 : [1 1 1 0] press switch 3 : [1 1 1 1]
 * 
 * 
 */
public class Bulbs {

	public int bulbs(int[] A) {
		if (A.length < 1) {
			return 0;
		}

		A[0] = A[0] == 0 ? 1 : 0;

		for (int i = 1; i < A.length; i++) {
			if (A[i - 1] % 2 == 0) {
				A[i] = A[i] == 0 ? A[i - 1] + 1 : A[i - 1];
			} else {
				A[i] = A[i] == 0 ? A[i - 1] : A[i - 1] + 1;
			}
		}

		return A[A.length - 1];
	}

}
