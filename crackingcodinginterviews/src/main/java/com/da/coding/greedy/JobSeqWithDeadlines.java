package com.da.coding.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 
 * @author dagraw1
 *
 *
 *         you are given array of jobs, each job has corresponding profit value
 *         and deadlines associated with it.
 * 
 *         each job requires 1 unit of work to be.
 * 
 *         your job is to maximize the profit.
 * 
 *         Ex- profit values: 20, 15, 10, 5, 1 deadlines- 2, 2, 1, 3, 3
 * 
 *         output: 20+15+5= 40
 */
public class JobSeqWithDeadlines {

	public int maxProfit(Integer[] profits, Integer[] deadlines) {

		int largest = deadlines[0];
		Job[] jobs = new Job[profits.length];
		for (int i = 0; i < deadlines.length; i++) {
			if (deadlines[i] > largest) {
				largest = deadlines[i];
			}
			jobs[i] = new Job(profits[i], deadlines[i]);
		}

		Arrays.sort(jobs, new Comparator<Job>() {

			@Override
			public int compare(Job o1, Job o2) {
				return o2.profit.compareTo(o1.profit);
			}
		});

		Integer[] sequences = new Integer[largest];

		int maxProfit = 0;
		for (Job job : jobs) {
			int pos = job.deadline - 1;
			while (pos >= 0) {
				if (sequences[pos] == null) {
					sequences[pos] = job.profit;
					maxProfit = maxProfit + job.profit;
					break;
				}
				pos--;
			}
		}
		return maxProfit;
	}

	public static void main(String[] args) {

		JobSeqWithDeadlines jobproblem = new JobSeqWithDeadlines();
		System.out.println(jobproblem.maxProfit(new Integer[] { 20, 15, 10, 5, 1 }, new Integer[] { 2, 2, 1, 3, 3 }));
		System.out.println(jobproblem.maxProfit(new Integer[] { 20, 15, 10, 5 }, new Integer[] { 4, 1, 5, 2 }));

	}
}

class Job {
	Integer profit;
	Integer deadline;

	public Job(Integer profit, Integer deadline) {
		this.profit = profit;
		this.deadline = deadline;

	}
}
