package com.da.coding.combinations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1 Given a string containing digits from 2-9 inclusive, return
 *         all possible letter combinations that the number could represent.
 * 
 *         A mapping of digit to letters (just like on the telephone buttons) is
 *         given below. Note that 1 does not map to any letters.
 * 
 *         Example:
 * 
 *         Input: "23" Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce",
 *         "cf"].
 */
public class LetterCombinationsOfPhoneNbr {
	public List<String> letterCombinations(String digits) {
		List<String> result = new ArrayList<>();

		Map<Character, String> digitLettersMap = getMap();
		for (int i = 0; i < digits.length(); i++) {
			result = findCombo(digitLettersMap.get(digits.charAt(i)), result);
		}

		return result;
	}

	private List<String> findCombo(String str, List<String> currentList) {
		if (str == null) {
			throw new RuntimeException("Wrong input digit");
		}
		List<String> resultList = new ArrayList<>();
		if (currentList.isEmpty()) {
			for (int i = 0; i < str.length(); i++) {
				resultList.add(str.substring(i, i + 1));
			}
		} else {
			for (int i = 0; i < str.length(); i++) {
				for (int j = 0; j < currentList.size(); j++) {
					resultList.add(currentList.get(j) + str.charAt(i));
				}
			}
		}

		return resultList;
	}

	private Map<Character, String> getMap() {
		Map<Character, String> digitLettersMap = new HashMap<>();
		digitLettersMap.put('2', "abc");
		digitLettersMap.put('3', "def");
		digitLettersMap.put('4', "ghi");
		digitLettersMap.put('5', "jkl");
		digitLettersMap.put('6', "mno");
		digitLettersMap.put('7', "pqrs");
		digitLettersMap.put('8', "tuv");
		digitLettersMap.put('9', "wxyz");
		return digitLettersMap;
	}

	public static void main(String[] args) {
		LetterCombinationsOfPhoneNbr problem = new LetterCombinationsOfPhoneNbr();
		System.out.println(problem.letterCombinations("234"));
	}
}
