package com.da.coding.tries;

import com.da.coding.commons.Constants;
import com.da.coding.queues.QueueUsingSingleLinkedList;

public class TrieUsingArray {
	private TrieNode[] nodes;

	public TrieUsingArray() {
		nodes = new TrieNode[Constants.ASCII_SIZE];
	}

	public void add(String word) {
		if (word == null || word.isEmpty())
			throw new RuntimeException("Word is empty...");
		addNode(nodes, word, 0);
	}

	public boolean findPrefix(String word) {
		if (word == null || word.isEmpty())
			throw new RuntimeException("Word is empty...");
		return isPresent(nodes, word, 0, WORD_SERACH_TYPE.PREFIX_WORD);
	}

	public boolean findCompleteWord(String word) {
		if (word == null || word.isEmpty())
			throw new RuntimeException("Word is empty...");
		return isPresent(nodes, word, 0, WORD_SERACH_TYPE.COMPLETE_WORD);
	}

	//Time Complexity- O(K), K-length of the word being searched
	private boolean isPresent(TrieNode[] nodes, String word, int charPosition, WORD_SERACH_TYPE type) {
		char ch = word.charAt(charPosition++);
		TrieNode node = nodes[ch];
		if (node == null) {
			return false;
		} else if (charPosition < word.length()) {
			return isPresent(node.getNodes(), word, charPosition, type);
		}

		if (type.equals(WORD_SERACH_TYPE.PREFIX_WORD)) {
			return true;
		} else {
			return node.isComplete();
		}
	}

	//Time Complexity- O(N*256), N- number of nodes in TRIE
	public void print() {
		TrieNode[] array = nodes;
		int index = 0;
		QueueUsingSingleLinkedList<TrieNode> queue = new QueueUsingSingleLinkedList<>();
		System.out.println("Printing Trie...");
		while (index < array.length) {
			if (array[index] != null) {
				queue.add(array[index]);
			}
			index++;
		}
		while (!queue.isEmpty()) {
			TrieNode node = queue.remove();
			System.out.print(" " + node.getValue());
			array = node.getNodes();
			index = 0;
			while (index < array.length) {
				if (array[index] != null) {
					queue.add(array[index]);
				}
				index++;
			}

		}

		System.out.println();
	}

	public void printSuggestedWords(String word) {
		search(nodes, word, 0);
	}

	//Time Complexity- O(K+N), K-length of the word being searched, N-number of nodes
	private void search(TrieNode[] nodes, String word, int charPosition) {
		char ch = word.charAt(charPosition++);
		TrieNode node = nodes[ch];
		if (node == null) {
			System.out.println("Sorry! No results found...");
			return;
		} else if (charPosition < word.length()) {
			search(node.getNodes(), word, charPosition);
		} else {
			System.out.println("Printing Word Suggestion matching the prefix = " + word);
			printSuggestions(node, word);
		}
	}

	//Time Complexity- O(N*256), N- number of nodes in TRIE
	private void printSuggestions(TrieNode node, String prefix) {
		TrieNode[] array = node.getNodes();
		int index = 0;

		if (node.isComplete()) {
			System.out.print(prefix + " ");
		}
		QueueUsingSingleLinkedList<TrieWordNode> queue = new QueueUsingSingleLinkedList<>();
		while (index < array.length) {
			if (array[index] != null) {
				queue.add(new TrieWordNode(array[index], prefix));
			}
			index++;
		}
		while (!queue.isEmpty()) {
			TrieWordNode wordNode = queue.remove();
			if (wordNode.getNode().isComplete())
				System.out.print(wordNode.getBuilderValue().toString() + " ");
			array = wordNode.getNode().getNodes();
			index = 0;
			while (index < array.length) {
				if (array[index] != null) {
					queue.add(new TrieWordNode(array[index], wordNode.getBuilderValue().toString()));
				}
				index++;
			}

		}

		System.out.println();
	}

	private void addNode(TrieNode[] nodes, String word, int charPosition) {
		char ch = word.charAt(charPosition);
		TrieNode node = nodes[ch];
		if (node == null) {
			node = new TrieNode(ch);
			nodes[ch] = node;
		}
		charPosition++;
		if (charPosition == word.length()) {
			node.setComplete(true);
		} else {
			addNode(node.getNodes(), word, charPosition);
		}
	}

	public static void main(String[] args) {
		TrieUsingArray trie = new TrieUsingArray();
//		trie.add("ruchi");
//		trie.add("dharmendra");
//		trie.add("ruhi");
//		trie.add("dharmavaram");
//
//		trie.print();
//
//		System.out.println("searching for prefix");
//		System.out.println(trie.findPrefix("ru"));
//		System.out.println(trie.findPrefix("ruchin"));
//		System.out.println(trie.findPrefix("ruh"));
//		System.out.println(trie.findPrefix("dharmc"));
//
//		System.out.println("searching for complete word");
//		System.out.println(trie.findCompleteWord("ruchi"));
//		System.out.println(trie.findCompleteWord("rui"));
//
//		trie.printSuggestedWords("dharm");
		
		trie.add("Hi, I am Dharmendra");
		trie.add("Hi, I am Ruchi");
		
		trie.print();
		
		trie.printSuggestedWords("Hi");
	}
}

enum WORD_SERACH_TYPE {
	PREFIX_WORD, COMPLETE_WORD
}

class TrieWordNode {
	private TrieNode node;
	private StringBuilder builderValue;

	public TrieWordNode(TrieNode node, String prefix) {
		this.node = node;
		builderValue = new StringBuilder(prefix);
		builderValue.append(node.getValue());
	}

	public TrieNode getNode() {
		return node;
	}

	public void setNode(TrieNode node) {
		this.node = node;
	}

	public StringBuilder getBuilderValue() {
		return builderValue;
	}

	public void setBuilderValue(StringBuilder builderValue) {
		this.builderValue = builderValue;
	}

}
