package com.da.coding.tries;

import java.io.InputStream;
import java.util.Scanner;

public class TrieWordSuggestions {
	TrieUsingArray trie;

	public TrieWordSuggestions() {
		trie = new TrieUsingArray();
	}

	public void loadSentences(String fileName) {
		InputStream inputStream = TrieWordSuggestions.class.getClassLoader().getResourceAsStream(fileName);

		try (Scanner scn = new Scanner(inputStream)) {
			while (scn.hasNext()) {
				String line = scn.nextLine();
				if (!line.isEmpty())
					trie.add(line);
			}

		}
	}

	public void loadWords(String fileName) {
		InputStream inputStream = TrieWordSuggestions.class.getClassLoader().getResourceAsStream(fileName);

		try (Scanner scn = new Scanner(inputStream)) {
			while (scn.hasNext()) {
				trie.add(scn.next());
			}

		}
	}

	public void askSuggestions(String prefix) {
		trie.printSuggestedWords(prefix);
	}

	public static void main(String[] args) {
		TrieWordSuggestions suggestions = new TrieWordSuggestions();
		suggestions.loadSentences("words.txt");
		suggestions.askSuggestions("Manik Sarkar");
		suggestions.askSuggestions("\"I know");
		
		suggestions.loadWords("words.txt");
		suggestions.askSuggestions("si");
		suggestions.askSuggestions("wh");
		suggestions.askSuggestions("soci");

	}
}
