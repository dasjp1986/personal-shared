package com.da.coding.tries;

import com.da.coding.commons.Constants;

public class TrieNode {
	private char value;
	private boolean isComplete;
	private TrieNode[] nodes;

	public TrieNode(char ch) {
		this.value = ch;
		this.nodes = new TrieNode[Constants.ASCII_SIZE];
	}

	public char getValue() {
		return value;
	}

	public void setValue(char value) {
		this.value = value;
	}

	public TrieNode[] getNodes() {
		return nodes;
	}

	public void setNodes(TrieNode[] nodes) {
		this.nodes = nodes;
	}

	public boolean isComplete() {
		return isComplete;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}

}
