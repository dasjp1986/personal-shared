package com.da.coding.trees;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 * 
 * 
 *         A Cartesian tree is a tree data structure created from a set of data
 *         that obeys the following structural invariants: The tree obeys in the
 *         min (or max) heap property – each node is less (or greater) than its
 *         children. An inorder traversal of the nodes yields the values in the
 *         same order in which they appear in the initial sequence.
 * 
 *
 * 
 */

// Time Complexity- O(NLogN) after you use parent and last added node pointers
public class CartesianTreeMinHeap {
	private TreeNode root;

	public void add(int value) {
		TreeNode node = new TreeNode(value);
		if (root == null) {
			root = node;
		} else {
			Stack<TreeNode> stack = new Stack<>();
			TreeNode temp = root;
			while (temp != null) {
				stack.push(temp);
				temp = temp.right;
			}
			while (!stack.isEmpty()) {
				temp = stack.pop();
				if (temp.val < value) {
					break;
				}
			}
			if (temp.val > value) {
				node.left = temp;
				if (temp == root) {
					root = node;
				}
			} else {
				node.left = temp.right;
				temp.right = node;
			}

		}
	}

	public void inorder(TreeNode node) {
		if (node != null) {
			inorder(node.left);
			System.out.print(node.val + " ");
			inorder(node.right);
		}
	}

	public static void main(String[] args) {
		CartesianTreeMinHeap cartesian = new CartesianTreeMinHeap();
		// int[] array = { 9, 3, 7, 1, 8, 12, 10, 20, 15, 18, 5 };
		int[] array = { 5, 4, 3, 2, 1 };
		for (int i = 0; i < array.length; i++) {
			cartesian.add(array[i]);
			System.out.println("inorder");
			cartesian.inorder(cartesian.root);
			System.out.println();
		}

	}
}
