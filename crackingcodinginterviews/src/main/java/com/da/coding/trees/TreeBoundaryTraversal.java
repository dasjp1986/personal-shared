package com.da.coding.trees;

import ch.qos.logback.core.pattern.parser.Node;

/**
 * 
 * @author dagraw1
 *
 *         Given a binary tree, print boundary nodes of the binary tree
 *         Anti-Clockwise starting from the root. For example, boundary
 *         traversal of the following tree is 
 *         			 20
 *         			/  \
 *                 8   22
 *                / \    \
 *               4  12    25
 *                 / \
 *                10  14
 *         
 *         
 *         “20 8 4 10 14 25 22”
 * 
 * 
 */
public class TreeBoundaryTraversal {
	//Time Complexity- O(N)
	public void boundaryTraversal(TNode<Integer> root) {
		leftPrint(root);
		leavesPrint(root);
		rightPrint(root.getRight());
		
	}

	private void rightPrint(TNode<Integer> node) {
		if(node!=null) {
			if(node.getRight()!=null) {
				rightPrint(node.getRight());
				System.out.println(node.getValue());
			}else if(node.getLeft()!=null) {
				rightPrint(node.getLeft());
				System.out.println(node.getValue());
			}
		}
	}

	private void leavesPrint(TNode<Integer> node) {
		if(node!=null) {
			if(node.getLeft()==null && node.getRight()==null) {
				System.out.println(node.getValue());
			}
			leavesPrint(node.getLeft());
			leavesPrint(node.getRight());
		}
	}

	private void leftPrint(TNode<Integer> node) {
		if(node!=null) {
			if(node.getLeft()!=null) {
				System.out.println(node.getValue());
				leftPrint(node.getLeft());
			}else if(node.getRight()!=null) {
				System.out.println(node.getValue());
				leftPrint(node.getRight());
			}
		}
	}
	
	public static void main(String[] args) {
		TNode<Integer> root= new TNode<Integer>(1);
		root.setLeft(new TNode<Integer>(2));
		root.setRight(new TNode<Integer>(3));
		//root.getLeft().setLeft(new TNode<Integer>(4));
		root.getLeft().setRight(new TNode<Integer>(6));
		root.getLeft().getRight().setRight(new TNode<Integer>(15));
		root.getRight().setLeft(new TNode<Integer>(7));
		root.getRight().setRight(new TNode<Integer>(8));
		
		TreeBoundaryTraversal traversal= new TreeBoundaryTraversal();
		traversal.boundaryTraversal(root);
	}
}
