package com.da.coding.trees;

import com.da.coding.queues.QueueUsingSingleLinkedList;
import com.da.coding.stack.StackUsingLinkedList;

public abstract class AbstractBinaryTree<T> {
	protected TNode<T> root;

	public abstract TNode<T> add(T value);

	public abstract TNode<T> getLastNode();

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public void bfs() {
		if (root == null) {
			throw new RuntimeException("Tree is empty");
		}
		System.out.println("Printing Tree in Breadth First Traversal...");
		QueueUsingSingleLinkedList<TNode<T>> queue = new QueueUsingSingleLinkedList<>();
		queue.add(root);

		while (!queue.isEmpty()) {
			TNode<T> node = queue.remove();
			System.out.print(" " + node.getValue());
			if (node.getLeft() != null)
				queue.add(node.getLeft());

			if (node.getRight() != null)
				queue.add(node.getRight());
		}

		System.out.println();
	}

	// Time Complexity- O(N)
	// Space Complexity- O(LogN)
	public void dfsIteratively() {
		if (root == null) {
			throw new RuntimeException("Tree is empty");
		}

		System.out.println("Printing Tree in Depth First Traversal...");
		StackUsingLinkedList<TNode<T>> stack = new StackUsingLinkedList<>();
		stack.push(root);

		while (!stack.isEmpty()) {
			TNode<T> node = stack.pop();
			System.out.print(" " + node.getValue());
			if (node.getRight() != null) {
				stack.push(node.getRight());
			}

			if (node.getLeft() != null) {
				stack.push(node.getLeft());
			}
		}
		System.out.println();
	}

	public void dfsRecursively(TNode<T> node) {
		System.out.print(node.getValue() + " ");
		if (node.getLeft() != null) {
			dfsRecursively(node.getLeft());
		}

		if (node.getRight() != null) {
			dfsRecursively(node.getRight());
		}
	}

	// Time Complexity- O(N)
	// Space Complexity- O(1)
	public void preOrderRecursively(TNode<T> node) {
		if (node != null) {
			System.out.print(" " + node.getValue());
			preOrderRecursively(node.getLeft());
			preOrderRecursively(node.getRight());
		}
	}

	// Time Complexity- O(N)
	// Space Complexity- O(1)
	public void inOrderRecursively(TNode<T> node) {
		if (node != null) {
			inOrderRecursively(node.getLeft());
			System.out.print(" " + node.getValue());
			inOrderRecursively(node.getRight());
		}
	}

	// Time Complexity- O(N)
	// Space Complexity- O(1)
	public void postOrderRecursively(TNode<T> node) {
		if (node != null) {
			postOrderRecursively(node.getLeft());
			postOrderRecursively(node.getRight());
			System.out.print(" " + node.getValue());
		}
	}

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public void preOrderIteratively() {
		if (root == null) {
			throw new RuntimeException("Tree is empty...");
		}

		StackUsingLinkedList<TNode<T>> stack = new StackUsingLinkedList<>();
		stack.push(root);
		System.out.println("\n Printing tree in preOrder iteratively");
		while (!stack.isEmpty()) {
			TNode<T> node = stack.pop();
			System.out.print(" " + node.getValue());
			if (node.getRight() != null) {
				stack.push(node.getRight());
			}

			if (node.getLeft() != null) {
				stack.push(node.getLeft());
			}
		}
	}

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public void inOrderIteratively() {
		if (root == null) {
			throw new RuntimeException("Tree is empty...");
		}

		StackUsingLinkedList<TNode<T>> stack = new StackUsingLinkedList<>();
		System.out.println("\n Printing tree in inOrder iteratively");
		TNode<T> node = root;
		while (node != null) {
			stack.push(node);
			node = node.getLeft();

			while (node == null && !stack.isEmpty()) {
				node = stack.pop();
				System.out.print(" " + node.getValue());
				node = node.getRight();
			}
		}
		System.out.println();
	}

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public void postOrderIteratively() {
		if (root == null) {
			throw new RuntimeException("Tree is empty...");
		}

		StackUsingLinkedList<TNode<T>> stack = new StackUsingLinkedList<>();
		System.out.println("\n Printing tree in postOrder iteratively");
		TNode<T> node = root;
		while (node != null) {
			if (node.getRight() != null) {
				stack.push(node.getRight());
			}
			stack.push(node);
			node = node.getLeft();

			while (node == null && !stack.isEmpty()) {
				node = stack.pop();
				if (node.getRight() == null || stack.isEmpty()) {
					System.out.print(" " + node.getValue());
					node = null;
				} else {
					if (node.getRight().getValue().equals(stack.peek().getValue())) {
						stack.pop();
						stack.push(node);
						node = node.getRight();
					} else {
						System.out.print(" " + node.getValue());
						node = null;
					}
				}
			}
		}
	}

	public boolean isEmpty() {
		return root == null;
	}

	public TNode<T> getRoot() {
		return root;
	}

	public void setRoot(TNode<T> root) {
		this.root = root;
	}

	public boolean isBalanced() {
		if (root == null) {
			throw new RuntimeException("Tree is empty...");
		}

		if (Math.abs(getHeight(root.getLeft()) - getHeight(root.getRight())) > 1)
			return false;
		return true;
	}

	private int getHeight(TNode<T> node) {
		if (node == null)
			return -1;

		return Math.max(getHeight(node.getLeft()), getHeight(node.getRight())) + 1;
	}
	
	public abstract boolean remove(TNode<T> node);

}
