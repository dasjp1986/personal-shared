package com.da.coding.trees;

/**
 * 
 * @author dagraw1 Create a BST with minimal height when nodes are added sorted
 */
public class BinarySearchTreeMinimalHeight {

	private BinarySearchTree bst;

	public BinarySearchTreeMinimalHeight() {
		bst = new BinarySearchTree();
	}

	//Time Complexity- O(NLogN)
	public void addNodes(int[] nodeArray) {
		addNode(nodeArray, 0, nodeArray.length - 1);
	}

	private void addNode(int[] nodeArray, int start, int end) {
		if (start > end)
			return;
		int mid = (start + end) / 2;
		bst.add(nodeArray[mid]);

		addNode(nodeArray, start, mid - 1);
		addNode(nodeArray, mid + 1, end);

	}

	public void print() {
		bst.bfs();
		bst.inOrderIteratively();
	}
	
	public boolean isBalanced(){
		return bst.isBalanced();
	}

	public static void main(String[] args) {
		BinarySearchTreeMinimalHeight bstBalanced = new BinarySearchTreeMinimalHeight();
		int[] nodeArray1 = { 5, 8, 9, 10, 13, 17, 50 };
		bstBalanced.addNodes(nodeArray1);
		bstBalanced.print();

		bstBalanced = new BinarySearchTreeMinimalHeight();
		int[] nodeArray2 = { 50, 17, 13, 10, 9, 8, 5 };
		bstBalanced.addNodes(nodeArray2);
		bstBalanced.print();
		System.out.println("IsBalanced: "+bstBalanced.isBalanced());
	}
}
