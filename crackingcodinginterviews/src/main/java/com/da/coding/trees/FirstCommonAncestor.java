package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 * 
 *         Given a Binary Tree not necessarily a BST, find First common Ancestor
 *
 */
public class FirstCommonAncestor<T> {
	private AbstractBinaryTree<T> binTree;

	public FirstCommonAncestor(AbstractBinaryTree<T> binTree) {
		this.binTree = binTree;
	}

	// Time Complexity- O(height)->O(logN) , But worst case O(N) if tree is
	// skewed
	public T fcaByDepthWithParent(TNode<T> p, TNode<T> q) {
		int pd = getDepth(p);
		int qd = getDepth(q);

		if (pd == -1 || qd == -1) {
			return null;
		}

		if (pd > qd) {
			while (pd != qd) {
				p = p.getParent();
				pd--;
			}
		} else {
			while (pd != qd) {
				q = q.getParent();
				qd--;
			}
		}
		while (p != null && q != null && p != q) {
			p = p.getParent();
			q = q.getParent();
		}
		return p == q ? q.getValue() : null;
	}

	// Time Complexity- O(height)->O(logN) , But worst case O(N) if tree is
	// skewed
	public T fcaBySiblngWithParent(TNode<T> p, TNode<T> q) {
		if (p.getParent() == null && p != binTree.getRoot()) {
			return null;
		}

		if (q.getParent() == null && q != binTree.getRoot()) {
			return null;
		}

		if (p == binTree.getRoot() || q == binTree.getRoot()) {
			return binTree.getRoot().getValue();
		}

		if (p == q || p == q.getParent()) {
			return p.getValue();
		}

		if (q == p.getParent()) {
			return q.getValue();
		}

		TNode<T> parent = p.getParent();
		TNode<T> sibling = parent.getLeft() == p ? parent.getRight() : parent.getLeft();
		if (find(sibling, q)) {
			return parent.getValue();
		} else {
			return fcaBySiblngWithParent(parent, q);
		}
	}

	// Time Complexity- O(height)->O(logN) , But worst case O(N) if tree is
	// skewed
	public T fcaWithoutParent(TNode<T> p, TNode<T> q) {
		if (!find(binTree.getRoot(), p) || !find(binTree.getRoot(), q)) {
			return null;
		}
		return fcaHelper(binTree.getRoot(), p, q);
	}

	private T fcaHelper(TNode<T> node, TNode<T> p, TNode<T> q) {
		if (node == null) {
			return null;
		}
		if (node == p || node == q) {
			return node.getValue();
		}
		boolean isPLeft = find(node.getLeft(), p);
		boolean isQLeft = find(node.getLeft(), q);

		if (isPLeft != isQLeft) {
			return node.getValue();
		}
		if (isPLeft) {
			return fcaHelper(node.getLeft(), p, q);
		} else {
			return fcaHelper(node.getRight(), p, q);
		}
	}

	private boolean find(TNode<T> root, TNode<T> node) {
		if (root == null) {
			return false;
		}
		if (root == node) {
			return true;
		}
		return find(root.getLeft(), node) || find(root.getRight(), node);
	}

	private int getDepth(TNode<T> node) {
		if (node == binTree.getRoot()) {
			return 0;
		}
		if (node.getParent() == null) {
			return -1;
		}
		int d = -1;
		while (node != null) {
			node = node.getParent();
			d++;
		}
		return d;
	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> binTree = new CompleteBinaryTree<>();
		for (int i = 1; i <= 15; i++) {
			binTree.add(i);
		}
		binTree.preOrderIteratively();
		FirstCommonAncestor<Integer> fca = new FirstCommonAncestor<>(binTree);
		TNode<Integer> p = binTree.getRoot();
		TNode<Integer> q = binTree.getRoot();
		System.out.println();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		q = q.getRight();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		q = q.getRight();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		p = p.getLeft();
		q = q.getRight();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		q = p.getRight().getLeft();
		p = p.getLeft().getLeft();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		p = q;
		p = binTree.getLastNode();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		p = new TNode<Integer>(100);
		q = binTree.getRoot();
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));

		p = binTree.getRoot();
		q = p;
		System.out.println("Finding FCA between p, q " + p.getValue() + " , " + q.getValue());
		System.out.println(fca.fcaByDepthWithParent(p, q));
		System.out.println(fca.fcaBySiblngWithParent(p, q));
		System.out.println(fca.fcaWithoutParent(p, q));
	}
}
