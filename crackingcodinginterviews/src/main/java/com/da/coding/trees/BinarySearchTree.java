package com.da.coding.trees;

/**
 * 
 * @author dagraw1 This is the basic implementation of BST where all the left
 *         children are less than the parent node and all the right nodes are
 *         greater than or equal to the parent node
 * 
 *         BST night get skewed if inputs are sorted in ascending or descending
 *         order
 */
public class BinarySearchTree extends AbstractBinaryTree<Integer> {

	// Time Complexity- O(LogN)
	@Override
	public TNode<Integer> add(Integer value) {
		TNode<Integer> node = new TNode<>(value);
		if (root == null) {
			root = node;
		} else {
			addChild(node, root);
		}
		return node;
	}

	private void addChild(TNode<Integer> node, TNode<Integer> parent) {
		if (node.getValue().intValue() <= parent.getValue().intValue()) {
			if (parent.getLeft() == null) {
				parent.setLeft(node);
				node.setParent(parent);
			} else {
				addChild(node, parent.getLeft());
			}
		} else {
			if (parent.getRight() == null) {
				parent.setRight(node);
				node.setParent(parent);
			} else {
				addChild(node, parent.getRight());
			}
		}
		parent.setSize(parent.getSize() + 1);
	}

	@Override
	public TNode<Integer> getLastNode() {
		return null;
	}

	@Override
	public boolean remove(TNode<Integer> node) {
		if (isEmpty() || node == null) {
			return false;
		}

		// leaf nodes
		if (node.getLeft() == null && node.getRight() == null) {
			deleteNode(node, null);
		} else if (node.getLeft() == null) {
			deleteNode(node, node.getRight());
		} else if (node.getRight() == null) {
			deleteNode(node, node.getLeft());
		} else {
			TNode<Integer> temp = null;
			if (node == root) {
				temp = node.getLeft();
				while (temp.getRight() != null) {
					temp = temp.getRight();
				}
			} else {
				temp = node.getLeft();
			}
			swap(node, temp);
			remove(temp);

		}
		return true;
	}

	private void swap(TNode<Integer> node1, TNode<Integer> node2) {
		if (node1 != null && node2 != null) {
			Integer temp = node1.getValue();
			node1.setValue(node2.getValue());
			node2.setValue(temp);
		}
	}

	private void deleteNode(TNode<Integer> node, TNode<Integer> newValue) {
		if (node == root) {
			root = newValue;
		} else if (node.getParent().getLeft() == node) {
			node.getParent().setLeft(newValue);
		} else {
			node.getParent().setRight(newValue);
		}
		if (newValue != null)
			newValue.setParent(node);
	}

	public static void main(String[] args) {
		// AbstractBinaryTree<Integer> bst = new BinarySearchTree();
		// bst.add(50);
		// bst.add(17);
		// bst.add(13);
		// bst.add(10);
		// bst.add(9);
		// bst.add(8);
		// bst.add(1);
		// bst.add(5);
		//
		// bst.inOrderIteratively();
		// bst.bfs();
		// bst.dfsRecursively(bst.getRoot());
		// System.out.println(bst.isBalanced());
		//
		// TNode<Integer> node= bst.getRoot().getLeft().getLeft();
		// System.out.println("removing..."+node.getValue()+"
		// "+bst.remove(node));
		// bst.preOrderIteratively();
		//
		AbstractBinaryTree<Integer> bst2 = new BinarySearchTree();
		bst2.add(10);
		bst2.add(5);
		bst2.add(15);
		bst2.add(4);
		bst2.add(8);
		bst2.add(11);
		bst2.add(20);
		bst2.add(7);
		bst2.add(9);

		TNode<Integer> temp = bst2.getRoot().getLeft().getLeft();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();

		temp = bst2.getRoot().getLeft().getRight();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();

		temp = bst2.getRoot();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();
		System.out.println("new root now..." + bst2.getRoot().getValue());

		temp = temp.getRight();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();

		temp = bst2.getRoot();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();
		System.out.println("new root now..." + bst2.getRoot().getValue());

		temp = temp.getRight();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();

		temp = bst2.getRoot();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();
		System.out.println("new root now..." + bst2.getRoot().getValue());

		temp = bst2.getRoot();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		bst2.inOrderIteratively();
		System.out.println("new root now..." + bst2.getRoot().getValue());

		temp = bst2.getRoot();
		System.out.println("removing..." + temp.getValue() + " " + bst2.remove(temp));
		System.out.println("new root now..." + bst2.getRoot());

	}

}
