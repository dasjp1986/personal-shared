package com.da.coding.trees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeLevelTraversel {

	public int[][] levelOrder(TreeNode A) {
		List<Integer[]> result = new ArrayList<>();

		Queue<TreeNode> first = new LinkedList<>();
		Queue<TreeNode> second = new LinkedList<>();

		first.add(A);

		TreeNode node = null;
		while (!first.isEmpty() || !second.isEmpty()) {
			List<Integer> list = new ArrayList<>();
			while (!first.isEmpty()) {
				node = first.remove();
				list.add(node.val);
				if (node.left != null) {
					second.add(node.left);
				}
				if (node.right != null) {
					second.add(node.right);
				}
			}
			if (!list.isEmpty())
				result.add(list.toArray(new Integer[list.size()]));

			list = new ArrayList<>();
			while (!second.isEmpty()) {
				node = second.remove();
				list.add(node.val);
				if (node.left != null) {
					first.add(node.left);
				}
				if (node.right != null) {
					first.add(node.right);
				}
			}

			if (!list.isEmpty())
				result.add(list.toArray(new Integer[list.size()]));
		}

		int[][] res = new int[result.size()][];
		int i = 0;
		for (Integer[] arr : result) {
			res[i] = new int[arr.length];
			for (int j = 0; j < arr.length; j++) {
				res[i][j] = arr[j];
			}

			i++;
		}

		return res;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);

		TreeLevelTraversel level = new TreeLevelTraversel();
		System.out.println(Arrays.toString(level.levelOrder(root)));
	}

}
