package com.da.coding.trees;

import java.util.ArrayList;
import java.util.Stack;

public class RecoverBST {

	public ArrayList<Integer> recoverTree(TreeNode A) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		TreeNode node = A;
		Stack<TreeNode> stack = new Stack<>();

		Integer min = null;
		Integer max = null;
		Integer last = null;

		while (node != null) {
			stack.push(node);
			node = node.left;

			while (node == null && !stack.isEmpty()) {
				node = stack.pop();
				if (last != null && node.val < last) {
					if(min==null) {
						min= node.val;
					}else if (node.val < min) {
						min = node.val;
					}
					if(max==null) {
						max = last;
					}else if (node.val > max) {
						max = node.val;
					}
				}
				last = node.val;
				node = node.right;
			}
		}

		if (min != max) {
			res.add(min);
			res.add(max);
		}

		return res;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(8);
		//root.right = new TreeNode(15);
		root.left.left = new TreeNode(7);
		root.left.right = new TreeNode(12);
		//root.right.left = new TreeNode(4);
		//root.right.right = new TreeNode(30);

		RecoverBST recoverBST = new RecoverBST();
		System.out.println(recoverBST.recoverTree(root));
	}
}
