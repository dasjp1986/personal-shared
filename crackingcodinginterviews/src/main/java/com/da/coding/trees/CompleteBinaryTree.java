package com.da.coding.trees;

import com.da.coding.queues.QueueUsingSingleLinkedList;

public class CompleteBinaryTree<T> extends AbstractBinaryTree<T> {

	@Override
	public TNode<T> add(T value) {
		TNode<T> node = new TNode<>(value);
		if (root == null) {
			root = node;
		} else {
			QueueUsingSingleLinkedList<TNode<T>> queue = new QueueUsingSingleLinkedList<>();
			queue.add(root);
			while (!queue.isEmpty()) {
				TNode<T> temp = queue.remove();

				if (temp.getLeft() == null) {
					temp.setLeft(node);
					node.setParent(temp);
					break;
				} else {
					queue.add(temp.getLeft());
				}

				if (temp.getRight() == null) {
					temp.setRight(node);
					node.setParent(temp);
					break;
				} else {
					queue.add(temp.getRight());
				}
			}
		}
		return node;
	}

	public static void main(String[] args) {
		CompleteBinaryTree<Integer> tree = new CompleteBinaryTree<>();

		for (int i = 1; i <= 20; i++) {
			tree.add(i);
		}

		tree.bfs();
		tree.dfsIteratively();
		System.out.println("Printing DFS recusively...");
		tree.dfsRecursively(tree.getRoot());
		System.out.println("\n Printing Pre-Order Traversal recursively");
		tree.preOrderRecursively(tree.root);
		tree.preOrderIteratively();

		System.out.println("\n Printing In-Order Traversal recursively");
		tree.inOrderRecursively(tree.root);
		tree.inOrderIteratively();

		System.out.println("\n Printing Post-Order Traversal recursively");
		tree.postOrderRecursively(tree.root);
		tree.postOrderIteratively();
		
		System.out.println("Is Balanced : "+tree.isBalanced());
	}

	@Override
	public TNode<T> getLastNode() {
		if (isEmpty()) {
			throw new RuntimeException("Tree is empty");
		}

		TNode<T> node = root;
		TNode<T> lastNode = null;
		while (node != null) {
			lastNode = node;
			if (node.getRight() == null) {
				node = node.getLeft();
			} else {
				node = node.getRight();
			}
		}
		return lastNode;
	}

	@Override
	public boolean remove(TNode<T> node) {
		// TODO Auto-generated method stub
		return false;
	}
}
