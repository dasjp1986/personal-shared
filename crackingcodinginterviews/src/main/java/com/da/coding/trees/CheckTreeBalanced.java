package com.da.coding.trees;

public class CheckTreeBalanced {
	
	//Time and Space Complexity- O(n)
	public int isBalancedBin(TNode<Integer> node, Balanced balanced) {
		if (node == null) {
			return -1;
		}
		int left = isBalancedBin(node.getLeft(), balanced);
		int right = isBalancedBin(node.getRight(), balanced);

		if (Math.abs(left - right) > 1) {
			balanced.isBalanced = false;
		}
		return Math.max(left, right) + 1;
	}
	
	public static void main(String[] args) {
		CheckTreeBalanced checkTreeBalanced= new CheckTreeBalanced();
		TNode<Integer> root= new TNode<Integer>(1);
		root.setLeft(new TNode<Integer>(2));
		root.setRight(new TNode<Integer>(3));
		root.getRight().setRight(new TNode<Integer>(4));
		root.getRight().getRight().setLeft(new TNode<Integer>(5));
		
		Balanced result= new Balanced();
		checkTreeBalanced.isBalancedBin(root, result);
		System.out.println(result.isBalanced);
	}
}

class Balanced {
	boolean isBalanced = true;
}
