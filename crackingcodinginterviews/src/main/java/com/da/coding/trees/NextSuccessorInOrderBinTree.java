package com.da.coding.trees;

import com.da.coding.stack.StackUsingLinkedList;

/**
 * 
 * @author dagraw1
 * 
 *         Find the next successor(node next in the in order traversal) of the
 *         given node value
 */
public class NextSuccessorInOrderBinTree<T> {

	private AbstractBinaryTree<T> binTree;

	public NextSuccessorInOrderBinTree(AbstractBinaryTree<T> binTree) {
		this.binTree = binTree;
	}

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public T getSuccessorWithoutParent(T value) {

		StackUsingLinkedList<TNode<T>> stack = new StackUsingLinkedList<>();
		TNode<T> node = binTree.getRoot();
		boolean isFound = false;

		while (node != null) {
			stack.push(node);
			node = node.getLeft();

			while (node == null && !stack.isEmpty()) {
				node = stack.pop();
				if (isFound) {
					return node.getValue();
				}
				if (node.getValue().equals(value)) {
					isFound = true;
				}
				node = node.getRight();
			}
		}

		return null;
	}

	// Time Complexity- O(LogN)
	// Space Complexity- O(1)
	public TNode<T> getSuccessorWithParent(TNode<T> node) {
		TNode<T> succ = node.getRight();

		if (succ == null) {
			while (node.getParent() != null && node.getParent().getRight() == node) {
				node = node.getParent();
			}
			succ = node.getParent();
		} else {
			while (succ.getLeft() != null) {
				succ = succ.getLeft();
			}
		}

		return succ;
	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> tree = new CompleteBinaryTree<>();
		for (int i = 1; i <= 21; i++) {
			tree.add(i);
		}

		NextSuccessorInOrderBinTree<Integer> successor = new NextSuccessorInOrderBinTree<>(tree);
		successor.binTree.inOrderIteratively();
		System.out.println(successor.getSuccessorWithoutParent(3));
		TNode<Integer> root = successor.binTree.getRoot();
		System.out.println("Node next to root - " + root.getValue() + " : " + successor.getSuccessorWithParent(root));

		TNode<Integer> node = root.getRight();
		System.out.println("Node next to root - " + node.getValue() + " : " + successor.getSuccessorWithParent(node));

		node = root;
		while (node.getLeft() != null) {
			node = node.getLeft();
		}

		System.out.println("Node next to root - " + node.getValue() + " : " + successor.getSuccessorWithParent(node));

		node = node.getParent().getRight();
		System.out.println("Node next to root - " + node.getValue() + " : " + successor.getSuccessorWithParent(node));

		node = root.getRight();
		while (node.getRight() != null) {
			node = node.getRight();
		}
		System.out.println("Node next to root - " + node.getValue() + " : " + successor.getSuccessorWithParent(node));

	}

	public TNode<Integer> findSuccessorOptimized(TNode<Integer> root, Integer data) {

		TNode<Integer> node = root;
		TNode<Integer> res = null;

		while (node != null) {

			if (data < node.getValue()) {
				res = node;
				node = node.getLeft();
			} else {
				node = node.getRight();
			}
		}

		return res;

	}

}
