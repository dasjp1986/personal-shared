package com.da.coding.trees;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Serialization is to store tree in a file so that it can be later
 *         restored. The structure of tree must be maintained. Deserialization
 *         is reading tree back from file.
 */
public class SerializeDeserializeBinTreePreOrder {
	private static int index = 0;

	public List<Integer> serialize(TNode<Integer> root) {
		List<Integer> nodes = new ArrayList<>();
		ser(root, nodes);
		return nodes;
	}

	private void ser(TNode<Integer> node, List<Integer> nodes) {
		if (node == null) {
			nodes.add(-1);
			return;
		}
		nodes.add(node.getValue());
		ser(node.getLeft(), nodes);
		ser(node.getRight(), nodes);
	}

	public TNode<Integer> deserialize(List<Integer> nodes) {
		index = 0;
		return des(nodes);
	}

	private TNode<Integer> des(List<Integer> nodes) {
		if (index == nodes.size() || nodes.get(index) == -1) {
			return null;
		}

		TNode<Integer> tNode = new TNode<Integer>(nodes.get(index));
		++index;
		TNode<Integer> left = des(nodes);
		if (left != null) {
			tNode.setLeft(left);
		}
		++index;
		TNode<Integer> right = des(nodes);
		if (right != null) {
			tNode.setRight(right);
		}

		return tNode;
	}

	public void inorder(TNode<Integer> node) {
		if (node == null) {
			return;
		}

		inorder(node.getLeft());
		System.out.print(node.getValue() + "->>");
		inorder(node.getRight());
	}

	public static void main(String[] args) {
		TNode<Integer> root = new TNode<Integer>(20);
		root.setLeft(new TNode<Integer>(8));
		root.setRight(new TNode<Integer>(10));
		root.getLeft().setRight(new TNode<Integer>(5));
		root.getRight().setLeft(new TNode<Integer>(4));
		root.getLeft().getRight().setLeft(new TNode<Integer>(1));

		SerializeDeserializeBinTreePreOrder tree = new SerializeDeserializeBinTreePreOrder();

		System.out.println("Original-preorder");
		tree.inorder(root);
		List<Integer> nodes = tree.serialize(root);
		System.out.println("Serialized");
		System.out.println(nodes);
		TNode<Integer> node = tree.deserialize(nodes);
		System.out.println("Deserialized");
		tree.inorder(node);

	}
}
