package com.da.coding.trees;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Given a binary tree with positive and negative node values and a
 *         target sum. Find count of paths which can lead to target sum, path
 *         does not necessarily start with root or end with leaf node but
 *         direction of path traversal must only be downwards
 */
public class TreePathsWithSum {

	public int getPathsCountRecurse(TNode<Integer> root, int targetSum) {
		Map<Integer, Boolean> rSumMap = new HashMap<>();
		return getCountsDFS(root, rSumMap, 0, targetSum);
	}

	private int getCountsDFS(TNode<Integer> node, Map<Integer, Boolean> rSumMap, int rSum, int targetSum) {
		int count = 0;
		int diff = 0;
		if (node == null) {
			rSumMap.remove(rSum);
			return count;
		} else {
			rSum = rSum + node.getValue();
			diff = rSum - targetSum;
			if (diff == 0 || rSumMap.get(diff) != null) {
				count++;
			}
			rSumMap.put(rSum, true);
			count = count + getCountsDFS(node.getLeft(), rSumMap, rSum, targetSum);
			count = count + getCountsDFS(node.getRight(), rSumMap, rSum, targetSum);
		}

		return count;
	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> binaryTree = new CompleteBinaryTree<>();
//		binaryTree.add(1);
//		binaryTree.getRoot().setLeft(new TNode<Integer>(4));
//		binaryTree.getRoot().getLeft().setLeft(new TNode<Integer>(2));
//		binaryTree.getRoot().getLeft().getLeft().setLeft(new TNode<Integer>(2));
//		binaryTree.getRoot().getLeft().setRight(new TNode<Integer>(-3));
//		binaryTree.getRoot().getLeft().getRight().setRight(new TNode<Integer>(2));
		
		binaryTree.add(1);
		binaryTree.getRoot().setLeft(new TNode<Integer>(2));
		binaryTree.getRoot().setRight(new TNode<Integer>(4));
		binaryTree.getRoot().getLeft().setLeft(new TNode<Integer>(3));
		binaryTree.getRoot().getLeft().setRight(new TNode<Integer>(6));
		binaryTree.getRoot().getRight().setLeft(new TNode<Integer>(7));
		binaryTree.getRoot().getRight().setRight(new TNode<Integer>(5));

		TreePathsWithSum paths = new TreePathsWithSum();
		binaryTree.bfs();
		System.out.println(paths.getPathsCountRecurse(binaryTree.getRoot(), 17));

	}
}
