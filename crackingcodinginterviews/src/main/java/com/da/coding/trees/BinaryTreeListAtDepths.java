package com.da.coding.trees;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1 Given a Binary Tree, create linked list at every depth so if
 *         depth is d then there has to be d linked lists
 */
public class BinaryTreeListAtDepths<T> {
	private AbstractBinaryTree<T> binaryTree;

	public BinaryTreeListAtDepths(AbstractBinaryTree<T> binaryTree) {
		this.binaryTree = binaryTree;
	}

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public List<List<TNode<T>>> getLists() {
		List<List<TNode<T>>> lists = new ArrayList<>();

		List<TNode<T>> list = new ArrayList<>();
		if (binaryTree.getRoot() != null) {
			list.add(binaryTree.getRoot());
		}

		while (!list.isEmpty()) {
			lists.add(list);
			List<TNode<T>> parents = list;
			list = new ArrayList<>();

			for (TNode<T> parent : parents) {
				if (parent.getLeft() != null) {
					list.add(parent.getLeft());
				}

				if (parent.getRight() != null) {
					list.add(parent.getRight());
				}
			}

		}
		return lists;

	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> binaryTree = new CompleteBinaryTree<>();
		for (int i = 1; i <= 15; i++) {
			binaryTree.add(i);
		}
		BinaryTreeListAtDepths<Integer> bstDepth = new BinaryTreeListAtDepths<>(binaryTree);
		List<List<TNode<Integer>>> lists = bstDepth.getLists();
		for (List<TNode<Integer>> list : lists) {
			System.out.println(list);
		}
	}
}
