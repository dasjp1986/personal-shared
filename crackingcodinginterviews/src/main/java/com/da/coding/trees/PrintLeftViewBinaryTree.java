package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 * 
 *         Given a Binary Tree, print left view of it. Left view of a Binary
 *         Tree is set of nodes visible when tree is visited from left side.
 */
public class PrintLeftViewBinaryTree {
	private static int maxLevel = 0;

	public static void main(String[] args) {
		TNode<Integer> root = new TNode<Integer>(1);
		TNode<Integer> left11 = new TNode<Integer>(11);
		TNode<Integer> right12 = new TNode<Integer>(12);
		root.setLeft(left11);
		root.setRight(right12);
		TNode<Integer> left21 = new TNode<Integer>(21);
		left11.setLeft(left21);
		TNode<Integer> right22 = new TNode<Integer>(22);
		right12.setLeft(right22);
		TNode<Integer> right32 = new TNode<Integer>(32);
		right22.setLeft(right32);

		PrintLeftViewBinaryTree leftViewBinaryTree = new PrintLeftViewBinaryTree();
		leftViewBinaryTree.leftView(root);
	}

	//Time Complexity- O(n)
	public void leftView(TNode<Integer> root) {
		printLeftView(root, 1);
	}

	private void printLeftView(TNode<Integer> node, int level) {
		if (node == null) {
			return;
		}

		if (maxLevel < level) {
			System.out.println(node.getValue());
			maxLevel = level;
		}
		printLeftView(node.getLeft(), level + 1);
		printLeftView(node.getRight(), level + 1);
	}

}
