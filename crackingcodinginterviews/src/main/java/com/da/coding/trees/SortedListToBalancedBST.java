package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 *
 *Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

 A height balanced BST : a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1. 
Example :


Given A : 1 -> 2 -> 3
A height balanced BST  :

      2
    /   \
   1     3
 */
public class SortedListToBalancedBST {

	public TreeNode sortedListToBST(ListNode a) {
		ListNode end = a;
		while (end.next != null) {
			end = end.next;
		}
		if (a == end) {
			return new TreeNode(a.val);
		}
		return build(a, end);
	}

	TreeNode build(ListNode start, ListNode end) {
		if (start==null || end==null) {
			return null;
		}
		
		if(start==end) {
			return new TreeNode(start.val);
		}

		ListNode prev= null;
		ListNode slow = start;
		ListNode fast = start;
		if (fast != end) {
			fast = fast.next;
		}

		while (fast != end) {
			prev= slow;
			slow = slow.next;
			fast = fast.next;
			if (fast != end) {
				fast = fast.next;
			}
		}

		TreeNode node = new TreeNode(slow.val);
		TreeNode left = build(start, prev);
		if (left != null) {
			node.left = left;
		}

		TreeNode right = build(slow.next, end);
		if (right != null) {
			node.right = right;
		}

		return node;
	}
	
	public static void main(String[] args) {
		SortedListToBalancedBST problem= new SortedListToBalancedBST();
		ListNode node= new ListNode(1);
		node.next= new ListNode(2);
		node.next.next= new ListNode(3);
		node.next.next.next= new ListNode(4);
		node.next.next.next.next= new ListNode(5);
		node.next.next.next.next.next= new ListNode(6);
		node.next.next.next.next.next.next= new ListNode(7);
		
		
		TreeNode root = problem.sortedListToBST(node);
		System.out.println(root);
	}

}

class ListNode {
	public int val;
	public ListNode next;

	ListNode(int x) {
		val = x;
		next = null;
	}

	@Override
	public String toString() {
		return "ListNode [val=" + val + "]";
	}
	
	
}
