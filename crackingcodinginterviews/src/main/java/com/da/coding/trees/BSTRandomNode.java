package com.da.coding.trees;

import java.util.Random;

/**
 * 
 * @author dagraw1
 * 
 *         Construct a Binary Search Tree with a function a return a Random node
 *         with equal probability along with insert and remove node functions.
 */
public class BSTRandomNode<T> {

	// Time Complexity- O(height)~ O(LogN)
	public TNode<T> getRandomNode(TNode<T> root) {
		Random random = new Random();
		int num = random.nextInt(root.getSize());
		System.out.println("random number : " + num);
		return getNode(root, num);
	}

	private TNode<T> getNode(TNode<T> node, int num) {
		if (node != null) {
			int leftSize = node.getLeft() == null ? 0 : node.getLeft().getSize();
			if (num < leftSize) {
				return getNode(node.getLeft(), num);
			} else if (num == leftSize) {
				return node;
			} else {
				return getNode(node.getRight(), num - (leftSize + 1));
			}
		}
		return null;
	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> bst = new BinarySearchTree();
		int[] nodeArray = { 10, 8, 4, 9, 15, 13, 18 };

		BSTRandomNode<Integer> bstRandom = new BSTRandomNode<>();
		for (int i = 0; i < nodeArray.length; i++) {
			bst.add(nodeArray[i]);
		}

		for (int i = 0; i < nodeArray.length; i++) {
			System.out.println(bstRandom.getRandomNode(bst.getRoot()));
		}
	}
}
