package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 *
Given a binary tree, check whether it is a mirror of itself.

For example, this binary tree is symmetric:

     1
   /   \
  2     2
 / \   / \
3   4 4   3
But the following is not:

    1
   / \
  2   2
   \   \
   3    3
 */
public class CheckMirrorTree {
	//Time and Space Complexity- O(n)
	public static boolean isMirror(TNode<Integer> node1, TNode<Integer> node2) {
		if (node1 == null && node2 == null) {
			return true;
		}

		if (node1 != null && node2 != null && node1.getValue().equals(node2.getValue())) {
			return isMirror(node1.getLeft(), node2.getRight()) && isMirror(node1.getRight(), node2.getLeft());
		} else {
			return false;
		}
	}
	
	public static void main(String[] args){
		CompleteBinaryTree<Integer> cbt= new CompleteBinaryTree<>();
		cbt.add(1);
		cbt.add(2);
		cbt.add(2);
		cbt.add(3);
		cbt.add(4);
		cbt.add(4);
		cbt.add(3);
		
		System.out.println(isMirror(cbt.getRoot(), cbt.getRoot()));
	}
}
