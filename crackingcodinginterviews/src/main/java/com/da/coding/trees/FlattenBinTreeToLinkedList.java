package com.da.coding.trees;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *         Given a binary tree, flatten it to a linked list in-place.
 * 
 *         Example : Given
 * 
 * 
 *         1 / \ 2 5 / \ \ 3 4 6 The flattened tree should look like:
 * 
 *         1 \ 2 \ 3 \ 4 \ 5 \ 6 Note that the left child of all nodes should be
 *         NULL.
 */
public class FlattenBinTreeToLinkedList {

	public TreeNode flatten(TreeNode a) {

        Stack<TreeNode> stack = new Stack<>();
        TreeNode node= null;
        TreeNode prev= null;
        stack.push(a);
        
        while(!stack.isEmpty()){
            node= stack.pop();
            
            if(prev==null){
                prev= node;
            }else{
                prev.right= node;
                prev.left= null;
                prev= prev.right;
            }
            
            if(node.right!=null){
                stack.push(node.right);
            }
            
             if(node.left!=null){
                stack.push(node.left);
            }
        }
        
        
        return a;
    
	}

	public static void main(String[] args) {
		FlattenBinTreeToLinkedList flattenBinTreeToLinkedList = new FlattenBinTreeToLinkedList();
		TreeNode root = new TreeNode(1);
		// root.left = new TreeNode(2);
		// root.right = new TreeNode(3);
		// root.left.left = new TreeNode(4);
		// root.left.right = new TreeNode(5);
		// root.right.left = new TreeNode(6);
		// root.right.right = new TreeNode(7);
		//
		// flattenBinTreeToLinkedList.flatten(root);
		//
		// TreeNode node = root;
		// System.out.println("Flattened");
		// while (node != null) {
		// System.out.print(node.val + " ");
		// node = node.right;
		// }

		SerializeDeserializeBinTreeBFS bfs = new SerializeDeserializeBinTreeBFS();
		root = bfs.deserialize(
				"47 42 52 41 44 50 64 40 -1 43 45 49 51 63 77 -1 -1 -1 -1 -1 46 48 -1 -1 -1 55 -1 75 88 -1 -1 -1 -1 53 58 69 76 81 94 -1 54 56 60 68 73 -1 -1 79 87 92 100 -1 -1 -1 57 59 61 66 -1 72 74 78 80 85 -1 89 93 96 102 -1 -1 -1 -1 -1 62 65 67 71 -1 -1 -1 -1 -1 -1 -1 84 86 -1 90 -1 -1 95 99 101 -1 -1 -1 -1 -1 -1 -1 70 -1 83 -1 -1 -1 -1 91 -1 -1 98 -1 -1 -1 -1 -1 82 -1 -1 -1 97 -1 -1 -1 -1 -1");
		flattenBinTreeToLinkedList.flatten(root);
	}

}
