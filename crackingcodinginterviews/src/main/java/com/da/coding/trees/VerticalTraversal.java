package com.da.coding.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class VerticalTraversal {

	public ArrayList<ArrayList<Integer>> verticalOrderTraversal(TreeNode A) {
		Map<Integer, ArrayList<Integer>> map = new TreeMap<>();
		traverse(A, map, 0);
		return new ArrayList<ArrayList<Integer>>(map.values());
	}

	private void traverse(TreeNode node, Map<Integer, ArrayList<Integer>> map, int d) {
		Queue<VNode> q = new LinkedList<>();
		q.add(new VNode(node, d));

		VNode temp;
		while (!q.isEmpty()) {
			temp = q.remove();
			ArrayList<Integer> list = map.get(temp.distance);
			if (list == null) {
				list = new ArrayList<Integer>();
				map.put(temp.distance, list);
			}
			list.add(temp.node.val);
			if (temp.node.left != null) {
				q.add(new VNode(temp.node.left, temp.distance - 1));
			}
			if (temp.node.right != null) {
				q.add(new VNode(temp.node.right, temp.distance + 1));
			}
		}

	}
	
	public static void main(String[] args) {
		TreeNode node= new TreeNode(1);
		node.left= new TreeNode(2);
		node.right= new TreeNode(3);
		node.left.right= new TreeNode(4);
		node.right.left= new TreeNode(5);
		node.left.right.left= new TreeNode(6);
		node.right.left.right= new TreeNode(9);
		node.left.right.left.left= new TreeNode(7);
		node.left.right.left.right= new TreeNode(8);
		node.left.right.left.right.right= new TreeNode(11);
		node.right.left.right.left= new TreeNode(10);
		
		VerticalTraversal traversal= new VerticalTraversal();
		System.out.println(traversal.verticalOrderTraversal(node));
	}

}

class VNode {
	TreeNode node;
	int distance;

	public VNode(TreeNode node, int dis) {
		this.node = node;
		this.distance = dis;
	}
}
