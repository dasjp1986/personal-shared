package com.da.coding.trees;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *
 *
 *         Given a binary search tree T, where each node contains a positive
 *         integer, and an integer K, you have to find whether or not there
 *         exist two different nodes A and B such that A.value + B.value = K.
 * 
 *         Return 1 to denote that two such nodes exist. Return 0, otherwise.
 * 
 *         Notes
 * 
 *         Your solution should run in linear time and not take memory more than
 *         O(height of T). Assume all values in BST are distinct. Example :
 * 
 *         Input 1:
 * 
 *         T : 10 / \ 9 20
 * 
 *         K = 19
 * 
 *         Return: 1
 * 
 *         Input 2:
 * 
 *         T: 10 / \ 9 20
 * 
 *         K = 40
 * 
 *         Return: 0
 * 
 */
public class TwoSumBST {

	public int t2Sum(TreeNode A, int B) {
		Stack<TreeNode> left = new Stack<>();
		Stack<TreeNode> right = new Stack<>();

		TreeNode node = A;
		while (node != null) {
			left.push(node);
			node = node.left;
		}

		node = A;
		while (node != null) {
			right.push(node);
			node = node.right;
		}

		while (!left.isEmpty() && !right.isEmpty()) {
			if (left.peek().val + right.peek().val == B && left.peek().val != right.peek().val) {
				return 1;
			} else if (left.peek().val + right.peek().val < B) {
				node = left.pop();
				node = node.right;
				while (node != null) {
					left.push(node);
					node = node.left;
				}
			} else {
				node = right.pop();
				node = node.left;
				while (node != null) {
					right.push(node);
					node = node.right;
				}
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(11);
		root.left = new TreeNode(5);
		root.right = new TreeNode(15);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(8);
		root.right.left = new TreeNode(12);
		root.right.right = new TreeNode(20);

		TwoSumBST twoSumBST = new TwoSumBST();
		System.out.println(" 19 is present " + twoSumBST.t2Sum(root, 19));
		System.out.println(" 24 is present " + twoSumBST.t2Sum(root, 24));
		System.out.println(" 20 is present " + twoSumBST.t2Sum(root, 20));
		System.out.println(" 22 is present " + twoSumBST.t2Sum(root, 22));
		System.out.println(" 32 is present " + twoSumBST.t2Sum(root, 32));
		System.out.println(" 25 is present " + twoSumBST.t2Sum(root, 25));
		System.out.println(" 35 is present " + twoSumBST.t2Sum(root, 35));

	}
}
