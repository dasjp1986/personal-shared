package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 * 
 *         Given two larger trees- T1 and T2, find if T2 is subtree of T1 where
 *         T1 is even larger than T2.
 * 
 *         Definition of subtree is, Once the tree T1 is cut at the root node of
 *         T2, both the subtrees have to be identical.
 * 
 *         Assume N- No.of nodes in T1 and M- No. of nodes in T2
 */
public class SubTreeFinder<T> {
	
	//Time Complexity- O(N+M)
	//Space Complexity- O(1)
	public boolean isSubTree(AbstractBinaryTree<T> tree1, AbstractBinaryTree<T> tree2) {
		TNode<T> node = findJoinNode(tree1.getRoot(), tree2.getRoot().getValue());
		if (node != null) {
			return comparePreOrders(node, tree2.getRoot());
		}
		return false;
	}

	
	//Time Complexity- O(M)
	private boolean comparePreOrders(TNode<T> node1, TNode<T> node2) {
		if (node1 == null || node2 == null) {
			return node1 == node2;
		}
		if (node1.getValue().equals(node2.getValue())) {
			return comparePreOrders(node1.getLeft(), node2.getLeft())
					? comparePreOrders(node1.getRight(), node2.getRight()) : false;
		} else {
			return false;
		}

	}

	//Time Complexity- O(N)
	private TNode<T> findJoinNode(TNode<T> node, T value) {
		if (node == null) {
			return null;
		}
		if (node.getValue().equals(value)) {
			return node;
		}
		return findJoinNode(node.getLeft(), value) != null ? node.getLeft() : findJoinNode(node.getRight(), value);
	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> tree1 = new CompleteBinaryTree<>();
		for (int i = 1; i <= 23; i++) {
			tree1.add(i);
		}

		tree1.preOrderIteratively();

		AbstractBinaryTree<Integer> tree2 = new CompleteBinaryTree<>();
		tree2.add(3);
		tree2.add(6);
		tree2.add(7);
		tree2.add(12);
		tree2.add(13);
		tree2.add(14);
		tree2.add(15);

		tree2.preOrderIteratively();

		SubTreeFinder<Integer> subTreeFinder = new SubTreeFinder<>();
		System.out.println(subTreeFinder.isSubTree(tree1, tree2));

		AbstractBinaryTree<Integer> tree3 = new CompleteBinaryTree<>();
		tree3.add(1);
		TNode<Integer> root3 = tree3.getRoot();
		root3.setLeft(new TNode<Integer>(4));
		root3.getLeft().setLeft(new TNode<Integer>(3));

		AbstractBinaryTree<Integer> tree4 = new CompleteBinaryTree<>();
		tree4.add(1);
		TNode<Integer> root4 = tree4.getRoot();
		root4.setLeft(new TNode<Integer>(4));
		root3.getLeft().setRight(new TNode<Integer>(3));

		System.out.println(subTreeFinder.isSubTree(tree3, tree4));

	}
}
