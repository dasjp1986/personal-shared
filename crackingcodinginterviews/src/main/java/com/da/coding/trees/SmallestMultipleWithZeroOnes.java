package com.da.coding.trees;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.Queue;

public class SmallestMultipleWithZeroOnes {
	public String multiple(int A) {

		Queue<String> q = new LinkedList<>();
		q.add("1");

		BigInteger x = new BigInteger(A + "");
		StringBuilder builder = null;
		while (!q.isEmpty()) {
			String s = q.remove();
			BigInteger num = new BigInteger(s);

			BigInteger mod = num.mod(x);
			if (mod.intValue() == 0) {
				return s;
			}

			builder = new StringBuilder(s);
			builder.append(0);
			q.add(builder.toString());

			builder = new StringBuilder(s);
			builder.append(1);
			q.add(builder.toString());
		}

		return null;
	}

	public static void main(String[] args) {
		SmallestMultipleWithZeroOnes problem = new SmallestMultipleWithZeroOnes();
		System.out.println(problem.multiple(12345));

	}
}
