package com.da.coding.trees;

import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author dagraw1
 *
 *         Bottom View of a Binary Tree Given a Binary Tree, we need to print
 *         the bottom view from left to right. A node x is there in output if x
 *         is the bottommost node at its horizontal distance. Horizontal
 *         distance of left child of a node x is equal to horizontal distance of
 *         x minus 1, and that of right child is horizontal distance of x plus
 *         1.
 * 
 *         Examples:
 * 
 *         20 / \ 8 22 / \ \ 5 3 25 / \ 10 14
 * 
 *         For the above tree the output should be 5, 10, 3, 14, 25.
 * 
 *         If there are multiple bottom-most nodes for a horizontal distance
 *         from root, then print the later one in level traversal. For example,
 *         in the below diagram, 3 and 4 are both the bottom-most nodes at
 *         horizontal distance 0, we need to print 4.
 * 
 * 
 *         20 / \ 8 22 / \ / \ 5 3 4 25 / \ 10 14 For the above tree the output
 *         should be 5, 10, 4, 14, 25.
 */
public class BottomViewTree {

	public static void main(String[] args) {
		TNode<Integer> root = new TNode<Integer>(1);

		root.setLeft(new TNode<Integer>(2));
		root.getLeft().setParent(root);

		root.setRight(new TNode<Integer>(3));
		root.getRight().setParent(root);

		root.getLeft().setRight(new TNode<Integer>(4));
		root.getLeft().getRight().setParent(root.getLeft());

		root.getRight().setLeft(new TNode<Integer>(5));
		root.getRight().getLeft().setParent(root.getRight());

		leftView(root);
	}

	// Time and Space Complexity- O(n)
	public static void leftView(TNode<Integer> root) {
		Map<Integer, TNode<Integer>> sortedMap = new TreeMap<>();
		traverse(root, null, sortedMap, 0);
		System.out.println(sortedMap.values());
	}

	private static void traverse(TNode<Integer> node, TNode<Integer> parent, Map<Integer, TNode<Integer>> sortedMap,
			int key) {
		if (node == null) {
			return;
		}

		int distance = key;
		if (parent == null) {
			sortedMap.put(distance, node);
		} else {

			if (parent.getLeft() == node) {
				distance = key - 1;
			} else {
				distance = key + 1;
			}

			sortedMap.put(distance, node);
		}

		traverse(node.getLeft(), node, sortedMap, distance);
		traverse(node.getRight(), node, sortedMap, distance);
	}

}
