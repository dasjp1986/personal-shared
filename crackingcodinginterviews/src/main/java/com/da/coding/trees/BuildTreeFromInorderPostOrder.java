package com.da.coding.trees;

import java.util.ArrayList;

public class BuildTreeFromInorderPostOrder {

	static int postIndex = 0;

	public TreeNode buildTree(ArrayList<Integer> inorder, ArrayList<Integer> postorder) {
		postIndex = postorder.size();
		return build(inorder, 0, inorder.size() - 1, postorder);
	}

	private TreeNode build(ArrayList<Integer> ir, int is, int ie, ArrayList<Integer> pr) {
		System.out.println("is: " + is + ", ie:" + ie + ", pe:" + postIndex);
		if (is <= ie && postIndex > 0) {
			postIndex--;
			TreeNode node = new TreeNode(pr.get(postIndex));
			if (is == ie) {
				return node;
			}
			int index = is;
			while (index <= ie) {
				if (ir.get(index).equals(pr.get(postIndex))) {
					break;
				}
				index++;
			}

			TreeNode right = build(ir, index + 1, ie, pr);
			TreeNode left = build(ir, is, index - 1, pr);

			if (left != null) {
				node.left = left;
			}

			if (right != null) {
				node.right = right;
			}

			return node;
		}

		return null;
	}

	public static void main(String[] args) {
		 int[] inorderArray = { 8, 2, 7, 4, 1, 3, 6, 5 };
		 int[] postorderArray = { 8, 7, 4, 2, 6, 5, 3, 1 };

//		int[] inorderArray = { 2, 1, 3 };
//		int[] postorderArray = { 2, 3, 1 };

		ArrayList<Integer> inorder = new ArrayList<>();
		ArrayList<Integer> postorder = new ArrayList<>();
		for (int i = 0; i < inorderArray.length; i++) {
			inorder.add(inorderArray[i]);
			postorder.add(postorderArray[i]);
		}

		BuildTreeFromInorderPostOrder build = new BuildTreeFromInorderPostOrder();
		TreeNode node = build.buildTree(inorder, postorder);
		build.inorder(node);
	}

	public void inorder(TreeNode node) {
		if (node != null) {
			inorder(node.left);
			System.out.println(node.val);
			inorder(node.right);
		}
	}

}
