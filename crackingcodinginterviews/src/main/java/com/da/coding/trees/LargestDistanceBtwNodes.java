package com.da.coding.trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class LargestDistanceBtwNodes {

	int longest = 0;

	public int solve(int[] A) {
		Map<Integer, List<Integer>> map = new HashMap<>();

		int root = -1;
		for (int i = 0; i < A.length; i++) {
			int parent = A[i];
			if (parent == -1) {
				root = i;
				continue;
			}

			List<Integer> list = map.get(parent);
			if (list == null) {
				list = new ArrayList<>();
				map.put(parent, list);
			}
			list.add(i);
		}

		dfs(map, root);
		dfsIteratively(map, root,A);

		return longest;
	}

	private void dfsIteratively(Map<Integer, List<Integer>> map, int root,int[] A) {
		Stack<Integer> stack = new Stack<>();
		stack.push(root);

		int first = 0;
		int second = 0;
		
		int[] heights= new int[A.length];
;
		while (!stack.isEmpty()) {
			Integer node = stack.pop();

			List<Integer> nodes = map.get(node);

			if (nodes == null) {
				heights[node]=1;
			} else {
				for (Integer n : nodes) {
					stack.push(n);
				}
			}

		}
	}

	private int dfs(Map<Integer, List<Integer>> map, int node) {
		List<Integer> nodes = map.get(node);
		if (nodes == null) {
			return 1;
		}

		int first = 0;
		int second = 0;

		for (Integer n : nodes) {
			int distance = dfs(map, n);
			if (distance > first) {
				second = first;
				first = distance;
			} else if (distance > second) {
				second = distance;
			}
		}

		int path = first + second;
		if (path > longest) {
			longest = path;
		}
		return first + 1;
	}

	public static void main(String[] args) {
		int[] a = { -1, 0, 1, 0, 0, 4, 5 };
		LargestDistanceBtwNodes dist = new LargestDistanceBtwNodes();
		System.out.println(dist.solve(a));
	}

}
