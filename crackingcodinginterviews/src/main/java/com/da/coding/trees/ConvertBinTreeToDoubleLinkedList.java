package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 *
 *         Given a Binary Tree (BT), convert it to a Doubly Linked List(DLL).
 *         The left and right pointers in nodes are to be used as previous and
 *         next pointers respectively in converted DLL. The order of nodes in
 *         DLL must be same as Inorder of the given Binary Tree. The first node
 *         of Inorder traversal (left most node in BT) must be head node of the
 *         DLL.
 */
public class ConvertBinTreeToDoubleLinkedList {
	private TNode<Integer> head;
	private TNode<Integer> lastNode;
	private TNode<Integer> prev;

	public void convert(TNode<Integer> root) {
		fixPrevPointers(root);
		lastNode = root;
		while (lastNode.getRight() != null) {
			lastNode = lastNode.getRight();
		}
		fixNextPointers();
		printDLL();
	}

	private void printDLL() {
		TNode<Integer> temp = head;
		System.out.println("Ordered DLL");
		while (temp != null) {
			System.out.println(temp.getValue() + "->>");
			temp = temp.getRight();
		}

		System.out.println("Reversed DLL");
		temp = lastNode;
		while (temp != null) {
			System.out.println(temp.getValue() + "->>");
			temp = temp.getLeft();
		}
	}

	private void fixNextPointers() {
		TNode<Integer> tNode = lastNode;
		TNode<Integer> next = null;
		while (tNode != null) {
			tNode.setRight(next);
			next = tNode;
			tNode = tNode.getLeft();
		}

	}

	private void fixPrevPointers(TNode<Integer> node) {
		if (node == null) {
			return;
		}

		fixPrevPointers(node.getLeft());
		if (head == null) {
			head = node;
		}
		node.setLeft(prev);
		prev = node;
		fixPrevPointers(node.getRight());
	}

	public static void main(String[] args) {
		CompleteBinaryTree<Integer> cbt = new CompleteBinaryTree<>();
		for (int i = 1; i <= 3; i++) {
			cbt.add(i);
		}
		cbt.inOrderIteratively();
		ConvertBinTreeToDoubleLinkedList convertProblem = new ConvertBinTreeToDoubleLinkedList();
		convertProblem.convert(cbt.getRoot());
	}
}
