package com.da.coding.trees;

import java.util.ArrayList;

public class RootToLeafPathSumBinTree {

	public ArrayList<ArrayList<Integer>> pathSum(TreeNode A, int B) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();
		findPaths(A, B, 0, new ArrayList<Integer>(), results);

		return results;
	}

	private void findPaths(TreeNode node, int target, int runningSum, ArrayList<Integer> res,
			ArrayList<ArrayList<Integer>> results) {

		if (node != null) {
			res.add(node.val);
			if (node.val + runningSum == target && node.left == null && node.right == null) {
				results.add(res);
			} else {
				ArrayList<Integer> clone = new ArrayList<Integer>();
				clone.addAll(res);
				findPaths(node.left, target, node.val + runningSum, res, results);
				findPaths(node.right, target, node.val + runningSum, clone, results);
			}

		}

	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(4);
		root.right.left = new TreeNode(0);
		root.right.right = new TreeNode(2);

		RootToLeafPathSumBinTree binTree = new RootToLeafPathSumBinTree();
		ArrayList<ArrayList<Integer>> results = binTree.pathSum(root, 4);
		System.out.println(results);
	}

}
