package com.da.coding.trees;

import java.util.LinkedList;
import java.util.Queue;

public class NextRightPointersBinTree {

	public void connectSingleQueue(TreeLinkNode root) {
		Queue<TreeLinkNode> q = new LinkedList<>();
		q.add(root);
		q.add(null);

		TreeLinkNode node = null;

		while (!q.isEmpty()) {
			node = q.remove();

			if (node != null) {
				node.next = q.peek();

				if (node.left != null) {
					q.add(node.left);
				}

				if (node.right != null) {
					q.add(node.right);
				}
			} else if (!q.isEmpty()) {
				q.add(null);
			}
		}

	}

	public void connect(TreeLinkNode root) {
		Queue<TreeLinkNode> q1 = new LinkedList<>();
		Queue<TreeLinkNode> q2 = new LinkedList<>();

		TreeLinkNode temp;
		TreeLinkNode last;
		q1.add(root);

		while (!q1.isEmpty() || !q2.isEmpty()) {
			last = null;

			while (!q1.isEmpty()) {
				temp = q1.remove();
				if (last != null) {
					last.next = temp;
				}
				if (temp.left != null) {
					q2.add(temp.left);
				}
				if (temp.right != null) {
					q2.add(temp.right);
				}
				last = temp;
			}

			last = null;
			while (!q2.isEmpty()) {
				temp = q2.remove();
				if (last != null) {
					last.next = temp;
				}
				if (temp.left != null) {
					q1.add(temp.left);
				}
				if (temp.right != null) {
					q1.add(temp.right);
				}
				last = temp;
			}

		}
	}

	public static void main(String[] args) {
		TreeLinkNode root = new TreeLinkNode(1);
		root.left = new TreeLinkNode(2);
		root.right = new TreeLinkNode(3);
		root.left.left = new TreeLinkNode(4);
		root.left.right = new TreeLinkNode(5);
		root.right.left = new TreeLinkNode(6);
		root.right.right = new TreeLinkNode(7);

		NextRightPointersBinTree next = new NextRightPointersBinTree();
//		next.connect(root);
//		next.print(root);
		
		next.connectSingleQueue(root);
		next.print(root);
	}

	private void print(TreeLinkNode node) {
		if (node != null) {
			System.out.println(node);
			print(node.left);
			print(node.right);
		}

	}

}

class TreeLinkNode {
	int val;
	TreeLinkNode left, right, next;

	TreeLinkNode(int x) {
		val = x;
	}

	@Override
	public String toString() {
		return "TreeLinkNode [val=" + val + ", next=" + next + "]";
	}

}
