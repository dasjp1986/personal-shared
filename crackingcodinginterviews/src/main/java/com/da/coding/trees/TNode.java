package com.da.coding.trees;

public class TNode<T> {
	private TNode<T> left;
	private T value;
	private TNode<T> right;
	private int size;

	// this is optional
	private TNode<T> parent;

	public TNode(T value) {
		super();
		this.value = value;
		this.size= 1;
	}

	public TNode<T> getLeft() {
		return left;
	}

	public void setLeft(TNode<T> left) {
		this.left = left;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public TNode<T> getRight() {
		return right;
	}

	public void setRight(TNode<T> right) {
		this.right = right;
	}

	public TNode<T> getParent() {
		return parent;
	}

	public void setParent(TNode<T> parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return value + "";
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
