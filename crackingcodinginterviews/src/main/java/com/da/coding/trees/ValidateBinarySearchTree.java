package com.da.coding.trees;

/**
 * 
 * @author dagraw1 Given a Binary Tree, Validate if it a valid Binary Seach Tree
 * 
 *         This solution works perfectly fine if the tree does not contain any
 *         duplicates as inorder traversal will be same for both the trees as
 *         below 1 \ 1 and 1 / 1 But only one of the tree can only be valid BTS
 *         but not both
 */
public class ValidateBinarySearchTree {

	private AbstractBinaryTree<Integer> tree;
	private int minValue = Integer.MIN_VALUE;

	public ValidateBinarySearchTree(AbstractBinaryTree<Integer> tree) {
		this.tree = tree;
	}

	public boolean isValidBSTUsingInOrder() {
		return inOrder(tree.getRoot());
	}

	// Time Complexity- O(N)
	private boolean inOrder(TNode<Integer> node) {
		if (node != null) {
			if (!inOrder(node.getLeft())) {
				return false;
			}
			if (node.getValue() < minValue) {
				return false;
			} else {
				minValue = node.getValue();
			}
			if (!inOrder(node.getRight())) {
				return false;
			}
		}
		return true;
	}

	// Time Complexity- O(N)
	// Space Complexity- O(LogN)- due to recursive calls
	public boolean isValidBSTByDefinition() {
		TNode<Integer> root = tree.getRoot();
		if (root.getLeft() != null) {
			if (root.getLeft().getValue().intValue() <= root.getValue()) {
				if (!isBSTByDef(root.getLeft(), root.getValue(), true)) {
					return false;
				}
			} else {
				return false;
			}
		}
		if (root.getRight() != null) {
			if (root.getRight().getValue().intValue() > root.getValue()) {
				if (!isBSTByDef(root.getRight(), root.getValue(), false)) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	public boolean isValidOptimized() {
		return isValidOpt(tree.getRoot(), null, null);
	}

	private boolean isValidOpt(TNode<Integer> node, Integer max, Integer min) {

		if (node == null) {
			return true;
		}

		if (node.getLeft() != null && node.getLeft().getValue() > node.getValue()
				|| node.getRight() != null && max != null && node.getRight().getValue() > max) {
			return false;
		}

		if (node.getRight() != null && node.getRight().getValue() <= node.getValue()
				|| node.getLeft() != null && min != null && node.getLeft().getValue() <= min) {
			return false;
		}

		if (!(isValidOpt(node.getLeft(), node.getValue(), min) || isValidOpt(node.getRight(), max, node.getValue()))) {
			return false;
		}

		return true;
	}

	private boolean isBSTByDef(TNode<Integer> node, Integer value, boolean isLeftSubTree) {

		if (node.getLeft() != null) {
			if (node.getLeft().getValue().intValue() <= node.getValue()) {
				if (!isLeftSubTree && node.getLeft().getValue().intValue() <= value) {
					return false;
				}
				if (!isBSTByDef(node.getLeft(), node.getValue(), true)) {
					return false;
				}
			} else {
				return false;
			}
		}

		if (node.getRight() != null) {
			if (node.getRight().getValue().intValue() > node.getValue()) {
				if (isLeftSubTree && node.getLeft().getValue().intValue() > value) {
					return false;
				}
				if (!isBSTByDef(node.getRight(), node.getValue(), false)) {
					return false;
				}
			} else {
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) {
		// AbstractBinaryTree<Integer> binTree = new CompleteBinaryTree<>();
		// for (int i = 1; i <= 10; i++) {
		// binTree.add(i);
		// }
		//
		// ValidateBinarySearchTree validateBinaryTree = new
		// ValidateBinarySearchTree(binTree);
		// System.out.println("Inorder travesal says : " +
		// validateBinaryTree.isValidBSTUsingInOrder());
		// System.out.println("BST definition says : " +
		// validateBinaryTree.isValidBSTByDefinition());
		// System.out.println("Optimized BST definition says : " +
		// validateBinaryTree.isValidOptimized());
		//
		// AbstractBinaryTree<Integer> bst = new BinarySearchTree();
		//
		// for (int i = 1; i <= 3; i++) {
		// bst.add(i);
		// }
		//
		// ValidateBinarySearchTree validateBST = new
		// ValidateBinarySearchTree(bst);
		// System.out.println("Inorder travesal says : " +
		// validateBST.isValidBSTUsingInOrder());
		// System.out.println("BST definition says : " +
		// validateBST.isValidBSTByDefinition());
		// System.out.println("Optimized BST definition says : " +
		// validateBST.isValidOptimized());

		TNode<Integer> root = new TNode<Integer>(11);
		root.setLeft(new TNode<Integer>(3));
		root.setRight(new TNode<Integer>(2));
		root.getLeft().setLeft(new TNode<Integer>(4));
		root.getLeft().setRight(new TNode<Integer>(1));
		root.getRight().setLeft(new TNode<Integer>(3));

		System.out.println(isValidBST(root));
	}

	public static int isValidBST(TNode<Integer> node) {
		return isValid(node, null, null);
	}

	private static int isValid(TNode<Integer> node, Integer max, Integer min) {
		if (node == null) {
			return 1;
		}

		if (node.getLeft() != null) {
			if (node.getLeft().getValue() >= node.getValue() || (min != null && node.getLeft().getValue() < min)) {
				return 0;
			}
		}

		if (node.getRight() != null) {
			if (node.getRight().getValue() < node.getValue() || (max != null && node.getRight().getValue() > max)) {
				return 0;
			}
		}

		return isValid(node.getLeft(), node.getValue(), min) & isValid(node.getRight(), max, node.getValue());
	}

}
