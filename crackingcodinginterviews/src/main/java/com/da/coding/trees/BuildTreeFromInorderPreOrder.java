package com.da.coding.trees;

import java.util.ArrayList;

public class BuildTreeFromInorderPreOrder {

	int preIndex = -1;

	public TreeNode buildTree(ArrayList<Integer> preorder, ArrayList<Integer> inorder) {

		return build(preorder, inorder, 0, inorder.size() - 1);
	}

	private TreeNode build(ArrayList<Integer> preorder, ArrayList<Integer> inorder, int start, int end) {
		if (preIndex < preorder.size() - 1 && start <= end) {
			preIndex++;
			TreeNode node = new TreeNode(preorder.get(preIndex));

			if (start == end) {
				return node;
			}

			int index = start;
			while (index <= end) {
				if (inorder.get(index).equals(preorder.get(preIndex))) {
					break;
				}
				index++;
			}
			TreeNode left = build(preorder, inorder, start, index - 1);
			TreeNode right = build(preorder, inorder, index + 1, end);

			if (left != null) {
				node.left = left;
			}

			if (right != null) {
				node.right = right;
			}

			return node;
		}

		return null;
	}

	public static void main(String[] args) {

		int[] preorderArray = { 2, 1, 6, 5, 3, 4 };
		int[] inorderArray = { 5, 6, 1, 2, 3, 4 };

		ArrayList<Integer> inorder = new ArrayList<>();
		ArrayList<Integer> preorder = new ArrayList<>();
		for (int i = 0; i < inorderArray.length; i++) {
			inorder.add(inorderArray[i]);
			preorder.add(preorderArray[i]);
		}

		BuildTreeFromInorderPreOrder build = new BuildTreeFromInorderPreOrder();
		TreeNode node = build.buildTree(preorder, inorder);
		build.inorder(node);
	}

	public void inorder(TreeNode node) {
		if (node != null) {
			inorder(node.left);
			System.out.println(node.val);
			inorder(node.right);
		}
	}

}
