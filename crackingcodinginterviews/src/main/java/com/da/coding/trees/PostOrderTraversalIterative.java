package com.da.coding.trees;

import java.util.ArrayList;
import java.util.Stack;

public class PostOrderTraversalIterative {
	public void inorder(TNode<Integer> root) {
		if (root == null) {
			throw new RuntimeException("Tree is empty...");
		}

		Stack<TNode<Integer>> stack = new Stack<>();
		System.out.println("\n Printing tree in postOrder iteratively");
		TNode<Integer> node = root;
		while (node != null) {
			if (node.getRight() != null) {
				stack.push(node.getRight());
			}
			stack.push(node);
			node = node.getLeft();

			while (node == null && !stack.isEmpty()) {
				node = stack.pop();
				if (node.getRight() == null) {
					System.out.print(" " + node.getValue());
					node = null;
				} else {
					if (!stack.isEmpty() && node.getRight().getValue().equals(stack.peek().getValue())) {
						TNode<Integer> temp = node;
						node = stack.pop();
						stack.push(temp);
					} else {
						System.out.print(" " + node.getValue());
						node = null;
					}
				}
			}
		}
	}

	public static ArrayList<Integer> postorderTraversal(TreeNode A) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		TreeNode node = A;
		Stack<TreeNode> stack = new Stack<>();
		stack.push(node);
		TreeNode prev = A;
		boolean isMatch= false;

		while (!stack.isEmpty()) {
			node = stack.peek();
			isMatch= false;
			if (node.left == null && node.right == null) {
				node = stack.pop();
				prev = node;
				res.add(node.val);
			} else {
				if (node.right != null) {
					if (node.right == prev) {
						node = stack.pop();
						prev = node;
						res.add(node.val);
						isMatch= true;
					} else {
						stack.push(node.right);
					}
				}
				if (!isMatch && node.left != null) {
					if (node.left == prev) {
						node = stack.pop();
						prev = node;
						res.add(node.val);
					} else {
						stack.push(node.left);
					}
				}
			}
		}

		return res;
	}

	public static void main(String[] args) {
		// PostOrderTraversalIterative postOrder = new
		// PostOrderTraversalIterative();
		// TNode<Integer> root = new TNode<Integer>(1);
		// root.setLeft(new TNode<Integer>(2));
		// root.setRight(new TNode<Integer>(3));
		// root.getLeft().setLeft(new TNode<Integer>(4));
		// root.getLeft().setRight(new TNode<Integer>(5));
		// root.getLeft().getRight().setLeft(new TNode<Integer>(6));
		// root.getLeft().getRight().setRight(new TNode<Integer>(7));
		// root.getRight().setRight(new TNode<Integer>(8));
		// root.getRight().getRight().setLeft(new TNode<Integer>(9));
		// root.getRight().getRight().getLeft().setRight(new
		// TNode<Integer>(10));
		//
		// postOrder.inorder(root);

		TreeNode root = new TreeNode(1);
		root.left= new TreeNode(2);
		root.right= new TreeNode(3);
		root.left.left= new TreeNode(4);
		root.left.right= new TreeNode(5);
		
		System.out.println(postorderTraversal(root));
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
		left = null;
		right = null;
	}

	@Override
	public String toString() {
		return "TreeNode [val=" + val + "]";
	}
	
	
}
