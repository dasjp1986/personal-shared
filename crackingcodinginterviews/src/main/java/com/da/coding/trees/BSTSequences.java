package com.da.coding.trees;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.da.coding.linkedlist.ListsWeaver;

/**
 * Given a Binary Search Tree, Print all the arrays which could have lead to
 * this tree.
 * 
 * 	 2 
 *  / \ 
 * 1   3 
 * 
 * Arrays- {2,1,3} and {2,3,1}
 * 
 * Assumption- There are no duplicates in the BST
 * 
 * @author dagraw1
 *
 */
public class BSTSequences<T> {
	ListsWeaver<T> weaveLists = new ListsWeaver<>();

	public ArrayList<LinkedList<T>> getSequences(TNode<T> node) {
		ArrayList<LinkedList<T>> results = new ArrayList<>();
		if (node == null) {
			results.add(new LinkedList<T>());
			return results;
		}

		LinkedList<T> prefix = new LinkedList<>();
		prefix.add(node.getValue());

		ArrayList<LinkedList<T>> leftSeq = getSequences(node.getLeft());
		ArrayList<LinkedList<T>> rightSeq = getSequences(node.getRight());

		for (LinkedList<T> left : leftSeq) {
			for (LinkedList<T> right : rightSeq) {
				ArrayList<LinkedList<T>> weaved = new ArrayList<>();
				weaveLists.weave(left, right, prefix, weaved);
				results.addAll(weaved);
			}
		}

		return results;
	}

	public static void main(String[] args) {
		AbstractBinaryTree<Integer> binTree = new BinarySearchTree();
		binTree.add(10);
		binTree.add(5);
		binTree.add(20);
		binTree.add(4);
		binTree.add(9);
		binTree.add(15);
		binTree.add(30);
		BSTSequences<Integer> bstSequences = new BSTSequences<>();

		ArrayList<LinkedList<Integer>> sequences = bstSequences.getSequences(binTree.getRoot());
		Iterator<LinkedList<Integer>> iterator = sequences.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());

		}
	}

}
