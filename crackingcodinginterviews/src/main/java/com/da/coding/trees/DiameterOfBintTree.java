package com.da.coding.trees;

/**
 * 
 * @author dagraw1
 *
 *         The diameter of a tree (sometimes called the width) is the number of
 *         nodes on the longest path between two end nodes. The diagram below
 *         shows two trees each with diameter nine, the leaves that form the
 *         ends of a longest path are shaded (note that there is more than one
 *         path in each tree of length nine, but no path longer than nine
 *         nodes).
 */
public class DiameterOfBintTree {

	//Time Complexity- O(n)
	public int largestDiameter(TNode<Integer> root) {
		Result result = new Result();
		find(root, result);
		return result.max;
	}

	private int find(TNode<Integer> node, Result result) {
		if (node == null) {
			return 0;
		}
		if (node.getLeft() == null && node.getRight() == null) {
			return 1;
		}
		int left = find(node.getLeft(), result);
		int right = find(node.getRight(), result);

		if (node.getLeft() != null && node.getRight() != null) {
			result.max = Math.max(result.max, left + right + 1);

			return Math.max(left, right) + 1;
		}
		return node.getLeft() != null ? left + 1 : right + 1;
	}

	public static void main(String[] args) {
		TNode<Integer> root = new TNode<Integer>(1);
		root.setLeft(new TNode<Integer>(2));
		root.setRight(new TNode<Integer>(3));
		root.getLeft().setRight(new TNode<Integer>(4));
		root.getRight().setLeft(new TNode<Integer>(5));

		DiameterOfBintTree diameterOfBintTree = new DiameterOfBintTree();
		System.out.println(diameterOfBintTree.largestDiameter(root));
	}
}

class Result {
	int max;
}
