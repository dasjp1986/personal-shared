package com.da.coding.strings;

/**
 * 
 * 
 * @author dagraw1
 *
 *         Given a string, recursively remove adjacent duplicate characters from
 *         string. The output string should not have any adjacent duplicates.
 *         See following examples.
 * 
 *         Input: azxxzy Output: ay First "azxxzy" is reduced to "azzy". The
 *         string "azzy" contains duplicates, so it is further reduced to "ay".
 * 
 *         Input: geeksforgeeg Output: gksfor First "geeksforgeeg" is reduced to
 *         "gksforgg". The string "gksforgg" contains duplicates, so it is
 *         further reduced to "gksfor".
 * 
 *         Input: caaabbbaacdddd Output: Empty String
 * 
 *         Input: acaaabbbacdddd Output: acac
 */
public class RemoveAllConsecutiveDupChars {

	public static void main(String[] args) {
		System.out.println(removeConsecDups("azxxzy"));
	}

	//Time Complexity- O(n)
	public static String removeConsecDups(String str) {
		if (str.length() < 2) {
			return str;
		}

		return rd(str, str.substring(0, 1), 1);
	}

	private static String rd(String str, String res, int i) {
		if (i == str.length()) {
			return res;
		}
		int j = res.length() - 1;
		boolean isDups = false;
		while (j >= 0 && i < str.length() && res.charAt(j) == str.charAt(i)) {
			i++;
			isDups = true;
		}
		if (isDups) {
			res = res.substring(0, j);
			return rd(str, res, i);
		} else {
			res = res + str.charAt(i);
			return rd(str, res, i + 1);
		}

	}
}
