package com.da.coding.strings;

/**
 * 
 * @author dagraw1
 *
 *         Given two strings ‘X’ and ‘Y’, find the length of the longest common
 *         substring.
 * 
 *         Examples :
 * 
 *         Input : X = "GeeksforGeeks", y = "GeeksQuiz" Output : 5 The longest
 *         common substring is "Geeks" and is of length 5.
 * 
 *         Input : X = "abcdxyz", y = "xyzabcd" Output : 4 The longest common
 *         substring is "abcd" and is of length 4.
 * 
 *         Input : X = "zxabcdezy", y = "yzabcdezx" Output : 6 The longest
 *         common substring is "abcdez" and is of length 6.
 */
public class LongestCommonSubstring {
	public static void main(String[] args) {
		System.out.println(lcs("ab", "ab"));
		System.out.println(lcs("GeeksforGeeks", "GeeksQuiz"));
		System.out.println(lcs("abcdxyz", "xyzabcd"));
		System.out.println(lcs("zxabcdezy", "yzabcdezx"));
	}

	//Time and Space Complexity- O(m*n)
	public static int lcs(String str1, String str2) {
		int[][] matrix = new int[str1.length()][str2.length()];
		return find(matrix, str1, str2);
	};
	private static int find(int[][] matrix, String str1, String str2) {
		int max = 0;
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[row].length; col++) {
				if (str1.charAt(row) == str2.charAt(col)) {
					matrix[row][col] = 1;
					if (row - 1 >= 0 && col - 1 >= 0) {
						matrix[row][col] = matrix[row][col] + matrix[row - 1][col - 1];
					}
					if (max < matrix[row][col]) {
						max = matrix[row][col];
					}
				}
			}
		}
		return max;
	}

}
