package com.da.coding.strings;

import java.util.Stack;

public class ReverseSentance {
	public String reverseWords(String a) {
		Stack<String> stack = new Stack<>();

		StringBuilder builder = new StringBuilder();
		populateStack(a, stack);

		int size = stack.size();
		while (!stack.isEmpty()) {
			builder.append(stack.pop());
			size--;
			if (size != 0) {
				builder.append(" ");
			}
		}

		return builder.toString();
	}

	private void populateStack(String a, Stack<String> stack) {
		int i = 0;
		StringBuilder builder;
		while (i < a.length()) {
			if (a.charAt(i) == ' ') {
				i++;
				continue;
			} else {
				builder = new StringBuilder();
				while (i < a.length() && a.charAt(i) != ' ') {
					builder.append(a.charAt(i) + "");
					i++;
				}
				stack.push(builder.toString());
			}
			i++;
		}
	}

	public static void main(String[] args) {
		ReverseSentance sentance = new ReverseSentance();
		System.out.println(sentance.reverseWords("i am a software engineer"));
	}
}
