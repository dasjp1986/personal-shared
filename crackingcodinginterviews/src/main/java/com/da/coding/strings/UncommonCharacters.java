package com.da.coding.strings;

/**
 * 
 * @author dagraw1
 *
 *         Find and print the uncommon characters of the two given strings. Here
 *         uncommon character means that either the character is present in one
 *         string or it is present in other string but not in both. The strings
 *         contain only lowercase characters and can contain duplicates.
 */
public class UncommonCharacters {
	public static void uncommon(String str1, String str2) {
		char[] ascii = new char[256];
		for (int i = 0; i < str1.length(); i++) {
			ascii[str1.charAt(i)] = 1;
		}
		print(ascii);
		for (int j = 0; j < str2.length(); j++) {
			if(ascii[str2.charAt(j)]!=0){
				ascii[str2.charAt(j)]= (char) -1;
			}else{
				ascii[str2.charAt(j)]=1;
			}
		}
		print(ascii);
	}
	
	private static void print(char[] ascii){
		StringBuilder builder= new StringBuilder();
		for (int i = 0; i < ascii.length; i++) {
			if(ascii[i]==1){
				builder.append((char) i);
			}
		}
		System.out.println(builder.toString());
	}
	
	public static void main(String[] args) {
		uncommon("characters", "alphabets");
	}
}
