package com.da.coding.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubstringConcat {

	// DO NOT MODIFY THE LIST. IT IS READ ONLY
	public ArrayList<Integer> findSubstring(String A, final List<String> B) {
		Map<Character, List<String>> lookup = createLookUp(B);
		int total = wordSizes(B);
		int index = 0;
		ArrayList<Integer> results = new ArrayList<>();
		for (int i = 0; i < A.length(); i++) {
			List<String> words = lookup.get(A.charAt(i));
			if (words != null) {
				index = i;
				int count = 0;
				int j = i;
				List<String> clone = new ArrayList<>();
				clone.addAll(B);
				while (j < A.length() && j < i + total) {
					words = lookup.get(A.charAt(j));
					boolean flag = true;
					if (words == null)
						break;

					for (String word : words) {
						if (isMatch(A, word, j)) {
							j = j + word.length();
							flag = false;
							count++;
							clone.remove(word);
						}
					}

					if (flag) {
						break;
					}
				}
				if (count == B.size() && clone.size() == 0) {
					results.add(index);
				}

			}
		}
		return results;
	}

	private boolean isMatch(String str, String word, int i) {
		String s = str.substring(i, (i + word.length()) <= str.length() ? i + word.length() : str.length());
		return s.equals(word);
	}

	private int wordSizes(List<String> B) {
		int count = 0;
		for (String str : B) {
			count += str.length();
		}
		return count;
	}

	private Map<Character, List<String>> createLookUp(List<String> B) {
		Map<Character, List<String>> map = new HashMap<>();
		for (String str : B) {
			List<String> list = map.get(str.charAt(0));
			if (list == null) {
				list = new ArrayList<>();
				map.put(str.charAt(0), list);
			}
			list.add(str);
		}

		return map;
	}

	public static void main(String[] args) {
		SubstringConcat concat = new SubstringConcat();
//		String str = "bcabcbcbcacccbaaaabcaaabacccaaaacbaacabbcabccca";
//		List<String> list = new ArrayList<>();
//		list.add("bc");

		 String str = "barfoothefoobarman";
		 List<String> list = new ArrayList<>();
		 list.add("foo");
		 list.add("bar");

		ArrayList<Integer> res = concat.findSubstring(str, list);
		System.out.println(res);

		for (int index : res) {
			System.out.println(str.substring(index, index + 2));
		}
	}

}
