package com.da.coding.strings;

public class BinarySum {
	public String addBinary(String A, String B) {
        StringBuilder builder = new StringBuilder();
        int i = A.length() - 1;
        int j = B.length() - 1;

        int sum = 0, carry = 0;
        while (i >= 0 && j >= 0) {
            sum = Integer.parseInt(A.charAt(i) + "");
            sum = sum + Integer.parseInt(B.charAt(j) + "") + carry;
            builder.append(sum % 2);
            carry = sum / 2;
            i--;
            j--;
        }
        while (i >= 0) {
            sum = Integer.parseInt(A.charAt(i) + "") + carry;
            builder.append(sum % 2);
            carry = sum / 2;
            i--;
        }

        while (j >= 0) {
            sum = Integer.parseInt(A.charAt(j) + "") + carry;
            builder.append(sum % 2);
            carry = sum / 2;
            j--;
        }

        if(carry!=0){
            builder.append(carry);
        }
        return builder.reverse().toString();
    }
	
	public static void main(String[] args) {
		BinarySum binarySum= new BinarySum();
		System.out.println(binarySum.addBinary("1010110111001101101000", "1000011011000000111100110"));
	}
}
