package com.da.coding.strings;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1
 *
 *         	Given a String, check of one of its permutation is Palindrome
 *         	Assumption- Don't count spaces, case insensitive
 *         	Tact coa
			true (permutations taco cat)
 *         	Time Complexity- O(N), Space Complexity O(C)
 */
public class PalindromePermutation {

	public static boolean isPalindrome(String strInput) {

		// Use ASCII charSet- 128, user 256- for Unicode
		int[] charCounts = new int[128];
		char[] charArray = strInput.toCharArray();

		for (char ch : charArray) {
			if (ch != ' ')
				charCounts[ch] = charCounts[ch] + 1;
		}

		int oddCounts = 0;

		for (int charCount : charCounts) {
			if (charCount % 2 != 0)
				oddCounts++;
		}

		if (oddCounts < 2)
			return true;

		return false;
	}

	public static void main(String[] args) {
		String inputString = SystemInputReaderUtil.readString();
		System.out.println(isPalindrome(inputString.toLowerCase()));
	}
}
