package com.da.coding.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         You're given strings J representing the types of stones that are
 *         jewels, and S representing the stones you have. Each character in S
 *         is a type of stone you have. You want to know how many of the stones
 *         you have are also jewels.
 * 
 *         The letters in J are guaranteed distinct, and all characters in J and
 *         S are letters. Letters are case sensitive, so "a" is considered a
 *         different type of stone from "A".
 * 
 *         Example 1:
 * 
 *         Input: J = "aA", S = "aAAbbbb" Output: 3 Example 2:
 * 
 *         Input: J = "z", S = "ZZ" Output: 0 Note:
 * 
 *         S and J will consist of letters and have length at most 50. The
 *         characters in J are distinct.
 */
public class JewelsAndStones {
	public int numJewelsInStones(String J, String S) {
		int count = 0;
		if (J == null || J.isEmpty() || S == null || S.isEmpty()) {
			return count;
		}

		Map<Character, Boolean> jewelsMap = createMap(J);

		for (int i = 0; i < S.length(); i++) {
			if (jewelsMap.get(S.charAt(i)) != null) {
				count++;
			}
		}

		return count;
	}

	private Map<Character, Boolean> createMap(String J) {
		Map<Character, Boolean> map = new HashMap<>();

		for (int i = 0; i < J.length(); i++) {
			map.put(J.charAt(i), true);
		}

		return map;
	}
	
	public static void main(String[] args) {
		JewelsAndStones problem= new JewelsAndStones();
		String J= "aA";
		String S= "aAAbbbb";
		System.out.println(problem.numJewelsInStones(J, S));
	}
}
