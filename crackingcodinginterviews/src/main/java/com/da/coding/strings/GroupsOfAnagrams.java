package com.da.coding.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author dagraw1
 * 
 *         Given an array of strings, return all groups of strings that are
 *         anagrams. Represent a group by a list of integers representing the
 *         index in the original list. Look at the sample case for
 *         clarification.
 * 
 *         Anagram : a word, phrase, or name formed by rearranging the letters
 *         of another, such as 'spar', formed from 'rasp' Note: All inputs will
 *         be in lower-case. Example :
 * 
 *         Input : cat dog god tca Output : [[1, 4], [2, 3]] cat and tca are
 *         anagrams which correspond to index 1 and 4. dog and god are another
 *         set of anagrams which correspond to index 2 and 3. The indices are 1
 *         based ( the first element has index 1 instead of index 0).
 * 
 *         Ordering of the result : You should not change the relative ordering
 *         of the words / phrases within the group. Within a group containing
 *         A[i] and A[j], A[i] comes before A[j] if i < j.
 *
 */
public class GroupsOfAnagrams {

	// DO NOT MODIFY THE LIST. IT IS READ ONLY
	public ArrayList<ArrayList<Integer>> anagrams(final List<String> A) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();
		Map<Integer, ArrayList<Integer>> indexMap = new HashMap<>();
		for (int i = 0; i < A.size(); i++) {
			Integer h = hash(A.get(i));
			ArrayList<Integer> indexes = indexMap.get(h);
			if (indexes == null) {
				indexes = new ArrayList<>();
				indexMap.put(h, indexes);
			}
			indexes.add(i + 1);
		}
		for (Entry<Integer, ArrayList<Integer>> entry : indexMap.entrySet()) {
			results.add(entry.getValue());
		}

		return results;
	}

	private Integer hash(String str) {
		Integer hash = 0;
		for (int i = 0; i < str.length(); i++) {
			hash += str.charAt(i) * (str.charAt(i) - 'a' + 1);
		}

		return hash;
	}

}
