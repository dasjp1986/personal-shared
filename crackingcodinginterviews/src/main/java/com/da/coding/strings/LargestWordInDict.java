package com.da.coding.strings;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *
 *         Giving a dictionary and a string ‘str’, your task is to find the
 *         longest string in dictionary of size x which can be formed by
 *         deleting some characters of the given ‘str’.
 * 
 *         Examples:
 * 
 *         Input : dict = {"ale", "apple", "monkey", "plea"} str = "abpcplea"
 *         Output : apple
 * 
 *         Input : dict = {"pintu", "geeksfor", "geeksgeeks", " forgeek"} str =
 *         "geeksforgeeks" Output : geeksgeeks
 * 
 * 
 *         Input: The first line of input contains an integer T denoting the no
 *         of test cases . Then T test cases follow. Each test case contains an
 *         integer x denoting the no of words in the dictionary. Then in the
 *         next line are x space separated strings denoting the contents of the
 *         dictionary. Then in the next line is a string 'str'.
 * 
 *         Output: For each test case in a new line print the required output.
 * 
 *         Constraints: 1<=T<=100 1<=x, length of str<=1000
 * 
 *         Example: Input: 2 4 ale apple monkey plea abpcplea 4 pintu geeksfor
 *         geeksgeeks forgeek geeksforgeeks Output: apple geeksgeeks
 */
public class LargestWordInDict {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			String[] dict = new String[n];
			for (int i = 0; i < n; i++) {
				dict[i] = scn.next();
			}
			System.out.println(solve(n, dict, scn.next()));
		}
		scn.close();
	}

	//Time complexity- O(words*string length) = O(M*N)
	public static String solve(int n, String[] words, String str) {
		int max = 0;
		String result = null;

		for (String word : words) {
			if (isMatch(word, str)) {
				if (word.length() > max) {
					max = word.length();
					result = word;
				}
			}
		}

		return result;
	}

	public static boolean isMatch(String word, String str) {
		int i = 0, j = 0;
		if (word.length() > str.length()) {
			return false;
		}
		while (i < word.length() && j < str.length()) {
			if (word.charAt(i) == str.charAt(j)) {
				i++;
			}
			j++;
		}

		if (i == word.length()) {
			return true;
		}

		return false;
	}

}
