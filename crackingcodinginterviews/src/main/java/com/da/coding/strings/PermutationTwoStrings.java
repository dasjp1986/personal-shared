package com.da.coding.strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1 Given two strings, check if one is permutation of the other
 *         String
 * 
 *         abcd 
 *         dbca 
 *         true
 * 
 *         abc 
 *         abd 
 *         false
 *
 */
public class PermutationTwoStrings {

	// Time Complexity- O(N), Space Complexity- O(C)~O(1)
	public static boolean isPermutationCharSet(String str1, String str2) {
		// number of ASCII characters
		int[] charCounter = new int[128];

		if (str1.length() != str2.length())
			return false;

		for (int i = 0; i < str1.length(); i++) {
			charCounter[str1.charAt(i)] = charCounter[str1.charAt(i)] + 1;
		}

		for (int i = 0; i < str2.length(); i++) {
			charCounter[str2.charAt(i)] = charCounter[str2.charAt(i)] - 1;
		}

		for (int i = 0; i < charCounter.length; i++) {
			if (charCounter[i] != 0)
				return false;
		}

		return true;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(isPermutationCharSet(bfr.readLine(), bfr.readLine()));
		bfr.close();
	}

}
