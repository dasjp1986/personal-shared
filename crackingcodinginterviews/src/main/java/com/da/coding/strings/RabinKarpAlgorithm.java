package com.da.coding.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 * 
 *         For given String and pattern, find starting index of all occurrences
 *         where pattern is found in the given string
 */
public class RabinKarpAlgorithm {
	private static final int MAX_CHARS = Character.MAX_VALUE;

	// Time Complexity- O(N-M+1)
	public List<Integer> match(String str, String pattern) {
		List<Integer> result = new ArrayList<>();
		int n = str.length();
		int m = pattern.length();

		if (n < m) {
			return result;

		}

		long patternHash = hashFunc(pattern);
		long strHash = hashFunc(str.substring(0, m));

		if (patternHash == strHash) {
			result.add(0);
		}

		for (int index = 1; index <= n - m; index++) {

			strHash = (strHash - getCode(str.charAt(index - 1)) * (long) Math.pow(MAX_CHARS, m - 1)) * MAX_CHARS
					+ getCode(str.charAt(index + m - 1));

			if (patternHash == strHash) {
				result.add(index);
			}
		}

		return result;
	}

	public static void main(String[] args) {
		RabinKarpAlgorithm algo = new RabinKarpAlgorithm();
//		System.out.println(algo.match("ababc", "abc"));
//		System.out.println(algo.match("AAAAABAAABA", "AAAA"));
//		System.out.println(algo.match("AAAAB", "AB"));
		System.out.println(algo.match("ABDAB", "AB"));
	}

	private long getCode(char ch) {
		return (ch - 'A' + 1);
	}

	private long hashFunc(String s) {
		long hash = 0;
		int power = s.length() - 1;

		for (int i = 0; i < s.length(); i++) {
			long pow = (long) Math.pow(MAX_CHARS, power--);
			long code = getCode(s.charAt(i));
			hash = hash + code * pow;
		}

		return hash;
	}

}
