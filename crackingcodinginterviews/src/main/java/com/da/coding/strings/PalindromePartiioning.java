package com.da.coding.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Given a string s, partition s such that every string of the partition
 *         is a palindrome.
 * 
 *         Return all possible palindrome partitioning of s.
 * 
 *         For example, given s = "aab", Return
 * 
 *         [ ["a","a","b"] ["aa","b"], ] Ordering the results in the answer :
 *         Entry i will come before Entry j if : len(Entryi[0]) < len(Entryj[0])
 *         OR (len(Entryi[0]) == len(Entryj[0]) AND len(Entryi[1]) <
 *         len(Entryj[1])) OR
 *
 *
 *
 *         (len(Entryi[0]) == len(Entryj[0]) AND … len(Entryi[k] <
 *         len(Entryj[k])) In the given example, ["a", "a", "b"] comes before
 *         ["aa", "b"] because len("a") < len("aa")
 */
public class PalindromePartiioning {
	public ArrayList<ArrayList<String>> partition(String a) {
		Map<Integer, ArrayList<ArrayList<String>>> map = new HashMap();
		return poop(0, a, map);
		// return map.get(0);
	}

	private ArrayList<ArrayList<String>> poop(int l, String a, Map<Integer, ArrayList<ArrayList<String>>> map) {
		if (map.containsKey(l)) {
			return map.get(l);
		}
		ArrayList<ArrayList<String>> ret = new ArrayList();
		for (int i = l + 1; i <= a.length(); i++) {
			if (isPol(a.substring(l, i))) {
				if (i < a.length()) {
					ArrayList<ArrayList<String>> li = poop(i, a, map);
					for (int j = 0; j < li.size(); j++) {
						ArrayList<String> ret2 = new ArrayList();
						ret2.add(a.substring(l, i));
						ret2.addAll(li.get(j));
						ret.add(ret2);
					}
				} else {
					ArrayList<String> ret2 = new ArrayList();
					ret2.add(a.substring(l, i));
					ret.add(ret2);
				}
			}
		}
		map.put(l, ret);
		return ret;
	}

	private boolean isPol(String a) {
		if (a.length() <= 1) {
			return true;
		}
		for (int i = 0, j = a.length() - 1; i <= j; i++, j--) {
			if (a.charAt(i) != a.charAt(j)) {
				return false;
			}
		}
		return true;
	}
}
