package com.da.coding.strings;

/**
 * 
 * @author dagraw1 Find the length of longest substring without repeating any
 *         characters
 * 
 *         Ex- abcabcbb Ans- 3 (abc)
 * 
 *         Ex- bbbb Ans- 1(b)
 * 
 *         Ex- pwwkew Ans- 3(wke)
 */
public class LongestSubstringWtReptChars {
	static int iterations = 0;

	// Time Complexity- O(n*k) where k is length of longest substring
	// Worst case O(n^2)
	public int lengthOfLongestSubstring(String inputStr) {
		iterations = 0;
		if (inputStr == null || inputStr.isEmpty()) {
			return 0;
		}
		if (inputStr.length() == 1) {
			return 1;
		}

		int start = 0, current = 1, max = 1;

		for (int i = 1; i < inputStr.length(); i++) {
			int lastIndex = isRepeat(start, i, inputStr);
			if (lastIndex == -1) {
				current++;
			} else {
				current = i - lastIndex;
				start = lastIndex + 1;
			}

			if (current > max) {
				max = current;
			}
		}
		return max;
	}

	private int isRepeat(int start, int index, String inputStr) {
		for (int i = start; i < index; i++) {
			iterations++;
			if (inputStr.charAt(i) == inputStr.charAt(index)) {
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		LongestSubstringWtReptChars problem = new LongestSubstringWtReptChars();
		System.out.println(problem.lengthOfLongestSubstring("abcabcdbb") + ", iterations:" + iterations);
		System.out.println(problem.lengthOfLongestSubstring("aaaaaaaa") + ", iterations:" + iterations);
		System.out.println(problem.lengthOfLongestSubstring("abcdeabcd") + ", iterations:" + iterations);
		System.out.println(problem.lengthOfLongestSubstring("pwwkew") + ", iterations:" + iterations);
		System.out.println(problem.lengthOfLongestSubstring("dvdf") + ", iterations:" + iterations);
	}
}
