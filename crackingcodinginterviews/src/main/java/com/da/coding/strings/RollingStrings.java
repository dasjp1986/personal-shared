package com.da.coding.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 * 
 *         for a given string and rolling operations, perform all the operations
 *         and return last updated string
 * 
 *         String contains 'a'-'z' 26 characters only
 * 
 *         one roll operation means 'a'->'b' or 'z'->'a'
 * 
 *         ex- abz, rolling [3,2,1] value in rolling operation asks to roll all
 *         the characters where k=0 and k<roll value
 * 
 *         after first operation bca after second operation cda after third
 *         operation dda
 *
 */

public class RollingStrings {
	public static String rollTheString(String s, List<Integer> roll) {
		if (s == null || s.isEmpty() || roll == null || roll.size() == 0) {
			return s;
		}

		char[] array = s.toCharArray();
		int[] r = new int[array.length];

		int count = 0;
		for (Integer rol : roll) {
			r[rol - 1] = 1;
		}

		count = r[r.length - 1];
		for (int i = r.length - 2; i >= 0; i--) {
			count = count + r[i];
			r[i] = count;
		}

		for (int i = 0; i < r.length; i++) {
			array[i] = nextChar(array[i], r[i]);
		}

		return new String(array);
	}

	private static char nextChar(char ch, int k) {
		int ascii = ch + k;
		char res = (char) ascii;
		if (ascii > 'z') {
			res = (char) ('a' + ascii - 'z' - 1);
		}
		return res;
	}

	public static void main(String[] args) {
		List<Integer> rolls = new ArrayList<>();
//		rolls.add(3);
//		rolls.add(2);
//		rolls.add(1);
//
//		System.out.println(rollTheString("abz", rolls));

		rolls = new ArrayList<>();
		rolls.add(1);
		rolls.add(4);

		System.out.println(rollTheString("xyvz", rolls));
	}
}
