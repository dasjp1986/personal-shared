package com.da.coding.strings;

/**
 * 
 * @author dagraw1
 *
 *         Given a string, find length of the longest substring with all
 *         distinct characters. For example, for input "abca", the output is 3
 *         as "abc" is the longest substring with all distinct characters.
 * 
 *         Input:
 * 
 *         The first line of input contains an integer T denoting the number of
 *         test cases. The first line of each test case is String str.
 * 
 *         Output:
 * 
 *         Print length of smallest substring with maximum number of distinct
 *         characters. Note: The output substring should have all distinct
 *         characters.
 * 
 *         Constraints:
 * 
 *         1 ≤ T ≤ 100 1 ≤ size of str ≤ 10000
 * 
 *         Example:
 * 
 *         Input 2 abababcdefababcdab geeksforgeeks
 * 
 * 
 *         Output: 6 7
 */
public class LongestDistinctChars {
	
	//Time Complexity- O(n)
	public static int longest(String str) {
		boolean[] visited = new boolean[256];
		int max = 1;
		int start = 0;

		visited[str.charAt(0)] = true;
		for (int end = 1; end < str.length(); end++) {
			if (visited[str.charAt(end)]) {
				start++;
			} else {
				int count = end - start + 1;
				if (count > max) {
					max = count;
				}
				visited[str.charAt(end)] = true;
			}
		}
		return max;
	}

	public static void main(String[] args) {
		System.out.println(longest("aaaaaa"));
	}
}
