package com.da.coding.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         For given String and pattern, find starting index of all occurrences
 *         where pattern is found in the given string
 */

public class KMPAlgorithm {

	// Time Complexity- O(M+N)
	public List<Integer> match(String str, String pattern) {
		int i = 0, j = 0;

		List<Integer> result = new ArrayList<>();
		int[] lps = lps(str);

		while (i < str.length() && j < pattern.length()) {
			if (str.charAt(i) == pattern.charAt(j)) {
				i++;
				j++;

				if (j == pattern.length()) {
					result.add(i - j);
					j = lps[j - 1];
				}
			} else {
				if (j > 0) {
					j = lps[j - 1];
				} else {
					i++;
				}
			}

		}

		return result;
	}

	public int[] lps(String str) {
		int[] lps = new int[str.length()];

		int len = 0;
		int i = 1;

		while (i < str.length()) {
			if (str.charAt(len) == str.charAt(i)) {
				len++;
				lps[i] = len;
				i++;
			} else {
				if (len != 0) {
					len = lps[len - 1];
				} else {
					lps[i] = len;
					i++;
				}
			}
		}

		return lps;

	}

	public static void main(String[] args) {
		KMPAlgorithm algo = new KMPAlgorithm();
//		System.out.println(Arrays.toString(algo.lps("AAAA")));
//		System.out.println(Arrays.toString(algo.lps("ABCDE")));
//		System.out.println(Arrays.toString(algo.lps("AABAACAABAA")));
//		System.out.println(Arrays.toString(algo.lps("AAACAAAAAC")));
//		System.out.println(Arrays.toString(algo.lps("AAABAAA")));

		System.out.println(algo.match("AAAAABAAABA", "AAAA"));
		System.out.println(algo.match("ABABDABACDABABCABAB", "ABABCABAB"));
		System.out.println(algo.match("AAAAB", "AB"));

	}

}
