package com.da.coding.strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.da.coding.sort.MergeSort;

/**
 * 
 * @author dagraw1
 *
 *         Find if the given String of size-N has all the characters unique
 *         
 *         Ex- 
 *         dharmendra
 *         false
 *         
 *         abcv
 *         true
 */

public class AllUniqueChars {
	// Time Complexity- O(NlogN), Space Complexity- same as sorting algorithm
	// used
	public static boolean isUniqueUsingSorting(String stringInput) {
		// Time Complexity- O(NlogN)
		String sortedInput = MergeSort.sortString(stringInput);
		// O(N)
		for (int i = 1; i < sortedInput.length(); i++) {
			if (sortedInput.charAt(i) == sortedInput.charAt(i - 1))
				return false;
		}
		return true;
	}

	// Time Complexity- O(N), Space Complexity- O(C)~O(1)
	public static boolean isUniqueUsingCharset(String stringInput) {
		// ASCII chars size is 128

		if (stringInput.length() > 128)
			return false;

		boolean[] isCharSet = new boolean[128];
		for (int i = 0; i < stringInput.length(); i++) {
			int charIndex = stringInput.charAt(i);
			if (isCharSet[charIndex]) {
				return false;
			}
			isCharSet[charIndex] = true;
		}

		return true;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		String stringInput = bfr.readLine();
		System.out.println("Sorting- is all chars unique : " + isUniqueUsingSorting(stringInput));
		System.out.println("Charset- is all chars unique : " + isUniqueUsingCharset(stringInput));
		bfr.close();
	}
}
