package com.da.coding.strings;

import com.da.coding.commons.SystemInputReaderUtil;
/**
 * 
 * @author dagraw1
 * Given two string str1 and str2 return true if String str2 is single rotated string of the str1
 * Ex
 * waterbottle erbottlewat
 * true
 * 
 * Time Complexity= O(str1.lengh()+str2.length())->O(N)
 * Space Complexity= O(1)
 */
public class RotatedSubstring {
	public static boolean isRotatedSubstring(String str, String rotatedStr) {

		int i = 0, j = 0;
		int matchPoint = 0;
		while (i < str.length() && j < rotatedStr.length()) {
			if (str.charAt(i) == rotatedStr.charAt(j)) {
				matchPoint = i;
				while (i < str.length() && j < rotatedStr.length() && str.charAt(i) == rotatedStr.charAt(j)) {
					i++;
					j++;
				}

				int k = 0;
				while (k < matchPoint && j < rotatedStr.length()) {
					if (str.charAt(k) != rotatedStr.charAt(j))
						return false;
					k++;
					j++;
				}
			} else {
				i++;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		String[] inputStrings = SystemInputReaderUtil.readString().split(" ");
		System.out.println(isRotatedSubstring(inputStrings[0], inputStrings[1]));
	}
}
