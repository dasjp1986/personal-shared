package com.da.coding.strings;

public class MincharsToPalindrome {

    public int solve(String A) {
        return palindrome(A,0,A.length()-1);
    }
    
    private int palindrome(String str, int start, int end){
        if(start<end){
            if(str.charAt(start)==str.charAt(end)){
                return palindrome(str,start+1,end-1);
            }
            return 1+Math.min(palindrome(str,start+1,end),palindrome(str,start,end-1));
        }
        return 0;
    }

}
