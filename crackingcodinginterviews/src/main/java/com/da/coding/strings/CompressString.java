package com.da.coding.strings;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1
 *  Compress the given string and return compressed string if its size is lesser than the original String
 *  
 *  Ex-
 *  aaabbcc->a3b2c2
 *  aabcdf->aabcdf
 *
 */

public class CompressString {
	public static String compress(String inputString) {
		char[] resultAray = new char[inputString.length()];
		int i = 0, j = 0, k = 0, count = 0;

		while (j < resultAray.length) {
			while (j < resultAray.length && inputString.charAt(i) == inputString.charAt(j)) {
				count++;
				j++;
			}
			String tempStr = inputString.substring(i, i + 1) + count;
			if (k + tempStr.length() < resultAray.length) {
				for (int l = 0; l < tempStr.length(); l++) {
					resultAray[k++] = tempStr.charAt(l);
				}
			} else {
				// compressed string takes more space than original
				return inputString;
			}

			i = j;
			count = 0;
		}
		return new String(resultAray);
	}

	public static void main(String[] args) {
		System.out.println(compress(SystemInputReaderUtil.readString()));
	}
}
