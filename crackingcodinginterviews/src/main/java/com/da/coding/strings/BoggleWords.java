package com.da.coding.strings;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         Given a dictionary, a method to do lookup in dictionary and a M x N
 *         board where every cell has one character. Find all possible words
 *         that can be formed by a sequence of adjacent characters. Note that we
 *         can move to any of 8 adjacent characters, but a word should not have
 *         multiple instances of same cell.
 * 
 *         Example:
 * 
 *         Input: dictionary[] = {"GEEKS", "FOR", "QUIZ", "GO"}; boggle[][] =
 *         {{'G','I','Z'}, {'U','E','K'}, {'Q','S','E'}}; isWord(str): returns
 *         true if str is present in dictionary else false.
 * 
 *         Output: Following words of the dictionary are present GEEKS QUIZ
 */
public class BoggleWords {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			int[] nums = new int[n];
			for (int i = 0; i < n; i++) {
				nums[i] = scn.nextInt();
			}
		}
		scn.close();

		
		//using trie and dfs can solve this question
		//time complexity= O(M*N*largest word length)
	}
}
