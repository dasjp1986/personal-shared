package com.da.coding.strings;

public class CountAndSay {
	public String countAndSay(int A) {
		
		StringBuilder builder = new StringBuilder();
		builder.append(1);
		
		if(A==1){
			return builder.toString();
		}
		
		StringBuilder result = new StringBuilder();

		for (int j = 1; j < A; j++) {
			int i = 0;
			result = new StringBuilder();
			while (i < builder.length()) {
				int count = 0;
				int index = i;
				while (i < builder.length() && builder.charAt(index) == builder.charAt(i)) {
					count++;
					i++;
				}
				result.append(count);
				result.append(builder.charAt(index));
			}
			builder = new StringBuilder(result.toString());
		}

		return result.toString();
	}
	
	public static void main(String[] args) {
		CountAndSay countAndSay= new CountAndSay();
		System.out.println(countAndSay.countAndSay(4));
	}
}
