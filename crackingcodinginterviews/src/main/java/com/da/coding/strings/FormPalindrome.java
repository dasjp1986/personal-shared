package com.da.coding.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 * 
 *         Given a string, find the minimum number of characters to be inserted
 *         to convert it to palindrome.
 * 
 *         Before we go further, let us understand with few examples: ab: Number
 *         of insertions required is 1. bab aa: Number of insertions required is
 *         0. aa abcd: Number of insertions required is 3. dcbabcd abcda: Number
 *         of insertions required is 2. adcbcda which is same as number of
 *         insertions in the substring bcd(Why?). abcde: Number of insertions
 *         required is 4. edcbabcde
 */
public class FormPalindrome {
	static int iterations = 0;

	//Time and Space Complexity- O(n^2)
	public static int form(String str) {
		Map<Key, Integer> cache = new HashMap<>();
		return find(str, 0, str.length() - 1,cache);
	}

	private static int find(String str, int start, int end, Map<Key, Integer> cache) {
		if (start >= end) {
			return 0;
		}
		Key key = new Key(start, end);
		Integer value = cache.get(key);
		if(value!=null){
			return value;
		}
		iterations++;
		if (str.charAt(start) == str.charAt(end)) {
			return find(str, start + 1, end - 1,cache);
		}
		value= 1 + Math.min(find(str, start, end - 1,cache), find(str, start + 1, end,cache));
		cache.put(key, value);
		return value;
	}

	public static void main(String[] args) {
		System.out.println(form("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"));
		System.out.println(iterations);
	}
}

class Key {
	Integer i;
	Integer j;

	public Key(int i, int j) {
		this.i = i;
		this.j = j;
	}

	@Override
	public boolean equals(Object obj) {
		Key key = (Key) obj;
		return this.i.equals(key.i) && this.j.equals(key.j);
	}

	@Override
	public int hashCode() {
		return i.hashCode() + j.hashCode();
	}
}
