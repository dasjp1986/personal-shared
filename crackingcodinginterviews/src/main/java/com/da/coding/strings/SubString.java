package com.da.coding.strings;

public class SubString {
	public static int strStr(final String A, final String B) {
		if (A.isEmpty() || B.isEmpty()) {
			return -1;
		}

		int i = 0, j = 0, index = -1;
		while (i < A.length() && j < B.length()) {
			if (A.charAt(i) == B.charAt(j)) {
				if ((A.length()-i)<(B.length()- j)) {
					return -1;
				}
				index = i;
				while (i < A.length() && j < B.length()) {
					if (A.charAt(i) == B.charAt(j)) {
						i++;
						j++;
					}else{
						i= index;
						j=0;
						break;
					}
					
				}
				if(j==B.length()){
					return index;
				}
			}
			i++;
		}

		return -1;
	}

	public static void main(String[] args) {
		System.out.println(strStr("bbaabbbbbaabbaabbbbbbabbbabaabbbabbabbbbababbbabbabaaababbbaabaaaba", "babaaa"));
	}

}
