package com.da.coding.strings;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1
 * Given two strings as an input, find if there are just one
 *         operation away one operation represent one of these operations
 *         replace- abc->axb 
 *         remove- abcd->acd 
 *         add- acd->abcd
 *         
 *         Time Complexity- O(N), Space Complexity- O(1)
 */
public class StringOneOperationAway {
	public static boolean isOneAway(String str1, String str2) {

		if (Math.abs(str1.length() - str2.length()) > 1) {
			return false;
		}

		// replace
		if (str1.length() == str2.length()) {
			return replace(str1, str2);
		}
		// add or remove
		if (str1.length() > str2.length()) {
			return addOrRemove(str1, str2, true, str2.length());
		} else {
			return addOrRemove(str1, str2, false, str1.length());
		}

	}

	private static boolean replace(String str1, String str2) {
		boolean isOneAway= false;
		for (int i = 0; i < str1.length(); i++) {
			if (str1.charAt(i) != str2.charAt(i)) {
				if (isOneAway)
					return false;
				isOneAway = true;
			}
		}
		return isOneAway;
	}

	private static boolean addOrRemove(String str1, String str2, boolean isAdd, int length) {
		int j = 0;
		boolean isOneAway= false;
		for (int i = 0; i < length; i++) {
			if (str1.charAt(j) != str2.charAt(i)) {
				if (isOneAway)
					return false;
				isOneAway = true;
				if(isAdd)
					j++;
				else
					j--;
			}
			j++;
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(isOneAway(SystemInputReaderUtil.readString(), SystemInputReaderUtil.readString()));
	}
}
