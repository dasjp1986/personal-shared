package com.da.coding.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         International Morse Code defines a standard encoding where each
 *         letter is mapped to a series of dots and dashes, as follows: "a" maps
 *         to ".-", "b" maps to "-...", "c" maps to "-.-.", and so on.
 * 
 *         For convenience, the full table for the 26 letters of the English
 *         alphabet is given below:
 * 
 *         [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]
 *         Now, given a list of words, each word can be written as a
 *         concatenation of the Morse code of each letter. For example, "cab"
 *         can be written as "-.-.-....-", (which is the concatenation "-.-." +
 *         "-..." + ".-"). We'll call such a concatenation, the transformation
 *         of a word.
 * 
 *         Return the number of different transformations among all words we
 *         have.
 * 
 *         Example: Input: words = ["gin", "zen", "gig", "msg"] Output: 2
 *         Explanation: The transformation of each word is: "gin" -> "--...-."
 *         "zen" -> "--...-." "gig" -> "--...--." "msg" -> "--...--."
 * 
 *         There are 2 different transformations, "--...-." and "--...--.".
 */
public class UniqueMorseCodes {
	public int uniqueMorseRepresentations(String[] words) {

		if (words == null || words.length == 0) {
			return 0;
		}
		Map<Character, String> morseMap = createMap();
		return transform(words, morseMap);
	}

	private int transform(String[] words, Map<Character, String> morseMap) {
		Map<String, Integer> transMap = new HashMap<>();
		StringBuilder builder = null;
		for (String word : words) {
			builder = new StringBuilder();
			for (int i = 0; i < word.length(); i++) {
				builder.append(morseMap.get(word.charAt(i)));
			}
			Integer c = transMap.get(builder.toString());
			if (c == null) {
				c = 0;
			}
			transMap.put(builder.toString(), c + 1);
		}

		return transMap.keySet().size();
	}

	private Map<Character, String> createMap() {
		Map<Character, String> map = new HashMap<>();
		String[] morses = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
				"-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.." };
		char ch = 'a';
		for (int i = 0; i < 26; i++) {
			map.put(ch, morses[i]);
			ch = (char) (ch + 1);
		}
		return map;
	}

	public static void main(String[] args) {
		UniqueMorseCodes problem = new UniqueMorseCodes();
		String[] words = { "gin", "zen", "gig", "msg" };
		System.out.println(problem.uniqueMorseRepresentations(words));

	}

}
