package com.da.coding.sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * 
 * @author dagraw1 Time Complexity- N(logN)
 *
 */

public class MergeSort {

	public static int[] sortInt(int[] intArray) {
		return mergeSortInt(0, intArray.length - 1, intArray);
	}

	public static char[] sortChars(char[] charArray) {
		return mergeSortChars(0, charArray.length - 1, charArray);
	}

	public static String sortString(String stringInput) {
		return mergeSortString(0, stringInput.length() - 1, stringInput).toString();
	}

	private static int[] mergeSortInt(int start, int end, int[] inputArray) {
		if (start >= end) {
			return new int[] { inputArray[start] };
		}

		int mid = (start + end) / 2;

		int[] leftArray = mergeSortInt(start, mid, inputArray);
		int[] rightArray = mergeSortInt(mid + 1, end, inputArray);

		int[] resultArray = new int[leftArray.length + rightArray.length];
		int l = 0, r = 0, k = 0;

		while (l < leftArray.length && r < rightArray.length) {
			if (leftArray[l] < rightArray[r]) {
				resultArray[k] = leftArray[l];
				l++;
			} else {
				resultArray[k] = rightArray[r];
				r++;
			}
			k++;
		}

		while (l < leftArray.length) {
			resultArray[k++] = leftArray[l++];
		}

		while (r < rightArray.length) {
			resultArray[k++] = rightArray[r++];
		}

		return resultArray;
	}

	private static char[] mergeSortChars(int start, int end, char[] inputArray) {
		if (start >= end) {
			return new char[] { inputArray[start] };
		}

		int mid = (start + end) / 2;

		char[] leftArray = mergeSortChars(start, mid, inputArray);
		char[] rightArray = mergeSortChars(mid + 1, end, inputArray);

		char[] resultArray = new char[leftArray.length + rightArray.length];
		int l = 0, r = 0, k = 0;

		while (l < leftArray.length && r < rightArray.length) {
			if (leftArray[l] < rightArray[r]) {
				resultArray[k] = leftArray[l];
				l++;
			} else {
				resultArray[k] = rightArray[r];
				r++;
			}
			k++;
		}

		while (l < leftArray.length) {
			resultArray[k++] = leftArray[l++];
		}

		while (r < rightArray.length) {
			resultArray[k++] = rightArray[r++];
		}

		return resultArray;
	}

	private static StringBuilder mergeSortString(int start, int end, String stringInput) {
		if (start >= end) {
			return new StringBuilder(stringInput.substring(start, start + 1));
		}

		int mid = (start + end) / 2;

		StringBuilder leftBuilder = mergeSortString(start, mid, stringInput);
		StringBuilder rightBuilder = mergeSortString(mid + 1, end, stringInput);

		StringBuilder resultBuilder = new StringBuilder();
		int l = 0, r = 0;

		while (l < leftBuilder.length() && r < rightBuilder.length()) {
			if (leftBuilder.charAt(l) < rightBuilder.charAt(r)) {
				resultBuilder.append(leftBuilder.charAt(l));
				l++;
			} else {
				resultBuilder.append(rightBuilder.charAt(r));
				r++;
			}
		}

		if (l < leftBuilder.length()) {
			resultBuilder.append(leftBuilder.substring(l));
		}

		if (r < rightBuilder.length()) {
			resultBuilder.append(rightBuilder.substring(r));
		}

		return resultBuilder;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter integer array...");
		String[] stringArray = bfr.readLine().split(" ");
		int[] intArray = new int[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			intArray[i] = Integer.parseInt(stringArray[i]);
		}
		System.out.println("Sorted Integer Array : " + Arrays.toString(sortInt(intArray)));

		System.out.println("Enter character array...");
		stringArray = bfr.readLine().split(" ");
		char[] charArray = new char[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			charArray[i] = stringArray[i].charAt(0);
		}
		System.out.println("Sorted Integer Array : " + Arrays.toString(sortChars(charArray)));

		System.out.println("Enter a String ...");
		System.out.println("Sorted String : " + sortString(bfr.readLine()));

		bfr.close();

	}
}
