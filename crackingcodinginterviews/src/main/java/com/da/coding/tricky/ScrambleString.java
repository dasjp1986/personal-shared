package com.da.coding.tricky;

/**
 * 
 * @author dagraw1 Given a string s1, we may represent it as a binary tree by
 *         partitioning it to two non-empty substrings recursively.
 * 
 *         Below is one possible representation of s1 = “great”:
 * 
 * 
 *         great / \ gr eat / \ / \ g r e at / \ a t
 * 
 *         To scramble the string, we may choose any non-leaf node and swap its
 *         two children.
 * 
 *         For example, if we choose the node “gr” and swap its two children, it
 *         produces a scrambled string “rgeat”.
 * 
 *         rgeat / \ rg eat / \ / \ r g e at / \ a t We say that “rgeat” is a
 *         scrambled string of “great”.
 * 
 *         Similarly, if we continue to swap the children of nodes “eat” and
 *         “at”, it produces a scrambled string “rgtae”.
 * 
 *         rgtae / \ rg tae / \ / \ r g ta e / \ t a We say that “rgtae” is a
 *         scrambled string of “great”.
 * 
 *         Given two strings s1 and s2 of the same length, determine if s2 is a
 *         scrambled string of s1. Return 0/1 for this problem.
 *
 */
public class ScrambleString {
	public int isScramble(final String strA, final String strB) {
		if (strA == null || strB == null) {
			return 0;
		} else if (strA.length() != strB.length()) {
			return 0;
		} else if (strA.equals(strB)) {
			return 1;
		}

		int length = strA.length();

		boolean[][][] scrambled = new boolean[length][length][1 + length];

		for (int k = 1; k <= length; k++) {
			for (int i = 0; i + k <= length; i++) {
				for (int j = 0; j + k <= length; j++) {
					if (k == 1) {
						scrambled[i][j][k] = strA.charAt(i) == strB.charAt(j);
					} else {
						for (int q = 1; q < k && !scrambled[i][j][k]; q++) {
							scrambled[i][j][k] = (scrambled[i][j][q] && scrambled[i + q][j + q][k - q])
									|| (scrambled[i][j + k - q][q] && scrambled[i + q][j][k - q]);
						}
					}
				}
			}
		}

		return scrambled[0][0][length] ? 1 : 0;
	}

	public static void main(String[] args) {
		ScrambleString sr = new ScrambleString();
		System.out.println(sr.isScramble("great", "rgeat"));

	}

}
