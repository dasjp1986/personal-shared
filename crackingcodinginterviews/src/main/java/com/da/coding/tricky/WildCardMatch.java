package com.da.coding.tricky;

/**
 * 
 * @author dagraw1
 *
 *         Implement wildcard pattern matching with support for '?' and '*'.
 * 
 *         '?' : Matches any single character. '*' : Matches any sequence of
 *         characters (including the empty sequence). The matching should cover
 *         the entire input string (not partial).
 * 
 *         The function prototype should be:
 * 
 *         int isMatch(const char *s, const char *p) Examples :
 * 
 *         isMatch("aa","a") → 0 isMatch("aa","aa") → 1 isMatch("aaa","aa") → 0
 *         isMatch("aa", "*") → 1 isMatch("aa", "a*") → 1 isMatch("ab", "?*") →
 *         1 isMatch("aab", "c*a*b") → 0 Return 1/0 for this problem.
 */
public class WildCardMatch {

	// DO NOT MODIFY THE ARGUMENTS WITH "final" PREFIX. IT IS READ ONLY

	public int isMatch(final String s, final String p) {

		char[] str = s.toCharArray();
		char[] pattern = p.toCharArray();

		// replace multiple * with one *
		// e.g a**b***c --> a*b*c
		int writeIndex = 0;
		boolean isFirst = true;
		for (int i = 0; i < pattern.length; i++) {
			if (pattern[i] == '*') {
				if (isFirst) {
					pattern[writeIndex++] = pattern[i];
					isFirst = false;
				}
			} else {
				pattern[writeIndex++] = pattern[i];
				isFirst = true;
			}
		}

		boolean T[][] = new boolean[str.length + 1][writeIndex + 1];

		if (writeIndex > 0 && pattern[0] == '*') {
			T[0][1] = true;
		}

		T[0][0] = true;

		for (int i = 1; i < T.length; i++) {
			for (int j = 1; j < T[0].length; j++) {
				if (pattern[j - 1] == '?' || str[i - 1] == pattern[j - 1]) {
					T[i][j] = T[i - 1][j - 1];
				} else if (pattern[j - 1] == '*') {
					T[i][j] = T[i - 1][j] || T[i][j - 1];
				}
			}
		}

		return T[str.length][writeIndex] ? 1 : 0;

	}
//	
//	public static String removeStars(String s) {
//		StringBuilder b= new StringBuilder();
//		b.append(s.charAt(0));
//		for (int i = 1; i < s.length(); i++) {
//			if(s.charAt(i)==s.charAt(i-1) && s.charAt(i)=='*') {
//				continue;
//			}
//			b.append(s.charAt(i));
//		}
//		
//		return b.toString();
//	}

	public static void main(String[] args) {
		WildCardMatch reg = new WildCardMatch();
		System.out.println("0=" + reg.isMatch("aabbaaabbbaa", "a*bbb*aaa"));
		System.out.println("1=" + reg.isMatch("bacb", "b**c*?*"));
		System.out.println("0=" + reg.isMatch("bcaccbabaa", "bb*c?c*?"));
		System.out.println("0=" + reg.isMatch("acba", "*?b*a*ba*"));
		System.out.println("1=" + reg.isMatch("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "*"));
	}

}
