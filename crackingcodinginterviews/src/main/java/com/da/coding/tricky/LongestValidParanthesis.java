package com.da.coding.tricky;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given a string containing just the characters '(' and ')', find the
 *         length of the longest valid (well-formed) parentheses substring.
 * 
 *         For "(()", the longest valid parentheses substring is "()", which has
 *         length = 2.
 * 
 *         Another example is ")()())", where the longest valid parentheses
 *         substring is "()()", which has length = 4.
 * 
 */
public class LongestValidParanthesis {

	public int longestValidParentheses(String A) {

		Stack<Integer> stk = new Stack<Integer>();
		stk.push(-1);
		int res = 0;
		for (int i = 0; i < A.length(); i++) {
			char c = A.charAt(i);
			if (c == '(') {
				stk.push(i);
			} else {
				stk.pop();
				if (!stk.empty())
					res = Math.max(res, i - stk.peek());
				if (stk.empty()) {
					stk.push(i);
				}
			}

		}
		return res;

	}

	public static void main(String[] args) {
		LongestValidParanthesis longPar = new LongestValidParanthesis();
		System.out.println(longPar.longestValidParentheses(")((()))))))))("));
	}

}
