package com.da.coding.tricky;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Say you have an array for which the ith element is the price of a
 *         given stock on day i.
 * 
 *         Design an algorithm to find the maximum profit. You may complete at
 *         most two transactions.
 * 
 *         Note: You may not engage in multiple transactions at the same time
 *         (ie, you must sell the stock before you buy again).
 * 
 *         Example :
 * 
 *         Input : [1 2 1 2] Output : 2
 * 
 *         Explanation : Day 1 : Buy Day 2 : Sell Day 3 : Buy Day 4 : Sell
 * 
 * 
 */

public class BuySell2Transactions {
	public int maxProfit(final int[] A) {

		if (A.length == 0)
			return 0;

		int fB = Integer.MIN_VALUE;
		int fS = 0;
		int sB = Integer.MIN_VALUE;
		int sS = 0;

		for (int curPrice : A) {
			fB = Math.max(fB, -curPrice);
			fS = Math.max(fS, curPrice + fB);
			sB = Math.max(sB, fS - curPrice);
			sS = Math.max(sS, curPrice + sB);
		}

		return sS;

	}

	public static void main(String[] args) {
		BuySell2Transactions buySell3 = new BuySell2Transactions();
		System.out.println(buySell3.maxProfit(new int[] { 1, 4, 1, 5, 1, 6 }));
	}

}
