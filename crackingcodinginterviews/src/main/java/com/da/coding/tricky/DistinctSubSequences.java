package com.da.coding.tricky;

/**
 * 
 * @author dagraw1
 *
 *
 *         Given two sequences S, T, count number of unique ways in sequence S,
 *         to form a subsequence that is identical to the sequence T.
 * 
 *         Subsequence : A subsequence of a string is a new string which is
 *         formed from the original string by deleting some (can be none ) of
 *         the characters without disturbing the relative positions of the
 *         remaining characters. (ie, "ACE" is a subsequence of "ABCDE" while
 *         "AEC" is not). Example :
 * 
 *         S = "rabbbit" T = "rabbit" Return 3. And the formations as follows:
 * 
 *         S1= "ra_bbit" S2= "rab_bit" S3="rabb_it" "_" marks the removed
 *         character.
 */
public class DistinctSubSequences {
	public int numDistinct(String str, String seq) {
		int[][] cache = new int[str.length() + 1][seq.length() + 1];

		for (int i = 0; i <= str.length(); i++) {
			cache[i][0] = 1;
		}

		for (int r = 1; r < cache.length; r++) {
			for (int c = 1; c < cache[r].length; c++) {
				if (str.charAt(r - 1) == seq.charAt(c - 1)) {
					cache[r][c] = cache[r - 1][c - 1] + cache[r - 1][c];
				} else {
					cache[r][c] = cache[r - 1][c];
				}
			}
		}

		return cache[str.length()][seq.length()];

	}

	public static void main(String[] args) {
		DistinctSubSequences distinct = new DistinctSubSequences();
		System.out.println(distinct.numDistinct("rabbbit", "rabbit"));
	}
}
