package com.da.coding.tricky;

/**
 * 
 * @author dagraw1
 *
 *         Implement regular expression matching with support for '.' and '*'.
 * 
 *         '.' Matches any single character. '*' Matches zero or more of the
 *         preceding element.
 * 
 *         The matching should cover the entire input string (not partial).
 * 
 *         The function prototype should be:
 * 
 *         int isMatch(const char *s, const char *p) Some examples:
 * 
 *         isMatch("aa","a") → 0 isMatch("aa","aa") → 1 isMatch("aaa","aa") → 0
 *         isMatch("aa", "a*") → 1 isMatch("aa", ".*") → 1 isMatch("ab", ".*") →
 *         1 isMatch("aab", "c*a*b") → 1 Return 0 / 1 ( 0 for false, 1 for true
 *         ) for this problem
 */
public class RegularExpMatch {

	public int isMatch(final String s, final String p) {
		int m = s.length();
		int n = p.length();
		boolean[][] t = new boolean[m + 1][n + 1];
		t[0][0] = true;
		// pattern of type a*b* can match to empty string;
		for (int j = 1; j < t[0].length; j++) {
			if (p.charAt(j - 1) == '*') {
				t[0][j] = t[0][j - 2];
			}
		}

		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				if (s.charAt(i - 1) == p.charAt(j - 1) || p.charAt(j - 1) == '.') {
					t[i][j] = t[i - 1][j - 1];
				} else if (p.charAt(j - 1) == '*') {
					t[i][j] = t[i][j - 2];
					if (s.charAt(i - 1) == p.charAt(j - 2) || p.charAt(j - 2) == '.') {
						t[i][j] = t[i][j] || t[i - 1][j];
					}
				} else {
					t[i][j] = false;
				}
			}
		}

		return (t[m][n]) ? 1 : 0;
	}

}
