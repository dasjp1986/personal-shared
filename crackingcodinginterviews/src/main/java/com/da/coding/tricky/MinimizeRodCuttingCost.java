package com.da.coding.tricky;

import java.util.ArrayList;
import java.util.List;

public class MinimizeRodCuttingCost {

	int minCost = Integer.MAX_VALUE;
	List<Integer> finalRes;

	public int[] rodCut(int A, int[] B) {

		int[] result = new int[B.length];
		List<Integer> points = new ArrayList<>();
		cuts(0, 0, A, points, new ArrayList<>());
		return result;
	}

	private void cuts(int cost, int start, int end, List<Integer> points, List<Integer> res) {

		if (points.isEmpty()) {
			if (cost < minCost) {
				finalRes = res;
			}
			return;
		}

		if (points.get(points.size() - 1) < start || points.get(0) > end) {
			return;
		}

		cost = cost + end - start;
		List<Integer> pointsClone = new ArrayList<>(points);
		List<Integer> resClone = new ArrayList<>(res);
		
		for(Integer point: points) {
			pointsClone.remove(point);
			resClone.add(point);
			
		}
	}

}
