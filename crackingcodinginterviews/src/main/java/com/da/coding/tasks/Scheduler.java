package com.da.coding.tasks;

import java.util.Timer;
import java.util.TimerTask;

public class Scheduler {
	public void schedule() {
		TimerTask task= new TimerTaskEx();
		Timer timer= new Timer(false);
		//timer.schedule(task, 1000);
		//timer.scheduleAtFixedRate(task, 1000, 2000);
		
	}
	
	public static void main(String[] args) {
		Scheduler scheduler= new Scheduler();
		scheduler.schedule();
	}
}

class TimerTaskEx extends TimerTask {

	@Override
	public void run() {

		System.out.println("entered timetask");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("completed timetask");
	}

}
