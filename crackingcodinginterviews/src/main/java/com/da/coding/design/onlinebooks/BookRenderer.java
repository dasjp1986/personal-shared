package com.da.coding.design.onlinebooks;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class BookRenderer {
	private static Scanner scanner = new Scanner(System.in);

	public int render(BookSocial bookSocial) {
		int pageNbr=0;
		System.out.println("playing book content....press n for next page or e to exit");
		System.out.println(bookSocial);
		Random random= new Random();

		do {
			for (int line = 0; line < 20; line++) {
				for (int j = 0; j < 10; j++) {
					System.out.print(UUID.randomUUID().toString().substring(0, j+ random.nextInt(23)));
				}
				System.out.println();
			}
			pageNbr++;
			System.out.println("press n for next page or e to exit");
		} while (!scanner.next().equals("e"));

		return pageNbr;
	}
}
