package com.da.coding.design.callcenter;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Conversation implements Runnable {

	private Employee employee;
	
	private static final Logger LOGGER= LoggerFactory.getLogger(Conversation.class);

	public Conversation(Employee employee) {
		this.employee = employee;
	}

	@Override
	public void run() {
		LOGGER.debug("Conversation is in progress, started by employee : {}", employee);
		int converseTime = new Random().nextInt(5000);
		try {
			Thread.sleep(converseTime);
		} catch (InterruptedException e) {
		}
		LOGGER.debug("Conversation is completed by employee : {}" , employee);
		HelpLine.getInstance().update(employee, true);
	}

}
