package com.da.coding.design.onlinebooks;

public enum BookCategory {
	ACADEMIC, NOVEL, NON_FICTIONAL, ROMANACE, FINANCE, TECHNOLOGY, FICTIONAL
}
