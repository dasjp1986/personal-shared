package com.da.coding.design.onlinebooks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//Singleton
public class BookSocialManager {
	private Map<String, BookSocial> bookSocials;

	public BookSocialManager() {
		bookSocials = new ConcurrentHashMap<>();
	}

	public BookSocial add(String bookCode) {
		BookSocial bookSocial = new BookSocial(bookCode);
		bookSocials.put(bookSocial.getBookSocialCode(), bookSocial);
		return bookSocial;
	}

	public void remove(String bookSocialCode) {
		bookSocials.remove(bookSocialCode);
	}

	public BookSocial getBookSocial(String bookCode) {
		return bookSocials.get(bookCode);
	}

	public List<BookSocial> findTopByRating(List<Book> books, int top) {
		List<BookSocial> results = new ArrayList<>();
		if (books != null) {
			List<BookSocial> socials = new ArrayList<>();
			for (Book book : books) {
				socials.add(bookSocials.get(book.getBookSocialCode()));
			}
			Collections.sort(socials, (BookSocial s1, BookSocial s2) -> {
				return s2.getRating().compareTo(s1.getRating());
			});
			if (socials.size() <= top) {
				results = socials;
			} else {
				for (BookSocial social : socials) {
					results.add(social);
					if (results.size() == top) {
						break;
					}
				}
			}
		}
		return results;
	}

}
