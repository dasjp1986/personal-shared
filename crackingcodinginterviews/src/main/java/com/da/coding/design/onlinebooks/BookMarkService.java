package com.da.coding.design.onlinebooks;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

//Singleton
public class BookMarkService {
	private Map<String, Set<BookMark>> bookMarks;

	public BookMarkService() {
		this.bookMarks = new ConcurrentHashMap<>();
	}

	public void addBookMark(String userId, Book book, int pageNbr) {
		Set<BookMark> set = bookMarks.get(userId);
		if (set == null) {
			set = new HashSet<>();
			bookMarks.put(userId, set);
		}
		set.add(new BookMark(book.getBookCode(), book.getTitle(), pageNbr));
		System.out.println(
				"Added bookmark : userId-" + userId + ", bookCode-" + book.getBookCode() + ", pageNbr-" + pageNbr);
	}

	public void remove(String userId, Book book) {
		Set<BookMark> set = bookMarks.get(userId);
		if (set != null) {
			set.remove(new BookMark(book.getBookCode()));
			if (set.isEmpty()) {
				bookMarks.remove(userId);
			}
		}
	}
}
