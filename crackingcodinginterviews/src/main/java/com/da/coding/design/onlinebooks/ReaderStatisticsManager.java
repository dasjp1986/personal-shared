package com.da.coding.design.onlinebooks;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//Singleton
public class ReaderStatisticsManager {
	private Map<String, ReaderStatistics> stats;

	public ReaderStatisticsManager() {
		this.stats = new ConcurrentHashMap<>();
	}

	public void save(ReaderStatistics statistics) {
		stats.put(statistics.getUserId(), statistics);
	}

	public void remove(String userId) {
		stats.remove(userId);
	}

	public ReaderStatistics load(String userId) {
		return stats.get(userId);
	}
}
