package com.da.coding.design.onlinebooks;

public class BookMark {
	private String bookCode;
	private String bookTitle;
	private int pageNbr;

	public BookMark(String bookCode, String bookTitle, int pageNbr) {
		super();
		this.bookCode = bookCode;
		this.bookTitle = bookTitle;
		this.pageNbr = pageNbr;
	}

	public BookMark(String bookCode) {
		super();
		this.bookCode = bookCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookCode == null) ? 0 : bookCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookMark other = (BookMark) obj;
		if (bookCode == null) {
			if (other.bookCode != null)
				return false;
		} else if (!bookCode.equals(other.bookCode))
			return false;
		return true;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public int getPageNbr() {
		return pageNbr;
	}

	public void setPageNbr(int pageNbr) {
		this.pageNbr = pageNbr;
	}

}
