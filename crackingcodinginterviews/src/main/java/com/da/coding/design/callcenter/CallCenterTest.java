package com.da.coding.design.callcenter;

import java.io.InputStreamReader;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class CallCenterTest {
	public static void main(String[] args) throws InterruptedException {
		final Logger LOGGER = LoggerFactory.getLogger(CallCenterTest.class);
		Employee ceo= new Employee("ceo", null, DESIGNATION.CEO);
		HelpLine.getInstance().update(ceo, true);
		
		Employee director = new Employee("Director-1", ceo, DESIGNATION.DIRECTOR);
		HelpLine.getInstance().update(director, true);

		Employee manager1 = new Employee("Manager-1", director, DESIGNATION.MANAGER);
		HelpLine.getInstance().update(manager1, true);
		Employee manager2 = new Employee("Manager-2", director, DESIGNATION.MANAGER);
		HelpLine.getInstance().update(manager2, true);

		HelpLine.getInstance().update(new Employee("Executive-11", manager1, DESIGNATION.EXECUTIVE), true);
		HelpLine.getInstance().update(new Employee("Executive-12", manager1, DESIGNATION.EXECUTIVE), true);
		HelpLine.getInstance().update(new Employee("Executive-13", manager1, DESIGNATION.EXECUTIVE), true);
		HelpLine.getInstance().update(new Employee("Executive-14", manager1, DESIGNATION.EXECUTIVE), true);

		HelpLine.getInstance().update(new Employee("Executive-21", manager2, DESIGNATION.EXECUTIVE), true);
		HelpLine.getInstance().update(new Employee("Executive-22", manager2, DESIGNATION.EXECUTIVE), true);
		HelpLine.getInstance().update(new Employee("Executive-23", manager2, DESIGNATION.EXECUTIVE), true);
		HelpLine.getInstance().update(new Employee("Executive-24", manager2, DESIGNATION.EXECUTIVE), true);

		Scanner scn= new Scanner(new InputStreamReader(System.in));
		int next= scn.nextInt();
		int cust=1;
		
		while(next!=0){
			MDC.put("customer", "customer-"+cust++);
			LOGGER.info("Receiving call from the customer");
			HelpLine.getInstance().connectCustomer();
			next= scn.nextInt();
		}
		scn.close();
		
		EmployeeCallStatistics.printStatistics();
		
	}
}
