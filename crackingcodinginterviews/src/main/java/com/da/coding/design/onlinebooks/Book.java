package com.da.coding.design.onlinebooks;

import java.util.Date;
import java.util.UUID;

public class Book {
	private String bookCode;
	private String title;
	private BookCategory category;
	private String author;
	private String publisher;
	private double price;
	private Date release;
	private String bookSocialCode;

	public Book(String title, BookCategory category, String author, String publisher, double price, Date release) {
		super();
		this.bookCode = UUID.randomUUID().toString();
		this.title = title;
		this.category = category;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.release = release;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BookCategory getCategory() {
		return category;
	}

	public void setCategory(BookCategory category) {
		this.category = category;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getRelease() {
		return release;
	}

	public void setRelease(Date release) {
		this.release = release;
	}

	public String getBookSocialCode() {
		return bookSocialCode;
	}

	public void setBookSocialCode(String bookSocialCode) {
		this.bookSocialCode = bookSocialCode;
	}

	@Override
	public String toString() {
		return "Book [bookCode=" + bookCode + ", title=" + title + ", category=" + category + ", author=" + author
				+ ", publisher=" + publisher + ", price=" + price + ", release=" + release + "]";
	}

}
