package com.da.coding.design.parkinglot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ParkingLot {
	private Map<Integer, ParkingLevel> levelsAvailableMap;
	private int available;
	private static final Gson GSON = new GsonBuilder().create();
	private static final int CAPACITY = 12;
	private Map<VehicleType, List<VehicleSpotCountFactor>> vehicleSpotMapping;

	public ParkingLot() {
		levelsAvailableMap = new ConcurrentHashMap<>();
		vehicleSpotMapping = new HashMap<>();
	}

	public String findParkingSpot(VehicleType vehicleType) {
		if (isAvailable(vehicleType)) {
			for (Entry<Integer, ParkingLevel> levelEntry : levelsAvailableMap.entrySet()) {
				ParkingLevel parkingLevel = levelEntry.getValue();
				if (!parkingLevel.isAvailable(vehicleType))
					continue;
				Map<Integer, ParkingRow> rowsAvailableMap = parkingLevel.getRowsAvailableMap();
				for (Entry<Integer, ParkingRow> rowEntry : rowsAvailableMap.entrySet()) {
					ParkingRow parkingRow = rowEntry.getValue();
					if (!parkingRow.isAvailable(vehicleType))
						continue;
					ParkingSpotAllocation parkingToken = generateParkingToken(parkingRow, parkingLevel, vehicleType);
					if (parkingToken == null) {
						continue;
					} else {
						return GSON.toJson(parkingToken);
					}
				}
			}
		}
		System.out.println("Sorry! Parking is full for your vehicle : " + vehicleType.name());
		return null;
	}

	public void releaseParkingSpot(String parkingTokenStr) {
		ParkingSpotAllocation parkingAllocation = GSON.fromJson(parkingTokenStr, ParkingSpotAllocation.class);
		ParkingLevel parkingLevel = levelsAvailableMap.get(parkingAllocation.getLevelId());
		ParkingRow parkingRow = parkingLevel.getRowsAvailableMap().get(parkingAllocation.getRowId());
		List<ParkingSpot> spotsList = parkingRow.getSpotsMap()
				.get(ParkingSpotType.valueOf(parkingAllocation.getSpotType()));
		for (int index = 0; index < spotsList.size(); index++) {
			if (index >= parkingAllocation.getStartSpot() || index <= parkingAllocation.getEndSpot()) {
				ParkingSpot parkingSpot = spotsList.get(index);
				parkingSpot.setAvailable(parkingSpot.getType().getSize());
			}
		}
		parkingRow.setAvailable(parkingRow.getAvailable() + parkingAllocation.getAllocated());
		parkingLevel.setAvailable(parkingLevel.getAvailable() + parkingAllocation.getAllocated());
		this.setAvailable(this.getAvailable() + parkingAllocation.getAllocated());
	}

	private ParkingSpotAllocation generateParkingToken(ParkingRow parkingRow, ParkingLevel parkingLevel,
			VehicleType vehicleType) {

		List<VehicleSpotCountFactor> vsFactors = vehicleSpotMapping.get(vehicleType);
		List<ParkingSpot> identifiedSpots = new ArrayList<>();
		for (VehicleSpotCountFactor vsFactor : vsFactors) {
			ParkingSpotType spotType = vsFactor.getSpotType();
			Integer count = vsFactor.getCount();

			List<ParkingSpot> parkingSpots = parkingRow.getSpotsMap().get(spotType);

			for (ParkingSpot spot : parkingSpots) {
				if (spot.isAvailable(vehicleType)) {
					identifiedSpots.add(spot);
				}
				if (identifiedSpots.size() == count) {
					break;
				}

			}
			if (identifiedSpots.size() == count) {
				for (ParkingSpot sp : identifiedSpots) {
					sp.setAvailable(0);
				}
				parkingRow.setAvailable(parkingRow.getAvailable() - vehicleType.getSize());
				parkingLevel.setAvailable(parkingLevel.getAvailable() - vehicleType.getSize());
				this.setAvailable(this.getAvailable() - vehicleType.getSize());
				return new ParkingSpotAllocation(parkingRow.getLevelId(), parkingRow.getRowId(),
						identifiedSpots.get(0).getType().name(), identifiedSpots.get(0).getSpotId(),
						identifiedSpots.get(count - 1).getSpotId(), vehicleType.getSize());
			}

		}

		return null;
	}

	public void setUp(int levels, int rows) {
		setUpVehicleSpotMapping();
		for (int l = 1; l <= levels; l++) {
			ParkingLevel level = new ParkingLevel(l);
			for (int r = 1; r <= rows; r++) {
				ParkingRow row = new ParkingRow(r, l);
				int counter = 0;
				for (ParkingSpotType type : ParkingSpotType.values()) {
					List<ParkingSpot> parkingSpots = new ArrayList<>();
					counter = CAPACITY / type.getSize();
					for (int s = 1; s <= counter; s++) {
						ParkingSpot ps = new ParkingSpot(s, type);
						parkingSpots.add(ps);
					}
					row.getSpotsMap().put(type, parkingSpots);
					row.setAvailable(row.getAvailable() + CAPACITY);

				}
				level.getRowsAvailableMap().put(r, row);
				level.setAvailable(level.getAvailable() + row.getAvailable());
			}
			levelsAvailableMap.put(l, level);
			this.setAvailable(this.getAvailable() + level.getAvailable());
		}

	}

	// larger vehicle can take up multiple small spots but not otherwise
	private void setUpVehicleSpotMapping() {
		for (VehicleType vehicleType : VehicleType.values()) {
			List<VehicleSpotCountFactor> list = new ArrayList<>();
			for (ParkingSpotType spotType : ParkingSpotType.values()) {
				if (vehicleType.getSize() / spotType.getSize() > 0) {
					VehicleSpotCountFactor vsFactor = new VehicleSpotCountFactor(spotType,
							vehicleType.getSize() / spotType.getSize());
					list.add(vsFactor);
				}
			}
			Collections.sort(list, new Comparator<VehicleSpotCountFactor>() {

				@Override
				public int compare(VehicleSpotCountFactor o1, VehicleSpotCountFactor o2) {
					return o1.getCount().compareTo(o2.getCount());
				}
			});
			vehicleSpotMapping.put(vehicleType, list);
		}
	}

	public void printSetup() {
		for (Entry<Integer, ParkingLevel> plEntry : levelsAvailableMap.entrySet()) {
			System.out.println(
					"------------------------showing level-" + plEntry.getKey() + "----------------------------------");
			for (Entry<Integer, ParkingRow> prEntry : plEntry.getValue().getRowsAvailableMap().entrySet()) {
				System.out.println("------------------------showing row-" + prEntry.getKey()
						+ "----------------------------------");
				ParkingRow parkingRow = prEntry.getValue();
				for (Entry<ParkingSpotType, List<ParkingSpot>> prsEntry : parkingRow.getSpotsMap().entrySet()) {
					List<ParkingSpot> spots = prsEntry.getValue();
					System.out.println(spots);
				}
			}

		}
	}

	public static void main(String[] args) {
		ParkingLot parkingLot = new ParkingLot();
		parkingLot.setUp(2, 2);
		parkingLot.printSetup();
		System.out
				.println("-------------------------------Generating Parking Tokens-----------------------------------");

		System.out.println("Lorry is assigned " + parkingLot.findParkingSpot(VehicleType.LORRY));
		String parkingAlloc = parkingLot.findParkingSpot(VehicleType.LORRY);
		System.out.println("Lorry is assigned " + parkingAlloc);
		parkingLot.releaseParkingSpot(parkingAlloc);

		System.out.println("Lorry is assigned " + parkingLot.findParkingSpot(VehicleType.LORRY));
		System.out.println("Lorry is assigned " + parkingLot.findParkingSpot(VehicleType.LORRY));
		System.out.println("Lorry is assigned " + parkingLot.findParkingSpot(VehicleType.LORRY));

		System.out.println("Bus is assigned " + parkingLot.findParkingSpot(VehicleType.BUS));
		System.out.println("Bus is assigned " + parkingLot.findParkingSpot(VehicleType.BUS));
		System.out.println("Bus is assigned " + parkingLot.findParkingSpot(VehicleType.BUS));

		System.out.println("Car is assigned " + parkingLot.findParkingSpot(VehicleType.CAR));
		System.out.println("Car is assigned " + parkingLot.findParkingSpot(VehicleType.CAR));
		System.out.println("Car is assigned " + parkingLot.findParkingSpot(VehicleType.CAR));
		System.out.println("Car is assigned " + parkingLot.findParkingSpot(VehicleType.CAR));
		System.out.println("Car is assigned " + parkingLot.findParkingSpot(VehicleType.CAR));

		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));
		System.out.println("Bike is assigned " + parkingLot.findParkingSpot(VehicleType.BIKE));

		parkingLot.printSetup();
	}

	public boolean isAvailable(VehicleType type) {
		return available >= type.getSize();
	}

	public Map<Integer, ParkingLevel> getLevelsAvailableMap() {
		return levelsAvailableMap;
	}

	public void setLevelsAvailableMap(Map<Integer, ParkingLevel> levelsAvailableMap) {
		this.levelsAvailableMap = levelsAvailableMap;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

}
