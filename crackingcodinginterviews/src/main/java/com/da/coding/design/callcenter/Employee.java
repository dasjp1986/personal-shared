package com.da.coding.design.callcenter;

import java.util.Random;

public class Employee {
	protected String id;
	protected Employee manager;
	protected DESIGNATION designation;

	public Employee(String id, Employee manager, DESIGNATION designation) {
		super();
		this.id = id;
		this.manager = manager;
		this.designation = designation;
	}

	public String getId() {
		return id;
	}

	public Employee getManager() {
		return manager;
	}

	public DESIGNATION getDesignation() {
		return designation;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public boolean respond() {
		int respondTime = new Random().nextInt(1000);
		try {
			Thread.sleep(new Random().nextInt(respondTime));
		} catch (InterruptedException e) {
		}
		return respondTime < 500 ? true : false;
	}
	
	public void converse() {
		new Thread(new Conversation(this)).start();
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Employee emp = (Employee) obj;
		return emp.id.equals(this.id);
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", designation=" + designation + "]";
	}

}
