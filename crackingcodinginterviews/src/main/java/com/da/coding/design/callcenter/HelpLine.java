package com.da.coding.design.callcenter;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author dagraw1
 *
 *         Design a call center where there is hierarchy of people starting from
 *         Executive->Manager->Director
 * 
 *         whenever customer calls comes it should be assigned to one of the
 *         available Executive but if he/she is unable to answer the call then
 *         it should be escalated to their manager and then to the director if
 *         manager also fails to respond.
 * 
 * 
 */
public class HelpLine {
	private Map<Employee, Boolean> availableEmps;

	private static volatile HelpLine CALL_CENTER;
	private static final Logger LOGGER = LoggerFactory.getLogger(HelpLine.class);

	private HelpLine() {
		availableEmps = new ConcurrentHashMap<>();
	}

	public static HelpLine getInstance() {
		if (CALL_CENTER == null) {
			synchronized (HelpLine.class) {
				if (CALL_CENTER == null) {
					CALL_CENTER = new HelpLine();
				}
			}
		}

		return CALL_CENTER;
	}

	public Employee findAvailable(DESIGNATION designation) {
		for (Entry<Employee, Boolean> entry : availableEmps.entrySet()) {
			if (entry.getValue() && entry.getKey().getDesignation() == designation) {
				return entry.getKey();
			}
		}
		return null;
	}

	public void update(Employee employee, boolean status) {
		availableEmps.put(employee, status);
	}

	public Boolean findStatus(Employee employee) {
		return availableEmps.get(employee);
	}

	public void connectCustomer() {

		Employee available = findAvailable(DESIGNATION.EXECUTIVE);
		int retry = 1;
		while (available == null) {
			LOGGER.info("All executive all busy retrying....{}", retry++);
			if (retry > 5) {
				LOGGER.error("Disconnecting the call... Please call later");
				return;
			}
		}

		update(available, false);
		new Thread(new CallDispatcher(available)).start();

	}
}
