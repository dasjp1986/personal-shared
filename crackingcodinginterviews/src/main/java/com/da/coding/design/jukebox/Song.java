package com.da.coding.design.jukebox;

import java.util.Date;

public class Song {
	private String code;
	private String desc;
	private String artist;
	private GENRE genre;
	private Date releaseDateTime;

	public Song(String code, String desc, String artist, GENRE genre, Date releaseDateTime) {
		super();
		this.code = code;
		this.desc = desc;
		this.artist = artist;
		this.genre = genre;
		this.releaseDateTime = releaseDateTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public GENRE getGenre() {
		return genre;
	}

	public void setGenre(GENRE genre) {
		this.genre = genre;
	}

	public Date getReleaseDateTime() {
		return releaseDateTime;
	}

	public void setReleaseDateTime(Date releaseDateTime) {
		this.releaseDateTime = releaseDateTime;
	}

	@Override
	public String toString() {
		return "Song [desc=" + desc + ", artist=" + artist + ", genre=" + genre + "]";
	}

}
