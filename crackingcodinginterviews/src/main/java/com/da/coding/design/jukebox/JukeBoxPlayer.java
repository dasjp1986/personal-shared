package com.da.coding.design.jukebox;

import java.util.List;
import java.util.Set;

public class JukeBoxPlayer {

	public void createPlayList(String playListId, List<Song> songs) {
		PlayLists.getInstance().save(playListId, songs);
	}

	public void runPlayList(String playListId) {
		Set<Song> songs = PlayLists.getInstance().load(playListId);
		Player.play(songs);
	}

	public void playTop(int limit) {
		List<Song> songs = Library.getInstance().findLatest(limit);
		Player.play(songs);

	}

	public void playRandom() {
		Song song = Library.getInstance().random();
		Player.play(song);
	}

	public void playByCode(String code) {
		Song song = Library.getInstance().findByCode(code);
		if (song == null) {
			System.out.println("Sorry! Song does not exist...");
		} else {
			Player.play(song);
		}
	}

	public void playByArtist(String artist) {
		List<Song> songs = Library.getInstance().findByArtist(artist);
		Player.play(songs);
	}

	public void playByArtistLatest(String artist, int limit) {
		List<Song> songs = Library.getInstance().findByArtistLatest(artist, limit);
		Player.play(songs);
	}

}
