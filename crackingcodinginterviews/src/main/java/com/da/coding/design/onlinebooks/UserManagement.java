package com.da.coding.design.onlinebooks;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//Singleton
public class UserManagement {
	private Map<String, BookUser> users;
	private ReaderStatisticsManager readerStatisticsManager;

	public UserManagement() {
		this.users = new ConcurrentHashMap<>();
		this.readerStatisticsManager = new ReaderStatisticsManager();
	}

	public void register(BookUser bookUser) {
		users.put(bookUser.getUserId(), bookUser);
		readerStatisticsManager.save(new ReaderStatistics(bookUser.getUserId()));
	}

	public void remove(String userId) {
		users.remove(userId);
		readerStatisticsManager.remove(userId);
	}
}
