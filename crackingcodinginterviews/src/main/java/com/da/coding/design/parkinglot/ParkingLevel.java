package com.da.coding.design.parkinglot;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ParkingLevel {
	private int levelId;
	private Map<Integer, ParkingRow> rowsAvailableMap;
	private int available;

	public ParkingLevel(int levelId) {
		rowsAvailableMap = new ConcurrentHashMap<>();
		this.levelId = levelId;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public Map<Integer, ParkingRow> getRowsAvailableMap() {
		return rowsAvailableMap;
	}

	public void setRowsAvailableMap(Map<Integer, ParkingRow> rowsAvailableMap) {
		this.rowsAvailableMap = rowsAvailableMap;
	}

	public boolean isAvailable(VehicleType type) {
		return available >= type.getSize();
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

}
