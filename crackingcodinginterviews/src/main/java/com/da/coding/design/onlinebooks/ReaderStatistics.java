package com.da.coding.design.onlinebooks;

public class ReaderStatistics {
	private String userId;
	private int booksCompleted;
	private int booksReading;
	private Book activeBook;

	public ReaderStatistics(String userId) {
		super();
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getBooksCompleted() {
		return booksCompleted;
	}

	public void setBooksCompleted(int booksCompleted) {
		this.booksCompleted = booksCompleted;
	}

	public int getBooksReading() {
		return booksReading;
	}

	public void setBooksReading(int booksReading) {
		this.booksReading = booksReading;
	}

	public Book getActiveBook() {
		return activeBook;
	}

	public void setActiveBook(Book activeBook) {
		this.activeBook = activeBook;
	}

}
