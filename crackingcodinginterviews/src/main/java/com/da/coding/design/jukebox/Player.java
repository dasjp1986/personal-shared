package com.da.coding.design.jukebox;

import java.util.Collection;

public class Player {
	public static void play(Song song) {
		System.out.println("Playing song...." + song);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void play(Collection<Song> songs) {

		if (songs == null) {
			System.out.println("PlayList is empty...");
		} else {
			for (Song song : songs) {
				play(song);
			}
		}
	}
}
