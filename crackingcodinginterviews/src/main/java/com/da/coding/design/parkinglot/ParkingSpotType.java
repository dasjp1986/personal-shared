package com.da.coding.design.parkinglot;

public enum ParkingSpotType {
	SMALL(1), MEDIUM(3), LARGE(6), XLARGE(12);
	private int size;

	private ParkingSpotType(int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	

}
