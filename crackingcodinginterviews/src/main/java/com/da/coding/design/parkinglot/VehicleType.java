package com.da.coding.design.parkinglot;

public enum VehicleType {
	BIKE(1), CAR(3), BUS(6), LORRY(12);

	private int size;

	private VehicleType(int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
