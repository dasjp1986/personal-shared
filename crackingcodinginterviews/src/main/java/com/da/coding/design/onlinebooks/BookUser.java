package com.da.coding.design.onlinebooks;

public class BookUser {
	private String userId;
	private String emailAddress;
	private String userName;

	public BookUser(String userId, String emailAddress, String userName) {
		super();
		this.userId = userId;
		this.emailAddress = emailAddress;
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
