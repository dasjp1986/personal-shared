package com.da.coding.design.parkinglot;

public class VehicleSpotCountFactor {
	private ParkingSpotType spotType;
	private Integer count;

	public VehicleSpotCountFactor(ParkingSpotType spotType, Integer count) {
		super();
		this.spotType = spotType;
		this.count = count;
	}

	public ParkingSpotType getSpotType() {
		return spotType;
	}

	public void setSpotType(ParkingSpotType spotType) {
		this.spotType = spotType;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
