package com.da.coding.design.onlinebooks;

import java.util.List;

//Singleton
public class OnlineBookStore {
	private BookSearchService bookSearchService;
	private UserManagement userManagement;
	private BookMarkService bookMarkService;
	private BookManager bookManager;
	private BookSocialManager bookSocialManager;

	public OnlineBookStore() {
		this.bookSocialManager = new BookSocialManager();
		this.bookManager = new BookManager(bookSocialManager);
		this.bookSearchService = new BookSearchService(bookManager, bookSocialManager);
		this.userManagement = new UserManagement();
		this.bookMarkService = new BookMarkService();
	}

	public static void main(String[] args) {
		OnlineBookStore onlineBookStore = new OnlineBookStore();
		onlineBookStore.test();

	}

	public void test() {
		BookUser bookUser = new BookUser("da", "da@gmail.com", "Dharmendra");
		userManagement.register(bookUser);
		System.out.println("Searching by Author");
		List<BookSocial> books = bookSearchService.findByAuthor("JK Rolling");
		displayBooks(bookUser, books);

		System.out.println("Searching by Title");
		books = bookSearchService.findByTitle("Lord");
		displayBooks(bookUser, books);

		System.out.println("Searching Top 5 Fictional");
		books = bookSearchService.findTopNByCategory(BookCategory.FICTIONAL, 5);
		displayBooks(bookUser, books);

	}

	private void displayBooks(BookUser bookUser, List<BookSocial> books) {
		if (books != null) {
			System.out.println("Book Results...");
			for (BookSocial book : books) {
				System.out.println(book);
			}
			BookSocial bookSocial = books.get(0);
			System.out.println("opening book : " + bookSocial.getBook());
			BookRenderer bookRenderer = new BookRenderer();
			int pageNbr = bookRenderer.render(books.get(0));
			bookMarkService.addBookMark(bookUser.getUserId(), bookManager.find(bookSocial.getBook().getBookCode()),
					pageNbr);
		}
	}

}
