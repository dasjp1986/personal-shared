package com.da.coding.design.callcenter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CallDispatcher implements Runnable {

	private Employee employee;
	private static final Logger LOGGER = LoggerFactory.getLogger(CallDispatcher.class);

	public CallDispatcher(Employee employee) {
		this.employee = employee;
	}

	public void dispatchCall() {
		if (employee.respond()) {
			LOGGER.debug("Call is picked by the employee : {}", employee);
			employee.converse();
			EmployeeCallStatistics.addCallCompleted(employee);
		} else {
			EmployeeCallStatistics.addCallEscalated(employee);
			HelpLine.getInstance().update(employee, true);
			escalate(employee);
		}
	}

	private void escalate(Employee emp) {
		LOGGER.debug("Call escalated from : {} to its manager ", emp);
		Employee manager = emp.getManager();
		if (manager == null) {
			LOGGER.error("Call can not be escalated further... call ended");
			return;
		}
		if (manager.respond() && HelpLine.getInstance().findStatus(manager)) {
			HelpLine.getInstance().update(manager, false);
			LOGGER.debug("Call is picked by the employee : {}", manager);
			manager.converse();
			EmployeeCallStatistics.addCallCompleted(manager);
		} else {
			EmployeeCallStatistics.addCallEscalated(manager);
			escalate(emp.getManager());
		}

	}

	@Override
	public void run() {
		dispatchCall();
	}

}
