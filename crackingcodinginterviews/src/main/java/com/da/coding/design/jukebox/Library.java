package com.da.coding.design.jukebox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Library {
	private Map<String, Song> songs;
	private static volatile Library SINGLETON;

	private Library() {
		songs = new HashMap<>();
	}

	public static Library getInstance() {
		if (SINGLETON == null) {
			synchronized (Library.class) {
				if (SINGLETON == null) {
					SINGLETON = new Library();
				}
			}
		}

		return SINGLETON;
	}

	public void add(Song song) {
		songs.put(song.getCode(), song);
	}

	public void remove(String code) {
		songs.remove(code);
	}

	public Song findByCode(String songCode) {
		return songs.get(songCode);
	}

	public Song random() {
		int number = new Random().nextInt(songs.size());
		int count = 0;
		String code = null;
		for (String c : songs.keySet()) {
			if (count++ == number) {
				code = c;
				break;
			}
		}
		return songs.get(code);
	}

	public List<Song> findByCodes(Set<String> songCodes) {
		List<Song> songsList = new ArrayList<>();
		for (String code : songCodes) {
			songsList.add(songs.get(code));
		}
		return songsList;
	}

	public List<Song> findByArtist(String artist) {
		List<Song> songsList = new ArrayList<>();
		for (Song song : songs.values()) {
			if (song.getArtist().equalsIgnoreCase(artist)) {
				songsList.add(song);
			}
		}
		return songsList;
	}

	public List<Song> findByArtistLatest(String artist, int limit) {
		List<Song> songsList = findByArtist(artist);
		if (songsList.size() > limit) {
			return latest(limit, songsList);
		}
		return songsList;
	}

	public List<Song> findLatest(int limit) {
		return latest(limit, new ArrayList<>(songs.values()));
	}

	private List<Song> latest(int limit, List<Song> songsList) {
		Collections.sort(songsList, new Comparator<Song>() {
			@Override
			public int compare(Song o1, Song o2) {
				return o2.getReleaseDateTime().compareTo(o1.getReleaseDateTime());
			}
		});

		if (songsList.size() < limit) {
			return songsList;
		} else {
			List<Song> sList = new ArrayList<>();
			int counter = 0;
			while (counter < limit) {
				sList.add(songsList.get(counter++));
			}
			return sList;
		}

	}

}
