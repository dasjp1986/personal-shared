package com.da.coding.design.callcenter;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmployeeCallStatistics {
	private static Map<Employee, CallStatistics> stats = new ConcurrentHashMap<>();
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeCallStatistics.class);

	public static void addCallCompleted(Employee emp) {
		CallStatistics statistics = stats.get(emp);
		if (statistics == null) {
			statistics = new CallStatistics();
			stats.put(emp, statistics);
		}
		statistics.setCompleted(statistics.getCompleted() + 1);
	}

	public static void addCallEscalated(Employee emp) {
		CallStatistics statistics = stats.get(emp);
		if (statistics == null) {
			statistics = new CallStatistics();
			stats.put(emp, statistics);
		}
		statistics.setEscalated(statistics.getEscalated() + 1);
	}

	public static void printStatistics() {
		LOGGER.info("------------------------------------call statistics----------------------------");
		LOGGER.info("EmployeeID\t\t Designation\t\t CallsCompleted\t\t CallsEscalated\t\t");
		for (Entry<Employee, CallStatistics> entry : stats.entrySet()) {
			LOGGER.info(entry.getKey().getId() + "\t\t " + entry.getKey().getDesignation().name() + "\t\t\t "
					+ entry.getValue().getCompleted() + "\t\t\t\t " + entry.getValue().getEscalated());
		}
	}
}
