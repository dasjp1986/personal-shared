package com.da.coding.design.jukebox;

import java.util.Date;

public class JukeBoxTest {
	public static void main(String[] args) {
		// Load Songs
		Library library = Library.getInstance();
		library.add(new Song("ddlj1", "Tujhe dekha to yeh...", "Kumar Sanu", GENRE.LOVE, new Date(1995, 1, 1, 0, 1)));
		library.add(new Song("ddlj2", "Mere khwabo mein jo aaye...", "Lata Mangeshkar", GENRE.LOVE,
				new Date(1995, 1, 1, 1, 1)));
		library.add(new Song("kkhh1", "Tum pas aaye...", "Udit Narayan", GENRE.LOVE, new Date(2000, 1, 1, 0, 1)));
		library.add(
				new Song("kkhh2", "Tujhe yad na meri aayi...", "Alka Yagnik", GENRE.LOVE, new Date(2000, 1, 1, 1, 1)));
		library.add(new Song("kmg1", "Koi mil gaya...", "Udit Narayan", GENRE.BOLLYWOOD, new Date(2004, 1, 1, 0, 1)));
		library.add(new Song("k3g1", "Kabhi khushi kabhi gam...", "Lata Mangeshkar", GENRE.SAD,
				new Date(2005, 1, 1, 0, 1)));
		library.add(
				new Song("k3g2", "Sooraj hua maddham...", "Udit Narayan", GENRE.BOLLYWOOD, new Date(2005, 1, 1, 0, 1)));
		library.add(new Song("padmavat1", "Ghoomer ghoomer...", "Shreya Ghosal", GENRE.BOLLYWOOD,
				new Date(2018, 1, 1, 0, 1)));
		library.add(
				new Song("hg1", "Yeh mausam ki baarish...", "Arijit Singh", GENRE.LOVE, new Date(2017, 2, 1, 0, 1)));
		library.add(new Song("aashiqui2", "Tum hi ho...", "Arijit Singh", GENRE.LOVE, new Date(2015, 2, 1, 0, 1)));
		library.add(new Song("aashiqui2", "Sun raha he na...", "Arijit Singh", GENRE.LOVE, new Date(2015, 2, 1, 0, 1)));
		library.add(new Song("bkd1", "Badri ki dulhaniya...", "Arijit Singh", GENRE.LOVE, new Date(2017, 1, 1, 0, 1)));

		JukeBoxPlayer jukeBoxPlayer = new JukeBoxPlayer();

//		jukeBoxPlayer.playRandom();
//		jukeBoxPlayer.playTop(5);
//		jukeBoxPlayer.playByArtist("Udit Narayan");
//		jukeBoxPlayer.playByArtistLatest("Arijit Singh", 3);
		jukeBoxPlayer.createPlayList("DA", Library.getInstance().findLatest(5));
		//jukeBoxPlayer.createPlayList("DA", Library.getInstance().findByArtist("Lata Mangeshkar"));
		jukeBoxPlayer.runPlayList("DA");
	}
}
