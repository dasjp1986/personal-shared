package com.da.coding.design.parkinglot;

public class ParkingSpot {
	private int spotId;
	private ParkingSpotType type;
	private int available;

	public ParkingSpot(int spotId, ParkingSpotType type) {
		super();
		this.spotId = spotId;
		this.type = type;
		this.available = type.getSize();
	}

	public int getSpotId() {
		return spotId;
	}

	public void setSpotId(int spotId) {
		this.spotId = spotId;
	}

	public ParkingSpotType getType() {
		return type;
	}

	public void setType(ParkingSpotType type) {
		this.type = type;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public boolean isAvailable(VehicleType type) {
		return available >0;
	}

	@Override
	public String toString() {
		return type.ordinal() + ":" + spotId + ":" + (available > 0);
	}

}
