package com.da.coding.design.callcenter;

public enum DESIGNATION {
	EXECUTIVE, MANAGER, DIRECTOR, CEO;
}
