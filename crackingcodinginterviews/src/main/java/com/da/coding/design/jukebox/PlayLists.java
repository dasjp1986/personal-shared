package com.da.coding.design.jukebox;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PlayLists {
	private Map<String, Set<Song>> playListMap;
	private static volatile PlayLists SINGLETON;

	private PlayLists() {
		playListMap = new HashMap<>();
	}

	public static PlayLists getInstance(){
		if(SINGLETON==null){
			synchronized (PlayLists.class) {
				if(SINGLETON==null){
					SINGLETON= new PlayLists();
				}
			}
		}
		return SINGLETON;
	}

	public void save(String playListId, List<Song> songs) {
		Set<Song> playList = playListMap.get(playListId);
		if (playList == null) {
			playList = new HashSet<>(songs);
			playListMap.put(playListId, playList);
		} else {
			playList.addAll(songs);
		}
	}

	public Set<Song> load(String playListId) {
		return playListMap.get(playListId);
	}
}
