package com.da.coding.design.onlinebooks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//Singleton
public class BookManager {
	private Map<String, Book> bookLibrary;
	private BookSocialManager bookSocialManager;

	public BookManager(BookSocialManager bookSocialManager) {
		this.bookSocialManager = bookSocialManager;
		bookLibrary = new ConcurrentHashMap<>();
		setUpLibrary();
	}

	public void addBook(Book book) {
		BookSocial bookSocial = bookSocialManager.add(book.getBookCode());
		book.setBookSocialCode(bookSocial.getBookSocialCode());
		bookLibrary.put(book.getBookCode(), book);
		bookSocial.setBookCode(book);
	}

	public void removeBook(String bookCode) {
		Book book = bookLibrary.get(bookCode);
		bookSocialManager.remove(book.getBookSocialCode());
	}

	public Book find(String bookCode) {
		return bookLibrary.get(bookCode);
	}

	public List<Book> findBooksByTitle(String title) {
		List<Book> books = new ArrayList<>();
		for (Book book : bookLibrary.values()) {
			if (book.getTitle().contains(title)) {
				books.add(book);
			}
		}
		return books;
	}

	public List<Book> findBooksByAuthor(String author) {
		List<Book> books = new ArrayList<>();
		for (Book book : bookLibrary.values()) {
			if (book.getAuthor().contains(author)) {
				books.add(book);
			}
		}
		return books;
	}

	public List<Book> findBooksByCategory(BookCategory category) {
		List<Book> books = new ArrayList<>();
		for (Book book : bookLibrary.values()) {
			if (book.getCategory().equals(category)) {
				books.add(book);
			}
		}
		return books;
	}

	private void setUpLibrary() {
		addBook(new Book("Harry Potter- The Philosopher's Stone", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(1997, 01, 01)));
		addBook(new Book("Harry Potter- The Chamber of Secrets", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(1998, 01, 01)));
		addBook(new Book("Harry Potter- The Prisoner of Azkaban", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(1999, 01, 01)));
		addBook(new Book("Harry Potter- The Goblet of Fire", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(2000, 01, 01)));
		addBook(new Book("Harry Potter- The Order of the Phoenix", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(2003, 01, 01)));
		addBook(new Book("Harry Potter- The Half-Blood Prince", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(2005, 01, 01)));
		addBook(new Book("Harry Potter- The Deathly Hallows", BookCategory.FICTIONAL, "JK Rolling", "Bloomsburry",
				100.00, new Date(2007, 01, 01)));

		addBook(new Book("The Lord of the rings-part1", BookCategory.FICTIONAL, "J R R Tolkein", "Allen & Alwin",
				300.00, new Date(1954, 11, 01)));
		addBook(new Book("The Lord of the rings-part2", BookCategory.FICTIONAL, "J R R Tolkein", "Allen & Alwin",
				300.00, new Date(1954, 11, 01)));
		addBook(new Book("The Lord of the rings-part3", BookCategory.FICTIONAL, "J R R Tolkein", "Allen & Alwin",
				300.00, new Date(1954, 11, 01)));

		addBook(new Book("A brief history of time", BookCategory.NON_FICTIONAL, "Stephan Hawking", "Bantam Dell",
				150.00, new Date(1988, 05, 01)));

		addBook(new Book("A game of thrones-part1", BookCategory.NOVEL, "George R R Martin", "Bantam Spectra", 450.00,
				new Date(1997, 01, 01)));
		addBook(new Book("A game of thrones-part2", BookCategory.NOVEL, "George R R Martin", "Bantam Spectra", 450.00,
				new Date(1998, 01, 01)));
		addBook(new Book("A game of thrones-part3", BookCategory.NOVEL, "George R R Martin", "Bantam Spectra", 450.00,
				new Date(1999, 01, 01)));
		addBook(new Book("A game of thrones-part4", BookCategory.NOVEL, "George R R Martin", "Bantam Spectra", 450.00,
				new Date(2001, 01, 01)));
		addBook(new Book("A game of thrones-part5", BookCategory.NOVEL, "George R R Martin", "Bantam Spectra", 450.00,
				new Date(2002, 01, 01)));
		addBook(new Book("A game of thrones-part6", BookCategory.NOVEL, "George R R Martin", "Bantam Spectra", 450.00,
				new Date(2011, 01, 01)));

	}

}
