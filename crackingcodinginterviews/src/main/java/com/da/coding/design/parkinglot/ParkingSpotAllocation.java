package com.da.coding.design.parkinglot;

public class ParkingSpotAllocation {
	private int levelId;
	private int rowId;
	private String spotType;
	private int startSpot;
	private int endSpot;
	private int allocated;

	public ParkingSpotAllocation(int levelId, int rowId, String spotType, int startSpot, int endSpot, int allocated) {
		super();
		this.levelId = levelId;
		this.rowId = rowId;
		this.spotType = spotType;
		this.startSpot = startSpot;
		this.endSpot = endSpot;
		this.allocated = allocated;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getSpotType() {
		return spotType;
	}

	public void setSpotType(String spotType) {
		this.spotType = spotType;
	}

	public int getStartSpot() {
		return startSpot;
	}

	public void setStartSpot(int startSpot) {
		this.startSpot = startSpot;
	}

	public int getEndSpot() {
		return endSpot;
	}

	public void setEndSpot(int endSpot) {
		this.endSpot = endSpot;
	}

	public int getAllocated() {
		return allocated;
	}

	public void setAllocated(int allocated) {
		this.allocated = allocated;
	}

}
