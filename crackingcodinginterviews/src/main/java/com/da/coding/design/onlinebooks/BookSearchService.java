package com.da.coding.design.onlinebooks;

import java.util.ArrayList;
import java.util.List;

//Singleton
public class BookSearchService {
	private BookManager bookManager;
	private BookSocialManager bookSocialManager;

	public BookSearchService(BookManager bookManager, BookSocialManager bookSocialManager) {
		this.bookManager = bookManager;
		this.bookSocialManager = bookSocialManager;
	}

	public List<BookSocial> findByTitle(String title) {
		List<BookSocial> results = new ArrayList<>();
		List<Book> books = bookManager.findBooksByTitle(title);
		if (books != null) {
			for (Book book : books) {
				results.add(bookSocialManager.getBookSocial(book.getBookSocialCode()));
			}
		}
		return results;
	}

	public List<BookSocial> findByAuthor(String author) {
		List<BookSocial> results = new ArrayList<>();
		List<Book> books = bookManager.findBooksByAuthor(author);
		if (books != null) {
			for (Book book : books) {
				results.add(bookSocialManager.getBookSocial(book.getBookSocialCode()));
			}
		}
		return results;
	}

	public List<BookSocial> findTopNByCategory(BookCategory category, int top) {
		List<Book> books = bookManager.findBooksByCategory(category);
		return bookSocialManager.findTopByRating(books, top);
	}
}
