package com.da.coding.design.callcenter;

public class CallStatistics {
	private int completed;
	private int escalated;

	public int getCompleted() {
		return completed;
	}

	public void setCompleted(int completed) {
		this.completed = completed;
	}

	public int getEscalated() {
		return escalated;
	}

	public void setEscalated(int escalated) {
		this.escalated = escalated;
	}

}
