package com.da.coding.design.onlinebooks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class BookSocial {
	private String bookSocialCode;
	private Double rating;
	private int readersCount;
	private List<String> reviews;
	private Book book;

	public BookSocial(String bookCode) {
		super();
		this.bookSocialCode = UUID.randomUUID().toString();
		this.reviews = new ArrayList<>();
		this.rating = new Random().nextDouble() * 5;
	}

	public String getBookSocialCode() {
		return bookSocialCode;
	}

	public void setBookSocialCode(String bookSocialCode) {
		this.bookSocialCode = bookSocialCode;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public int getReadersCount() {
		return readersCount;
	}

	public void setReadersCount(int readersCount) {
		this.readersCount = readersCount;
	}

	public List<String> getReviews() {
		return reviews;
	}

	public void setReviews(List<String> reviews) {
		this.reviews = reviews;
	}

	public Book getBook() {
		return book;
	}

	public void setBookCode(Book book) {
		this.book = book;
	}

	@Override
	public String toString() {
		return "BookSocial [bookSocialCode=" + bookSocialCode + ", rating=" + rating + ", readersCount=" + readersCount
				+ ", reviews=" + reviews + ", bookTitle=" + book.getTitle() + "]";
	}

}
