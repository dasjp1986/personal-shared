package com.da.coding.design.parkinglot;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ParkingRow {
	private int rowId;
	private int levelId;
	private Map<ParkingSpotType, List<ParkingSpot>> spotsMap;
	private int available;

	public ParkingRow(int rowId, int levelId) {
		this.rowId = rowId;
		this.levelId = levelId;
		spotsMap = new TreeMap<>();
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public Map<ParkingSpotType, List<ParkingSpot>> getSpotsMap() {
		return spotsMap;
	}

	public void setSpotsMap(Map<ParkingSpotType, List<ParkingSpot>> spotsMap) {
		this.spotsMap = spotsMap;
	}

	public boolean isAvailable(VehicleType type) {
		return available >= type.getSize();
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

}
