package com.da.coding.commons;

public class Constants {
	public static final int ASCII_SIZE=256;
	public static enum GRAPH_TYPE{
		CYCLIC, ACYLIC, DIRECTED, UNDIRECTED
	}
}
