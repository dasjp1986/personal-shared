package com.da.coding.commons;

import java.util.Map;
import java.util.TreeMap;

public class TestTreeMap{
	public static void main(String[] args) {
		Map<Employee, Integer> map= new TreeMap<>();
		map.put(new Employee(1, "DA"), 100);
		map.put(new Employee(1, "SA"), 100);
		
		for(Employee e: map.keySet()){
			System.out.println(e);
		}
	}
}

class Employee implements Comparable<Employee>{
	Integer id;
	String name;
	public Employee(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + "]";
	}
	@Override
	public int compareTo(Employee o) {
		if(this.getId().equals(o.getId())){
			return this.getName().compareTo(o.getName());
		}
		return this.getId().compareTo(o.getId());
	}

}
