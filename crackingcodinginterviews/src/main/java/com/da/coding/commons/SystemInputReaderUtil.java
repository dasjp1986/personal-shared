package com.da.coding.commons;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInputReaderUtil {
	private static BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));

	public static String readString() {
		String str = "";
		try {
			str = bfr.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}

	public static int[][] readMatrix() {
		String[] rowsCols = SystemInputReaderUtil.readString().split(" ");
		int rows = Integer.parseInt(rowsCols[0]);
		int cols = Integer.parseInt(rowsCols[1]);
		
		int[][] matrix = new int[rows][cols];

		try {
			for (int i = 0; i < rows; i++) {
				String array[] = bfr.readLine().split(" ");
				for (int j = 0; j < cols; j++) {
					matrix[i][j] = Integer.parseInt(array[j]);
				}
			}
		} catch (IOException io) {

		}
		return matrix;
	}
}
