package com.da.coding.learnings;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 * 
 *         For a given n and array of elements. find out how many ways elements
 *         can add up to n. find permutations (coin change problem) and
 *         combinations (ways of steps taken for distance-n)
 */
public class PermutationAndCombinations {

	// Time and space complexity- O(n*coins.length)
	public int coinChange(int n, int[] coins) {
		Integer[][] cache = new Integer[n + 1][coins.length];
		return change(n, 0, coins, cache);
	}

	// Time and space complexity- O(n*steps.length)
	public int waysToDistance(int n, int[] steps) {
		Integer[] cache = new Integer[n + 1];
		Arrays.sort(steps);
		return ways(n, steps, cache);
	}

	private int ways(int n, int[] steps, Integer[] cache) {
		int count = 0;

		if (n == 0) {
			return 1;
		}

		if (n < 0) {
			return 0;
		}

		if (cache[n] != null) {
			return cache[n];
		}

		for (int i = 0; i < steps.length; i++) {
			count = count + ways(n - steps[i], steps,cache);
		}
		return count;
	}

	private int change(int n, int index, int[] coins, Integer[][] cache) {
		if (n == 0) {
			return 1;
		}

		if (n < 0 || index == coins.length) {
			return 0;
		}

		if (cache[n][index] != null) {
			return cache[n][index];
		}

		int exclude = change(n, index + 1, coins, cache);
		int include = change(n - coins[index], index, coins, cache);
		cache[n][index] = include + exclude;
		return cache[n][index];
	}

	public static void main(String[] args) {
		PermutationAndCombinations permute = new PermutationAndCombinations();
		System.out.println("no of ways change coin : " + permute.coinChange(5, new int[] { 1, 3, 5 }));

		PermutationAndCombinations combinations = new PermutationAndCombinations();
		System.out.println("no of ways to cover distance : " + combinations.waysToDistance(10, new int[] { 1, 3, 5 }));
	}

}
