package com.da.coding.search;

public class BinarySearch {
	public void searchIndex(int[] array, int key) {
		System.out.println(searchRecursively(array, 0, array.length - 1, key));
		System.out.println(searchIteratively(array, key));
	}

	//Time complexity O(LogN) and Space Complexity O(1)
	private int searchIteratively(int[] array, int key) {

		int start = 0;
		int end = array.length - 1;

		int pos = -1;

		int mid = 0;
		while (start <= end) {
			mid = (start + end) / 2;
			if (key == array[mid]) {
				pos = mid;
				break;
			} else if (key > array[mid]) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}

		return pos;
	}

	//Time complexity O(LogN) and Space Complexity O(LogN)
	private int searchRecursively(int[] array, int start, int end, int key) {

		if (start > end) {
			return -1;
		}

		int mid = (start + end) / 2;

		int pos = -1;
		if (key == array[mid]) {
			pos = mid;
		} else if (key > array[mid]) {
			pos = searchRecursively(array, mid + 1, end, key);
		} else {
			pos = searchRecursively(array, start, mid - 1, key);
		}

		return pos;
	}

	public static void main(String[] args) {
		BinarySearch binarySearch = new BinarySearch();
		int[] array = new int[50];
		for (int i = 0; i < 50; i++) {
			array[i] = i + 1;
		}

		binarySearch.searchIndex(array, 10);
		binarySearch.searchIndex(array, 50);
		binarySearch.searchIndex(array, 0);
		binarySearch.searchIndex(array, 51);
	}
}
