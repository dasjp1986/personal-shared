package com.da.coding.graphs;

/**
 * 
 * @author dagraw1
 *
 *         Given a boolean 2D matrix (0-based index), find whether there is path
 *         from (0,0) to (x,y) and if there is one path, print the minimum no of
 *         steps needed to reach it, else print -1 if the destination is not
 *         reachable. You may move in only four direction ie up, down, left and
 *         right. The path can only be created out of a cell if its value is 1.
 */
public class ShortestSourceToDest {

	// Time and Space complexity- O(m*n)
	public int shortestPath(int[][] matrix, int desti, int destj) {
		int[] rows = { 0, 0, 1, -1 };
		int[] columns = { 1, -1, 0, 0 };

		boolean[][] visited = new boolean[matrix.length][matrix[0].length];
		return dfs(matrix, 0, 0, visited, rows, columns, desti, destj);
	}

	private int dfs(int[][] matrix, int row, int col, boolean[][] visited, int[] rows, int[] columns, int desti,
			int destj) {
		visited[row][col] = true;
		int count = 0;
		if (row == desti && col == destj) {
			return count;
		}

		for (int k = 0; k < columns.length; k++) {
			if (isSafe(matrix, row + rows[k], col + columns[k], visited))
				count = 1 + dfs(matrix, row + rows[k], col + columns[k], visited, rows, columns, desti, destj);
		}

		return count;
	}

	private boolean isSafe(int[][] matrix, int row, int col, boolean[][] visited) {
		if (row >= 0 && row < matrix.length && col >= 0 && col < matrix[0].length && !visited[row][col]
				&& matrix[row][col] == 1)
			return true;
		return false;
	}

	public static void main(String[] args) {
		int[][] matrix = { { 1, 1, 1, 1 }, { 0, 0, 0, 1 }, { 0, 0, 0, 1 } };
		// int[][] matrix={{1,1,0},{0,0,1},{1,0,1}};
		ShortestSourceToDest path = new ShortestSourceToDest();
		System.out.println(path.shortestPath(matrix, 0, 3));
	}

}
