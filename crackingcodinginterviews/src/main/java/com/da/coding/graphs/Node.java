package com.da.coding.graphs;

public class Node {

	int key;
	int value;

	public Node(int key, int value) {
		this.key = key;
		this.value = value;
	}

}
