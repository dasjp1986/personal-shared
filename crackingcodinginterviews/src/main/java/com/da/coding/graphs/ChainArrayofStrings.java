package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Find if an array of strings can be chained to form a circle | Set 1
 *         Given an array of strings, find if the given strings can be chained
 *         to form a circle. A string X can be put before another string Y in
 *         circle if the last character of X is same as first character of Y.
 * 
 *         Examples:
 * 
 *         Input: arr[] = {"geek", "king"} Output: Yes, the given strings can be
 *         chained. Note that the last character of first string is same as
 *         first character of second string and vice versa is also true.
 * 
 *         Input: arr[] = {"for", "geek", "rig", "kaf"} Output: Yes, the given
 *         strings can be chained. The strings can be chained as "for", "rig",
 *         "geek" and "kaf"
 * 
 *         Input: arr[] = {"aab", "bac", "aaa", "cda"} Output: Yes, the given
 *         strings can be chained. The strings can be chained as "aaa", "aab",
 *         "bac" and "cda"
 * 
 *         Input: arr[] = {"aaa", "bbb", "baa", "aab"}; Output: Yes, the given
 *         strings can be chained. The strings can be chained as "aaa", "aab",
 *         "bbb" and "baa"
 * 
 *         Input: arr[] = {"aaa"}; Output: Yes
 * 
 *         Input: arr[] = {"aaa", "bbb"}; Output: No
 * 
 *         Input : arr[] = ["abc", "efg", "cde", "ghi", "ija"] Output : Yes
 *         These strings can be reordered as, “abc”, “cde”, “efg”, “ghi”, “ija”
 * 
 *         Input : arr[] = [“ijk”, “kji”, “abc”, “cba”] Output : No
 */
public class ChainArrayofStrings {
	
	public static void main(String[] args) {
		ChainArrayofStrings chainArrayofStrings= new ChainArrayofStrings();
		String arr1[] = {"geek", "king"};
		System.out.println(chainArrayofStrings.canBeChained(arr1));
		
		String arr2[] = {"for", "geek", "rig", "kaf"};
		System.out.println(chainArrayofStrings.canBeChained(arr2));
		
		String arr3[] = {"aab", "bac", "aaa", "cda"};
		System.out.println(chainArrayofStrings.canBeChained(arr3));
		
		String arr4[] = {"aaa", "bbb", "baa", "aab"};
		System.out.println(chainArrayofStrings.canBeChained(arr4));
		
		String arr5[] = {"aaa"};
		System.out.println(chainArrayofStrings.canBeChained(arr5));
		
		String arr6[] = {"aaa", "bbb"};
		System.out.println(chainArrayofStrings.canBeChained(arr6));
		
		String arr7[] = {"abc", "efg", "cde", "ghi", "ija"};
		System.out.println(chainArrayofStrings.canBeChained(arr7));
		
		String arr8[] = {"ijk", "kji", "abc", "cba"};
		System.out.println(chainArrayofStrings.canBeChained(arr8));
	}
	
	
	//Time and space complexity- O(2n)
	public boolean canBeChained(String[] input) {
		Map<Character, Boolean> visited = new HashMap<>();
		Map<Character, List<Character>> adjList = createGraph(input, visited);
		dfs(input[0].charAt(0), adjList, visited);
		return checkAllVisited(visited);
	}

	private boolean checkAllVisited(Map<Character, Boolean> visited) {
		for(Boolean isVisited: visited.values()){
			if(!isVisited){
				return false;
			}
		}
		return true;
	}

	private void dfs(Character chNode, Map<Character, List<Character>> adjList, Map<Character, Boolean> visited) {
		if (visited.get(chNode)) {
			return;
		} else {
			visited.put(chNode, true);
			for (Character ch : adjList.get(chNode)) {
				dfs(ch, adjList, visited);
			}
		}
	}

	private Map<Character, List<Character>> createGraph(String[] input, Map<Character, Boolean> visited) {
		Map<Character, List<Character>> adjList = new HashMap<>();
		for (String str : input) {
			Character start = str.charAt(0);
			Character end = str.charAt(str.length() - 1);

			List<Character> list = adjList.get(start);
			if (list == null) {
				list = new ArrayList<>();
				adjList.put(start, list);
			}
			list.add(end);
			visited.put(start, false);
			visited.put(end, false);
		}
		return adjList;
	}
}
