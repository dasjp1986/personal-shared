package com.da.coding.graphs;

public class GraphEdge {

	private Integer start;
	private Integer end;
	private Integer cost;

	public GraphEdge(Integer start, Integer end, Integer cost) {
		this.start = start;
		this.end = end;
		this.cost = cost;
	}

	public Integer getStart() {
		return start;
	}

	public Integer getEnd() {
		return end;
	}

	public Integer getCost() {
		return cost;
	}

	@Override
	public String toString() {
		return "GraphEdge [start=" + start + ", end=" + end + ", cost=" + cost + "]";
	}
	
	

}
