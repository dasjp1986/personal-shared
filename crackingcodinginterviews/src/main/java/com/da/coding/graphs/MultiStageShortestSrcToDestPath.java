package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 * 
 *         Given a graph and source and destination nodes. find minimum path to
 *         reach destination from source node.
 * 
 *         Multi Stage algorithm has two versions- forward and backward here is
 *         the implementation of forward multi stage algorithm costs are
 *         calculated from back to front but path finding happens from front to
 *         the back.
 */
public class MultiStageShortestSrcToDestPath {
	public List<Integer> shortestPath(int[][] weightMatrix, int src, int dest) {
		List<Integer> path = new ArrayList<>();
		int[] cost = new int[weightMatrix.length];
		int[] intermediate = new int[weightMatrix.length];
		intermediate[intermediate.length-1]= dest;
		
		for (int i = weightMatrix.length - 2; i >= 0; i--) {
			int min = Integer.MAX_VALUE;
			int d= i;
			for (int j = i + 1; j < intermediate.length; j++) {
				if (weightMatrix[i][j] != 0 && (weightMatrix[i][j] + cost[j]) < min) {
					min = weightMatrix[i][j] + cost[j];
					d= j;
				}
			}
			cost[i] = min;
			intermediate[i]= d;
		}

		path.add(src);
		int vertex = intermediate[0];
		while (intermediate[vertex] != vertex) {
			path.add(vertex);
			vertex = intermediate[vertex];
		}
		path.add(dest);
		return path;
	}

	public static void main(String[] args) {
		int[][] weightMatrix = { { 0, 2, 1, 3, 0, 0, 0, 0 }, { 0, 0, 0, 0, 2, 3, 0, 0 }, { 0, 0, 0, 0, 6, 7, 0, 0 },
				{ 0, 0, 0, 0, 6, 8, 9, 0 }, { 0, 0, 0, 0, 0, 0, 0, 6 }, { 0, 0, 0, 0, 0, 0, 0, 4 },
				{ 0, 0, 0, 0, 0, 0, 0, 5 }, { 0, 0, 0, 0, 0, 0, 0, 0 } };
		
		MultiStageShortestSrcToDestPath destPath= new MultiStageShortestSrcToDestPath();
		System.out.println(destPath.shortestPath(weightMatrix, 0, 7));
	}
}
