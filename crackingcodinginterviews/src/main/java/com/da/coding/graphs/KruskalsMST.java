package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Implement Kruskal algorithm to find Mninimum Spanning Tree in
 *         weighted, connected and undirected graph
 *         
 *         Greedy Approach
 */
public class KruskalsMST {
	public int mst(int[][] weightMatrix) {
		int cost = 0;

		List<GraphEdge> edges = new ArrayList<>();
		for (int i = 0; i < weightMatrix.length; i++) {
			for (int j = i + 1; j < weightMatrix.length; j++) {
				if (weightMatrix[i][j] != 0)
					edges.add(new GraphEdge(i, j, weightMatrix[i][j]));
			}
		}

		Collections.sort(edges, new Comparator<GraphEdge>() {

			@Override
			public int compare(GraphEdge o1, GraphEdge o2) {
				return o1.getCost().compareTo(o2.getCost());
			}
		});

		int[] parent = new int[weightMatrix.length];
		for (int i = 0; i < parent.length; i++) {
			parent[i] = -1;
		}
		int count = weightMatrix.length - 1;
		for (GraphEdge edge : edges) {
			if (count > 0 && findUnion(edge, parent)) {
				cost += edge.getCost();
				System.out.println("Added " + edge);
				count--;
			}
		}
		return cost;
	}

	public static void main(String[] args) {
//		int[][] weightMatrix = { { 0, 1, 3, 0 }, { 1, 0, 2, 0 }, { 3, 2, 0, 4 }, { 0, 0, 4, 0 } };

		int[][] weightMatrix = { { 0, 28, 0, 0, 0, 10, 0 }, { 28, 0, 16, 0, 0, 0, 14 }, { 0, 16, 0, 12, 0, 0, 0 },
				{ 0, 0, 12, 0, 22, 0, 18 }, { 0, 0, 0, 22, 0, 25, 24 }, { 10, 0, 0, 0, 25, 0, 0 },
				{ 0, 14, 0, 18, 24, 0, 0 } };

		KruskalsMST algo = new KruskalsMST();
		System.out.println(algo.mst(weightMatrix));
	}

	private boolean findUnion(GraphEdge edge, int[] parent) {
		int ps = getParent(edge.getStart(), parent);
		int pe = getParent(edge.getEnd(), parent);

		if (ps == pe) {
			return false;
		}
		if (Math.abs(parent[ps]) >= Math.abs(parent[pe])) {
			parent[ps] = parent[ps] + parent[edge.getEnd()];
			parent[pe] = ps;
		} else {
			parent[pe] = parent[pe] + parent[edge.getStart()];
			parent[ps] = pe;
		}
		return true;
	}

	private int getParent(int start, int[] parent) {
		int p = start;

		while (parent[p] >= 0) {
			p = parent[p];
		}

		return p;
	}
}
