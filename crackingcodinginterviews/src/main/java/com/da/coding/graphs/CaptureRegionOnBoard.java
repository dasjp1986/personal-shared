package com.da.coding.graphs;

import java.util.ArrayList;

public class CaptureRegionOnBoard {

	public void solve(ArrayList<ArrayList<Character>> str) {
		int m = str.size();
		int n = str.get(0).size();
		int k = Math.max(m, n);

		int[] x = { -1, 1, 0, 0 };
		int[] y = { 0, 0, -1, 1 };

		for (int i = 0; i < k; i++) {

			if (i < m && str.get(i).get(0) == 'O') {
				dfs(i, 0, str, x, y, m, n);
			}

			if (i < m && str.get(i).get(n - 1) == 'O') {
				dfs(i, n - 1, str, x, y, m, n);
			}

			if (i < n && str.get(0).get(i) == 'O') {
				dfs(0, i, str, x, y, m, n);
			}

			if (i < n && str.get(m - 1).get(i) == 'O') {
				dfs(m - 1, i, str, x, y, m, n);
			}
		}

		for (int i = 1; i < m-1; i++) {
			for (int j = 1; j < n-1; j++) {
				if (str.get(i).get(j) == '*') {
					str.get(i).set(j, 'O');
				} else if (str.get(i).get(j) == 'O') {
					str.get(i).set(j, 'X');
				}
			}
		}
	}

	private void dfs(int i, int j, ArrayList<ArrayList<Character>> str, int[] x, int[] y, int m, int n) {
		for (int k = 0; k < x.length; k++) {
			int row = i + x[k];
			int col = j + y[k];
			if (row > 0 && row < m - 1 && col > 0 && col < n - 1 && str.get(row).get(col) == 'O') {
				str.get(row).set(col, '*');
				dfs(row, col, str, x, y, m, n);
			}
		}
	}

	public static void main(String[] args) {
		ArrayList<ArrayList<Character>> str = new ArrayList<>();
		ArrayList<Character> l = new ArrayList<>();
		l.add('X');
		l.add('O');
		l.add('X');

		str.add(l);

		l = new ArrayList<>();
		l.add('X');
		l.add('O');
		l.add('X');
		str.add(l);

		l = new ArrayList<>();
		l.add('X');
		l.add('O');
		l.add('X');
		str.add(l);

		CaptureRegionOnBoard board = new CaptureRegionOnBoard();
		board.solve(str);
		System.out.println(str);
	}

}
