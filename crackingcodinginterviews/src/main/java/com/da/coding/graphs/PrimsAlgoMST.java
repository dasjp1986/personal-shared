package com.da.coding.graphs;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author dagraw1
 *
 *         Prim's algorithm to find Minimum Spanning Tree
 * 
 *         Greedy Approach
 */
public class PrimsAlgoMST {
	public int mst(int[][] weightMatrix) {
		int cost = 0;
		Map<Integer, Integer> universal = new HashMap<>();
		universal.put(0, 0);
		for (int i = 1; i < weightMatrix.length; i++) {
			universal.put(i, Integer.MAX_VALUE);
		}
		Node node = new Node(0, 0);
		for (int count = 1; count < weightMatrix.length; count++) {
			update(universal, node.key, weightMatrix);
			node = findMin(universal);
			if (node != null) {
				cost += node.value;
			}
		}
		return cost;
	}

	private Node findMin(Map<Integer, Integer> universal) {
		int min = Integer.MAX_VALUE;
		Node node = null;
		for (Entry<Integer, Integer> entry : universal.entrySet()) {
			if (entry.getValue() < min) {
				min = entry.getValue();
				node = new Node(entry.getKey(), entry.getValue());
			}
		}
		return node;
	}

	private void update(Map<Integer, Integer> universal, int vertex, int[][] weightMatrix) {
		for (int j = 0; j < weightMatrix.length; j++) {
			if (universal.get(j) != null && weightMatrix[vertex][j] != 0) {
				if (weightMatrix[vertex][j] < universal.get(j)) {
					universal.put(j, weightMatrix[vertex][j]);
				}
			}
		}
		universal.remove(vertex);
	}

	public static void main(String[] args) {
		int[][] weightMatrix = { { 0, 28, 0, 0, 0, 10, 0 }, { 28, 0, 16, 0, 0, 0, 14 }, { 0, 16, 0, 12, 0, 0, 0 },
				{ 0, 0, 12, 0, 22, 0, 18 }, { 0, 0, 0, 22, 0, 25, 24 }, { 10, 0, 0, 0, 25, 0, 0 },
				{ 0, 14, 0, 18, 24, 0, 0 } };

//		int[][] weightMatrix = { { 0, 1, 3, 0 }, { 1, 0, 2, 0 }, { 3, 2, 0, 4 }, { 0, 0, 4, 0 } };

		PrimsAlgoMST algo = new PrimsAlgoMST();
		System.out.println(algo.mst(weightMatrix));
	}

}
