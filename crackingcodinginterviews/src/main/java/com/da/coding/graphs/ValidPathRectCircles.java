package com.da.coding.graphs;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         There is a rectangle with left bottom as (0, 0) and right up as (x,
 *         y). There are N circles such that their centers are inside the
 *         rectangle. Radius of each circle is R. Now we need to find out if it
 *         is possible that we can move from (0, 0) to (x, y) without touching
 *         any circle.
 * 
 *         Note : We can move from any cell to any of its 8 adjecent neighbours
 *         and we cannot move outside the boundary of the rectangle at any point
 *         of time.
 * 
 * 
 *         Input Format
 * 
 *         1st argument given is an Integer x. 2nd argument given is an Integer
 *         y. 3rd argument given is an Integer N, number of circles. 4th
 *         argument given is an Integer R, radius of each circle. 5th argument
 *         given is an Array A of size N, where A[i] = x cordinate of ith circle
 *         6th argument given is an Array B of size N, where B[i] = y cordinate
 *         of ith circle
 * 
 *         Output Format
 * 
 *         Return YES or NO depending on weather it is possible to reach cell
 *         (x,y) or not starting from (0,0).
 * 
 *         Constraints
 * 
 *         0 <= x, y, R <= 100 1 <= N <= 1000 Center of each circle would lie
 *         within the grid
 * 
 *         For Example
 * 
 *         Input: x = 2 y = 3 N = 1 R = 1 A = [2] B = [3] Output: NO
 * 
 *         Explanation: There is NO valid path in this case
 * 
 * 
 */

public class ValidPathRectCircles {

	public String solve(int m, int n, int circles, int radius, int[] x, int[] y) {
		boolean[][] adjMatrix = new boolean[m][n];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				adjMatrix[i][j] = true;
			}
		}

		// this needs correction
		setObstacles(m, n, adjMatrix, radius, x, y, circles);

		return bfs(adjMatrix, m, n);
	}

	private void setObstacles(int m, int n, boolean[][] matrix, int radius, int[] x, int[] y, int circles) {

		for (int circle = 0; circle < circles; circle++) {
			for (int row = x[circle] - radius; row < x[circle] + radius; row++) {
				if (row < 0 || row >= m) {
					continue;
				}
				for (int col = y[circle] - radius; col < y[circle] + radius; col++) {
					if (col < 0 || col >= n) {
						continue;
					}
					matrix[row][col] = false;
				}
			}
		}

	}

	private String bfs(boolean[][] matrix, int m, int n) {
		Queue<Integer> q = new LinkedList<>();
		q.add(0);

		while (!q.isEmpty()) {
			int i = q.remove();
			for (int j = 0; j < n; j++) {
				if (i >= m) {
					continue;
				}
				if (matrix[i][j]) {
					q.add(j);
					//to mark visited
					matrix[i][j]= false;
					if (i == m - 1 && j == n - 1) {
						return "YES";
					}
				}
			}
		}

		return "NO";
	}

	public static void main(String[] args) {
		ValidPathRectCircles vc = new ValidPathRectCircles();
		System.out.println(vc.solve(4, 5, 1, 1, new int[] { 2 }, new int[] { 3 }));
	}

}
