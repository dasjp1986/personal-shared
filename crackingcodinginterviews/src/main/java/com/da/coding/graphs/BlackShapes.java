package com.da.coding.graphs;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given N * M field of O's and X's, where O=white, X=black Return the
 *         number of black shapes. A black shape consists of one or more
 *         adjacent X's (diagonals not included)
 * 
 *         Example:
 * 
 *         OOOXOOO OOXXOXO OXOOOXO
 * 
 *         answer is 3 shapes are : (i) X X X (ii) X (iii) X X
 * 
 *         Note that we are looking for connected shapes here.
 * 
 *         For example,
 * 
 *         XXX XXX XXX
 * 
 *         is just one single connected black shape.
 * 
 */
public class BlackShapes {

	public int black(String[] A) {
		int[] x = { -1, 1, 0, 0 };
		int[] y = { 0, 0, -1, 1 };

		int m = A.length;
		int n = A[0].length();
		int[][] matrix = new int[m][n];

		int count = 0;

		Queue<Coord> q = new LinkedList<>();

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (A[i].charAt(j) == 'O' || matrix[i][j] == 1) {
					continue;
				}

				Coord coord = new Coord(i, j);
				q.add(coord);

				while (!q.isEmpty()) {
					coord = q.remove();

					matrix[coord.x][coord.y] = 1;

					for (int k = 0; k < x.length; k++) {
						Coord c = valid(coord.x + x[k], coord.y + y[k], A, matrix);
						if (c != null) {
							matrix[c.x][c.y] = 1;
							q.add(c);
						}
					}

				}

				count++;
			}
		}

		return count;
	}

	private Coord valid(int r, int c, String[] A, int[][] matrix) {
		Coord cor = null;

		if (r < 0 || r >= matrix.length || c < 0 || c >= matrix[0].length) {
			return cor;
		}
		if (A[r].charAt(c) == 'X' && matrix[r][c] == 0) {
			cor = new Coord(r, c);
		}
		return cor;
	}

	public static void main(String[] args) {
		BlackShapes shapes = new BlackShapes();
		String[] sh = { "OOOXOOO", "OOXXOXO", "OXOOOXO" };
		System.out.println(shapes.black(sh));
	}

}

class Coord {
	int x;
	int y;

	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
