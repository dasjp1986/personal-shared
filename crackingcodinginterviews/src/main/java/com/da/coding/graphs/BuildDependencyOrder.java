package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author dagraw1
 *
 *         Given pairs of projects to be built, pair can look like (a,b) where b
 *         is dependent on a. Find the correct build order given such pairs else
 *         throw an exception
 * 
 *         Ex- {(a,b),(a,d),(b,c),(d,c),(c,e)} Output- a,b,d,c,e
 */
public class BuildDependencyOrder {

	public List<Character> buildOrder(char[][] pairs) {
		List<Character> orderList = new ArrayList<>();
		Set<Character> nodes = new LinkedHashSet<>();
		Map<Character, Set<Character>> outgoing = new HashMap<>();
		Map<Character, Set<Character>> incoming = new HashMap<>();

		for (int i = 0; i < pairs.length; i++) {
			addToMap(outgoing, pairs[i][0], pairs[i][1]);
			addToMap(incoming, pairs[i][1], pairs[i][0]);
			nodes.add(pairs[i][0]);
			nodes.add(pairs[i][1]);
		}

		while (!nodes.isEmpty()) {
			traverse(orderList, nodes, outgoing, incoming);
		}

		return orderList;
	}

	private void traverse(List<Character> orderList, Set<Character> nodes, Map<Character, Set<Character>> outgoing,
			Map<Character, Set<Character>> incoming) {
		Iterator<Character> iterator = nodes.iterator();
		boolean isFound = false;
		while (iterator.hasNext()) {
			Character node = iterator.next();
			Set<Character> set = incoming.get(node);
			if (set == null || set.isEmpty()) {
				Set<Character> childrens = outgoing.get(node);
				if (childrens != null) {
					for (Character child : childrens) {
						incoming.get(child).remove(node);
					}
				}
				orderList.add(node);
				iterator.remove();
				isFound = true;
				break;
			}
		}
		if (!isFound)
			throw new RuntimeException("Invalid Build Order...");
	}

	private void addToMap(Map<Character, Set<Character>> map, char src, char dest) {
		Set<Character> set = map.get(src);
		if (set == null) {
			set = new HashSet<>();
			map.put(src, set);
		}
		set.add(dest);
	}

	public static void main(String[] args) {
		char[][] inputPairs = { { 'a', 'b' }, { 'a', 'd' }, { 'b', 'c' }, { 'd', 'c' }, { 'c', 'e' }, { 'c', 'f' } };
		BuildDependencyOrder buildDependencyOrder = new BuildDependencyOrder();
		System.out.println(buildDependencyOrder.buildOrder(inputPairs));
		
		char[][] inputPairs2 = { { '1', '2' }, { '2', '4' }, { '4', '5' }, { '4', '6' }, { '3', '4' }, { '9', '8' } };
		System.out.println(buildDependencyOrder.buildOrder(inputPairs2));

	}
}
