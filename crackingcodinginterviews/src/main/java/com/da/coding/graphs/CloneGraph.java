package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * 
 * @author dagraw1
 *
 *         Clone the given graph, make sure self-loops are handled
 */

public class CloneGraph {

	public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
		Map<Integer, UndirectedGraphNode> map = new HashMap<>();
		Map<Integer, Boolean> visited = new HashMap<>();
		Queue<UndirectedGraphNode> q = new LinkedList<>();

		UndirectedGraphNode root = new UndirectedGraphNode(node.label);
		map.put(root.label, root);
		visited.put(root.label, true);

		q.add(node);

		UndirectedGraphNode temp = null;

		while (!q.isEmpty()) {
			temp = q.remove();

			UndirectedGraphNode cn = map.get(temp.label);
			for (UndirectedGraphNode neigh : temp.neighbors) {
				if (visited.get(neigh.label) == null) {
					q.add(neigh);
					visited.put(neigh.label, true);
				}

				UndirectedGraphNode cc = map.get(neigh.label);
				if (cc == null) {
					cc = new UndirectedGraphNode(neigh.label);
				}
				map.put(neigh.label, cc);
				cn.neighbors.add(cc);
			}
		}

		return root;

	}

	public static void main(String[] args) {
		UndirectedGraphNode node703 = new UndirectedGraphNode(703);
		UndirectedGraphNode node43 = new UndirectedGraphNode(43);
		UndirectedGraphNode node279 = new UndirectedGraphNode(279);

		node703.neighbors.add(node43);
		node703.neighbors.add(node279);
		node703.neighbors.add(node703);

		node43.neighbors.add(node279);
		node43.neighbors.add(node703);

		node279.neighbors.add(node43);
		node279.neighbors.add(node279);
		node279.neighbors.add(node703);

		CloneGraph graph = new CloneGraph();
		System.out.println(graph.cloneGraph(node703));
	}

}

class UndirectedGraphNode {
	int label;
	List<UndirectedGraphNode> neighbors;

	UndirectedGraphNode(int x) {
		label = x;
		neighbors = new ArrayList<UndirectedGraphNode>();
	}

	@Override
	public String toString() {
		return "UndirectedGraphNode [label=" + label + "]";
	}

};
