package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.Arrays;

public class WordLadder2 {

	public ArrayList<ArrayList<String>> findLadders(String start, String end, ArrayList<String> dict) {

		ArrayList<ArrayList<String>> results = new ArrayList<>();
		ArrayList<String> result = new ArrayList<>();
		ArrayList<Integer> availableIndices = new ArrayList<>();

		if (dict.size() == 2) {
			result.add(start);
			if (!start.equals(end)) {
				result.add(end);
			}

			results.add(result);

			return results;
		}

		for (int i = 0; i < start.length(); i++) {
			availableIndices.add(i);
		}
		result.add(start);

		dfs(results, result, start, end, dict, availableIndices);

		return results;
	}

	private void dfs(ArrayList<ArrayList<String>> results, ArrayList<String> result, String start, String end,
			ArrayList<String> dict, ArrayList<Integer> availableIndices) {
		//System.out.println("result: " + result + ", start : " + start + ", dict : " + dict);
		if (availableIndices.isEmpty()) {
			String last = result.get(result.size() - 1);
			if (valid(last, end)) {
				result.add(end);
				results.add(result);
			}
		} else {
			boolean noMatch = true;
			for (String word : dict) {
				Integer ind = find(availableIndices, word, start);
				if (ind == -1) {
					continue;
				}

				noMatch = false;
				ArrayList<String> cloneResult = new ArrayList<>(result);
				cloneResult.add(word);
				ArrayList<String> cloneDict = new ArrayList<>(dict);
				cloneDict.remove(word);
				availableIndices.remove(ind);
				dfs(results, cloneResult, word, end, cloneDict, availableIndices);
				availableIndices.add(ind);
			}
			
			if(noMatch) {
				dfs(results, result, start, end, dict, new ArrayList<>());
			}
		}
	}

	private Integer find(ArrayList<Integer> indexes, String word, String start) {
		Integer ind = -1;

		int count = 0;

		for (int i = word.length() - 1; i >= 0; i--) {
			if (word.charAt(i) != start.charAt(i)) {
				ind = i;
				count++;
			}
		}

		return count == 1 && indexes.contains(ind) ? ind : -1;
	}

	private boolean valid(String s1, String s2) {
		int count = 0;
		for (int i = 0; i < s1.length(); i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				count++;
			}
			if (count > 1) {
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) {
		WordLadder2 ladder = new WordLadder2();
//		ArrayList<String> dict = new ArrayList(Arrays.asList("hot", "dot", "dog", "lot", "log"));
//
//		System.out.println(ladder.findLadders("hit", "cog", dict));

		ArrayList<String> dict = new ArrayList(Arrays.asList("baba", "abba", "aaba", "bbbb", "abaa", "abab", "aaab",
				"abba", "abba", "abba", "bbba", "aaab", "abaa", "baba", "baaa"));

		System.out.println(ladder.findLadders("bbaa", "babb", dict));
	}
}
