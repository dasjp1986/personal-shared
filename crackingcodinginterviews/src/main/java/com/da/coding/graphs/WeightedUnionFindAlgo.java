package com.da.coding.graphs;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 * 
 * 
 *         Useful in detecting a cycle in undirected graph
 */
public class WeightedUnionFindAlgo {

	public boolean isCycle(boolean[][] adjMatrix) {
		int[] parent = new int[adjMatrix.length];
		for (int i = 0; i < parent.length; i++) {
			parent[i] = -1;
		}

		for (int i = 0; i < parent.length; i++) {
			for (int j = i + 1; j < parent.length; j++) {
				if (adjMatrix[i][j]) {
					int pi = getParent(i, parent);
					int pj = getParent(j, parent);

					if (pi == pj) {
						return true;
					}
					if (Math.abs(parent[pi]) >= Math.abs(parent[pj])) {
						parent[pi] = parent[pi] + parent[j];
						parent[pj] = pi;
					} else {
						parent[pj] = parent[pj] + parent[i];
						parent[pi] = pj;
					}

				}
			}
		}
		System.out.println(Arrays.toString(parent));
		return false;
	}

	private int getParent(int i, int[] parent) {
		int p = i;
		while (parent[p] >= 0) {
			p = parent[p];
		}
		return p;
	}

	public static void main(String[] args) {
		boolean[][] matrix = new boolean[6][6];
		matrix[0][1] = true;
		matrix[1][2] = true;
		matrix[2][3] = true;
		matrix[3][4] = true;
		matrix[0][4] = true;

		WeightedUnionFindAlgo algo = new WeightedUnionFindAlgo();
		System.out.println(algo.isCycle(matrix));
	}

}
