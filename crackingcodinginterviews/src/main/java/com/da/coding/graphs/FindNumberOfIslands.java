package com.da.coding.graphs;
/**
 * 
 * @author dagraw1
 *
 *Given a boolean 2D matrix, find the number of islands. A group of connected 1s forms an island. For example, the below matrix contains 5 islands

Example:

Input : mat[][] = {{1, 1, 0, 0, 0},
                   {0, 1, 0, 0, 1},
                   {1, 0, 0, 1, 1},
                   {0, 0, 0, 0, 0},
                   {1, 0, 1, 0, 1} 
Output : 5
This is a variation of the standard problem: “Counting the number of connected components in an undirected graph”.
 */
public class FindNumberOfIslands {
	//Time and Space complexity- O(m*n)
	public int islands(int[][] matrix) {
		int count = 0;
		int[] rows = { -1, -1, -1, 0, 0, 1, 1, 1 };
		int[] columns = { -1, 0, 1, -1, 1, -1, 0, 1 };

		boolean[][] visited = new boolean[matrix.length][matrix[0].length];
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				if (isSafe(matrix, row, col, visited)) {
					dfs(matrix, row, col, visited, rows, columns);
					count++;
				}
			}
		}
		return count;
	}

	private void dfs(int[][] matrix, int row, int col, boolean[][] visited, int[] rows, int[] columns) {
		visited[row][col] = true;
		for (int k = 0; k < columns.length; k++) {
			if (isSafe(matrix, row + rows[k], col + columns[k], visited))
				dfs(matrix, row + rows[k], col + columns[k], visited, rows, columns);
		}
	}

	private boolean isSafe(int[][] matrix, int row, int col, boolean[][] visited) {
		if (row >= 0 && row < matrix.length && col >= 0 && col < matrix[0].length && !visited[row][col]
				&& matrix[row][col] == 1)
			return true;
		return false;
	}

	public static void main(String[] args) {
		int[][] matrix = { { 1, 1, 0, 0, 0 }, { 0, 1, 0, 0, 1 }, { 1, 0, 0, 1, 1 }, { 0, 0, 0, 0, 0 },
				{ 1, 0, 1, 0, 1 } };
		// int[][] matrix={{1,1,0},{0,0,1},{1,0,1}};
		FindNumberOfIslands islands = new FindNumberOfIslands();
		System.out.println(islands.islands(matrix));
	}
}
