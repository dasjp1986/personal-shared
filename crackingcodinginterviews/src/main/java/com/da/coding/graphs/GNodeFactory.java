package com.da.coding.graphs;

import java.util.HashMap;
import java.util.Map;

public class GNodeFactory<T> {
	private Map<T, GNode<T>> cache;

	public GNodeFactory() {
		cache = new HashMap<>();
	}

	public GNode<T> createNode(T value) {
		GNode<T> node = cache.get(value);
		if (node == null) {
			node= new GNode<T>(value);
			cache.put(value, node);
		}
		return node;
	}
}
