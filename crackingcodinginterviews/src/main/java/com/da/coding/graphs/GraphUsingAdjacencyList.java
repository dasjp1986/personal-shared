package com.da.coding.graphs;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.da.coding.commons.Constants.GRAPH_TYPE;
import com.da.coding.queues.QueueUsingSingleLinkedList;
import com.da.coding.stack.StackUsingLinkedList;

public class GraphUsingAdjacencyList<T> {
	private Set<GNode<T>> nodes;
	private GNodeFactory<T> factory;
	private GRAPH_TYPE type;

	public GraphUsingAdjacencyList(GRAPH_TYPE type) {
		this.nodes = new LinkedHashSet<>();
		this.type = type;
		this.factory = new GNodeFactory<>();
	}

	public Set<GNode<T>> getNodes() {
		return nodes;
	}

	public void setNodes(Set<GNode<T>> nodes) {
		this.nodes = nodes;
	}

	public boolean isEmpty() {
		return nodes.isEmpty();
	}

	public GRAPH_TYPE getType() {
		return type;
	}

	public void add(T src, T dest) {
		GNode<T> srcNode = factory.createNode(src);
		GNode<T> destNode = factory.createNode(dest);

		srcNode.getNodes().add(destNode);
		nodes.add(srcNode);
		nodes.add(destNode);
		if (type == GRAPH_TYPE.UNDIRECTED) {
			destNode.getNodes().add(srcNode);
		}
	}

	public void bfs(T startNode) {
		if (isEmpty()) {
			throw new RuntimeException("Graph is empty...");
		}

		System.out.println("Printing graph using BFS...");
		Map<T, Boolean> visited = new HashMap<>();
		Set<GNode<T>> set = nodes;

		QueueUsingSingleLinkedList<GNode<T>> queue = new QueueUsingSingleLinkedList<>();

		GNode<T> node = factory.createNode(startNode);
		queue.add(node);
		visited.put(node.getValue(), true);

		while (!queue.isEmpty()) {
			node = queue.remove();

			set = node.getNodes();
			for (GNode<T> tNode : set) {
				if (visited.get(tNode.getValue()) == null) {
					queue.add(tNode);
					visited.put(tNode.getValue(), true);
				}

			}
			System.out.print(node.getValue() + " ");

		}

		System.out.println();
	}

	public void dfs(T startNode) {
		Map<T, Boolean> visited = new HashMap<>();
		GNode<T> node = factory.createNode(startNode);
		StackUsingLinkedList<GNode<T>> stack = new StackUsingLinkedList<>();

		stack.push(node);
		visited.put(node.getValue(), true);

		System.out.println("Printing nodes in graph DFS traversal...");
		while (!stack.isEmpty()) {
			node = stack.pop();

			for (GNode<T> tNode : node.getNodes()) {
				if (visited.get(tNode.getValue()) == null) {
					stack.push(tNode);
					visited.put(tNode.getValue(), true);
				}
			}
			System.out.print(node.getValue() + " ");

		}
		System.out.println();
	}

	public void dfsRecursive(T value) {
		System.out.println("DFS Recursive...");
		Map<T, Boolean> visited = new HashMap<>();
		visited.put(value, true);
		dfsRercs(factory.createNode(value), visited);
		System.out.println();
	}

	private void dfsRercs(GNode<T> node, Map<T, Boolean> visited) {
		System.out.print(node.getValue() + " ");
		Set<GNode<T>> nodes = node.getNodes();
		for (GNode<T> tNode : nodes) {
			if (visited.get(tNode.getValue()) == null) {
				visited.put(tNode.getValue(), true);
				dfsRercs(tNode, visited);
			}
		}

	}

	public void print() {
		System.out.println("Printing the Adjacency List...");
		for (GNode<T> node : nodes) {
			System.out.print(node.getValue() + "->");
			for (GNode<T> t : node.getNodes()) {
				System.out.print(t.getValue() + " ");
			}
			System.out.println();
		}
	}

	public GNode<T> getNode(T value) {
		return factory.createNode(value);
	}

	public static void main(String[] args) {
		GraphUsingAdjacencyList<Integer> graph = new GraphUsingAdjacencyList<>(GRAPH_TYPE.DIRECTED);
		// graph.add(1, 2);
		// graph.add(1, 3);
		// graph.add(2, 4);
		// graph.add(4, 3);
		// graph.add(4, 6);
		// graph.add(3, 5);
		// graph.add(5, 6);
		// graph.add(5, 7);
		//
		// graph.print();
		//
		// graph.bfs(1);
		// graph.dfs(1);
		//
		// graph = new GraphUsingAdjacencyList<>(GRAPH_TYPE.DIRECTED);
		// graph.add(1, 2);
		// graph.add(2, 8);
		// graph.add(8, 3);
		// graph.add(3, 9);
		// graph.add(9, 1);
		// graph.add(5, 1);
		// graph.add(3, 2);
		// graph.add(8, 5);
		//
		// graph.print();
		//
		// graph.bfs(1);
		// graph.dfs(1);

		graph.add(1, 2);
		graph.add(1, 3);
		graph.add(3, 4);
		graph.add(2, 4);
		graph.add(4, 5);
		graph.add(4, 6);

		graph.print();
		graph.bfs(1);
		graph.dfs(1);
		graph.dfsRecursive(1);

	}

}
