package com.da.coding.graphs;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author dagraw1
 * 
 *         Dijkstra Algorithm Find shortest path to each node from given source
 *         node
 * 
 *         Greedy Approach
 * 
 *         d[v]= (d[u]+weight(u,v)) < d[v]? d[u]+weight(u,v): d[v]
 *         
 *         Time Complexity- O(N^2)
 */
public class DijkstraAlgoShortestPath {
	public Map<Integer, Integer> shortestPaths(int[][] weightMatrix, int source) {
		Map<Integer, Integer> result = new LinkedHashMap<>();
		Map<Integer, Integer> universal = new HashMap<>();

		universal.put(source, 0);
		for (int i = 1; i < weightMatrix.length; i++) {
			universal.put(i, Integer.MAX_VALUE);
		}

		Node node = new Node(source, 0);
		for (int count = 1; count < weightMatrix.length; count++) {
			update(universal, node.key, node.value, weightMatrix);
			node = findMin(universal);
			if (node != null) {
				result.put(node.key, node.value);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		//int[][] weightMatrix = { { 0, 1, 3, 0 }, { 1, 0, 2, 0 }, { 3, 2, 0, 4 }, { 0, 0, 4, 0 } };
		
		int[][] weightMatrix = { { 0, 28, 0, 0, 0, 10, 0 }, { 28, 0, 16, 0, 0, 0, 14 }, { 0, 16, 0, 12, 0, 0, 0 },
				{ 0, 0, 12, 0, 22, 0, 18 }, { 0, 0, 0, 22, 0, 25, 24 }, { 10, 0, 0, 0, 25, 0, 0 },
				{ 0, 14, 0, 18, 24, 0, 0 } };

		DijkstraAlgoShortestPath algo = new DijkstraAlgoShortestPath();
		System.out.println(algo.shortestPaths(weightMatrix, 0));
	}

	private void update(Map<Integer, Integer> universal, int vertex, int distance, int[][] weightMatrix) {
		for (int j = 0; j < weightMatrix.length; j++) {
			if (universal.get(j) != null && weightMatrix[vertex][j] != 0) {
				if (weightMatrix[vertex][j] + distance < universal.get(j)) {
					universal.put(j, weightMatrix[vertex][j] + distance);
				}
			}
		}
		universal.remove(vertex);
	}

	private Node findMin(Map<Integer, Integer> universal) {
		int min = Integer.MAX_VALUE;
		Node node = null;
		for (Entry<Integer, Integer> entry : universal.entrySet()) {
			if (entry.getValue() < min) {
				min = entry.getValue();
				node = new Node(entry.getKey(), entry.getValue());
			}
		}
		return node;
	}
}
