package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *
 *
 * 
 *         There are a total of N courses you have to take, labeled from 1 to N.
 *         Some courses may have prerequisites, for example to take course 2 you
 *         have to first take course 1, which is expressed as a pair: [1,2].
 * 
 *         Given the total number of courses and a list of prerequisite pairs,
 *         is it possible for you to finish all courses. return 1/0 if it is
 *         possible/not possible. The list of prerequisite pair are given in two
 *         integer arrays B and C where B[i] is a prerequisite for C[i].
 * 
 *         Example:
 * 
 *         If N = 3 and the prerequisite pairs are [1,2] and [2,3], then you can
 *         finish courses in the following order: 1, 2 and 3.
 * 
 *         But if N = 2 and the prerequisite pairs are [1,2] and [2,1], then it
 *         is not possible for you to finish all the courses.
 * 
 * 
 */
public class CourseRquisites {

	public int solve(int A, int[] B, int[] C) {
		Map<Integer, List<Integer>> adj = new HashMap<>();

		int[] depend = new int[A + 1];
		List<Integer> zeros = new ArrayList<>();

		for (int i = 0; i < B.length; i++) {
			List<Integer> list = adj.get(B[i]);
			if (list == null) {
				list= new ArrayList<>();
				adj.put(B[i], list);
			}
			list.add(C[i]);
			adj.put(B[i], list);
			depend[C[i]] += 1;
		}

		for (int i = 1; i < depend.length; i++) {
			if (depend[i] == 0) {
				zeros.add(i);
			}
		}

		for (int i = 0; i < A; i++) {
			if (zeros.isEmpty()) {
				return 0;
			}

			int vertex = zeros.get(0);
			update(adj, depend, vertex, zeros);
			zeros.remove(0);

		}

		return 1;
	}

	private void update(Map<Integer, List<Integer>> adj, int[] depend, int vertex, List<Integer> zeros) {

		List<Integer> list = adj.get(vertex);
		if (list != null) {
			for (Integer v : list) {
				depend[v] -= 1;
				if (depend[v] == 0) {
					zeros.add(v);
				}
			}
		}

	}

	public static void main(String[] args) {
		CourseRquisites course = new CourseRquisites();
//		int[] B = { 67, 8, 48, 42, 35, 25, 37, 69, 31, 36, 7, 33, 2, 47, 42, 52, 31, 70, 29, 38, 36, 60, 15, 37, 33, 27,
//				4, 32, 43, 55, 49, 35, 21, 28, 62, 17, 2, 61, 54, 22, 9, 56, 12, 3, 60, 52, 21, 15, 54, 63, 33, 64, 38,
//				16, 59, 69, 49, 52, 10, 10, 6, 56, 43, 32, 41, 66, 6 };
//		int[] C = { 51, 43, 55, 27, 34, 8, 14, 5, 70, 64, 65, 57, 45, 19, 53, 50, 44, 51, 19, 41, 14, 68, 12, 58, 50,
//				66, 7, 47, 40, 62, 29, 5, 22, 39, 23, 34, 25, 4, 40, 26, 26, 45, 18, 28, 61, 59, 17, 46, 39, 46, 68, 24,
//				63, 59, 67, 53, 9, 11, 3, 44, 24, 37, 13, 1, 65, 18, 48 };

		int[] B = { 1, 3, 2};
		int[] C = { 2, 2, 4 };
		System.out.println(course.solve(4, B, C));
	}

}
