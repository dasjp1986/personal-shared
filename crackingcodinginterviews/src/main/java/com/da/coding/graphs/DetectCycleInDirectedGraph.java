package com.da.coding.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * 
 * @author dagraw1
 * 
 *         Given a directed graph, check whether the graph contains a cycle or
 *         not. Your function should return true if the given graph contains at
 *         least one cycle, else return false. For example, the following graph
 *         contains three cycles 0->2->0, 0->1->2->0 and 3->3, so your function
 *         must return true.
 */
public class DetectCycleInDirectedGraph {
	Map<Integer, List<Integer>> adjacencyList;

	public DetectCycleInDirectedGraph() {
		adjacencyList = new HashMap<>();
	}

	public void addEdge(Integer a, Integer b) {
		List<Integer> list = adjacencyList.get(a);
		if (list == null) {
			list = new ArrayList<>();
			adjacencyList.put(a, list);
		}
		list.add(b);
	}

	public void dfs(Integer node, Map<Integer, Boolean> visited) {
		if (visited.get(node) == null) {
			visited.put(node, true);
			System.out.println(node);
			List<Integer> list = adjacencyList.get(node);
			if (list != null) {
				for (Integer n : list) {
					dfs(n, visited);
				}
			}
		}
	}

	public boolean detectCycles(Integer node, Map<Integer, Boolean> visited, List<Integer> inprocess) {
		if (inprocess.contains(node)) {
			return true;
		}
		if (visited.get(node) == null) {
			visited.put(node, true);
			inprocess.add(node);
			System.out.println(node);
			List<Integer> list = adjacencyList.get(node);
			if (list != null) {
				for (Integer n : list) {
					if (detectCycles(n, visited, inprocess)) {
						return true;
					}
					inprocess.remove(n);
				}
			}
		}
		return false;
	}

	public void bfs(Integer node, Map<Integer, Boolean> visited) {
		Queue<Integer> queue = new LinkedList<>();
		queue.add(node);

		while (!queue.isEmpty()) {
			Integer n = queue.remove();
			if (visited.get(n) == null) {
				visited.put(n, true);
				System.out.println(n);
				List<Integer> list = adjacencyList.get(n);
				if (list != null) {
					for (Integer tNode : list) {
						queue.add(tNode);
					}

				}
			}
		}

	}

	public static void main(String[] args) {
		DetectCycleInDirectedGraph graph = new DetectCycleInDirectedGraph();
		graph.addEdge(1, 2);
		graph.addEdge(2, 3);
		graph.addEdge(3, 4);
		graph.addEdge(4, 5);
		graph.addEdge(2, 5);
		
		System.out.println("BFS traversal");
		graph.bfs(1, new HashMap<>());

		System.out.println("BFS traversal");
		graph.dfs(1, new HashMap<>());

		Map<Integer, Boolean> visited = new HashMap<>();
		for (Integer node : graph.adjacencyList.keySet()) {
			if(graph.detectCycles(node, visited, new ArrayList<>())){
				System.out.println("Graph has cycles");
				return;
			}
		}
		System.out.println("Graph has no cycles");
	}
}
