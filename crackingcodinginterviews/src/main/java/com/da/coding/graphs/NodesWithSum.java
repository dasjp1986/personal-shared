package com.da.coding.graphs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import sun.security.ssl.HandshakeMessage;

/**
 * 
 * @author dagraw1
 *
 * 
 * 
 *         Given a fully connected graph with n nodes and corresponding values.
 *         One node can interact with other node at a time, to
 *         replace/ignore/add its value to other node’s value. Assuming this
 *         operation takes 1 unit of time, how much time would it take for all
 *         the nodes to have value equal to sum of all the nodes.
 * 
 *         Examples : Given a graph with values {1,2,3,4}, find total time it
 *         takes, such that all nodes have value as 10.
 */
public class NodesWithSum {
	public void updateSums(GNode<Integer> root) {
		Map<GNode<Integer>, Boolean> visited = new HashMap<>();
		Integer sum = calculate(root, 0, visited, false);
		visited.clear();
		calculate(root, sum, visited, true);

	}

	public static void main(String[] args) {
		GNode<Integer> node1 = new GNode<Integer>(1);
		GNode<Integer> node2 = new GNode<Integer>(2);
		GNode<Integer> node3 = new GNode<Integer>(3);
		GNode<Integer> node4 = new GNode<Integer>(4);

		Set<GNode<Integer>> nodes1 = new HashSet<>();
		nodes1.add(node2);
		nodes1.add(node3);
		nodes1.add(node4);

		Set<GNode<Integer>> nodes2 = new HashSet<>();
		nodes2.add(node1);
		nodes2.add(node3);
		nodes2.add(node4);

		Set<GNode<Integer>> nodes3 = new HashSet<>();
		nodes3.add(node2);
		nodes3.add(node1);
		nodes3.add(node4);

		Set<GNode<Integer>> nodes4 = new HashSet<>();
		nodes4.add(node2);
		nodes4.add(node3);
		nodes4.add(node1);

		node1.setNodes(nodes1);
		node2.setNodes(nodes2);
		node3.setNodes(nodes3);
		node4.setNodes(nodes4);

		NodesWithSum problem = new NodesWithSum();
		problem.updateSums(node1);

		//problem.dfs(node1,new HashMap<GNode<Integer>,Boolean>());
	}

	private void dfs(GNode<Integer> node, HashMap<GNode<Integer>, Boolean> visited) {
		visited.put(node, true);
		System.out.println(node.getValue());
		Set<GNode<Integer>> nodes = node.getNodes();
		for(GNode<Integer> n: nodes) {
			if(visited.get(n)==null) {
				dfs(n, visited);
			}
		}
		
	}

	private Integer calculate(GNode<Integer> node, Integer sum, Map<GNode<Integer>, Boolean> visited,
			boolean isUpdate) {

		visited.put(node, true);
		if (isUpdate)
			node.setValue(sum);
		else
			sum = sum + node.getValue();

		Set<GNode<Integer>> nodes = node.getNodes();
		for (GNode<Integer> n : nodes) {
			if (visited.get(n) == null) {
				sum = calculate(n, sum, visited, isUpdate);
			}
		}

		return sum;
	}

}
