package com.da.coding.graphs;

import java.util.Set;
import java.util.TreeSet;

public class GNode<T> implements Comparable<GNode<T>> {
	private T value;
	private Set<GNode<T>> nodes;

	public GNode(T value) {
		this.value = value;
		this.nodes = new TreeSet<>();
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public Set<GNode<T>> getNodes() {
		return nodes;
	}

	public void setNodes(Set<GNode<T>> nodes) {
		this.nodes = nodes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GNode<T> other = (GNode<T>) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public int compareTo(GNode<T> obj) {
		int result = 0;
		if (obj.getValue() instanceof Integer) {
			Integer one = (Integer) this.getValue();
			Integer two = (Integer) obj.getValue();
			result = two.compareTo(one);
		}

		return result;
	}

	@Override
	public String toString() {
		return "GNode [value=" + value + "]";
	}

	
}
