package com.da.coding.graphs;

import java.util.HashMap;
import java.util.Map;

import com.da.coding.commons.Constants.GRAPH_TYPE;
import com.da.coding.queues.QueueUsingSingleLinkedList;

/**
 * 
 * @author dagraw1 Find out if there is any route between two given nodes
 */
public class RoutesBtwNodesDirectedGraph<T> {
	private GraphUsingAdjacencyList<T> directedGraph;

	public GraphUsingAdjacencyList<T> getDirectedGraph() {
		return directedGraph;
	}

	public RoutesBtwNodesDirectedGraph() {
		this.directedGraph = new GraphUsingAdjacencyList<T>(GRAPH_TYPE.DIRECTED);
	}

	// Time Complexity- O(k^d), k- size of list of nodes at each node, d- levels
	// where source and destination are apart
	// Space Complexity- O(N)
	public boolean isRouteBFS(T src, T dest) {
		QueueUsingSingleLinkedList<GNode<T>> queue = new QueueUsingSingleLinkedList<>();
		Map<T, Boolean> visited = new HashMap<>();

		queue.add(directedGraph.getNode(src));
		visited.put(src, true);

		GNode<T> node = null;
		while (!queue.isEmpty()) {
			node = queue.remove();

			if (node.getValue().equals(dest)) {
				return true;
			}

			if (node.getNodes() != null) {
				for (GNode<T> tNode : node.getNodes()) {
					if (tNode.getValue().equals(dest)) {
						return true;
					} else if (visited.get(tNode.getValue()) == null) {
						queue.add(tNode);
						visited.put(tNode.getValue(), true);
					}
				}
			}
		}

		return false;

	}

	public boolean isRouteDFS(T src, T dest) {
		Map<T, Boolean> visited = new HashMap<>();
		visited.put(src, true);
		return dfsRecursively(getDirectedGraph().getNode(src), dest, visited);
	}

	private boolean dfsRecursively(GNode<T> node, T dest, Map<T, Boolean> visited) {
		if (node.getValue().equals(dest)) {
			return true;
		}
		if (node.getNodes() != null) {
			for (GNode<T> tNode : node.getNodes()) {
				if (tNode.getValue().equals(dest)) {
					return true;
				} else if (visited.get(tNode.getValue()) == null) {
					visited.put(tNode.getValue(), true);
					if (dfsRecursively(tNode, dest, visited))
						return true;
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		RoutesBtwNodesDirectedGraph<Integer> routeFinder = new RoutesBtwNodesDirectedGraph<>();
		routeFinder.getDirectedGraph().add(1, 2);
		routeFinder.getDirectedGraph().add(1, 3);
		routeFinder.getDirectedGraph().add(1, 5);
		routeFinder.getDirectedGraph().add(2, 4);
		routeFinder.getDirectedGraph().add(3, 7);
		routeFinder.getDirectedGraph().add(6, 4);
		routeFinder.getDirectedGraph().add(7, 6);
		routeFinder.getDirectedGraph().add(5, 3);

		routeFinder.getDirectedGraph().print();
		System.out.println("BFS finding route between 1->6 " + routeFinder.isRouteBFS(1, 6));
		System.out.println("DFS finding route between 1->6 " + routeFinder.isRouteDFS(1, 6));
		System.out.println("BFS finding route between 4->5 " + routeFinder.isRouteBFS(4, 5));
		System.out.println("DFS finding route between 4->5 " + routeFinder.isRouteDFS(4, 5));
		System.out.println("BFS finding route between 5->4 " + routeFinder.isRouteBFS(5, 4));
		System.out.println("DFS finding route between 5->4 " + routeFinder.isRouteDFS(5, 4));
	}
}
