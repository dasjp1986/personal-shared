package com.da.coding.graphs;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 * 
 *         Bellman ford algorithm is used to find shortest path to all vertices
 *         from given single source
 * 
 *         Dynamic Programming Approach
 * 
 *         Works well with negative edge weight too unlike dijkstra
 * 
 *         Fails for negative weight cycle in the graph as it would continue to
 *         relax vertices even after |V|-1 times relaxations.
 */
public class BellmanFordAlgo {
	private static final Integer MAX = 1000;

	public Map<Integer, Integer> shortestPaths(int[][] weightMatrix, int source) {

		Map<Integer, Integer> paths = new HashMap<>();

		for (int i = 0; i < weightMatrix.length; i++) {
			if (i == source) {
				paths.put(i, 0);
			} else {
				paths.put(i, MAX);
			}
		}

		for (int k = 0; k < weightMatrix.length - 1; k++) {
			for (int i = 0; i < weightMatrix.length; i++) {
				for (int j = 0; j < weightMatrix.length; j++) {
					if (weightMatrix[i][j] != 0) {
						int min = paths.get(j);
						min = Math.min(min, paths.get(i) + weightMatrix[i][j]);
						paths.put(j, min);
					}
				}
			}
		}
		return paths;
	}

	public static void main(String[] args) {

		int[][] weightMatrix = { { 0, 4, 0, 5 }, { 0, 0, 0, 0 }, { 0, -10, 0, 0 }, { 0, 0, 3, 0 } };
		
//		int[][] weightMatrix = { { 0, 28, 0, 0, 0, 10, 0 }, { 28, 0, 16, 0, 0, 0, 14 }, { 0, 16, 0, 12, 0, 0, 0 },
//				{ 0, 0, 12, 0, 22, 0, 18 }, { 0, 0, 0, 22, 0, 25, 24 }, { 10, 0, 0, 0, 25, 0, 0 },
//				{ 0, 14, 0, 18, 24, 0, 0 } };
//
		BellmanFordAlgo algo = new BellmanFordAlgo();
		System.out.println(algo.shortestPaths(weightMatrix, 0));
	
	}

}
