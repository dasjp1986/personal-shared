package com.da.coding.graphs;

import java.util.HashMap;
import java.util.Map;

import com.da.coding.commons.Constants.GRAPH_TYPE;
import com.da.coding.queues.QueueUsingSingleLinkedList;
import com.da.coding.stack.StackUsingLinkedList;

public class GraphUsingAdjacencyMatrix<T> {
	private boolean[][] matrix;
	private Map<T, Integer> posMap;
	private T[] lookUp;
	private GRAPH_TYPE type;

	public GraphUsingAdjacencyMatrix(int vertices, GRAPH_TYPE type) {
		this.matrix = new boolean[vertices][vertices];
		this.posMap = new HashMap<>();
		this.lookUp = (T[]) new Object[vertices];
		this.type = type;
	}

	public void add(T src, T dest) {
		int srcPos = getMatrixPos(src);
		int destPos = getMatrixPos(dest);

		matrix[srcPos][destPos] = true;
		lookUp[srcPos] = src;
		if (type == GRAPH_TYPE.UNDIRECTED) {
			matrix[destPos][srcPos] = true;
			lookUp[destPos] = dest;
		}

	}

	public void bfs() {
		QueueUsingSingleLinkedList<Integer> queue = new QueueUsingSingleLinkedList<>();
		boolean[] visited = new boolean[matrix.length];

		queue.add(0);
		visited[0] = true;

		System.out.println("Printing BFS traversal...");

		while (!queue.isEmpty()) {
			int pos = queue.remove();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[pos][j] && !visited[j]) {
					queue.add(j);
					visited[j] = true;
				}
			}

			System.out.print(lookUp[pos] + " ");
		}
		System.out.println();

	}

	public void dfs() {
		StackUsingLinkedList<Integer> stack = new StackUsingLinkedList<>();
		boolean[] visited = new boolean[matrix.length];

		stack.push(0);
		visited[0] = true;
		int pos = 0;

		System.out.println("Printing graph in DFS...");
		while (!stack.isEmpty()) {
			pos = stack.pop();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[pos][j] && !visited[j]) {
					stack.push(j);
					visited[j] = true;
				}
			}
			System.out.print(lookUp[pos] + " ");
		}
		System.out.println();

	}

	private int getMatrixPos(T value) {
		Integer pos = posMap.get(value);
		if (pos == null) {
			pos = posMap.size();
			posMap.put(value, pos);
		}
		return pos;
	}

	public static void main(String[] args) {
		GraphUsingAdjacencyMatrix<Integer> graph = new GraphUsingAdjacencyMatrix<>(7, GRAPH_TYPE.UNDIRECTED);
		graph.add(1, 2);
		graph.add(1, 3);
		graph.add(2, 4);
		graph.add(4, 3);
		graph.add(4, 6);
		graph.add(3, 5);
		graph.add(5, 6);
		graph.add(5, 7);

		graph.bfs();
		graph.dfs();
	}
}
