package com.da.coding.graphs;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Generate all pairs shortest path in given graph
 *         
 *         Dynamic Programming Approach
 *          
 *         Time Complexity- O(N^3)
 */
public class FlodWarshallAlgo {

	public int[][] shortesPaths(int[][] weightMatrix) {
		int[][] paths = new int[weightMatrix.length][weightMatrix.length];

		for (int i = 0; i < paths.length; i++) {
			for (int j = 0; j < paths.length; j++) {
				paths[i][j] = weightMatrix[i][j];
			}
		}

		for (int k = 0; k < paths.length; k++) {
			for (int i = 0; i < paths.length; i++) {
				if (k == i) {
					continue;
				}
				for (int j = 0; j < paths.length; j++) {
					if (k == j) {
						continue;
					}
					paths[i][j] = Math.min(paths[i][j], paths[i][k] + paths[k][j]);
				}
			}
		}

		return paths;
	}

	public static void main(String[] args) {
		int max = 100;
		int[][] weightMatrix = { { 0, 3, max, 7 }, { 8, 0, 2, max }, { 5, max, 0, 1 }, { 2, max, max, 0 } };
		FlodWarshallAlgo algo= new FlodWarshallAlgo();
		int[][] paths = algo.shortesPaths(weightMatrix);
		
		for (int i = 0; i < paths.length; i++) {
			System.out.println(Arrays.toString(paths[i]));
		}
	}

}
