package com.da.coding.stack;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 * 
 *         Given an array, print the Next Greater Element (NGE) for every
 *         element. The Next greater Element for an element x is the first
 *         greater element on the right side of x in array. Elements for which
 *         no greater element exist, consider next greater element as -1.
 * 
 *         Examples: a) For any array, rightmost element always has next greater
 *         element as -1. b) For an array which is sorted in decreasing order,
 *         all elements have next greater element as -1. c) For the input array
 *         [4, 5, 2, 25}, the next greater elements for each element are as
 *         follows.
 * 
 *         Element NGE 4 --> 5 5 --> 25 2 --> 25 25 --> -1 d) For the input
 *         array [13, 7, 6, 12}, the next greater elements for each element are
 *         as follows.
 * 
 *         Element NGE 13 --> -1 7 --> 12 6 --> 12 12 --> -1
 */
public class PrintNextGreaterElement {
	public void printNGE(int[] a) {
		Stack<Integer> stack = new Stack<>();
		stack.push(a[0]);

		for (int i = 1; i < a.length; i++) {
			int larger = stack.pop();
			int j = i;
			while (j < a.length) {
				if (a[j] >= larger) {
					larger = a[j];
					break;
				}
				j++;
			}
			if (j == a.length) {
				larger = -1;
			}
			stack.push(larger);
			System.out.println(larger);
		}
		System.out.println(-1);

	}

	public static void main(String[] args) {
		PrintNextGreaterElement printNextGreaterElement = new PrintNextGreaterElement();
		//int[] a = { 13, 7, 6, 12 };
		//int[] a = { 4, 5, 2, 25 };
		int[] a = { 1, 2, 3, 4 };
		printNextGreaterElement.printNGE(a);
	}
}
