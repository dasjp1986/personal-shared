package com.da.coding.stack;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *         Given an absolute path for a file (Unix-style), simplify it.
 * 
 *         Examples:
 * 
 *         path = "/home/", => "/home" path = "/a/./b/../../c/", => "/c" Note
 *         that absolute path always begin with ‘/’ ( root directory ) Path will
 *         not have whitespace characters.
 * 
 *         Note: . means current directory and .. means parent directory
 */
public class SimplyfyDirectoryPath {

	public static String simplifyPath(String A) {
		String[] tokens = A.split("/");
		Stack<String> stack = new Stack<>();
		for (String token : tokens) {
			switch (token) {
			case ".":
			case "":
				break;
			case "..":
				if (!stack.isEmpty())
					stack.pop();
				break;
			default:
				stack.push(token);
				break;
			}
		}

		StringBuilder builder = new StringBuilder();

		for (String str : stack) {
			builder.append('/');
			builder.append(str);
		}
		return builder.toString().isEmpty() ? "/" : builder.toString();
	}

	public static void main(String[] args) {
		System.out.println(simplifyPath("/../"));
		System.out.println(simplifyPath("/a//b/c"));
		
		List<String> a= Arrays.asList("a");
	
	}

}
