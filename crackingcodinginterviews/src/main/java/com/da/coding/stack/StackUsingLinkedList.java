package com.da.coding.stack;

public class StackUsingLinkedList<T> {
	private int count;
	StackNode<T> top;

	// Time Complexity- O(1)
	public void push(T value) {
		StackNode<T> node = new StackNode<>(value);
		if (top == null) {
			top = node;
		} else {
			node.setPrev(top);
			top = node;
		}
		count++;
	}

	// Time Complexity- O(1)
	public T pop() {
		if (top == null) {
			throw new RuntimeException("Stack is empty...");
		}
		StackNode<T> node = top;
		top = top.getPrev();
		node.setPrev(null);
		count--;
		return node.getValue();
	}

	public T peek() {
		if (top == null) {
			throw new RuntimeException("Stack is empty...");
		}

		return top.getValue();
	}

	public boolean isEmpty() {
		return top == null;
	}

	public int getSize() {
		return count;
	}

	public void print() {
		System.out.println("\nprinting stack");
		StackNode<T> node = top;
		while (node != null) {
			System.out.print("->" + node.getValue());
			node = node.getPrev();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		StackUsingLinkedList<Integer> stack = new StackUsingLinkedList<>();
		for (int i = 1; i <= 5; i++) {
			stack.push(i);
			if (i % 2 == 0)
				stack.pop();
		}
		stack.print();
		System.out.println(stack.peek());
		System.out.println(stack.peek());
	}
}
