package com.da.coding.stack;

import java.util.ArrayList;
import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *         Given an array, find the nearest smaller element G[i] for every
 *         element A[i] in the array such that the element has an index smaller
 *         than i.
 * 
 *         More formally,
 * 
 *         G[i] for an element A[i] = an element A[j] such that j is maximum
 *         possible AND j < i AND A[j] < A[i] Elements for which no smaller
 *         element exist, consider next smaller element as -1.
 * 
 *         Example:
 * 
 *         Input : A : [4, 5, 2, 10, 8] Return : [-1, 4, -1, 2, 2]
 * 
 *         Example 2:
 * 
 *         Input : A : [3, 2, 1] Return : [-1, -1, -1]
 */
public class NearestSmallerInt {

	public ArrayList<Integer> prevSmaller(ArrayList<Integer> A) {
		Stack<Integer> stack = new Stack<>();
		ArrayList<Integer> res = new ArrayList<Integer>();
		for (Integer n : A) {
			if (stack.isEmpty()) {
				res.add(-1);
				stack.push(n);
			} else {
				while (!stack.isEmpty() && stack.peek() >= n) {
					stack.pop();
				}
				if (stack.isEmpty()) {
					res.add(-1);

				} else {
					res.add(stack.peek());

				}
				stack.push(n);
			}
		}

		return res;
	}

	public static void main(String[] args) {
		Integer[] array = { 34, 35, 27, 42, 5, 28, 39, 20, 28 };
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int a : array) {
			list.add(a);
		}
		
		NearestSmallerInt nearestSmallerInt= new NearestSmallerInt();
		System.out.println(nearestSmallerInt.prevSmaller(list));

	}

}
