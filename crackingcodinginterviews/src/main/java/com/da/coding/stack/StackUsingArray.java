package com.da.coding.stack;

public class StackUsingArray<T> {
	private Object[] stack;
	private int top;
	private int currentCapacity;
	private int initialCapacity;
	private static final double ARRAY_GROWTH_FACTOR = 2.0;
	private static final double ARRAY_SHRINK_FACTOR = 0.75;

	public StackUsingArray(int initialCapacity) {
		this.initialCapacity = initialCapacity;
		this.currentCapacity = initialCapacity;
		this.stack = new Object[initialCapacity];
		top = -1;
	}

	// Time Complexity- O(1)
	public void push(T value) {
		++top;
		if (top >= currentCapacity) {
			System.out.println("increasing the array by growth factor, current : " + currentCapacity
					+ " new capacity : " + currentCapacity * ARRAY_GROWTH_FACTOR);
			resizeArray(currentCapacity, ARRAY_GROWTH_FACTOR);
		}

		stack[top] = value;
	}

	// Time Complexity- O(1)
	public T pop() {
		if (isEmpty())
			throw new RuntimeException("Stack is empty");
		else {

			Object value = stack[top];
			if (top <= (currentCapacity / ARRAY_GROWTH_FACTOR) && top >= initialCapacity) {
				System.out.println("shrinking the array by shrink factor");
				resizeArray(currentCapacity, ARRAY_SHRINK_FACTOR);
			}

			top--;
			return (T) value;
		}

	}

	private void resizeArray(int capacity, double factor) {
		int newCapacity = (int) (capacity * factor);
		Object[] newStack = new Object[newCapacity];
		for (int i = 0; i < top; i++) {
			newStack[i] = stack[i];
		}
		this.currentCapacity = newCapacity;
		this.stack = newStack;
	}

	public T peek() {
		if (isEmpty())
			throw new RuntimeException("Stack is empty");
		else {
			return (T) stack[top];
		}
	}

	public boolean isEmpty() {
		return getSize() == 0;
	}

	public int getSize() {
		return top + 1;
	}

	public void print() {
		System.out.println("\nprinting stack");
		for (int i = top; i >= 0; i--) {
			System.out.println("->" + stack[i]);
		}
		System.out.println();
	}

	public static void main(String[] args) {
		StackUsingArray<Integer> stack = new StackUsingArray<>(10);
		for (int i = 1; i <= 11; i++) {
			stack.push(i);
		}
		stack.print();
		// System.out.println(stack.peek());
		for (int i = 1; i <= 11; i++) {
			System.out.println(stack.pop());
		}
		stack.print();
	}
}
