package com.da.coding.stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *         Evaluate the value of an arithmetic expression in Reverse Polish
 *         Notation.
 * 
 *         Valid operators are +, -, *, /. Each operand may be an integer or
 *         another expression.
 * 
 *         Examples:
 * 
 *         ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9 ["4", "13", "5", "/",
 *         "+"] -> (4 + (13 / 5)) -> 6
 */
public class EvaluateExpression {

	public int evalRPN(ArrayList<String> A) {
		int res = 0;
		List<String> operators = Arrays.asList("+", "-", "*", "/");
		Stack<Integer> stack = new Stack<>();
		try {
			for (String op : A) {
				if (operators.contains(op)) {
					int opr1 = stack.pop();
					int opr2 = stack.pop();
					stack.push(calculate(opr1, opr2, op));
				} else {
					stack.push(Integer.parseInt(op));
				}
			}
			res = stack.pop();
		} catch (Exception ex) {
			res = -1;
		}
		return res;
	}

	private int calculate(int opr1, int opr2, String op) {
		int res = 0;

		switch (op) {
		case "+":
			res = opr2 + opr1;
			break;

		case "-":
			res = opr2 - opr1;
			break;

		case "/":
			res = opr2 / opr1;
			break;

		case "*":
			res = opr2 * opr1;
			break;
		default:
			break;
		}

		return res;
	}

	public static void main(String[] args) {
		EvaluateExpression evaluateExpression= new EvaluateExpression();
		String[] array= {"2", "1", "+", "3", "*"};
		ArrayList<String> A= new ArrayList<>();
		for (String a : array) {
			A.add(a);
		}
		System.out.println(evaluateExpression.evalRPN(A));
	}

}
