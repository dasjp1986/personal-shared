package com.da.coding.stack;

/**
 * 
 * @author dagraw1 
 * Implement min function in addition to push and pop in stack
 * 
 * All the operation should have O(1) Time Complexity
 *
 */
// Space Complexity- O(N)
public class StackMinFinder {
	private StackUsingLinkedList<Integer> stack;
	private StackUsingLinkedList<Integer> minStack;

	public StackMinFinder() {
		stack = new StackUsingLinkedList<>();
		minStack = new StackUsingLinkedList<>();
	}

	// Time Complexity- O(1),
	public void push(Integer value) {
		if (stack.isEmpty()) {
			minStack.push(value);
		} else {
			if (value.intValue() < minStack.peek().intValue()) {
				minStack.push(value);
			}
		}

		stack.push(value);

	}

	// Time Complexity- O(1)
	public Integer pop() {
		Integer value = stack.pop();

		if (value.intValue() == minStack.peek().intValue()) {
			minStack.pop();
		}

		return value;
	}

	// Time Complexity- O(1)
	public Integer min() {
		return minStack.peek();
	}

	public static void main(String[] args) {
		StackMinFinder finder = new StackMinFinder();
		finder.push(10);
		finder.push(15);
		finder.push(5);
		finder.push(4);
		finder.push(7);
		finder.push(19);
		System.out.println(finder.min());
		finder.pop();
		finder.pop();
		finder.pop();
		System.out.println(finder.min());
	}
}
