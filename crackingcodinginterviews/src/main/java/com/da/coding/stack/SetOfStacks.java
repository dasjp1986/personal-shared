package com.da.coding.stack;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 * Given max height limit for the stack, post to which stack might get toppled off.
 * Similar to stack of plates in a restaurants with a max height limit.
 * 
 * Once a stack reaches to its maximum limit, create another stack and add it to the set of stacks.
 * 
 * Use should have an experience of pushing and popping from a single stack instead of sets of it.
 * 
 * Follow up question to pop a number from one of the stack identified by stack index
 */

//Space Complexity= O(N)
public class SetOfStacks<T> {

	private List<StackUsingLinkedList<T>> stacks;
	private int heightLimit;
	private int stackIndex;

	public SetOfStacks(int heightLimit) {
		this.stacks = new ArrayList<>();
		this.heightLimit = heightLimit;
		this.stackIndex = -1;
	}

	public int getStackIndex() {
		return stackIndex;
	}

	public void setStackIndex(int stackIndex) {
		this.stackIndex = stackIndex;
	}

	// Time Complexity- O(1)
	public void push(T value) {
		if (stackIndex == -1) {
			this.stacks.add(new StackUsingLinkedList<T>());
			stackIndex++;
		}

		StackUsingLinkedList<T> stack = stacks.get(stackIndex);
		if (stack.getSize() >= heightLimit) {
			stack = new StackUsingLinkedList<>();
			stacks.add(stack);
			stackIndex++;
		}
		stack.push(value);
	}

	// Time Complexity- O(1)
	public T pop() {
		if (stackIndex == -1) {
			throw new RuntimeException("Stack is empty");
		}

		StackUsingLinkedList<T> stack = stacks.get(stackIndex);
		T value = stack.pop();

		if (stack.isEmpty()) {
			stacks.remove(stackIndex);
			stackIndex--;
		}

		return value;
	}

	// Time Complexity- O(1)
	public T pop(int index) {
		if (stackIndex == -1 || index >= stacks.size()) {
			throw new RuntimeException("Stack is empty or invalid index being searched");
		}

		StackUsingLinkedList<T> stack = stacks.get(index);
		T value = stack.pop();

		if (stack.isEmpty()) {
			stacks.remove(stackIndex);
			stackIndex--;
		}

		return value;
	}

	public static void main(String[] args) {
		SetOfStacks<Integer> stacks = new SetOfStacks<>(3);
		for (int i = 1; i <= 10; i++) {
			stacks.push(i);
		}

		for (int i = 1; i <= 4; i++) {
			System.out.println("Stack no. : " + stacks.getStackIndex() + " popped : " + stacks.pop());
		}

		System.out.println("Stack no. : " + 1 + " popped : " + stacks.pop(1));
		System.out.println("Stack no. : " + 1 + " popped : " + stacks.pop(1));
		System.out.println("Stack no. : " + 1 + " popped : " + stacks.pop(1));
		System.out.println("Stack no. : " + 0 + " popped : " + stacks.pop(0));

	}
}
