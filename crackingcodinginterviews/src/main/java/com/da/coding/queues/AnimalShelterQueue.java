package com.da.coding.queues;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author dagraw1
 *
 *         An animal shelter strictly works on First in First Out basis It can
 *         hold both CATs and DOGs and adopter can only adopt the oldest
 *         animal(joined first) Adopter may wish to specify if they want DOG or
 *         CAT but can not ask for any specific animal
 */

// Space Complexity- O(2)
public class AnimalShelterQueue {
	private Map<ANIMAL, QueueUsingSingleLinkedList<AnimalNode>> queueMap;
	private int size;

	public AnimalShelterQueue() {
		queueMap = new HashMap<>();
		for (ANIMAL a : ANIMAL.values()) {
			queueMap.put(a, new QueueUsingSingleLinkedList<AnimalNode>());
		}
	}

	// Time Complexity- O(1)
	public void add(ANIMAL type, AnimalNode value) {
		if (type == null) {
			throw new RuntimeException("Please sepcify correct animal type");
		}
		queueMap.get(type).add(value);
		size++;
	}

	// Time Complexity- O(type_of_animals)~O(1)
	public AnimalNode remove(ANIMAL type) {
		if (size <= 0) {
			throw new RuntimeException("Queue is empty");
		}

		AnimalNode value = null;
		if (type == null) {

			long ts = Long.MAX_VALUE;
			ANIMAL lowest= null;
			for (Entry<ANIMAL, QueueUsingSingleLinkedList<AnimalNode>> entry : queueMap.entrySet()) {
				QueueUsingSingleLinkedList<AnimalNode> queue = entry.getValue();
				if (!queue.isEmpty() && queue.poll().getTimeStamp() < ts) {
					ts = queue.poll().getTimeStamp();
					lowest= entry.getKey();
				}
			}
			value= queueMap.get(lowest).remove();

		} else {
			value = queueMap.get(type).remove();
		}
		size--;
		return value;
	}

	public static void main(String[] args) {
		AnimalShelterQueue queue = new AnimalShelterQueue();
		queue.add(ANIMAL.DOG, new AnimalNode("DOG1"));
		queue.add(ANIMAL.CAT, new AnimalNode("CAT1"));
		queue.add(ANIMAL.DEER, new AnimalNode("DEER1"));
		queue.add(ANIMAL.DOG, new AnimalNode("DOG2"));
		queue.add(ANIMAL.CAT, new AnimalNode("CAT2"));
		queue.add(ANIMAL.DOG, new AnimalNode("DOG3"));
		queue.add(ANIMAL.CAT, new AnimalNode("CAT3"));

		System.out.println(queue.remove(ANIMAL.CAT));
		System.out.println(queue.remove(ANIMAL.DOG));
		System.out.println(queue.remove(ANIMAL.DOG));
		System.out.println(queue.remove(null));
		System.out.println(queue.remove(null));
		System.out.println(queue.remove(null));

	}
}

enum ANIMAL {
	DOG, CAT, DEER
}

class AnimalNode {
	private String name;
	private long timeStamp;

	public AnimalNode(String name) {
		this.name = name;
		timeStamp = System.nanoTime();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public String toString() {
		return "AnimalNode [name=" + name + ", timeStamp=" + timeStamp + "]";
	}

}
