package com.da.coding.queues;

import com.da.coding.stack.StackUsingLinkedList;

//Space Complexity- O(N)
public class QueueUsingStacksPushOptimized<T> {
	private StackUsingLinkedList<T> stack;

	public QueueUsingStacksPushOptimized() {
		stack = new StackUsingLinkedList<>();

	}

	// Time Complexity- O(1)
	public void add(T value) {
		stack.push(value);
	}

	// Time Complexity- O(N)
	public T remove() {
		T value= null;
		StackUsingLinkedList<T> tempStack = new StackUsingLinkedList<>();
		if (stack.isEmpty()) {
			throw new RuntimeException("Queue is empty...");
		} else {
			while (!stack.isEmpty()) {
				tempStack.push(stack.pop());
			}
			value= tempStack.pop();
			while(!tempStack.isEmpty()){
				stack.push(tempStack.pop());
			}
		}
		return value;
	}

	public static void main(String[] args) {
		QueueUsingStacksPushOptimized<Integer> queue = new QueueUsingStacksPushOptimized<>();
		for (int i = 1; i <= 5; i++) {
			queue.add(i);
		}

		for (int i = 1; i <= 10; i++) {
			System.out.println(queue.remove());
			queue.add(i+5);
		}
	}
}
