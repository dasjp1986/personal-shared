package com.da.coding.queues;

public class QueueUsingSingleLinkedList<T> {
	private QNode<T> front;
	private QNode<T> rear;

	public void add(T value) {
		QNode<T> node = new QNode<>(value);
		if (rear == null) {
			rear = node;
			front = node;
		} else {
			rear.setNext(node);
			rear = rear.getNext();
		}

	}

	public T remove() {
		if (front == null)
			throw new RuntimeException("Queue is empty");

		QNode<T> node = front;
		front = front.getNext();
		if (front == null) {
			rear = null;
		}
		node.setNext(null);
		return node.getValue();

	}

	public T poll() {
		if (front == null)
			throw new RuntimeException("Queue is empty");
		return front.getValue();
	}

	public boolean isEmpty() {
		return front == null;
	}

	public void print() {
		QNode<T> node = front;
		System.out.println("printing queue...");
		while (node != null) {
			System.out.println("->" + node.getValue());
			node = node.getNext();
		}
	}

	public static void main(String[] args) {
		QueueUsingSingleLinkedList<Integer> queue = new QueueUsingSingleLinkedList<>();
		for (int i = 1; i <= 10; i++) {
			queue.add(i);
		}

		queue.print();
		System.out.println("poll "+queue.poll());
		System.out.println("removed "+queue.remove());
		System.out.println("poll "+queue.poll());
	}

}
