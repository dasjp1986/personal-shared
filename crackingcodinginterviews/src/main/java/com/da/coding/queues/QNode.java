package com.da.coding.queues;

public class QNode<T> {
	private T value;
	private QNode<T> next;

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public QNode<T> getNext() {
		return next;
	}

	public void setNext(QNode<T> next) {
		this.next = next;
	}

	public QNode(T value) {
		super();
		this.value = value;
	}

	@Override
	public String toString() {
		return "QNode [value=" + value + "]";
	}

}
