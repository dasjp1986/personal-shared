package com.da.coding.queues;

public class QueueUsingArray {
	private int[] array;
	private int size;
	private int currentCapacity;
	private int initialCapacity;
	private int rear = -1;
	private int front = -1;
	private static final double GROWTH_FACTOR = 2.0;
	private static final double SHRINK_FACTOR = 0.75;

	public QueueUsingArray(int initialCapacity) {
		this.initialCapacity = initialCapacity;
		this.currentCapacity = initialCapacity;
		this.array = new int[initialCapacity];
		this.rear = -1;
		this.front = -1;
	}

	public void add(int value) {
		if (size == currentCapacity) {
			System.out.println("increasing array size from : " + currentCapacity + " to : "
					+ (int) (currentCapacity * GROWTH_FACTOR));
			resize((int) (currentCapacity * GROWTH_FACTOR));
		} else if (rear + 1 >= currentCapacity) {
			rear = -1;
		}
		rear++;
		size++;
		this.array[rear] = value;

		if (front == -1) {
			front = rear;
		}
		System.out.println("Added : " + value + " size : " + size);
	}

	public int remove() {
		if (isEmpty()) {
			throw new RuntimeException("Queue is empty...");
		}

		if (size < currentCapacity / GROWTH_FACTOR && size > initialCapacity) {
			System.out.println("decreasing array size from : " + currentCapacity + " to : "
					+ (int) (currentCapacity * SHRINK_FACTOR));
			resize((int) (currentCapacity * SHRINK_FACTOR));
		}

		int value = array[front];
		size--;
		front++;

		if (size == 0) {
			front = -1;
			rear = -1;
		} else if (front > currentCapacity) {
			front = 0;
		}

		System.out.println("Removed : " + value + " size : " + size);
		return value;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	private void resize(int newCapacity) {
		int[] newArray = new int[newCapacity];
		int k = front;
		for (int i = 0; i < size; i++) {
			if (k >= currentCapacity) {
				k = 0;
			}

			newArray[i] = array[k];
			k++;

			this.rear = i;
		}

		this.array = newArray;
		this.currentCapacity = newCapacity;
		this.front = 0;
	}

	public void print() {
		int k = front;
		System.out.println("Printing queue...");
		for (int i = 0; i < size; i++) {
			System.out.print("->" + array[k]);
			k++;

			if (k >= currentCapacity) {
				k = 0;
			}
		}
		System.out.println();
	}

	public static void main(String[] args) {
		QueueUsingArray queue = new QueueUsingArray(3);

		queue.add(1);
		queue.add(2);
		queue.add(3);
		queue.print();

		queue.remove();
		queue.print();

		queue.remove();
		queue.print();

		queue.add(4);
		queue.print();

		queue.add(5);
		queue.print();

		queue.add(6);
		queue.print();

		for (int i = 0; i < 10; i++) {
			queue.add(i+7);
		}
		queue.print();
		
		for (int i = 0; i < 14; i++) {
			queue.remove();
		}
		queue.print();
	}

}
