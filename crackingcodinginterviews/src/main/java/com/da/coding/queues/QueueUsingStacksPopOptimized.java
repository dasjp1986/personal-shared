package com.da.coding.queues;

import com.da.coding.stack.StackUsingLinkedList;

public class QueueUsingStacksPopOptimized<T> {
	private StackUsingLinkedList<T> stack;
	private StackUsingLinkedList<T> tempStack;
	
	public QueueUsingStacksPopOptimized() {
		stack= new StackUsingLinkedList<>();
		tempStack= new StackUsingLinkedList<>();
	}
	
	//Time Complexity- O(2N)->O(N)
	//Space Complexity- O(2N)->O(N)
	public void add(T value){
		if(stack.isEmpty()){
			stack.push(value);
		}else{
			while(!stack.isEmpty()){
				tempStack.push(stack.pop());
			}
			stack.push(value);
			while(!tempStack.isEmpty()){
				stack.push(tempStack.pop());
			}
		}
	}
	
	//Time Complexity- O(1)
	public T remove(){
		if(stack.isEmpty()){
			throw new RuntimeException("Queue is empty...");
		}
		return stack.pop();
	}
	
	public static void main(String[] args) {
		QueueUsingStacksPopOptimized<Integer> queue= new QueueUsingStacksPopOptimized<>();
		for (int i = 1; i <= 5; i++) {
			queue.add(i);
		}
		
		for (int i = 1; i <= 5; i++) {
			System.out.println(queue.remove());
		}
	}
}	
