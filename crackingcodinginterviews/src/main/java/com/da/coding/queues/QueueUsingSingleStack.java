package com.da.coding.queues;

import java.util.Stack;

public class QueueUsingSingleStack<T> {
	Stack<T> stack = new Stack<>();

	public void add(T val) {
		stack.push(val);
	}

	public T remove() {
		if (stack.isEmpty()) {
			throw new RuntimeException("Invalid operation");
		}

		return removeUtil();
	}

	private T removeUtil() {
		T result = null;
		T temp = stack.pop();

		if (stack.isEmpty()) {
			result = temp;
		} else {
			result = removeUtil();
			stack.push(temp);
		}

		return result;
	}

	public boolean isEmpty() {
		return stack.isEmpty();
	}

	public static void main(String[] args) {
		QueueUsingSingleStack<Integer> queue = new QueueUsingSingleStack<>();
		queue.add(1);
		queue.add(2);
		queue.add(3);
		queue.add(4);
		queue.add(5);
		queue.add(6);

		while (!queue.isEmpty()) {
			System.out.println(queue.remove());
		}

	}
}
