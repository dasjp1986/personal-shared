package com.da.coding.chess;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * 
 * @author dagraw1
 *
 *         Knight movement on a chess board
 * 
 *         Given any source point and destination point on a chess board, we
 *         need to find whether Knight can move to the destination or not.
 * 
 *         Knight's movements on a chess board
 * 
 *         The above figure details the movements for a knight ( 8 possibilities
 *         ). Note that a knight cannot go out of the board.
 * 
 *         If yes, then what would be the minimum number of steps for the knight
 *         to move to the said point. If knight can not move from the source
 *         point to the destination point, then return -1
 * 
 *         Input:
 * 
 *         N, M, x1, y1, x2, y2 where N and M are size of chess board x1, y1
 *         coordinates of source point x2, y2 coordinates of destination point
 *         Output:
 * 
 *         return Minimum moves or -1 Example
 * 
 *         Input : 8 8 1 1 8 8 Output : 6
 */
public class KnightOnChessBoard {

	int[] x = { -2, -2, -1, 1, 2, 2, 1, -1 };
	int[] y = { -1, 1, 2, 2, 1, -1, -2, -2 };
	int n;

	public static void main(String[] args) {
		KnightOnChessBoard board = new KnightOnChessBoard();
		System.out.println(board.knight(4, 2, 1, 1, 2));
	}

	public int knight(int n, int x1, int y1, int x2, int y2) {
		if (x1 == x2 && y1 == y2) {
			return 0;
		}

		Map<Coord, Boolean> visited = new HashMap<>();
		this.n = n;
		Coord coord = new Coord(x1, y1, 0);
		Queue<Coord> q = new LinkedList<>();
		q.add(coord);
		visited.put(coord, true);

		// BFS Traversal is needed as it follows greedy approach, no need to maintain
		// minimum
		while (!q.isEmpty()) {
			coord = q.remove();

			for (int i = 0; i < x.length; i++) {
				int row = coord.x + x[i];
				int col = coord.y + y[i];

				if (row < 1 || row > n || col < 1 || col > n) {
					continue;
				}

				if (row == x2 && col == y2) {
					return coord.cost + 1;
				}

				Coord next = new Coord(row, col, coord.cost + 1);
				if (visited.get(next) != null) {
					continue;
				}
				// make sure you mark visited as soon as added to the queue
				visited.put(next, true);
				q.add(next);
			}
		}

		return -1;
	}

}

class Coord {
	Integer x;
	Integer y;
	int cost;

	public Coord(Integer x, Integer y, int cost) {
		this.x = x;
		this.y = y;
		this.cost = cost;
	}

	public boolean equals(Object obj) {
		Coord cor = (Coord) obj;
		if (this == cor) {
			return true;
		}

		return this.x.equals(cor.x) && this.y.equals(cor.y);
	}

	public int hashCode() {
		return this.x.hashCode() * 2 + this.y.hashCode() * 3;
	}

	@Override
	public String toString() {
		return "Coord [x=" + x + ", y=" + y + ", cost=" + cost + "]";
	}

}
