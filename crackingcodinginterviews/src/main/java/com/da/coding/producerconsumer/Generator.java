package com.da.coding.producerconsumer;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Generator implements Runnable {

	Lock lock;
	Condition notFullCondition;
	Condition fullCondition;
	Queue<Integer> buffer;

	public Generator(Lock lock, Condition notFullCondition, Condition fullCondition, Queue<Integer> buffer) {
		super();
		this.lock = lock;
		this.notFullCondition = notFullCondition;
		this.fullCondition = fullCondition;
		this.buffer = buffer;
	}

	@Override
	public void run() {
		while (true) {
			try {
				lock.lock();
				while (buffer.size() == 10) {
					notFullCondition.await();

				}
				buffer.add(new Random().nextInt(100));
				fullCondition.signalAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
	}

}
