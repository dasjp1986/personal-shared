package com.da.coding.producerconsumer;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Receiver implements Runnable {

	Lock lock;
	Condition notFullCondition;
	Condition fullCondition;
	Queue<Integer> buffer;
	AtomicInteger indexer;

	public Receiver(Lock lock, Condition notFullCondition, Condition fullCondition, Queue<Integer> buffer, AtomicInteger indexer) {
		super();
		this.lock = lock;
		this.notFullCondition = notFullCondition;
		this.fullCondition = fullCondition;
		this.buffer = buffer;
		this.indexer= indexer;
	}

	@Override
	public void run() {
		while (true) {
			try {
				lock.lock();
				while (buffer.size() == 0) {
					System.out.println("waiting buffer to fill");
					fullCondition.await();
				}
				int index= indexer.incrementAndGet();
				System.out.println(buffer.poll());
				System.out.println(Thread.currentThread().getName()+" : index : "+index);
				notFullCondition.signalAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
	}

}
