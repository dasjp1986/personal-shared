package com.da.coding.producerconsumer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GenerateReceiverTester {
	public static void main(String[] args) {
		Lock lock= new ReentrantLock();
		Condition notFullCondition = lock.newCondition();
		Condition fullCondition = lock.newCondition();
		Queue<Integer> buffer= new LinkedList<>();
		AtomicInteger indexer= new AtomicInteger(-1);
		for (int i = 0; i < 10; i++) {
			new Thread(new Receiver(lock, notFullCondition, fullCondition, buffer,indexer)).start();
		}
		for (int i = 0; i < 10; i++) {
			new Thread(new Generator(lock, notFullCondition, fullCondition, buffer)).start();
		}
	}
}
