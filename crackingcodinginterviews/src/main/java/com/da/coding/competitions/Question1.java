package com.da.coding.competitions;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Question1 {

	public static void main(String[] args) {
		// YOUR CODE GOES HERE
		// Please take input and print output to standard input/output (stdin/stdout)
		// DO NOT USE ARGUMENTS FOR INPUTS
		// E.g. 'Scanner' for input & 'System.out' for output

		Scanner scn = new Scanner(System.in);
		int tests = scn.nextInt();
		for (int i = 0; i < tests; i++) {
			int n = scn.nextInt();
			int q = scn.nextInt();
			String str = scn.next();
			for (int j = 0; j < q; j++) {
				int type = scn.nextInt();
				if (type == 1) {
					long counts = find(str, scn.nextInt(), scn.nextInt(), scn.nextInt());
					System.out.println(counts);
				}

				if (type == 2) {
					str = update(str, scn.nextInt(), scn.nextLine());
				}
			}
		}

		scn.close();

	}

	private static long find(String str, int s, int e, int k) {
		System.out.println("substring: "+str.substring(s-1, e));
		long count = 0;
		if (str.length() < e || s < 1) {
			return count;
		}

		Map<Character, Integer> map = new HashMap<Character, Integer>();
		for (int i = s - 1; i < e; i++) {
			map.put((char)(str.charAt(i) - k), 1);
		}

		System.out.println(map);
		for (int i = s - 1; i < e; i++) {
			Integer look = map.get(str.charAt(i));
			if (look != null) {
				System.out.println(str.charAt(i));
				count++;
			}
		}

		long mod = (long) (Math.pow(10, 9) + 7);
		return count % mod;

	}

	private static String update(String str, int i, String replace) {
		i = i - 1;
		if (i < str.length() && i >= 0) {
			char[] array = str.toCharArray();
			char ch= replace.charAt(1);
			array[i] = ch;
			str = new String(array);
		}

		System.out.println("updated : "+str);
		return str;
	}

}
