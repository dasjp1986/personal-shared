package com.da.coding.competitions;

import java.util.Scanner;

public class Question4 {

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int m = scn.nextInt();
		int n = scn.nextInt();
		int[][] matrix = new int[m][n];
		for (int r = 0; r < m; r++) {
			for (int c = 0; c < n; c++) {
				matrix[r][c] = scn.nextInt();
			}
		}
		System.out.println(solve(m, n, matrix));
		scn.close();
	}

	private static int solve(int m, int n, int[][] matrix) {
		int min = 0;
		int max = Math.max(m, n);
		int total = 0;
		for (int r = 0; r < max; r++) {
			int rSum = 0;
			int cSum = 0;
			for (int c = 0; c < max; c++) {
				if (r < m && c < n) {
					rSum += matrix[r][c];
					total += matrix[r][c];
				}

				if (r < n && c < m) {
					cSum += matrix[c][r];
				}
			}

			if (rSum < min) {
				min = rSum;
			}

			if (cSum < min) {
				min = cSum;
			}
		}
		return total - min;
	}

}
