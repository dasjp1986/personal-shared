package com.da.coding.competitions;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         Given a String find count of all the subsequences containing one
 *         vowels and any number of consonants
 *         
 *         Ex- acv
 *         Output: a, ac, av, acv
 */
public class SubsequenceWithConsVowels {

	private static Map<Character, Boolean> vowels;

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		String input = scn.nextLine();
		System.out.println(solve(input));
		scn.close();
	}

	private static int solve(String input) {

		int[][] cache = new int[input.length() + 2][2];
		createMap();
		return counts(input, 0, cache, 0);
	}

	private static int counts(String input, int index, int[][] cache, int isVowel) {

		if (index >= input.length()) {
			return isVowel;
		}

		if (cache[index][isVowel] != 0) {
			return cache[index][isVowel];
		}

		System.out.println(index + "," + isVowel);

		int exclude = 0;
		int include = 0;
		if (vowels.get(input.charAt(index)) != null) {
			if (isVowel == 0) {
				include = counts(input, index + 1, cache, 1);
			}
		} else {
			include = counts(input, index + 1, cache, isVowel);
		}
		exclude = counts(input, index + 1, cache, isVowel);
		int total = include + exclude;

		cache[index][isVowel] = total;
		return total;

	}

	private static void createMap() {
		Map<Character, Boolean> map = new HashMap<>();
		map.put('a', true);
		map.put('e', true);
		map.put('i', true);
		map.put('o', true);
		map.put('u', true);
		map.put('A', true);
		map.put('E', true);
		map.put('I', true);
		map.put('O', true);
		map.put('U', true);

		vowels = map;
	}

}
