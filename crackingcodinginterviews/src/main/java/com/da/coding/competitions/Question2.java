package com.da.coding.competitions;

public class Question2 {
	public static void main(String[] args) {
		Question2 q = new Question2();
		System.out.println(q.solve(100000));

	}

	public int solve(int A) {
		long ans = 0;
		int i = 1;
		while (i <= A) {
			ans += P(i);
			i += 1;
		}

		return (int) (ans % 1000000007);
	}

	private int P(int m) {
		long ans = 1;
		int k = 1;
		while (k <= m) {
			if (gcd(k, m) == 1)
				ans *= k;
			k += 1;
		}
		return (int) (ans % m);
	}

	private int gcd(int a, int b) {

		if (a == 0 || b == 0) {
			return Math.max(a, b);
		}

		if (a == 1 || b == 1) {
			return 1;
		}

		if (a == b) {
			return a;
		}

		int gcd = 0;
		if (a > b) {
			gcd = gcd(a - b, b);
		} else {
			gcd = gcd(a, b - a);
		}

		return gcd;
	}

}
