package com.da.coding.competitions;
import java.util.*;
import java.util.Map.Entry;


public class HackerEarthTemplate {

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int testCases = scn.nextInt();
		for (int t = 0; t < testCases; t++) {
			int n = scn.nextInt();
			int k = scn.nextInt();
			int[] nums = new int[n];
			for (int i = 0; i < n; i++) {
				nums[i] = scn.nextInt();
			}
			System.out.println(solve(n, k, nums));
		}
		scn.close();
	}

	private static int solve(int n, int k, int[] nums) {
		return 0;
	}

}
