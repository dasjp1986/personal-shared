package com.da.coding.competitions;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Question5 {

    public static void main(String args[] ) throws Exception {
        Scanner scn = new Scanner(System.in);
        int testCases = scn.nextInt();
        for(int t=0;t<testCases;t++){
            int n= scn.nextInt();
            int m= scn.nextInt();
            int start= scn.nextInt();
            int end= scn.nextInt();
            Map<Integer,Boolean> map= new HashMap<>();
    
            for(int i=0;i<n;i++){
                int s= scn.nextInt();
                int e= scn.nextInt();
                
                for(int j=s;j<=e;j++){
                    map.put(j,true);
                }
            }
	        System.out.println(solve(start,end,map));
        }
        scn.close();
    }
    
    private static int solve(int start, int end, Map<Integer,Boolean> map){
        int count=0;
        
        for(int i=start;i<=end;i++){
            if(map.get(i)==null){
                count++;
            }
        }
        return count;
    }

}
