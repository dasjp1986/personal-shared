package com.da.coding.matrix;

public class GridUniquePaths {
	int[] rows = { 0, 1 };
	int[] columns = { 1, 0 };
	boolean[][] visited;

	public int uniquePaths(int A, int B) {
		visited = new boolean[A][B];
		return traverse(0, 0, A, B);
	}

	private int traverse(int row, int col, int A, int B) {
		int count = 0;
		if (row == A - 1 && col == B - 1) {
			return 1;
		}
		if (row < A && col < B) {
			for (int k = 0; k < rows.length; k++) {
				if (isSafe(row + rows[k], col + columns[k], A, B))
					count = count + traverse(row + rows[k], col + columns[k], A, B);
			}

		}
		return count;
	}

	boolean isSafe(int currentRow, int currentCol, int A, int B) {
		if (currentRow < A && currentCol < B) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		GridUniquePaths paths = new GridUniquePaths();
		System.out.println(paths.uniquePaths(15, 19));

	}
}
