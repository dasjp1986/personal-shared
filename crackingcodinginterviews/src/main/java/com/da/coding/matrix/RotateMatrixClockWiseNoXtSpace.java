package com.da.coding.matrix;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given a square matrix, turn it by 90 degrees in anti-clockwise
 *         direction without using any extra space.
 */
public class RotateMatrixClockWiseNoXtSpace {
	// approach is to rotate clock wise transpose the matrix first and then
	// reverse values in each
	// row

	// approach is to rotate anti clock wise transpose the matrix first and then
	// reverse values in each
	// column

	public static void main(String[] args) {
		int[][] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		rotateClockWise(matrix);
		print(matrix);

	}

	private static void print(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			System.out.println(Arrays.toString(matrix[i]));
		}
	}

	// Time Complexity- (N^2)
	// Space Complexity- O(1)
	public static void rotateClockWise(int[][] matrix) {
		transpose(matrix);
		print(matrix);
		reverseRows(matrix);
	}

	private static void reverseRows(int[][] matrix) {
		int temp = 0;
		for (int i = 0; i < matrix.length; i++) {
			int j = 0, k = matrix.length - 1;
			while (j < k) {
				temp = matrix[i][j];
				matrix[i][j] = matrix[i][k];
				matrix[i][k] = temp;
				j++;
				k--;
			}
		}
	}

	private static void transpose(int[][] matrix) {
		int temp = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < i; j++) {
				temp = matrix[i][j];
				matrix[i][j] = matrix[j][i];
				matrix[j][i] = temp;
			}
		}
	}
}
