package com.da.coding.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Write an efficient algorithm that searches for a value in an m x n
 *         matrix.
 * 
 *         This matrix has the following properties:
 * 
 *         Integers in each row are sorted from left to right. The first integer
 *         of each row is greater than or equal to the last integer of the
 *         previous row. Example:
 * 
 *         Consider the following matrix:
 * 
 *         [ [1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 50] ] Given target =
 *         3, return 1 ( 1 corresponds to true )
 * 
 *         Return 0 / 1 ( 0 if the element is not present, 1 if the element is
 *         present ) for this problem
 */
public class MatrixSearch {
	// Time complexity- O(Max(M,N))
	public int searchMatrix(List<List<Integer>> a, int target) {
		int rows = a.size();
		int cols = a.get(0).size();

		// start with top right
		int row = 0, col = cols - 1;
		int res = 0;
		while (true) {
			if (row < 0 || row >= rows || col < 0 || col >= cols) {
				break;
			}
			Integer element = a.get(row).get(col);
			if (element == target) {
				res = 1;
				break;
			} else if (target < element) {
				col--;
			} else {
				row++;
			}
		}

		return res;
	}

	public static void main(String[] args) {
		MatrixSearch matrixSearch = new MatrixSearch();
//		List<List<Integer>> list = new ArrayList<>();
//		list.add(Arrays.asList(1, 3, 5, 7));
//		list.add(Arrays.asList(10, 11, 16, 20));
//		list.add(Arrays.asList(23, 30, 34, 50));
//		System.out.println(matrixSearch.searchMatrix(list, 34));

		List<List<Integer>> list = new ArrayList<>();
		list.add(Arrays.asList(10, 20, 30, 40));
		list.add(Arrays.asList(15, 25, 35, 45));
		list.add(Arrays.asList(27, 29, 37, 48));
		list.add(Arrays.asList(32, 33, 39, 50));
		System.out.println(matrixSearch.searchMatrix(list, 49));

	}
}
