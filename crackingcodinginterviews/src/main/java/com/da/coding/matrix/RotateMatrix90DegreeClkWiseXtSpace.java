package com.da.coding.matrix;

import java.util.Arrays;

import com.da.coding.commons.SystemInputReaderUtil;
/**
 * 
 * @author dagraw1
 * Rotate a given matrix of n,n by 90 degrees to the left
 * Ex-
 * 
 * 	3 3
	1 2 3
	4 5 6
	7 8 9
	
	rotated as
	3 6 9
	2 5 8
	1 4 7
	
	Time Complexity- O(N^2), as each of the element is being accessed
	Space Complexity- O(N)
 */
public class RotateMatrix90DegreeClkWiseXtSpace {
	public static void main(String[] args) {
		int[][] matrix = SystemInputReaderUtil.readMatrix();

		int[][] result = rotate(0, matrix.length-1, 0, matrix[0].length-1, matrix);
		System.out.println("Matrix Rotated at 90 degree-");
		print(result);
	}

	private static void print(int[][] result) {
		System.out.println();
		for (int i = 0; i < result.length; i++) {
			System.out.println(Arrays.toString(result[i]));
		}
	}

	private static int[][] rotate(int rowStart, int rowEnd, int colStart, int colEnd, int[][] matrix) {
		if (rowStart >= rowEnd && colStart >= colStart) {
			return matrix;
		}

		// copy left->temp
		int[] temp = new int[rowEnd - rowStart + 1];
		for (int i = rowStart, j = 0; i <= rowEnd; i++) {
			temp[j++] = matrix[i][colStart];
		}
		// copy top->left
		for (int i = colStart, j=rowEnd; i <= colEnd; i++) {
			matrix[j--][colStart] = matrix[rowStart][i];
		}
		print(matrix);

		// copy right->top
		for (int i = rowStart; i <= rowEnd; i++) {
			matrix[rowStart][i] = matrix[i][colEnd];
		}
		print(matrix);
		
		// copy bottom->right
		for (int i = colEnd, j=rowStart; i >= colStart; i--) {
			matrix[j++][colEnd] = matrix[rowEnd][i];
		}
		print(matrix);

		// copy temp->bottom
		for (int i = 0, j=colStart; i < temp.length; i++) {
			matrix[rowEnd][j++] = temp[i];
		}
		print(matrix);
		return rotate(rowStart + 1, rowEnd - 1, colStart + 1, colEnd - 1, matrix);
	}
}
