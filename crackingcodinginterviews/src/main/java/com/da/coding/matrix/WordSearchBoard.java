package com.da.coding.matrix;

/**
 * 
 * @author dagraw1
 *
 *         Given a 2D board and a word, find if the word exists in the grid.
 * 
 *         The word can be constructed from letters of sequentially adjacent
 *         cell, where "adjacent" cells are those horizontally or vertically
 *         neighboring. The cell itself does not count as an adjacent cell. The
 *         same letter cell may be used more than once.
 * 
 *         Example :
 * 
 *         Given board =
 * 
 *         [ ["ABCE"], ["SFCS"], ["ADEE"] ] word = "ABCCED", -> returns 1, word
 *         = "SEE", -> returns 1, word = "ABCB", -> returns 1, word = "ABFSAB"
 *         -> returns 1 word = "ABCD" -> returns 0 Note that 1 corresponds to
 *         true, and 0 corresponds to false.
 */

public class WordSearchBoard {

	int[] x = { -1, 1, 0, 0 };
	int[] y = { 0, 0, -1, 1 };
	int m = 0;
	int n = 0;

	public int exist(String[] A, String B) {
		m = A.length;
		n = A[0].length();

		for (int r = 0; r < m; r++) {
			for (int c = 0; c < n; c++) {
				if (A[r].charAt(c) == B.charAt(0)) {
					if (dfs(r, c, A, B.substring(1))) {
						return 1;
					}
				}

			}
		}

		return 0;
	}

	boolean dfs(int r, int c, String[] a, String str) {
		if (str.isEmpty()) {
			return true;
		}

		for (int i = 0; i < x.length; i++) {
			int row = r + x[i];
			int col = c + y[i];

			if (row >= 0 && row < m && col >= 0 && col < n && a[row].charAt(col) == str.charAt(0)) {
				if (dfs(row, col, a, str.substring(1))) {
					return true;
				}
			}
		}

		return false;
	}
	
	public static void main(String[] args) {
		
	}

}
