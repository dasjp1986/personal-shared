package com.da.coding.matrix;

import java.util.Arrays;

import com.da.coding.commons.SystemInputReaderUtil;
/**
 * 
 * @author dagraw1
 * Given a matrix of size m*n
 * Set entire row and column as zero if matrix[r][c]==0, where r-row and c-column
 * 
 *  Ex-
 *  3 3
	1 0 0
	2 3 4
	2 3 4
	
	0 0 0
	2 0 0
	2 0 0
	
	Time Complexity- O(MN)
	Space Complexity- O(M+N)
 */
public class MatrixZeroOperation {
	public static void setToZero(int[][] matrix) {

		int m = matrix.length;
		int n = matrix[0].length;

		boolean[] isRowsZero = new boolean[m];
		boolean[] isColumssZero = new boolean[n];

		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[r].length; c++) {
				if (matrix[r][c] == 0) {
					isRowsZero[r] = true;
					isColumssZero[c] = true;
				}
			}
		}

		for (int i = 0; i < m; i++) {
			if (isRowsZero[i]) {
				for (int j = 0; j < n; j++) {
					matrix[i][j] = 0;
				}
			}
		}

		for (int i = 0; i < n; i++) {
			if (isColumssZero[i]) {
				for (int j = 0; j < m; j++) {
					matrix[j][i] = 0;
				}
			}
		}

		for (int i = 0; i < matrix.length; i++) {
			System.out.println(Arrays.toString(matrix[i]));
		}
	}

	public static void main(String[] args) {
		setToZero(SystemInputReaderUtil.readMatrix());
	}
}
