package com.da.coding.matrix;

import java.util.ArrayList;
import java.util.List;

public class MatrixSpiral {
	public ArrayList<Integer> spiralOrder(final List<ArrayList<Integer>> A) {

		ArrayList<Integer> res = new ArrayList<>();
		for (int k = 0; k <= A.size() / 2; k++) {
			int i = k;
			int j = k;
			while (j < A.size() - k) {
				res.add(A.get(i).get(j));
				j++;
			}
			j--;
			i++;
			while (i < A.size() - k) {
				res.add(A.get(i).get(j));
				i++;
			}
			i = j;
			j--;
			while (j >= k) {
				res.add(A.get(i).get(j));
				j--;
			}
			j++;
			i = A.size()-k-2;
			while (i > k) {
				res.add(A.get(i).get(j));
				i--;
			}

		}

		return res;
	}

	public static void main(String[] args) {
		List<ArrayList<Integer>> A = new ArrayList<>();
		int n = 4;
		int k=1;
		for (int i = 1; i <= n; i++) {
			ArrayList<Integer> l = new ArrayList<>();
			for (int j = 1; j <= n; j++) {
				l.add(k++);
			}
			A.add(l);
		}
		
		MatrixSpiral spiral= new MatrixSpiral();
		System.out.println(spiral.spiralOrder(A));

	}
}
