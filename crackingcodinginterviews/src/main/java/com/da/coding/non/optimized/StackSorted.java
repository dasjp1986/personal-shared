package com.da.coding.non.optimized;

import com.da.coding.stack.StackUsingLinkedList;

/**
 * 
 * @author dagraw1 
 * Keep the stack always sorted with smallest number at the top
 * of the stack
 * 
 * NOTE: MUST BE OPTIMIZED BY USING MERGE OR QUICK SORT
 */
//Space Complexity- O(N)
public class StackSorted {
	private StackUsingLinkedList<Integer> stack;

	public StackSorted() {
		this.stack = new StackUsingLinkedList<>();
	}

	//Time Complexity- O(N^2)
	public void push(Integer value) {
		if (stack.isEmpty()) {
			stack.push(value);
		} else {
			StackUsingLinkedList<Integer> tempStack = new StackUsingLinkedList<>();
			while (!stack.isEmpty() && stack.peek() < value) {
				tempStack.push(stack.pop());
			}
			stack.push(value);
			while (!tempStack.isEmpty()) {
				stack.push(tempStack.pop());
			}
		}
	}
	
	public Integer pop(){
		return stack.pop();
	}
	
	public Integer peek(){
		return stack.peek();
	}
	
	public static void main(String[] args) {
		StackSorted stack= new StackSorted();
		stack.push(10);
		stack.push(15);
		stack.push(5);
		stack.push(1);
		stack.push(17);
		stack.push(13);
		
		for (int i = 0; i < 6; i++) {
			System.out.println(stack.pop());
		}
	}
}
