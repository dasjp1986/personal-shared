package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *

Given A, how many structurally unique BST’s (binary search trees) that store values 1...A?

Example :

Given A = 3, there are a total of 5 unique BST’s.


   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3


 */
public class UniqueBinarySearchTrees {

    public int numTrees(int A) {
        int[] dp= new int[A+1];
        dp[0]=1;
        dp[1]=1;
        
        return recurse(dp,A);  
      
    }
    
    private int recurse(int[] dp, int n){
        if(dp[n]!=0){
            return dp[n];
        }

        int perms=0;
        for(int p=1;p<=n;p++){
            perms+= recurse(dp,p-1)*recurse(dp,n-p);
        }
        
        dp[n]= perms;
        
        return perms;
    }


}
