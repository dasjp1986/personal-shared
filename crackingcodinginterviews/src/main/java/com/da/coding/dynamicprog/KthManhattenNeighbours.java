package com.da.coding.dynamicprog;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given a matrix M of size nxm and an integer K, find the maximum
 *         element in the K manhattan distance neighbourhood for all elements in
 *         nxm matrix. In other words, for every element M[i][j] find the
 *         maximum element M[p][q] such that abs(i-p)+abs(j-q) <= K.
 * 
 *         Note: Expected time complexity is O(N*N*K)
 * 
 *         Constraints:
 * 
 *         1 <= n <= 300 1 <= m <= 300 1 <= K <= 300 0 <= M[i][j] <= 1000
 * 
 *         Example:
 * 
 *         Input: M = [[1,2,4],[4,5,8]] , K = 2
 * 
 *         Output: ans = [[5,8,8],[8,8,8]]
 * 
 * 
 */
public class KthManhattenNeighbours {

	public int[][] solve(int A, int[][] B) {

		int n = B.length;
		int m = B[0].length;
		int[][][] sol = new int[A + 1][n][m];

		for (int k = 0; k <= A; k++) {
			for (int r = 0; r < n; r++) {
				for (int c = 0; c < m; c++) {
					if (k == 0) {
						sol[k][r][c] = B[r][c];
					} else {
						int res = sol[k - 1][r][c];
						res = Math.max(res, c - 1 >= 0 ? sol[k - 1][r][c - 1] : 0);
						res = Math.max(res, c + 1 < m ? sol[k - 1][r][c + 1] : 0);
						res = Math.max(res, r - 1 >= 0 ? sol[k - 1][r - 1][c] : 0);
						res = Math.max(res, r + 1 < n ? sol[k - 1][r + 1][c] : 0);
						sol[k][r][c] = res;
					}

				}
			}
		}

		return sol[A];

	}

	public static void main(String[] args) {
		int[][] matrix = { { 1, 2, 4 }, { 4, 5, 8 } };
		KthManhattenNeighbours neig = new KthManhattenNeighbours();
		int[][] sol = neig.solve(4, matrix);
		for (int i = 0; i < sol.length; i++) {
			System.out.println(Arrays.toString(sol[i]));
		}

	}

}
