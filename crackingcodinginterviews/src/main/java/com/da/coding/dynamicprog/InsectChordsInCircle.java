package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given a number N, return number of ways you can draw N chords in a
 *         circle with 2*N points such that no 2 chords intersect. Two ways are
 *         different if there exists a chord which is present in one way and not
 *         in other.
 * 
 *         For example,
 * 
 *         N=2 If points are numbered 1 to 4 in clockwise direction, then
 *         different ways to draw chords are: {(1-2), (3-4)} and {(1-4), (2-3)}
 * 
 *         So, we return 2. Notes:
 * 
 *         1 ≤ N ≤ 1000 Return answer modulo 109+7.
 */
public class InsectChordsInCircle {
	public int chordCnt(int A) {

		if (A < 1) {
			return 0;
		}

		int[] cache = new int[A + 1];
		cache[0] = 1;
		cache[1] = 1;

		int modulo = (int) (Math.pow(10, 9) + 7);

		for (int i = 2; i < cache.length; i++) {
			for (int j = 0; j < i; j++) {
				cache[i] = cache[i] + (cache[j] * cache[i - j - 1]) % modulo;
				cache[i] = cache[i] % modulo;
			}
		}

		return cache[A];
	}

	public static void main(String[] args) {
		InsectChordsInCircle intersect = new InsectChordsInCircle();
		System.out.println(intersect.chordCnt(0));
		System.out.println(intersect.chordCnt(1));
		System.out.println(intersect.chordCnt(2));
		System.out.println(intersect.chordCnt(3));
		System.out.println(intersect.chordCnt(4));
		System.out.println(intersect.chordCnt(5));
	}
}
