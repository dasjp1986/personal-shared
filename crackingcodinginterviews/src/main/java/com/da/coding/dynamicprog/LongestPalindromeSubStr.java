package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given a string S, find the longest palindromic substring in S.
 * 
 *         Substring of string S:
 * 
 *         S[ i . . . . j ] where 0 ≤ i ≤ j < len(S)
 * 
 *         Palindrome string:
 * 
 *         A string which reads the same backwards. More formally, S is
 *         palindrome if reverse(S) = S.
 * 
 *         Incase of conflict, return the substring which occurs first ( with
 *         the least starting index ).
 */
public class LongestPalindromeSubStr {
	public static void main(String[] args) {
		System.out.println(longestStr("forgeeksskeegfor"));
	}

	//Time Complexity- O(n*n/2) ~ O(n^2)
	//Space Complexity- O(n*n)
	public static String longestStr(String input) {
		int[][] matrix = new int[input.length()][input.length()];

		int max = 1;
		int start = 0, end = 0;
		for (int row = input.length() - 1; row >= 0; row--) {
			for (int column = row; column < input.length(); column++) {
				if (column == row) {
					matrix[row][column] = 1;
				} else if (input.charAt(row) == input.charAt(column)) {
					matrix[row][column] = 2 + matrix[row + 1][column - 1];
					if (matrix[row][column] > max) {
						max = matrix[row][column];
						start = row;
						end = column;
					}
				}
			}
		}
		return input.substring(start, end + 1);
	}

}
