package com.da.coding.dynamicprog;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1 Given n pairs of opening and closing parenthesis, find all
 *         possible valid permutations
 * 
 *         Ex- n :3 Output: {((()))},{()()()},{()(())},{(())()},{(()())}
 */
public class Parenthesis {
	// Time Complexity- O(N*cat(N))
	// checkout for CATALAN Number here-
	// https://anonymouscoders.wordpress.com/2015/07/20/its-all-about-catalan/
	public static List<String> paren(int pairs) {
		List<String> result = new ArrayList<>();
		char[] array = new char[2 * pairs];
		permute(pairs, pairs, result, array, 0);
		return result;
	}

	private static void permute(int remLeft, int remRight, List<String> result, char[] array, int index) {
		if (remLeft == 0 && remRight == 0) {
			result.add(new String(array));
		}
		if (remLeft > 0) {
			array[index] = '(';
			permute(remLeft - 1, remRight, result, array, index + 1);
		}
		if (remRight > remLeft) {
			array[index] = ')';
			permute(remLeft, remRight - 1, result, array, index + 1);
		}
	}

	public static void main(String[] args) {
		List<String> parenList = paren(5);
		System.out.println(parenList);
		System.out.println(parenList.size());
	}
}
