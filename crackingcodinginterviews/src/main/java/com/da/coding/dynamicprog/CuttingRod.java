package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given an integer N denoting the Length of a line segment. you need to
 *         cut the line segment in such a way that the cut length of a line
 *         segment each time is integer either x , y or z. and after performing
 *         all cutting operation the total number of cutted segments must be
 *         maximum.
 * 
 *         Input
 * 
 *         2 4 2 1 1 5 5 3 2
 * 
 * 
 *         Output 4 2
 * 
 * 
 **/
public class CuttingRod {
	public static int iterations = 0;

	//Time and space complexity- O(target)
	private int cuts(int target, int x, int y, int z, int[] cache) {
		if (target == 0) {
			return 0;
		}
		if (target < 0) {
			return 0;
		}

		if (cache[target] != 0) {
			return cache[target];
		}
		iterations++;

		if (target >= x) {
			cache[target] = Math.max(cache[target], 1 + cuts(target - x, x, y, z, cache));
		}

		if (target >= y) {
			cache[target] = Math.max(cache[target], 1 + cuts(target - y, x, y, z, cache));
		}

		if (target >= z) {
			cache[target] = Math.max(cache[target], 1 + cuts(target - z, x, y, z, cache));
		}

		return cache[target];
	}

	public int rodCuts(int target, int x, int y, int z) {
		int[] cache = new int[target + 1];
		return cuts(target, x, y, z,cache);
	}

	public static void main(String[] args) {
		CuttingRod cuttingRod = new CuttingRod();
		System.out.println(cuttingRod.rodCuts(4, 5, 3, 2));
		System.out.println("iterations : " + iterations);

		iterations = 0;
		System.out.println(cuttingRod.rodCuts(4, 2, 1, 1));
		System.out.println("iterations : " + iterations);
	}
}
