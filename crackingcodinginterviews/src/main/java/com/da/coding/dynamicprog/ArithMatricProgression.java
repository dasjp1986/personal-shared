package com.da.coding.dynamicprog;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author dagraw1
 *
 *         Find longest Arithmetic Progression in an integer array and return
 *         its length. More formally, find longest sequence of indeces, 0 < i1 <
 *         i2 < … < ik < ArraySize(0-indexed) such that sequence A[i1], A[i2],
 *         …, A[ik] is an Arithmetic Progression. Arithmetic Progression is a
 *         sequence in which all the differences between consecutive pairs are
 *         the same, i.e sequence B[0], B[1], B[2], …, B[m - 1] of length m is
 *         an Arithmetic Progression if and only if B[1] - B[0] == B[2] - B[1]
 *         == B[3] - B[2] == … == B[m - 1] - B[m - 2]. Examples 1) 1, 2, 3(All
 *         differences are equal to 1) 2) 7, 7, 7(All differences are equal to
 *         0) 3) 8, 5, 2(Yes, difference can be negative too)
 * 
 *         Samples 1) Input: 3, 6, 9, 12 Output: 4
 * 
 *         2) Input: 9, 4, 7, 2, 10 Output: 3(If we choose elements in positions
 *         1, 2 and 4(0-indexed))
 */
public class ArithMatricProgression {

	// DO NOT MODIFY THE ARGUMENTS WITH "final" PREFIX. IT IS READ ONLY

	// Time Complexity- O(N^2)
	public int solveUsingMap(final int[] A) {

		if (A.length <= 2) {
			return A.length;
		}

		Arrays.sort(A);

		Map<Integer, Set<Integer>> map = new HashMap<>();

		int max = 0;
		int same = A[1] - A[0];
		int count = 1;
		for (int i = 0; i < A.length - 1; i++) {
			if (A[i + 1] - A[i] == same) {
				count++;
			}
			for (int j = i + 1; j < A.length; j++) {
				int diff = A[j] - A[i];
				Set<Integer> set = map.get(diff);
				if (set == null) {
					set = new HashSet<>();
					map.put(diff, set);
				}
				set.add(A[i]);
				set.add(A[j]);
				if (set.size() > max) {
					max = set.size();
				}
			}
		}
		return Math.max(count, max);

	}

	public int solveUsingDef(final int[] A) {

		if (A.length <= 2) {
			return A.length;
		}
		Arrays.sort(A);
		int count = 2;

		for (int j = 1; j < A.length - 1; j++) {
			int i = j - 1;
			int k = j + 1;

			while (i >= 0 && k < A.length) {
				if (2 * A[j] == A[i] + A[k]) {
					count++;
					break;
				} else if (2 * A[j] > A[i] + A[k]) {
					k++;
				} else {
					i--;
				}

			}
		}

		return count;
	}

	public static void main(String[] args) {
		ArithMatricProgression arth = new ArithMatricProgression();
		int[] a1 = { 6, 3, 12, 9 };
		System.out.println(arth.solveUsingMap(a1) + "," + arth.solveUsingDef(a1));

		int[] a2 = { 1, 7, 10, 15, 27, 29 };
		System.out.println(arth.solveUsingMap(a2) + "," + arth.solveUsingDef(a2));

		int[] a3 = { 5, 10, 15, 20, 25, 30 };
		System.out.println(arth.solveUsingMap(a3) + "," + arth.solveUsingDef(a3));

		int[] a4 = { 1, 1, 1, 1 };
		System.out.println(arth.solveUsingMap(a4) + "," + arth.solveUsingDef(a4));
	}

}
