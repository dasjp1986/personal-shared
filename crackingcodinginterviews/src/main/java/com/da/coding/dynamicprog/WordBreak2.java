package com.da.coding.dynamicprog;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author dagraw1
 *
 *         Given a string s and a dictionary of words dict, add spaces in s to
 *         construct a sentence where each word is a valid dictionary word.
 * 
 *         Return all such possible sentences.
 * 
 *         For example, given
 * 
 *         s = "catsanddog", dict = ["cat", "cats", "and", "sand", "dog"].
 * 
 *         A solution is
 * 
 *         [ "cat sand dog", "cats and dog" ]
 * 
 * 
 */
public class WordBreak2 {

	public String[] wordBreak(String A, String[] B) {
		if (A.isEmpty()) {
			return new String[] {};
		}
		StringBuilder builder = new StringBuilder();
		Set<String> set = find(A, B, builder);
		return set.toArray(new String[0]);
	}

	private Set<String> find(String str, String[] words, StringBuilder builder) {

		Set<String> result = new TreeSet<>();

		for (String word : words) {
			if (startsWith(str, word)) {
				StringBuilder bld = new StringBuilder(builder.toString());

				if (str.length() == word.length()) {
					bld.append(word);
					result.add(bld.toString());
				} else {
					bld.append(word + " ");
					Set<String> set = find(str.substring(word.length()), words, bld);
					if (!set.isEmpty()) {
						result.addAll(set);
					}

				}

			}
		}

		return result;

	}

	public static void main(String[] args) {
		WordBreak2 wb = new WordBreak2();
		// String[] array = wb.wordBreak("catsanddog", new String[] { "cat", "cats",
		// "and", "sand", "dog" });
		String[] array = wb.wordBreak("bbbabbbbbaababbababa",
				new String[] { "bba", "ababb", "aaabbbbbab", "b", "aba", "bbaababab" });
		System.out.println(Arrays.toString(array));
	}

	private boolean startsWith(String str, String word) {
		if (word.length() > str.length()) {
			return false;
		}

		int i = 0;
		while (i < word.length()) {
			if (word.charAt(i) != str.charAt(i)) {
				return false;
			}
			i++;
		}

		return true;
	}

}
