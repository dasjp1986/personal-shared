package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given two sequences, find the length of longest subsequence present
 *         in both of them. A subsequence is a sequence that appears in the same
 *         relative order, but not necessarily contiguous. For example, “abc”,
 *         “abg”, “bdf”, “aeg”, ‘”acefg”, .. etc are subsequences of “abcdefg”.
 *         So a string of length n has 2^n different possible subsequences.
 */
public class LongestCommonSubSeqIterative {

	// Time Complexity- O(M*N)
	// Space Complexity- O(M*N)
	public static String findLCSString(String str1, String str2) {
		if (str1 == null || str2 == null) {
			return "";
		}

		int rows = str1.length() + 1;
		int columns = str2.length() + 1;

		String[][] cache = new String[rows][columns];

		for (int r = 1; r < rows; r++) {
			for (int c = 1; c < columns; c++) {
				if (str1.charAt(r - 1) == str2.charAt(c - 1)) {
					cache[r][c] = (cache[r - 1][c - 1] == null ? "" : cache[r - 1][c - 1]) + str1.charAt(r - 1);
				} else {
					String first = cache[r][c - 1] == null ? "" : cache[r][c - 1];
					String second = cache[r - 1][c] == null ? "" : cache[r - 1][c];
					if (first.length() > second.length()) {
						cache[r][c] = first;
					} else {
						cache[r][c] = second;
					}

				}
			}
		}

//		for (int i = 0; i < cache.length; i++) {
//			System.out.println(Arrays.toString(cache[i]));
//		}
		return cache[rows - 1][columns - 1];

	}

	public static void main(String[] args) {
		System.out.println(findLCSString("abc", "acd"));
		System.out.println(findLCSString("adgh", "aedh"));
		System.out.println(findLCSString("ab", "ab"));
		System.out.println(findLCSString("abcdgh", "aedfhr"));
	}
}
