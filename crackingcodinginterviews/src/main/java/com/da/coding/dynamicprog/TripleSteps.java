package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 * 
 * 
 *         a child can run n steps in hops of 1,2,3 steps at a time. how many
 *         possible ways for him to complete n steps on staircase
 */
public class TripleSteps {
	public int findPossibleWays(int totalSteps, int[] hops) {
		int cache[] = new int[totalSteps + 1];
		return countWays(totalSteps, totalSteps, hops, cache);
	}

	private int countWays(int totalSteps, int remainSteps, int[] hops, int[] cache) {
		if (remainSteps == 0) {
			return 1;
		}
		if (cache[remainSteps] != 0) {
			return cache[remainSteps];
		}
		int ways = 0;
		for (int i = 0; i < hops.length; i++) {
			int remainder = remainSteps - hops[i];
			if (remainder >= 0) {
				ways += countWays(totalSteps, remainder, hops, cache);
			}
			cache[remainSteps] = ways;
		}
		return ways;
	}

	public static void main(String[] args) {
		TripleSteps steps = new TripleSteps();
		int[] hops = { 1, 2, 3 };
		System.out.println(steps.findPossibleWays(5, hops));
	}
}
