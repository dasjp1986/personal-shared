package com.da.coding.dynamicprog;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         A top secret message containing letters from A-Z is being encoded to
 *         numbers using the following mapping:
 * 
 *         'A' -> 1 'B' -> 2 ... 'Z' -> 26
 * 
 *         You are an FBI agent. You have to determine the total number of ways
 *         that message can be decoded. Note: An empty digit sequence is
 *         considered to have one decoding. It may be assumed that the input
 *         contains valid digits from 0 to 9 and If there are leading 0’s, extra
 *         trailing 0’s and two or more consecutive 0’s then it is an invalid
 *         string.
 * 
 *         Example : Given encoded message "123", it could be decoded as "ABC"
 *         (1 2 3) or "LC" (12 3) or "AW"(1 23). So total ways are 3.
 * 
 *         Input: First line contains the test cases T. 1<=T<=1000 Each test
 *         case have two lines First is length of string N. 1<=N<=40 Second line
 *         is string S of digits from '0' to '9' of N length.
 * 
 *         Example: Input: 2 3 123 4 2563 Output: 3 2
 */
public class TotalDecodingMsgs {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			char[] array = scn.next().toCharArray();
			System.out.println(solve(n, array));
		}
		scn.close();
	}

	//Time and SPace complexity- O(N)
	public static int solve(int n, char[] array) {
		Integer[] dp = new Integer[n + 1];
		if (array[0] == '0') {
			return 0;
		}
		return find(dp, n, array);
	}

	public static int find(Integer[] dp, int n, char[] array) {
		if (n == 0 || n == 1) {
			return 1;
		}

		if (dp[n] != null) {
			return dp[n];
		}

		dp[n] = 0;

		if (array[n - 1] == '0') {
			if (n - 2 < 0 || array[n - 2] > '2') {
				return 0;
			}
		} else {
			dp[n] = find(dp, n - 1, array);
		}

		if (array[n - 2] == '1' || (array[n - 2] == '2' && array[n - 1] < '7')) {
			dp[n] = dp[n] + find(dp, n - 2, array);
		}

		return dp[n];
	}

}
