package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Write a program to find nth fibonacci number index starts at 1
 * 
 *         Ex 0 1 1 2 3 5 8 Input: 1 Output: 0 Input: 5 Output: 3
 */
public class FibbonacciNumbers {

	// Time Complexity- O(n)
	// Space Complexity- O(n)
	public int fibonacciTopDown(int nth) {
		// ignore 0th index
		int[] cache = new int[nth + 1];
		if (nth < 1) {
			throw new RuntimeException("Invalid input");
		}
		return getFibTD(nth, cache);
	}

	private int getFibTD(int nth, int[] cache) {
		if (nth < 3) {
			return nth - 1;
		}
		if (cache[nth] == 0) {
			cache[nth] = getFibTD(nth - 1, cache) + getFibTD(nth - 2, cache);
		}

		return cache[nth];
	}

	// Time Complexity- O(n)
	// Space Complexity- O(n)
	public int fibonacciBottomUp(int nth) {
		// ignore 0th index
		int[] cache = new int[nth + 1];
		if (nth < 1) {
			throw new RuntimeException("Invalid input");
		}

		if (nth > 1) {
			cache[2] = 1;
		}

		for (int i = 3; i <= nth; i++) {
			cache[i] = cache[i - 1] + cache[i - 2];
		}

		return cache[nth];
	}

	// Time Complexity- O(n)
	// Space Complexity- O(1)
	public int fibonacciBottomUpSpace1(int nth) {
		if (nth < 1) {
			throw new RuntimeException("Invalid input");
		}

		if (nth < 3) {
			return nth - 1;
		}
		
		int a = 0;
		int b = 1;
		int c = 0;

		for (int i = 3; i <= nth; i++) {
			c = a + b;
			a = b;
			b = c;

		}

		return c;
	}

	public static void main(String[] args) {
		FibbonacciNumbers numbers = new FibbonacciNumbers();
		for (int i = 1; i < 10; i++) {
			System.out.println("ith : " + i);
			System.out.println(numbers.fibonacciTopDown(i));
			System.out.println(numbers.fibonacciBottomUp(i));
			System.out.println(numbers.fibonacciBottomUpSpace1(i));
		}
		

	}
}
