package com.da.coding.dynamicprog;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Find the contiguous subarray within an array (containing at least one
 *         number) which has the largest sum.
 * 
 *         For example:
 * 
 *         Given the array [-2,1,-3,4,-1,2,1,-5,4],
 * 
 *         the contiguous subarray [4,-1,2,1] has the largest sum = 6.
 * 
 *         For this problem, return the maximum sum.
 */

class MaxSumContiguousArray {

	public static void main(String[] args) {
		Integer[] A = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };

		MaxSumContiguousArray problem = new MaxSumContiguousArray();
		System.out.println(problem.maxSubArray(Arrays.asList(A)));
	}

	//Time Complexity- O(N)
	//Space Complexity- O(1)
	public int maxSubArray(final List<Integer> A) {
		if (A == null || A.size() == 0) {
			return 0;
		}
		int max = A.get(0);
		int prev = A.get(0);
		for (int i = 1; i < A.size(); i++) {
			if (prev + A.get(i) > A.get(i)) {
				prev = prev + A.get(i);
			} else {
				prev = A.get(i);
			}
			if (prev > max) {
				max = prev;
			}
		}

		return max;
	}

}
