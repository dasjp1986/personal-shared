package com.da.coding.dynamicprog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1
 * 
 *         Find all the permutations of the given String assuming duplicate
 *         characters can be present in the String
 * 
 *         but there should not be any duplicate string in the permutations
 *
 */
public class StringPermutationsDups {
	// Time Complexity O(n*n!) as there are n! leaves and each of them have a
	// path of length of n. printing each permutations can take another n
	// complexity so all together
	// it can go up to O(n*n*n!)
	public static void permutations(String input) {
		List<String> result = new ArrayList<>();
		Map<Key, String> cache = new HashMap<>();
		permute(input, "", result, cache);

		for (String res : result) {
			System.out.println(res);
		}
		System.out.println("Possible permutations for : " + input.length() + " is " + result.size());

	}

	private static void permute(String input, String output, List<String> result, Map<Key, String> cache) {
		// System.out.println("Running for input : " + input + " , output : " +
		// output);
		Key key = new Key(input, output);
		if (cache.get(key) != null) {
			output = "";
			input = "";
		}
		if (input.isEmpty() && !output.isEmpty()) {
			cache.put(key, output);
			result.add(output);
		} else {
			for (int i = 0; i < input.length(); i++) {
				permute(input.substring(0, i) + input.substring(i + 1), output + input.charAt(i), result, cache);
			}
		}
	}

	public static void main(String[] args) {
		//permutations("aabb");
		permutations("abc");
	}
}

class Key {
	private String input;
	private String output;

	public Key(String input, String output) {
		super();
		this.input = input;
		this.output = output;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		result = prime * result + ((output == null) ? 0 : output.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Key other = (Key) obj;
		if (input == null) {
			if (other.input != null)
				return false;
		} else if (!input.equals(other.input))
			return false;
		if (output == null) {
			if (other.output != null)
				return false;
		} else if (!output.equals(other.output))
			return false;
		return true;
	}

}
