package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 * 
 *         Given weights and values of n items, put these items in a knapsack of
 *         capacity W to get the maximum total value in the knapsack. In other
 *         words, given two integer arrays val[0..n-1] and wt[0..n-1] which
 *         represent values and weights associated with n items respectively.
 *         Also given an integer W which represents knapsack capacity, find out
 *         the maximum value subset of val[] such that sum of the weights of
 *         this subset is smaller than or equal to W.
 * 
 * 
 *         You cannot break an item, either pick the complete item, or don’t
 *         pick it (0-1 property).
 *
 */
public class KnapSack10Problem {

	public static void main(String[] args) {
//		int[] values = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
//		int[] weights = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
//		int total = 500;

		int[] values = { 2, 3, 4, 5, 1 };
		int[] weights = { 1, 2, 3, 4, 5 };
		int total = 10;

		//System.out.println(knaps(values, weights, total));
		System.out.println(pickTabulation(values, weights, total));
	}

	// Time Complexity- O(total*n), n-weights.length
	// Space Complexity- O(total*n), n-weights.length
//	public static int knaps(int[] values, int[] weights, int total) {
//		Integer[][] cache = new Integer[total + 1][weights.length];
//		return pick(values, weights, total, 0, weights.length - 1, cache);
//	}
//
//	private static int pick(int[] values, int[] weights, int total, int max, int index, Integer[][] cache) {
//		if (total == 0) {
//			return max;
//		}
//
//		if (index < 0) {
//			return 0;
//		}
//
//		if (cache[total][index] != null) {
//			return cache[total][index];
//		}
//		if (weights[index] > total) {
//			cache[total][index] = pick(values, weights, total, max, index - 1, cache);
//		} else {
//			cache[total][index] = Math.max(
//					pick(values, weights, total - weights[index], max + values[index], index - 1, cache),
//					pick(values, weights, total, max, index - 1, cache));
//		}
//
//		return cache[total][index];
//	}

	// Time Complexity- O(total*n), n-weights.length
	// Space Complexity- O(total*n), n-weights.length
	public static int pickTabulation(int[] values, int weights[], int capacity) {

		int[][] cache = new int[weights.length + 1][capacity + 1];

		for (int i = 1; i < cache.length; i++) {
			for (int j = 1; j < cache[0].length; j++) {
				if (weights[i-1] > j) {
					cache[i][j] = cache[i - 1][j];
				} else {
					cache[i][j] = Math.max(cache[i - 1][j], values[i-1] + cache[i - 1][j - weights[i-1]]);
				}
			}
		}

		return cache[weights.length][capacity];
	}
}
