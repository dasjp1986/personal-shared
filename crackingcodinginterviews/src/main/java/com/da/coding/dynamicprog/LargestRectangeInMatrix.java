package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given a 2D binary matrix filled with 0’s and 1’s, find the largest
 *         rectangle containing all ones and return its area.
 * 
 *         Bonus if you can solve it in O(n^2) or less.
 * 
 *         Example :
 * 
 *         A : [ 1 1 1 0 1 1 1 0 0 ]
 * 
 *         Output : 4
 * 
 *         As the max area rectangle is created by the 2x2 rectangle created by
 *         (0,1), (0,2), (1,1) and (1,2)
 * 
 * 
 */
public class LargestRectangeInMatrix {

	public int maximalRectangle(int[][] A) {

		int m = A.length;
		int n = A[0].length;
		int[][] dp = new int[m][n];
		int rect = 0;

		for (int r = 0; r < m; r++) {
			for (int c = 0; c < n; c++) {
				if (c == 0) {
					dp[r][c] = A[r][c];
				} else if (A[r][c] == 1) {
					dp[r][c] = dp[r][c - 1] + 1;
				}

				int row = r;
				int width = dp[r][c];
				while (row >= 0 && dp[row][c] > 0) {
					width = Math.min(width, dp[row][c]);
					rect = Math.max(rect, width * (r - row + 1));
					row--;
				}
			}
		}

		return rect;

	}

	public static void main(String[] args) {
		int[][] a = { { 1, 1, 1 }, { 0, 1, 1 }, { 1, 0, 0 } };
		LargestRectangeInMatrix rect = new LargestRectangeInMatrix();
		System.out.println(rect.maximalRectangle(a));
	}

}
