package com.da.coding.dynamicprog;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given a set of non-negative integers, and a value sum, determine if
 *         there is a subset of the given set with sum equal to given sum.
 *         Example:
 * 
 *         Input: set[] = {3, 34, 4, 12, 5, 2}, sum = 9 Output: True //There is
 *         a subset (4, 5) with sum 9.
 * 
 */
public class SubsetSumProblem {

	// Time and space complexity- O(set.length*targetSum)
	public boolean isSubsetSum(int[] set, int targetSum) {
		if (set == null || set.length == 0) {
			return targetSum == 0;
		}
		if (set.length == 1) {
			return set[0] == targetSum;
		}

		int m = set.length;
		int n = targetSum + 1;
		boolean[][] cache = new boolean[m][n];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (j == 0) {
					cache[i][j] = true;
				} else {
					if (set[i] > j) {
						cache[i][j] = i - 1 >= 0 ? cache[i - 1][j] : false;
					} else {
						cache[i][j] = i - 1 >= 0 && j - set[i] >= 0 ? cache[i - 1][j] | cache[i - 1][j - set[i]]
								: set[i] == j;
					}
				}
			}
		}
//		for (int i = 0; i < cache.length; i++) {
//			System.out.println(Arrays.toString(cache[i]));
//		}
		return cache[m - 1][n - 1];

	}

	public static void main(String[] args) {
		SubsetSumProblem subsetSumProblem = new SubsetSumProblem();
		System.out.println(subsetSumProblem.isSubsetSum(new int[] { 1, 2, 3, 4 }, 7));
		System.out.println(subsetSumProblem.isSubsetSum(new int[] { 3, 34, 4, 12, 5, 2 }, 9));
		System.out.println(subsetSumProblem.isSubsetSum(new int[] { 2, 3, 7, 8, 10 }, 11));

	}

}
