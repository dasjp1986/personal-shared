package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 * Multiply two numbers without using multiplication operations
 */
public class MultiplyNumbers {
	static int iterations = 0;

	//Time Complexity- O(LogS) where S is small number
	public static long multiply(int num1, int num2) {
		if (num1 == 0 || num2 == 0) {
			return 0;
		}

		if (num1 < num2) {
			return mult(num1, num2);
		} else {
			return mult(num2, num1);
		}
	}

	private static long mult(int small, int large) {

		iterations++;
		System.out.println("Running for..." + small);

		if (small == 1) {
			return large;
		}

		long sum = mult(small / 2, large);
		if (small % 2 == 0) {
			return sum + sum;
		} else {
			return sum + sum + large;
		}
	}

	public static void main(String[] args) {
		System.out.println(multiply(500, 700));
		System.out.println(iterations);
	}
}
