package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 * 
 *         You are given coins of worth 1,2,5,10 write to find to find all
 *         combinations sum up to n
 */
public class Coins {
	static int iterations = 0;

	// Time Complexity- O(M*N)
	// Space Complexity- O(M*N)
	public static int coins(int[] array, int n) {
		Integer[][] matrix = new Integer[array.length + 1][n + 1];
		return count(array, array.length, n, matrix);
	}

	private static int count(int[] array, int length, int n, Integer[][] matrix) {
		if (n == 0) {
			return 1;
		}

		if (n < 0) {
			return 0;
		}

		if (length < 1 && n > 0) {
			return 0;
		}

		if (matrix[length][n] != null) {
			return matrix[length][n];
		}

		iterations++;

		matrix[length][n] = count(array, length - 1, n, matrix) + count(array, length, n - array[length - 1], matrix);
		return matrix[length][n];
	}

	public static void main(String[] args) {
		int[] array = { 1, 2, 5, 10 };
		int n = 100;
		System.out.println(coins(array, n));
		System.out.println("Total Iterations: " + iterations + " Expected : " + array.length * n);

	}
}
