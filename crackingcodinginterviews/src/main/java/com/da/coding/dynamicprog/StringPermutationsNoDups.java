package com.da.coding.dynamicprog;

import java.util.ArrayList;

import java.util.List;

/**
 * 
 * @author dagraw1
 * 
 *         Find all the permutations of the given String assuming there are no
 *         duplicate characters in the String
 *
 */
public class StringPermutationsNoDups {
	// Time Complexity O(n*n!) as there are n! leaves and each of them have a
	// path of length of n printing each permutations can take another n
	// complexity so all together
	// it can go up to O(n*n*n!)
	public static void permutations(String input) {
		List<String> result = new ArrayList<>();
		permute(input, "", result);

		for (String res : result) {
			System.out.println(res);
		}
		System.out.println("Possible permutations for : " + input.length() + " is " + result.size());

	}

	private static void permute(String input, String output, List<String> result) {
		// System.out.println("Running for input : " + input + " , output : " +
		// output);
		if (input.isEmpty()) {
			result.add(output);
		} else {
			for (int i = 0; i < input.length(); i++) {
				permute(input.substring(0, i) + input.substring(i + 1), input.charAt(i) + output, result);
			}
		}
	}

	public static void main(String[] args) {
		permutations("abcdefg");
	}
}
