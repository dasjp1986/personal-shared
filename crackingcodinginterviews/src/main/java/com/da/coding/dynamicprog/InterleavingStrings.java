package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given s1, s2, s3, find whether s3 is formed by the interleaving of s1
 *         and s2.
 * 
 *         Example, Given:
 * 
 *         s1 = "aabcc", s2 = "dbbca", When s3 = "aadbbcbcac", return true. When
 *         s3 = "aadbbbaccc", return false.
 * 
 *         Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 */
public class InterleavingStrings {

	public int isInterleave(String s1, String s2, String s3) {
		if (s3.length() != s1.length() + s2.length()) {
			return 0;
		}

		Integer[][] cache = new Integer[s1.length() + 1][s2.length() + 1];

		return find(s1, 0, s2, 0, s3, 0, cache);
	}

	private int find(String s1, int i, String s2, int j, String s3, int k, Integer[][] cache) {
		if (k == s3.length()) {
			return 1;
		}

		if (cache[i][j] != null) {
			return cache[i][j];
		}
		int res = 0;
		if (i < s1.length() && s1.charAt(i) == s3.charAt(k) && j < s2.length() && s2.charAt(j) == s3.charAt(k)) {
			res = find(s1, i + 1, s2, j, s3, k + 1, cache) | find(s1, i, s2, j + 1, s3, k + 1, cache);
		} else if (i < s1.length() && s1.charAt(i) == s3.charAt(k)) {
			res = find(s1, i + 1, s2, j, s3, k + 1, cache);
		} else if (j < s2.length() && s2.charAt(j) == s3.charAt(k)) {
			res = find(s1, i, s2, j + 1, s3, k + 1, cache);
		}

		cache[i][j] = res;

		return res;
	}
	
	public static void main(String[] args) {
		InterleavingStrings inter= new InterleavingStrings();
		System.out.println(inter.isInterleave("ab", "bc", "abcb"));
	}

}
