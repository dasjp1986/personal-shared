package com.da.coding.dynamicprog;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 * 
 *         Given a screen made of pixels represented in matrix of size
 *         rows*columns. each cell of the matrix can be painted with some color.
 *         user can start painting the cells at row,column and then cursor keep
 *         paining to adjacent cells like the chain reactions.
 * 
 *         color of the entire screen will be changed once all the cells are
 *         colored
 */
public class PaintFill {
	// Time Complexity- O(rows*colms)
	// Space Complexity- O(rows*colms)
	public static void pixels(int rows, int colms, int r, int c) {
		boolean[][] matrix = new boolean[rows][colms];
		traverse(rows, colms, r, c, matrix);
		for (int i = 0; i < matrix.length; i++) {
			System.out.println(Arrays.toString(matrix[i]));
		}
	}

	private static void traverse(int rows, int colms, int r, int c, boolean[][] matrix) {

		if (r < 0 || r >= rows || c < 0 || c >= colms) {
			return;
		}
		if (!matrix[r][c]) {
			System.out.println("traversing for r: " + r + " c: " + c);
			matrix[r][c] = true;
			traverse(rows, colms, r - 1, c, matrix);
			traverse(rows, colms, r + 1, c, matrix);
			traverse(rows, colms, r, c - 1, matrix);
			traverse(rows, colms, r, c + 1, matrix);
		}
	}

	public static void main(String[] args) {
		pixels(5, 5, 2, 3);
	}
}
