package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 * 
 * 
 *         Given a boolean expression with operators & | and ^ and result Find
 *         all possible ways parenthesis can be applied to compute the expected
 *         result
 * 
 *         input- 1&0
 * 
 */
public class BooleanEval {
	public static int countEval(String exp, boolean result) {
		int ways = 0;
		if (exp.length() == 0) {
			return 0;
		}
		if (exp.length() == 1) {
			return getBoolean(exp) == result ? 1 : 0;
		}
		for (int i = 1; i < exp.length(); i = i + 2) {
			char operator = exp.charAt(i);
			String leftExp = exp.substring(0, i);
			String rightExp = exp.substring(i + 1, exp.length());

			int leftTrue = countEval(leftExp, true);
			int leftFalse = countEval(leftExp, false);
			int rightTrue = countEval(rightExp, true);
			int rightFalse = countEval(rightExp, false);

			int totalWaysExp = (leftTrue + leftFalse) * (rightTrue + rightFalse);
			int totalTrue = 0;
			if (operator == '&') {
				totalTrue = leftTrue * rightTrue;
			} else if (operator == '|') {
				totalTrue = (leftTrue * rightFalse) + (leftFalse * rightTrue) + (leftTrue * rightTrue);
			} else if (operator == '^') {
				totalTrue = (leftTrue * rightFalse) + (leftFalse * rightTrue);
			} else {
				throw new RuntimeException("Invalid Expression");
			}

			ways = ways + (result ? totalTrue : totalWaysExp - totalTrue);
		}

		return ways;
	}

	private static boolean getBoolean(String exp) {
		return Integer.parseInt(exp) == 1;
	}

	public static void main(String[] args) {
		String exp = "0&0&0&1^1|0";
		System.out.println(countEval(exp, true));
	}
}
