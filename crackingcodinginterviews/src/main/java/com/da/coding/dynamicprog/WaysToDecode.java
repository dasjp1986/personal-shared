package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         A message containing letters from A-Z is being encoded to numbers
 *         using the following mapping:
 * 
 *         'A' -> 1 'B' -> 2 ... 'Z' -> 26 Given an encoded message containing
 *         digits, determine the total number of ways to decode it.
 * 
 *         Example :
 * 
 *         Given encoded message "12", it could be decoded as "AB" (1 2) or "L"
 *         (12).
 * 
 *         The number of ways decoding "12" is 2.
 */
public class WaysToDecode {

	public int numDecodings(String input) {
		int n = input.length();
		int[] cache = new int[n + 1];

		if (input == null || input.length() == 0) {
			return 0;
		}

		cache[0] = 1;
		if (input.charAt(0) != '0') {
			cache[1] = 1;
		}

		for (int i = 2; i < cache.length; i++) {
			int one = Integer.parseInt(input.substring(i - 1, i));
			int two = Integer.parseInt(input.substring(i - 2, i));

			if (1 <= one && one <= 9) {
				cache[i] = cache[i] + cache[i - 1];
			}

			if (10 <= two && two <= 26) {
				cache[i] = cache[i] + cache[i - 2];
			}
		}
		return cache[n];
	}

	public static void main(String[] args) {
		WaysToDecode ways = new WaysToDecode();
		String input = "1";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="0";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="01";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="011";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="10";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="1012";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="123";
		System.out.println(input + ":" + ways.numDecodings(input));
		input="222";
		System.out.println(input + ":" + ways.numDecodings(input));
	}

}
