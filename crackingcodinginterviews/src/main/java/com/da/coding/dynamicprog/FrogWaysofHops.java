package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Frog steps either 1, 2 or 3 steps to go to top. In how many ways it
 *         reaches the top?
 * 
 *         Input:
 * 
 *         The first line of input contains an integer T denoting the number of
 *         test cases. The first line of each test case is N. Where is the
 *         number of steps it has to hop.
 * 
 *         Output:
 * 
 *         Print the number of ways.
 * 
 *         Constraints:
 * 
 *         1 ≤ T ≤ 50 1 ≤ N ≤ 50
 * 
 *         Example:
 * 
 *         Input: 2 1 5
 * 
 *         Output: 1 13
 */
public class FrogWaysofHops {
	//Time complexity- O(target*jumps)
	//Space complexity- O(target)
	public int ways(int target, int[] jumps) {
		int[] cache = new int[target + 1];

		return hops(target, cache, jumps);
	}

	private int hops(int target, int[] cache, int[] jumps) {
		if (target == 0) {
			return 1;
		}

		cache[target] = hops(target - jumps[0], cache, jumps);

		for (int i = 1; i < jumps.length; i++) {
			if (target >= jumps[i]) {
				cache[target] = cache[target] + hops(target - jumps[i], cache, jumps);
			}
		}
		return cache[target];
	}

	public static void main(String[] args) {
		FrogWaysofHops wys = new FrogWaysofHops();
		int[] jumps = { 1, 2, 3 };
		System.out.println(wys.ways(5, jumps));
	}
}
