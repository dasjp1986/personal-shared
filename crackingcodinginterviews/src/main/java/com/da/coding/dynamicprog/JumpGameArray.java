package com.da.coding.dynamicprog;

public class JumpGameArray {

	public int canJump(int[] A) {
		int[] cache = new int[A.length];
		if (A.length == 1 && A[0] == 0) {
			return 1;
		}

		cache[cache.length - 1] = 1;

		for (int i = cache.length - 2; i >= 0; i--) {
			int res = 0;
			for (int j = i + 1; j <= i + A[i] && j < A.length; j++) {
				res = res | cache[j];
			}

			cache[i] = res;
		}

		return cache[0];
	}

	public static void main(String[] args) {
		JumpGameArray jmp = new JumpGameArray();
		int[] a = { 0, 10, 0, 1, 1, 0 };
		System.out.println(jmp.canJump(a));

	}

}
