package com.da.coding.dynamicprog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         A robot is present at top left corner in a matrix of r-rows and
 *         c-columns it can move only in down or right position there are few
 *         cells in the matrix marked as off positions and robot will be blocked
 *         by them
 * 
 *         find a path for robot to reach to the down right corner
 */
public class RobotInGrid {
	static int iterations = 0;

	public static List<Cell> findPath(boolean matrix[][]) {
		List<Cell> path = new ArrayList<>();
		path.add(new Cell(0, 0));
		Map<Cell, List<Cell>> cache = new HashMap<>();
		path = walk(matrix, 0, 0, matrix.length - 1, matrix[0].length - 1, path, cache);
		return path;
	}

	private static List<Cell> walk(boolean[][] matrix, int startRow, int startCol, int lastRow, int lastCol,
			List<Cell> path, Map<Cell, List<Cell>> cache) {

		System.out.println("Visting [" + startRow + ", " + startCol + "]");
		Cell currentCell = new Cell(startRow, startCol);
		List<Cell> cachedPath = cache.get(currentCell);

		if (cachedPath != null) {
			return cachedPath;
		}
		iterations++;
		if (startRow == lastRow && startCol == lastCol) {
			return path;
		}

		List<Cell> newPath = null;
		Cell nextCell;
		if (startRow + 1 < matrix.length && matrix[startRow + 1][startCol]) {
			nextCell = new Cell(startRow + 1, startCol);
			path.add(nextCell);
			newPath = walk(matrix, startRow + 1, startCol, lastRow, lastCol, path, cache);
			if (newPath == null) {
				path.remove(nextCell);
			}

		}
		if (startCol + 1 < matrix[0].length && matrix[startRow][startCol + 1]) {
			nextCell = new Cell(startRow, startCol + 1);
			path.add(nextCell);
			newPath = walk(matrix, startRow, startCol + 1, lastRow, lastCol, path, cache);
			if (newPath == null) {
				path.remove(nextCell);
			}
		}
		cache.put(currentCell, newPath);
		return newPath;
	}

	public static void main(String[] args) {
		boolean[][] matrix = { { true, true, true }, { true, true, true }, { true, true, true }, { true, true, true },
				{ true, true, true } };
		System.out.println(findPath(matrix));
		System.out.println("Iterations : " + iterations);
	}
}

class Cell {
	private int row;
	private int column;

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public Cell(int row, int column) {
		super();
		this.row = row;
		this.column = column;
	}

	@Override
	public String toString() {
		return "Cell [row=" + row + ", column=" + column + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

}
