package com.da.coding.dynamicprog;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of integers, find the length of longest subsequence
 *         which is first increasing then decreasing.
 ** 
 *         Example: **
 * 
 *         For the given array [1 11 2 10 4 5 2 1]
 * 
 *         Longest subsequence is [1 2 10 4 2 1]
 * 
 *         Return value 6
 */
public class LengthOfLongestIncreasingDecreasingSeq {

	public int longestSubsequenceLength(final List<Integer> A) {
		int n = A.size();
		if (n < 2) {
			return n;
		}
		int[] inc = new int[n];
		int[] dec = new int[n];
		inc[0] = 1;
		for (int i = 1; i < n; i++) {
			inc[i] = 1;
			for (int j = i - 1; j >= 0; j--) {
				if (A.get(i) - A.get(j) > 0) {
					inc[i] = Math.max(inc[i], inc[j] + 1);
				}
			}
		}
		System.out.println(Arrays.toString(inc));
		dec[n - 1] = 1;
		for (int i = n - 2; i >= 0; i--) {
			dec[i] = 1;
			for (int j = i + 1; j < n; j++) {
				if (A.get(i) - A.get(j) > 0) {
					dec[i] = Math.max(dec[i], dec[j] + 1);
				}
			}
		}
		System.out.println(Arrays.toString(dec));
		int max = 0;
		for (int i = 0; i < n; i++) {
			if (inc[i] + dec[i] - 1 > max) {
				max = inc[i] + dec[i] - 1;
			}
		}
		return max;
	}

	public static void main(String[] args) {
		LengthOfLongestIncreasingDecreasingSeq lenth = new LengthOfLongestIncreasingDecreasingSeq();
		// Integer[] array = { 1, 11, 2, 10, 4, 5, 2, 1 };

		Integer[] array = { 1, 2, 3, 4 };

		System.out.println(lenth.longestSubsequenceLength(Arrays.asList(array)));
	}

}
