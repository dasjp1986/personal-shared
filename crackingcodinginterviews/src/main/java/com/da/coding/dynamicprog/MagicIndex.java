package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1 Given a sorted array with distinct numbers return magic
 *         number such that A[i]=i
 *         
 *         follow up question if numbers are not distinct
 */
public class MagicIndex {
	//Time Complexity- O(LogN)
	public int findMagicIndexDistinct(int[] input){
        return findIndex(input,0,input.length-1);
    }

    private int findIndex(int[] input,int start,int last){
        if(start>last){
            return -1;
        }

        int mid= (start+last)/2;
        if(mid==input[mid]){
            return mid;
        }else if(input[mid]>mid){
            return findIndex(input,start,last-1);
        }else{
            return findIndex(input,start+1,last);
        }
    }

  //Time Complexity- O(N)
    public int findMagicIndexNotDistinct(int[] input){
        for(int i=0;i<input.length;i++){
            if(input[i]==i){
                return i;
            }
        }

        return -1;
    }
    public static void main(String[] args){
        MagicIndex ind= new MagicIndex();
        int[] distinctInput={-4,-3,-1,2,4,5,7};
        System.out.println(ind.findMagicIndexDistinct(distinctInput));
        
        int[] nonDistinctInput={-4,-3,-1,1,5,5,7};
        System.out.println(ind.findMagicIndexNotDistinct(nonDistinctInput));
        
    }
}
