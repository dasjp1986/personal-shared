package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given two sequences, find the length of longest subsequence present
 *         in both of them. A subsequence is a sequence that appears in the same
 *         relative order, but not necessarily contiguous. For example, “abc”,
 *         “abg”, “bdf”, “aeg”, ‘”acefg”, .. etc are subsequences of “abcdefg”.
 *         So a string of length n has 2^n different possible subsequences.
 */
public class LongestCommonSubSequence {
	private static int iterations = 0;

	// Time Complexity- O(M*N)
	// Space Complexity- O(M*N)
	public static String common(String str1, String str2) {
		if (str1 == null || str2 == null) {
			return "";
		}

		iterations = 0;
		String[][] cache = new String[str1.length() + 1][str2.length() + 1];
		return lcs(str1, str2, 0, 0, cache);
	}

	private static String lcs(String str1, String str2, int s1, int s2, String[][] cache) {
		if (s1 == str1.length() || s2 == str2.length()) {
			return "";
		}
		if (cache[s1][s2] != null) {
			return cache[s1][s2];
		}
		iterations++;
		StringBuilder builder = new StringBuilder();
		if (str1.charAt(s1) == str2.charAt(s2)) {
			builder.append(str1.charAt(s1));
			builder.append(lcs(str1, str2, s1 + 1, s2 + 1, cache));
		} else {
			String first = lcs(str1, str2, s1, s2 + 1, cache);
			String second = lcs(str1, str2, s1 + 1, s2, cache);
			if (first.length() > second.length()) {
				builder.append(first);
			} else {
				builder.append(second);
			}
		}
		cache[s1][s2] = builder.toString();
		return builder.toString();
	}

	public static void main(String[] args) {
		System.out.println(common("abc", "acd"));
		System.out.println(lcsIterative("abc", "acd"));
		System.out.println(common("adgh", "aedh"));
		System.out.println(lcsIterative("adgh", "aedh"));
		System.out.println(common("ab", "ab"));
		System.out.println(lcsIterative("ab", "ab"));
		System.out.println(common("abcdgh", "aedfhr"));
		System.out.println(lcsIterative("abcdgh", "aedfhr"));
		//System.out.println("Iterations : " + iterations);
	}

	// Time Complexity- O(M*N)
	// Space Complexity- O(M*N)
	public static String lcsIterative(String s1, String s2) {

		if (s1 == null || s1.isEmpty() || s2 == null || s2.isEmpty()) {
			return "";
		}

		String[][] cache = new String[s1.length() + 1][s2.length() + 1];

		for (int i = 0; i < cache.length; i++) {
			for (int j = 0; j < cache.length; j++) {
				cache[i][j] = "";
			}
		}

		for (int i = 1; i < cache.length; i++) {
			for (int j = 1; j < cache.length; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					cache[i][j] = cache[i - 1][j - 1] + s1.charAt(i - 1);
				} else {
					if (cache[i - 1][j].length() > cache[i][j - 1].length()) {
						cache[i][j] = cache[i - 1][j];
					} else {
						cache[i][j] = cache[i][j - 1];
					}
				}
			}
		}

		return cache[s1.length()][s2.length()];
	}
}
