package com.da.coding.dynamicprog;

import java.util.ArrayList;

public class MinPathTriangle {

	public int minimumTotal(ArrayList<ArrayList<Integer>> a) {
		int globalMin=a.get(0).get(0);
		for (int i = 1; i < a.size(); i++) {
			globalMin= Integer.MAX_VALUE;
			for (int j = 0; j < a.get(i).size(); j++) {
				int left = Integer.MAX_VALUE;
				int right = Integer.MAX_VALUE;
				if (j > 0) {
					left = a.get(i - 1).get(j - 1);
				}
				if (j < a.get(i - 1).size()) {
					right = a.get(i - 1).get(j);
				}
				int min = Math.min(left, right);
				a.get(i).set(j, a.get(i).get(j) + min);
				globalMin = Math.min(globalMin, a.get(i).get(j));
			}
		}

		return globalMin;
	}

	public static void main(String[] args) {
		ArrayList<ArrayList<Integer>> list = new ArrayList<>();
		ArrayList<Integer> sList = new ArrayList<>();
		sList.add(2);
		list.add(sList);

		sList = new ArrayList<>();
		sList.add(6);
		sList.add(6);
		list.add(sList);

		sList = new ArrayList<>();
		sList.add(7);
		sList.add(8);
		sList.add(4);
		list.add(sList);

		MinPathTriangle tr = new MinPathTriangle();
		System.out.println(tr.minimumTotal(list));

	}

}
