package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Find the contiguous subarray within an array (containing at least one
 *         number) which has the largest product. Return an integer
 *         corresponding to the maximum product possible.
 * 
 *         Example :
 * 
 *         Input : [2, 3, -2, 4] Return : 6
 * 
 *         Possible with [2, 3]
 * 
 *         Expected Time complexity- O(N) and Space O(1)
 * 
 */
public class MaxProductSubArray {
	public int maxProduct(final int[] A) {
		if (A.length == 0) {
			return 0;
		}
		int posProduct = A[0];
		int negProduct = A[0];
		int maxProduct = A[0];
		for (int i = 1; i < A.length; i++) {
			int a = posProduct * A[i];
			int b = negProduct * A[i];
			posProduct = Math.max(Math.max(a, b), A[i]);
			negProduct = Math.min(Math.min(a, b), A[i]);
			maxProduct = Math.max(posProduct, maxProduct);
		}
		return maxProduct;
	}
}
