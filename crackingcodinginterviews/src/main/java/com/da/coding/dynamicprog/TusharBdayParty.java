package com.da.coding.dynamicprog;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         As it is Tushar’s Birthday on March 1st, he decided to throw a party
 *         to all his friends at TGI Fridays in Pune. Given are the eating
 *         capacity of each friend, filling capacity of each dish and cost of
 *         each dish. A friend is satisfied if the sum of the filling capacity
 *         of dishes he ate is equal to his capacity. Find the minimum cost such
 *         that all of Tushar’s friends are satisfied (reached their eating
 *         capacity).
 * 
 *         NOTE:
 * 
 *         Each dish is supposed to be eaten by only one person. Sharing is not
 *         allowed. Each friend can take any dish unlimited number of times.
 *         There always exists a dish with filling capacity 1 so that a solution
 *         always exists.
 * 
 *         Input Format
 * 
 *         Friends : List of integers denoting eating capacity of friends
 *         separated by space. Capacity: List of integers denoting filling
 *         capacity of each type of dish. Cost : List of integers denoting cost
 *         of each type of dish.
 * 
 *         Constraints: 1 <= Capacity of friend <= 1000 1 <= No. of friends <=
 *         1000 1 <= No. of dishes <= 1000
 * 
 *         Example:
 * 
 *         Input: 2 4 6 2 1 3 2 5 3
 * 
 *         Output: 14
 * 
 *         Explanation: First friend will take 1st and 2nd dish, second friend
 *         will take 2nd dish twice. Thus, total cost = (5+3)+(3*2)= 14
 * 
 * 
 */
public class TusharBdayParty {

	// DO NOT MODIFY THE ARGUMENTS WITH "final" PREFIX. IT IS READ ONLY
	public int solve(final int[] fnds, final int[] capacity, final int[] cost) {
		int sum = 0;

		Map<Integer, Integer> cache = new HashMap<>();
		for (int i = 0; i < fnds.length; i++) {
			sum += find(fnds[i], capacity, cost, cache);
		}

		return sum;
	}

	private int find(int hunger, int[] capacity, int[] cost, Map<Integer, Integer> cache) {
		if (cache.get(hunger) != null) {
			return cache.get(hunger);
		}

		if (hunger <= 0) {
			return 0;
		}

		int minCost = Integer.MAX_VALUE;

		for (int i = 0; i < capacity.length; i++) {
			if (hunger >= capacity[i]) {
				minCost = Math.min(minCost, cost[i] + find(hunger - capacity[i], capacity, cost, cache));
			}
		}

		cache.put(hunger, minCost);
		return minCost;
	}

}
