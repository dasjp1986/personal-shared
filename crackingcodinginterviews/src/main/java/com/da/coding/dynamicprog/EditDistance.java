package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *         Given two strings str1 and str2 and below operations that can
 *         performed on str1. Find minimum number of edits (operations) required
 *         to convert ‘str1’ into ‘str2’.
 * 
 *         Insert Remove Replace All of the above operations are of equal cost.
 * 
 *         Examples:
 * 
 *         Input: str1 = "geek", str2 = "gesek" Output: 1 We can convert str1
 *         into str2 by inserting a 's'.
 * 
 *         Input: str1 = "cat", str2 = "cut" Output: 1 We can convert str1 into
 *         str2 by replacing 'a' with 'u'.
 * 
 *         Input: str1 = "sunday", str2 = "saturday" Output: 3 Last three and
 *         first characters are same. We basically need to convert "un" to
 *         "atur". This can be done using below three operations. Replace 'n'
 *         with 'r', insert t, insert a
 */
public class EditDistance {
	public static int edit(String str1, String str2, int m, int n, int[][] cache) {
		if (m == 0) {
			return n;
		}
		if (n == 0) {
			return m;
		}

		if (cache[m][n] != 0) {
			return cache[m][n];
		}
		if (str1.charAt(m - 1) == str2.charAt(n - 1)) {
			cache[m][n] = edit(str1, str2, m - 1, n - 1,cache);
		} else {
			int add = edit(str1, str2, m - 1, n,cache);
			int remove = edit(str1, str2, m, n - 1,cache);
			int replace = edit(str1, str2, m - 1, n - 1,cache);
			cache[m][n] = Math.min(Math.min(add, remove), replace) + 1;
		}

		return cache[m][n];

	}

	public static int editDistance(String str1, String str2) {
		int[][] cache = new int[str1.length() + 1][str2.length() + 1];
		return edit(str1, str2, str1.length(), str2.length(), cache);
	}

	public static void main(String[] args) {
		System.out.println(editDistance("geek", "gesek"));
		System.out.println(editDistance("cat", "cut"));
		System.out.println(editDistance("sunday", "saturday"));
		System.out.println(editDistance("abcdef", "fabcde"));

	}
}
