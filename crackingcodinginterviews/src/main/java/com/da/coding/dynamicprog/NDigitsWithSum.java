package com.da.coding.dynamicprog;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Find out the number of N digit numbers, whose digits on being added
 *         equals to a given number S. Note that a valid number starts from
 *         digits 1-9 except the number 0 itself. i.e. leading zeroes are not
 *         allowed.
 * 
 *         Since the answer can be large, output answer modulo 1000000007
 **
 * 
 * 
 *         N = 2, S = 4 Valid numbers are {22, 31, 13, 40} Hence output 4.
 */
public class NDigitsWithSum {

	public int solve(int d, int n) {
		Map<String, Integer> cache = new HashMap<>();
		long count = 0;
		for (int i = 1; i <= 9 && i <= n; i++) {
			count = count + find(d - 1, n - i, cache) % 1000000007;
			count = count % 1000000007;
		}

		return (int) count;
	}

	private int find(int d, int n, Map<String, Integer> cache) {
		if (n == 0) {
			return 1;
		}

		if (d == 1 && 1 <= n && n <= 9) {
			return 1;
		}

		if (d == 0 && n != 0) {
			return 0;
		}
		String key = d + ":" + n;

		if (cache.get(key) != null) {
			return cache.get(key);
		}

		long count = 0;

		for (int i = 0; i <= 9 && i <= n; i++) {
			count = count + find(d - 1, n - i, cache) % 1000000007;
			count = count % 1000000007;
		}

		cache.put(key, (int) count);

		return (int) count;
	}
	
	public static void main(String[] args) {
		NDigitsWithSum sum= new NDigitsWithSum();
		System.out.println(sum.solve(3, 4));
	}

}
