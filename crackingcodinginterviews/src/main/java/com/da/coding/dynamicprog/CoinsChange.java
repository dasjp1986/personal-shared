package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         You are given a set of coins S. In how many ways can you make sum N
 *         assuming you have infinite amount of each coin in the set.
 * 
 *         Note : Coins in set S will be unique. Expected space complexity of
 *         this problem is O(N).
 * 
 *         Example :
 * 
 *         Input : S = [1, 2, 3] N = 4
 * 
 *         Return : 4
 * 
 *         Explanation : The 4 possible ways are {1, 1, 1, 1} {1, 1, 2} {2, 2}
 *         {1, 3}
 * 
 *         Note that the answer can overflow. So, give us the answer % 1000007
 * 
 */
public class CoinsChange {

	// Time complexity- O(M*N)= M= array.length and N= n
	// Space complexity- O(N)= M= array.length and N= n
	public int coinchange2(int[] array, int n) {
		int[] dp = new int[n + 1];

		dp[0] = 1;

		for (int i = 0; i < array.length; i++) {
			for (int j = array[i]; j <= n; j++) {
				dp[j] = (dp[j] + dp[j - array[i]]) % 1000007;
			}
		}
		return dp[n];
	}

	// Time complexity- O(M*N)= M= array.length and N= n
	// Space complexity- O(M*N)= M= array.length and N= n
	public int coinChange(int[] array, int n) {
		Integer[][] dp = new Integer[n + 1][array.length];

		return find(array, n, 0, dp);
	}

	private int find(int[] array, int target, int index, Integer[][] dp) {

		if (target == 0) {
			return 1;
		}

		if (target < 0 || index == array.length) {
			return 0;
		}

		if (dp[target][index] != null) {
			return dp[target][index];
		}

		dp[target][index] = find(array, target, index + 1, dp) + find(array, target - array[index], index, dp);

		return dp[target][index];
	}

	public static void main(String[] args) {
		CoinsChange cg = new CoinsChange();
		System.out.println(cg.coinchange2(new int[] { 1, 2, 5, 10 }, 100));
		System.out.println(cg.coinChange(new int[] { 1, 2, 5, 10 }, 100));
		System.out.println(cg.coinchange2(new int[] { 1, 2, 3 }, 5));
		System.out.println(cg.coinChange(new int[] { 1, 2, 3 }, 5));
	}

}
