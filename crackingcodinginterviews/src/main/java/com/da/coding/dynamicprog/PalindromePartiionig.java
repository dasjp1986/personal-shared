package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given a string s, partition s such that every substring of the
 *         partition is a palindrome.
 * 
 *         Return the minimum cuts needed for a palindrome partitioning of s.
 * 
 *         Example : Given s = "aab", Return 1 since the palindrome partitioning
 *         ["aa","b"] could be produced using 1 cut.
 * 
 */
public class PalindromePartiionig {
	public int minCut(String A) {
		int length = A.length();
		int[][] dp = new int[length][length];
		boolean[][] p = new boolean[length][length];
		for (int i = 0; i < length; i++) {
			for (int j = i + 1; j <= length; j++) {
				StringBuffer sb = new StringBuffer(A.substring(i, j));
				if (String.valueOf(sb).equals(String.valueOf(sb.reverse()))) {
					p[i][j - 1] = true;
				}
			}
		}
		for (int len = 1; len < length; len++) {
			int j = 0;
			for (int i = 0; i < length - len; i++) {
				j = i + len;
				dp[i][j] = Integer.MAX_VALUE;
				if (p[i][j] == true) {
					dp[i][j] = 0;
				} else {
					for (int k = i; k < j; k++) {
						dp[i][j] = Math.min(dp[i][k] + 1 + dp[k + 1][j], dp[i][j]);
					}
				}
			}
		}
		return dp[0][length - 1];
	}
}
