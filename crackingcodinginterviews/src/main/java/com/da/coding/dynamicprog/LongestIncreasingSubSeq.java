package com.da.coding.dynamicprog;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         The Longest Increasing Subsequence (LIS) problem is to find the
 *         length of the longest subsequence of a given sequence such that all
 *         elements of the subsequence are sorted in increasing order. For
 *         example, the length of LIS for {10, 22, 9, 33, 21, 50, 41, 60, 80} is
 *         6 and LIS is {10, 22, 33, 50, 60, 80}.
 */
public class LongestIncreasingSubSeq {
	public static int iterations = 0;

	// Solution-1
	// Time and space complexity ~(n*k), k is length of longest subsequence
	public int longestSubSeq(int[] a) {
		Map<LISKey, Integer> cache = new HashMap<>();
		return Math.max(1 + lis(a, 1, a[0], cache), lis(a, 1, Integer.MIN_VALUE, cache));
	}

	private int lis(int[] a, int index, int largest, Map<LISKey, Integer> cache) {
		if (index == a.length) {
			return 0;
		}
		LISKey key = new LISKey(index, largest);
		Integer result = cache.get(key);
		if (result != null) {
			return result;
		}
		iterations++;
		if (a[index] >= largest) {
			result = Math.max(1 + lis(a, index + 1, a[index], cache), lis(a, index + 1, largest, cache));
		} else {
			result = lis(a, index + 1, largest, cache);
		}

		cache.put(key, result);

		return result;
	}

	// Solution-2, BETTER SOLUTION
	public int lis(final int[] A) {
		int[] cache = new int[A.length];
		int max = 0;

		for (int i = 0; i < A.length; i++) {
			int count = 1 + find(i, A, cache);
			if (count > max) {
				max = count;
			}
		}

		return max;

	}

	private int find(int last, int[] a, int[] cache) {
		int max = 0;
		if (last == a.length) {
			return 0;
		}

		if (cache[last] != 0) {
			return cache[last];
		}

		for (int i = last + 1; i < a.length; i++) {
			if (a[i] > a[last]) {
				max = Math.max(max, 1 + find(i, a, cache));
			}
		}

		cache[last] = max;

		return max;
	}

	public static void main(String[] args) {
		LongestIncreasingSubSeq lis = new LongestIncreasingSubSeq();
		int[] a1 = { 10, 22, 9, 33, 21, 50, 41, 60, 80 };
		System.out.println(lis.longestSubSeq(a1));
		System.out.println("Iterations : " + iterations);

		iterations = 0;
		int[] a2 = { 5, 6, 7, 1, 2, 3, 4 };
		System.out.println(lis.longestSubSeq(a2));
		System.out.println("Iterations : " + iterations);

		iterations = 0;
		int[] a3 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		System.out.println(lis.longestSubSeq(a3));
		System.out.println("Iterations : " + iterations);

		iterations = 0;
		int[] a4 = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		System.out.println(lis.longestSubSeq(a4));
		System.out.println("Iterations : " + iterations);
	}
}

class LISKey {
	private Integer index;
	private Integer largest;

	public LISKey(Integer index, Integer largest) {
		this.index = index;
		this.largest = largest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((index == null) ? 0 : index.hashCode());
		result = prime * result + ((largest == null) ? 0 : largest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LISKey other = (LISKey) obj;
		if (index == null) {
			if (other.index != null)
				return false;
		} else if (!index.equals(other.index))
			return false;
		if (largest == null) {
			if (other.largest != null)
				return false;
		} else if (!largest.equals(other.largest))
			return false;
		return true;
	}

}
