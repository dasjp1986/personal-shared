package com.da.coding.dynamicprog;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 *         You are given 3 towers- source, auxiliary, destination and n- disks
 *         initially all the disks are placed on to the source tower write a
 *         program to move all the disks to destination tower provided
 * 
 *         - All the disks are placed in ascending order of their size from top
 *         to bottom
 * 
 *         - you can only place smaller disk onto the larger disk
 * 
 *         - you can move only one disk at a time
 * 
 */
public class TowerOfHanoi {
	// Time Complexity- (2^(N+1))
	public static void main(String[] args) {
		Tower src = new Tower("source");
		Tower aux = new Tower("aux");
		Tower dest = new Tower("destination");

		int disks = 10;
		for (int disk = disks; disk > 0; disk--) {
			src.add(disk);
		}

		src.moveDisks(disks, aux, dest);
		dest.print();
		System.out.println("Iterations : " + Tower.iterations);
	}

}

class Tower {
	static int iterations = 0;
	private String id;
	private Stack<Integer> stack;

	public Tower(String id) {
		this.id = id;
		this.stack = new Stack<>();
	}

	public void add(int num) {
		stack.push(num);
	}

	public void print() {
		while (!stack.isEmpty()) {
			System.out.println(stack.pop());
		}
	}

	public int pop() {
		return stack.pop();
	}

	public void moveToTop(Tower dest) {
		dest.add(this.pop());
	}

	public void moveDisks(int disks, Tower aux, Tower dest) {
		iterations++;
		System.out.println(disks + ":" + this.toString() + aux + dest);
		if (disks > 0) {
			moveDisks(disks - 1, dest, aux);
			moveToTop(dest);
			aux.moveDisks(disks - 1, this, dest);
		}
	}

	@Override
	public String toString() {
		return stack.toString();
	}

}
