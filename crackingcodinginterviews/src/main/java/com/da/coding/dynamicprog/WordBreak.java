package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given a string s and a dictionary of words dict, determine if s can
 *         be segmented into a space-separated sequence of one or more
 *         dictionary words.
 * 
 *         For example, given
 * 
 *         s = "myinterviewtrainer", dict = ["trainer", "my", "interview"].
 * 
 *         Return 1 ( corresponding to true ) because "myinterviewtrainer" can
 *         be segmented as "my interview trainer".
 * 
 *         Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 * 
 */
public class WordBreak {

	public int wordBreak(String str, String[] words) {
		int[] dp = new int[str.length()];
		int ans = solve(0, dp, str, words);
		return ans == 1 ? 1 : 0;
	}

	static boolean startsWith(int index, String word, String str) {
		if ((index + word.length()) > str.length())
			return false;
		for (int i = 0; i < word.length(); i++)
			if (str.charAt(i + index) != word.charAt(i))
				return false;
		return true;
	}

	static int solve(int index, int[] dp, String str, String[] words) {
		if (index == str.length())
			return 1;
		if (dp[index] != 0)
			return dp[index];
		int ans = -1;
		for (String word : words) {
			if (startsWith(index, word, str) && solve(index + word.length(), dp, str, words) == 1) {
				ans = 1;
				break;
			}
		}
		dp[index] = ans;
		return ans;
	}

	public static void main(String[] args) {
		WordBreak wordBreak = new WordBreak();
		String str = "abbabaaaabbababaaaabababaaababaabaaabbaaaabbabaabbaababababaaababbbbbbabbabbaabbaaababbaabababaabbbbaabbbaabbaabbbbbababbabbbbbaaabbabbabbabaabbbbababaaaaaaaabbbabbaaabbbababbbabbabbaabaaaababbaababbbbabaaabbbbbabbababbbbaaababbbaabbbbabaabaaababbaababaabbbaabbbaaaabaaaaaaabbabbaabbaabbaaaabaabaaaabaabbabbababbabaaaaababbbabaaabaaaaabbabaabbabbbbaaaabbbabaabbaabbbbbaaabababbaaabbbaaabaaaaaaabbaaaabababaaabbaaaaaabbaabaabbaabbbbabbaababbbbbbabbaabaabaaabaababbbabaaaabaabbbbabbbabaaabbaaaaabbaabaaaabbababaaaabababbbbaabbaabaaaaabbbbbaababbbaaaaabbaaaaaaaaaaaaaaaababaaaaabbaaabaabaaabbabaababaabbabbabbabbabaabbbaaababaaaaabbababaaababbbbaabaababbbabbaabababbaababaabbbbaaabbaaabbabbaabbababbbbbabaaaaaabaaaaababbaabbbaaaaabbbabaabaaabbabbbbababbababbbabbbbaaaaaabaabbaabababaabbbabaaaaaba";
		String[] words = { "a", "babbbba", "abbbba", "abb", "bbabbab", "b", "abbb", "aab", "abaaaaaa" };
		System.out.println(wordBreak.wordBreak(str, words));
	}

}
