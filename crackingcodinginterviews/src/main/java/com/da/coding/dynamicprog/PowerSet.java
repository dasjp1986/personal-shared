package com.da.coding.dynamicprog;

import java.util.HashSet;
import java.util.Set;

public class PowerSet {

	static int iterations = 0;

	//Time Complexity- O(N^3) far more than N*2^N
	public static Set<Set<Integer>> powerSets(Set<Integer> input) {
		Set<Set<Integer>> sets = new HashSet<>();
		for (Integer num : input) {
			Set<Integer> set = new HashSet<>();
			set.add(num);
			sets.add(set);
		}
		for (int i = 1; i < input.size(); i++) {
			for (Integer num : input) {
				Set<Set<Integer>> tempSets = new HashSet<>();
				for (Set<Integer> set : sets) {
					Set<Integer> newSet = new HashSet<>(set);
					newSet.add(num);
					tempSets.add(newSet);
					iterations++;
				}
				sets.addAll(tempSets);
			}
		}

		return sets;
	}
	
	//Time Complexity- O(2^N)
	public static void findPowerSets(int[] input) {
		Integer[] subsets = new Integer[input.length];
		generateSets(input, 0, subsets);
	}

	public static void generateSets(int[] input, int index, Integer[] subsets) {
		System.out.println("Running for index : "+index+", subsets : "+subsets);
		iterations++;
		if (index >= input.length) {
			print(subsets);
		} else {
			
			subsets[index] = null;
			generateSets(input, index + 1, subsets);
			subsets[index] = input[index];
			generateSets(input, index + 1, subsets);
		}

	}

	static void print(Integer[] set) {
		for (Integer num : set) {
			if (num != null)
				System.out.print(num + ",");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// Set<Integer> input = new HashSet<>();
		// input.add(1);
		// input.add(2);
		// input.add(3);
		// input.add(4);
		// input.add(5);
		// Set<Set<Integer>> powerSets = powerSets(input);
		// System.out.println(powerSets.size() + " actual iterations : " +
		// iterations + " expected iterations : "
		// + input.size() * Math.pow(2, input.size()));
		// System.out.println(powerSets);

		int[] input = { 1, 2, 3 };
		findPowerSets(input);
		System.out.println(" actual iterations : " + iterations + " expected iterations : "
				+ input.length * Math.pow(2, input.length));

	}
}
