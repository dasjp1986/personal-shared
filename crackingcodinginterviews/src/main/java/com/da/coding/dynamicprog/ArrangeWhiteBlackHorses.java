package com.da.coding.dynamicprog;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         You are given a sequence of black and white horses, and a set of K
 *         stables numbered 1 to K. You have to accommodate the horses into the
 *         stables in such a way that the following conditions are satisfied:
 * 
 *         You fill the horses into the stables preserving the relative order of
 *         horses. For instance, you cannot put horse 1 into stable 2 and horse
 *         2 into stable 1. You have to preserve the ordering of the horses. No
 *         stable should be empty and no horse should be left unaccommodated.
 *         Take the product (number of white horses * number of black horses)
 *         for each stable and take the sum of all these products. This value
 *         should be the minimum among all possible accommodation arrangements
 * 
 *         Example:
 * 
 * 
 *         Input: {WWWB} , K = 2 Output: 0
 * 
 *         Explanation: We have 3 choices {W, WWB}, {WW, WB}, {WWW, B} for first
 *         choice we will get 1*0 + 2*1 = 2. for second choice we will get 2*0 +
 *         1*1 = 1. for third choice we will get 3*0 + 0*1 = 0.
 * 
 *         Of the 3 choices, the third choice is the best option.
 * 
 *         If a solution is not possible, then return -1
 * 
 */

public class ArrangeWhiteBlackHorses {

	private Map<String, Map<Integer, Integer>> cache = new HashMap<>();

	public int arrange(String str, int k) {
		if (str.length() == k) {
			return 0;
		}
		if (k > str.length()) {
			return -1;
		}

		if (k == 1) {
			int whiteCount = countWhites(str);
			return whiteCount * (str.length() - whiteCount);
		}
		Integer min = Integer.MAX_VALUE;
		if (cache.containsKey(str) && cache.get(str).get(k) != null) {
			return cache.get(str).get(k);
		}

		for (int i = 1; i <= str.length() - k + 1; i++) {
			String prefix = str.substring(0, i);
			String suffix = str.substring(i);
			int whiteCount = countWhites(prefix);
			min = Math.min(min, whiteCount * (i - whiteCount) + arrange(suffix, k - 1));
		}

		Map<Integer, Integer> map = cache.get(str);
		if (map == null) {
			map = new HashMap<>();
			cache.put(str, map);
		}
		map.put(k, min);
		return min;
	}

	private int countWhites(String str) {
		if (str == null || str.isEmpty()) {
			return 0;
		}

		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == 'W') {
				count++;
			}
		}

		return count;
	}

}
