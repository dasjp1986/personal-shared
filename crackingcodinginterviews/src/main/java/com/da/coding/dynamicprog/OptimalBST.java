package com.da.coding.dynamicprog;

import com.da.coding.queues.QueueUsingSingleLinkedList;
import com.da.coding.trees.TNode;

/**
 * 
 * @author dagraw1
 * 
 *         For given input and their frequency for search, build an optimal
 *         binary search tree which can minimize the no. of comparisons
 *
 */
public class OptimalBST {
	TNode<Integer> root;

	public int build(int[] input, int[] freq) {

		int[][] cost = new int[input.length + 1][input.length + 1];
		int[][] roots = new int[input.length + 1][input.length + 1];

		for (int i = 1; i < roots.length; i++) {
			for (int j = i + 1; j < roots.length; j++) {
				roots[i][j] = i;
			}
		}

		for (int m = 0; m < roots.length; m++) {
			for (int i = 0; i < roots.length - m; i++) {
				int j = i + m;
				if (i == j) {
					continue;
				}
				int weight = 0;
				for (int l = i; l < j; l++) {
					weight = weight + freq[l];
				}
				int min = Integer.MAX_VALUE;
				int root = j;
				for (int k = i + 1; k <= j; k++) {
					if (cost[i][k - 1] + cost[k][j] < min) {
						min = cost[i][k - 1] + cost[k][j];
						root = k;
					}
				}
				cost[i][j] = min + weight;
				roots[i][j] = root;
			}
		}

		root = buildTree(roots, 0, input.length, input);

		return cost[0][input.length];
	}

	private TNode<Integer> buildTree(int[][] roots, int row, int col, int[] input) {
		if (row == col) {
			return null;
		}

		TNode<Integer> node = new TNode<Integer>(input[roots[row][col] - 1]);
		TNode<Integer> left = buildTree(roots, row, roots[row][col] - 1, input);

		if (left != null) {
			node.setLeft(left);
		}

		TNode<Integer> right = buildTree(roots, roots[row][col], col, input);
		if (right != null) {
			node.setRight(right);
		}

		return node;
	}

	public void inorder(TNode<Integer> node) {
		if (node != null) {
			inorder(node.getLeft());
			System.out.println(node.getValue() + "->>");
			inorder(node.getRight());
		}
	}
	
	public void bfs() {
		if (root == null) {
			throw new RuntimeException("Tree is empty");
		}
		System.out.println("Printing Tree in Breadth First Traversal...");
		QueueUsingSingleLinkedList<TNode<Integer>> queue = new QueueUsingSingleLinkedList<>();
		queue.add(root);

		while (!queue.isEmpty()) {
			TNode<Integer> node = queue.remove();
			System.out.print(" " + node.getValue());
			if (node.getLeft() != null)
				queue.add(node.getLeft());

			if (node.getRight() != null)
				queue.add(node.getRight());
		}

		System.out.println();
	}

	public static void main(String[] args) {
//		int[] input = { 1, 2, 3, 4 };
//		int[] freq = { 4, 2, 6, 3 };

		int[] input = { 1, 2, 3, 4, 5 };
		int[] freq = { 5, 4, 3, 2, 1 };

		OptimalBST bst = new OptimalBST();
		System.out.println(bst.build(input, freq));
		bst.inorder(bst.root);
		bst.bfs();
	}

}
