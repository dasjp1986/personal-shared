package com.da.coding.dynamicprog;

/**
 * 
 * @author dagraw1
 *
 *
 * 
 *         Given a 2 * N Grid of numbers, choose numbers such that the sum of
 *         the numbers is maximum and no two chosen numbers are adjacent
 *         horizontally, vertically or diagonally, and return it.
 * 
 *         Example:
 * 
 *         Grid: 1 2 3 4 2 3 4 5 so we will choose 3 and 5 so sum will be 3 + 5
 *         = 8
 * 
 * 
 *         Note that you can choose more than 2 numbers
 * 
 */
public class MaxSumWithoutAdjacent {

	public int adjacent(int[][] A) {

		int[] leftCache = new int[A[0].length];

		for (int i = 0; i < leftCache.length; i++) {
			int num = Math.max(A[0][i], A[1][i]);
			num = Math.max(i - 1 >= 0 ? leftCache[i - 1] : 0, num + (i - 2 >= 0 ? leftCache[i - 2] : 0));
			leftCache[i] = num;

		}

		return leftCache[leftCache.length - 1];
	}

}
