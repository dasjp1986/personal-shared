package com.da.coding.dynamicprog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         You are given n boxes with width, height and depth. write a program
 *         to stack them to achieve stack with tallest height you can only place
 *         smaller box on top of larger box in all the dimensions (width, height
 *         and depth)
 */
public class StackOfBoxes {

	// Time Complexity- O(2^N+ N*LogN) -> O(2^N)
	public static void main(String[] args) {
		List<Box> boxes = new ArrayList<>();
		boxes.add(new Box(2, 2, 2));
		boxes.add(new Box(3, 3, 2));
		boxes.add(new Box(3, 4, 3));
		boxes.add(new Box(3, 10, 1));
		boxes.add(new Box(1, 1, 1));

		System.out.println(findTallest(boxes));
	}

	public static int findTallest(List<Box> boxes) {
		Collections.sort(boxes);
		return build(boxes, null, 0, 0);
	}

	private static int build(List<Box> boxes, Box bottom, int index, int total) {
		if (index == boxes.size()) {
			return total;
		}

		int left = build(boxes, bottom, index + 1, total);
		int right = 0;
		if (boxes.get(index).canStack(bottom)) {
			right = build(boxes, boxes.get(index), index + 1, total + boxes.get(index).getHeight());
		}

		return Math.max(left, right);
	}

}

class Box implements Comparable<Box> {
	private int width;
	private int height;
	private int depth;
	private Integer area;

	public Box(int width, int height, int depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.area = width * height * depth;
	}

	public int getHeight() {
		return height;
	}

	public boolean canStack(Box bottom) {
		if (bottom == null)
			return true;
		return this.width <= bottom.width && this.height <= bottom.height && this.depth <= bottom.depth;
	}

	public int compareTo(Box box) {
		return box.area.compareTo(this.area);
	};

	@Override
	public String toString() {
		return "area : " + area;
	}

}
