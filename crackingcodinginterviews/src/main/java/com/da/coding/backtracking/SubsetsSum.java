package com.da.coding.backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         For given Set and target sum, find all the subsets whose sum is equal
 *         to the target sum
 * 
 *         Worst Time Complexity can be 2^N but using bonding functions, reduces
 *         it to polynomial time
 */
public class SubsetsSum {

	public List<List<Integer>> subsetSum(int[] set, int target) {
		List<List<Integer>> results = new ArrayList<>();

		int runningSum = 0;
		int remainingSum = 0;

		for (int i = 0; i < set.length; i++) {
			remainingSum += set[i];
		}

		subset(results, new ArrayList<>(), runningSum, remainingSum, 0, target, set);
		return results;
	}

	private void subset(List<List<Integer>> results, List<Integer> result, int runningSum, int remainingSum, int index,
			int target, int[] set) {

		if (target == runningSum) {
			results.add(result);
		}

		if (index >= set.length) {
			return;
		}

		if (set[index] + runningSum <= target && runningSum + remainingSum >= target) {
			List<Integer> include = new ArrayList<>(result);
			include.add(set[index]);
			// select current value at index
			subset(results, include, runningSum + set[index], remainingSum - set[index], index + 1, target, set);
			// ignore current value at index
			// List<Integer> exclude = new ArrayList<>(result);
			subset(results, result, runningSum, remainingSum - set[index], index + 1, target, set);
		}
	}

	public static void main(String[] args) {
//		int[] set = { 1, 2, 3, 4, 5, 6 };
//		int target= 7;

		int[] set = { 5, 10, 12, 13, 15, 18 };
		int target = 30;

		SubsetsSum subset = new SubsetsSum();
		List<List<Integer>> results = subset.subsetSum(set, target);
		results.forEach(res -> System.out.println(res));
	}

}
