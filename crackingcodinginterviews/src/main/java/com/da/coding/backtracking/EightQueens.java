package com.da.coding.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given a chess board of 8X8 and 8 queens, find all possible
 *         arrangements where all the 8 queens can be placed such that, not more
 *         than one queen can be on the same row, column or diagonal position
 */
public class EightQueens {
	public static List<Integer[]> placeQueens(int n) {
		List<Integer[]> results = new ArrayList<>();
		//using columns array is useful as add and remove in recursions can be avoided
		Integer[] columns = new Integer[n];
		arrange(n, columns, results, 0);
		return results;
	}

	private static void arrange(int n, Integer[] columns, List<Integer[]> results, int row) {
		if (row == n) {
			results.add(columns.clone());
		} else {
			for (int col = 0; col < n; col++) {
				if (checkValid(col, row, columns)) {
					// System.out.println(Arrays.toString(columns));
					columns[row] = col;
					arrange(n, columns, results, row + 1);
				}
			}
		}
	}

	private static boolean checkValid(int col2, int row2, Integer[] columns) {
		for (int row1 = 0; row1 < row2; row1++) {
			int col1 = columns[row1];

			if (col1 == col2) {
				return false;
			}

			if (Math.abs(col2 - col1) == row2 - row1) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		List<Integer[]> queens = placeQueens(8);

		for (Integer[] qs : queens) {
			System.out.println(Arrays.toString(qs));
		}

		System.out.println("Possibilities : " + queens.size());
	}
}
