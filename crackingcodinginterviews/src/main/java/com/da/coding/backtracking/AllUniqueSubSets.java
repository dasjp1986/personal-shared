package com.da.coding.backtracking;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * @author dagraw1
 *
 *         Given a set of distinct integers, S, return all possible subsets.
 * 
 *         Note: Elements in a subset must be in non-descending order. The
 *         solution set must not contain duplicate subsets. Also, the subsets
 *         should be sorted in ascending ( lexicographic ) order. The list is
 *         not necessarily sorted. Example :
 * 
 *         If S = [1,2,3], a solution is:
 * 
 *         [ [], [1], [1, 2], [1, 2, 3], [1, 3], [2], [2, 3], [3], ]
 * 
 */
public class AllUniqueSubSets {

	public ArrayList<ArrayList<Integer>> subsets(ArrayList<Integer> A) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> res = new ArrayList<>();
		results.add(res);
		Collections.sort(A);
		for (int i = 0; i < A.size(); i++) {
			res = new ArrayList<>();
			res.add(A.get(i));
			results.addAll(generate(res, i + 1, A));
		}

		return results;
	}

	private ArrayList<ArrayList<Integer>> generate(ArrayList<Integer> res, int index, ArrayList<Integer> A) {

		ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();
		if (index == A.size()) {
			results.add(res);
			return results;
		}
		results.add(res);
		for (int i = index; i < A.size(); i++) {
			ArrayList<Integer> clone = (ArrayList<Integer>) res.clone();
			clone.add(A.get(i));
			results.addAll(generate(clone, i + 1, A));
		}

		return results;

	}

	public static void main(String[] args) {
		AllUniqueSubSets subSets = new AllUniqueSubSets();
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		System.out.println(subSets.subsets(list));
	}

}
