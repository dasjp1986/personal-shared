package com.da.coding.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 * 
 *         Color a graph with given set of colors in such a way that no two
 *         adjacent vertices have same color
 * 
 *         colors={"R","G","B"}
 * 
 */
public class GraphColoring {
	public List<List<String>> fillColors(int[][] adjMatrix, List<String> colors) {
		List<List<String>> results = new ArrayList<>();

		boolean[] visited = new boolean[adjMatrix.length];

		dfs(results, new ArrayList<>(), 0, visited, adjMatrix, colors);
		return results;
	}

	private void dfs(List<List<String>> results, List<String> result, int vertex, boolean[] visited, int[][] adjMatrix,
			List<String> colors) {

		// System.out.println("result: " + result + "vertex : " + vertex);

		if (visited[vertex] || colors.isEmpty()) {
			return;
		}

		boolean[] visitedClone = visited.clone();
		visitedClone[vertex] = true;
		List<String> colorsToUse = new ArrayList<>(colors);
		//remove the color used in backedge
		for (int i = 0; i < vertex; i++) {
			if (adjMatrix[i][vertex] == 1 && i < result.size()) {
				colorsToUse.remove(result.get(i));
			}
		}

		//traverse for connected edges
		for (int i = vertex + 1; i < adjMatrix.length; i++) {
			if (adjMatrix[vertex][i] != 0 && !visited[i]) {
				for (String col : colorsToUse) {
					List<String> resClone = new ArrayList<>(result);
					resClone.add(col);
					dfs(results, resClone, i, visitedClone, adjMatrix, colors);
				}

			}
		}
		
		// for the last vertex, add remaining color and if all other vertices already colored add this combination to results
		if (!colorsToUse.isEmpty() && vertex == adjMatrix.length - 1 && result.size() == adjMatrix.length - 1) {
			result.add(colorsToUse.get(0));
			results.add(result);
		}
	}

	public static void main(String[] args) {
		GraphColoring graphColoring = new GraphColoring();
		List<String> colors = Arrays.asList("R", "G");
		int[][] adjMatrix = { { 0, 1, 0, 0, 1 }, { 1, 0, 1, 0, 1 }, { 0, 1, 0, 1, 1 }, { 0, 0, 1, 0, 1 },
				{ 1, 1, 1, 1, 0 } };

		List<List<String>> list = graphColoring.fillColors(adjMatrix, colors);
		for (List<String> res : list) {
			System.out.println(res);
		}
	}
}
