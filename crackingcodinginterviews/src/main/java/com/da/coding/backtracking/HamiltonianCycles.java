package com.da.coding.backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         In a given connected- directed or undirected graph, find all possible
 *         hamiltonian cycles.
 * 
 *         hamiltonian cycle is a tour which visits all the vertices and come
 *         back to the origin node
 */

public class HamiltonianCycles {
	public List<List<Integer>> cycles(int[][] adjMatrix) {
		List<List<Integer>> results = new ArrayList<>();
		boolean[] visited = new boolean[adjMatrix.length];
		List<Integer> result = new ArrayList<>();
		result.add(0);
		dfs(results, result, visited, 0, adjMatrix);
		return results;
	}

	private void dfs(List<List<Integer>> results, List<Integer> result, boolean[] visited, int vertex,
			int[][] adjMatrix) {

		if (visited[vertex]) {
			return;
		}

		visited[vertex] = true;

		for (int i = 0; i < adjMatrix.length; i++) {
			if (adjMatrix[vertex][i] == 1 && !visited[i]) {
				result.add(i);
				dfs(results, result, visited, i, adjMatrix);
				// ensures removing object by value instead of index position
				result.remove(Integer.valueOf(i));
				visited[i] = false;
			}
		}

		if (result.size() == adjMatrix.length && adjMatrix[result.get(result.size() - 1)][0] == 1) {
			// cloning of results is much needed
			results.add(new ArrayList<>(result));
		}

	}

	public static void main(String[] args) {
		// int[][] adjMatrix = { { 0, 1, 1, 1 }, { 1, 0, 1, 1 }, { 1, 1, 0, 1 }, { 1, 1,
		// 1, 0 } };

		int[][] adjMatrix = { { 0, 1, 1, 0, 0, 1 }, { 1, 0, 2, 0, 1, 1 }, { 1, 1, 0, 1, 0, 0 }, { 0, 0, 1, 0, 1, 0 },
				{ 0, 1, 0, 1, 0, 1 }, { 1, 1, 0, 0, 1, 0 } };

		HamiltonianCycles cycles = new HamiltonianCycles();
		List<List<Integer>> results = cycles.cycles(adjMatrix);

		for (List<Integer> res : results) {
			System.out.println(res);
		}

	}
}
