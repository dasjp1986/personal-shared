package com.da.coding.threads;

public class Service implements Runnable {

	private static final ThreadLocal<String> thLocal = new ThreadLocal<>();
	private UserService userService;
	private String userId;

	public Service(UserService userService, String userId) {
		this.userService = userService;
		this.userId = userId;
	}

	@Override
	public void run() {
		String userName = userService.getUserName(userId);
		thLocal.set(userName);
		System.out.println(thLocal.get());
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.println(thLocal.get());
		super.finalize();
	}

	public static void main(String[] args) throws InterruptedException {
		UserService userService= new UserService();
		Thread th1= new Thread(new Service(userService, "Dharmendra"));
		Thread th2= new Thread(new Service(userService, "Agrawal"));
		
		th1.start();
		th2.start();
		
		Thread.sleep(10000);
	}

}
