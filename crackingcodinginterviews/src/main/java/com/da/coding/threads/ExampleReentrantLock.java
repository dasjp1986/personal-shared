package com.da.coding.threads;

import java.util.concurrent.locks.ReentrantLock;

public class ExampleReentrantLock implements Runnable {

	private ReentrantLock lock;

	public ExampleReentrantLock(ReentrantLock lock) {
		this.lock = lock;
	}

	@Override
	public void run() {

		try {
			boolean isLock = lock.tryLock();
			System.out.println(isLock+" : "+lock.getHoldCount()+" : "+lock.isHeldByCurrentThread());
			Thread.sleep(1000);
			lock.lock();
			System.out.println(lock.getHoldCount());
			System.out.println("queue length : "+lock.getQueueLength());
			lock.unlock();
			System.out.println(lock.getHoldCount());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			lock.unlock();
			System.out.println(lock.getHoldCount());
		}
	}
	
	public static void main(String[] args) {
		new Thread(new ExampleReentrantLock(new ReentrantLock())).start();
	}

}
