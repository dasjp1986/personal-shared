package com.da.coding.puzzles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * 
 * @author dagraw1
 *
 *         You are given 100 soda bottles out of which exactly one bottle is
 *         poisoned You can test if soda is poisoned by dropping one or multiple
 *         drops onto the given 10 tester strips. But results of the strips will
 *         be available only after 7 days. you can reuse the strip if it was
 *         tested negative else strip is no longer useful.
 * 
 *         Your task is to find the bottle with poisoned soda in minimum number
 *         of days.
 * 
 *         Solution can be extended to n bottles with one of them poisoned by
 *         using k number of strips such that n<2^k
 * 
 *         Bottle are numbered from 0,1,2...n-1
 */
public class PoisonedBottles {
	public static int findDays(int totalBottles, int strips, int oneTestDays, int spoiledNbr) {
		int days = oneTestDays;

		// idea is to map all the bottles to strips using 1s position in binary
		// representation

		Map<Integer, Set<Integer>> stripToBottleMap = getStripsBottleMap(totalBottles, strips);
		List<Integer> positiveStrips = getPositiveTestedStrips(stripToBottleMap, spoiledNbr);
		int poisonedBottleNbr = getPoisonedBottleNbr(positiveStrips);
		System.out.println("Found Poisoned Bottle Nbr : " + poisonedBottleNbr);
		return days;
	}

	private static int getPoisonedBottleNbr(List<Integer> positiveStrips) {
		int bottleNbr = 0;
		for (int strip : positiveStrips) {
			bottleNbr = (int) (bottleNbr + Math.pow(2, strip));
		}
		return bottleNbr;
	}

	private static List<Integer> getPositiveTestedStrips(Map<Integer, Set<Integer>> stripToBottleMap, int spoiledNbr) {
		List<Integer> positiveStrips = new ArrayList<>();
		for (Integer strip : stripToBottleMap.keySet()) {
			Set<Integer> bottles = stripToBottleMap.get(strip);
			if (bottles != null) {
				for (Integer bottle : bottles) {
					if (bottle.equals(spoiledNbr)) {
						positiveStrips.add(strip);
					}
				}
			}
		}
		return positiveStrips;
	}

	private static Map<Integer, Set<Integer>> getStripsBottleMap(int totalBottles, int strips) {
		if (totalBottles > Math.pow(2, strips)) {
			throw new RuntimeException("Insufficient strips...");
		}
		Map<Integer, Set<Integer>> stripToBottleMap = new TreeMap<>();
		for (int bottle = 1; bottle < totalBottles; bottle++) {
			int pos = 0;
			int temp = bottle;
			while (temp != 0) {
				if ((temp & 1) == 1) {
					Set<Integer> set = stripToBottleMap.get(pos);
					if (set == null) {
						set = new HashSet<>();
						stripToBottleMap.put(pos, set);
					}
					set.add(bottle);
				}
				temp = temp >>> 1;
				pos++;
			}
		}
		return stripToBottleMap;
	}

	public static void main(String[] args) {
		int totalBottles = 1000;
		int strips = 10;
		int spoiledNbr = 999;
		System.out.println("Expected Poisoned Bottle Nbr : " + spoiledNbr);
		System.out.println("Days taken to reveal results : " + findDays(totalBottles, strips, 7, spoiledNbr));
	}
}
