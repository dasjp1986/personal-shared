package com.da.coding.puzzles;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *Suppose you have N eggs and you want to determine from which floor in a K-floor building you can drop an egg such that it doesn't break. You have to determine the minimum number of attempts you need in order find the critical floor in the worst case while using the best strategy.There are few rules given below. 

    An egg that survives a fall can be used again.
    A broken egg must be discarded.
    The effect of a fall is the same for all eggs.
    If the egg doesn't break at a certain floor, it will not break at any floor below.
    If the eggs breaks at a certain floor, it will break at any floor above.

For more description on this problem see wiki page

Input:
The first line of input is  T denoting the number of testcases.Then each of the T lines contains two positive integer N and K where 'N' is the number of eggs and 'K' is number of floor in building.

Output:
For each test case, print a single line containing one integer the minimum number of attempt you need in order find the critical floor.

Constraints:
1<=T<=30
1<=N<=10
1<=K<=50


Example:
Input:
1
2 10

Output:
4
 */
public class NEggDropPuzzle {

	//Time Complexity- O(N*K^2)
	//Time Complexity- O(N*K)
	public static void main (String[] args)
	 {
	  Scanner scn= new Scanner(System.in);
	  int ts= scn.nextInt();
	  for(int t=0;t<ts;t++){
	      int n= scn.nextInt();
	      int k= scn.nextInt();
	      int[][] cache= new int[n+1][k+1];
	      System.out.println(drops(n,k,cache));
	  }
	  scn.close();
	 }
	 
	 public static int drops(int n, int k,int[][] cache){
	    if(n==1){
	        return k;
	    }
	    
	    if(n<=0 || k<=0){
	        return 0;
	    }
	    
	    if(cache[n][k]!=0){
	        return cache[n][k];
	    }
	    
	    int res= Integer.MAX_VALUE;
	    for(int x=1;x<=k;x++){
	        res= Math.min(res, Math.max(drops(n-1,x-1,cache),
	        drops(n,k-x,cache)));
	    }
	    
	    cache[n][k]= res+1;
	    return cache[n][k];
	 }


}
