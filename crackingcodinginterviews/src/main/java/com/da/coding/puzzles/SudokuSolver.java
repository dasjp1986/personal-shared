package com.da.coding.puzzles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SudokuSolver {
	private int[][] sudoku;
	private Map<Nonet, List<Cell>> nonets;
	private List<ArrayList<Integer>> leftInRows;
	private List<ArrayList<Integer>> leftInColms;
	public static int MATRIX_SIZE = 9;

	public static void main(String[] args) {
		// int[][] matrix = { { 0, 0, 2, 9, 8, 0, 5, 0, 0 }, { 4, 0, 0, 0, 7, 0,
		// 0, 1, 3 }, { 0, 3, 9, 6, 0, 4, 0, 7, 0 },
		// { 2, 0, 0, 0, 5, 6, 4, 0, 0 }, { 8, 4, 0, 3, 0, 0, 2, 0, 1 }, { 9, 0,
		// 7, 0, 0, 1, 0, 8, 6 },
		// { 6, 0, 0, 7, 0, 5, 1, 3, 0 }, { 0, 9, 1, 4, 0, 0, 0, 0, 5 }, { 0, 2,
		// 0, 0, 3, 0, 6, 0, 8 } };

		int[][] matrix = { { 0, 4, 0, 0, 0, 8, 9, 1, 0 }, { 0, 0, 3, 0, 4, 0, 0, 0, 8 }, { 7, 6, 0, 9, 0, 3, 0, 4, 2 },
				{ 2, 5, 0, 0, 3, 0, 0, 0, 1 }, { 0, 0, 9, 0, 5, 0, 8, 0, 0 }, { 8, 0, 0, 0, 9, 0, 0, 5, 7 },
				{ 1, 2, 0, 6, 0, 4, 0, 7, 9 }, { 4, 0, 0, 0, 2, 0, 1, 0, 0 }, { 0, 8, 6, 1, 0, 0, 0, 2, 0 } };

		SudokuSolver solver = new SudokuSolver(matrix);
		System.out.println(solver.solver());
		System.out.println("Sudoku Solution : ");
		for (int i = 0; i < matrix.length; i++) {
			System.out.println(Arrays.toString(matrix[i]));
		}
	}

	public SudokuSolver(int[][] matrix) {
		sudoku = matrix;
		nonets = new LinkedHashMap<>();
		leftInRows = new ArrayList<>();
		leftInColms = new ArrayList<>();

		for (int row = 0; row < matrix.length; row++) {
			ArrayList<Integer> rows = new ArrayList<>();
			ArrayList<Integer> colms = new ArrayList<>();
			for (int i = 1; i <= matrix.length; i++) {
				colms.add(i);
				rows.add(i);
			}
			leftInColms.add(colms);
			leftInRows.add(rows);
		}

		for (int row = 0; row < matrix.length; row += 3) {
			for (int col = 0; col < matrix.length; col += 3) {
				Nonet nonet = new Nonet(row, col);
				updateNonetCells(nonet);
			}
		}
	}

	public boolean solver() {
		// sudoku is solved completely
		if (nonets.isEmpty()) {
			return true;
		}

		List<Nonet> nonetsToDel = new ArrayList<>();
		for (Entry<Nonet, List<Cell>> entry : nonets.entrySet()) {
			if (solveNonet(entry.getKey(), entry.getValue())) {
				nonetsToDel.add(entry.getKey());
			}
		}
		cleanFilledNonets(nonetsToDel);
		return solver();
	}

	private void cleanFilledNonets(List<Nonet> nonetsToDel) {
		for (Nonet nonet : nonetsToDel) {
			nonets.remove(nonet);
		}
	}

	private boolean solveNonet(Nonet nonet, List<Cell> emptyCells) {
		// System.out.println("solving nonet : " + nonet);
		List<Cell> filledCells = new ArrayList<>();
		for (Cell cell : emptyCells) {
			ArrayList<Integer> interSectedValues = intersect(leftInRows.get(cell.getRow()),
					leftInColms.get(cell.getCol()), nonet.getPossibleValues());
			if (interSectedValues.isEmpty()) {
				System.out.println("could not find any value for rows: " + leftInRows.get(cell.getRow()) + " columns: "
						+ leftInColms.get(cell.getCol()) + " nonet : " + nonet.getPossibleValues());
				throw new RuntimeException("Sudoku can't be solved!");
			} else if (interSectedValues.size() == 1) {
				Integer value = interSectedValues.get(0);
				sudoku[cell.getRow()][cell.getCol()] = value;
				leftInRows.get(cell.getRow()).remove(value);
				leftInColms.get(cell.getCol()).remove(value);
				filledCells.add(cell);
				nonet.getPossibleValues().remove(value);
				nonet.setPossibleSum(nonet.getPossibleSum() - value);
			}
		}
		nonets.get(nonet).removeAll(filledCells);
		return nonet.getPossibleSum() == 0;
	}

	private void printSudoku() {
		for (int i = 0; i < sudoku.length; i++) {
			System.out.println(Arrays.toString(sudoku[i]));
		}
	}

	private ArrayList<Integer> intersect(ArrayList<Integer> possibleInRows, ArrayList<Integer> possibleInColms,
			ArrayList<Integer> possibleValues) {

		ArrayList<Integer> rowsClone = (ArrayList<Integer>) possibleInRows.clone();
		ArrayList<Integer> colmsClone = (ArrayList<Integer>) possibleInColms.clone();
		rowsClone.retainAll(colmsClone);
		ArrayList<Integer> possibleClone = (ArrayList<Integer>) possibleValues.clone();
		rowsClone.retainAll(possibleClone);
		return rowsClone;
	}

	private void updateNonetCells(Nonet nonet) {
		List<Cell> emptyCells = new ArrayList<>();
		for (int row = nonet.getStartRow(); row < nonet.getStartRow() + 3; row++) {
			for (int col = nonet.getStartCol(); col < nonet.getStartCol() + 3; col++) {
				Integer cellValue = sudoku[row][col];
				if (cellValue == 0) {
					Cell cell = new Cell(row, col);
					emptyCells.add(cell);
				} else {
					ArrayList<Integer> possibleValues = nonet.getPossibleValues();
					boolean isRemoved = possibleValues.remove(cellValue);
					if (isRemoved) {
						nonet.setPossibleSum(nonet.getPossibleSum() - cellValue);
						leftInRows.get(row).remove(cellValue);
						leftInColms.get(col).remove(cellValue);
					}
				}
			}
		}
		nonets.put(nonet, emptyCells);
	}

}

class Nonet {
	private int startRow;
	private int startCol;
	private ArrayList<Integer> possibleValues;
	private int possibleSum;

	public Nonet(int row, int col) {
		super();
		this.startRow = row;
		this.startCol = col;
		this.possibleValues = new ArrayList<>();
		for (int i = 1; i <= SudokuSolver.MATRIX_SIZE; i++) {
			possibleValues.add(i);
		}
		this.possibleSum = 45;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getStartCol() {
		return startCol;
	}

	public void setStartCol(int startCol) {
		this.startCol = startCol;
	}

	public ArrayList<Integer> getPossibleValues() {
		return possibleValues;
	}

	public void setPossibleValues(ArrayList<Integer> possibleValues) {
		this.possibleValues = possibleValues;
	}

	public int getPossibleSum() {
		return possibleSum;
	}

	public void setPossibleSum(int possibleSum) {
		this.possibleSum = possibleSum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + startCol;
		result = prime * result + startRow;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nonet other = (Nonet) obj;
		if (startCol != other.startCol)
			return false;
		if (startRow != other.startRow)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Nonet [startRow=" + startRow + ", startCol=" + startCol + "]";
	}

}

class Cell {
	private int row;
	private int col;

	public Cell(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

}
