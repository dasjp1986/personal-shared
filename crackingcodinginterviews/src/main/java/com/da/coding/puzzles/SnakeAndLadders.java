package com.da.coding.puzzles;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author dagraw1
 *
 *
 *         Markov takes out his Snakes and Ladders game, stares at the board and
 *         wonders: "If I can always roll the die to whatever number I want,
 *         what would be the least number of rolls to reach the destination?"
 * 
 *         Rules The game is played with a cubic die of faces numbered to .
 * 
 *         Starting from square , land on square with the exact roll of the die.
 *         If moving the number rolled would place the player beyond square , no
 *         move is made.
 * 
 *         If a player lands at the base of a ladder, the player must climb the
 *         ladder. Ladders go up only.
 * 
 *         If a player lands at the mouth of a snake, the player must go down
 *         the snake and come out through the tail. Snakes go down only.
 * 
 *         Input Format
 * 
 *         The first line contains the number of tests, .
 * 
 *         For each testcase: - The first line contains , the number of ladders.
 *         - Each of the next lines contains two space-separated integers, the
 *         start and end of a ladder. - The next line contains the integer , the
 *         number of snakes. - Each of the next lines contains two
 *         space-separated integers, the start and end of a snake.
 * 
 *         Constraints
 * 
 * 
 * 
 *         The board is always with squares numbered to . Neither square nor
 *         square will be the starting point of a ladder or snake. A square will
 *         have at most one endpoint from either a snake or a ladder.
 * 
 *         Output Format
 * 
 *         For each of the t test cases, print the least number of rolls to move
 *         from start to finish on a separate line. If there is no solution,
 *         print -1.
 * 
 *         Sample Input
 * 
 *         2 3 32 62 42 68 12 98 7 95 13 97 25 93 37 79 27 75 19 49 47 67 17 4 8
 *         52 6 80 26 42 2 72 9 51 19 39 11 37 29 81 3 59 5 79 23 53 7 43 33 77
 *         21 Sample Output
 * 
 *         3 5 Explanation
 * 
 *         For the first test:
 * 
 *         The player can roll a and a to land at square . There is a ladder to
 *         square . A roll of ends the traverse in rolls.
 * 
 *         For the second test:
 * 
 *         The player first rolls and climbs the ladder to square . Three rolls
 *         of get to square . A final roll of lands on the target square in
 *         total rolls.
 */
public class SnakeAndLadders {
	static Map<Integer, Integer> ladders;
	static Map<Integer, Integer> snakes;
	static int target = 100;
	static int DICE_MAX = 6;

	public static int quickestWayUp(Integer[][] ladrs, Integer[][] snkes) {
		ladders = createMap(ladrs);
		snakes = createMap(snkes);
		Integer[] cache = new Integer[100];
		int minSteps = walk(0, new HashMap<Integer, Boolean>(), cache);
		return minSteps;
	}

	private static Map<Integer, Integer> createMap(Integer[][] array) {
		Map<Integer, Integer> map = new TreeMap<>();
		for (int i = 0; i < array.length; i++) {
			if (array[i].length == 2)
				map.put(array[i][0], array[i][1]);
		}
		return map;
	}

	private static int walk(int pos, HashMap<Integer, Boolean> visited, Integer[] cache) {
		if (pos >= target) {
			System.out.println("reached target : " + pos);
			return 0;
		}

		if (cache[pos] != null) {
			return cache[pos];
		}
		System.out.println("calculate for pos: " + pos);
		int min = Integer.MAX_VALUE;
		for (int dice = 0; dice <= DICE_MAX; dice++) {
			int newPos = dice + pos;

			Integer move = ladders.get(newPos);
			if (move == null) {
				move = snakes.get(newPos);
				if (move == null) {
					continue;
				}
				System.out.println("Snake = start:" + (newPos) + " - end: " + move);
			} else {
				System.out.println("Ladder = start:" + (newPos) + " - end: " + move);
			}
			if (visited.get(move) != null) {
				continue;
			}
			visited.put(move, true);
			min = Math.min(min, (dice == 0 ? 0 : 1) + walk(move, (HashMap<Integer, Boolean>) visited.clone(), cache));
		}
		// only with dice
		visited.put(pos + 6, true);
		min = Math.min(min, 1 + walk(pos + 6, (HashMap<Integer, Boolean>) visited.clone(), cache));
		cache[pos] = min;
		return min;
	}

	public static void main(String[] args) {
		// Integer[][] ladders = { { 32, 62 }, { 42, 68 }, { 12, 98 } };
		// Integer[][] snakes = { { 95, 13 }, { 97, 25 }, { 93, 37 }, { 79, 27
		// }, { 75, 19 }, { 49, 47 }, { 67, 17 } };

		// Integer[][] ladders = { { 8, 52 }, { 6, 80 }, { 26, 42 }, { 2, 72 }
		// };
		// Integer[][] snakes = { { 51, 19 }, { 39, 11 }, { 37, 29 }, { 81, 3 },
		// { 59, 5 }, { 79, 23 }, { 53, 7 },
		// { 43, 33 }, { 77, 21 } };

		Integer[][] ladders = { { 3, 90 } };
		Integer[][] snakes = { { 99, 10 }, { 97, 20 }, { 98, 30 }, { 96, 40 }, { 95, 50 }, { 94, 60 }, { 93, 70 } };

		System.out.println(quickestWayUp(ladders, snakes));
	}

}
