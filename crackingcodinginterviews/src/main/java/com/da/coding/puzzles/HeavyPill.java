package com.da.coding.puzzles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author dagraw1
 * 
 *         Given n bottles of same size[1,2,3...n] filled with pills of 1gm
 *         except one of bottle filled with 1.1gm pills you are given a scale
 *         which can measure exact weight. find out the bottle with heavy pills
 *         by using scale only once.
 * 
 *         Assumption: each bottle with have n number of pills 2<=n<100
 */
public class HeavyPill {
	public static int find(int n) {
		int bottle = 0;
		List<Double> pills = new ArrayList<>();
		for (int i = 0; i < n - 1; i++) {
			pills.add(1.0);
		}
		pills.add(1.1);
		Collections.shuffle(pills);

		System.out.println(pills);
		double expected = 0;
		double actual = 0;

		for (int i = 0; i < pills.size(); i++) {
			actual = actual + (i + 1) * pills.get(i);
			expected = expected + (i + 1) * 1.0;
		}

		bottle = (int) ((actual - expected) / 0.1);
		return bottle;
	}
	
	public static void main(String[] args) {
		System.out.println("bottle # : "+find(10));
	}
}
