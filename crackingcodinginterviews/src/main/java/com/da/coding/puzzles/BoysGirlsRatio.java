package com.da.coding.puzzles;

import java.util.Random;

/**
 * 
 * @author dagraw1
 *
 *         Post Apocalyptic world, a new policy of having one girl child is
 *         created. After the girl child birth, pregnancy must be stopped. There
 *         can be zero or any number of boys delivered before girl is delivered.
 * 
 *         Calculate ratio of boys after policy is created
 * 
 *         Ex- G Ex- BG Ex- BBBBG
 * 
 */
public class BoysGirlsRatio {
	public static void main(String[] args) {
		System.out.printf("%.2f",ratioAcrossFamilies(100000));
	}

	private static double ratioAcrossFamilies(int family) {
		int girls = 0;
		int boys = 0;

		for (int i = 0; i < family; i++) {
			int[] childCounts = runFamily();
			girls = girls + childCounts[0];
			boys = boys + childCounts[1];
		}

		return (boys * 1.0) / (girls + boys);
	}

	private static int[] runFamily() {
		int[] childCounts = new int[2];
		Random random = new Random();

		int girls = 0;
		int boys = 0;
		while (girls == 0) {
			if (random.nextBoolean()) {
				girls++;
			} else {
				boys++;
			}
		}
		childCounts[0] = girls;
		childCounts[1] = boys;
		return childCounts;
	}
}
