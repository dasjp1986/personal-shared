package com.da.coding.puzzles;

/**
 * 
 * @author dagraw1
 * 
 *         you are given n(100) locks and all of them were close initially a
 *         person runs through n iterations (i=1,2,...n) and toggle every lock
 *         at the ith position
 * 
 *         after he completes all the iterations of toggles, return how many
 *         locks are open.
 *         
 *         ex- when i=1 all the locks are open
 *         when i=2 every lock divisible by 2 is toggled
 *         when i=3 every lock divisible by 3 is toggled and so on...
 *
 */
public class LockToggles {
	//only perfect squares will have odd number of unique factors
	//16- 1,2,4,8,16= (1,16),(2,8),(4)
	//each pairs toggles themselves out
	//(4,4) will be left over which in fact has perfect square less than 100
	public static int openLocks(int maxLocks){
		int count=0;
		for (int x = 1; x*x <=maxLocks; x++) {
			count++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		System.out.println(openLocks(100));
	}
}
