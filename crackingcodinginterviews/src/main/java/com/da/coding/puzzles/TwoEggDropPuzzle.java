package com.da.coding.puzzles;

/**
 * 
 * @author dagraw1
 *
 *         Given 100 floors and 2 eggs find the floor with minimum attempts at
 *         which egg would break.
 */
public class TwoEggDropPuzzle {
	public static int findAttempts(int totalFloors, int breakFloor) {
		int attempts = 0;
		int increment = getIncrement(totalFloors);
		// System.out.println("value of x : "+increment);
		int firstBreakFloor = 0;
		// use first egg
		for (int i = increment; i <= breakFloor; i = i + increment - attempts) {
			attempts++;
			firstBreakFloor = i;
		}
		if (attempts < 1) {
			attempts = 1;
		}
		// use second egg
		for (int j = firstBreakFloor + 1; j <= breakFloor; j++) {
			attempts++;
		}
		return attempts;
	}

	private static int getIncrement(int totalFloors) {
		int increment = 1;
		while (increment * (increment + 1) <= 2 * totalFloors) {
			increment++;
		}
		return increment;
	}

	public static void main(String[] args) {
		int worst = 0;
		for (int i = 1; i <= 100; i++) {
			int attempts = findAttempts(100, i);
			if (attempts > worst) {
				worst = attempts;

			}
		}
		System.out.println("worst Attempts : " + worst);
	}
}
