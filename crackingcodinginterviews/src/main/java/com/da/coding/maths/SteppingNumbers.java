package com.da.coding.maths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author dagraw1
 *
 *         Given N and M find all stepping numbers in range N to M
 * 
 *         The stepping number:
 * 
 *         A number is called as a stepping number if the adjacent digits have a
 *         difference of 1. e.g 123 is stepping number, but 358 is not a
 *         stepping number
 * 
 *         Example:
 * 
 *         N = 10, M = 20 all stepping numbers are 10 , 12 Return the numbers in
 *         sorted order.
 */

public class SteppingNumbers {

	public int[] stepnum(int s, int e) {
		Set<Integer> res = new TreeSet<>();

		if (e <= 10) {
			for (int i = s; i <= e; i++) {
				res.add(i);
			}
		} else {
			List<String> list = new ArrayList<>();
			for (int i = 0; i < 10; i++) {
				list.add(i + "");
				if (s <= i && i <= e) {
					res.add(i);
				}
			}
			int lenEnd = (e + "").length();

			for (int l = 2; l <= lenEnd; l++) {
				List<String> lst = new ArrayList<>();
				for (int i = 0; i < 10; i++) {
					for (String temp : list) {
						int d = temp.charAt(0) - 48;
						if (d - 1 == i || d + 1 == i) {
							String str = new String(i + temp);
							Integer num = Integer.parseInt(str);
							lst.add(str);
							if (num < s) {
								continue;
							}

							if (num > e) {
								return convertToArray(res);
							}
							res.add(num);
						}

						if (d > i + 1) {
							break;
						}
					}
				}
				list = lst;
			}

		}

		return convertToArray(res);
	}

	int[] convertToArray(Set<Integer> res) {
		int[] array = new int[res.size()];
		int i = 0;
		for (Integer num : res) {
			array[i++] = num;
		}

		return array;
	}

	public static void main(String[] args) {
		SteppingNumbers step = new SteppingNumbers();
		System.out.println(Arrays.toString(step.stepnum(1000, 2000)));
	}

}
