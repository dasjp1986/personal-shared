package com.da.coding.maths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         For Given Number N find if its COLORFUL number or not
 * 
 *         Return 0/1
 * 
 *         COLORFUL number:
 * 
 *         A number can be broken into different contiguous sub-subsequence
 *         parts. Suppose, a number 3245 can be broken into parts like 3 2 4 5
 *         32 24 45 324 245. And this number is a COLORFUL number, since product
 *         of every digit of a contiguous subsequence is different Example:
 * 
 *         N = 23 2 3 23 2 -> 2 3 -> 3 23 -> 6 this number is a COLORFUL number
 *         since product of every digit of a sub-sequence are different.
 * 
 *         Output : 1
 */
public class ColorfulDigits {

	public static int colorful(int n) {
		List<Integer> digits = new ArrayList<>();
		while (n > 0) {
			Integer d = n % 10;
			n = n / 10;
			digits.add(d);
		}

		Map<Integer, Boolean> map = new HashMap<>();
		for (int size = 1; size <= digits.size(); size++) {
			for (int i = 0; i <= digits.size() - size; i++) {
				Integer sum = 1;
				for (int j = i; j < i + size; j++) {
					sum *= digits.get(j);
				}
				if (map.get(sum) == null) {
					map.put(sum, true);
				} else {
					return 0;
				}
			}
		}

		return 1;
	}

	public static void main(String[] args) {
		System.out.println(colorful(23));
	}

}
