package com.da.coding.maths;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JumpingNumbers {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			jumping(n);
		}
		scn.close();
	}

	public static void jumping(int n) {
		List<Integer> res = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			System.out.print(i + " ");
			res.add(i);
			if (i == n) {
				return;
			}
		}
		int digits = Integer.toString(n).length();
		int power = 10;
		for (int d = 2; d <= digits; d++) {
			int t = 0;
			List<Integer> temp = new ArrayList<>();
			for (Integer r : res) {
				for (int i = 0; i < 10; i++) {
					if (Math.abs(i - (r % power)) == 1) {
						t = power * r + i;
						if (t > n) {
							return;
						}
						if (t >= power) {
							temp.add(t);
							System.out.print(t + " ");
						}

					}

				}
			}
			res = temp;
		}

	}

}
