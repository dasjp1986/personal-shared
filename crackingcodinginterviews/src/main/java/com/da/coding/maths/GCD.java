package com.da.coding.maths;

/**
 * 
 * @author dagraw1
 *
 *         Given 2 non negative integers m and n, find gcd(m, n)
 * 
 *         GCD of 2 integers m and n is defined as the greatest integer g such
 *         that g is a divisor of both m and n. Both m and n fit in a 32 bit
 *         signed integer.
 * 
 *         Example
 * 
 *         m : 6 n : 9
 * 
 *         GCD(m, n) : 3
 */
public class GCD {
	static int iterations = 0;

	//Time complexity- O(a/b) if a>b else O(b/a)
	public static int gcd(int a, int b) {
		iterations++;
		if (a == 0 || b == 0) {
			return Math.max(a, b);
		}
		if (a == 1 || b == 1) {
			return 1;
		}
		if (a == b) {
			return a;
		}
		if (a > b) {
			return gcd(a - b, b);
		} else {
			return gcd(a, b - a);
		}
	}

	public static void main(String[] args) {
//		System.out.println(gcd(0, 1));
//		System.out.println(gcd(2, 3));
//		System.out.println(gcd(15, 30));
//		System.out.println(gcd(20, 60));
		System.out.println(gcd(2, 6000));
		System.out.println(iterations);
	}
}
