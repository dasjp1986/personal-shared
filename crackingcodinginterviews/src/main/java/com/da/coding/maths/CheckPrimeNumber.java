package com.da.coding.maths;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *         Find if given number-N is prime or not
 *         Time Complexity- O(sqrt(N))
 */
public class CheckPrimeNumber {
	public static boolean isPrime(int num) {
		if (num == 1)
			return false;
		for (int guess = 2; guess * guess <= num; guess++) {
			if (num % guess == 0)
				return false;
		}
		return true;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(isPrime(Integer.parseInt(bfr.readLine())));
		bfr.close();
	}
}
