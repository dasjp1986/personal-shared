package com.da.coding.maths;

/**
 * 
 * @author dagraw1
 *
 *         Implement pow(A, B) % C.
 * 
 *         In other words, given A, B and C, find (AB)%C.
 * 
 *         Input : A = 2, B = 3, C = 3 Return : 2 2^3 % 3 = 8 % 3 = 2
 */
public class PowerFunction {

	//Time Complexity- O(LogN), Space Complexity- O(c) constant
	public static int pow(int x, int n, int d) {
		long a = x;
		long out = 1L;

		while (n > 0) {

			if (n % 2 == 1) {
				out *= a;
				out %= d;
			}

			a *= a;
			a %= d;
			n = n / 2;

		}

		out = (out + d) % d;

		return (int) out;

	}

	public static void main(String[] args) {
		System.out.println(pow(71045970, 41535484, 64735492));
		int a = 6, b = 6, d = 11;
		System.out.println(pow(a, b, d));
		System.out.println(Math.pow(a, b) % d);
	}

}
