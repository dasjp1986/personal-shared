package com.da.coding.maths;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given a sorted array of integers, find the number of occurrences of a
 *         given target value. Your algorithm’s runtime complexity must be in
 *         the order of O(log n). If the target is not found in the array,
 *         return 0
 ** 
 *         Example : ** Given [5, 7, 7, 8, 8, 10] and target value 8, return 2.
 */
public class CountElementOccurence {
	// DO NOT MODIFY THE LIST. IT IS READ ONLY
	public int findCount(final List<Integer> A, int B) {
		int pos = binSearch(A, B, 0, A.size() - 1);
		if (pos == -1) {
			return 0;
		}
		int count = 1;
		int prev = pos - 1;
		while (prev >= 0 && A.get(prev) == B) {
			count++;
			prev--;
		}

		int next = pos + 1;
		while (next < A.size() && A.get(next) == B) {
			count++;
			next++;
		}

		return count;
	}

	private int binSearch(final List<Integer> A, int B, int start, int end) {
		if (start <= end) {
			int mid = (start + end) / 2;
			if (A.get(mid) == B) {
				return mid;
			} else if (A.get(mid) < B) {
				return binSearch(A, B, mid + 1, end);
			} else {
				return binSearch(A, B, start, mid - 1);
			}
		}

		return -1;
	}

	public static void main(String[] args) {
		CountElementOccurence occurence = new CountElementOccurence();
		Integer[] array = { 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 7, 7, 8, 8, 8, 8, 9, 9,
				10, 10, 10 };
		System.out.println(occurence.findCount(Arrays.asList(array), 1));
	}
}
