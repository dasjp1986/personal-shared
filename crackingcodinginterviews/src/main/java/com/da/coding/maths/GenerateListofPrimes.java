package com.da.coding.maths;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given a number- n, generate list of prime numbers 2 through n
 *         inclusive
 */
public class GenerateListofPrimes {
	public static void main(String[] args) {
		System.out.println(generatePrimes(10));
	}

	//Time Complexity- O(n*no.of primes)
	//Space Complexity- O(no. of primes)
	public static List<Integer> generatePrimes(int number) {
		List<Integer> primes = new ArrayList<>();

		for (int i = 2; i <= number; i++) {
			addPrime(i, primes);
		}

		return primes;
	}

	//non-prime number will be divisible by a prime number
	private static void addPrime(int n, List<Integer> primes) {
		if (primes.isEmpty()) {
			primes.add(n);
		} else {
			for (int p=0;p<primes.size()/2 ; p++) {
				if (n % primes.get(p) == 0) {
					return;
				}
			}
			primes.add(n);
		}
	}
}
