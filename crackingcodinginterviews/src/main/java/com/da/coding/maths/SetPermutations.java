package com.da.coding.maths;

import java.util.ArrayList;

/**
 * 
 * 
 * Given a collection of numbers, return all possible permutations.
 * 
 * Example:
 * 
 * [1,2,3] will have the following permutations:
 * 
 * [1,2,3] [1,3,2] [2,1,3] [2,3,1] [3,1,2] [3,2,1] NOTE No two entries in the
 * permutation sequence should be the same. For the purpose of this problem,
 * assume that all the numbers in the collection are unique.@author dagraw1
 *
 */
public class SetPermutations {

	public ArrayList<ArrayList<Integer>> permute(ArrayList<Integer> A) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();
		perms(results, A, new ArrayList<>());
		return results;
	}

	private void perms(ArrayList<ArrayList<Integer>> results, ArrayList<Integer> remainsList,
			ArrayList<Integer> resList) {

		if (remainsList.isEmpty()) {
			results.add(resList);
		} else {
			for (int i = 0; i < remainsList.size(); i++) {
				ArrayList<Integer> remainsClone = (ArrayList<Integer>) remainsList.clone();
				ArrayList<Integer> resClone = (ArrayList<Integer>) resList.clone();

				resClone.add(remainsClone.get(i));
				remainsClone.remove(i);
				perms(results, remainsClone, resClone);
			}
		}

	}

	public static void main(String[] args) {
		Integer[] array = { 1, 2, 3 };
		ArrayList<Integer> list = new ArrayList<>();
		for (int i = 0; i < array.length; i++) {
			list.add(array[i]);
		}
		
		SetPermutations permutations= new SetPermutations();
		System.out.println(permutations.permute(list));
	}

}
