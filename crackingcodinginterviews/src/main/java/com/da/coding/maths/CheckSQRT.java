package com.da.coding.maths;

public class CheckSQRT {
	static int iteration = 0;

	public int sqrt(int a) {

		if (a == 0) {
			return 0;
		}
		return binSearch(1, a, a);
	}

	private int binSearch(int start, int last, int a) {
		if (start > last) {
			return last;
		}

		int guess = (start + last) / 2;

		long g = guess * guess;
		if (g == a) {
			return guess;
		}

		if (g < a) {
			System.out.println("iterationg left " + iteration + " start : " + start + " last : " + last);
			guess = binSearch(guess + 1, last, a);
		} else {
			System.out.println("iterationg right " + iteration + " start : " + start + " last : " + last);
			guess = binSearch(start, guess - 1, a);
		}

		return guess;
	}

	public static void main(String[] args) {
		CheckSQRT sq = new CheckSQRT();
		System.out.println(sq.sqrt(Integer.MAX_VALUE));
	}
}
