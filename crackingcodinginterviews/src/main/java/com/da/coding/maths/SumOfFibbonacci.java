package com.da.coding.maths;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         How many minimum numbers from fibonacci series are required such that
 *         sum of numbers should be equal to a given Number N? Note : repetition
 *         of number is allowed.
 * 
 *         Example:
 * 
 *         N = 4 Fibonacci numbers : 1 1 2 3 5 .... so on here 2 + 2 = 4 so
 *         minimum numbers will be 2
 */

public class SumOfFibbonacci {

	public int fibsum(int A) {
		List<Integer> fib = new ArrayList<>();

		int f = 0;
		int s = 1;

		fib.add(0);

		while (true) {
			int temp = f + s;
			if (temp == A) {
				return 1;
			} else if (temp > A) {
				break;
			} else {
				s = f;
				f = temp;

				fib.add(temp);
			}
		}

		int count = 0;
		int i = fib.size() - 1;
		while (i >= 0) {
			if (fib.get(i) > A) {
				i--;
				continue;
			}
			A = A - fib.get(i);
			count++;

			if (A == 0) {
				break;
			}

			if (fib.get(i) == A) {
				i++;
			}

			i--;
		}

		return count;
	}

	public static void main(String[] args) {
		SumOfFibbonacci fibo = new SumOfFibbonacci();
		System.out.println(fibo.fibsum(54));
	}

}
