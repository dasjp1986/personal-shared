package com.da.coding.maths;

import java.util.ArrayList;

/**
 * 
 * 
 * @author dagraw1
 *
 *         Given an even number ( greater than 2 ), return two prime numbers
 *         whose sum will be equal to given number.
 */
public class PrimeSum {

	public static void main(String[] args) {
		PrimeSum primes = new PrimeSum();
		System.out.println(primes.primesum(16777214));
	}

	public ArrayList<Integer> primesum(int A) {
		ArrayList<Integer> result = new ArrayList<>();

		for (int i = 2; i < A; i++) {
			if (isPrime(i) && isPrime(A - i)) {
				result.add(i);
				result.add(A - i);
				break;
			}
		}
		return result;
	}

	public boolean isPrime(int num) {
		if (num == 1)
			return false;
		for (int guess = 2; guess <= num / 2; guess++) {
			if (num % guess == 0)
				return false;
		}
		return true;
	}
}
