package com.da.coding.maths;

import java.util.ArrayList;
import java.util.List;

public class PalindromsUptoN {
	
	int iterations=0;
	
	public List<Integer> generatePalindromes(int n) {

		List<Integer> res = new ArrayList<>();
		for (int i = 1; i <= 2; i++) {
			for (int j = 1; j < n; j++) {
				int num = palindrome(i, j);
				iterations++;
				if (num <= n) {
					res.add(num);
				} else {
					return res;
				}
			}
		}

		return res;
	}
	
	public static void main(String[] args) {
		PalindromsUptoN palins= new PalindromsUptoN();
		
		System.out.println(palins.generatePalindromes(1500));
		System.out.println(palins.iterations);
	}

	private int palindrome(int e, int n) {

		int t1 = n;
		int t2 = n;

		if (e == 1) {
			t2 = t2 / 10;
		}

		while (t2 > 0) {
			t1 = t1 * 10 + t2 % 10;
			t2= t2/10;
		}

		return t1;
	}
}
