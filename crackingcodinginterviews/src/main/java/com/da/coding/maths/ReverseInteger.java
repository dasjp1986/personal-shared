package com.da.coding.maths;

/**
 * 
 * @author dagraw1
 * 
 *         Write a program to reverse a signed integer- 32 bits consider over
 *         and under flow
 */
public class ReverseInteger {
	//Time Complexity- O(N)
	public static int reverse(int x) {
		int sum = 0;
		while (x != 0) {
			int d = x % 10;
			x = x / 10;

			if (sum > Integer.MAX_VALUE / 10 || (sum == Integer.MAX_VALUE / 10 && d > 7))
				return 0;
			if (sum < Integer.MIN_VALUE / 10 || (sum == Integer.MIN_VALUE / 10 && d < -8))
				return 0;
			sum = sum * 10 + d;
		}
		return sum;
	}

	public static void main(String[] args) {
		System.out.println(reverse(Integer.MAX_VALUE));
		System.out.println(reverse(Integer.MIN_VALUE));
		System.out.println(reverse(120));
		System.out.println(reverse(12345));
	}
}
