package com.da.coding.maths;

/**
 * 
 * @author dagraw1
 * 
 *         Determine whether an integer is a palindrome. Do this without extra
 *         space.
 * 
 *         A palindrome integer is an integer x for which reverse(x) = x where
 *         reverse(x) is x with its digit reversed. Negative numbers are not
 *         palindromic.
 * 
 *         Example :
 * 
 *         Input : 12121 Output : 1
 * 
 *         Input : 123 Output : 0
 *
 */
public class IsPalindromeNumber {

	//Time complexity- O(n)
	public static int isPalindrome(int n) {
		int num = n;
		int sum = 0;
		while (n > 0) {
			sum = 10 * sum + n % 10;
			n = n / 10;
		}
		System.out.println(sum);
		if (sum == num) {
			return 1;
		} else {
			return 0;
		}
	}

	public static void main(String[] args) {
		System.out.println(isPalindrome(2147447412));
	}
}
