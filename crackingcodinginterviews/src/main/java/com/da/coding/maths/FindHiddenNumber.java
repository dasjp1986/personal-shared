package com.da.coding.maths;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *         You are given an array of integers. Now there is another integer
 *         which is unknown but you know its properties. The properties of the
 *         unknown number are:
 * 
 *         . It is an integer.
 * 
 *         . If you subtract all the elements of the array individually from the
 *         number and add all the differences then it adds to .
 * 
 *         Your goal is to find the number .
 * 
 *         Input The first line contains an integer denoting the total number of
 *         test cases. The first line of each test case contains an integer as
 *         input. Next line of each test case contains space separated integers.
 * 
 *         Output In the output you need to print the number if it exists or
 *         else you need to print .
 * 
 *         Constraints
 * 
 * 
 *         Sample Input 1 3 1 2 3 Sample Output 2 Explanation If you subtract
 *         all the integers from 2 the sum of difference is .
 */
public class FindHiddenNumber {

	public static void main(String args[]) throws Exception {

		// BufferedReader
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int testCases = Integer.parseInt(br.readLine()); // Reading input from STDIN
		for (int i = 0; i < testCases; i++) {
			int n = Integer.parseInt(br.readLine());
			String[] strs = br.readLine().split(" ");
			if (n != strs.length) {
				System.out.println(0);
			} else {
				int[] nums = new int[n];
				for (int j = 0; j < n; j++) {
					nums[j] = Integer.parseInt(strs[j]);
				}
				System.out.println(findX(nums));
			}

		}

	}

	private static int findX(int[] nums) {
		int sum = 0;
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
		}

		return sum % nums.length == 0 ? sum / nums.length : -1;
	}

}
