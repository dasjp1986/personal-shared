package com.da.coding.maths;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *         Number formation You are given an array of integers. Each integer is
 *         a single digit number in the range . You are also given a number .
 *         Now, you need to count how many subsequences of the array exist such
 *         that they form a digit valid number.
 * 
 *         A subsequence of size is called a valid number if there are no
 *         leading zeros in the number formed.
 * 
 *         Notes:
 * 
 *         A subsequence of an array is not necessarily contiguous. Suppose the
 *         given array is , then if you choose subsequence to be , then it is
 *         not a valid digit number. Also, it will not be considered as a single
 *         digit number. A valid digit number in the array is . Please go
 *         through the sample I/O for better understanding. Input Fomat
 * 
 *         The first line contains an integer as input denoting the size of the
 *         array. Next line contains space separated integers that denote
 *         elements of the array. Next line contains an integer .
 * 
 *         Output Format
 * 
 *         In the output, you need to print the count of valid digit numbers
 *         modulo .
 * 
 *         Constraints
 * 
 * 
 * 
 *         Sample Input 5 1 1 0 1 0 3 Sample Output 9
 */
public class NumberFormationByKDigits {

	public static void main(String args[]) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		String[] strs = br.readLine().split(" ");
		int[] nums = new int[n];
		for (int j = 0; j < n; j++) {
			nums[j] = Integer.parseInt(strs[j]);
		}
		int k = Integer.parseInt(br.readLine());
		if (k == 1) {
			System.out.println(nums.length);
		} else {
			int[][] cache = new int[n][k + 1];
			int count = 0;
			for (int i = 0; i <= n - k; i++) {
				if (nums[i] != 0) {
					count = count + find(nums, i + 1, k - 1, cache);
				}

			}

			System.out.println(count);
		}

	}

	private static int find(int[] nums, int i, int k, int[][] cache) {
		if (k == 0) {
			return 1;
		}

		if (i >= nums.length || k < 0) {
			return 0;
		}

		if (cache[i][k] != 0) {
			return cache[i][k];
		}
		int count = 0;
		count = count + find(nums, i + 1, k, cache) + find(nums, i + 1, k - 1, cache);

		cache[i][k] = count;
		return count;
	}

}
