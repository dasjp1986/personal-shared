package com.da.coding.maths;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1 Find a perfect square root of a given number if possible else
 *         return -1
 *
 *		Time Complexity O(logN) better than O(sqrt(N))
 */
public class PerfectSquareRoot {
	static long iterations = 0;

	//Time Complexity O(sqrt(N))
	public static long findPerfectSqrt(long num) {
		for (long guess = 1; guess * guess <= num; guess++) {
			iterations++;
			//this might exceed max bit size
			if (guess * guess == num)
				return guess;
		}
		return -1;
	}

	public static long findPerfectSqrtBinSearch(long num) {
		return binSearch(num, 1, num);
	}

	//Time Complexity O(logN)
	private static long binSearch(long num, long min, long max) {
		iterations++;
		
		if(max<min)
			return -1;
		long guess = (min + max) / 2;
		System.out.println("guessing..."+guess);
		if (guess * guess == num)
			return guess;
		else if (guess * guess < num) {
			return binSearch(num, guess + 1, max);
		} else {
			return binSearch(num, min, guess - 1);
		}

	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		long num =Long.parseLong(bfr.readLine());
		bfr.close();
		
		System.out.println("SQRT is :"+findPerfectSqrt(num));
		System.out.println("SQRT approach Iterations : " + iterations);
		
		iterations=0;
		System.out.println("SQRT is :"+findPerfectSqrtBinSearch(num));
		System.out.println("Bin Search approach Iterations : " + iterations);
		
	}
}
