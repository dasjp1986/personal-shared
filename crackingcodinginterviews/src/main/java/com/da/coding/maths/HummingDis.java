package com.da.coding.maths;

import java.util.ArrayList;
import java.util.List;

public class HummingDis {
//	public int hammingDistance(final List<Integer> A) {
//        int dis=0;
//        for(int i=0;i<A.size()-1;i++){
//            for(int j=i+1;j<A.size();j++){
//                dis+= distance(A.get(i),A.get(j));
//            }
//        }
//        
//        
//        return 2*dis;
//    }
//    
//    private int distance(int a, int b){
//        int d=0;
//        while(a!=0 || b!=0){
//            if((a&1)!=(b&1)){
//                d++;
//            }
//            a= a>>1;
//            b= b>>1;
//        }
//        return d;
//    }
	
	public int hammingDistance(final List<Integer> A) {
        int dis=0;
        int[] x0= new int[Integer.BYTES];
        int[] x1= new int[Integer.BYTES];
        for(int i=0;i<A.size();i++){
            int a= A.get(i);
            int bit0Pos=0;
            int bit1Pos=0;
            while(a!=0){
                if((a&1)==0){
                    x0[bit0Pos++]= x0[bit0Pos]+1;
                }else{
                    x1[bit1Pos++]= x1[bit1Pos]+1;
                }
                a=a>>1;
            }
        }
        
        for(int i=0;i<x0.length;i++){
            dis+= x0[i]*x1[i];
        }
        
        return 2*dis;
    }
    
    
    public static void main(String[] args) {
		HummingDis hm= new HummingDis();
		List<Integer> A= new ArrayList<>();
		A.add(2);
		A.add(4);
		A.add(6);
		System.out.println(hm.hammingDistance(A));
				
		
	}
}
