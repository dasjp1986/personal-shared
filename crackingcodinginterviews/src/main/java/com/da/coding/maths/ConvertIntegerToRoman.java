package com.da.coding.maths;

public class ConvertIntegerToRoman {
	public static String convert(int x) {
		String[] units = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
		String[] tens = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
		String[] hundreds = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
		String[] thousands = { "", "M", "MM", "MMM" };

		return thousands[x / 1000] + hundreds[(x % 1000) / 100] + tens[(x % 100) / 10] + units[x % 10];
	}
	
	public static void main(String[] args) {
		System.out.println(convert(1019));
	}
}
