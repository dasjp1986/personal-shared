package com.da.coding.bitwise;

/**
 * 
 * @author dagraw1
 *
 *         You are given an array A containing 2*N+2 positive numbers, out of
 *         which 2*N numbers exist in pairs whereas the other two number occur
 *         exactly once and are distinct. You need to find the other two numbers
 *         and print them in ascending order.
 * 
 * 
 *         Input : The first line contains a value T, which denotes the number
 *         of test cases. Then T test cases follow .The first line of each test
 *         case contains a value N. The next line contains 2*N+2 space separated
 *         integers.
 * 
 *         Output : Print in a new line the two numbers in ascending order.
 * 
 *         Constraints : 1<=T<=100 1<=N<=10^6 1<=A[i]<=5*10^8
 * 
 *         Example: Input : 2 2 1 2 3 2 1 4 1 2 1 3 2
 * 
 *         Output : 3 4 1 3
 */
public class FindTwoDistinctNumbers {

	public void find(int[] a) {
		int allXor = 0;
		for (int i = 0; i < a.length; i++) {
			allXor = allXor ^ a[i];
		}

		int bits = Integer.BYTES * 8 - 1;
		int num = 1 << bits;
		int pos = findFirstBitPos(allXor, num);
		num = Math.abs((num >> pos));
		
		int n = 0;
		for (int i = 0; i < a.length; i++) {
			if ((a[i] & num) != 0) {
				n = n ^ a[i];
			}
		}

		int first = Math.min(n, allXor ^ n);
		int second = Math.max(n, allXor ^ n);

		System.out.println(first + "," + second);

	}

	private int findFirstBitPos(int allXor, int num) {

		for (int i = 1; i < Integer.BYTES * 8; i++) {
			num = num >> 1;
			if ((allXor & num) != 0) {
				return i;
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		FindTwoDistinctNumbers problem = new FindTwoDistinctNumbers();
		problem.find(new int[] { 1, 2, 3, 1, 4, 3 });
		problem.find(new int[] {1,4,3,1,4,5});
		problem.find(new int[] {10,20,30,50,40,10,20,30});
	}
}
