package com.da.coding.bitwise;

public class NextSmallerAndLargerNumber {
	public static void printNextNumbers(int number) {
		System.out.println("searching for..."+number+" Binary : "+Integer.toBinaryString(number));
		int i = 0;
		int smaller = number;
		int larger = number;
		boolean isFoundSmaller = false;
		boolean isFoundLarger = false;
		int searchfor = (number & 1) == 0 ? 0 : 1;
		while (i < Integer.BYTES * 8) {
			while (i < Integer.BYTES * 8 && (number & 1) == searchfor) {
				i++;
				number = number >>> 1;
			}
			if(i>=Integer.BYTES*8-1){
				break;
			}
			if (searchfor == 0 && !isFoundSmaller) {
				smaller = setValues(smaller, i, i - 1, 0, 1);
				isFoundSmaller = true;
			} else if (!isFoundLarger) {
				larger = setValues(larger, i, i - 1, 1, 0);
				isFoundLarger = true;
			}
			if (isFoundSmaller && isFoundLarger) {
				break;
			}
			searchfor = number & 1;
		}
		System.out.println("next smaller : " + smaller+" next larger : " + larger);
		System.out.println();
	}

	private static int setValues(int nbr, int i, int j, int first, int second) {
		nbr = (nbr & ~(1 << i)) | (first << i);
		nbr = (nbr & ~(1 << j)) | (second << j);
		return nbr;
	}

	public static void main(String[] args) {
		printNextNumbers(5);
		printNextNumbers(10);
		printNextNumbers(11);
		printNextNumbers(12);
		printNextNumbers(1);
		printNextNumbers(Integer.MAX_VALUE);
	}
}
