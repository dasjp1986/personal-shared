package com.da.coding.bitwise;

/**
 * coun number of 1 bits
 * 
 * @author dagraw1
 *
 */
public class NumberOf1sBits {

	public static int numSetBits(long a) {
		int pos = Integer.BYTES * 8 - 1;
		int count = 0;
		while (pos >= 0) {
			if (((1 << pos) & a) != 0) {
				count++;
			}
			pos--;
		}

		return count;
	}

	public static void main(String[] args) {
		System.out.println(numSetBits(1));
	}

}
