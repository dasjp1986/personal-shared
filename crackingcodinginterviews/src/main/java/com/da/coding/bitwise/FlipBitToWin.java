package com.da.coding.bitwise;

/**
 * 
 * @author dagraw1
 *
 *         Given a number flip one of the bit from 0 to 1 which can result in
 *         maximum no. of sequences of 1's
 * 
 *         Ex- 0001011101101111 Ans- 7 (000101110[1101111])
 * 
 *         Ex- 1110 Ans- 4
 * 
 *         Ex- 0111 Ans- 4
 */
public class FlipBitToWin {
	//Time Complexity- O(no. of bits)
	//Space Complexity- O(1)
	public static int getMxSequence(int number) {
		int max = 0;
		int i = 0;
		int seqFor = 0;
		int counter = 0;
		int[] cals = new int[3]; // prev, zeros, ones
		while (i++ < Integer.BYTES * 8) {
			if ((number & 1) != seqFor) {
				if (seqFor == 0) {
					cals[1] = counter;
				} else {
					cals[2] = counter;
					int total = cals[0] + (cals[1] == 1 ? 1 : 0) + cals[2];
					if (total > max) {
						max = total;
					}
					cals[0] = cals[2];
					cals[2] = 0;
					cals[1] = 0;
				}
				counter = 0;
				seqFor = number & 1;
			}
			counter++;
			number = number >>> 1;
		}
		System.out.println(i);
		return max;
	}

	public static void main(String[] args) {
		int number = 1888;
		System.out.println("Binary : " + Integer.toBinaryString(number));
		System.out.println(getMxSequence(number));
	}
}
