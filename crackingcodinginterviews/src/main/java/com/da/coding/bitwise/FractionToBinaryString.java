package com.da.coding.bitwise;

import java.math.BigInteger;

/**
 * 
 * @author dagraw1
 *
 *         Given a fraction number- N (less than 1) convert it to binary
 *         representation print an error if it can not represented accurately in
 *         32 characters.
 */
public class FractionToBinaryString {
	public static String toString(double number) {
		StringBuilder builder = new StringBuilder();
		double mult;
		while (number != 0) {
			if (builder.length() == 32) {
				return "ERROR";
			}
			mult = number * 2;
			if (mult >= 1) {
				builder.append('1');
				number = mult - (int) mult;
			} else {
				builder.append('0');
				number = mult;
			}
		}
		return "0." + builder.reverse();
	}

	public static void main(String[] args) {
		System.out.println("Binary String : " + toString(0.625));
		System.out.println("Binary String : " + toString(0.125));
		System.out.println("Binary String : " + toString(0.3));
	}
}
