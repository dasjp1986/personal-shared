package com.da.coding.bitwise;

/**
 * 
 * @author dagraw1
 *
 *         Convert a given number A to B and returns number of bits needs to be
 *         fliped for this conversion
 *         
 *         Ex- 	91 (0101 1011)
 *         		77 (0100 1101)
 *         Ans- 3 
 */
public class Coversion {
	//Time Complexity- O(bits)
	public static int getBitsCount1Xors(int a, int b) {
		int counter = 0;

		for (int c = a ^ b; c != 0; c = c >> 1) {
			counter = counter+ (c & 1);
		}

		return counter;
	}
	
	//Time Complexity- O(diff bits)
	public static int getBitsCount1XorsFlip(int a, int b) {
		int counter = 0;

		for (int c = a ^ b; c != 0; c = c & (c - 1)) {
			counter++;
		}

		return counter;
	}
	
	public static void main(String[] args) {
		System.out.println(getBitsCount1Xors(91, 77));
		System.out.println(getBitsCount1XorsFlip(91, 77));
	}
	
}
