package com.da.coding.bitwise;

/**
 * 
 * @author dagraw1
 *
 *         Given a number swap bits at odd position with bits on to the event
 *         position with minimum instructions
 * 
 */
public class SwapBits {
	public static int swap(int num) {
		int result = 0;
		System.out.println("Given number : " + Integer.toBinaryString(num));
		result = ((num & 0xAAAAAAAA) >>> 1) | (num & 0x55555555) << 1;
		System.out.println("Swapped number : " + Integer.toBinaryString(result));
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(swap(Integer.MAX_VALUE));
	}
}
