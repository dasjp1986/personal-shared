package com.da.coding.bitwise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of N integers, find the pair of integers in the array
 *         which have minimum XOR value. Report the minimum XOR value.
 * 
 *         Examples : Input 0 2 5 7 Output 2 (0 XOR 2) Input 0 4 7 9 Output 3 (4
 *         XOR 7)
 * 
 *         Constraints: 2 <= N <= 100 000 0 <= A[i] <= 1 000 000 000
 * 
 * 
 */
public class MinXor {

	public int findMinXor(ArrayList<Integer> a) {
		int pos = Integer.BYTES * 8;
		Map<Integer, Boolean> map = new HashMap<>();
		for (int i = 0; i < a.size(); i++) {
			if (map.get(a.get(i)) == null) {
				map.put(a.get(i), true);
			} else {
				return 0;
			}
		}
		return min(a, pos - 1);
	}

	private int min(ArrayList<Integer> a, int pos) {
		if (a.size() < 2) {
			return Integer.MAX_VALUE;
		}
		if (a.size() == 2) {
			return a.get(0) ^ a.get(1);
		}

		ArrayList<Integer> zeros = new ArrayList<>();
		ArrayList<Integer> ones = new ArrayList<>();

		for (int i = 0; i < a.size(); i++) {
			if ((a.get(i) & 1 << pos) == 0) {
				zeros.add(a.get(i));
			} else {
				ones.add(a.get(i));
			}
		}
		return Math.min(min(zeros, pos - 1), min(ones, pos - 1));
	}

	public static void main(String[] args) {
		MinXor minXor = new MinXor();
		ArrayList<Integer> list = new ArrayList<>();
		int[] array = { 15, 5, 1, 10, 2 };
		for (int i = 0; i < array.length; i++) {
			list.add(array[i]);
		}
		System.out.println(minXor.findMinXor(list));
	}

}
