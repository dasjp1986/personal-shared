package com.da.coding.bitwise;

/**
 * 
 * @author dagraw1
 * 
 *         Given two Numbers M and N with bits position i and j such that N is
 *         much larger than and is capable of fitting all the bits of M. value
 *         of i and j should be enough such that j-i bits can fit M when
 *         represented in binary.
 * 
 *         Insert M into N starting from i through j from low significant to
 *         most significant bits of N
 * 
 *         Ex- N- 10000000 (128) M- 1011(11) i-2 j-5 After insertion- 10101100
 *         (172)
 */
public class NumbersInsertion {
	public int insert(int m, int n, int i, int j) {
		System.out.println(
				"Trying inserting m : " + Integer.toBinaryString(m) + " into n : " + Integer.toBinaryString(n));
		int maxNumber = (int) (Math.pow(2, j - i + 1) - 1);
		if (m > maxNumber) {
			throw new RuntimeException("M can not be fitted for given value of i and j, maxNumber : " + maxNumber);
		}
		for (int k = i; k <= j; k++) {
			int value = getBitAtIthPos(m, k - i) == 1 ? 1 : 0;
			n = (n & ~(1 << k)) | (value << k);
			System.out.println("set : " + value + " at kth : " + k + " resulted : " + Integer.toBinaryString(n));
		}
		return n;
	}

	private int getBitAtIthPos(int m, int i) {

		return (m & 1 << i) == 0 ? 0 : 1;
	}

	public static void main(String[] args) {
		NumbersInsertion insertion = new NumbersInsertion();
		int result = insertion.insert(11, 128, 2, 5);
		System.out.println("Result : " + result + " binaries : " + Integer.toBinaryString(result));
		
		result = insertion.insert(10, 127, 3, 7);
		System.out.println("Result : " + result + " binaries : " + Integer.toBinaryString(result));
	}
}
