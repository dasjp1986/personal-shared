package com.da.coding.contests;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * 
 * @author dagraw1 You are given coins- {5,10,20} and lemonade price - 5 write a
 *         program to take customer orders for one lemonade each, return change
 *         to the customer and exit program with false if not possible
 * 
 *         coins-{5,10,20} orders- {5,5,10,20} returns- true
 * 
 *         coins-{5,10,20} orders- {5,5,10,20,10} returns- false
 */
public class Lemonade {
	public static void main(String[] args) {
		int[] inputs = { 5, 5, 10, 20, 10 };
		int[] coins = { 5, 10, 20 };
		int ticket = 5;
		Map<Integer, Integer> sortedMap = createMap(coins);
		for (int i = 0; i < inputs.length; i++) {

			if (change(inputs[i] - ticket, sortedMap)) {
				sortedMap.put(inputs[i], sortedMap.get(inputs[i]) + 1);
			} else {
				System.out.println(false);
				return;
			}
		}
		System.out.println(true);
	}

	private static boolean change(int amount, Map<Integer, Integer> sortedMap) {

		if (amount == 0) {
			return true;
		}

		for (Entry<Integer, Integer> entry : sortedMap.entrySet()) {
			if (amount >= entry.getKey() && entry.getValue() > 0) {
				sortedMap.put(entry.getKey(), entry.getValue() - 1);
				if (change(amount - entry.getKey(), sortedMap)) {
					return true;
				}
			}
		}

		return false;
	}

	private static Map<Integer, Integer> createMap(int[] coins) {
		Map<Integer, Integer> sortedMap = new TreeMap<>(new Comparator<Integer>() {
			public int compare(Integer first, Integer second) {
				return second.compareTo(first);
			}
		});

		for (int coin : coins) {
			sortedMap.put(coin, 0);
		}

		return sortedMap;
	}
}
