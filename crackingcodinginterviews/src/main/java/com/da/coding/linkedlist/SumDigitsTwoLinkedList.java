package com.da.coding.linkedlist;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1
 * Given two linked lists where each node in the linked list refer to single digit values
 * compute sum of both the linked list and return newly generated linked list as output.
 * 
 * Ex-
 * 1 2 3 4
 * 9 9
 * 
 * when head represent 1s position of the number
 * 4321+99= 4420     ->0->2->4->4
 * 
 * when tail represent 1s position of the number
 * 1234+99= 1333     ->1->3->3->3
 *
 */
public class SumDigitsTwoLinkedList {
	public static SinglyLinkedList<Integer> sumHead1sDigits(SLNode<Integer> p1, SLNode<Integer> p2, int carryFwd,
			SinglyLinkedList<Integer> resultList) {
		int sum = 0;
		if (p1 == null && p2 == null) {
			if (carryFwd != 0) {
				resultList.addTail(carryFwd);
			}
			return resultList;
		} else if (p1 == null) {
			sum = p2.getValue() + carryFwd;
			resultList.addTail(sum % 10);
			p2 = p2.getNext();
		} else if (p2 == null) {
			sum = p1.getValue() + carryFwd;
			resultList.addTail(sum % 10);
			p1 = p1.getNext();
		} else {
			sum = p1.getValue() + p2.getValue() + carryFwd;
			resultList.addTail(sum % 10);

			p1 = p1.getNext();
			p2 = p2.getNext();
		}
		carryFwd = sum / 10;
		return sumHead1sDigits(p1, p2, carryFwd, resultList);

	}

	// Time Complexity- O(4N)->O(N)
	public static SinglyLinkedList<Integer> sumTail1sDigits(SinglyLinkedList<Integer> list1,
			SinglyLinkedList<Integer> list2, SinglyLinkedList<Integer> resultList) {

		list1.reverse();
		list2.reverse();

		resultList = sumHead1sDigits(list1.getHead(), list2.getHead(), 0, resultList);
		resultList.reverse();
		return resultList;

	}

	public static void main(String[] args) {
		String[] strArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> list1 = new SinglyLinkedList<>();
		for (int i = 0; i < strArray.length; i++) {
			list1.addTail(Integer.parseInt(strArray[i]));
		}
		strArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> list2 = new SinglyLinkedList<>();
		for (int i = 0; i < strArray.length; i++) {
			list2.addTail(Integer.parseInt(strArray[i]));
		}
		SinglyLinkedList<Integer> resultList = sumHead1sDigits(list1.getHead(), list2.getHead(), 0,
				new SinglyLinkedList<Integer>());
		System.out.println("printing sum of linked list starting 1s from head");
		resultList.print();

		resultList = sumTail1sDigits(list1, list2, new SinglyLinkedList<Integer>());
		System.out.println("printing sum of linked list starting 1s from tail");
		resultList.print();

	}
}
