package com.da.coding.linkedlist;

/**
 * 
 * @author dagraw1
 *
 *         Given a linked list and a value x, partition it such that all nodes
 *         less than x come before nodes greater than or equal to x.
 * 
 *         You should preserve the original relative order of the nodes in each
 *         of the two partitions.
 * 
 *         For example, Given 1->4->3->2->5->2 and x = 3, return
 *         1->2->2->4->3->5.
 */
public class PartitionLinkedListAtX {
	
	//Time complexity- o(n), Space Complexity- O(1)
	public static ListNode partition(ListNode A, int B) {
		ListNode smallHead = null;
		ListNode largeHead = null;

		ListNode temp = A;
		ListNode smallEnd = null;
		ListNode largeEnd = null;
		ListNode node = null;
		ListNode head = null;

		while (temp != null) {
			if (temp.val < B) {
				if (smallHead == null) {
					smallHead = temp;
					smallEnd = temp;
				} else {
					smallEnd.next = temp;
					smallEnd = smallEnd.next;
				}
			} else {
				if (largeHead == null) {
					largeHead = temp;
					largeEnd = temp;
				} else {
					largeEnd.next = temp;
					largeEnd = largeEnd.next;
				}
			}
			node = temp;
			temp = temp.next;
			node.next = null;
		}
		if (largeHead != null) {
			head = largeHead;
		}
		if (smallHead != null) {
			head = smallHead;
			smallEnd.next = largeHead;
		}
		return head;

	}

	public static void main(String[] args) {
		ListNode root = new ListNode(1);
		ListNode node = root;

		node.next = new ListNode(4);
		node = node.next;

		node.next = new ListNode(3);
		node = node.next;

		node.next = new ListNode(2);
		node = node.next;

		node.next = new ListNode(5);
		node = node.next;

		node.next = new ListNode(2);
		node = node.next;

		printList(root);
		ListNode partition = partition(root, 3);
		System.out.println("Partioned");
		printList(partition);
	}

	private static void printList(ListNode root) {
		ListNode node = root;
		while (node != null) {
			System.out.println(node.val + "->>");
			node = node.next;
		}

	}
}
