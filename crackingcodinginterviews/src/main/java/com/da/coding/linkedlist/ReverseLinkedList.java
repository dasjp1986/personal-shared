package com.da.coding.linkedlist;

public class ReverseLinkedList {

	public static ListNode reverseIteratively(ListNode head) {
		ListNode prev = head;
		ListNode next = head.next;

		prev.next = null;

		while (next != null) {
			ListNode temp = prev;
			prev = next;
			next = next.next;
			prev.next = temp;

		}

		return prev;
	}

	public static ListNode recurseSimple(ListNode node, ListNode current) {
		if(current==null) {
			return node;
		}
		
		ListNode next= current.next;
		current.next= node;
		return recurseSimple(current, next);
	}
	
	public static ListNode reverseRecursively(ListNode head) {
		ListNode prev= head;
		ListNode next= head.next;
		prev.next= null;
		
		
		return recursiveUtil(prev, next);
	}

	private static ListNode recursiveUtil(ListNode prev, ListNode next) {

		
		if (next != null) {
			ListNode temp = prev;
			prev = next;
			next = next.next;
			prev.next = temp;
			
			prev= recursiveUtil(prev, next);
		}

		return prev;
	}

	public static void main(String[] args) {
		ListNode root = new ListNode(1);
		ListNode node = root;
		for (int i = 2; i <= 10; i++) {
			node.next = new ListNode(i);
			node = node.next;
		}
		// printList(root);
		//printList(reverseIteratively(root));
		//printList(reverseRecursively(root));
		// printList(reverseList(root));
		printList(recurseSimple(null, root));
	}

	private static void printList(ListNode root) {
		ListNode node = root;
		while (node != null) {
			System.out.println(node.val + "->>");
			node = node.next;
		}

	}
}
