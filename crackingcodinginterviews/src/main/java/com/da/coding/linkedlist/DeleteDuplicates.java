package com.da.coding.linkedlist;

/**
 * 
 * @author dagraw1
 *
 *         Given a sorted linked list, delete all duplicates such that each
 *         element appear only once.
 * 
 *         For example, Given 1->1->2, return 1->2. Given 1->1->2->3->3, return
 *         1->2->3.
 */
public class DeleteDuplicates {
	public ListNode deleteDuplicates(ListNode A) {
		ListNode prev = A;
		ListNode node = A.next;

		while (node != null) {
			ListNode temp = null;
			if (node.val == prev.val) {
				temp = node;
				prev.next = node.next;
			} else {
				prev = node;
			}
			node = node.next;
			if (temp != null) {
				temp.next = null;
			}

		}

		return A;
	}

	public static void main(String[] args) {
		ListNode start = new ListNode(1);
		ListNode node = start;
		node.next = new ListNode(1);
		node = node.next;

		node.next = new ListNode(2);
		node = node.next;

		node.next = new ListNode(3);
		node = node.next;

		node.next = new ListNode(3);
		node = node.next;

		printList(start);
		DeleteDuplicates deleteDuplicates = new DeleteDuplicates();
		printList(deleteDuplicates.deleteDuplicates(start));

	}

	private static void printList(ListNode root) {
		ListNode node = root;
		while (node != null) {
			System.out.println(node.val + "->>");
			node = node.next;
		}

	}
}
