package com.da.coding.linkedlist;

import java.util.HashMap;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1
 * Find if two given singly linked lists(of size M and N) intersect at some point in time.
 * When linked lists intersect they will have same node reference not just the value.
 * 
 * Ex
 * 1->2->3->4->5
 * 4->3->4->5
 * 
 * both the linked list intersect at 3, which is after 2,4
 * 
 * Follow up, return the node reference where both linked list intersect
 */
public class IntersectLinkedList {
	//Time Complexity- O(M+N)
	//Space Complexity- O(M)
	public static <T> boolean isIntersectUsingHashMap(SLNode<T> head1, SLNode<T> head2){
		HashMap<SLNode<T>, Boolean> map= new HashMap<>();
		
		System.out.println("Finding intersect by using HashMap...");
		
		SLNode<T> node= head1;
		while(node!=null){
			map.put(node, true);
			node= node.getNext();
		}
		
		node= head2;
		while(node!=null){
			if(map.get(node)!=null){
				System.out.println("Linked Lists intersect at : "+node.getValue());
				return true;
			}
			node= node.getNext();
		}
		return false;
	}
	
	//Time Complexity- O(M+N)
	//Space Complexity- O(1)
	public static <T> void isIntersectNoExtraSpace(SLNode<T> head1, SLNode<T> head2){
		
		SLNode<T> node1= head1;
		SLNode<T> node2= head2;
		int length1=1;
		int length2=1;
		
		System.out.println("Finding intersect without extra space...");
		
		while(node1.getNext()!=null){
			length1++;
			node1= node1.getNext();
		}
		
		while(node2.getNext()!=null){
			length2++;
			node2= node2.getNext();
		}
		
		if(node1!=node2){
			System.out.println("Linked lists do not intersect");
			return;
		}
		
		SLNode<T> intersect= null;
		if(length1>length2){
			intersect= findIntersection(head1, head2, length1-length2, length1);
		}else{
			intersect= findIntersection(head2, head1, length2-length1, length2);
		}
		if(intersect==null){
			System.out.println("Error in the logic");
		}else{
			System.out.println("Linked Lists intersect at : "+intersect.getValue());
		}
	}
	
	private static <T> SLNode<T> findIntersection(SLNode<T> largeListHead, SLNode<T> smallListHead, int diff, int length) {
		
		SLNode<T> node1= largeListHead;
		SLNode<T> node2= smallListHead;
		int i = 0;
		while ( i < diff) {
			node1= node1.getNext();
			i++;
		}
		
		while(i<length){
			if(node1==node2)
				return node1;
			node1= node1.getNext();
			node2= node2.getNext();
			i++;
			
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println("Enter first two lists without intersection");
		String[] strArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> list1= new SinglyLinkedList<>();
		for (int i = 0; i < strArray.length; i++) {
			list1.addTail(Integer.parseInt(strArray[i]));
		}
		
		strArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> list2= new SinglyLinkedList<>();
		for (int i = 0; i < strArray.length; i++) {
			list2.addTail(Integer.parseInt(strArray[i]));
		}
		
		System.out.println("Enter common elements");
		strArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> common= new SinglyLinkedList<>();
		for (int i = 0; i < strArray.length; i++) {
			common.addTail(Integer.parseInt(strArray[i]));
		}
		
		appendList(list1, common);
		appendList(list2, common);
		list1.print();
		list2.print();
		System.out.println(isIntersectUsingHashMap(list1.getHead(), list2.getHead()));
		isIntersectNoExtraSpace(list1.getHead(), list2.getHead());
	}

	private static <T> void appendList(SinglyLinkedList<T> list1, SinglyLinkedList<T> common) {
		SLNode<T> node= list1.getHead();
		while (node.getNext()!=null) {
			node= node.getNext();
		}
		node.setNext(common.getHead());
	}
}
