package com.da.coding.linkedlist;

import java.util.Random;

import com.da.coding.commons.SystemInputReaderUtil;
/**
 * 
 * @author dagraw1
 * Given a damaged linked list which might have loop, find if it has a loop.
 * Return node which is at the beginning of the loop
 *
 */
public class LoopSinglyLinkedList {
	//Time Complexity- O(N+loop-length)->O(N)
	//Space Complexity- O(1)
	public static <T> T loopBeginAt(SinglyLinkedList<T> list) {
		SLNode<T> slow= list.getHead();
		SLNode<T> fast= slow.getNext();
		
		boolean isLoop= false;
		
		while(fast!=null){
			if(fast== slow){
				isLoop= true;
				break;
			}
			slow= slow.getNext();
			fast= fast.getNext();
			if(fast!=null)
				fast= fast.getNext();
		}
		
		if(!isLoop)
			return null;
		//as fast was started one pointer ahead of slow
		slow= slow.getNext();
		SLNode<T> temp= list.getHead();
		while (temp!=slow) {
			temp= temp.getNext();
			slow= slow.getNext();
		}
		return slow.getValue();
	}

	public static void main(String[] args) {
		String[] strArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> intList = new SinglyLinkedList<>();
		for (int i = 0; i < strArray.length; i++) {
			intList.addTail(Integer.parseInt(strArray[i]));
		}
		SLNode<Integer> temp= intList.getHead();
		SLNode<Integer> middle = null;
		int i=0;
		while (temp.getNext()!=null) {
			if(loopCondition(strArray.length, i)){
				middle= temp;
			}
			temp = temp.getNext();
			i++;
		}
		if(middle==null)
			middle= temp;
		System.out.println("Expected loop begins at : "+middle.getValue());
		temp.setNext(middle);
		System.out.println(loopBeginAt(intList));
	}

	private static boolean loopCondition(int length, int counter) {
		Random ran= new Random();
		return length-1==counter;
	}
}
