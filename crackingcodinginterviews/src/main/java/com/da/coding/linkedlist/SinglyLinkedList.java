package com.da.coding.linkedlist;

import com.da.coding.commons.SystemInputReaderUtil;

public class SinglyLinkedList<T> {
	private SLNode<T> head;

	// Time Complexity- O(1)
	public void addFront(T value) {
		SLNode<T> node = new SLNode<T>(value);

		if (head == null)
			head = node;
		else {
			node.setNext(head);
			head = node;
		}
	}

	// Time Complexity- O(N)
	public void addTail(T value) {
		SLNode<T> node = new SLNode<T>(value);

		if (head == null)
			head = node;
		else {
			SLNode<T> temp = head;

			while (temp.getNext() != null) {
				temp = temp.getNext();
			}
			temp.setNext(node);
		}
	}

	// Time Complexity- O(N)
	public boolean addAfterNode(T value, T kValue) {
		SLNode<T> node = new SLNode<T>(value);

		SLNode<T> temp = head;

		while (temp.getNext() != null) {
			if (temp.getValue().equals(kValue)) {
				node.setNext(temp.getNext());
				temp.setNext(node);

				return true;
			}
			temp = temp.getNext();
		}

		return false;

	}

	// Time Complexity- O(N)
	public boolean remove(T value) {
		SLNode<T> temp = head;
		SLNode<T> prev = head;
		while (temp != null) {
			if (temp.getValue().equals(value)) {
				if (temp == head) {
					head = head.getNext();
				} else {
					prev.setNext(temp.getNext());
				}
				return true;
			}
			prev = temp;
			temp = temp.getNext();
		}

		return false;
	}

	public SLNode<T> removeFirst() {
		SLNode<T> node = this.head;
		this.head = head.getNext();
		return node;
	}

	public void print() {
		SLNode<T> temp = head;

		while (temp != null) {
			System.out.print("->" + temp.getValue());
			temp = temp.getNext();
		}
		System.out.println();

	}

	// Time Complexity- O(N)
	public void reverse() {
		SLNode<T> prev = head;
		SLNode<T> next = head.getNext();
		System.out.println("Reversing the linked list");
		while (next != null) {
			prev.setNext(next.getNext());
			next.setNext(head);
			head = next;

			next = prev.getNext();
		}
	}

	public SLNode<T> getHead() {
		return head;
	}

	public void setHead(SLNode<T> head) {
		this.head = head;
	}

	public boolean isEmpty() {
		return head == null;
	}

	public static void main(String[] args) {
		String[] array = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> intList = new SinglyLinkedList<>();
		for (int i = 0; i < array.length; i++) {
			intList.addTail(Integer.parseInt(array[i]));
		}
		intList.print();
		intList.remove(5);
		intList.print();
		intList.addFront(5);
		intList.print();
		System.out.println(intList.addAfterNode(10, 5));
		intList.print();
		System.out.println(intList.addAfterNode(10, 50));
		intList.print();
		intList.reverse();
		intList.print();
	}

	@Override
	public String toString() {
		return "SinglyLinkedList [head=" + head + "]";
	}

	public SinglyLinkedList<T> clone() {
		SinglyLinkedList<T> newList = new SinglyLinkedList<>();
		SLNode<T> temp = head;
		while (temp != null) {
			newList.addTail(temp.getValue());
			temp = temp.getNext();
		}
		return newList;
	}

}
