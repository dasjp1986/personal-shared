package com.da.coding.linkedlist;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1 Find/Delete middle of the given singly linked list when you
 *         have access to head node 
 *         Ex ->1->2->3 returns: 2
 * 
 *         ->1->2->3->4->5->6->7->8 
 *         returns: 4
 * 
 *         Delete middle of the given singly linked list when you have no access
 *         to head node
 *
 */
public class MiddleOfSinglyLinkedList {
	// Time Complexity- O(N), Space Complexity- O(1)
	public static <T> SLNode<T> getMiddle(SinglyLinkedList<T> list) {
		SLNode<T> p1 = list.getHead();
		SLNode<T> p2 = p1;

		while (p2.getNext() != null) {
			p2 = p2.getNext();
			p2 = p2.getNext();
			if (p2 == null) {
				break;
			}
			p1 = p1.getNext();
		}
		return p1;

	}

	// Time Complexity- O(N), Space Complexity- O(1)
	public static <T> void deleteMiddle(SinglyLinkedList<T> list) {
		SLNode<T> p1 = list.getHead();
		SLNode<T> p2 = p1;
		SLNode<T> prev = p1;

		while (p2.getNext() != null) {
			p2 = p2.getNext();
			p2 = p2.getNext();
			if (p2 == null) {
				break;
			}
			prev = p1;
			p1 = p1.getNext();
		}

		prev.setNext(p1.getNext());
		p1.setNext(null);

	}

	// Time Complexity- O(1), Space Complexity- O(1)
	public static <T> void deleteMiddleWithNoHeadAccess(SLNode<T> mNode) {
		SLNode<T> next = mNode.getNext();
		mNode.setValue(next.getValue());
		mNode.setNext(next.getNext());
		next.setNext(null);

	}

	public static void main(String[] args) {
		String[] inputArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> intList = new SinglyLinkedList<>();
		for (int i = 0; i < inputArray.length; i++) {
			intList.addTail(Integer.parseInt(inputArray[i]));
		}
		intList.print();
		System.out.println(getMiddle(intList).getValue());
		deleteMiddle(intList);
		intList.print();

		deleteMiddleWithNoHeadAccess(getMiddle(intList));
		intList.print();
	}
}
