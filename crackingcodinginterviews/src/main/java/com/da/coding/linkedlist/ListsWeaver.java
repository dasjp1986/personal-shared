package com.da.coding.linkedlist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * 
 * @author dagraw1
 * 
 *         Given two Lists with unique items, weave them by ensuring their
 *         relative order remains unchanged.
 * 
 *         {(1,2) (3,4)}
 *         {(1,2,3,4),(1,3,2,4),(1,3,4,2),(3,4,1,2),(3,1,4,2),(3,1,2,4)}
 * 
 *         Return list of lists weaved
 */
public class ListsWeaver<T> {
	public ArrayList<LinkedList<T>> weaveLists(LinkedList<T> first, LinkedList<T> second) {
		LinkedList<T> prefix = new LinkedList<>();
		ArrayList<LinkedList<T>> results = new ArrayList<>();
		weave(first, second, prefix, results);
		return results;
	}

	public void weave(LinkedList<T> first, LinkedList<T> second, LinkedList<T> prefix,
			ArrayList<LinkedList<T>> results) {

		if (first.isEmpty() || second.isEmpty()) {
			LinkedList<T> result = new LinkedList<>(prefix);
			result.addAll(first);
			result.addAll(second);
			results.add(result);
			return;
		}

		T node = first.removeFirst();
		prefix.addLast(node);
		weave(first, second, prefix, results);
		prefix.remove(node);
		first.addFirst(node);

		node = second.removeFirst();
		prefix.addLast(node);
		weave(first, second, prefix, results);
		prefix.remove(node);
		second.addFirst(node);

	}

	public static void main(String[] args) {
		LinkedList<Integer> first = new LinkedList<>();
		LinkedList<Integer> second = new LinkedList<>();

		first.addLast(1);
		first.addLast(2);

		second.addLast(3);
		second.addLast(4);

		ListsWeaver<Integer> weaver = new ListsWeaver<>();
		ArrayList<LinkedList<Integer>> weaveLists = weaver.weaveLists(first, second);

		Iterator<LinkedList<Integer>> iterator = weaveLists.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
