package com.da.coding.linkedlist;

import java.util.Stack;

/**
 * 
 * @author dagraw1
 * 
 *         Given a singly linked list, determine if its a palindrome. Return 1
 *         or 0 denoting if its a palindrome or not, respectively.
 * 
 *         Notes:
 * 
 *         Expected solution is linear in time and constant in space. For
 *         example,
 * 
 *         List 1-->2-->1 is a palindrome. List 1-->2-->3 is not a palindrome.
 * 
 */
public class PalindromeLinkedList {

	// Time complexity- O(n)
	// Space complexity- O(1)
	public static int lPalin(ListNode start) {
		ListNode slow = start;
		ListNode fast = slow.next;

		while (fast.next != null) {
			slow = slow.next;
			fast = fast.next;
			if (fast.next != null) {
				fast = fast.next;
			}
		}

		ListNode second = reverseList(slow.next);
		int res = 1;

		while (start != null && second != null) {
			if (start.val != second.val) {
				res = 0;
				break;
			}
			start = start.next;
			second = second.next;
		}

		return res;

	}

	private static ListNode reverseList(ListNode start) {
		ListNode end = start;
		while (end.next != null) {
			end = end.next;
		}

		ListNode node = start;
		while (node != end) {
			ListNode temp = node;
			node = node.next;

			temp.next = end.next;
			end.next = temp;
		}

		return end;
	}

	// Time Complexity- O(N/2+N/2+N/2)-> O(N)
	// Space Complexity- O(N/2)
	public static <T> boolean isPalindromeUsingTempList(SinglyLinkedList<T> list) {
		SLNode<T> slow = list.getHead();
		SLNode<T> fast = slow.getNext();

		while (fast != null) {
			slow = slow.getNext();
			fast = fast.getNext();
			if (fast != null)
				fast = fast.getNext();
		}
		SinglyLinkedList<T> tempList = new SinglyLinkedList<>();
		while (slow != null) {
			tempList.addFront(slow.getValue());
			slow = slow.getNext();
		}

		tempList.print();
		slow = list.getHead();
		fast = tempList.getHead();

		while (fast != null) {
			if (!slow.getValue().equals(fast.getValue()))
				return false;
			slow = slow.getNext();
			fast = fast.getNext();
		}
		return true;
	}

	// Time Complexity- O(N/2+N/2+N/2)-> O(N)
	// Space Complexity- O(N/2)
	public static <T> boolean isPalindromeUsingTempStack(SinglyLinkedList<T> list) {
		SLNode<T> slow = list.getHead();
		SLNode<T> fast = slow.getNext();

		while (fast != null) {
			slow = slow.getNext();
			fast = fast.getNext();
			if (fast != null)
				fast = fast.getNext();
		}
		Stack<T> tempStack = new Stack<>();
		while (slow != null) {
			tempStack.push(slow.getValue());
			slow = slow.getNext();
		}

		slow = list.getHead();

		while (!tempStack.isEmpty()) {
			if (!slow.getValue().equals(tempStack.pop()))
				return false;
			slow = slow.getNext();
		}
		return true;
	}

	public static void main(String[] args) {
		// String[] strArray = SystemInputReaderUtil.readString().split(" ");
		// SinglyLinkedList<Integer> intList = new SinglyLinkedList<>();
		// for (int i = 0; i < strArray.length; i++) {
		// intList.addTail(Integer.parseInt(strArray[i]));
		// }
		// System.out.println(isPalindromeUsingTempList(intList));
		// System.out.println(isPalindromeUsingTempStack(intList));

		ListNode start = new ListNode(1);
		ListNode node = start;
		node.next = new ListNode(2);
		node = node.next;

		node.next = new ListNode(3);
		node = node.next;

		node.next = new ListNode(2);
		node = node.next;

		node.next = new ListNode(2);
		node = node.next;

		printList(start);
		System.out.println(lPalin(start));

	}

	private static void printList(ListNode root) {
		ListNode node = root;
		while (node != null) {
			System.out.println(node.val + "->>");
			node = node.next;
		}

	}
}
