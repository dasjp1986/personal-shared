package com.da.coding.linkedlist;

import java.util.HashMap;
import java.util.Map;

import com.da.coding.commons.SystemInputReaderUtil;

public class RemoveDuplicatesLinkedList {

	// Time Complexity- O(N^2), Space Complexity- O(1)
	public static void removeDupsNoBuffer(SinglyLinkedList<Integer> linkedList) {
		SLNode<Integer> node = linkedList.getHead();
		SLNode<Integer> next;
		SLNode<Integer> prev;

		while (node != null) {
			prev = node;
			next = node.getNext();

			while (next != null) {
				if (node.getValue().equals(next.getValue())) {
					prev.setNext(next.getNext());
				} else {
					prev = next;
				}
				next = next.getNext();
			}

			node = node.getNext();
		}
	}

	// Time Complexity- O(N), Space Complexity- O(N)
	public static void removeDupsWithBuffer(SinglyLinkedList<Integer> linkedList) {
		SLNode<Integer> node = linkedList.getHead();
		SLNode<Integer> prev = node;

		Map<Integer, SLNode<Integer>> nodeMap = new HashMap<>();

		while (node != null) {
			SLNode<Integer> temp = nodeMap.get(node.getValue());
			if (temp == null) {
				nodeMap.put(node.getValue(), node);
				prev = node;
			} else {
				prev.setNext(node.getNext());
			}
			node = node.getNext();

		}
	}

	public static void main(String[] args) {
		String[] stringArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> intList = new SinglyLinkedList<>();
		for (int i = 0; i < stringArray.length; i++) {
			intList.addTail(Integer.parseInt(stringArray[i]));
		}
		intList.print();
		//removeDupsNoBuffer(intList);
		removeDupsWithBuffer(intList);
		intList.print();
	}
}
