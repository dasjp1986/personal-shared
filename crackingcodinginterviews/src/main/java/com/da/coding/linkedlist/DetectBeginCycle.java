package com.da.coding.linkedlist;

/**
 * 
 * @author dagraw1
 *
 *         Given a linked list, return the node where the cycle begins. If there
 *         is no cycle, return null.
 * 
 *         Try solving it using constant additional space.
 * 
 *         Example :
 * 
 *         Input :
 * 
 *         ______ | | \/ | 1 -> 2 -> 3 -> 4
 * 
 *         Return the node corresponding to node 3.
 */
public class DetectBeginCycle {

	public static ListNode detectCycle(ListNode a) {
		// detect cycle
		ListNode cycle = detect(a);
		if (cycle == null) {
			return null;
		}
		// calculate length of the cycle
		int length = calculateLength(cycle);
		// restart from the beginning and skip nodes equals to the cycle length
		// find the intersection point
		ListNode node = a;
		for (int i = 0; i < length; i++) {
			node = node.next;
		}

		ListNode temp = a;
		while (temp != node) {
			temp = temp.next;
			node = node.next;
		}

		return temp;

	}

	private static ListNode detect(ListNode a) {
		ListNode slow = a;
		ListNode fast = slow.next;

		while (fast != null && slow != fast) {
			slow = slow.next;
			fast = fast.next;
			if (fast != null) {
				fast = fast.next;
			}
		}

		return fast;
	}

	private static int calculateLength(ListNode node) {
		ListNode temp = node.next;
		int length = 1;
		while (temp != node) {
			temp = temp.next;
			length++;
		}

		return length;
	}

	public static void main(String[] args) {
		ListNode start = new ListNode(1);
		ListNode node = start;
		node.next = new ListNode(2);
		node = node.next;

		node.next = new ListNode(4);
		node = node.next;

		ListNode node3 = new ListNode(3);
		node.next = node3;
		node = node.next;

		node.next = new ListNode(5);
		node = node.next;

		node.next = new ListNode(6);
		node = node.next;
		node.next = node3;

		// printList(start);
		System.out.println("detected");
		System.out.println(detectCycle(start));
	}

	private static void printList(ListNode root) {
		ListNode node = root;
		while (node != null) {
			System.out.println(node.val + "->>");
			node = node.next;
		}

	}

}
