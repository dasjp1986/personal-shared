package com.da.coding.linkedlist;

public class SLNode<T> {
	private SLNode<T> next;
	private T value;

	public SLNode(T value) {
		super();
		this.value = value;
	}

	public SLNode<T> getNext() {
		return next;
	}

	public T getValue() {
		return value;
	}

	public void setNext(SLNode<T> next) {
		this.next = next;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "SLNode [value=" + value + "]";
	}

}
