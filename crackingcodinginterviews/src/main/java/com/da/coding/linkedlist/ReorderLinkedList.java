package com.da.coding.linkedlist;

/**
 * 
 * @author dagraw1
 *
 *         Given a singly linked list
 * 
 *         L: L0 → L1 → … → Ln-1 → Ln, reorder it to:
 * 
 *         L0 → Ln → L1 → Ln-1 → L2 → Ln-2 → … You must do this in-place without
 *         altering the nodes’ values.
 * 
 *         For example, Given {1,2,3,4}, reorder it to {1,4,2,3}.
 */
public class ReorderLinkedList {
	public static ListNode reorderList(ListNode A) {
		// move two pointers slow and fast and reach to middle
		ListNode slow = A;
		ListNode fast = slow.next;
		if (fast == null) {
			return A;
		}

		if (fast != null) {
			fast = fast.next;
		}
		while (fast != null) {
			slow = slow.next;
			fast = fast.next;
			if (fast != null) {
				fast = fast.next;
			}
		}
		// reverse the second half
		ListNode second = reverse(slow);

		// join first and second half
		ListNode first = A;

		while (first != null && second != null && first != second) {
			ListNode temp1 = first;
			ListNode temp2 = second;

			first = first.next;
			second = second.next;

			temp2.next = temp1.next;
			temp1.next = temp2;
		}

		return A;
	}

	private static ListNode reverse(ListNode start) {
		ListNode end = start;
		while (end.next != null) {
			end = end.next;
		}

		while (start != end) {
			ListNode temp = end.next;
			end.next = start;
			start = start.next;
			end.next.next = temp;
		}

		return end;
	}

	private static void printList(ListNode root) {
		ListNode node = root;
		while (node != null) {
			System.out.println(node.val + "->>");
			node = node.next;
		}

	}

	public static void main(String[] args) {
		ListNode start = new ListNode(1);
		ListNode node = start;
		node.next = new ListNode(2);
		node = node.next;

		node.next = new ListNode(3);
		node = node.next;

		node.next = new ListNode(4);
		node = node.next;

		// node.next = new ListNode(5);
		// node = node.next;

		printList(start);
		System.out.println("reordered");
		printList(reorderList(start));
	}
}
