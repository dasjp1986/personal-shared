package com.da.coding.linkedlist;

public class DLNode<T> {
	private T value;
	private DLNode<T> prev;
	private DLNode<T> next;

	public DLNode(T value) {
		super();
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public DLNode<T> getPrev() {
		return prev;
	}

	public void setPrev(DLNode<T> prev) {
		this.prev = prev;
	}

	public DLNode<T> getNext() {
		return next;
	}

	public void setNext(DLNode<T> next) {
		this.next = next;
	}

}
