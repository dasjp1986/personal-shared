package com.da.coding.linkedlist;

import com.da.coding.commons.SystemInputReaderUtil;
/**
 * 
 * @author dagraw1
 * Find the kth node from the last in given singly linked list
 * 
 * Ex- 
 * 1->2->3, k=2
 * returns: 2
 * 
 * 1->2->3, k=3
 * returns: 1
 * 
 * 1->2->3, k=4
 * returns: null
 *
 */
public class KthNodeFromLast {
	
	//Time Complexity- O(N), Space Complexity- O(1)
	public static <T> SLNode<T> getNode(int k, SinglyLinkedList<T> list) {
		SLNode<T> p1 = list.getHead();
		SLNode<T> p2 = p1;

		int i = 0;
		while (i++ < k) {
			if (p2 == null)
				return null;
			p2 = p2.getNext();
		}

		while (p2 != null) {
			p1 = p1.getNext();
			p2 = p2.getNext();
		}
		return p1;

	}

	public static void main(String[] args) {
		String[] inputArray = SystemInputReaderUtil.readString().split(" ");
		SinglyLinkedList<Integer> intList = new SinglyLinkedList<>();
		for (int i = 0; i < inputArray.length; i++) {
			intList.addTail(Integer.parseInt(inputArray[i]));
		}
		SLNode<Integer> node = getNode(Integer.parseInt(SystemInputReaderUtil.readString()), intList);
		System.out.println(node == null ? null : node.getValue());
	}
}
