package com.da.coding.linkedlist;

public class TWoListSums {
	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = new ListNode(0);
		ListNode lastNode = result;
		ListNode temp1 = l1;
		ListNode temp2 = l2;
		int sum = 0;
		int carryFwd = 0;

		while (temp1 != null || temp2 != null) {
			sum=0;
			if (temp1 != null) {
				sum += temp1.val;
				temp1 = temp1.next;
			}
			if (temp2 != null) {
				sum += temp2.val;
				temp2 = temp2.next;
			}
			sum += carryFwd;
			carryFwd = sum / 10;
			lastNode.next = new ListNode(sum%10);
			lastNode = lastNode.next;
		}
		if (carryFwd != 0) {
			lastNode.next = new ListNode(carryFwd);
		}
		return result.next;
	}

	public static void main(String[] args) {
		ListNode temp1 = new ListNode(2);
		ListNode l1 = temp1;

		ListNode temp = new ListNode(4);
		temp1.next = temp;
		temp1 = temp1.next;

		temp = new ListNode(3);
		temp1.next = temp;
		temp1 = temp1.next;

		ListNode temp2 = new ListNode(5);
		ListNode l2 = temp2;

		temp = new ListNode(6);
		temp2.next = temp;
		temp2 = temp2.next;

		temp = new ListNode(4);
		temp2.next = temp;
		temp2 = temp2.next;

		print(l1);
		print(l2);
		print(addTwoNumbers(l1, l2));
	}

	private static void print(ListNode node) {
		System.out.println();
		ListNode n = node;
		while (n != null) {
			System.out.print("->>" + n.val);
			n = n.next;
		}
	}
}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}

	@Override
	public String toString() {
		return val+"";
	}
	
	
}
