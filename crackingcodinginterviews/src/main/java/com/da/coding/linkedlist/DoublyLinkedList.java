package com.da.coding.linkedlist;

import com.da.coding.commons.SystemInputReaderUtil;

public class DoublyLinkedList<T> {
	private DLNode<T> head;

	// Time Complexity- O(1)
	public void addFront(T value) {
		DLNode<T> node = new DLNode<>(value);
		if (head == null) {
			head = node;
		} else {
			node.setNext(head);
			head.setPrev(node);
			head = node;
		}
		System.out.println("Added To The Front successfully : " + value);
	}

	// Time Complexity- O(N)
	public void addTail(T value) {
		DLNode<T> node = new DLNode<>(value);
		if (head == null) {
			head = node;
		} else {

			DLNode<T> temp = head;
			while (temp.getNext() != null) {
				temp = temp.getNext();
			}

			temp.setNext(node);
			node.setPrev(temp);

		}
		System.out.println("Added To The Tail successfully : " + value);
	}

	// Time Complexity- O(N)
	public boolean remove(T value) {

		DLNode<T> temp = head;

		while (temp != null) {
			if (temp.getValue().equals(value)) {
				if (temp == head) {
					head = temp.getNext();
					if (head != null)
						head.setPrev(null);
				} else {
					DLNode<T> prev = temp.getPrev();
					DLNode<T> next = temp.getNext();
					prev.setNext(next);
					if (next != null)
						next.setPrev(prev);
				}
				System.out.println("Removed successfully : " + value);
				return true;
			}
			temp = temp.getNext();
		}

		return false;
	}

	//Time Complexity- O(N)
	public boolean addAfter(T value, T kValue) {
		DLNode<T> temp = head;
		DLNode<T> node = new DLNode<>(value);

		while (temp != null) {
			if (temp.getValue().equals(kValue)) {
				node.setNext(temp.getNext());
				node.setPrev(temp);
				if (temp.getNext() != null) {
					temp.getNext().setPrev(node);
				}
				temp.setNext(node);
				System.out.println("Added To After successfully : " + value);
				return true;
			}
			temp = temp.getNext();
		}

		return false;
	}

	public void printInOrder() {
		DLNode<T> temp = head;

		System.out.println("\nPrint Linked List inorder...");
		while (temp != null) {
			System.out.print("->" + temp.getValue());
			temp = temp.getNext();
		}
	}

	public void printReverse() {
		DLNode<T> temp = head;

		System.out.println("\nPrint Linked List reversed...");
		while (temp.getNext() != null) {
			temp = temp.getNext();
		}
		while (temp != null) {
			System.out.print("->" + temp.getValue());
			temp = temp.getPrev();
		}
	}

	public static void main(String[] args) {
		String[] inputStrings = SystemInputReaderUtil.readString().split(" ");
		DoublyLinkedList<Integer> intList = new DoublyLinkedList<>();
		for (int i = 0; i < inputStrings.length; i++) {
			intList.addTail(Integer.parseInt(inputStrings[i]));
		}
		intList.printInOrder();
		intList.printReverse();

		System.out.println(intList.remove(5));
		intList.printInOrder();
		intList.printReverse();

		System.out.println(intList.remove(1));
		intList.printInOrder();
		intList.printReverse();

		System.out.println(intList.addAfter(10, 4));
		intList.printInOrder();
		intList.printReverse();

		System.out.println(intList.addAfter(11, 1));
		intList.printInOrder();
		intList.printReverse();
	}
}
