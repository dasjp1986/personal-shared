package com.da.coding.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Design and implement a data structure for LRU (Least Recently Used)
 *         cache. It should support the following operations: get and set.
 * 
 *         get(key) - Get the value (will always be positive) of the key if the
 *         key exists in the cache, otherwise return -1. set(key, value) - Set
 *         or insert the value if the key is not already present. When the cache
 *         reaches its capacity, it should invalidate the least recently used
 *         item before inserting the new item. The LRU Cache will be initialized
 *         with an integer corresponding to its capacity. Capacity indicates the
 *         maximum number of unique keys it can hold at a time.
 * 
 *         Definition of “least recently used” : An access to an item is defined
 *         as a get or a set operation of the item. “Least recently used” item
 *         is the one with the oldest access time.
 * 
 *         NOTE: If you are using any global variables, make sure to clear them
 *         in the constructor. Example :
 * 
 *         Input : capacity = 2 set(1, 10) set(5, 12) get(5) returns 12 get(1)
 *         returns 10 get(10) returns -1 set(6, 14) this pushes out key = 5 as
 *         LRU is full. get(5) returns -1
 * 
 */
public class LRUCacheFixed {

	Map<Integer, Node> lookup;
	Node head;
	Node last;
	int count;
	int capacity;

	public LRUCacheFixed(int capacity) {
		this.lookup = new HashMap<>();
		this.head = null;
		this.last = null;
		this.count = 0;
		this.capacity = capacity;
	}

	public int get(int key) {
		Node node = lookup.get(key);
		if (node == null) {
			return -1;
		}
		if (node != head) {
			Node prev = node.prev;
			Node next = node.next;

			prev.next = next;
			if (next != null)
				next.prev = prev;

			if (node == last) {
				last = last.prev;
			}

			node.next = head;
			node.prev = null;
			head.prev = node;
			head = node;
		}
		// print();
		return node.val;
	}

	public void set(int key, int value) {
		Node node = lookup.get(key);
		if (node == null) {
			node = new Node(key, value);
		} else {
			node.val = value;
			count--;
		}
		if (head == null) {
			head = node;
			last = node;
		} else {
			if (count == capacity) {
				lookup.remove(last.key);
				if (head == last) {
					head = node;
					last = node;
				} else {
					last = last.prev;
				}

				if (last != null)
					last.next = null;

				count--;
			}

			if (node != head) {
				Node prev = node.prev;
				Node next = node.next;

				if (prev != null) {
					prev.next = next;
				}

				if (next != null)
					next.prev = prev;

				if (node == last) {
					last = last.prev;
				}

				node.next = head;
				node.prev = null;
				head.prev = node;
				head = node;
			}
		}
		lookup.put(key, node);
		count++;

		// print();
	}

	public static void main(String[] args) {
//		LRUCacheFixed lru = new LRUCacheFixed(3);
//		lru.set(1, 1);
//		lru.set(2, 2);
//		lru.set(3, 3);
//		lru.set(1, 11);
//		lru.set(2, 22);
//		lru.set(3, 33);
//		lru.set(4, 4);
//		lru.get(3);

		int ops = 93;
		int capacity = 4;
		LRUCacheFixed lru = new LRUCacheFixed(capacity);
		String[] str = "S 5 13 S 9 6 S 4 1 G 4 S 6 1 S 8 11 G 13 G 1 S 12 12 G 10 S 15 13 S 2 13 S 7 5 S 10 3 G 6 G 10 S 15 14 S 5 12 G 5 G 7 G 15 G 5 G 6 G 10 S 7 13 G 14 S 8 9 G 4 S 6 11 G 9 S 6 12 G 3"
				.split(" ");
		for (int i = 0; i < str.length; i++) {
			if (str[i].equals("S")) {
				lru.set(Integer.parseInt(str[i + 1]), Integer.parseInt(str[i + 2]));
				i = i + 2;
			}
			if (str[i].equals("G")) {
				System.out.println(lru.get(Integer.parseInt(str[i + 1])));
				i = i + 1;
			}
		}
	}

	private void print() {
		Node temp = head;
		Node last = null;
		System.out.println(" forward List : ");
		while (temp != null) {
			System.out.print(temp + " ");
			last = temp;
			temp = temp.next;
		}
//		temp = last;
//		System.out.println(" backward List : ");
//		while (temp != null) {
//			System.out.print(temp + " ");
//			temp = temp.prev;
//		}
		System.out.println();
	}
}

class Node {
	Node prev;
	int val;
	int key;
	Node next;

	public Node(int key, int val) {
		this.key = key;
		this.val = val;
	}

	@Override
	public String toString() {
		return "Node [key=" + key + "]";
	}

}
