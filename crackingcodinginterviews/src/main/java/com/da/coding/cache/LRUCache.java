package com.da.coding.cache;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import com.da.coding.linkedlist.DLNode;

/**
 * 
 * @author dagraw1
 *
 *         THERE ARE ISSUES WITH CONCURRENCY, AND REQUIRES A FIX
 * @param <T>
 */
public class LRUCache<T> {
	Map<T, DLNode<T>> lookup;
	volatile DLNode<T> head;
	volatile DLNode<T> tail;
	int cacheSize;
	volatile int counter = 0;

	public LRUCache(int cacheSize) {
		this.cacheSize = cacheSize;
		lookup = new ConcurrentHashMap<>();
	}

	public void fetch(T key) {
		String threadName = Thread.currentThread().getName();
		DLNode<T> extValue = lookup.get(key);
		if (extValue == null) {
			System.out.println("thread:" + threadName + " cache miss: " + key);
			network();
			System.out.println("thread:" + threadName + " network fetch completed: " + key);
			synchronized (key) {
				System.out.println("thread:" + threadName + " entered syncronization for add key: " + key);
				extValue = addLinkedList(key);
				lookup.put(key, extValue);
			}
		} else {
			System.out.println("thread:" + threadName + " cached key returned: " + key);
			synchronized (extValue.getValue()) {
				updateLinkedList(extValue);
			}

		}
	}

	private void network() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void updateLinkedList(DLNode<T> extValue) {
		String threadName = Thread.currentThread().getName();
		System.out.println("thread:" + threadName + " entered syncronization for update key: " + extValue.getValue());
		DLNode<T> node = lookup.get(extValue.getValue());
		if (node == null) {
			System.out.println(
					"thread:" + threadName + " discontinue syncronization for update key: " + extValue.getValue());
			addLinkedList(extValue.getValue());
		} else if (extValue != head) {
			System.out.println(
					"thread:" + threadName + " continue syncronization for update key: " + extValue.getValue());
			DLNode<T> next = extValue.getNext();
			DLNode<T> prev = extValue.getPrev();
			if (prev != null)
				prev.setNext(next);
			if (next != null)
				next.setPrev(prev);

			head.setPrev(extValue);
			extValue.setNext(head);
			head = extValue;
			tail = prev;
		}
	}

	private DLNode<T> addLinkedList(T value) {
		String threadName = Thread.currentThread().getName();
		DLNode<T> node = lookup.get(value);
		if (node == null) {
			System.out.println("thread:" + threadName + " continued syncronization for add key: " + value);
			node = new DLNode<T>(value);
			counter++;
			if (head == null) {
				head = node;
				tail = node;
			} else {
				node.setNext(head);
				head.setPrev(node);
				head = node;
				if (counter > cacheSize) {
					lookup.remove(tail.getValue());
					DLNode<T> prev = tail.getPrev();
					tail.setPrev(null);
					if (prev != null)
						prev.setNext(null);
					tail = prev;
				}

			}
		} else {
			System.out.println("thread:" + threadName + " discontinue syncronization for add key: " + value);
			updateLinkedList(node);
		}
		return node;
	}

	public static void main(String[] args) {
		LRUCache<Integer> cache = new LRUCache<>(3);
		ThreadJob job1 = new ThreadJob(cache);
		ThreadJob job2 = new ThreadJob(cache);

		new Thread(job1, "1").start();
		new Thread(job2, "2").start();
		new Thread(job1, "3").start();
		new Thread(job2, "4").start();
	}

	public void printLinkedList() {
		DLNode<T> node = head;
		System.out.println();
		while (node != null) {
			System.out.print(node.getValue() + "->>");
			node = node.getNext();
		}
	}

}

class ThreadJob implements Runnable {
	LRUCache<Integer> cache;

	public ThreadJob(LRUCache<Integer> cache) {
		this.cache = cache;
	}

	@Override
	public void run() {
		int n = 9;
		Random random = new Random();
		for (int i = 1; i < n; i++) {
			cache.fetch(random.nextInt(n));
		}
		cache.printLinkedList();
	}

}
