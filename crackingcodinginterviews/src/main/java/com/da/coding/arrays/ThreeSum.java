package com.da.coding.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ThreeSum {
	public int threeSumClosest(List<Integer> A, int B) {
		int low, high, mid;
		Collections.sort(A);
		int n = A.size();
		int res = A.get(0) + A.get(1) + A.get(2);

		if (n == 3) {
			return res;
		}

		int sum;

		for (low = 0; low < n - 2; low++) {
			mid = low + 1;
			high = n - 1;
			int num = B - A.get(low);

			while (mid < high) {

				sum = A.get(mid) + A.get(high);

				if (sum == num)
					return B;
				else if (sum < num) {
					mid++;
				} else {
					high--;
				}

				int diff = Math.abs(num-sum);
				int otherDiff = Math.abs(res - B);

				if (diff < otherDiff)
					res = sum + A.get(low);

			}

		}

		return res;

	}

	public static void main(String[] args) {
		ThreeSum ts = new ThreeSum();
		Integer[] array = { -5, 1, 4, -7, 10, -7, 0, 7, 3, 0 };
		System.out.println(ts.threeSumClosest(Arrays.asList(array), 2));
	}
}
