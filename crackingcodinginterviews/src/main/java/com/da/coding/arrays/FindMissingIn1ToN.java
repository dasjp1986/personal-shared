package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         You are given n-1 elements(huge numbers) in 1 to N, find the missing
 *         number
 * 
 */
public class FindMissingIn1ToN {

	//problem with overflow due to large sum
	public static int findUsingSum(int[] input) {
		int n = input.length + 1;
		int expectedSum = n * (n + 1) / 2;
		int actualSum = 0;
		for (int i = 0; i < n - 1; i++) {
			actualSum += input[i];
		}
		return expectedSum - actualSum;
	}

	public static int findUsingXor(int[] input) {
		int allXor = 0;
		int givenXor = 0;
		for (int i = 0; i < input.length; i++) {
			allXor = allXor ^ (i + 1);
			givenXor = givenXor ^ input[i];
		}
		allXor = allXor ^ (input.length + 1);
		return allXor ^ givenXor;
	}

	public static void main(String[] args) {
		int[] input = { 4, 2, 5, 6, 3 };
		System.out.println(findUsingSum(input));
		System.out.println(findUsingXor(input));
	}
}
