package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given an array containing only 0s and 1s, find the largest subarray
 *         which contain equal no of 0s and 1s. Expected time complexity is O(n)
 * 
 *         Examples:
 * 
 *         Input: arr[] = {1, 0, 1, 1, 1, 0, 0} Output: 1 to 6 (Starting and
 *         Ending indexes of output subarray)
 * 
 *         Input: arr[] = {1, 1, 1, 1} Output: No such subarray
 * 
 *         Input: arr[] = {0, 0, 1, 1, 0} Output: 0 to 3 Or 1 to 4
 */
public class LargestSubArrayWith0and1s {
	//Time complexity- O(n)
	public void subArray(int[] a) {
		int start = 0, end = a.length - 1;
		int count = 0;

		for (int i = 0; i < a.length; i++) {
			count = count + (a[i] == 0 ? -1 : 1);
		}

		if (Math.abs(count) == a.length) {
			System.out.println("No Such subArray exists");
			return;
		}

		if (count == 0) {
			System.out.println("SubArray start,end " + start + "," + end);
		} else {
			while (start <= end && count != 0) {
				if (count > 0) {
					// need to remove extra 1's
					if (a[start] == 1) {

						start++;
					} else if (a[end] == 1) {
						end--;
					}
					count--;
				} else {
					// need to remove extra 0's
					if (a[start] == 0) {
						start++;
					} else if (a[end] == 0) {
						end--;
					}
					count++;
				}

			}
			System.out.println("SubArray start,end " + start + "," + end);
		}
	}

	public static void main(String[] args) {
		LargestSubArrayWith0and1s subArray = new LargestSubArrayWith0and1s();
		int[] a1 = { 1, 1, 1, 0, 0, 0 };
		subArray.subArray(a1);

		int[] a2 = { 1, 1, 1, 0, 0 };
		subArray.subArray(a2);

		int[] a3 = { 1, 0, 1, 1, 1, 0, 0 };
		subArray.subArray(a3);

		int[] a4 = { 1, 1, 1, 1 };
		subArray.subArray(a4);

		int[] a5 = { 0, 0, 1, 1, 0 };
		subArray.subArray(a5);

		int[] a6 = { 1, 0, 0, 1, 0, 1 };
		subArray.subArray(a6);
	}
}
