package com.da.coding.arrays;

import java.util.Arrays;
import java.util.List;

public class RotationPoint {
	public int findIndex(List<Integer> a) {
		if (a.size() == 1) {
			return 0;
		} else if (a.size() == 2 && a.get(1) < a.get(0)) {
			return 1;
		}
		return pivot(a, 0, a.size() - 1);
	}

	private int pivot(List<Integer> a, int start, int end) {
		if (start > end) {
			return 0;
		}
		int mid = (start + end) / 2;
		if (mid == 0) {
			return 0;
		}

		if (a.get(mid - 1) > a.get(mid)) {
			return mid;
		}
		if (a.get(mid) < a.get(start)) {
			return pivot(a, start, mid);
		} else {
			return pivot(a, mid + 1, end);
		}
	}

	public static void main(String[] args) {
		Integer[] array = { 1, 0 };
		RotationPoint point = new RotationPoint();
		System.out.println(point.findIndex(Arrays.asList(array)));
	}
}
