package com.da.coding.arrays;

import java.util.Arrays;

public class ArrayRotation {
	void leftRotate(int arr[], int d, int n) {
		int i, j, k, temp;
		for (i = 0; i < gcd(d, n); i++) {
			/* move i-th values of blocks */
			temp = arr[i];
			j = i;
			while (true) {
				k = j + d;
				if (k >= n)
					k = k - n;
				if (k == i)
					break;
				arr[j] = arr[k];
				j = k;
			}
			arr[j] = temp;
		}
	}

	int gcd(int a, int b) {
		if (b == 0)
			return a;
		else
			return gcd(b, a % b);
	}
	
	public static void main(String[] args) {
		ArrayRotation rotate= new ArrayRotation();
		int arr[] = { 1, 2, 3, 4}; 
        rotate.leftRotate(arr, 1, arr.length); 
        System.out.println(Arrays.toString(arr));
	}

}
