package com.da.coding.arrays;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given a set of digits (A) in sorted order, find how many numbers of
 *         length B are possible whose value is less than number C.
 * 
 *         NOTE: All numbers can only have digits from the given set. Examples:
 * 
 *         Input: 0 1 5 1 2 Output: 2 (0 and 1 are possible)
 * 
 *         Input: 0 1 2 5 2 21 Output: 5 (10, 11, 12, 15, 20 are possible)
 *         Constraints:
 * 
 *         1 <= B <= 9, 0 <= C <= 1e9 & 0 <= A[i] <= 9
 */
public class NumbersWithLengthNandSumLessThanK {

	//NEED A FIX, GETTING TLE ERROS, PROBLEM IS SIMILAR TO PREFIX SUM
	//EXPECTED COMPLEXITY- O(N)
	public static int solve(List<Integer> A, int B, int C) {
		int count = 0;
		for (int i = 0; i < A.size(); i++) {
			if (A.get(i) == 0) {
				if (B == 1)
					count++;
			} else {
				if (A.get(i) < C)
					count += counts(A, B, C, 1, A.get(i));
			}
		}
		return count;
	}

	private static int counts(List<Integer> list, int length, int targetSum, int index, int runningSum) {
		if (index == length) {
			return 1;
		}
		int count = 0;
		for (int i = 0; i < list.size(); i++) {
			int newSum = runningSum * 10 + list.get(i);
			if (newSum < targetSum) {
				count = count + counts(list, length, targetSum, index + 1, newSum);
			} else {
				break;
			}
		}

		return count;

	}

	public static void main(String[] args) {
		Integer[] array = { 0, 1, 2, 5 };
		System.out.println(solve(Arrays.asList(array), 3, 599));
	}

}
