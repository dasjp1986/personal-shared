package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 * 
 *         Given an array arr[], find the maximum j – i such that arr[j] >
 *         arr[i].
 * 
 *         Examples :
 * 
 *         Input: {34, 8, 10, 3, 2, 80, 30, 33, 1} Output: 6 (j = 7, i = 1)
 * 
 *         Input: {9, 2, 3, 4, 5, 6, 7, 8, 18, 0} Output: 8 ( j = 8, i = 0)
 * 
 *         Input: {1, 2, 3, 4, 5, 6} Output: 5 (j = 5, i = 0)
 * 
 *         Input: {6, 5, 4, 3, 2, 1} Output: -1
 */
public class MaxOfIAndJ {
	//Time Complexity- O(n), Space Complexity- O(2n)
	public static int max(int[] a) {

		int n = a.length;
		int[] leftIndex = new int[n];
		int[] rightIndex = new int[n];

		leftIndex[0] = 0;
		rightIndex[n - 1] = n - 1;

		for (int i = 1; i < n; i++) {
			if (a[i] < a[leftIndex[i - 1]]) {
				leftIndex[i] = i;
			} else {
				leftIndex[i] = leftIndex[i - 1];
			}
		}

		for (int i = n - 2; i >= 0; i--) {
			if (a[i] > a[rightIndex[i + 1]]) {
				rightIndex[i] = i;
			} else {
				rightIndex[i] = rightIndex[i + 1];
			}
		}

		int i = 0, j = 0;

		int res = -1;
		while (i < n && j < n) {
			if (a[leftIndex[i]] < a[rightIndex[j]]) {
				res = Math.max(res, j - i);
				j++;
			} else {
				i++;
			}
		}

		return res;
	}

	public static void main(String[] args) {

		System.out.println(max(new int[] {34, 8, 10, 3, 2, 80, 30, 33, 1}));
		System.out.println(max(new int[] {9, 2, 3, 4, 5, 6, 7, 8, 18, 0}));
		System.out.println(max(new int[] {1, 2, 3, 4, 5, 6}));
		System.out.println(max(new int[] {6, 5, 4, 3, 2, 1}));
	}
}
