package com.da.coding.arrays;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Inversion Count for an array indicates – how far (or close) the array
 *         is from being sorted. If array is already sorted then inversion count
 *         is 0. If array is sorted in reverse order that inversion count is the
 *         maximum. Formally speaking, two elements a[i] and a[j] form an
 *         inversion if a[i] > a[j] and i < j
 * 
 *         Example: The sequence 2, 4, 1, 3, 5 has three inversions (2, 1), (4,
 *         1), (4, 3).
 */
public class CountInversions {
	int count = 0;

	public int count(int[] array) {
		int[] sortedArray = merge(array, 0, array.length - 1);
		System.out.println(Arrays.toString(sortedArray));
		return count;
	}

	private int[] merge(int[] array, int start, int end) {
		if (start == end) {
			return new int[] { array[start] };
		}

		int mid = (start + end) / 2;
		int[] left = merge(array, start, mid);
		int[] right = merge(array, mid + 1, end);

		int[] result = new int[left.length + right.length];

		int i = 0, j = 0, k = 0;
		while (i < left.length && j < right.length) {
			if (left[i] <= right[j]) {
				result[k] = left[i];
				i++;
			} else {
				count = count + mid - i + 1;
				result[k] = right[j];
				j++;
			}
			k++;
		}

		while (i < left.length) {
			result[k++] = left[i++];
		}

		while (j < right.length) {
			result[k++] = right[j++];
		}

		return result;

	}
	
	public static void main(String[] args) {
		CountInversions inversions= new CountInversions();
		int[] input= {2,4,1,3,5};
		System.out.println(inversions.count(input));
	}
}
