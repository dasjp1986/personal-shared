package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of integers and a number k, write a function that
 *         returns true if given array can be divided into pairs such that sum
 *         of every pair is divisible by k.
 * 
 *         Examples:
 * 
 *         Input: arr[] = {9, 7, 5, 3}, k = 6 Output: True We can divide array
 *         into (9, 3) and (7, 5). Sum of both of these pairs is a multiple of
 *         6.
 * 
 *         Input: arr[] = {92, 75, 65, 48, 45, 35}, k = 10 Output: True We can
 *         divide array into (92, 48), (75, 65) and (45, 35). Sum of all these
 *         pairs is a multiple of 10.
 * 
 *         Input: arr[] = {91, 74, 66, 48}, k = 10 Output: False
 */
public class ArrayPairsDivisbleByK {
	//Time and Space Complexity- O(n)
	public boolean isAllPairsDivisible(int[] array, int k){
		if(array.length%2!=0){
			return false;
		}
		Map<Integer, Boolean> map= new HashMap<>();
		for (int i = 0; i < array.length; i++) {
			map.put(array[i]%k, false);
		}
		for (int i = 0; i < array.length; i++) {
			Integer look= k-array[i]%k;
			Boolean available= map.get(look);
			if(available==null){
				return false;
			}else{
				map.put(look, true);
			}
		}
		for(Boolean isPair:map.values()){
			if(!isPair){
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		ArrayPairsDivisbleByK divisbleByK= new ArrayPairsDivisbleByK();
		int[] array1={9, 7, 5, 3};
		System.out.println(divisbleByK.isAllPairsDivisible(array1, 6));
		
		int[] array2={92, 75, 65, 48, 45, 35};
		System.out.println(divisbleByK.isAllPairsDivisible(array2, 10));
		
		int[] array3={91, 74, 66, 48};
		System.out.println(divisbleByK.isAllPairsDivisible(array3, 10));
		
		int[] array4={10,10,10,10};
		System.out.println(divisbleByK.isAllPairsDivisible(array4, 20));
	}
}
