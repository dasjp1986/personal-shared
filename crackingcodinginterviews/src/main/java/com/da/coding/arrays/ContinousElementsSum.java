package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 * 
 *         Given an array of integers containing positive and negative numbers,
 *         find if there are continuous elements are present whose sum is equal
 *         to given the Sum.
 * 
 *         You can not skip an element in between
 * 
 *         Given sum= 5 Ex- {1,2,-1,5,-2}- true Ex- {0,1,3,6,4}- false (1,4) is
 *         present but they are not continuous
 * 
 *         Follow up question, return count of such combinations Given sum= 8
 *         Ex- {10,2,1,-1,-1,7,1,2}- 2
 */
public class ContinousElementsSum {
	static int counter = 0;
	static Boolean[][] cache;

	public Boolean findSum(Integer[] array, int start, int end, int sum) {

		if (start > end) {
			return false;
		}

		if (cache[start][end] != null) {
			return cache[start][end];
		}

		counter++;

		int total = 0;
		boolean result;
		for (int i = start; i <= end; i++) {
			total += array[i];
		}
		if (total == sum) {
			result = true;
		} else {
			result = findSum(array, start, end - 1, sum) || findSum(array, start + 1, end, sum);
		}

		cache[start][end] = result;
		return result;
	}

	// Time Complexity- O(N)
	// Space Complexity- O(N)
	public int countOfSum(Integer[] array, int targetSum) {
		int counter = 0;
		Map<Integer, Boolean> sumMap = new HashMap<>();
		sumMap.put(array[0], true);
		for (int i = 1; i < array.length; i++) {
			array[i] = array[i] + array[i - 1];
			Boolean isPresent = sumMap.get(array[i]);
			if (isPresent == null) {
				sumMap.put(array[i], true);
			}
		}
		for (int i = 0; i < array.length; i++) {
			int diff = array[i] - targetSum;
			if (diff == 0) {
				counter++;
			} else if (sumMap.get(diff) != null) {
				counter++;
			}
		}
		return counter;
	}

	public static void main(String[] args) {
		ContinousElementsSum continousElementsSum = new ContinousElementsSum();
		// int[] array1 = { 1, 2, -1, 5, -2 };
		// cache = new Boolean[array1.length][array1.length];
		// System.out.println(continousElementsSum.findSum(array1, 0,
		// array1.length - 1, 5));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println("counts : " +
		// continousElementsSum.countOfSum(array1, 5));
		//
		// System.out.println("----------------------------------------------------------------");
		// int[] array2 = { 0, 1, 3, 6, 4 };
		// cache = new Boolean[array2.length][array2.length];
		// System.out.println(continousElementsSum.findSum(array2, 0,
		// array2.length - 1, 5));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println("counts : " +
		// continousElementsSum.countOfSum(array2, 5));
		//
		// System.out.println("----------------------------------------------------------------");
		// int[] array3 = { 1, 2, 3, -4, 4 };
		// cache = new Boolean[array3.length][array3.length];
		// System.out.println(continousElementsSum.findSum(array3, 0,
		// array3.length - 1, 5));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println("counts : " +
		// continousElementsSum.countOfSum(array3, 5));
		//
		// System.out.println("----------------------------------------------------------------");
		// int[] array4 = { 1, 2, -1, 1, 4 };
		// cache = new Boolean[array4.length][array4.length];
		// System.out.println(continousElementsSum.findSum(array4, 0,
		// array4.length - 1, 5));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println("counts : " +
		// continousElementsSum.countOfSum(array4, 5));

		System.out.println("----------------------------------------------------------------");
		Integer[] array5 = { 1, 2, 1, -2, 3 };
		cache = new Boolean[array5.length][array5.length];
		System.out.println(continousElementsSum.findSum(array5, 0, array5.length - 1, 5));
		System.out.println("recursed..." + counter);
		counter = 0;
		System.out.println("counts : " + continousElementsSum.countOfSum(array5, 5));

		// System.out.println("----------------------------------------------------------------");
		// int[] array6 = { 1, 2, 3, 4, -7 };
		// cache = new Boolean[array6.length][array6.length];
		// System.out.println(continousElementsSum.findSum(array6, 0,
		// array6.length - 1, 5));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println("counts : " +
		// continousElementsSum.countOfSum(array6, 5));
		//
		// System.out.println("----------------------------------------------------------------");
		// int[] array7 = { 15, 2, 6, 7, 5 };
		// cache = new Boolean[array7.length][array7.length];
		// System.out.println(continousElementsSum.findSum(array7, 0,
		// array7.length - 1, 5));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println("counts : " +
		// continousElementsSum.countOfSum(array7, 5));

		System.out.println("----------------------------------------------------------------");
		Integer[] array8 = { 5, 2, 6, 7, 15 };
		cache = new Boolean[array8.length][array8.length];
		System.out.println(continousElementsSum.findSum(array8, 0, array8.length - 1, 5));
		System.out.println("recursed..." + counter);
		counter = 0;
		System.out.println("counts : " + continousElementsSum.countOfSum(array8, 5));

		System.out.println("----------------------------------------------------------------");
		Integer[] array9 = { 5, 5, 5, 5, 5 };
		cache = new Boolean[array9.length][array9.length];
		System.out.println(continousElementsSum.findSum(array9, 0, array9.length - 1, 5));
		System.out.println("recursed..." + counter);
		counter = 0;
		System.out.println("counts : " + continousElementsSum.countOfSum(array9, 5));
		

		// System.out.println("----------------------------------------------------------------");
		// int[] bigArray = new int[Short.MAX_VALUE];
		// int size = 20;
		// cache = new Boolean[bigArray.length][size];
		// Random random = new Random();
		// for (int i = 0; i < size; i++) {
		// bigArray[i] = random.nextInt(size);
		// }
		// System.out.println(continousElementsSum.findSum(bigArray, 0, size -
		// 1, 50));
		// System.out.println("recursed..." + counter);
		// counter = 0;
		// System.out.println(continousElementsSum.countOfSum(bigArray, 50));
	}
}
