package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of integers, find the length of the longest
 *         sub-sequence such that elements in the subsequence are consecutive
 *         integers, the consecutive numbers can be in any order.
 * 
 *         Examples
 * 
 *         Input: arr[] = {1, 9, 3, 10, 4, 20, 2}; Output: 4 The subsequence 1,
 *         3, 4, 2 is the longest subsequence of consecutive elements
 * 
 *         Input: arr[] = {36, 41, 56, 35, 44, 33, 34, 92, 43, 32, 42} Output: 5
 *         The subsequence 36, 35, 33, 34, 32 is the longest subsequence of
 *         consecutive elements.
 */
public class LongestConsecutiveSubSeq {
	//Time and Space complexity- O(n)
	public int longest(int[] array){
		Map<Integer, Boolean> map= new HashMap<>();
		for (int i = 0; i < array.length; i++) {
			map.put(array[i], true);
		}
		int max=0, count=0;
		for (int i = 0; i < array.length; i++) {
			if(map.get(array[i]-1)==null){
				count=0;
				int k=0;
				while (map.get(array[i]+k)!=null) {
					count++;
					k++;
				}
				
				if(count>max){
					max= count;
				}
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
		LongestConsecutiveSubSeq css= new LongestConsecutiveSubSeq();
		int[] array1={1, 9, 3, 10, 4, 20, 2};
		System.out.println(css.longest(array1));
		int[] array2={36, 41, 56, 35, 44, 33, 34, 92, 43, 32, 42};
		System.out.println(css.longest(array2));
	}
}
