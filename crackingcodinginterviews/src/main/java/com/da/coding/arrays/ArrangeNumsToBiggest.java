package com.da.coding.arrays;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of numbers, arrange them in a way that yields the
 *         largest value. For example, if the given numbers are {54, 546, 548,
 *         60}, the arrangement 6054854654 gives the largest value. And if the
 *         given numbers are {1, 34, 3, 98, 9, 76, 45, 4}, then the arrangement
 *         998764543431 gives the largest value.
 */
public class ArrangeNumsToBiggest {
	public static String largest(Integer[] array) {
		StringBuilder builder = new StringBuilder();

		Arrays.sort(array, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				Integer first = Integer.parseInt(o1 + "" + o2);
				Integer second = Integer.parseInt(o2 + "" + o1);
				return second.compareTo(first);
			}
		});

		for (int i = 0; i < array.length; i++) {
			builder.append(array[i]);
		}
		return builder.toString();
	}

	public static void main(String[] args) {
		Integer[] array = {54,546,548,60};
		System.out.println(largest(array));
	}
}
