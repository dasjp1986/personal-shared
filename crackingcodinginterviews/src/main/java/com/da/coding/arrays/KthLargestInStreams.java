package com.da.coding.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *         Given an input stream of n integers the task is to insert integers to
 *         stream and print the kth largest element in the stream at each
 *         insertion.
 * 
 *         Example:
 * 
 *         Input: stream[] = {10, 20, 11, 70, 50, 40, 100, 5, ...} k = 3
 * 
 *         Output: {-1, -1, 10, 11, 20, 40, 50, 50, ...}
 */
public class KthLargestInStreams {
	int[] heap;

	//Time Complexity- O(k+logk)
	public KthLargestInStreams(int k) {
		this.heap = new int[k];
		buildHeap();
	}

	private void buildHeap() {
		for (int i = 0; i < heap.length; i++) {
			this.heap[i] = -1;
		}
	}

	private void heapify(int i) {
		int index = -1;
		if(i>=heap.length/2){
			return;
		}
		if (heap[i] > heap[2 * i + 1] || heap[i] > heap[2 * i + 2]) {
			if (heap[2 * i + 1] < heap[2 * i + 2]) {
				index = 2 * i + 1;
			} else {
				index = 2 * i + 2;
			}
			swap(i, index);
			heapify(index);
		}
	}

	private void swap(int i, int j) {
		int temp = heap[i];
		heap[i] = heap[j];
		heap[j] = temp;
	}

	public int kthLargest(int value) {
		if (heap[0] >= value) {
			return heap[0];
		}
		heap[0] = value;
		heapify(0);
		return heap[0];
	}

	public static void main(String[] args) throws IOException {
		KthLargestInStreams streams = new KthLargestInStreams(3);
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int value = 0;

		while ((value = Integer.parseInt(reader.readLine())) != -1) {
			System.out.println(streams.kthLargest(value));
		}
		reader.close();
	}
}
