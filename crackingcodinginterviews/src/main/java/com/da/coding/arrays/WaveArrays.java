package com.da.coding.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of integers, sort the array into a wave like array and
 *         return it, In other words, arrange the elements into a sequence such
 *         that a1 >= a2 <= a3 >= a4 <= a5.....
 * 
 *         Example
 * 
 *         Given [1, 2, 3, 4]
 * 
 *         One possible answer : [2, 1, 4, 3] Another possible answer : [4, 1,
 *         3, 2] NOTE : If there are multiple answers possible, return the one
 *         thats lexicographically smallest. So, in example case, you will
 *         return [2,
 */
public class WaveArrays {
	//Time complexity- O(nLogn)
	public static List<Integer> wave(List<Integer> A) {
		// sort the list
		Collections.sort(A);
		// arrange the numbers at alternating places
		for (int i = 1; i < A.size(); i += 2) {
			swap(A, i, i - 1);
		}

		return A;
	}

	private static void swap(List<Integer> A, int prev, int current) {
		Integer temp = A.get(prev);
		A.set(prev, A.get(current));
		A.set(current, temp);
	}

	public static void main(String[] args) {
		Integer[] array = { 4, 3, 7, 8, 6, 2, 1 };
		System.out.println(wave(Arrays.asList(array)));
	}
}
