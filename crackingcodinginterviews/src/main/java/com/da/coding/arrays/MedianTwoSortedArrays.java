package com.da.coding.arrays;

/**
 * 
 * @author dagraw1 Find median of two given sorted arrays
 * 
 *         Ex- [1,3] and [2] Ans- 2
 * 
 *         Ex- [1,2] and [3,4] Ans- (2+3)/2= 2.5
 */
public class MedianTwoSortedArrays {
	//Time Complexity O(Log(m+n))
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {

		int totalLength = nums1.length + nums2.length;
		if (nums1.length <= nums2.length) {
			return findMedian(nums1, nums2, totalLength, 0, nums1.length);
		} else {
			return findMedian(nums2, nums1, totalLength, 0, nums2.length);
		}

	}

	private double findMedian(int[] shorterArray, int[] largerArray, int totalLength, int start, int end) {
		int partShortArray = (start + end) / 2;
		int partLargeArray = (totalLength + 1) / 2 - partShortArray;

		int leftShort = partShortArray <= 0 ? Integer.MIN_VALUE : shorterArray[partShortArray-1];
		int leftLarge = partLargeArray <= 0 ? Integer.MIN_VALUE : largerArray[partLargeArray - 1];
		int rightShort = partShortArray > shorterArray.length - 1 ? Integer.MAX_VALUE
				: shorterArray[partShortArray];
		int rightLarge = partLargeArray > largerArray.length - 1 ? Integer.MAX_VALUE : largerArray[partLargeArray];

		double median = 0.0;
		if (leftShort <= rightLarge && leftLarge <= rightShort) {
			if (totalLength % 2 == 1) {
				// odd length
				median = Math.max(leftShort, leftLarge);
			} else {
				median = (Math.max(leftShort, leftLarge) + Math.min(rightShort, rightLarge)) / 2.0;
			}
		} else if (leftShort > rightLarge && leftLarge <= rightShort) {
			median = findMedian(shorterArray, largerArray, totalLength, start, end - 1);
		} else {
			median = findMedian(shorterArray, largerArray, totalLength, start + 1, end);
		}
		return median;
	}

	public static void main(String[] args) {
		int[] nums1 = {1,2};
		int[] nums2 = {3,4,5,6};
		MedianTwoSortedArrays problem = new MedianTwoSortedArrays();
		System.out.println("Median : " + problem.findMedianSortedArrays(nums1, nums2));
	}
}
