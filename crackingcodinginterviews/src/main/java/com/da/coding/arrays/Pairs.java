package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;

public class Pairs {
	static int numberOfPairs(int[] a, long k) {
		int count = 0;

		Map<Integer, Boolean> remains = new HashMap<>();

		for (int i = 0; i < a.length; i++) {
			Boolean isAvailable = remains.get(a[i]);
			if (isAvailable == null) {
				remains.put((int) (k - a[i]), true);
			} else if(isAvailable){
				count++;
				remains.put(a[i], false);
			}
		}

		return count;

	}

	public static void main(String[] args) {
		int[] a = {6,6,3,9,3,5,1,6};
		System.out.println(numberOfPairs(a, 12));
	}
}
