package com.da.coding.arrays;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author dagraw1
 *
 *There is a given binary matrix, we need to find if there exists any rectangle or square in the given matrix whose all four corners are equal to 1.

Examples:

Input :
mat[][] = { 1 0 0 1 0
            0 0 1 0 1
            0 0 0 1 0
            1 0 1 0 1}
Output : Yes
as there exists-
1 0 1
0 1 0
1 0 1

 */
public class RectangleInMatrix {
	//Time Complexity- O(M*N*N)
	public boolean isRectangle(int[][] matrix) {
		int rows= matrix.length;
		int cols= matrix[0].length;
		
		Map<Integer, Set<Integer>> pairs= new HashMap<>();
		
		for (int row = 0; row < rows; row++) {
			for (int col1 = 0; col1 < cols-1; col1++) {
				for (int col2 = col1+1; col2 < cols; col2++) {
					if(matrix[row][col1]==1 && matrix[row][col2]==1) {
						Set<Integer> set = pairs.get(col1);
						if(set==null) {
							set= new HashSet<>();
							pairs.put(col1, set);
						}else {
							if(set.contains(col2)) {
								return true;
							}
						}
						set.add(col2);
					}
				}
				
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		int[][] matrix= {{1,0,1,0},{0,1,0,1},{1,1,0,0},{0,1,0,0}};
		RectangleInMatrix rectangleInMatrix= new RectangleInMatrix();
		System.out.println(rectangleInMatrix.isRectangle(matrix));
	}
}
