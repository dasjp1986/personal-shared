package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given a stream of characters consisting only '0' and '1', print the
 *         last index of the '1' present in it. Input stream may either be
 *         sorted in decreasing order or increasing order. If not present than
 *         print "-1".
 * 
 *         Input: First line of the input contains the number of test cases (T)
 *         ,T lines follow each containing a stream of characters.
 * 
 *         Output: Corresponding to every test case , output the last index of 1
 *         present.
 * 
 *         Constraints: 1<=T<=110 ; 1<=sizeofstream<=100
 * 
 * 
 *         Example: Input: 2 00001 1 Output: 4 0
 */
public class LastOne {
	//Time Complexity- Log(n)
	public int lastIndex(int[] a) {
		if (a[a.length - 1] == 1) {
			return a.length - 1;
		} else if (a[0] == 0) {
			return -1;
		} else {
			return binSearch(a, 0, a.length, -1);
		}
	}

	private int binSearch(int[] a, int start, int end, int lastIndex) {
		if (start <= end) {
			int mid = (start + end) / 2;
			if (a[mid] == 1) {
				return binSearch(a, mid+1, end, mid);
			} else {
				return binSearch(a, start, mid-1, lastIndex);
			}
		}
		return lastIndex;
	}
	
	public static void main(String[] args) {
		LastOne lastOne= new LastOne();
		int[] input= {0,1,0};
		System.out.println(lastOne.lastIndex(input));
	}
}
