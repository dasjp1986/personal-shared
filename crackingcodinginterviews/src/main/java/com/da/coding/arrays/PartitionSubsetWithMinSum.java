package com.da.coding.arrays;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given an array, the task is to divide it into two sets S1 and S2 such
 *         that the absolute difference between their sums is minimum.
 * 
 *         Input: The first line contains an integer 'T' denoting the total
 *         number of test cases. In each test cases, the first line contains an
 *         integer 'N' denoting the size of array. The second line contains N
 *         space-separated integers A1, A2, ..., AN denoting the elements of the
 *         array.
 * 
 * 
 *         Output: In each seperate line print minimum absolute difference.
 * 
 * 
 *         Constraints: 1<=T<=30 1<=N<=50 1<=A[I]<=50
 * 
 * 
 *         Example: Input: 2 4 1 6 5 11 4 36 7 46 40
 * 
 *         Output : 1 23
 * 
 *         Explaination : Subset1 = {1, 5, 6}, sum of Subset1 = 12 Subset2 =
 *         {11}, sum of Subset2 = 11
 */
public class PartitionSubsetWithMinSum {
	// Time and Space complexity- O(nlogn)
	public int minSum(int[] a) {

		if (a.length <= 1) {
			return 0;
		}
		int diff = 0;

		// O(nlog(n))
		Arrays.sort(a);

		int sum = 0;
		for (int num : a) {
			sum += num;
		}

		int target = sum / 2;

		int i = 0, j = 1;
		int localSum = a[0];
		while (j < a.length) {
			if (localSum == target) {
				break;
			} else if (localSum > target) {
				if (localSum - a[i] >= target) {
					localSum = localSum - a[i];
					i++;
				}
			} else {
				localSum = localSum + a[j];
			}
			j++;
		}

		diff = sum - localSum;

		return Math.abs(diff - localSum);
	}

	//using dynamc programming
	private static int solve(int n, int[] a) {
		if (a.length == 1) {
			return a[0];
		}
		int total = 0;
		for (int i = 0; i < n; i++) {
			total += a[i];
		}
		Integer[][] dp = new Integer[total + 1][n];
		return diff(0, 0, total, a, dp);
	}

	private static int diff(int s1, int index, int total, int[] a, Integer[][] dp) {
		if (index == a.length) {
			return Math.abs(total - s1 - s1);
		}

		if (dp[s1][index] != null) {
			return dp[s1][index];
		}

		dp[s1][index] = Math.min(diff(s1 + a[index], index + 1, total, a, dp), diff(s1, index + 1, total, a, dp));

		return dp[s1][index];
	}

	public static void main(String[] args) {
		PartitionSubsetWithMinSum partition = new PartitionSubsetWithMinSum();
		int[] a1 = { 1, 6, 5, 11 };
		System.out.println(partition.minSum(a1));

		int[] a2 = { 36, 7, 46, 40 };
		System.out.println(partition.minSum(a2));

		int[] a3 = { 3, 1, 4, 2, 2, 1 };
		System.out.println(partition.minSum(a3));

		int[] a4 = { 1, 1, 1, 6, 5, 11 };
		System.out.println(partition.minSum(a4));

		int[] a5 = { 20, 21, 19, 9 };
		System.out.println(partition.minSum(a5));

		int[] a6 = { 37, 28, 16, 44, 36, 37, 43, 50, 22, 13, 28, 41, 10, 14, 27, 41, 27, 23, 37, 12, 19, 18, 30, 33, 31,
				13, 24, 18, 36, 30, 3, 23, 9, 20 };
		System.out.println(partition.minSum(a6));
	}
}
