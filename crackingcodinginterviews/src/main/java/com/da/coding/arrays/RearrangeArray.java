package com.da.coding.arrays;

import java.util.ArrayList;

public class RearrangeArray {
	/**
	 * 
	 * @param a
	 * 
	 *            Rearrange a given array so that Arr[i] becomes Arr[Arr[i]]
	 *            with O(1) extra space.
	 * 
	 *            Example:
	 * 
	 *            Input : [1, 0] Return : [0, 1]
	 */
	public void arrange(ArrayList<Integer> a) {

		for (int i = 0; i < a.size(); i++) {

			int index = a.get(i);
			int val = a.get(index) % a.size() + 1;
			a.set(i, a.get(i) + a.size() * val);
		}
		System.out.println(a);
		for (int i = 0; i < a.size(); i++) {
			int val = a.get(i);
			val = val / a.size() - 1;
			a.set(i, val);
		}

	}

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(2);
		list.add(1);
		list.add(0);
		// list.add(1);
		// list.add(3);

		RearrangeArray problem = new RearrangeArray();
		problem.arrange(list);
		System.out.println(list);
	}
}
