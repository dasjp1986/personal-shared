package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *         Find the largest subarray with 0 sum Given an array of integers, find
 *         length of the largest subarray with sum equals to 0.
 * 
 *         Examples :
 * 
 *         Input: arr[] = {15, -2, 2, -8, 1, 7, 10, 23}; Output: 5 The largest
 *         subarray with 0 sum is -2, 2, -8, 1, 7
 * 
 *         Input: arr[] = {1, 2, 3} Output: 0 There is no subarray with 0 sum
 * 
 *         Input: arr[] = {1, 0, 3} Output: 1
 */
public class MaxSum0SubArray {
	//Time and Space Complexity O(n)
	public int maxSubArray(int[] array) {
		int count = 0;
		int sum = 0;
		Map<Integer, Integer> sumIndexMap = new HashMap<>();
		for (int i = 0; i < array.length; i++) {
			sum = sum + array[i];
			Integer startIndex = sumIndexMap.get(sum);
			if (startIndex == null) {
				sumIndexMap.put(sum, i);
			} else {
				count = Math.max(count, i - startIndex);
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		MaxSum0SubArray subArray= new MaxSum0SubArray();
		int[] array= {1, 0, 3};
		System.out.println(subArray.maxSubArray(array));
	}
}
