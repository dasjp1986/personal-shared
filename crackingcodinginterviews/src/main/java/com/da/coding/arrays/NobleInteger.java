package com.da.coding.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given an integer array, find if an integer p exists in the array such
 *         that the number of integers greater than p in the array equals to p
 *         If such an integer is found return 1 else return -1.
 */
public class NobleInteger {
	public int solve(List<Integer> A) {
		Collections.sort(A, new Comparator<Integer>() {
			public int compare(Integer x, Integer y) {
				return y.compareTo(x);
			}
		});

		if (A.get(0) == 0) {
			return 1;
		}
		int count = 0;
		for (int index = 1; index < A.size(); index++) {
			if (!A.get(index).equals(A.get(index - 1))) {
				count = index;
			}
			if (A.get(index) == count) {
				return 1;
			}
		}

		return -1;
	}

	public static void main(String[] args) {
		NobleInteger nobleInteger = new NobleInteger();
		Integer[] array = { -4, -2, 0, -1, -6 };
		List<Integer> list = Arrays.asList(array);
		System.out.println(nobleInteger.solve(list));
	}
}
