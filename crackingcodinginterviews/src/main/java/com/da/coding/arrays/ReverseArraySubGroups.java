package com.da.coding.arrays;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given an array, reverse every sub-array formed by consecutive k
 *         elements.
 * 
 *         Examples:
 * 
 *         Input: arr = [1, 2, 3, 4, 5, 6, 7, 8, 9] k = 3 Output: [3, 2, 1, 6,
 *         5, 4, 9, 8, 7]
 * 
 *         Input: arr = [1, 2, 3, 4, 5, 6, 7, 8] k = 5 Output: [5, 4, 3, 2, 1,
 *         8, 7, 6]
 * 
 *         Input: arr = [1, 2, 3, 4, 5, 6] k = 1 Output: [1, 2, 3, 4, 5, 6]
 * 
 *         Input: arr = [1, 2, 3, 4, 5, 6, 7, 8] k = 10 Output: [8, 7, 6, 5, 4,
 *         3, 2, 1]
 */
public class ReverseArraySubGroups {
	//Time Complexity- O(n)
	public void reveseSubArrays(int[] a, int k) {

		int i = 0;

		while (i < a.length) {
			k = Math.min(a.length, k);
			reverse(a, i, k-1);
			i= k;
			k=i+k;
		}

	}

	private void reverse(int[] a, int i, int j) {

		while (i < j) {
			int temp = a[i];
			a[i] = a[j];
			a[j] = temp;

			i++;
			j--;
		}
	}

	public static void main(String[] args) {
		ReverseArraySubGroups problem = new ReverseArraySubGroups();
		int[] a = {1, 2, 3, 4, 5, 6, 7, 8};
		problem.reveseSubArrays(a, 10);
		System.out.println(Arrays.toString(a));
	}
}
