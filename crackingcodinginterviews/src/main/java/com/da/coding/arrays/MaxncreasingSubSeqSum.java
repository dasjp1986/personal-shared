package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 * 
 *         Given an array of n positive integers. Write a program to find the
 *         sum of maximum sum subsequence of the given array such that the
 *         integers in the subsequence are sorted in increasing order.
 * 
 *         Input:
 * 
 *         The first line of input contains an integer T denoting the number of
 *         test cases. The first line of each test case is N,N is the size of
 *         array. The second line of each test case contains N input A[].
 * 
 *         Output:
 * 
 *         Print the sum of maximum sum sequence of the given array.
 * 
 *         Constraints:
 * 
 *         1 ≤ T ≤ 100 1 ≤ N ≤ 100 1 ≤ A[] ≤ 1000
 * 
 *         Example:
 * 
 *         Input: 2 7 1 101 2 3 100 4 5 4 10 5 4 3
 * 
 *         Output: 106 10
 * 
 *         Explanation: For input: 7 1 101 2 3 100 4 5 All the increasing
 *         subsequences : (1,101); (1,2,3,100); (1,2,3,4,5), out of this
 *         (1,2,3,100) has maximum sum,i.e., 106. Hence the output is stated as
 *         106.
 */
public class MaxncreasingSubSeqSum {
	public int maxSumSeq(int[] a) {
		if (a.length == 1) {
			return a[0];
		}
		int[] cache = new int[a.length + 1];
		return Math.max(a[0] + find(a, 0, cache), find(a, 1, cache));
	}

	private int find(int[] a, int i, int[] cache) {
		int sum = 0;
		if (cache[i] != 0) {
			return cache[i];
		}
		for (int j = i + 1; j < a.length; j++) {
			if (a[j] > a[i]) {
				int s1 = a[j] + find(a, j, cache);
				int s2 = find(a, j + 1, cache);
				int s = Math.max(s1, s2);
				if (s > sum) {
					sum = s;
				}
			}
		}
		cache[i] = sum;
		return sum;
	}

	public static void main(String[] args) {
		int[] a = {1,2,3,7,4,9,5,6};
		MaxncreasingSubSeqSum problem = new MaxncreasingSubSeqSum();
		System.out.println(problem.maxSumSeq(a));
	}
}
