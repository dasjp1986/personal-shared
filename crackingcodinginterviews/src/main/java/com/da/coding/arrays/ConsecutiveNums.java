package com.da.coding.arrays;

public class ConsecutiveNums {
	static int iterations=0;

	static int consecutive(long num) {
		int count = 0;

		long maxIterations = num / 2;
		long sum = 0;

		for (long i = 1; i <= maxIterations; i++) {
			sum = i;
			for (long j = i + 1; j <= maxIterations + 1; j++) {
				sum += j;
				if (sum == num) {
					count++;
					break;
				}
				iterations++;
			}
		}
		return count;

	}

	public static void main(String[] args) {
		System.out.println(consecutive(15));
		System.out.println(iterations);
	}
}
