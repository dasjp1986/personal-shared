package com.da.coding.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1 Find the number of common elements in two given sorted array
 *         of same size-n Each array has distinct elements in it
 * 
 *         Time Complexity- O(n) Space Complexity- O(1)
 *         Ex-
 *         	13 27 35 40 49 55 59
			17 35 39 40 55 58 60
			3
 */
public class CommonInSortedArrays {
	public static int countCommon(int[] sortedArray1, int[] sortedArray2) {
		if (sortedArray1.length < 1 || sortedArray2.length < 1 || (sortedArray1.length != sortedArray2.length))
			return 0;

		int i = 0, j = 0;
		int counter = 0;
		int length = sortedArray1.length;

		while (i < length && j < length) {
			if (sortedArray1[i] == sortedArray2[j]) {
				counter++;
				i++;
				j++;
			} else if (sortedArray1[i] < sortedArray2[j]) {
				i++;
			} else {
				j++;
			}
		}

		return counter;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		String[] array1 = bfr.readLine().split(" ");
		String[] array2 = bfr.readLine().split(" ");

		bfr.close();
		
		int[] sortedArray1 = new int[array1.length];
		int[] sortedArray2 = new int[array2.length];

		for (int i = 0; i < array1.length; i++) {
			sortedArray1[i] = Integer.parseInt(array1[i]);
		}
		for (int i = 0; i < array2.length; i++) {
			sortedArray2[i] = Integer.parseInt(array2[i]);
		}
		
		System.out.println(countCommon(sortedArray1, sortedArray2));
		
	}
}
