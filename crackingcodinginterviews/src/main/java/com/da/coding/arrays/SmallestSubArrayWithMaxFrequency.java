package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         Given an array A. Let X be an element in the array which has the
 *         maximum frequency. The task is to find the smallest sub segment of
 *         the array which also has X as the maximum frequency element.
 * 
 *         Note: if two or more elements have the same frequency (i.e., maximum
 *         frequency) and the same sub segment size then print the sub segment
 *         which occurs first in the array.
 * 
 *         Input: The first line of input contains an integer T denoting the
 *         number of test cases. Then T test cases follow. Each test case
 *         consists of two lines. First line of each test case contains an
 *         Integer N denoting size of array and the second line contains N space
 *         separated elements.
 * 
 *         Output: For each test case, print the required sub segment of array
 *         in new line.
 * 
 *         Constraints: 1<=T<=100 1<=N<=105 1<=A[i]<=105
 * 
 *         Example: Input: 3 8 4 1 1 2 2 1 3 3 5 1 2 2 3 1 6 1 4 3 3 5 5 Output:
 *         1 1 2 2 1 2 2 3 3
 */

public class SmallestSubArrayWithMaxFrequency {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			int[] nums = new int[n];
			for (int i = 0; i < n; i++) {
				nums[i] = scn.nextInt();
			}
			solve(n, nums);
			System.out.println();
		}
		scn.close();
	}

	private static void solve(int n, int[] nums) {
		Map<Integer, Integer> countMap = new HashMap<>();
		int num = 0, max = 0;
		for (int i = 0; i < n; i++) {
			Integer count = countMap.get(nums[i]);
			if (count == null) {
				count = 0;
			}
			count++;
			countMap.put(nums[i], count);
			if (count > max) {
				max = count;
				num = nums[i];
			}
		}

		System.out.println(num + " : " + max);

		int i = 0, j = n - 1;
		while (nums[i] != num) {
			i++;
		}

		while (nums[j] != num) {
			j--;
		}

		for (; i <= j; i++) {
			System.out.print(nums[i] + " ");
		}
	}

}
