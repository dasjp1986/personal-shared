package com.da.coding.arrays;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

/**
 * 
 * @author dagraw1
 *
 *         Sliding Window Maximum (Maximum of all subarrays of size k) Given an
 *         array and an integer k, find the maximum for each and every
 *         contiguous subarray of size k.
 * 
 *         Examples :
 * 
 *         Input : arr[] = {1, 2, 3, 1, 4, 5, 2, 3, 6} k = 3 Output : 3 3 4 5 5
 *         5 6
 * 
 *         Input : arr[] = {8, 5, 10, 7, 9, 4, 15, 12, 90, 13} k = 4 Output : 10
 *         10 10 15 15 90 90
 */

// Time Complexity- O(n)
// Space Complexity- O(k)
public class MaxOfSubArraysSizeK {

	public int[] maxSubArray(int[] a, int k) {
		int[] res = new int[a.length - k + 1];
		Deque<Integer> q = new ArrayDeque<>();
		for (int i = 0; i < k; i++) {
			if (q.isEmpty() || a[q.peekLast()] >= a[i]) {
				q.addLast(i);
			} else {
				while (!q.isEmpty() && a[q.peekLast()] < a[i]) {
					q.removeLast();
				}
				q.addLast(i);
			}
		}

		for (int j = 0; j < res.length; j++) {
			res[j] = a[q.peekFirst()];
			if (j == q.peekFirst()) {
				q.removeFirst();
			}
			if (j + k < a.length) {
				if (q.isEmpty() || a[q.peekLast()] >= a[j + k]) {
					q.addLast(j + k);
				} else {
					while (!q.isEmpty() && a[q.peekLast()] < a[j + k]) {
						q.removeLast();
					}
					q.addLast(j + k);
				}
			}
		}
		return res;
	}

	public static void main(String[] args) {
		int[] a = { 8, 5, 10, 7, 9, 4, 15, 12, 90, 13 };
		MaxOfSubArraysSizeK problem = new MaxOfSubArraysSizeK();
		System.out.println(Arrays.toString(problem.maxSubArray(a, 3)));
	}
}
