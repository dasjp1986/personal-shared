package com.da.coding.arrays;

import java.util.ArrayList;
/**
 * 
 * @author dagraw1
 * Given numRows, generate the first numRows of Pascal’s triangle.

Pascal’s triangle : To generate A[C] in row R, sum up A’[C] and A’[C-1] from previous row R - 1.

Example:

Given numRows = 5,

Return

[
     [1],
     [1,1],
     [1,2,1],
     [1,3,3,1],
     [1,4,6,4,1]
]
 */
public class PascalTriangle {
	public static ArrayList<ArrayList<Integer>> generate(int A) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<>();
		ArrayList<Integer> prev;
		for (int i = 0; i < A; i++) {
			ArrayList<Integer> result = new ArrayList<>();
			for (int j = 0; j <= i; j++) {
				if (j == 0) {
					result.add(1);
				} else {
					prev = results.get(i - 1);
					result.add(prev.get(j - 1) + (prev.size() == j ? 0 : prev.get(j)));
				}
			}
			results.add(result);
		}
		return results;
	}
	
	public static void main(String[] args) {
		System.out.println(generate(5));
	}
}
