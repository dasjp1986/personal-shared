package com.da.coding.arrays;

import java.util.ArrayList;

/**
 * 
 * @author dagraw1
 *
 *         You are in an infinite 2D grid where you can move in any of the 8
 *         directions :
 * 
 *         (x,y) to (x+1, y), (x - 1, y), (x, y+1), (x, y-1), (x-1, y-1),
 *         (x+1,y+1), (x-1,y+1), (x+1,y-1) You are given a sequence of points
 *         and the order in which you need to cover the points. Give the minimum
 *         number of steps in which you can achieve it. You start from the first
 *         point.
 * 
 *         Example :
 * 
 *         Input : [(0, 0), (1, 1), (1, 2)] Output : 2
 */
public class InfiniteGrid {
	public int coverPoints(ArrayList<Integer> A, ArrayList<Integer> B) {
		int total = 0;
		for (int i = 1; i < A.size(); i++) {
			total += find(A.get(i - 1), A.get(i), B.get(i - 1), B.get(i));
		}
		return total;
	}

	private int find(int a1, int a2, int b1, int b2) {
		int da = Math.abs(a2 - a1);
		int db = Math.abs(b2 - b1);

		if (da == db) {
			return da;
		} else if (da > db) {
			return da;
		} else {
			return db;
		}

	}

	public static void main(String[] args) {
		InfiniteGrid grid = new InfiniteGrid();
		ArrayList<Integer> A = new ArrayList<>();
		ArrayList<Integer> B = new ArrayList<>();
		A.add(0);
		A.add(2);
		B.add(0);
		B.add(4);
		System.out.println(grid.coverPoints(A, B));
	}
}
