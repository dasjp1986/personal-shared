package com.da.coding.arrays;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *
 *         Search an element in a sorted and rotated array
 * 
 *         An element in a sorted array can be found in O(log n) time via binary
 *         search. But suppose we rotate an ascending order sorted array at some
 *         pivot unknown to you beforehand. So for instance, 1 2 3 4 5 might
 *         become 3 4 5 1 2. Devise a way to find an element in the rotated
 *         array in O(log n) time.
 * 
 *         sortedPivotedArray
 * 
 *         Input : arr[] = {5, 6, 7, 8, 9, 10, 1, 2, 3}; key = 3 Output : Found
 *         at index 8
 * 
 *         Input : arr[] = {5, 6, 7, 8, 9, 10, 1, 2, 3}; key = 30 Output : Not
 *         found
 * 
 *         Input : arr[] = {30, 40, 50, 10, 20} key = 10 Output : Found at index
 *         3
 * 
 */
public class RotatedArraySearch {
	public int search(final List<Integer> a, int target) {
		int pivot = getPivot(a, 0, a.size() - 1);
		if (pivot == -1) {
			return -1;
		}

		if (a.get(pivot) == target) {
			return pivot;
		} else if (a.get(pivot) < target && target <= a.get(a.size() - 1)) {
			return bin(a, target, pivot + 1, a.size() - 1);
		} else {
			return bin(a, target, 0, pivot - 1);
		}
	}

	//Time complexity- O(logN)
	private int getPivot(List<Integer> a, int start, int end) {
		if (start > end) {
			return -1;
		}
		int mid = (start + end) / 2;
		int left = Integer.MAX_VALUE;
		int right = Integer.MAX_VALUE;
		if (mid - 1 >= 0) {
			left = a.get(mid - 1);
		}
		if (mid + 1 < a.size()) {
			right = a.get(mid + 1);
		}
		if (left > a.get(mid) && right > a.get(mid)) {
			return mid;
		}

		if (a.get(mid) < a.get(start)) {
			return getPivot(a, start, mid - 1);
		} else {
			return getPivot(a, mid + 1, end);
		}
	}

	private int bin(List<Integer> a, int target, int start, int end) {
		if (start > end) {
			return -1;
		}

		int mid = (start + end) / 2;
		if (target == a.get(mid)) {
			return mid;
		}
		if (target < a.get(mid)) {
			return bin(a, target, start, mid - 1);
		} else {
			return bin(a, target, mid + 1, end);
		}

	}

	public static void main(String[] args) {
		Integer[] array = { 101, 103, 106, 109, 158, 164, 182, 187, 202, 205, 2, 3, 32, 57, 69, 74, 81, 99, 100 };
		RotatedArraySearch search = new RotatedArraySearch();
		System.out.println(search.search(Arrays.asList(array), 202));
	}
}
