package com.da.coding.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *
 *         Given an unsorted integer array, find the first missing positive
 *         integer.
 * 
 *         Example:
 * 
 *         Given [1,2,0] return 3,
 * 
 *         [3,4,-1,1] return 2,
 * 
 *         [-8, -7, -6] returns 1
 * 
 *         Your algorithm should run in O(n) time and use constant space.
 */
public class FindMissingInteger {
	public static int firstMissingPositive(List<Integer> A) {
		int min = 0, max = Integer.MIN_VALUE;
		Map<Integer, Boolean> cache = new HashMap<>();
		for (int i = 0; i < A.size(); i++) {
			if (A.get(i) > max) {
				max = A.get(i);
			}
			if (A.get(i) < min) {
				min = A.get(i);
			}
			cache.put(A.get(i), true);
		}
		if (max < 0) {
			return 1;
		}
		min = min < 0 ? 0 : min;
		for (int i = min + 1; i < max; i++) {
			Boolean isPresent = cache.get(i);
			if (isPresent == null) {
				return i;
			}
		}

		return max + 1;
	}

	public static void main(String[] args) {
		Integer[] array = { 3, 4, -1, 1, 2 };
		System.out.println(firstMissingPositive(Arrays.asList(array)));
	}
}
