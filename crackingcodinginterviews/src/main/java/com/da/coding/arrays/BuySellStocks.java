package com.da.coding.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *         The cost of a stock on each day is given in an array, find the max
 *         profit that you can make by buying and selling in those days. For
 *         example, if the given array is {100, 180, 260, 310, 40, 535, 695},
 *         the maximum profit can earned by buying on day 0, selling on day 3.
 *         Again buy on day 4 and sell on day 6. If the given array of prices is
 *         sorted in decreasing order, then profit cannot be earned at all.
 */
public class BuySellStocks {
	
	public static void buySell(int[] a) {
		int min = 0, max = 0;
		boolean isProfit= false;
		if (a.length == 1) {
			System.out.println("No Profit");
			return;
		}
		for (int i = 1; i < a.length; i++) {
			if (a[i] >= a[max]) {
				max = i;
			} else {
			    if(a[max]-a[min]>0){
				    System.out.print("("+min + " " + max+") ");
				    isProfit= true;
			    }
				min = i;
				max = i;
			}

			if (i == a.length - 1 && a[max]-a[min]>0) {
				System.out.print("("+min + " " + max+")");
				isProfit= true;
			}
		}
		if(!isProfit){
		    System.out.println("No Profit");
		}
	}

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		int testCases = Integer.parseInt(line);
		for (int i = 0; i < testCases; i++) {
			int size = Integer.parseInt(reader.readLine());
			int[] array = new int[size];
			line = reader.readLine();
			String[] strings = line.split(" ");
			for (int j = 0; j < array.length; j++) {
				array[j] = Integer.parseInt(strings[j]);
			}
			buySell(array);
			System.out.println();
		}

	}

}
