package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Digits {

	public static void main(String[] args) {
		Integer[] array={1, 9, 9, 9, 9, 9, 9};
		System.out.println(plusOne((Arrays.asList(array))));
		
	}

	public static List<Integer> plusOne(List<Integer> A) {
		if (A.size() == 1) {
			A.set(0, A.get(0) + 1);
			return A;
		}
		reverse(A);
		int carry = 1, d = 0;
		for (int i = 0; i < A.size(); i++) {
			d = (A.get(i) + carry) % A.size();
			carry = (A.get(i) + carry) / 10;
			A.set(i, d);
		}
		if (carry != 0) {
			A.add(carry);
		}
		reverse(A);

		return A;
	}

	private static void reverse(List<Integer> A) {
		int i = 0, j = A.size() - 1;
		while (i < j) {
			Integer temp = A.get(i);
			A.set(i, A.get(j));
			A.set(j, temp);
			i++;
			j--;
		}
	}
}
