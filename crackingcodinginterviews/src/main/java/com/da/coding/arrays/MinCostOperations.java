package com.da.coding.arrays;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *
 *         Chandan is back with his array to blow your mind. As usual Chandan
 *         has an array consisting of N integers .He allows you to perform 2
 *         kinds of operation on his array.
 * 
 *         Type 1 : Increment any integer of the array by 1.
 * 
 *         Type 2 : Decrement any integer of the array by 1.
 * 
 *         You can perform these operation as many times as you want on his
 *         array.
 * 
 *         Each operation of Type 1 costs 3 while each operation of Type 2 costs
 *         5.
 * 
 *         Now Chandan wants to have K equal elements in his array.So he asks
 *         you to tell him the minimum cost required in obtaining K equal
 *         elements in his array.
 * 
 *         Input:
 * 
 *         The first line contains T indicating test cases.Second line contains
 *         2 integers N indicating the number of elements in his array and K.
 * 
 *         Third line contains N space separated integers denoting Chandan
 *         array.
 * 
 *         Output:
 * 
 *         The minimum cost required to get K equal elements.
 * 
 *         Constraints :
 * 
 *         1 ≤ T ≤ 100
 * 
 *         1 ≤ K ≤ N ≤100
 * 
 *         1 ≤ A[i] ≤100
 * 
 *         SAMPLE INPUT 1 5 3 9 4 9 7 4 SAMPLE OUTPUT 6 Explanation We can
 *         convert 7 to 9 to get three 9. The cost of this conversion will be 6.
 */
public class MinCostOperations {

	public static void main(String args[]) throws Exception {

//		// BufferedReader
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		int t = Integer.parseInt(br.readLine());
//		for (int i = 0; i < t; i++) {
//			String[] inputs = br.readLine().split(" ");
//			int n = Integer.parseInt(inputs[0]);
//			int k = Integer.parseInt(inputs[1]);
//
//			Integer[] nums = new Integer[n];
//			String[] s = br.readLine().split(" ");
//			for (int j = 0; j < n; j++) {
//				nums[j] = Integer.parseInt(s[j]);
//			}
//			System.out.println(find(nums, k));
//		}

		/*
		 * Sample code to perform I/O: Use either of these methods for input
		 * 
		 * //BufferedReader BufferedReader br = new BufferedReader(new
		 * InputStreamReader(System.in)); String name = br.readLine(); // Reading input
		 * from STDIN System.out.println("Hi, " + name + "."); // Writing output to
		 * STDOUT
		 */
		// Scanner
		
		double d = 2.0/3;
	     DecimalFormat f = new DecimalFormat("0.000000");
	     System.out.println(f.format(d));

	}

	private static int find(Integer[] nums, int k) {
		Arrays.sort(nums);
		int minCost = Integer.MAX_VALUE;
		for (int i = 0; i <= nums.length - k; i++) {
			int min = nums[i];
			int cost = 0;
			for (int j = 1; j < k; j++) {
				if (min != nums[i + j]) {
					int plusOps = j * 3 * Math.abs(nums[i + j] - min);
					int minusOps = 5 * Math.abs(nums[i + j] - min);
					if (plusOps < minusOps) {
						min = nums[i + j];
						cost += plusOps;
					} else {
						cost += minusOps;
					}
				}
			}
			if (cost < minCost) {
				minCost = cost;
			}
		}

		return minCost;
	}

}
