package com.da.coding.arrays;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given an array of distinct elements, rearrange the elements of array
 *         in zig-zag fashion in O(n) time. The converted array should be in
 *         form a < b > c < d > e < f.
 * 
 *         Example: Input: arr[] = {4, 3, 7, 8, 6, 2, 1} Output: arr[] = {3, 7,
 *         4, 8, 2, 6, 1}
 * 
 *         Input: arr[] = {1, 4, 3, 2} Output: arr[] = {1, 4, 2, 3}
 */
public class ZigZagArrays {
	//Time Complexity- O(n)
	public static void zigzag(int[] array) {
		for (int i = 1; i < array.length; i++) {
			if (i % 2 == 0 && array[i] > array[i - 1]) {
				swap(array, i, i - 1);
			} else if (i % 2 != 0 && array[i] < array[i - 1]) {
				swap(array, i, i - 1);
			}
		}
	}

	private static void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	public static void main(String[] args) {
		int[] array={8,7,6,5,4,3,2,1};
		zigzag(array);
		System.out.println(Arrays.toString(array));
	}
}
