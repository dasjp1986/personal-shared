package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *
 *         Find subarray with given sum | Set 2 (Handles Negative Numbers)
 * 
 *         Given an unsorted array of integers, find a subarray which adds to a
 *         given number. If there are more than one subarrays with the sum as
 *         the given number, print any of them.
 * 
 *         Examples:
 * 
 *         Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33 Ouptut: Sum found
 *         between indexes 2 and 4
 * 
 *         Input: arr[] = {10, 2, -2, -20, 10}, sum = -10 Ouptut: Sum found
 *         between indexes 0 to 3
 * 
 *         Input: arr[] = {-10, 0, 2, -2, -20, 10}, sum = 20 Ouptut: No subarray
 *         with given sum exists
 * 
 */
class SubArrayGivenSum {
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			int[] a = new int[n];
			for (int j = 0; j < n; j++) {
				a[j] = scn.nextInt();
			}

			subArraySum(n, a, scn.nextInt());
		}
		scn.close();
	}

	public static void subArraySum(int n, int[] a, int target) {
		Map<Integer, Integer> map = new HashMap<>();
		int sum = 0;
		int start = 0;
		int end = -1;

		for (int i = 0; i < n; i++) {
			if (a[i] == target) {
				start = i;
				end = i;
				break;
			}

			sum += a[i];
			if (sum == target) {
				end = i;
				break;
			}

			Integer index = map.get(sum - target);
			if (index != null) {
				start = index + 1;
				end = i;
				break;
			}

			map.put(sum, i);

		}

		if (end == -1) {
			System.out.println("Target not found");
		} else {
			System.out.println(start + "," + end);
		}
	}
}