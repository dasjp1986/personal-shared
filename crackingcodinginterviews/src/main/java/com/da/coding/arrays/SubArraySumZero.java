package com.da.coding.arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *Given an array a[] of N positive and negative numbers. Find if there is a subarray (of size at-least one) with 0 sum.

Input:
The first line of input contains an integer T denoting the number of test cases. Then T test cases follow. Each test case contains an integer n denoting the size of the array. The next line contains N space separated integers forming the array.

Output:
Print "Yes" ( without quotes) if there exist a subarray of size at least one with sum equal to 0 or else print "No" ( without quotes).

Constraints:
1 <= T <= 100
1 <= N <= 104
-105 <= a[i] <= 105

Example:
Input:
2
5
4 2 -3 1 6
5
4 2 0 1 6

Output:
Yes
Yes
 */
public class SubArraySumZero {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			int[] array = new int[n];
			for (int i = 0; i < n; i++) {
				array[i] = scn.nextInt();
			}
			System.out.println(isSumZero(n, array));
		}
		scn.close();
	}

	public static String isSumZero(int n, int[] array) {
		Map<Integer, Integer> map = new HashMap<>();
		int sum = array[0];
		map.put(sum, 1);

		for (int i = 1; i < n; i++) {
			sum += array[i];
			if (sum==0 || map.get(sum) != null) {
				return "Yes";
			}
			map.put(sum, 1);
		}

		return "No";
	}

}
