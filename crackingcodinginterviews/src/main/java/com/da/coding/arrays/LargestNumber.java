package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given a list of non negative integers, arrange them such that they
 *         form the largest number.
 * 
 *         For example:
 * 
 *         Given [3, 30, 34, 5, 9], the largest formed number is 9534330.
 * 
 *         Note: The result may be very large, so you need to return a string
 *         instead of an integer.
 */
public class LargestNumber {
	//Time Complexity- O(NlogN)
	public static String largestNumber(final List<Integer> A) {
		StringBuilder builder = new StringBuilder();
		List<String> list = new ArrayList<>();
		int sum = 0;
		for (int i = 0; i < A.size(); i++) {
			list.add(A.get(i).toString());
			sum += A.get(i);
		}

		if (sum == 0) {
			return "0";
		}
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				String first = o1 + o2;
				String second = o2 + o1;
				return second.compareTo(first);
			}
		});
		for (int i = 0; i < list.size(); i++) {
			builder.append(list.get(i));
		}
		return builder.toString();
	}

	public static void main(String[] args) {
		Integer[] array = { 931, 94, 209, 448, 716, 903, 124, 372, 462, 196, 715, 802, 103, 740, 389, 872, 615, 638,
				771, 829, 899, 999, 29, 163, 342, 902, 922, 312, 326, 817, 288, 75, 37, 286, 708, 589, 975, 747, 743,
				699, 743, 954, 523, 989, 114, 402, 236, 855, 323, 79, 949, 176, 663, 587, 322 };
		System.out.println(largestNumber(Arrays.asList(array)));
	}
}
