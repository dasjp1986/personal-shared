package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given an array and a number k where k is smaller than size of array,
 *         we need to find the k’th smallest element in the given array. It is
 *         given that ll array elements are distinct.
 * 
 *         Examples:
 * 
 *         Input: arr[] = {7, 10, 4, 3, 20, 15} k = 3 Output: 7
 * 
 *         Input: arr[] = {7, 10, 4, 3, 20, 15} k = 4 Output: 10
 */
public class KthSmallestElementInArray {
	// modified version of quick sort, keep sorting until pivot position is same
	// as expected kth index. idea is to find kth element irrespective of all
	// smaller elements before it may not be in sorted order be guaranteed to be
	// smaller

	//Time Complexity- O(n)
	public static void main(String[] args) {
		KthSmallestElementInArray problem = new KthSmallestElementInArray();
		int[] a = { 10, 80, 30, 90, 40, 50, 70 };
		System.out.println(problem.kthSmallestElement(a, 7));
	}

	public int kthSmallestElement(int[] a, int k) {
		if (k > a.length || k < 1) {
			return -1;
		}

		return a[findSmallest(a, 0, a.length - 1, k)];

	}

	private int findSmallest(int[] a, int i, int j, int k) {
		int pos = partition(a, i, j, k);
		if (pos == k - 1) {
			return pos;
		}
		if (k <= pos) {
			return findSmallest(a, i, pos - 1, k);
		}
		return pos + 1 + findSmallest(a, pos + 1, j, k - pos - 1);
	}

	private int partition(int[] a, int start, int end, int k) {
		if (k <= end - start + 1) {
			int pivot = a[end];
			int i = start;

			for (int j = start; j < end; j++) {
				if (a[j] <= pivot) {
					swap(a, i, j);
					i++;
				}
			}
			swap(a, i, end);

			return i;
		}
		return -1;
	}

	private void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}
