package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CuttingSticks {
	static int[] cutSticks(int[] lengths) {
		List<Integer> result = new ArrayList<>();
		getSticksCount(lengths, result);
		return convertToIntArray(result);

	}

	private static int[] convertToIntArray(List<Integer> result) {
		int[] array = new int[result.size()];
		for (int i = 0; i < array.length; i++) {
			array[i] = result.get(i);
		}
		return array;
	}

	private static void getSticksCount(int[] lengths, List<Integer> result) {
		int shortest = Integer.MAX_VALUE;
		// find the shortest stick
		for (int i = 0; i < lengths.length; i++) {
			if (lengths[i] != 0 && lengths[i] < shortest) {
				shortest = lengths[i];
			}
		}
		int cutCount = 0;
		// find the shortest stick
		for (int i = 0; i < lengths.length; i++) {
			if (lengths[i] != 0) {
				lengths[i] = lengths[i] - shortest;
				cutCount++;
			}
		}
		if (cutCount > 0) {
			result.add(cutCount);
			getSticksCount(lengths, result);
		}

	}

	public static void main(String[] args) {
		int[] lengths = { 5, 4, 4, 2, 2, 8 };
		System.out.println(Arrays.toString(cutSticks(lengths)));
	}
}
