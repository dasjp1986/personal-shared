package com.da.coding.arrays;

import com.da.coding.commons.SystemInputReaderUtil;

/**
 * 
 * @author dagraw1 Given an array of process execution times and CPU schedule
 *         process execution in round robin fashion. Given the index of the
 *         process (stating from 0), return how much would be needed for process
 *         to complete.
 * 
 *         Ex- [3,2,2,1,4] 
 *         search for index- 
 *         2 
 *         [3,2,2,1,4] 
 *         [2,1,1,0,3]
 *         [1,0,0,0,3]= 6+1+1=8
 *         
 *         Time Complexity= O(N)
 */
public class RoundRobinProcessTime {
	public static int executionTime(int[] processes, int pos) {
		int totalTime = 0;
		for (int i = 0; i < processes.length; i++) {
			if (processes[i] < processes[pos]) {
				totalTime = totalTime + processes[i];
			} else {
				if (i <= pos) {
					totalTime = totalTime + processes[pos];
				} else {
					totalTime = totalTime + processes[pos] - 1;
				}
			}
		}
		return totalTime;
	}

	public static void main(String[] args) {
		String[] inputStrings = SystemInputReaderUtil.readString().split(" ");
		int[] intArray = new int[inputStrings.length];
		for (int i = 0; i < inputStrings.length; i++) {
			intArray[i] = Integer.parseInt(inputStrings[i]);
		}
		System.out.println(executionTime(intArray, Integer.parseInt(SystemInputReaderUtil.readString())));;
	}
}
