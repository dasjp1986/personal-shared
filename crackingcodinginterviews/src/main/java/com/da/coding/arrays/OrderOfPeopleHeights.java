package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/*
 * 
 * You are given the following :

A positive number N
Heights : A list of heights of N persons standing in a queue
Infronts : A list of numbers corresponding to each person (P) that gives the number of persons who are taller than P and standing in front of P
You need to return list of actual order of persons’s height

Consider that heights will be unique

Example

Input : 
Heights: 5 3 2 6 1 4
InFronts: 0 1 2 0 3 2
Output : 
actual order is: 5 3 2 1 6 4 
So, you can see that for the person with height 5, there is no one taller than him who is in front of him, and hence Infronts has 0 for him.

For person with height 3, there is 1 person ( Height : 5 ) in front of him who is taller than him.

You can do similar inference for other people in the list.
 */
public class OrderOfPeopleHeights {

	public ArrayList<Integer> order(List<Integer> h, List<Integer> in) {
		Map<Integer, Integer> map = new TreeMap<>();

		for (int i = 0; i < h.size(); i++) {
			map.put(h.get(i), in.get(i));
		}

		int last = -1;
		for (Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() == 0) {
				last = entry.getKey();
				break;
			}
		}

		ArrayList<Integer> res = new ArrayList<Integer>();
		for (int i = 0; i < h.size(); i++) {
			res.add(last);
			int find = -1;
			for (Entry<Integer, Integer> entry : map.entrySet()) {
				if (entry.getKey() == last) {
					break;
				}
				int val = entry.getValue() - 1 < 0 ? 0 : entry.getValue() - 1;
				map.put(entry.getKey(), val);
				if (val == 0 && find == -1) {
					find = entry.getKey();
				}
			}
			map.remove(last);
			if (find == -1) {
				for (Entry<Integer, Integer> entry : map.entrySet()) {
					if (entry.getValue() == 0) {
						last = entry.getKey();
						break;
					}
				}
			} else {
				last = find;
			}
		}

		return res;
	}

	public static void main(String[] args) {
		OrderOfPeopleHeights problem = new OrderOfPeopleHeights();
		Integer[] heights = { 5, 3, 2, 6, 1, 4 };
		Integer[] infronts = { 0, 1, 2, 0, 3, 2 };
		System.out.println(problem.order(Arrays.asList(heights), Arrays.asList(infronts)));
	}

}
