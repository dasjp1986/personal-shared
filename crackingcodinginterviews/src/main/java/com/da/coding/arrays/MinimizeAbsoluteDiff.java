package com.da.coding.arrays;

import java.util.Arrays;
import java.util.List;

public class MinimizeAbsoluteDiff {
	public static int findMin(List<Integer> a, List<Integer> b, List<Integer> c) {
		int minDiff = Integer.MAX_VALUE;

		int i = 0, j = 0, k = 0;

		while (i < a.size() && j < b.size() && k < c.size()) {
			int min = 0;
			int max = 0;

			if (a.get(i) <= b.get(j) && a.get(i) <= c.get(k)) {
				min = a.get(i++);
				if (b.get(j) < c.get(k)) {
					max = c.get(k);
					//k = k > 0 ? k - 1 : k;
				} else {
					max = b.get(j);
					//j = j > 0 ? j - 1 : j;
				}
			} else if (b.get(j) <= c.get(k) && b.get(j) <= a.get(i)) {
				min = b.get(j++);
				if (c.get(k) < a.get(i)) {
					max = a.get(i);
					i = i > 0 ? i - 1 : i;
				} else {
					max = c.get(k);
					//k = k > 0 ? k - 1 : k;
				}
			} else {
				min = c.get(k++);
				if (b.get(j) < a.get(i)) {
					max = a.get(i);
					//i = i > 0 ? i - 1 : i;
				} else {
					max = b.get(j);
					//j = j > 0 ? j - 1 : j;
				}
			}

			if (Math.abs(max - min) < minDiff) {
				minDiff = Math.abs(max - min);
			}
		}

		return minDiff;
	}

	public static void main(String[] args) {
		System.out.println(findMin(Arrays.asList(1, 4, 5, 8, 10), Arrays.asList(6, 9, 15), Arrays.asList(2, 3, 6, 6)));
		System.out.println(findMin(Arrays.asList(1), Arrays.asList(1,2), Arrays.asList(1,2,3)));
	}
}
