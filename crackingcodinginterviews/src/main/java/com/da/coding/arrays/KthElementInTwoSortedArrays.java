package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 * 
 *         Given two sorted arrays of size m and n respectively, you are tasked
 *         with finding the element that would be at the k’th position of the
 *         final sorted array.
 * 
 *         Examples:
 * 
 *         Input : Array 1 - 2 3 6 7 9 Array 2 - 1 4 8 10 k = 5 Output : 6
 *         Explanation: The final sorted array would be - 1, 2, 3, 4, 6, 7, 8,
 *         9, 10 The 5th element of this array is 6. Input : Array 1 - 100 112
 *         256 349 770 Array 2 - 72 86 113 119 265 445 892 k = 7 Output : 256
 *         Explanation: Final sorted array is - 72, 86, 100, 112, 113, 119, 256,
 *         265, 349, 445, 770, 892 7th element of this array is 256.
 */
public class KthElementInTwoSortedArrays {
	public int kth(int[] array1, int[] array2, int k) {
		if (k > array1.length + array2.length || k < 0) {
			return -1;
		}

		return find(array1, 0, array1.length, array2, 0, array2.length, k);
	}

	private int find(int[] array1, int s1, int length1, int[] array2, int s2, int length2, int k) {
		if (k == 1) {
			return array1[s1 + k - 1] < array2[s2 + k - 1] ? array1[s1 + k - 1] : array2[s2 + k - 1];
		}
		if (s1 == length1) {
			return array2[s2 + k - 1];
		}
		if (s2 == length2) {
			return array1[s1 + k - 1];
		}
		int partition = k / 2;
		if (array1[s1 + partition - 1] < array2[s2 + partition - 1]) {
			return find(array1, s1 + partition, length1, array2, s2, length2, k - partition);
		} else {
			return find(array1, s1, length1, array2, s2 + partition, length2, k - partition);
		}

	}

	public static void main(String[] args) {
		KthElementInTwoSortedArrays kth = new KthElementInTwoSortedArrays();
		int[] array1 = { 2, 3, 6, 7, 9 };
		int[] array2 = { 1, 4, 8, 10 };
		System.out.println(kth.kth(array1, array2, 8));
	}
}
