package com.da.coding.arrays;

import java.util.Arrays;
import java.util.List;

public class SortColors {
	public void sortColors(List<Integer> a) {
		if (a.size() < 2) {
			return;
		}
		int red = 0, i = 0, blue = a.size() - 1;
		while (i < blue) {
			if (a.get(i) == 0 && i != red) {
				int temp = a.get(red);
				a.set(red, 0);
				a.set(i, temp);
				red++;
			} else if (a.get(i) == 2 && i != blue) {
				int temp = a.get(blue);
				a.set(blue, 2);
				a.set(i, temp);
				blue--;
			}else{
				i++;
			}
			
		}
	}

	public static void main(String[] args) {
		SortColors sortColors = new SortColors();
		Integer[] array = { 2, 1, 0, 2, 1, 0 };
		List<Integer> list = Arrays.asList(array);
		sortColors.sortColors(list);
		System.out.println(list);
	}
}
