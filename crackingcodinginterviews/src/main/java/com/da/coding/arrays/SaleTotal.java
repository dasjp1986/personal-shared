package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * @author dagraw1
 * 
 *         for a given list of prices, find total price and indexes of price
 *         which are sold at MRP
 * 
 *         a discount will be given on MRP when there is any price available on
 *         the right of index which is first smaller or equal to the MRP
 *
 */
public class SaleTotal {
	public static void main(String[] args) {
		List<Integer> p = new ArrayList<>();
//		p.add(2);
//		p.add(3);
//		p.add(1);
//		p.add(2);
//		p.add(4);
//		p.add(2);

//		p.add(5);
//		p.add(1);
//		p.add(3);
//		p.add(4);
//		p.add(6);
//		p.add(2);

//		p.add(4);
//		p.add(2);
//		p.add(6);
//		p.add(1);
//		p.add(3);

		p.add(5);
		p.add(4);
		p.add(3);
		p.add(2);
		p.add(1);
		finalPrice(p);
	}

	public static void finalPrice(List<Integer> prices) {
		// Write your code h
		int n = prices.size();
		int[] mins = new int[n];
		mins[n - 1] = prices.get(n - 1);
		for (int i = n - 2; i >= 0; i--) {
			if (prices.get(i) <= mins[i + 1]) {
				mins[i] = prices.get(i);
			} else {
				if (prices.get(i) >= prices.get(i + 1)) {
					mins[i] = Math.max(prices.get(i + 1), mins[i + 1]);
				} else {
					mins[i] = mins[i + 1];
				}
			}
		}

		List<Integer> res = new ArrayList<>();
		int total = 0;
		for (int i = 0; i < n - 1; i++) {
			if (prices.get(i) == mins[i]) {
				if (mins[i] != mins[i + 1] && mins[i] != prices.get(i + 1)) {
					res.add(i);
					total += mins[i];
				}
			} else {
				total += prices.get(i) - mins[i];
			}
		}

		total += prices.get(n - 1);
		res.add(n - 1);

		System.out.println(total);
		for (int r : res) {
			System.out.print(r + " ");
		}
	}

}
