package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author dagraw1
 *
 *         Given an array S of n integers, are there elements a, b, c in S such
 *         that a + b + c = 0? Find all unique triplets in the array which gives
 *         the sum of zero.
 * 
 *         Note:
 * 
 *         Elements in a triplet (a,b,c) must be in non-descending order. (ie, a
 *         ≤ b ≤ c) The solution set must not contain duplicate triplets. For
 *         example, given array S = {-1 0 1 2 -1 -4}, A solution set is: (-1, 0,
 *         1) (-1, -1, 2)
 */

public class ThreeSumZero {
	public ArrayList<ArrayList<Integer>> threeSum(List<Integer> a) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<>();

		Collections.sort(a);
		System.out.println(a);
		int low = 0, mid = 0, high = 0;
		Integer last = null;
		while (low < a.size() - 2) {

			if (last != null && a.get(low) == last.intValue()) {
				low++;
				continue;
			}
			mid = low + 1;
			high = a.size() - 1;
			while (mid < high) {
				int sum = a.get(low) + a.get(mid) + a.get(high);
				if (sum == 0) {
					ArrayList<Integer> res = new ArrayList<>();
					res.add(a.get(low));
					res.add(a.get(mid));
					res.add(a.get(high));
					results.add(res);

					mid++;
					high--;

					while (mid < a.size() - 1 && a.get(mid) == a.get(mid - 1)) {
						mid++;
					}
					;
					while (high < a.size() - 1 && a.get(high) == a.get(high + 1)) {
						high--;
					}
					;
				} else if (sum < 0) {
					mid++;
				} else {
					high--;
				}
			}
			last = a.get(low);
			low++;
		}

		return results;
	}

	public static void main(String[] args) {
		Integer[] array = { -31013930, -31013930, 9784175, 21229755 };
		ThreeSumZero tsz = new ThreeSumZero();
		ArrayList<ArrayList<Integer>> list = tsz.threeSum(Arrays.asList(array));
		System.out.println(list);
	}
}
