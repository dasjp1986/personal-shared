package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given a column title as appears in an Excel sheet, return its
 *         corresponding column number.
 * 
 *         Example:
 * 
 *         A -> 1
 * 
 *         B -> 2
 * 
 *         C -> 3
 * 
 *         ...
 * 
 *         Z -> 26
 * 
 *         AA -> 27
 * 
 *         AB -> 28
 */
public class ExcelColumnNumber {
	//Time complexity- O(n)
	public static int titleToNumber(String A) {
		int num = 0;
		for (int i = 0; i < A.length(); i++) {
			num = num * 26 + A.charAt(i) - 64;
		}

		return num;
	}
	
	public static void main(String[] args) {
		System.out.println(titleToNumber("A"));
		System.out.println(titleToNumber("AB"));
		System.out.println(titleToNumber("BA"));
		System.out.println(titleToNumber("AAB"));
	}
}
