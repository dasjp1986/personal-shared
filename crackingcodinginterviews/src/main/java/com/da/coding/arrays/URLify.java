package com.da.coding.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *	Given a String with spaces, replace all the spaces by %20
 *	My Name is Dharmendra
 *  My%20Name%20is%20Dharmendra
 */
public class URLify {
	//Time Complexity- O(N+spaces*spaceTokenSize)~ O(N)
	//Space Complexity- O(N+spaces*spaceTokenSize)~ O(N)
	public static String replaceSpaces(String inputString, String spaceToken) {
		char[] charArray = inputString.toCharArray();
		int spaceCount = 0;
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == ' ')
				spaceCount++;
		}

		char[] resultArray = new char[charArray.length + (spaceToken.length() - 1) * spaceCount];
		int k = 0;
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == ' ') {
				for (int j = 0; j < spaceToken.length(); j++) {
					resultArray[k++] = spaceToken.charAt(j);
				}
			} else {
				resultArray[k++] = charArray[i];
			}
		}
		return new String(resultArray);
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(replaceSpaces(bfr.readLine(), "%20"));
		bfr.close();
	}
}
