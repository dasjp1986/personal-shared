package com.da.coding.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author dagraw1
 *
 *
 *         Find all triplets with zero sum
 * 
 *         Given an array of distinct elements. The task is to find triplets in
 *         array whose sum is zero.
 * 
 *         Examples :
 * 
 *         Input : arr[] = {0, -1, 2, -3, 1} Output : 0 -1 1 2 -3 1
 * 
 *         Input : arr[] = {1, -2, 1, 0, 5} Output : 1 -2 1
 * 
 */
public class TripletsWithSumZero {
	//Time Complexity - O(N*N) and Space O(N)
	public static void sumZeroHashing(int[] a) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = i + 1; j < a.length; j++) {
				Integer index = map.get(a[i] + a[j]);
				if (index != null && index != i && index != j) {
					System.out.println(a[i] + "," + a[index] + "," + a[j]);
				} else {
					map.put(-a[j], j);
				}
			}
		}
	}

	//Time Complexity - O(N*N) and Space O(N)
	public static void sumZeroSorting(int[] a) {
		Arrays.sort(a);
		int left = 0, right = a.length - 1;
		int sum = 0;
		for (int i = 0; i < a.length - 2; i++) {
			left = i + 1;
			right = a.length - 1;

			while (left < right) {
				sum = a[i] + a[left] + a[right];
				if (sum == 0) {
					System.out.println(a[i] + "," + a[left] + "," + a[right]);
					left++;
					right--;
				} else if (sum > 0) {
					right--;
				} else {
					left++;
				}
			}
		}
	}

	public static void main(String[] args) {
		int[] a = { 1, -1, 0, 2, -3 };

		sumZeroHashing(a);
		sumZeroSorting(a);
	}

}
