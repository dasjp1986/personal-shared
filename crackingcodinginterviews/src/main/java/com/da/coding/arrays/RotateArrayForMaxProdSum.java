package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         For given array, perform rotations such that sum of i*a[i] is max
 * 
 *         a=[8,4,1,2]
 * 
 *         after two left rotations max sum = 0*1+ 1*2+ 2*8+ 3*4= 30
 * 
 *         Expected TE= O(N)
 */
public class RotateArrayForMaxProdSum {
	
	// R(j)= R(j-1)+ arraySum- n*array[n-j];
	public int maxProduct(int[] array) {

		int arraySum = 0;
		int nextProduct = 0;
		int n = array.length;

		for (int i = 0; i < n; i++) {
			arraySum = arraySum + array[i];
			nextProduct = nextProduct + i * array[i];
		}

		int maxProduct = nextProduct;

		for (int i = 1; i < n; i++) {
			nextProduct = nextProduct + arraySum - n * array[n - i];
			if (nextProduct > maxProduct) {
				maxProduct = nextProduct;
			}
		}

		return maxProduct;
	}

	public static void main(String[] args) {
		int[] array = { 1,3,2 };
		RotateArrayForMaxProdSum rotate = new RotateArrayForMaxProdSum();
		System.out.println(rotate.maxProduct(array));
	}
}
