package com.da.coding.arrays;

/**
 * 
 * @author dagraw1
 *
 *         Given a sorted array in which all elements appear twice (one after
 *         one) and one element appears only once. Find that element in O(log n)
 *         complexity.
 * 
 *         Example:
 * 
 *         Input: arr[] = {1, 1, 3, 3, 4, 5, 5, 7, 7, 8, 8} Output: 4
 * 
 *         Input: arr[] = {1, 1, 3, 3, 4, 4, 5, 5, 7, 7, 8} Output: 8
 */
public class ElementExactlyOnce {
	
	public static void main(String[] args) {
		int[] a={1, 1, 3, 3, 4, 4, 5, 5, 7, 7, 8};
		System.out.println(find(a));
	}
	
	//Time Complexity- O(LogN)
	public static int find(int[] a) {
		if (a.length <= 1) {
			return 0;
		}

		return f(a, 0, a.length - 1);
	}

	private static int f(int[] a, int start, int end) {
		if (start > end) {
			return -1;
		}

		if (start == end) {
			return a[start];
		}

		int mid = (start + end) / 2;

		if (mid % 2 == 0) {
			if (mid + 1 < a.length && a[mid] == a[mid + 1]) {
				return f(a, mid + 1, end);
			} else {
				return f(a, start, mid-1);
			}
		} else {
			if (mid - 1 >= 0 && a[mid] == a[mid - 1]) {
				return f(a, mid + 1, end);
			} else {
				return f(a, start, mid - 1);
			}
		}
	}
}
