package com.da.coding.heaps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * 
 * @author dagraw1
 *
 *         Given two arrays A & B of size N each. Find the maximum n elements
 *         from the sum combinations (Ai + Bj) formed from elements in array A
 *         and B.
 * 
 *         For example if A = [1,2], B = [3,4], then possible pair sums can be
 *         1+3 = 4 , 1+4=5 , 2+3=5 , 2+4=6 and maximum 2 elements are 6, 5
 * 
 *         Example:
 * 
 *         N = 4 a[]={1,4,2,3} b[]={2,5,1,6}
 * 
 *         Maximum 4 elements of combinations sum are 10 (4+6), 9 (3+6), 9
 *         (4+5), 8 (2+6)
 */
public class TopNSumOfTwoArray {

	public static void main(String[] args) {
		TopNSumOfTwoArray sums = new TopNSumOfTwoArray();
		List<Integer> a = Arrays.asList(35, -38, -41, -1, -50, -46, 7, 19, -1, 12, 14, -20, 41, -31, -9, -40, -1, 27,
				25, -6, -13, -21, -40, -15, -8, 49, 19, -17, -33, 37, 34, 4, 1, -4, -46, 1, 0, -37, 23, 1, 25, -13, -16,
				-32, 6, 25, -22, -45, -46, -47, -48);
		List<Integer> b = Arrays.asList(-9, 35, -38, 26, -23, -39, 48, -37, 30, 35, 47, 36, 39, 45, 40, -10, -5, 6, -37,
				47, 33, -48, -19, -48, 9, 8, 32, -34, 12, -12, -32, -46, -27, -20, -18, -48, 43, 32, -35, -25, 18, 15,
				14, 7, 10, -44, -1, -44, 12, -35, 5);

		List<Integer> results = sums.solve(a, b);
		System.out.println(results);
	}

	public ArrayList<Integer> solve(List<Integer> A, List<Integer> B) {
		Collections.sort(A, Collections.reverseOrder());
		Collections.sort(B, Collections.reverseOrder());
		int n = A.size();

		PriorityQueue<Integer> pq = new PriorityQueue<>();
		for (int i : A) {
			for (int j : B) {
				int s = i + j;
				if (pq.size() < n) {
					pq.add(s);
				} else {
					if (s > pq.peek()) {
						pq.poll();
						pq.add(s);
					} else {
						break;
					}
				}
			}
		}

		ArrayList<Integer> ret = new ArrayList<>(pq);
		Collections.sort(ret, Collections.reverseOrder());
		return ret;

	}

}
