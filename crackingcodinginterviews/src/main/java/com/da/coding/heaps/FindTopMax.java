package com.da.coding.heaps;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         Given a stream of positive numbers find top k largest numbers
 * 
 *         ex- k=3
 * 
 *         input: 1,2,3,4,5,6,7 output: 7,6,5
 * 
 *         input: 1,2,3,4,5,6,7,3,9 output: 9,7,6
 * 
 *         ex- k=5
 * 
 *         input: 1,2,3 output: 3,2,1,-1,-1
 * 
 * 
 */
public class FindTopMax {
	int[] minHeap;

	public FindTopMax(int n) {
		this.minHeap = new int[n];
		for (int i = 0; i < minHeap.length; i++) {
			minHeap[i] = -1;
		}
	}

	public Integer[] topMax(int num) {
		Integer[] results = new Integer[minHeap.length];
		add(num);
		for (int i = 0; i < results.length; i++) {
			results[i] = minHeap[i];
		}
		Arrays.sort(results, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}
		});
		return results;
	}

	private void add(int num) {
		if (num > minHeap[0]) {
			minHeap[0] = num;
			heapify(0);
		}
	}

	private void heapify(int index) {
		int minIndex = index;
		if (2 * index + 1 < minHeap.length && minHeap[2 * index + 1] < minHeap[minIndex]) {
			minIndex = 2 * index + 1;
		}
		if (2 * index + 2 < minHeap.length && minHeap[2 * index + 2] < minHeap[minIndex]) {
			minIndex = 2 * index + 2;
		}

		if (minIndex != index) {
			int temp = minHeap[index];
			minHeap[index] = minHeap[minIndex];
			minHeap[minIndex] = temp;

			heapify(minIndex);
		}
	}

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int num;
		FindTopMax findTopMax = new FindTopMax(3);
		while ((num = scn.nextInt()) != -1) {
			System.out.println(Arrays.toString(findTopMax.topMax(num)));
		}
		scn.close();
	}

}
