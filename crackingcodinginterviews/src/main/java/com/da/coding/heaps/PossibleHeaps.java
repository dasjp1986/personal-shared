package com.da.coding.heaps;
//Wrong solution
public class PossibleHeaps {

	public static int solve(int A) {
		int count = 0;
		if (A == 1) {
			return 1;
		}
		for (int i = 0; i < A / 2; i++) {
			if ((2 * i + 1) < A) {
				count = count + 1;
			}
			if ((2 * i + 2) < A) {
				count = count * 2;
			}
		}

		return count;
	}

	public static void main(String[] args) {
		System.out.println(solve(4));
	}

}
