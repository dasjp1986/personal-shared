package com.da.coding.heaps;

import com.da.coding.trees.AbstractBinaryTree;
import com.da.coding.trees.CompleteBinaryTree;
import com.da.coding.trees.TNode;

/**
 * 
 * @author dagraw1 Binary Heap is essentially a complete binary tree where all
 *         the left node filled before right nodes.
 * 
 *         Min Heap will have parent node smaller than its left and right node
 *         and so on... so root node will be the smallest node in the heap.
 */
public class HeapUsingCompleteBinaryTree {
	AbstractBinaryTree<Integer> heap;

	public HeapUsingCompleteBinaryTree() {
		this.heap = new CompleteBinaryTree<>();
	}

	// Time Complexity- O(height of the tree)->O(logN)
	public void add(Integer value) {
		TNode<Integer> node = heap.add(value);
		if (!isValidHeap(node.getParent())) {
			heapify(node.getParent(), HEAPIFY_DIRECTION.BOTTOM_TO_TOP);
		}
	}

	//Time Complexity- O(LogN)
	public Integer extractMin() {
		if (heap.isEmpty()) {
			throw new RuntimeException("Heap is empty");
		}

		//O(LogN)
		TNode<Integer> lastNode = heap.getLastNode();
		swap(lastNode, heap.getRoot());
		removeLastNode(lastNode);

		if (!isValidHeap(heap.getRoot())) {
			//O(LogN)
			heapify(heap.getRoot(), HEAPIFY_DIRECTION.TOP_TO_BOTTOM);
		}
		return lastNode.getValue();
	}

	//Time Complexity- O(LogN)
	private void removeLastNode(TNode<Integer> lastNode) {
		TNode<Integer> parent = lastNode.getParent();
		if (parent == null) {
			heap.setRoot(null);
		} else {
			if (parent.getLeft() == lastNode) {
				parent.setLeft(null);
			} else {
				parent.setRight(null);
			}
		}
	}

	private boolean isValidHeap(TNode<Integer> parent) {
		if (parent != null) {
			if (parent.getLeft() != null && parent.getLeft().getValue().intValue() < parent.getValue().intValue()) {
				return false;
			}

			if (parent.getRight() != null && parent.getRight().getValue().intValue() < parent.getValue().intValue()) {
				return false;
			}
		}
		return true;
	}

	//Time Complexity- O(LogN)
	public void heapify(TNode<Integer> node, HEAPIFY_DIRECTION dir) {
		TNode<Integer> left = node.getLeft();
		TNode<Integer> right = node.getRight();
		TNode<Integer> shiftNode = null;

		if (left != null && right != null) {
			if (left.getValue().intValue() < right.getValue().intValue()) {
				shiftNode = left;
			} else {
				shiftNode = right;
			}
		} else if (left != null) {
			shiftNode = left;
		} else if (right != null) {
			shiftNode = right;
		}

		swap(shiftNode, node);

		TNode<Integer> nodeTrav = null;
		if (dir.equals(HEAPIFY_DIRECTION.BOTTOM_TO_TOP)) {
			nodeTrav = node.getParent();
		} else {
			nodeTrav = shiftNode;
		}
		if (!isValidHeap(nodeTrav)) {
			heapify(nodeTrav, dir);
		}
	}

	public void print() {
		System.out.println("Printing Heap...");
		heap.bfs();
	}
	
	public boolean isEmpty(){
		return heap.isEmpty();
	}

	private void swap(TNode<Integer> node, TNode<Integer> parent) {
		Integer temp = node.getValue();
		node.setValue(parent.getValue());
		parent.setValue(temp);
	}

	public static void main(String[] args) {
		HeapUsingCompleteBinaryTree heap = new HeapUsingCompleteBinaryTree();
		heap.add(4);
		heap.add(10);
		heap.add(50);
		heap.print();

		heap.add(25);
		heap.print();

		heap.add(9);
		heap.print();

		heap.add(6);
		heap.print();

		heap.add(8);
		heap.print();

		heap.add(3);
		heap.print();

		heap.add(1);
		heap.print();
		
		while(!heap.isEmpty()){
			System.out.println(heap.extractMin());
		}
	}
}

enum HEAPIFY_DIRECTION {
	TOP_TO_BOTTOM, BOTTOM_TO_TOP
}