package com.da.coding.heaps;

import java.util.Random;

public class HeapUsingArray {
	private int[] heap;
	private int lastIndex;
	private int capacity;

	public HeapUsingArray(int capacity) {
		this.capacity = capacity;
		this.heap = new int[capacity];
	}

	// Time complexity- O(logn)
	public void add(int num) {
		if (lastIndex == capacity) {
			throw new RuntimeException("Heap is already full");
		}
		heap[lastIndex++] = num;
		heapifyUp(lastIndex - 1);
	}

	// Time complexity- O(logn)
	private void heapifyUp(int index) {
		int parent = (index - 1) / 2;

		if (heap[index] < heap[parent]) {
			int temp = heap[parent];
			heap[parent] = heap[index];
			heap[index] = temp;
			heapifyUp(parent);
		}
	}

	// Time complexity- O(logn)
	private void heapifyDown(int parent) {
		int minIndex = parent;
		if ((2 * parent + 1) < lastIndex && heap[2 * parent + 1] < heap[minIndex]) {
			minIndex = 2 * parent + 1;
		}

		if ((2 * parent + 2) < lastIndex && heap[2 * parent + 2] < heap[minIndex]) {
			minIndex = 2 * parent + 2;
		}

		if (minIndex != parent) {
			int temp = heap[parent];
			heap[parent] = heap[minIndex];
			heap[minIndex] = temp;
			heapifyDown(minIndex);
		}
	}

	public int extractTop() {
		if (lastIndex == 0) {
			throw new RuntimeException("Heap is empty");
		}

		int top = heap[0];
		heap[0] = heap[lastIndex - 1];
		lastIndex--;
		heapifyDown(0);
		return top;
	}

	private void print() {
		System.out.println();
		for (int i = 0; i < lastIndex; i++) {
			System.out.print(heap[i] + ", ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		int n = 10;
		HeapUsingArray heapUsingArray = new HeapUsingArray(n);
		Random ran = new Random();
		for (int i = 0; i < n; i++) {
			heapUsingArray.add(ran.nextInt(n * 10));
		}
		heapUsingArray.print();
		for (int i = 0; i < n; i++) {
			System.out.println(heapUsingArray.extractTop());
		}
	}
}
