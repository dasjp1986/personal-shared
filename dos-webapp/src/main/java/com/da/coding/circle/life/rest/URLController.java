package com.da.coding.circle.life.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.da.coding.circle.life.common.DOSRequest;
import com.da.coding.circle.life.common.DOSResponse;

@RestController
public class URLController {
	private static final Logger LOGGER = LoggerFactory.getLogger(URLController.class);

	@RequestMapping(value = "/start", method = RequestMethod.POST)
	public ResponseEntity<DOSResponse> start(@RequestBody DOSRequest request) {
		LOGGER.info("IN start()");
		return null;
	}

}
