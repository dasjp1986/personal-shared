package com.da.coding.circle.life;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class DosWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DosWebappApplication.class, args);
	}

}

