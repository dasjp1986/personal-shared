package com.da.coding.circle.life.common;

import java.io.Serializable;

public class DOSRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String sessionId;
	private String url;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
