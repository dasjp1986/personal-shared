package com.da.coding.proxying;

import org.springframework.aop.framework.ProxyFactory;

public class ProxyGenerator {
	public static void main(String[] args) {
		ProxyFactory proxyFactory= new ProxyFactory(new SimplePojo());
		//proxyFactory.addInterface(Pojo.class);
	
		
		SimplePojo pojo= (SimplePojo) proxyFactory.getProxy();
		pojo.foo();
	}
}
