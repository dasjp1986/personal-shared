package com.da.coding.proxying;

public class SimplePojo{
	
	public SimplePojo() {
		System.out.println("Running constrcutor...");
	}
	
	public void foo(){
		System.out.println("foo called...");
		this.bar();
	}
	
	public void bar(){
		System.out.println("bar called...");
	}
}
