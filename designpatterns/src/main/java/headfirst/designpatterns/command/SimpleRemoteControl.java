package headfirst.designpatterns.command;

public class SimpleRemoteControl {
	Command slot;

	public Command getSlot() {
		return slot;
	}

	//a different command object can be reset
	public void setSlot(Command slot) {
		this.slot = slot;
	}
	
	public void pressButton(){
		slot.execute();
	}

}
