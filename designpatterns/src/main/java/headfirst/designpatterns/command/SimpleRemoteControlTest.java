package headfirst.designpatterns.command;

public class SimpleRemoteControlTest {
	public static void main(String[] args) {
		SimpleRemoteControl remote= new SimpleRemoteControl();
		Command livingLightOn= new LightOnCommand(new LivingRoomLight());
		remote.setSlot(livingLightOn);
		remote.pressButton();
		
		SimpleRemoteControl remote2= new SimpleRemoteControl();
		Command livingLightOn2= new LightOnCommand(new GarageLight());
		remote2.setSlot(livingLightOn2);
		remote2.pressButton();
	}
}
