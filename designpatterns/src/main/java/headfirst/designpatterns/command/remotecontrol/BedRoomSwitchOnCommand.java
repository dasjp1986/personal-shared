package headfirst.designpatterns.command.remotecontrol;

public class BedRoomSwitchOnCommand implements SwitchOnCommand{

	LightSwitch lightSwitch;
	
	public BedRoomSwitchOnCommand() {
		this.lightSwitch= new BedRoomLightSwitch();
	}
	
	@Override
	public void execute() {
		lightSwitch.switchOn();
	}

}
