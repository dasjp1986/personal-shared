package headfirst.designpatterns.command.remotecontrol;

public class RemoteControl {
	SwitchOnCommand[] ons;
	SwitchOffCommand[] offs;
	boolean[] toggle;

	public RemoteControl() {
		ons = new SwitchOnCommand[2];
		offs = new SwitchOffCommand[2];
		toggle = new boolean[2];
	}

	public void pressButton(int slot) {
		if (toggle[slot]){
			ons[slot].execute();
			toggle[slot]=false;
		}
		else{
			offs[slot].execute();
			toggle[slot]=true;
		}

	}

	public void setCommand(int slot, SwitchOnCommand onCommand, SwitchOffCommand offCommand) {
		ons[slot] = onCommand;
		offs[slot] = offCommand;
	}
}
