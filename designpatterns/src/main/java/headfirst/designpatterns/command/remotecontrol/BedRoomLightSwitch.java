package headfirst.designpatterns.command.remotecontrol;

//Receiver object
public class BedRoomLightSwitch implements LightSwitch{

	@Override
	public void switchOn() {
		System.out.println("BedRoom Light is switched on");
	}

	@Override
	public void switchOff() {
		System.out.println("BedRoom Light is switched off");
		
	}

}
