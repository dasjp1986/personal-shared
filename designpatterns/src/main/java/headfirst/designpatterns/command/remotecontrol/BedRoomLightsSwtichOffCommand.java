package headfirst.designpatterns.command.remotecontrol;

public class BedRoomLightsSwtichOffCommand implements SwitchOffCommand {

	LightSwitch lightSwitch;

	public BedRoomLightsSwtichOffCommand() {
		this.lightSwitch= new BedRoomLightSwitch();
	}

	@Override
	public void execute() {
		lightSwitch.switchOff();
	}

}
