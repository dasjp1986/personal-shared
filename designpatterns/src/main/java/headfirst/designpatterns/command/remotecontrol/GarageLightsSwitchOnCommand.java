package headfirst.designpatterns.command.remotecontrol;

public class GarageLightsSwitchOnCommand implements SwitchOnCommand {

	LightSwitch lightSwitch;

	public GarageLightsSwitchOnCommand() {
		this.lightSwitch= new GarageLightSwitch();
	}

	@Override
	public void execute() {
		lightSwitch.switchOn();
	}

}
