package headfirst.designpatterns.command.remotecontrol;

public interface LightSwitch {
	void switchOn();

	void switchOff();
}
