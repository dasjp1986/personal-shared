package headfirst.designpatterns.command.remotecontrol;

public class GarageLightSwitch implements LightSwitch {

	@Override
	public void switchOn() {
		System.out.println("Garage Light is switched on");
	}

	@Override
	public void switchOff() {
		System.out.println("Garage Light is switched off");
	}

}
