package headfirst.designpatterns.command.remotecontrol;

public interface SwitchOnCommand {
	void execute();
}
