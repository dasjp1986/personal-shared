package headfirst.designpatterns.command.remotecontrol;

public interface SwitchOffCommand {
	void execute();
}
