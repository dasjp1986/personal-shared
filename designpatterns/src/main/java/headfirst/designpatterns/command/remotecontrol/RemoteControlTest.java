package headfirst.designpatterns.command.remotecontrol;

public class RemoteControlTest{
	public static void main(String[] args) {
		RemoteControl rtc = new RemoteControl();
		SwitchOnCommand bedOn = new BedRoomSwitchOnCommand();
		SwitchOffCommand bedOff = new BedRoomLightsSwtichOffCommand();
		rtc.setCommand(0, bedOn, bedOff);
		

		SwitchOnCommand garageOn = new GarageLightsSwitchOnCommand();
		SwitchOffCommand garageOff = new GarageLightsSwitchOffCommand();
		rtc.setCommand(1, garageOn, garageOff);

		for (int i = 0; i < 5; i++) {
			rtc.pressButton(0);
			rtc.pressButton(1);
		}
	}

}
