package headfirst.designpatterns.command.remotecontrol;

public class GarageLightsSwitchOffCommand implements SwitchOffCommand {

	LightSwitch lightSwitch;
	
	public GarageLightsSwitchOffCommand() {
		this.lightSwitch= new GarageLightSwitch();
	}

	@Override
	public void execute() {
		lightSwitch.switchOff();
	}

}
