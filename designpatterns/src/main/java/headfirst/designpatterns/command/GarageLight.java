package headfirst.designpatterns.command;

public class GarageLight extends Light{

	@Override
	void switchOn() {
		System.out.println("Garage lights are on...");
	}

}
