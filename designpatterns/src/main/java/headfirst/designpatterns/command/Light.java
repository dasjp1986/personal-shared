package headfirst.designpatterns.command;

public abstract class Light {
	 abstract void switchOn();
}
