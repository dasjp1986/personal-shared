package headfirst.designpatterns.command;

public class LivingRoomLight extends Light{

	@Override
	void switchOn() {
		System.out.println("Light are switched on...");
	}

}
