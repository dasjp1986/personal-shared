package headfirst.designpatterns.singleton;

public class Singleton {
	private volatile static Singleton singleton;
	
	public static Singleton getInstance(){
		if(singleton==null){
			synchronized (Singleton.class) {
				if(singleton==null)
					singleton= new Singleton();
			}
		}
		
		return singleton;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 50; i++) {
			new Thread(new JobSingleton()).start();
		}
	}
	
}

class JobSingleton implements Runnable{

	@Override
	public void run() {
		System.out.println(Singleton.getInstance());
	}
	
}
