package headfirst.designpatterns.singleton;

public class ChocolateBoiler {
	private boolean isEmpty;
	private boolean isBoiled;
	
	private volatile static ChocolateBoiler singleton;
	
	private ChocolateBoiler(){
		isBoiled= false;
		isEmpty= true;
	}
	
	public synchronized void fill() throws InterruptedException{
		if(isEmpty){
			isEmpty= false;
			isBoiled= false;
			System.out.println(this+ " Fill the boiler for instance ");
			Thread.sleep(1000);
		}
	}
	
	public synchronized void drain() throws InterruptedException{
		if(!isEmpty && isBoiled){
			isEmpty= true;
			System.out.println(this+" drain the boiler");
			Thread.sleep(1000);
		}
	}
	
	public synchronized void boil() throws InterruptedException{
		if(!isEmpty && !isBoiled){
			isBoiled= true;
			System.out.println(this+" boiling the milk/chocolates");
			Thread.sleep(1000);
		}
	}
	
	public static ChocolateBoiler getInstance(){
		if(singleton==null){
			synchronized (ChocolateBoiler.class) {
				if(singleton==null){
					singleton= new ChocolateBoiler();
				}
			}
		}
		
		return singleton;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			new Thread(new BoilerJob()).start();
		}
	}
	
}

class BoilerJob implements Runnable{

	@Override
	public void run() {
		ChocolateBoiler boiler = ChocolateBoiler.getInstance();
		try {
			boiler.fill();
			boiler.boil();
			boiler.drain();
		} catch (InterruptedException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
	}
	
}
