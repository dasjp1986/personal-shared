package headfirst.designpatterns.composite;

import java.util.Iterator;

public class NullIterator implements Iterator<AbstractMenu> {

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public AbstractMenu next() {
		return null;
	}

}
