package headfirst.designpatterns.composite;

import java.util.Iterator;

import javax.naming.OperationNotSupportedException;

public class Waitress {
	private AbstractMenu allMenu;

	public Waitress(AbstractMenu allMenu) {
		this.allMenu = allMenu;
	}

	public void printAllMenu() {
		Iterator<AbstractMenu> iterator = allMenu.createIterator();
		while (iterator.hasNext()) {
			AbstractMenu abstractMenu = (AbstractMenu) iterator.next();
			System.out.println(abstractMenu.getDescription());
			try {
				System.out.println(abstractMenu.getPrice());
			} catch (OperationNotSupportedException e) {
			}
			System.out.println(abstractMenu.isVegeterian());
		}
	}

}
