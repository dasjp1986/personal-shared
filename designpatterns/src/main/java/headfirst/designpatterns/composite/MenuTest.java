package headfirst.designpatterns.composite;

import javax.naming.OperationNotSupportedException;

public class MenuTest {
	public static void main(String[] args) throws OperationNotSupportedException {
		AbstractMenu allMenu = new Menu("All Menu");

		AbstractMenu lunchMenu = new Menu("Lunch Menu");
		AbstractMenu lunchItem1 = new MenuItem("Lunch- Butter Chicken", 120.00, false);
		//AbstractMenu lunchItem2 = new MenuItem("Lunch- Paneer Handi", 100.00, true);
		lunchMenu.add(lunchItem1);
		//lunchMenu.add(lunchItem2);
		AbstractMenu lunchBeverages = new Menu("Lunch Beverage Menu");
		AbstractMenu lunchBev1 = new MenuItem("Lunch-Coffee", 10.5, true);
		lunchBeverages.add(lunchBev1);
		lunchMenu.add(lunchBeverages);
		allMenu.add(lunchMenu);

//		AbstractMenu dinnerMenu = new Menu("Dinner Menu");
//		AbstractMenu dinnerItem1 = new MenuItem("Dinner- Cheese Pasta", 120.00, true);
//		AbstractMenu dinnerItem2 = new MenuItem("Dinner- Ravioli", 100.00, true);
//		dinnerMenu.add(dinnerItem1);
//		dinnerMenu.add(dinnerItem2);
//		AbstractMenu dinnerDesserts = new Menu("Dinner Dessert Menu");
//		AbstractMenu dinnerDes1 = new MenuItem("Dinner-Cheese Cake", 10.5, true);
//		dinnerDesserts.add(dinnerDes1);
//		dinnerMenu.add(dinnerDesserts);
//		allMenu.add(dinnerMenu);

		Waitress waitress = new Waitress(allMenu);
		waitress.printAllMenu();
	}
}
