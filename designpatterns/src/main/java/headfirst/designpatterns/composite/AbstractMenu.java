package headfirst.designpatterns.composite;

import java.util.Iterator;

import javax.naming.OperationNotSupportedException;

public abstract class AbstractMenu {
	// common methods
	protected abstract String getDescription();

	protected abstract Iterator<AbstractMenu> createIterator();

	// menu methods
	protected void add(AbstractMenu menu) throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Method not supported...");
	}

	protected void remove(AbstractMenu menu) throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Method not supported...");
	}

	// menu items methods
	protected boolean isVegeterian() {
		return false;
	}

	protected double getPrice() throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Method not supported...");
	}

}
