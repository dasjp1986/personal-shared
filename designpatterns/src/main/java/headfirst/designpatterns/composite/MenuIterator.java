package headfirst.designpatterns.composite;

import java.util.Iterator;
import java.util.Stack;

public class MenuIterator implements Iterator<AbstractMenu> {

	private Stack<Iterator<AbstractMenu>> stack;

	public MenuIterator(Iterator<AbstractMenu> iterator) {
		stack = new Stack<>();
		stack.push(iterator);
	}

	@Override
	public boolean hasNext() {
		if (stack.isEmpty())
			return false;

		Iterator<AbstractMenu> iterator = stack.peek();
		if (!iterator.hasNext()) {
			stack.pop();
			return hasNext();
		}

		return true;
	}

	@Override
	public AbstractMenu next() {
		if(hasNext()){
			Iterator<AbstractMenu> iterator = stack.peek();
			AbstractMenu abstractMenu = iterator.next();
			if (abstractMenu instanceof Menu) {
				stack.push(abstractMenu.createIterator());
			}
			return abstractMenu;
		}else{
			return null;
		}
		
	}

}
