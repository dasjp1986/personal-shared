package headfirst.designpatterns.composite;

import java.util.Iterator;

import javax.naming.OperationNotSupportedException;

public class MenuItem extends AbstractMenu {
	private String description;
	private Double price;
	private boolean isVeg;

	public MenuItem(String description) {
		this.description = description;
	}

	public MenuItem(String description, Double price, boolean isVeg) {
		super();
		this.description = description;
		this.price = price;
		this.isVeg = isVeg;
	}

	@Override
	protected String getDescription() {
		return this.description;
	}

	@Override
	protected Iterator<AbstractMenu> createIterator() {
		return new NullIterator();
	}

	@Override
	protected boolean isVegeterian() {
		return isVeg;
	}

	@Override
	protected double getPrice() throws OperationNotSupportedException {
		return price;
	}

}
