package headfirst.designpatterns.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.naming.OperationNotSupportedException;

public class Menu extends AbstractMenu {

	private String description;

	private List<AbstractMenu> menuList;

	public Menu(String description) {
		this.description = description;
		this.menuList = new ArrayList<>();
	}

	@Override
	protected String getDescription() {
		return description;
	}

	@Override
	protected void add(AbstractMenu menu) throws OperationNotSupportedException {
		menuList.add(menu);
	}

	@Override
	protected void remove(AbstractMenu menu) throws OperationNotSupportedException {
		menuList.remove(menu);
	}

	@Override
	protected Iterator<AbstractMenu> createIterator() {
		return new MenuIterator(menuList.iterator());
	}

}
