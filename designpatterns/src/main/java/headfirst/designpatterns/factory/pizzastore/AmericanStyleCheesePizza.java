package headfirst.designpatterns.factory.pizzastore;

//concrete product
public class AmericanStyleCheesePizza extends Pizza{
	IngredientFactory factory;
	
	public AmericanStyleCheesePizza(IngredientFactory factory) {
		super("AmericanStyleCheesePizza");
		this.factory=factory;
	}

	@Override
	public void prepare() {
		this.cheese= factory.createCheese("AMERICAN");
		this.dough= factory.createDough("AMERICAN");
		this.sauce= factory.createSauce("AMERICAN");
		
		System.out.println("Pizza-"+this.getDescription()+" Ingredients- Cheese:"+this.cheese.cheeseType
				+ " dough:"+this.dough.thickNess+" sauce:"+this.sauce.sauceType);
	}

}
