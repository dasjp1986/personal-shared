package headfirst.designpatterns.factory.pizzastore;

public class AmericanIngredientFactory extends IngredientFactory{

	@Override
	public Cheese createCheese(String type) {
		Cheese cheese=null;
		if(type=="AMERICAN")
			cheese= new MozzerellaAmericanCheese();
		return cheese;
	}

	@Override
	public Sauce createSauce(String type) {
		Sauce sauce= null;
		if(type=="AMERICAN")
			sauce= new MusturdAmericanSauce();
		return sauce;
	}

	@Override
	public Dough createDough(String type) {
		Dough dough= null;
		if(type=="AMERICAN")
			dough= new ThickCrustDough();
		return dough;
	}

}
