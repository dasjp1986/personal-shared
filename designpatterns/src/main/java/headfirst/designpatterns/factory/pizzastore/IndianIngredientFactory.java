package headfirst.designpatterns.factory.pizzastore;

public class IndianIngredientFactory extends IngredientFactory{

	@Override
	public Cheese createCheese(String type) {
		Cheese cheese=null;
		if(type=="INDIAN")
			cheese= new CottageIndianCheese();
		return cheese;
	}

	@Override
	public Sauce createSauce(String type) {
		Sauce sauce= null;
		if(type=="INDIAN")
			sauce= new RedIndianSauce();
		return sauce;
	}

	@Override
	public Dough createDough(String type) {
		Dough dough= null;
		if(type=="INDIAN")
			dough= new ThinCrustDough();
		return dough;
	}

}
