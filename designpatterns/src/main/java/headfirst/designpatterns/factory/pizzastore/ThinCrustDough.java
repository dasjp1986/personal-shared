package headfirst.designpatterns.factory.pizzastore;

public class ThinCrustDough extends Dough{

	public ThinCrustDough() {
		super("ThinCrustDough");
	}

}
