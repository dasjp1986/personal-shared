package headfirst.designpatterns.factory.pizzastore;

public class CottageIndianCheese extends Cheese{

	public CottageIndianCheese() {
		super("CottageIndianCheese");
	}

}
