package headfirst.designpatterns.factory.pizzastore;

//concrete product
public class AmericanPorkPizza extends Pizza{
	
	IngredientFactory factory;

	public AmericanPorkPizza(IngredientFactory factory) {
		super("AmericanPorkPizza");
		this.factory= factory;
	}

	@Override
	public void prepare() {
		this.cheese= factory.createCheese("AMERICAN");
		this.dough= factory.createDough("AMERICAN");
		this.sauce= factory.createSauce("AMERICAN");
		
		System.out.println("Pizza-"+this.getDescription()+" Ingredients- Cheese:"+this.cheese.cheeseType
				+ " dough:"+this.dough.thickNess+" sauce:"+this.sauce.sauceType);
	}

}
