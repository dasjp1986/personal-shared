package headfirst.designpatterns.factory.pizzastore;

public abstract class Sauce {
	protected String sauceType;

	public Sauce(String sauceType) {
		this.sauceType = sauceType;
	}
	
}
