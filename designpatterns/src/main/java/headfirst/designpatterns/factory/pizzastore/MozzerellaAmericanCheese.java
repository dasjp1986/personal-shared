package headfirst.designpatterns.factory.pizzastore;

public class MozzerellaAmericanCheese extends Cheese{

	public MozzerellaAmericanCheese() {
		super("MozzerellaAmericanCheese");
	}

}
