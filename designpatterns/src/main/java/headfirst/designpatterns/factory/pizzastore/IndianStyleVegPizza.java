package headfirst.designpatterns.factory.pizzastore;

//concrete product
public class IndianStyleVegPizza extends Pizza {

	IngredientFactory factory;
	
	public IndianStyleVegPizza(IngredientFactory factory) {
		super("IndianStyleVegPizza");
		this.factory= factory;
	}

	@Override
	public void prepare() {
		this.cheese= factory.createCheese("INDIAN");
		this.dough= factory.createDough("INDIAN");
		this.sauce= factory.createSauce("INDIAN");
		
		System.out.println("Pizza-"+this.getDescription()+" Ingredients- Cheese:"+this.cheese.cheeseType
				+ " dough:"+this.dough.thickNess+" sauce:"+this.sauce.sauceType);
	}

}
