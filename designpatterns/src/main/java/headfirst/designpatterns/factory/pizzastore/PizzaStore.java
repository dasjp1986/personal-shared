package headfirst.designpatterns.factory.pizzastore;

//Abstract Creator
public abstract class PizzaStore {
	//factory method
	public abstract Pizza createPizza(String type);
	
	public void orderPizza(String type){
		Pizza pizza= createPizza(type);
		System.out.println(pizza.getDescription()+" is created");
		pizza.prepare();
		pizza.cut();
		pizza.box();
		pizza.deliver();
	}
}
