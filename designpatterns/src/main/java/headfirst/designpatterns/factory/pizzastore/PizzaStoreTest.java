package headfirst.designpatterns.factory.pizzastore;

public class PizzaStoreTest {
	public static void main(String[] args) {
		
		PizzaStore american= new AmericanPizzaStore();
		american.orderPizza("CHEESE");
		
		PizzaStore indian= new IndianPizzaStore();
		indian.orderPizza("MASALA");
	}
}
