package headfirst.designpatterns.factory.pizzastore;
//this is the product class
public abstract class Pizza {
	
	protected String description;
	protected Dough dough;
	protected Cheese cheese;
	protected Sauce sauce;
	
	public String getDescription(){
		return this.description;
	}
	
	public Pizza(String desc){
		this.description= desc;
	}
	
	public abstract void prepare();

	public void cut() {
		System.out.println("Cut Pizza");
	}

	public void box() {
		System.out.println("Box Pizza");
	}

	public void deliver() {
		System.out.println("Deliver Pizza");
	}
}
