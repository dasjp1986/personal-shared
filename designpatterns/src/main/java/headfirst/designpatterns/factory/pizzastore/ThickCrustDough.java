package headfirst.designpatterns.factory.pizzastore;

public class ThickCrustDough extends Dough{

	public ThickCrustDough() {
		super("ThickCrustDough");
	}

}
