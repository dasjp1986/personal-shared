package headfirst.designpatterns.factory.pizzastore;

public abstract class Dough {
	protected String thickNess;
	
	public Dough(String thickNess) {
		this.thickNess= thickNess;
	}
}
