package headfirst.designpatterns.factory.pizzastore;

public class RedIndianSauce extends Sauce{

	public RedIndianSauce() {
		super("RedIndianSauce");
	}

}
