package headfirst.designpatterns.factory.pizzastore;

//abstract factory design pattern
public abstract class IngredientFactory {
   public abstract Cheese createCheese(String type);
   public abstract Sauce createSauce(String type);
   public abstract Dough createDough(String type);
}
