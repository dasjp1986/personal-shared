package headfirst.designpatterns.factory.pizzastore;
//concrete creator
public class IndianPizzaStore extends PizzaStore {

	IngredientFactory factory;
	
	//forced indian store to use only indian ingredient factory
	public IndianPizzaStore() {
		this.factory= new IndianIngredientFactory();
	}
	
	@Override
	public Pizza createPizza(String type) {
		Pizza pizza = null;
		if (type.equals("VEG"))
			pizza = new IndianStyleVegPizza(factory);
		if (type.equals("MASALA"))
			pizza = new IndianHotSpicePizza(factory);
		return pizza;
	}

}
