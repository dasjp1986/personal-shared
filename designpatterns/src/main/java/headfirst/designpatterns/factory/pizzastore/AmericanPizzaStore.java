package headfirst.designpatterns.factory.pizzastore;

//concrete creator
public class AmericanPizzaStore extends PizzaStore {

	IngredientFactory factory;
	
	//forced american store to use only american ingredient factory
	public AmericanPizzaStore() {
		this.factory= new AmericanIngredientFactory();
	}
	
	@Override
	public Pizza createPizza(String type) {
		Pizza pizza = null;
		if (type.equals("PORK"))
			pizza = new AmericanPorkPizza(factory);
		if (type.equals("CHEESE"))
			pizza = new AmericanStyleCheesePizza(factory);

		return pizza;
	}

}
