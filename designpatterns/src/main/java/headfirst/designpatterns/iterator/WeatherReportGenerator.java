package headfirst.designpatterns.iterator;

import java.util.Iterator;

public interface WeatherReportGenerator {
	Iterator<Report> createIterator();

	void generateReport();
}
