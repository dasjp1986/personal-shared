package headfirst.designpatterns.iterator;

import java.util.Iterator;

public class WeatherReportTest {
	public static void main(String[] args) {
		WeatherReportGenerator southIndiaReportGenerator = new SouthIndiaWeatherReportGenerator(2);
		WeatherReportGenerator northIndiaReportGenerator = new NorthIndiaWeatherReportGenerator(3);

		System.out.println("Showing South India Weather Reports");
		printReports(southIndiaReportGenerator.createIterator());

		System.out.println("Showing North India Weather Reports");
		printReports(northIndiaReportGenerator.createIterator());
	}

	private static void printReports(Iterator<Report> iterator) {
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
