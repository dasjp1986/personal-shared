package headfirst.designpatterns.iterator;

import java.util.Iterator;

public class ReportArrayIterator implements Iterator<Report> {

	private int currentIndex;
	private int maxIndex;
	private Report[] reports;

	public ReportArrayIterator(Report[] reports) {
		super();
		this.currentIndex = -1;
		this.reports = reports;
		this.maxIndex = reports.length;
	}

	@Override
	public boolean hasNext() {
		return currentIndex < maxIndex-1;
	}

	@Override
	public Report next() {
		currentIndex++;
		return reports[currentIndex];
	}

}
