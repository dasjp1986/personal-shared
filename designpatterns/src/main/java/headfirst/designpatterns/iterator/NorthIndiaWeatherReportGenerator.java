package headfirst.designpatterns.iterator;

import java.util.Iterator;
import java.util.Random;

public class NorthIndiaWeatherReportGenerator implements WeatherReportGenerator {

	private Report[] reports;
	private int maxReports;

	public NorthIndiaWeatherReportGenerator(int maxReports) {
		this.maxReports = maxReports;
		this.reports = new Report[maxReports];
		
		generateReport();
	}

	@Override
	public Iterator<Report> createIterator() {
		return new ReportArrayIterator(reports);
	}

	@Override
	public void generateReport() {

		Random random = new Random();
		for (int i = 0; i < maxReports; i++) {
			Report report = new Report();
			report.setTemperature(random.nextDouble() * 100);
			report.setHumidity(random.nextDouble() * 200);
			report.setWind(random.nextDouble() * 200);

			reports[i] = report;
		}

	}

}
