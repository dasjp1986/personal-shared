package headfirst.designpatterns.iterator;

public class Report {
	private double temperature;
	private double humidity;
	private double wind;

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public double getWind() {
		return wind;
	}

	public void setWind(double wind) {
		this.wind = wind;
	}

	@Override
	public String toString() {
		return "Report [temperature=" + temperature + ", humidity=" + humidity + ", wind=" + wind + "]";
	}

}
