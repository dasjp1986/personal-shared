package headfirst.designpatterns.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class SouthIndiaWeatherReportGenerator implements WeatherReportGenerator {

	private List<Report> reports;
	private int maxReports;

	public SouthIndiaWeatherReportGenerator(int maxReports) {
		this.maxReports = maxReports;
		this.reports = new ArrayList<>();

		generateReport();
	}

	@Override
	public Iterator<Report> createIterator() {
		return reports.iterator();
	}

	@Override
	public void generateReport() {
		Random random = new Random();
		for (int i = 0; i < maxReports; i++) {
			Report report = new Report();
			report.setTemperature(random.nextDouble() * 100);
			report.setHumidity(random.nextDouble() * 200);
			report.setWind(random.nextDouble() * 200);

			reports.add(report);
		}
	}

}
