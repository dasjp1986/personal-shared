package headfirst.designpatterns.decorator.javaio;

import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class IODecoratorTest {
	public static void main(String[] args) throws IOException {
		InputStream componentAbstract= new FileInputStream("/Users/dagraw1/Documents/Personal/my learnings/System Designing/Object Oriented Principles");
		//FilterInputStream bisDecorator = new BufferedInputStream(componentAbstract);
		FilterInputStream bisDecorator= new LocalFileInputStream(componentAbstract);
		byte[] bytes= new byte[1024];
		while (bisDecorator.read(bytes)!=-1) {
			System.out.println(new String(bytes));
		}
		bisDecorator.close();
	}
}
