package headfirst.designpatterns.decorator.javaio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LocalFileInputStream extends FilterInputStream{

	protected LocalFileInputStream(InputStream in) {
		super(in);
	}
	
	@Override
	public int read() throws IOException {
		int c= super.read();
		return (c==-1?c:Character.toLowerCase(c));
	}
	
	@Override
	public int read(byte[] b) throws IOException {
		int length = super.read(b);
		for (int i = 0; i <length; i++) {
			b[i]= (byte) Character.toLowerCase(b[i]);
		}
		return length;
	}

}
