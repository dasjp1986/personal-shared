package headfirst.designpatterns.decorator.starbucks;

public class HouseBlendComponent extends BeverageComponent{

	@Override
	public float cost() {
		return .99f;
	}
	
	@Override
	public String description() {
		return "House Blend Coffee";
	}

}
