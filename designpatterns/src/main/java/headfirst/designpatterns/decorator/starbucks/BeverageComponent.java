package headfirst.designpatterns.decorator.starbucks;

public abstract class BeverageComponent {
	//default size
	protected SIZE size= SIZE.REGULAR;
	
	public String description(){
		return "Unknown Beverage";
	}
	public abstract float cost();
	public void setSize(SIZE size){
		this.size= size;
	}
	public SIZE getSize(){
		return this.size;
	}
	@Override
	public String toString() {
		return "BeverageComponent [size=" + size + ", description()=" + description() + ", cost()=" + cost() + "]";
	}
	
}
