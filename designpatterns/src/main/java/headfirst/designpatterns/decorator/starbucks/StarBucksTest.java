package headfirst.designpatterns.decorator.starbucks;

public class StarBucksTest {
	public static void main(String[] args) {
		BeverageComponent hb= new HouseBlendComponent();
		BeverageComponent mocha= new MochaDecorator(hb);
		BeverageComponent mochaDouble= new MochaDecorator(mocha);
		BeverageComponent coffee= new MilkDecorator(mochaDouble);
		coffee.setSize(SIZE.LARGE);
		
		System.out.println(coffee.toString());
	}
}
