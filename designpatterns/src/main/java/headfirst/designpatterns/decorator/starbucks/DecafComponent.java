package headfirst.designpatterns.decorator.starbucks;

public class DecafComponent extends BeverageComponent{

	@Override
	public float cost() {
		return 3.99f;
	}
	
	@Override
	public String description() {
		return "Decaf coffee";
	}

}
