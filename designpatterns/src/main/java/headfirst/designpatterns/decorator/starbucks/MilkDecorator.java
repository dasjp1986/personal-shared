package headfirst.designpatterns.decorator.starbucks;

public class MilkDecorator extends CondimentsDecorator{
	
	BeverageComponent beverage;
	
	public MilkDecorator(BeverageComponent beverage) {
		this.beverage= beverage;
	}

	@Override
	public String description() {
		return beverage.description()+" milk";
	}

	@Override
	public float cost() {
		float total=beverage.cost()+5;
		if(beverage.getSize()==SIZE.LARGE)
			total= total*4;
		if(beverage.getSize()==SIZE.MEDIUM)
			total= total*3;
		if(beverage.getSize()==SIZE.SMALL)
			total= total*2;
		return total;
	}

}
