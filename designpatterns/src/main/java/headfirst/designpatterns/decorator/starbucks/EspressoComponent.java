package headfirst.designpatterns.decorator.starbucks;

public class EspressoComponent extends BeverageComponent{

	@Override
	public float cost() {
		return 2.99f;
	}
	
	@Override
	public String description() {
		return "Espresso Coffee";
	}

}
