package headfirst.designpatterns.decorator.starbucks;

public abstract class CondimentsDecorator extends BeverageComponent{
	public abstract String description();
}
