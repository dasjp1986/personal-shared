package headfirst.designpatterns.decorator.starbucks;

public class MochaDecorator extends CondimentsDecorator{
	
	BeverageComponent beverage;
	
	public MochaDecorator(BeverageComponent beverage) {
		this.beverage= beverage;
	}

	@Override
	public String description() {
		return beverage.description()+" mocha";
	}

	@Override
	public float cost() {
		float total=beverage.cost()+10;
		if(beverage.getSize()==SIZE.LARGE)
			total= total*4;
		if(beverage.getSize()==SIZE.MEDIUM)
			total= total*3;
		if(beverage.getSize()==SIZE.SMALL)
			total= total*2;
		return total;
	}

}
