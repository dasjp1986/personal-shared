package headfirst.designpatterns.decorator.starbucks;

public class DarkRoastComponent extends BeverageComponent{

	@Override
	public float cost() {
		return 1.99f;
	}
	
	@Override
	public String description() {
		return "DarkRoast Coffee";
	}
	

}
