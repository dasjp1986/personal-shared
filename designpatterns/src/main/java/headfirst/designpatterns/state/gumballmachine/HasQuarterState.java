package headfirst.designpatterns.state.gumballmachine;

import java.rmi.RemoteException;
import java.util.Random;

public class HasQuarterState extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient GumballMachine gumballMachine;
	private Random random;

	public HasQuarterState(GumballMachine machine) {
		super("HAS QUARTER");
		this.gumballMachine = machine;

		random = new Random();
	}

	@Override
	public void insertQuarter() {
		System.out.println("You have already inserted the quarter...");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Quarter is returned...");
		gumballMachine.setCurrentState(gumballMachine.getNoQuarterState());
	}

	@Override
	public boolean turnCrank() throws RemoteException {
		System.out.println("Crank is turned on...");
		int num = random.nextInt(3);
		//for 33% for players, setting them as winner
		if (num == 0 && gumballMachine.getInventory() > 1) {
			System.out.println("Wow! we got the winner...");
			gumballMachine.setCurrentState(gumballMachine.getWinnerState());
		} else {
			gumballMachine.setCurrentState(gumballMachine.getSoldState());
		}

		return true;

	}

	@Override
	public void dispense() {
		System.out.println("No gumball can be dispensed...");

	}

}
