package headfirst.designpatterns.state.gumballmachine;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class GumballMachine extends UnicastRemoteObject implements GumballMachineRemote {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private State noQuarterState;
	private State hasQuarterState;
	private State soldState;
	private State soldOutState;
	private State winnerState;
	private State refillState;

	private int inventory;
	private String identifier;
	private State currentState;

	public GumballMachine(int inventory, String identifier) throws RemoteException {
		this.inventory = inventory;
		this.identifier = identifier;

		this.noQuarterState = new NoQuarterState(this);
		this.hasQuarterState = new HasQuarterState(this);
		this.soldState = new SoldState(this);
		this.soldOutState = new SoldOutState(this);
		this.winnerState = new WinnerState(this);
		this.refillState = new InventoryRefillState(this);

		this.currentState = noQuarterState;

	}

	public void setCurrentState(State state) {
		this.currentState = state;
	}

	public void insertQuarter() {
		currentState.insertQuarter();
	}

	public void ejectQuarter() {
		currentState.ejectQuarter();
	}

	public void turnCrank() throws RemoteException {
		if (currentState.turnCrank())
			currentState.dispense();
	}

	public void releaseBall() {
		if (inventory > 0) {
			System.out.println("Gumball rolls out the ball...");
			inventory--;
		}
	}

	public void refilGumball(int inventory) {
		System.out.println("refilling the gumball inventory...");
		this.setCurrentState(refillState);
		System.out.println(this + "\n");
		this.inventory += inventory;
		this.setCurrentState(noQuarterState);
		System.out.println(this + "\n");

	}

	public State getNoQuarterState() {
		return noQuarterState;
	}

	public State getHasQuarterState() {
		return hasQuarterState;
	}

	public State getSoldState() {
		return soldState;
	}

	public State getSoldOutState() {
		return soldOutState;
	}

	public State getWinnerState() {
		return winnerState;
	}

	public State getRefillState() {
		return refillState;
	}

	@Override
	public int getInventory() throws RemoteException {
		return inventory;
	}

	@Override
	public State getCurrentState() throws RemoteException {
		return currentState;
	}

	@Override
	public String getIdentifier() throws RemoteException {
		return identifier;
	}

	@Override
	public String toString() {
		return "GumballMachine [inventory=" + inventory + ", identifier=" + identifier + ", currentState="
				+ currentState + "]";
	}

	

}
