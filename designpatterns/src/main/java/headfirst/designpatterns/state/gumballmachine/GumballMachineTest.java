package headfirst.designpatterns.state.gumballmachine;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

/**
 * 
 * @author dagraw1 This imitates the play of Gumball Machine
 */
public class GumballMachineTest {
	public static void main(String[] args) throws RemoteException, MalformedURLException {
		for (int i = 1; i <= 3; i++) {
			GumballMachine gumballMachine = new GumballMachine(i * 2, "GUMB-" + i);

			Naming.rebind(gumballMachine.getIdentifier(), gumballMachine);
			System.out.println(gumballMachine);
			gumballMachine.ejectQuarter();
			gumballMachine.insertQuarter();
			gumballMachine.ejectQuarter();
			System.out.println(gumballMachine + "\n");

			gumballMachine.insertQuarter();
			gumballMachine.insertQuarter();
			gumballMachine.turnCrank();
			System.out.println(gumballMachine + "\n");

			gumballMachine.insertQuarter();
			gumballMachine.turnCrank();
			System.out.println(gumballMachine + "\n");

			gumballMachine.insertQuarter();
			gumballMachine.turnCrank();
			System.out.println(gumballMachine + "\n");

			gumballMachine.insertQuarter();
			gumballMachine.turnCrank();
			System.out.println(gumballMachine + "\n");

			gumballMachine.refilGumball(5);

			gumballMachine.insertQuarter();
			gumballMachine.turnCrank();
			System.out.println(gumballMachine + "\n");

			gumballMachine.insertQuarter();
			gumballMachine.turnCrank();
			System.out.println(gumballMachine + "\n");
		}
	}
}
