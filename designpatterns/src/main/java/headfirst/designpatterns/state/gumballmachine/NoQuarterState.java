package headfirst.designpatterns.state.gumballmachine;

public class NoQuarterState extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient GumballMachine gumballMachine;

	public NoQuarterState(GumballMachine machine) {
		super("NO QUARTER");
		this.gumballMachine = machine;
	}

	@Override
	public void insertQuarter() {
		System.out.println("Quarter Inserted...");
		gumballMachine.setCurrentState(gumballMachine.getHasQuarterState());
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Please insert the quarter before calling eject...");
	}

	@Override
	public boolean turnCrank() {
		System.out.println("Please insert the quarter before calling turn crank...");
		return false;
	}

	@Override
	public void dispense() {
		System.out.println("Please insert the quarter before calling dispense...");
	}

}
