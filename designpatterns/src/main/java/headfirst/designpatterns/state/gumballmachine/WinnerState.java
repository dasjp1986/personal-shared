package headfirst.designpatterns.state.gumballmachine;

import java.rmi.RemoteException;

public class WinnerState extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient GumballMachine gumballMachine;

	public WinnerState(GumballMachine gumballMachine) {
		super("WINNER STATE");
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void insertQuarter() {
		System.out.println("Please wait...you have already inserted the quarter");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("You can not eject the quarter as you have already turned the crank on...");
	}

	@Override
	public boolean turnCrank() {
		System.out.println("Turning the crank twice wont give you more balls");
		return false;
	}

	@Override
	public void dispense() throws RemoteException {
		System.out.println("Ready for dispensing the ball...");
		gumballMachine.releaseBall();
		if (gumballMachine.getInventory() == 0) {
			gumballMachine.setCurrentState(gumballMachine.getSoldOutState());
		} else {
			gumballMachine.releaseBall();
			if (gumballMachine.getInventory() > 0) {
				gumballMachine.setCurrentState(gumballMachine.getNoQuarterState());
			} else {
				System.out.println("Sorry, out of gumballs...");
				gumballMachine.setCurrentState(gumballMachine.getSoldOutState());
			}
		}

	}

}
