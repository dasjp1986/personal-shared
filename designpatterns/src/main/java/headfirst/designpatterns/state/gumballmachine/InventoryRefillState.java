package headfirst.designpatterns.state.gumballmachine;

public class InventoryRefillState extends State{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient GumballMachine gumballMachine;
	
	public InventoryRefillState(GumballMachine gumballMachine) {
		super("REFILL GUMBALL");
		this.gumballMachine= gumballMachine;
	}

	@Override
	public void insertQuarter() {
		System.out.println("Please wait...we are refilling the gumball");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Please wait...we are refilling the gumball");
	}

	@Override
	public boolean turnCrank() {
		System.out.println("Please wait...we are refilling the gumball");
		return false;
	}

	@Override
	public void dispense() {
		System.out.println("Please wait...we are refilling the gumball");
	}

}
