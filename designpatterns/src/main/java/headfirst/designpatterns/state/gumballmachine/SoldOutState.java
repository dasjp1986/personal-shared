package headfirst.designpatterns.state.gumballmachine;

public class SoldOutState extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient GumballMachine gumballMachine;

	public SoldOutState(GumballMachine gumballMachine) {
		super("SOLD OUT");
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void insertQuarter() {
		System.out.println("Opps, we are out of gumballs now. Please try later...");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Opps, we are out of gumballs now. Please try later...");
	}

	@Override
	public boolean turnCrank() {
		System.out.println("Opps, we are out of gumballs now. Please try later...");
		return false;
	}

	@Override
	public void dispense() {
		System.out.println("Opps, we are out of gumballs now. Please try later...");
	}

}
