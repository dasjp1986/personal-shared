package headfirst.designpatterns.state.gumballmachine;

import java.rmi.RemoteException;

public class SoldState extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient GumballMachine gumballMachine;

	public SoldState(GumballMachine gumballMachine) {
		super("SOLD");
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void insertQuarter() {
		System.out.println("Please wait... you have already inserted the quarter");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("You can not eject after crank is turned on...");
	}

	@Override
	public boolean turnCrank() {
		System.out.println("Dont turn on crank twice, it wont give you balls twice...");
		return false;
	}

	@Override
	public void dispense() throws RemoteException {
		System.out.println("Ready for dispensing the ball...");
		gumballMachine.releaseBall();
		if (gumballMachine.getInventory() > 0) {
			gumballMachine.setCurrentState(gumballMachine.getNoQuarterState());
		} else {
			System.out.println("Sorry, out of gumballs...");
			gumballMachine.setCurrentState(gumballMachine.getSoldOutState());
		}
	}

}
