package headfirst.designpatterns.state.gumballmachine;

import java.io.Serializable;
import java.rmi.RemoteException;

public abstract class State implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String desc;

	public State(String desc) {
		this.desc = desc;
	}

	public abstract void insertQuarter();

	public abstract void ejectQuarter();

	public abstract boolean turnCrank() throws RemoteException;

	public abstract void dispense() throws RemoteException;

	@Override
	public String toString() {
		return "State [desc=" + desc + "]";
	}
}
