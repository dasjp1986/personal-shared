package headfirst.designpatterns.state.gumballmachine;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GumballMachineRemote extends Remote {
	int getInventory() throws RemoteException;

	State getCurrentState() throws RemoteException;

	String getIdentifier() throws RemoteException;
}
