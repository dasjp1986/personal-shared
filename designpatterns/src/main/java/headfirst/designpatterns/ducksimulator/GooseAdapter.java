package headfirst.designpatterns.ducksimulator;

public class GooseAdapter implements Quackable {

	private Goose goose;
	private Observerable observerableHelper;

	public GooseAdapter(Goose goose) {
		this.goose = goose;
		observerableHelper= new Observerable(this);
	}

	@Override
	public void registerOberver(Observer observer) {
		observerableHelper.registerOberver(observer);
	}

	@Override
	public void notifyObservers() {
		observerableHelper.notifyObservers();
	}

	@Override
	public void quack() {
		goose.honks();
		notifyObservers();
	}

}
