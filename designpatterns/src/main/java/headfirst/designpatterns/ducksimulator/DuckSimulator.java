package headfirst.designpatterns.ducksimulator;

public class DuckSimulator {
	public static void main(String[] args) {
		AbstractDuckFactory duckFactory = new DuckFactory();
		Quackable mallardDuck = duckFactory.createMallardDuck();
		Quackable redHeadDuck = duckFactory.createRedHeadDuck();
		Quackable duckCall = duckFactory.createDuckCall();
		Quackable rubberDuck = duckFactory.createRubberDuck();

		Observer quackologist1 = new Quackologist();
		mallardDuck.registerOberver(quackologist1);
		redHeadDuck.registerOberver(quackologist1);
		duckCall.registerOberver(quackologist1);

		mallardDuck.quack();
		redHeadDuck.quack();
		duckCall.quack();
		rubberDuck.quack();

		AbstractDuckFactory countingDuckFactory = new CountingDuckFactory();
		Quackable mallardDuckCounter = countingDuckFactory.createMallardDuck();
		Quackable redHeadDuckCounter = countingDuckFactory.createRedHeadDuck();
		Quackable duckCallCounter = countingDuckFactory.createDuckCall();
		Quackable rubberDuckCounter = countingDuckFactory.createRubberDuck();
		Quackable goose = new GooseAdapter(new Goose());

		mallardDuckCounter.registerOberver(quackologist1);
		redHeadDuckCounter.registerOberver(quackologist1);
		duckCallCounter.registerOberver(quackologist1);
		rubberDuckCounter.registerOberver(quackologist1);
		goose.registerOberver(quackologist1);

		mallardDuck.quack();
		System.out.println(QuackCounter.getQuacks());
		redHeadDuckCounter.quack();
		System.out.println(QuackCounter.getQuacks());
		duckCallCounter.quack();
		System.out.println(QuackCounter.getQuacks());
		rubberDuckCounter.quack();
		System.out.println(QuackCounter.getQuacks());
		goose.quack();
		System.out.println(QuackCounter.getQuacks());
		
		System.out.println("\n Running Flocks");
		Flock flock= new Flock();
		flock.add(mallardDuckCounter);
		flock.add(redHeadDuckCounter);
		flock.add(duckCallCounter);
		flock.add(rubberDuckCounter);
		flock.registerOberver(quackologist1);
		flock.quack();
		System.out.println(QuackCounter.getQuacks());
	}
}
