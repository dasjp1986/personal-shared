package headfirst.designpatterns.ducksimulator;

public interface QuackObservable {
	void registerOberver(Observer observer);
	void notifyObservers();
}
