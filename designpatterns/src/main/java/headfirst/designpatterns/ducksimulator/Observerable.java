package headfirst.designpatterns.ducksimulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * 
 * @author dagraw1
 * its a helper class for implementing observer design pattern
 * which helps in registering observers to observable
 * observer's update method will be called when observable quacks
 *
 */
public class Observerable implements QuackObservable{

	private List<Observer> observers;
	private QuackObservable duck;
	
	public Observerable(QuackObservable duck) {
		observers= new ArrayList<>();
		this.duck= duck;
	}
	
	@Override
	public void registerOberver(Observer observer) {
		System.out.println("Adding an observer:"+ observer+" to duck : "+ duck);
		observers.add(observer);
	}

	@Override
	public void notifyObservers() {
		Iterator<Observer> iterator = observers.iterator();
		while (iterator.hasNext()) {
			Observer observer = (Observer) iterator.next();
			observer.update(duck);
		}
	}

}
