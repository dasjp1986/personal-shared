package headfirst.designpatterns.ducksimulator;

public class Quackologist implements Observer{

	@Override
	public void update(QuackObservable obervable) {
		System.out.println("Received update from - "+obervable);
	}

}
