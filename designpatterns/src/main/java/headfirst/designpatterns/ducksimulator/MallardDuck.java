package headfirst.designpatterns.ducksimulator;

public class MallardDuck implements Quackable {

	private String name;
	private QuackObservable observervableHelper;

	public MallardDuck(String name) {
		this.name = name;
		this.observervableHelper = new Observerable(this);
	}

	@Override
	public void registerOberver(Observer observer) {
		observervableHelper.registerOberver(observer);
	}

	@Override
	public void notifyObservers() {
		observervableHelper.notifyObservers();
	}

	@Override
	public void quack() {
		System.out.println("Mallard Duck : " + name + " Quacking...");
		notifyObservers();
	}

	@Override
	public String toString() {
		return "MallardDuck [name=" + name + "]";
	}

}
