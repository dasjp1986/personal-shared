package headfirst.designpatterns.ducksimulator;

public interface Observer {
	void update(QuackObservable obervable);
}
