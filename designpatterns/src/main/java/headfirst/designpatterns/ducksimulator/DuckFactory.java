package headfirst.designpatterns.ducksimulator;

import java.util.UUID;

public class DuckFactory extends AbstractDuckFactory {

	@Override
	public Quackable createMallardDuck() {
		return new MallardDuck("Mallard-" + UUID.randomUUID().toString().substring(0, 5));
	}

	@Override
	public Quackable createRedHeadDuck() {
		return new RedHeadDuck("RedHead-" + UUID.randomUUID().toString().substring(0, 5));
	}

	@Override
	public Quackable createDuckCall() {
		return new DuckCall("DuckCall-" + UUID.randomUUID().toString().substring(0, 5));
	}

	@Override
	public Quackable createRubberDuck() {
		return new RubberDuck("RubberDuck-" + UUID.randomUUID().toString().substring(0, 5));
	}

}
