package headfirst.designpatterns.ducksimulator;

import java.util.UUID;

public class CountingDuckFactory extends AbstractDuckFactory {

	@Override
	public Quackable createMallardDuck() {
		return new QuackCounter(new MallardDuck("Mallard-" + UUID.randomUUID().toString().substring(0, 5)));
	}

	@Override
	public Quackable createRedHeadDuck() {
		return new QuackCounter(new RedHeadDuck("RedHead-" + UUID.randomUUID().toString().substring(0, 5)));
	}

	@Override
	public Quackable createDuckCall() {
		return new QuackCounter(new DuckCall("DuckCall-" + UUID.randomUUID().toString().substring(0, 5)));
	}

	@Override
	public Quackable createRubberDuck() {
		return new QuackCounter(new RubberDuck("RubberDuck-" + UUID.randomUUID().toString().substring(0, 5)));
	}

}
