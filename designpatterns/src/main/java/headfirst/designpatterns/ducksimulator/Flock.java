package headfirst.designpatterns.ducksimulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Flock implements Quackable{
	private List<Quackable> ducks;
	private Observerable observerableHelper;
	
	public Flock() {
		ducks= new ArrayList<>();
		observerableHelper= new Observerable(this);
	}

	@Override
	public void registerOberver(Observer observer) {
		observerableHelper.registerOberver(observer);
	}

	@Override
	public void notifyObservers() {
		observerableHelper.notifyObservers();
	}

	@Override
	public void quack() {
		Iterator<Quackable> iterator = ducks.iterator();
		while (iterator.hasNext()) {
			Quackable quackable = (Quackable) iterator.next();
			quackable.quack();
			notifyObservers();
		}
	}
	
	public void add(Quackable duck){
		ducks.add(duck);
	}
}
