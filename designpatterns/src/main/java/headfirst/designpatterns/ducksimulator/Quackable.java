package headfirst.designpatterns.ducksimulator;

public interface Quackable extends QuackObservable{
	void quack();
}
