package headfirst.designpatterns.ducksimulator;

/**
 * 
 * @author dagraw1
 * This is the Decorator design pattern which adds a new behavior to count number of quacks
 * for all the ducks together
 * 
 * Decorator allows you to add new behavior at runtime without modifying the interface of the
 * object being decorated.
 *
 */


public class QuackCounter implements Quackable{

	private Quackable duck;
	private static int quacks;
	private Observerable observableHelper;
	
	public static int getQuacks(){
		return quacks;
	}
	
	public QuackCounter(Quackable duck) {
		this.duck= duck;
		this.observableHelper= new Observerable(duck);
	}
	
	@Override
	public void registerOberver(Observer observer) {
		observableHelper.registerOberver(observer);
	}

	@Override
	public void notifyObservers() {
		observableHelper.notifyObservers();
	}

	@Override
	public void quack() {
		duck.quack();
		quacks++;
	}

}
