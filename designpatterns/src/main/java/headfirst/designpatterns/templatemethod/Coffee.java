package headfirst.designpatterns.templatemethod;

public class Coffee extends AbstractBeverage{

	@Override
	public void brew() {
		System.out.println("Adding Coffee to boiling water");
	}

	@Override
	public void addCondiments() {
		System.out.println("Adding Milk and Sugar");
	}

}
