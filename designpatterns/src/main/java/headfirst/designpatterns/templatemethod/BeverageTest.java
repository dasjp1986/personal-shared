package headfirst.designpatterns.templatemethod;

public class BeverageTest {
	public static void main(String[] args) {
		AbstractBeverage coffee = new Coffee();
		coffee.prepare();

		AbstractBeverage tea = new Tea();
		tea.prepare();
	}
}
