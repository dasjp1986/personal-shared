package headfirst.designpatterns.templatemethod;

public class Tea extends AbstractBeverage {

	@Override
	public void brew() {
		System.out.println("Adding tea leaves to boiling water");
	}

	@Override
	public void addCondiments() {
		System.out.println("Adding Milk, Sugar and Ginger");
	}

}
