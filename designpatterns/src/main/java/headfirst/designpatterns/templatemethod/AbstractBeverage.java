package headfirst.designpatterns.templatemethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractBeverage {
	public final void prepare() {
		boil();
		brew();
		pour();
		if (shallAddCondiments())
			addCondiments();
	}

	// add hook method, subclass may choose to override
	public boolean shallAddCondiments() {

		return takeUserInput();
	}

	private boolean takeUserInput() {
		boolean flag = false;
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Please enter y/Y if you wish to add condiments else n/N");
			String input = bfr.readLine();
			if ("Y".equalsIgnoreCase(input))
				flag = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return flag;
	}

	public void boil() {
		System.out.println("Boil water for preparing beverage...");
	}

	public abstract void brew();

	public void pour() {
		System.out.println("Pour beverage to cup...");
	}

	public abstract void addCondiments();
}
