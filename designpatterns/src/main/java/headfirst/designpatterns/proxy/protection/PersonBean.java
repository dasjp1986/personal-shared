package headfirst.designpatterns.proxy.protection;

public class PersonBean implements Person {

	private String profile;
	private int rating;

	@Override
	public String getProfile() {
		return profile;
	}

	@Override
	public void setProfile(String profile) throws UnAuthorizedAccessException {
		this.profile = profile;
	}

	@Override
	public void setLooksRating(int rating) throws UnAuthorizedAccessException {
		this.rating = rating;
	}

	@Override
	public int getLooksRating() {
		return this.rating;
	}

}
