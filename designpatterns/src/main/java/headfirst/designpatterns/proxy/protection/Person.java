package headfirst.designpatterns.proxy.protection;

public interface Person {
	//can be accessed publicly
	String getProfile();
	int getLooksRating();
	
	//only owner should be able to set his own profile
	void setProfile(String profile) throws UnAuthorizedAccessException;
	
	//only other persons should be able to rate for looks
	void setLooksRating(int rating) throws UnAuthorizedAccessException;
}
