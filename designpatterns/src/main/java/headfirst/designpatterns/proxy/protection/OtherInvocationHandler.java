package headfirst.designpatterns.proxy.protection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class OtherInvocationHandler implements InvocationHandler {

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if(method.getName().contains("setProfile")){
			throw new  UnAuthorizedAccessException("others can not update owner's profile");
		}
		return method.invoke(proxy, args);
	}
}
