package headfirst.designpatterns.proxy.protection;

import java.lang.reflect.Proxy;

public class PersonTest {
	public static void main(String[] args) {
		Person person = new PersonBean();
		Person ownerProxy = getOwnerProxy(person);
		Person otherProxy = getOtherProxy(person);

		try {
			ownerProxy.setProfile("Owner-the-great");

		} catch (UnAuthorizedAccessException e) {
			e.printStackTrace();
		}

		try {
			otherProxy.setProfile("owner-the-worst");
		} catch (UnAuthorizedAccessException e) {
			e.printStackTrace();
		}

		try {
			otherProxy.setLooksRating(1);
		} catch (UnAuthorizedAccessException e) {
			e.printStackTrace();
		}

		try {
			ownerProxy.setLooksRating(100);
		} catch (UnAuthorizedAccessException e) {
			e.printStackTrace();
		}

		System.out.println(ownerProxy.getProfile());
		System.out.println(ownerProxy.getLooksRating());

	}
	
	public static Person getOwnerProxy(Person person) {
		return (Person) Proxy.newProxyInstance(person.getClass().getClassLoader(), person.getClass().getInterfaces(),
				new OwnerInvocationHandler());
	}

	public static Person getOtherProxy(Person person) {
		return (Person) Proxy.newProxyInstance(person.getClass().getClassLoader(), person.getClass().getInterfaces(),
				new OtherInvocationHandler());
	}
}
