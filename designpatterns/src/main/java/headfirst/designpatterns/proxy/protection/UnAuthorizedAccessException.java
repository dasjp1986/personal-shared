package headfirst.designpatterns.proxy.protection;

public class UnAuthorizedAccessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnAuthorizedAccessException(String msg) {
		super(msg);
	}

}
