package headfirst.designpatterns.proxy.protection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class OwnerInvocationHandler implements InvocationHandler {

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if(method.getName().contains("setLooksRating")){
			throw new UnAuthorizedAccessException("Owner can not set his own looks ratings");
		}
		return method.invoke(proxy, args);
	}
	
}
