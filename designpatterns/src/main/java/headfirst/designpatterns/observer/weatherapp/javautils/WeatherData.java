package headfirst.designpatterns.observer.weatherapp.javautils;

import java.util.Observable;

public class WeatherData extends Observable implements WeatherMeasurementsUpdater {

	private double temp;
	private double humidity;
	private double pressure;

	@Override
	public void updateMeasurements(double temp, double humidity, double pressure) {
		this.temp = temp;
		this.humidity = humidity;
		this.pressure = pressure;

		this.setChanged();
		notifyObservers(this);
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

}
