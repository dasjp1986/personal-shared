package headfirst.designpatterns.observer.weatherapp;

import java.util.Random;

public class WeatherPredictor implements Display, Observer {

	private double tempNew;
	private double humidityNew;
	private double pressureNew;

	private Subject weatherData;

	public WeatherPredictor(Subject weatherData) {
		this.weatherData = weatherData;
		this.weatherData.registerObserver(this);
	}

	@Override
	public void update(double temp, double humidity, double pressure) {
		Random random = new Random();
		tempNew = temp * random.nextFloat();
		humidityNew = humidity * random.nextFloat();
		pressureNew = pressure * random.nextFloat();

		display();
	}

	@Override
	public void display() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "WeatherPredictor [tempNew=" + String.format("%.2f", tempNew) + ", HumidityNew="
				+ String.format("%.2f", humidityNew) + ", PressureNew=" + String.format("%.2f", pressureNew) + "]";
	}

}
