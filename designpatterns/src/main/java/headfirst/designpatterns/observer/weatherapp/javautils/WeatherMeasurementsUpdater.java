package headfirst.designpatterns.observer.weatherapp.javautils;

public interface WeatherMeasurementsUpdater {
	void updateMeasurements(double temp, double humidity, double pressure);
}
