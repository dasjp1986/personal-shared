package headfirst.designpatterns.observer.weatherapp;

public interface Display {
	void display();
}
