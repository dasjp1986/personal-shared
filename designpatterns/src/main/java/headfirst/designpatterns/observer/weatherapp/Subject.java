package headfirst.designpatterns.observer.weatherapp;

public interface Subject {
	void registerObserver(Observer observer);

	void removeObserver(Observer observer);

	void notifyObservers();

}
