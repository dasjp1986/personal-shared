package headfirst.designpatterns.observer.weatherapp;

import java.util.Random;

public class WeatherAppTest {
	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		Subject subject = weatherData;
		WeatherMeasurementsUpdater updater = weatherData;

		Observer current = new WeatherCurrent(subject);
		Observer predictor = new WeatherPredictor(subject);
		Observer stats = new WeatherStatistics(subject);
		subject.registerObserver(stats);
		
		Random ran= new Random();
		System.out.println("\n\nShowing Current, Prediction and Statistics....");
		for (int i = 0; i < 2; i++) {
			System.out.println("*****************************************************************");
			System.out.println("Weather station updates triggered");
			updater.updateMeasurements(ran.nextDouble()*150, ran.nextDouble()*100, ran.nextDouble()*200);
		}
		
		System.out.println("\n\nShowing Current and Statistics....");
		subject.removeObserver(predictor);
		for (int i = 0; i < 2; i++) {
			System.out.println("*****************************************************************");
			System.out.println("Weather station updates triggered");
			updater.updateMeasurements(ran.nextDouble()*150, ran.nextDouble()*100, ran.nextDouble()*200);
		}
		
		System.out.println("\n\nShowing Current only....");
		subject.removeObserver(stats);
		for (int i = 0; i < 2; i++) {
			System.out.println("*****************************************************************");
			System.out.println("Weather station updates triggered");
			updater.updateMeasurements(ran.nextDouble()*150, ran.nextDouble()*100, ran.nextDouble()*200);
		}
	}
}
