package headfirst.designpatterns.observer.weatherapp;

public interface Observer {
	void update(double temp, double humidity, double pressure);
}
