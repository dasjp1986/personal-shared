package headfirst.designpatterns.observer.weatherapp.javautils;

import java.util.Observable;
import java.util.Observer;

public class WeatherStatistics implements Display, Observer {
	private double maxTemp = Double.MIN_VALUE;
	private double minTemp = Double.MAX_VALUE;
	private double tempSum;
	private double humiditySum;
	private double pressureSum;
	private int updateCount;

	private Observable weatherData;

	public WeatherStatistics(Observable weatherData) {
		this.weatherData = weatherData;
		this.weatherData.addObserver(this);
	}


	@Override
	public void display() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "WeatherStatistics [maxTemp=" + String.format("%.2f", maxTemp) + ", minTemp="
				+ String.format("%.2f", minTemp) + ", avgTemp=" + String.format("%.2f", tempSum / updateCount)
				+ ", avgHumidity=" + String.format("%.2f", humiditySum / updateCount) + ", AvgPressure="
				+ String.format("%.2f", pressureSum / updateCount) + ", updateCount=" + updateCount + "]";
	}

	@Override
	public void update(Observable o, Object arg) {

		updateCount++;
		WeatherData entity = (WeatherData) arg;

		if (entity.getTemp() > maxTemp)
			maxTemp = entity.getTemp();
		if (entity.getTemp() < minTemp)
			minTemp = entity.getTemp();
		tempSum += entity.getTemp();

		humiditySum += entity.getHumidity();
		pressureSum += entity.getPressure();

		display();
	}

}
