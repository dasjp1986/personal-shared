package headfirst.designpatterns.observer.weatherapp;

public class WeatherCurrent implements Display, Observer {

	private double temp;
	private double humidity;
	private double pressure;
	
	private Subject weatherData;
	
	public WeatherCurrent(Subject weatherData) {
		this.weatherData= weatherData;
		this.weatherData.registerObserver(this);
	}

	@Override
	public void update(double temp, double humidity, double pressure) {
		this.temp = temp;
		this.humidity = humidity;
		this.pressure = pressure;

		display();
	}

	@Override
	public void display() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "CurrentConditions [temp=" + String.format("%.2f", temp) + ", humidity="
				+ String.format("%.2f", humidity) + ", pressure=" + String.format("%.2f", pressure) + "]";
	}

}
