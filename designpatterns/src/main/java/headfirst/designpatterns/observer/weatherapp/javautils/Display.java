package headfirst.designpatterns.observer.weatherapp.javautils;

public interface Display {
	void display();
}
