package headfirst.designpatterns.observer.weatherapp.javautils;

import java.util.Observable;
import java.util.Observer;

public class WeatherCurrent implements Display, Observer {

	private double temp;
	private double humidity;
	private double pressure;

	private Observable weatherData;

	public WeatherCurrent(Observable weatherData) {
		this.weatherData = weatherData;
		this.weatherData.addObserver(this);
	}

	@Override
	public void display() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "CurrentConditions [temp=" + String.format("%.2f", temp) + ", humidity="
				+ String.format("%.2f", humidity) + ", pressure=" + String.format("%.2f", pressure) + "]";
	}

	@Override
	public void update(Observable o, Object arg) {
		WeatherData entity = (WeatherData) arg;
		this.temp = entity.getTemp();
		this.humidity = entity.getHumidity();
		this.pressure = entity.getPressure();

		display();
	}

}
