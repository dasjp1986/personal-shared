package headfirst.designpatterns.observer.weatherapp;

public class WeatherStatistics implements Display, Observer {
	private double maxTemp = Double.MIN_VALUE;
	private double minTemp = Double.MAX_VALUE;
	private double tempSum;
	private double humiditySum;
	private double pressureSum;
	private int updateCount;
	
	private Subject weatherData;

	public WeatherStatistics(Subject weatherData) {
		this.weatherData = weatherData;
		this.weatherData.registerObserver(this);
	}

	@Override
	public void update(double temp, double humidity, double pressure) {
		updateCount++;

		if (temp > maxTemp)
			maxTemp = temp;
		if (temp < minTemp)
			minTemp = temp;
		tempSum += temp;

		humiditySum += humidity;
		pressureSum += pressure;

		display();
	}

	@Override
	public void display() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "WeatherStatistics [maxTemp=" + String.format("%.2f", maxTemp) + ", minTemp="
				+ String.format("%.2f", minTemp) + ", avgTemp=" + String.format("%.2f", tempSum / updateCount)
				+ ", avgHumidity=" + String.format("%.2f", humiditySum / updateCount) + ", AvgPressure="
				+ String.format("%.2f", pressureSum / updateCount) + ", updateCount=" + updateCount + "]";
	}

}
