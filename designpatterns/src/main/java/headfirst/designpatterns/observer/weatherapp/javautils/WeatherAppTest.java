package headfirst.designpatterns.observer.weatherapp.javautils;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class WeatherAppTest {
	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		Observable subject = weatherData;
		WeatherMeasurementsUpdater updater = weatherData;

		Observer current = new WeatherCurrent(subject);
		Observer predictor = new WeatherPredictor(subject);
		Observer stats = new WeatherStatistics(subject);

		Random ran = new Random();
		System.out.println("\n\nShowing Current, Prediction and Statistics....");
		for (int i = 0; i < 2; i++) {
			System.out.println("*****************************************************************");
			System.out.println("Weather station updates triggered");
			updater.updateMeasurements(ran.nextDouble() * 150, ran.nextDouble() * 100, ran.nextDouble() * 200);
		}

		System.out.println("\n\nShowing Current and Statistics....");
		subject.deleteObserver(predictor);
		for (int i = 0; i < 2; i++) {
			System.out.println("*****************************************************************");
			System.out.println("Weather station updates triggered");
			updater.updateMeasurements(ran.nextDouble() * 150, ran.nextDouble() * 100, ran.nextDouble() * 200);
		}

		System.out.println("\n\nShowing Current only....");
		subject.deleteObserver(stats);
		for (int i = 0; i < 2; i++) {
			System.out.println("*****************************************************************");
			System.out.println("Weather station updates triggered");
			updater.updateMeasurements(ran.nextDouble() * 150, ran.nextDouble() * 100, ran.nextDouble() * 200);
		}
	}
}
