package headfirst.designpatterns.observer.weatherapp;

public interface WeatherMeasurementsUpdater {
	void updateMeasurements(double temp, double humidity, double pressure);
}
