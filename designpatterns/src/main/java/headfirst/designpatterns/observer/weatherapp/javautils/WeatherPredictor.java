package headfirst.designpatterns.observer.weatherapp.javautils;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class WeatherPredictor implements Display, Observer {

	private double tempNew;
	private double humidityNew;
	private double pressureNew;

	private Observable weatherData;

	public WeatherPredictor(Observable weatherData) {
		this.weatherData = weatherData;
		this.weatherData.addObserver(this);
	}

	@Override
	public void display() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "WeatherPredictor [tempNew=" + String.format("%.2f", tempNew) + ", HumidityNew="
				+ String.format("%.2f", humidityNew) + ", PressureNew=" + String.format("%.2f", pressureNew) + "]";
	}

	@Override
	public void update(Observable o, Object arg) {
		WeatherData entity = (WeatherData) arg;
		Random random = new Random();
		tempNew = entity.getTemp() * random.nextFloat();
		humidityNew = entity.getHumidity() * random.nextFloat();
		pressureNew = entity.getPressure() * random.nextFloat();

		display();
	}

}
