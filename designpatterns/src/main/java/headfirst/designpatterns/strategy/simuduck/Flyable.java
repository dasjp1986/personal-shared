package headfirst.designpatterns.strategy.simuduck;

public interface Flyable {
	String fly();
}
