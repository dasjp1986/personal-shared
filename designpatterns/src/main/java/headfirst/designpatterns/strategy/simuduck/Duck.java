package headfirst.designpatterns.strategy.simuduck;

public abstract class Duck {
	protected Flyable flying;
	protected Quackable quacking;
	
	public void setFlying(Flyable flyable){
		this.flying= flyable;
	}
	
	public void setQuacking(Quackable quacking){
		this.quacking= quacking;
	}
	
	public Duck(Flyable flying, Quackable quacking) {
		this.flying= flying;
		this.quacking= quacking;
	}
	
	public abstract void display();
	
	public void performFlying(){
		System.out.println(flying.fly());
	}
	
	public void performQuacking(){
		System.out.println(quacking.quack());
	}
}
