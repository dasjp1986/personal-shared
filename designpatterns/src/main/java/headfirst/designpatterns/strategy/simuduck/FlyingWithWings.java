package headfirst.designpatterns.strategy.simuduck;

public class FlyingWithWings implements Flyable{

	@Override
	public String fly() {
		return "Flying with wings";
	}

}
