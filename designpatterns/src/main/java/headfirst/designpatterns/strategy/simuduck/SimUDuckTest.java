package headfirst.designpatterns.strategy.simuduck;

public class SimUDuckTest {
	public static void main(String[] args) {
		IndianDuck indian= new IndianDuck(new FlyingWithWings(), new NiceQuacking());
		AmericanDuck american= new AmericanDuck(new ForgotFlying(), new WeirdQuacking());

		indian.display();
		indian.performFlying();
		indian.performQuacking();
		
		american.display();
		american.performFlying();
		american.performQuacking();
		american.setFlying(new FlyingWithWings());
		american.performFlying();
	}
}
