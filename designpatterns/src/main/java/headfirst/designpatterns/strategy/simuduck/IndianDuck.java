package headfirst.designpatterns.strategy.simuduck;

public class IndianDuck extends Duck{

	public IndianDuck(Flyable flying, Quackable quacking) {
		super(flying, quacking);
	}
	
	@Override
	public void display() {
		System.out.println("Indian Duck is displayed on the console");
	}

}
