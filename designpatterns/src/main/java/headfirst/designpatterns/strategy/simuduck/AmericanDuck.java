package headfirst.designpatterns.strategy.simuduck;

public class AmericanDuck extends Duck {

	public AmericanDuck(Flyable flying, Quackable quacking) {
		super(flying, quacking);
	}

	@Override
	public void display() {
		System.out.println("American Duck is displayed on the console");
	}

}
