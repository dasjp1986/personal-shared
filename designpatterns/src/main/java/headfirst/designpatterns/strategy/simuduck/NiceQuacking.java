package headfirst.designpatterns.strategy.simuduck;

public class NiceQuacking implements Quackable{

	@Override
	public String quack() {
		return "Quacks nicely";
	}

}
