package headfirst.designpatterns.strategy.simuduck;

public class ForgotFlying implements Flyable{

	@Override
	public String fly() {
		return "Forgot to fly";
	}

}
