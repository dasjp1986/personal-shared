package headfirst.designpatterns.strategy.simuduck;

public class WeirdQuacking implements Quackable{

	@Override
	public String quack() {
		return "Quacks weirdly";
	}

}
