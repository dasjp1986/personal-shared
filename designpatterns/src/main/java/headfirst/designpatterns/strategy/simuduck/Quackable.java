package headfirst.designpatterns.strategy.simuduck;

public interface Quackable {
	String quack();
}
