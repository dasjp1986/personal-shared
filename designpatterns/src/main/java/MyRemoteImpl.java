
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MyRemoteImpl extends UnicastRemoteObject implements MyRemote {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected MyRemoteImpl() throws RemoteException {
		super();
	}

	@Override
	public String sayHello() throws RemoteException{
		return "MyRemoteImpl Server says hello...";
	}
	
	public static void main(String[] args) throws RemoteException, MalformedURLException {
		MyRemote myRemote= new MyRemoteImpl();
		Naming.rebind("SayHello", myRemote);
	}

}
