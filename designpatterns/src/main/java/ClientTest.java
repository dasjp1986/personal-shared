
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ClientTest {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		MyRemote myRemote= (MyRemote) Naming.lookup("rmi://127.0.0.1/SayHello");
		System.out.println(myRemote.sayHello());
	}
}
