package com.da.coding.multithreading;

import java.util.Scanner;

public class GracefulShutDown extends Thread {

	// marking this as volatile will make sure value of this variable is not
	// cached by threads locally
	private volatile boolean isRunning = true;

	@Override
	public void run() {
		while (isRunning) {
			System.out.println("running...");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void shutdown() {
		isRunning = false;
	}

	public static void main(String[] args) {
		GracefulShutDown thread = new GracefulShutDown();
		thread.start();

		System.out.println("Hit Return key to shutdown");
		Scanner scn = new Scanner(System.in);
		scn.nextLine();
		scn.close();
		thread.shutdown();
	}

}
