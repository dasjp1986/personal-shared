package com.da.coding.amazon;

public class Task2 {
	
	public boolean solution(String S, String T) {
		// write your code in Java SE 8

		boolean isSame = true;

		String t1 = replaceWithQ(S);
		String t2 = replaceWithQ(T);
		
		if (t1.length() != t2.length())
			return false;

		for (int i = 0; i < t1.length(); i++) {

			if (t1.charAt(i) == t2.charAt(i))
				continue;
			else if (t1.charAt(i) == '?' || t2.charAt(i) == '?')
				continue;
			else{
				isSame= false;
				break;
			}
		}
		return isSame;
	}

	private String replaceWithQ(String s) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			StringBuilder str = new StringBuilder();
			while (i < s.length() && isDigit(s.charAt(i))) {
				str.append(s.charAt(i));
				i++;
			}
			if (!str.toString().isEmpty()) {
				i--;
				int c = Integer.parseInt(str.toString());
				for (int j = 0; j < c; j++) {
					builder.append("?");
				}
			} else
				builder.append(s.charAt(i));
		}
		return builder.toString();
	}

	private boolean isDigit(int ch){
		if(48<=ch && ch<=57)
			return true;
		return false;
	}
	
	public static void main(String[] args) {
		Task2 t= new Task2();
		System.out.println(t.solution("ba1", "1ad"));
	}
}
