package com.da.coding.matrix.matrixmirros;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Dharmendra
 * Given a square matrix of size N*N with some opaque objects and mirrors(45 degree) 
 * at the cells
 * Lets assume light can enter from the bottom of matrix and can be reflected if there is a 
 * mirror placed on a cell to the right direction until an object consumes it
 * 
 * we need to find out no. of maximum mirrors which can reflect light to ouside of the matrix
   Lets assume 0= opaque and 1=mirror
 *
 */
public class MatrixMirrosReflection {
	public static void main(String[] args) {
		MatrixMirrosReflection matrixMirros= new MatrixMirrosReflection();
		int[][] matrix1= {
				{1,0,0,1},
				{1,1,1,1},
				{1,0,1,1},
				{1,1,1,0}};
		int count= matrixMirros.countReflection(matrix1);
		System.out.println("Max Matrix Reflections : "+count);
		
		int[][] matrix2= {
				{1,0,0,1},
				{1,1,1,1},
				{1,0,1,0},
				{1,1,1,0}};
		count= matrixMirros.countReflection(matrix2);
		System.out.println("Max Matrix Reflections : "+count);
	}

	private int countReflection(int[][] matrix) {
		Set<Integer> entrySet= new HashSet<>();
		Set<Integer> exitSet= new HashSet<>();
		for (int i = 0; i < matrix.length; i++) {
			entrySet.add(i);
			exitSet.add(i);
		}
		
		Set<Integer> rowExitSet= new HashSet<>();
		int count=0;
		for (int i = matrix.length-1; i >=0 ; i--) {
			boolean isBlock=false;
			rowExitSet.addAll(exitSet);
			for (int j = matrix.length-1; j>=0; j--) {
				if(matrix[i][j]==0){
					entrySet.remove(j);
					rowExitSet.remove(j);
					isBlock=true;
				}else if(isBlock){
					rowExitSet.remove(j);
				}
			}
			System.out.println("Row : "+i+" entrySet : "+entrySet+" exitSet : "+rowExitSet);
			for (Integer exit : rowExitSet) {
				if(entrySet.contains(exit)){
					entrySet.remove(exit);
					count++;
				}
			}

		}
		return count;
	}
}
