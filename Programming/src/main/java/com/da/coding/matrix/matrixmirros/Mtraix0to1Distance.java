package com.da.coding.matrix.matrixmirros;
/**
 * 
 * @author Dharmendra
 * Given a matrix with all 0's and 1's calculate minimum distance from 0 to 1
 * considering only valid moves left, right, top and bottom with every move has cost 1
 * {{0,0,0},
 *  {0,0,0},
 *  {1,0,1}
 * }
 * 
 * answer
 * {{2,3,2},
 *  {1,2,1},
 *  {1,1,1}
 * }
 */
public class Mtraix0to1Distance {

}
