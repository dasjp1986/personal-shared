package com.da.coding.hackerrank;

import java.util.Scanner;

public class Question4 {
	public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		
		String[] line= scn.nextLine().split(" ");
		scn.close();
		int[] array= new int[line.length];
		
		array[0]= Integer.parseInt(line[0]);
		System.out.print(array[0]+" ");
		
		for (int i = 1; i < array.length; i++) {
			array[i]= Integer.parseInt(line[i]);
			int diff= array[i]- array[i-1];
			if(!(-127<=diff && diff<=127))
				System.out.print("-128 ");
			System.out.print(diff+" ");
		}
		
		
	}
}
