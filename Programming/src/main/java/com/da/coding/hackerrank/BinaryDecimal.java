package com.da.coding.hackerrank;

import java.util.Scanner;

public class BinaryDecimal {
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String binary= Integer.toBinaryString(n);
        int cMax=0, max=0;
        
        for(int i=0;i<binary.length();i++){
            if(binary.charAt(i)=='1'){
                cMax++;
                if(cMax>max)
                  max= cMax; 
            }else
                cMax=0;
        }
        System.out.println(max);
        in.close();
    }
    
    private static String convertToBinaryStr(int num){
        int rem=0;
        StringBuilder strBuilder= new StringBuilder();
        while(num!=0){
            rem= num%2;
            num= num/2;
            
            strBuilder.insert(0,rem);
        }
        
        return strBuilder.toString();
    }
}
