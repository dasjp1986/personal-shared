package com.da.coding.hackerrank;

import java.util.Scanner;

public class HoursGlass {
	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scn= new Scanner(System.in);
        int[][] a= new int[6][6];
        for(int i=0;i<6;i++){
            for(int j=0;j<6;j++){
                a[i][j]= scn.nextInt();
            }
        }
        scn.close();
        System.out.print(maxHoursGlass(a));
    }
    
    public static int maxHoursGlass(int[][] a){
        int max=Integer.MIN_VALUE;
        for(int i=1;i<a.length-1;i++){
            for(int j=1;j<a.length-1;j++){
                int m= a[i-1][j-1]+a[i-1][j]+a[i-1][j+1];
                m+= a[i][j];
                m+= a[i+1][j-1]+a[i+1][j]+a[i+1][j+1];
                if(m>max)
                    max= m;
                //System.out.println("Max..."+max);
            }
        }
        return max;
    }
}
