package com.da.coding.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Question2 {public static void main(String[] args) throws IOException {
	Question2 qus= new Question2();
	BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
	String number= reader.readLine();
	int n= Integer.parseInt(reader.readLine());
	System.out.println(qus.lookAndSay(number, n));
	
}

	private String lookAndSay(String nums, int n){
		if(n<=0)
			return nums;
		
		if(nums.length()==1)
			return 1+""+nums.charAt(0);
		String result="";
		for (int i = 0; i < nums.length(); i++) {
			char ch= nums.charAt(i);
			int count=1;
			i++;
			while(i<nums.length()){
				if(ch== nums.charAt(i)){
					count++;
					i++;
				}else{
					i--;
					break;
				}
			}
			result= result+count+ch;
		}
		result=lookAndSay(result, n-1);
		return result;
	}

}
