package com.da.coding.hackerrank.arrays;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[][] route = new int[m][2];
        Map<Integer, Set<Integer>> map= new HashMap<>();
        for(int route_i=0; route_i < m; route_i++){
           int s,d;
           s= in.nextInt();
           d= in.nextInt();
           
           Set<Integer> edges1= map.get(s);
           if(edges1==null){
               edges1= new HashSet<>();
               map.put(s, edges1);
           }
           edges1.add(d);
           
           Set<Integer> edges2= map.get(d);
           if(edges2==null){
               edges2= new HashSet<>();
               map.put(d, edges2);
           }
           edges2.add(s);
           
        }
        in.close();
        System.out.println(maxConnected(route,n, map));
        
        
    }
    
    public static int maxConnected(int[][] routes, int n, Map<Integer, Set<Integer>> map){
        
        Queue<Integer> queue;
        int maxPaths=0;
        for(int node=1;node<=n;node++){
            Set<Integer> connectedPaths= new HashSet<>();
            if(connectedPaths.contains(node))
            	continue;
            
            queue= new LinkedList<>();
            queue.add(node);
            
            while(!queue.isEmpty()){
                Integer temp= queue.poll();
                connectedPaths.add(temp);
                
                Set<Integer> paths= map.get(temp);
                if(paths!=null){
                    for(Integer p:paths){
                    	if(!connectedPaths.contains(p))
                    	queue.add(p);
                    }
                        
                }
                
            }
            
            if(connectedPaths.size()>maxPaths)
                maxPaths= connectedPaths.size();
            
        }
        return maxPaths;
    }
}
