package com.da.coding.hackerrank.arrays;

import java.util.Scanner;

/**
 * 
 * @author Dharmendra https://www.hackerrank.com/challenges/swaps-and-sum
 */
public class SwapsAndSums {
	public static void main(String[] args) {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		int q = scn.nextInt();
		int[] seq = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			seq[i] = scn.nextInt();
		}

		int tp = 0, l = 0, r = 0;
		for (int j = 0; j < q; j++) {
			tp = scn.nextInt();
			l = scn.nextInt();
			r = scn.nextInt();

			switch (tp) {
			case 1:
				swap(seq, l, r);
				break;
			case 2:
				System.out.println(sum(seq, l, r));
				break;
			}
		}
		scn.close();
	}

	private static void swap(int[] seq, int l, int r) {
		int t = 0;
		for (int i = l; i <= r - 1; i++) {
			t = seq[i];
			seq[i] = seq[i + 1];
			seq[i + 1] = t;
			i++;
		}
	}

	private static int sum(int[] seq, int l, int r) {
		int sum = 0;
		for (int i = l; i <= r; i++) {
			sum += seq[i];
		}
		return sum;
	}
}
