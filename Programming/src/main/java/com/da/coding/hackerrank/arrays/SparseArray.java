package com.da.coding.hackerrank.arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SparseArray {
	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scn= new Scanner(System.in);
        int n= scn.nextInt();
        Map<String, Integer> map= new HashMap<>();
        for(int i=0;i<n;i++){
            String s= scn.next();
            Integer c= map.get(s);
            if(c==null)
                c=0;
            map.put(s, c+1);
        }
        int q= scn.nextInt();
        for(int i=0;i<q;i++){
        	Integer count = map.get(scn.next());
        	if(count==null)
        		count=0;
            System.out.println(count);
        }
        scn.close();
    }
}
