package com.da.coding.hackerrank;

import java.util.Arrays;
import java.util.Scanner;

public class Question3 {
	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner= new Scanner(System.in);
        int testCases= scanner.nextInt();
        for(int i=0;i<testCases;i++){
            int n= scanner.nextInt();
            int[][] matrix= new int[2*n][2*n];
            for(int j=0;j<2*n;j++){
                for(int k=0;k<2*n;k++){
                    matrix[j][k]= scanner.nextInt();
                }
            }
            System.out.println(findMax(matrix, n, 0));
        }
        scanner.close();
    }
    
    private static int findMax(int[][] matrix, int n, int max){
        boolean isReverse= false;
       
        System.out.println("Inital Matrix check...");
        print(matrix);
        
        for(int c=n;c<2*n;c++){
            int sum1=0, sum2=0;
            for(int r=0;r<2*n;r++){
                if(r<n)
                    sum1+= matrix[r][c];
                else
                    sum2+=matrix[r][c];
            }
            if(sum1<sum2){
                isReverse= true;
                reverseColumn(matrix, c,2*n);
            }
        }
        
        System.out.println("Matrix after columns check...");
        print(matrix);
        
        for(int r=n;r<2*n;r++){
            int sum1=0, sum2=0;
            for(int c=0;c<2*n;c++){
                if(c<n)
                    sum1+= matrix[r][c];
                else
                    sum2+=matrix[r][c];
            }
            if(sum1<sum2){
                isReverse= true;
                reverseRow(matrix, r,2*n);
            }
        }
        
        System.out.println("Matrix after rows check...");
        print(matrix);
        
        for(int r=0;r<n;r++){
            int sum1=0, sum2=0;
            for(int c=0;c<2*n;c++){
                if(c<n)
                    sum1+= matrix[r][c];
                else
                    sum2+=matrix[r][c];
            }
            if(sum1<sum2){
                isReverse= true;
                reverseRow(matrix, r,2*n);
            }
        }
        
        System.out.println("Matrix after n rows check...");
        print(matrix);
        
        for(int c=0;c<n;c++){
            int sum1=0, sum2=0;
            for(int r=0;r<2*n;r++){
                if(r<n)
                    sum1+= matrix[r][c];
                else
                    sum2+=matrix[r][c];
            }
            if(sum1<sum2){
                isReverse= true;
                reverseColumn(matrix, c,2*n);
            }
        }
        
        System.out.println("Matrix after n columns check...");
        print(matrix);
        
        int sum=0;
        for(int r=0;r<n;r++){
            for(int c=0;c<n;c++){
                sum+=matrix[r][c];
            }
        }
        if(sum>max)
            max= sum;
        
        System.out.println("max..."+max);
        if(isReverse)
            findMax(matrix, n, max);
        return max;
    }
    
    private static void reverseColumn(int[][] matrix, int c, int rows){
        int temp=0;
        for(int i=0, j=rows-1;j>i;i++,j--){
            temp= matrix[i][c];
            matrix[i][c]= matrix[j][c];
            matrix[j][c]= temp;
        }
    }
    
    private static void reverseRow(int[][] matrix, int r, int columns){
        int temp=0;
        for(int i=0, j=columns-1;j>i;i++,j--){
            temp= matrix[r][i];
            matrix[r][i]= matrix[r][j];
            matrix[r][j]= temp;
        }
    }
    
    private static void print(int[][] matrix){
    	for (int i = 0; i < matrix.length; i++) {
			System.out.println(Arrays.toString(matrix[i]));
		}
    }
}
