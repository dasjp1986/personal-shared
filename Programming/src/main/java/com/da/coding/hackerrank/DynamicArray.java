package com.da.coding.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DynamicArray {
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		int q = scn.nextInt();
		int[][] queries = new int[q][3];
		for (int i = 0; i < q; i++) {
			for (int j = 0; j < 3; j++) {
				queries[i][j] = scn.nextInt();
			}
		}
		runQueries(queries, n, q);
		scn.close();
	}

	private static void runQueries(int[][] queries, int n, int q) {
		List<List<Integer>> seqList= new ArrayList<>();
		for (int i = 0; i < n; i++) {
			seqList.add(new ArrayList<>());
		}
		int lastAns = 0;

		for (int i = 0; i < q; i++) {
			int query = queries[i][0];
			int seqIndex=0;
			switch (query) {
			case 1:
				seqIndex= (queries[i][1]^lastAns)%n;
				seqList.get(seqIndex).add(queries[i][2]);
				break;
				
			case 2:
				seqIndex= (queries[i][1]^lastAns)%n;
				List<Integer> sequence = seqList.get(seqIndex);
				lastAns= sequence.get(queries[i][2]%sequence.size());
				System.out.println(lastAns);
				break;

			default:
				break;
			}
		}
	}
}
