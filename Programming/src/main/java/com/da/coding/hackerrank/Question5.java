package com.da.coding.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Question5 {
	public static void main(String[] args) {
		Scanner scanner= new Scanner(System.in);
		String[] ws= scanner.nextLine().split(" ");
		List<String> words= new ArrayList<>();
		for (String w: ws) {
			words.add(w.toLowerCase());
		}
		
		int m= scanner.nextInt();
		
		Map<Integer, Integer> result= new LinkedHashMap<>();
		for (int i = 0; i < m; i++) {
			int hotel= scanner.nextInt();
			scanner.nextLine();
			String[] tokens= scanner.nextLine().split(" ");
			for (String t: tokens) {
				if(t.equals(".") || t.equals(","))
					continue;
				else if(words.contains(t.toLowerCase())){
					Integer count= result.get(hotel);
					if(count==null)
						count=0;
					result.put(hotel, count+1);
				}
			}
		}
		
		scanner.close();
		List<Result> results= new ArrayList<>();
		for (java.util.Map.Entry<Integer, Integer> entry: result.entrySet()) {
			results.add(new Result(entry.getKey(), entry.getValue()));
		}
		
		Collections.sort(results, new Result());
		for (Result res: results) {
			System.out.print(res.key+" ");
		}
	}
}

class Result implements Comparator<Result>{
	Integer key;
	Integer value;
	
	public Result(Integer key,
	Integer value) {
		this.key= key;
		this.value= value;
	}

	public Result() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int compare(Result o1, Result o2) {
		int r = o2.value.compareTo(o1.value);
		if(r==0)
			r= o1.key.compareTo(o2.key);
		return r;
	}
}
