package com.da.coding.hackerrank;

import java.util.Scanner;

public class CulturalConference {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		// Information for employees 1 through n - 1:
		// The first value is the employee's supervisor ID
		// The second value is the employee's burnout status (0 is burned out, 1
		// is not)
		int[][] e = new int[n][2];
		int minimumEmployees = 0;
		e[0][0] = -1;
		e[0][1] = -1;
		for (int i = 1; i < n; i++) {
			for (int j = 0; j < 2; j++) {
				e[i][j] = in.nextInt();
			}
			if (e[i][1] == 1)
				continue;
			int p = e[i][0];

			if (e[p][0] == -1 || e[p][1] == 1)
				minimumEmployees++;
		}
		in.close();
		System.out.println(minimumEmployees);
	}
}
