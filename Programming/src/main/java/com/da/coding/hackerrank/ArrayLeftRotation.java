package com.da.coding.hackerrank;

import java.util.Scanner;

public class ArrayLeftRotation {
	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scn= new Scanner(System.in);
        int n= scn.nextInt();
        int d= scn.nextInt();
        int[] a= new int[n];
        
        for(int i=0;i<n;i++){
            int index= (i+n-d)%n;
            a[index]= scn.nextInt();
        }
        scn.close();
        for(int i=0;i<n;i++){
            System.out.print(a[i]+" ");
        }
    }
}
