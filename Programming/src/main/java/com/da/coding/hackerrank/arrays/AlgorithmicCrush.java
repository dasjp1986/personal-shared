package com.da.coding.hackerrank.arrays;

import java.util.Scanner;
/**
 * 
 * @author Dharmendra
 * https://www.hackerrank.com/challenges/crush
 */
public class AlgorithmicCrush {
	public static void main(String[] args) {
		/*
		 * Enter your code here. Read input from STDIN. Prlong output to STDOUT.
		 * Your class should be named Solution.
		 */
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		int m = scn.nextInt();
		
		
		//creating an array for n+1 to avoid index confusions
		long[] array = new long[n+1];
		int a=0, b=0,k=0;
		for (int i = 0; i < m; i++) {
			a= scn.nextInt();
			b= scn.nextInt();
			k= scn.nextInt();
			
			array[a]+= k;
			if(b+1<=n)
				array[b+1]-=k;
		}
		
		long max = array[1];
		long sum=max;
		for (int i = 2; i <=n; i++) {
			sum= sum+array[i];
			if(sum>max)
				max= sum;
		}
		
		System.out.println(max);
		scn.close();
	}
}
