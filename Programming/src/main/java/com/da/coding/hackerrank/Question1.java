package com.da.coding.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Question1 {
	public static void main(String[] args) throws IOException {
		Question1 qus= new Question1();
		
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		//read first list
		int list1length= Integer.parseInt(reader.readLine());
		List<Integer> list1= new ArrayList<>();
		for (int i = 0; i < list1length; i++) {
			list1.add(Integer.parseInt(reader.readLine()));
		}
		
		//read second list
		int list2length= Integer.parseInt(reader.readLine());
		List<Integer> list2= new ArrayList<>();
		for (int i = 0; i < list2length; i++) {
			list2.add(Integer.parseInt(reader.readLine()));
		}
		
		System.out.println(qus.findSubList(list1, list2));
	}

	private int findSubList(List<Integer> list1, List<Integer> list2) {
		int index=-1;
		

		for (int i = 0; i < list1.size(); i++) {
			for (int j = 0; j < list2.size(); j++) {
				if(list1.get(i)==list2.get(j)){
					index=i;
					boolean isSubList= true;
					i++;
					for (int k = j+1; k < list2.size() && i<list1.size(); k++) {
						if(list1.get(i)==list2.get(k)){
							i++;
							k++;
						}else{
							isSubList= false;
						}
					}
					if(isSubList)
						return index;
				}
			}
		}
		
		return -1;
	}
}
