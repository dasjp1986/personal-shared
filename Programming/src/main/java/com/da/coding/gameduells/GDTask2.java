package com.da.coding.gameduells;

import java.util.EmptyStackException;
import java.util.Stack;

public class GDTask2 {
	public static void main(String[] args) {
		GDTask2 task= new GDTask2();
		String s= "3 DUP 5 - -";
		System.out.println(task.solution(s));
	}

	public int solution(String s) {
		Integer res=-1;
		if (s == null || s.isEmpty())
			return res;

		Stack<Integer> stk= new Stack<>();
		Integer l1=0,l2=0;
		String[] tokens = s.split(" ");
		try{
			for (String token : tokens) {
				switch (token) {
				case "POP":
					stk.pop();
					break;
				case "DUP":
					stk.push(stk.peek());
					break;
				case "+":
					l1= stk.pop();
					l2= stk.pop();
					stk.push(l1+l2);
					break;
				case "-":
					l1= stk.pop();
					l2= stk.pop();
					stk.push(l1-l2);
					break;
				default:
					stk.push(Integer.parseInt(token));
					break;
				}
			}
			
			res= stk.pop();
		}catch(EmptyStackException ex){
			res=-1;
		}
		return res;
	}
}
