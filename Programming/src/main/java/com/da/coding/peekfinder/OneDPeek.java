package com.da.coding.peekfinder;

import java.util.Arrays;
import java.util.Random;

/**
 * 
 * @author Dharmendra
 * Find a peek in 1D array
 * ex- [1,2,3,5,4,2]
 * 5 is the peek
 *
 *	Solving Recurrence
 *	T(n)= T(n/2)+c => T(n/4)+c+c =>T(n/8)+c+c+c => T(n/2^k)+ 2k => T(n/2^logn)+ 2logn
 *	T(n)= T(n/n) + logn
 *	T(n)= logn
 */

public class OneDPeek {
	
	public static void main(String[] args) {
		OneDPeek peek= new OneDPeek();
		int[] input=new int[10];
		Random rand= new Random();
		for (int i = 0; i < input.length; i++) {
			input[i]= rand.nextInt(10);
		}
		System.out.println(Arrays.toString(input));
		System.out.println(input[peek.findPeek(input, 0, input.length-1)]);
	}
	
	public int findPeek(int[] input, int start, int end){
		int m= (start+ end)/2;
		if(input[m]>=input[m-1] && input[m]>=input[m+1])
			return m;
		else if(input[m-1]>input[m])
			return findPeek(input, start, m-1);
		else if(input[m+1]>input[m])
			return findPeek(input, m+1, end);
		return -1;
	}
}
