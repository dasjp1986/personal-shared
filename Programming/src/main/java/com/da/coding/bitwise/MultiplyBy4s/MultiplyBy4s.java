package com.da.coding.bitwise.MultiplyBy4s;

/**
 * 
 * @author Dharmendra
 * find out if a number is multiplied by 4 without using *,/,+ or % operator
 * for ex- 12-true
 * 10- false
 * 40- true
 */
public class MultiplyBy4s {
	public static void main(String[] args) {
		MultiplyBy4s multiply= new MultiplyBy4s();
		int num=13;
		boolean isMultiplied= multiply.isMultipliedBy4Shifts(num);
		System.out.println("number is multiplied by 4 : "+isMultiplied);
	}

	private boolean isMultiplyBy4XOR(int num) {
		boolean isMultiplied=false;
		int xor=0;
		
		//xor until n-1 iterations will be zero and the last iteration will make it same as the number
		for (int i = 0; i <= num; i++) {
			xor= xor^i;
		}
		if(xor==num)
			isMultiplied= true;
		return isMultiplied;
	}
	
	private boolean isMultipliedBy4Shifts(int num){
		boolean isMultiplied=false;
		//left shift the number by two bits
		int temp= num>>2;
		System.out.println("number after right shifts : "+temp);
		//right shift the number by two bits
		temp= temp<<2;
		System.out.println("number after left shifts : "+temp);
		
		if(temp==num)
			isMultiplied= true;
		
		return isMultiplied;
	}
}
