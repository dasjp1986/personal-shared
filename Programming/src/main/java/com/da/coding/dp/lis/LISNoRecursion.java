package com.da.coding.dp.lis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra
 * Given an array of unsorted numbers, find the length of longest common increasing subsequence
 * for ex {10,22,9,6,7,8}
 * LIS {6,7,8} so length is 3	
 */
class LISNoRecursion {
    int[] input;
	int[] aux;
	int fCalls=0;
	
	public LISNoRecursion(int[] input){
	    this.input= input;
		this.aux= new int[input.length];
	}
	
	public static void main (String[] args) throws IOException{
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		String strCount= reader.readLine();
		int count= Integer.parseInt(strCount);
		
		int c=0;
		while(c<count){
		    String strSize=reader.readLine();
		    String strInput=reader.readLine();
		    
		    String[] inputArray= strInput.split(" ");
		    int[] input= new int[inputArray.length];
		    for(int i=0;i<inputArray.length;i++){
		        input[i]= Integer.parseInt(inputArray[i]);
		    }
		    
		    LISNoRecursion g= new LISNoRecursion(input);
		    System.out.println(g.getMaxLis());
		    System.out.println("functions call: "+g.fCalls);
		    c++;
		}
	}
	
	public int getMaxLis(){
		int max=0;
		for (int i = 0; i < input.length; i++) {
			int temp= this.findLIS(i);
			if(temp>max)
				max=temp;
		}
		return max;
	}
	
	public int findLIS(int index){
		if(index==0){
			this.aux[0]=1;
			//System.out.println("Aux Populated for index : 0");
			return 1;
		}
		 
		int max=0;
		for (int i =0; i <index ; i++) {
			fCalls++;
			if(input[i]<input[index]){
				if(this.aux[i]>max)
					max= this.aux[i];
			}
		}
		if(max!=0){
			this.aux[index]= 1+max;
			//System.out.println("Aux Populated for index : "+index);
		}else{
			this.aux[index]=1;
			//System.out.println("Aux Populated for index : "+index);
		}	
		return this.aux[index];
	}
}
