package com.da.coding.dp;

import java.util.Arrays;

/**
 * 
 * @author Dharmendra
 * Given a value N, if we want to make change for N cents, and we have infinite supply of 
 * each of S = { S1, S2, .. , Sm} valued coins, how many ways can we make the change? 
 * The order of coins doesn’t matter.
   For example, 
   For N = 4 and S = {1,2,3}, there are four solutions: 
   {1,1,1,1},{1,1,2},{2,2},{1,3}. So output should be 4. 
   
   For N = 10 and S = {2, 5, 3, 6}, 
   there are five solutions: {2,2,2,2,2}, {2,2,3,3}, {2,2,6}, {2,3,5} and {5,5}.
   So the output should be 5.
 *
 */
public class CoinChange {
	
	
	public static void main(String[] args) {
		CoinChange coinChange= new CoinChange();
		int[] coins={1,2,3};
		int length= coins.length;
		int amount=6;
		int count= coinChange.countCoins(coins, length, amount);
		System.out.println("Possible ways to draw coins : "+count);
	}

	private int countCoins(int[] coins, int length, int amount) {
		
		System.out.println("calculating for coins : "+Arrays.toString(coins) +"length : "+length+", amount : "+amount);
		 // If amount is 0 then there is 1 solution (do not include any coin)
	    if (amount == 0)
	        return 1;
	     
	    // If amount is less than 0 then no solution exists
	    if (amount < 0)
	        return 0;
	 
	    // If there are no coins and amount is greater than 0, then no solution exist
	    if (length <=0 && amount >= 1)
	        return 0;
	    
	   int count= countCoins(coins, length-1, amount)+ countCoins(coins, length, amount-coins[length-1]);
	   return count;
	}
	
	
}
