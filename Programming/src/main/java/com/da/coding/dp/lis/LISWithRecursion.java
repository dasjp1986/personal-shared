package com.da.coding.dp.lis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra
 * Given an array of unsorted numbers, find the length of longest common increasing subsequence
 * for ex {10,22,9,6,7,8}
 * LIS {6,7,8} so length is 3	
 */
class LISWithRecursion {
	int max=0;
	int fCalls=0;
	
	public static void main (String[] args) throws IOException{
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		String strCount= reader.readLine();
		int count= Integer.parseInt(strCount);
		
		int c=0;
		while(c<count){
		    String strSize=reader.readLine();
		    String strInput=reader.readLine();
		    
		    String[] inputArray= strInput.split(" ");
		    int[] input= new int[inputArray.length];
		    for(int i=0;i<inputArray.length;i++){
		        input[i]= Integer.parseInt(inputArray[i]);
		    }
		    
		    LISWithRecursion g= new LISWithRecursion();
		    System.out.println("overall max : "+g.getLis(input, input.length));
		    System.out.println("function called : "+g.fCalls);
		    c++;
		}
	}
	
	public int getLis(int input[], int inputLength){
		int temp, fMax=1;
		fCalls++;
		//if size of the array is 1
		if(inputLength==1)
			return 1;
		
		for (int i = 1; i < inputLength; i++) {
			temp= getLis(input, i);
			
			if(input[i-1]<input[inputLength-1] && temp+1>fMax){
				fMax= temp+1;
			}
		}
		
		if(fMax>max)
			max= fMax;
		return fMax;
	}
	
}
