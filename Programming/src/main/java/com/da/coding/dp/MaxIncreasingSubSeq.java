package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra 
 * 		   Given an array of n positive integers. Write a program to
 *         find the sum of maximum sum subsequence of the given array such that
 *         the intgers in the subsequence are sorted in increasing order. For
 *         example, if input is {1, 101, 2, 3, 100, 4, 5}, then output should be
 *         106 (1 + 2 + 3 + 100), if the input array is {3, 4, 5, 10}, then
 *         output should be 22 (3 + 4 + 5 + 10) and if the input array is {10,
 *         5, 4, 3}, then output should be 10
 *         
 *         consider this edge case
 *         1
 *         7
		   90 10 80 20 70 30 60
		   output: 120
 */
public class MaxIncreasingSubSeq {
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		int testCount= Integer.parseInt(reader.readLine());
		MaxIncreasingSubSeq problem= new MaxIncreasingSubSeq();
		for (int i = 0; i < testCount; i++) {
			int length= Integer.parseInt(reader.readLine());
			String lines[]= reader.readLine().split(" ");
			Integer array[]= new Integer[length];
			for (int j = 0; j < length; j++) {
				array[j]= Integer.parseInt(lines[j]);
			}
			System.out.println(problem.getMaxSum(array));
		}
	}

	//complexity is O(n^2)
	private int getMaxSum(Integer[] array) {
		if(array.length==1)
			return array[0];
		
		Integer ans[]= new Integer[array.length];
		ans[array.length-1]= array[array.length-1];
		
		int globalMax=0;
		for (int i = array.length-2; i >=0; i--) {
			int max= ans[array.length-1];
			ans[i]= array[i];
			for (int j = array.length-1; j > i; j--) {
				if(array[i]<array[j] && array[i]+ans[j]>max){
					max= array[i]+ans[j];
					ans[i]=max;
				}
			}
			
			if(globalMax<ans[i])
				globalMax= ans[i];
		}
		//System.out.println(Arrays.toString(ans));
		return globalMax;
	}
}
