package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra 
 * 		   Given a sequence, find the length of the longest
 *         palindromic subsequence in it. For example, if the given sequence is
 *         “BBABCBCAB”, then the output should be 7 as “BABCBAB” is the longest
 *         palindromic subseuqnce in it. “BBBBB” and “BBCBB” are also
 *         palindromic subsequences of the given sequence, but not the longest
 *         ones.
 * 
 *         The naive solution for this problem is to generate all subsequences
 *         of the given sequence and find the longest palindromic subsequence.
 *         This solution is exponential in term of time complexity. Let us see
 *         how this problem possesses both important properties of a Dynamic
 *         Programming (DP) Problem and can efficiently solved using Dynamic
 *         Programming.
 */
public class LongestPalandromicSeq {
	Integer[][] cache;
	public static void main(String[] args) throws IOException {
		LongestPalandromicSeq problem= new LongestPalandromicSeq();
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		String input= reader.readLine();
		problem.cache= new Integer[input.length()][input.length()];
		System.out.println(problem.findLPS(input,0,input.length()-1));
	}
	
	private int findLPS(String input, int start, int end) {
		if(start==end)
			return 1;
		
		if(start>end)
			return 0;
		
		if(cache[start][end]!=null)
			return cache[start][end];
		
		int max=0;
		
		
		int match=0;
		int secondMatch=0;
		int secondLastMatch=0;
		int none=0;
		
		//when first and last matches
		if(input.charAt(start)==input.charAt(end))
			match= 2+ findLPS(input, start+1, end-1);
		//when second and last matches
		else if(start+1<=end && input.charAt(start+1)==input.charAt(end))
			secondMatch= 2+findLPS(input, start+2, end-1);
		//when start and last-1 matches
		else if(end-1>=0 && input.charAt(start)==input.charAt(end-1))
			secondLastMatch= 2+findLPS(input, start+1, end-2);
		else
			none= findLPS(input, start+1, end);
		
		max= Math.max(max, match);
		max= Math.max(max, secondMatch);
		max= Math.max(max, secondLastMatch);
		max= Math.max(max, none);
		
		cache[start][end]= max;
		return max;
	}
}
