package com.da.coding.dp.fibonacci;
/**
 * 
 * @author Dharmendra
 * In this implementation of fibonacci, a table is used to maintain computed results from bottoms to up
 * and the last entry is returned
 */
public class FibonacciTabulation {
	public int findFiboNum(int num){
		int table[]= new int[num+1];
		
		table[0]=0;
		table[1]=1;
		
		for (int i = 2; i <=num; i++) {
			table[i]=table[i-1]+table[i-2];
		}
		return table[num];
	}
	
	public static void main(String[] args) {
		FibonacciTabulation tabs= new FibonacciTabulation();
		System.out.println(tabs.findFiboNum(3));
	}
}
