package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra
 * Given two strings str1 and str2 with possible list of operations
 * Add
 * Remove
 * Replace
 * Find how many minimum edit operations are required to convert str1 to str2
 * Ex- str1="cat" str2="cut" only one replace is required to replace 'a' to 'u'
 * Ex- str1="sunday" str2="saturday" two operaions are required to add 'a' and 't' and one replace is required
 * to change 'n' to 'r'
 * 
 * input
 * 1 -no of test cases
 * 4 5 -length of strings
 * geek gesek
 * 
 * Output
 * 1 -no of operations required
 */
public class EditDistDynamicProg {

	int distance;
	
	public static void main(String[] args) throws IOException {
		EditDistDynamicProg edit= new EditDistDynamicProg();
		
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		
		int testsCount= Integer.parseInt(reader.readLine());
		for (int i = 0; i < testsCount; i++) {
			String[] lengths= reader.readLine().split(" ");
			int str1Len= Integer.parseInt(lengths[0]);
			int str2Len= Integer.parseInt(lengths[1]);
			
			String[] strings= reader.readLine().split(" ");
			String str1= strings[0];
			String str2= strings[1];
					System.out.println(edit.getDistance(str1,str2,str1.length(),str2.length()));
		}
	}

	private int getDistance(String str1, String str2, int str1Length, int str2Length) {
		int minEdits=Math.max(str1.length(), str2.length());
		
		//need to add all the characters as same in str2
		if(str1Length==0)
			return str2Length;
		
		//need to remove all characters as same in str1
		if(str2Length==0)
			return str1Length;
		
		if(str1.charAt(str1Length-1)==str2.charAt(str2Length-1))
			return getDistance(str1, str2, str1Length-1, str2Length-1);
		
		else{
				int addCount= 1+getDistance(str1,str2,str1Length,str2Length-1);
				int replaceCount=1+ getDistance(str1, str2, str1Length-1, str2Length-1);
				int removeCount= 1+getDistance(str1, str2,str1Length-1,str2Length);
				minEdits= Math.min(Math.min(addCount, replaceCount), removeCount);
		}
		
		return minEdits;
	}
}
