package com.da.coding.dp;

import java.util.Arrays;

/**
 * 
 * @author Dharmendra Given a sequence of matrices, find the most efficient way
 *         to multiply these matrices together. The problem is not actually to
 *         perform the multiplications, but merely to decide in which order to
 *         perform the multiplications.
 * 
 *         We have many options to multiply a chain of matrices because matrix
 *         multiplication is associative. In other words, no matter how we
 *         parenthesize the product, the result will be the same. For example,
 *         if we had four matrices A, B, C, and D, we would have:
 * 
 *         (ABC)D = (AB)(CD) = A(BCD) = .... However, the order in which we
 *         parenthesize the product affects the number of simple arithmetic
 *         operations needed to compute the product, or the efficiency. For
 *         example, suppose A is a 10 × 30 matrix, B is a 30 × 5 matrix, and C
 *         is a 5 × 60 matrix. Then,
 * 
 *         (AB)C = (10×30×5) + (10×5×60) = 1500 + 3000 = 4500 operations A(BC) =
 *         (30×5×60) + (10×30×60) = 9000 + 18000 = 27000 operations. Clearly the
 *         first parenthesization requires less number of operations
 *
 */
public class MinMaxtrixMultiplication {
	public static void main(String[] args) {
		MinMaxtrixMultiplication matrixMult= new MinMaxtrixMultiplication();
		int[] matrix1= {40, 20, 30, 10, 30};
		int count= matrixMult.minMultiplication(matrix1, 1,matrix1.length-1);
		System.out.println("The matrices : "+Arrays.toString(matrix1)+" count : "+count);
		
		int[] matrix2= {10, 20, 30, 40, 30};
		count= matrixMult.minMultiplication(matrix2, 1,matrix2.length-1);
		System.out.println("The matrices : "+Arrays.toString(matrix2)+" count : "+count);
	}

	private int minMultiplication(int[] matrix, int i, int j) {
		int minCount= Integer.MAX_VALUE;
		if(i==j)
			return 0;
		for (int k = i; k < j; k++) {
			int leftKthCount= minMultiplication(matrix, i, k);
			int rightKthCount= minMultiplication(matrix, k+1, j);
			int multiplyLeftAndRight= matrix[i-1]*matrix[k]*matrix[j];
			int count= leftKthCount+rightKthCount+multiplyLeftAndRight;
			
			if(count<minCount)
				minCount= count;
		}
		return minCount;
	}
	
}
