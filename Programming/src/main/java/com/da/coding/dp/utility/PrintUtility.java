package com.da.coding.dp.utility;

import java.util.Arrays;

public class PrintUtility {
	public static<T> void printMatrix(T[][] matrix){
		for (int i = 0; i < matrix.length; i++) {
			System.out.println(Arrays.toString(matrix[i]));
		}
	}
}
