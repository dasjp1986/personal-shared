package com.da.coding.dp;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Dharmendra
 * find the longest common subsquence between two given Strings
 * LCS for input Sequences “ABCDGH” and “AEDFHR” is “ADH” of length 3.
 * LCS for input Sequences “AGGTAB” and “GXTXAYB” is “GTAB” of length 4.
 * 
 * Tip- Subsequence is the sequence of the characters with same relative order but not necessarily contagious  
 */

public class LCSDynamicProg {
	private Map<String, String> cache= new HashMap<>();
	
	public String findLCS(String str1, String str2){
		String result="";
		if(!str1.isEmpty() && !str2.isEmpty()){
	
			String str= cache.get(str1+":"+str2);
			if(str!=null)
				return str;
			
			int l1= str1.length();
			int l2= str2.length();
			
			if(str1.charAt(l1-1)==str2.charAt(l2-1))
				result= findLCS(str1.substring(0, l1-1), str2.substring(0,l2-1))+str1.charAt(l1-1);
			else
			{
				result= findLCS(str1, str2.substring(0, l2-1));
				String result2= findLCS(str1.substring(0, l1-1), str2);
				
				if(result2.length()>result.length())
					result= result2;
			}
		}
		cache.put(str1+":"+str2, result);
		return result;
	}
	
	public static void main(String[] args) {
		LCSDynamicProg lcsBrutForce= new LCSDynamicProg();
		String str="";
		
		str= lcsBrutForce.findLCS("abcdgh", "aedfhr");
		System.out.println(str+" : "+str.length());
		
		str= lcsBrutForce.findLCS("aggtab", "gxtxayb");
		System.out.println(str+" : "+str.length());
		
		str= lcsBrutForce.findLCS("xazby", "yvaib");
		System.out.println(str+" : "+str.length());
		
		str= lcsBrutForce.findLCS("abc", "bca");
		System.out.println(str+" : "+str.length());
		
		str= lcsBrutForce.findLCS("abab", "baba");
		System.out.println(str+" : "+str.length());
		
		str= lcsBrutForce.findLCS("XMJYAUZ", "MZJAWXU");
		System.out.println(str+" : "+str.length());
		
		str= lcsBrutForce.findLCS("CHZVFRKMLNOZJK", "PQPXRJXKITZYXACBHHKICQCOENDTOMFGDWDWFCGPXIQVKUYTDLCGDEWHTACIOHORDTQKVWCSGSPQOQMSBOAGUWN");
		System.out.println(str+" : "+str.length());
	}
}
