package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra 
 * 		   Given a rod of length n inches and an array of prices that
 *         contains prices of all pieces of size smaller than n. Determine the
 *         maximum value obtainable by cutting up the rod and selling the
 *         pieces. For example, if length of the rod is 8 and the values of
 *         different pieces are given as following, then the maximum obtainable
 *         value is 22 (by cutting in two pieces of lengths 2 and 6)
 * 
 * 
 *         length | 1 2 3 4 5 6 7 8 
 *         --------------------------------------------
 *         price | 1 5 8 9 10 17 17 20
 * 
 *         And if the prices are as following, then the maximum obtainable value
 *         is 24 (by cutting in eight pieces of length 1)
 * 
 *         length | 1 2 3 4 5 6 7 8 
 *         --------------------------------------------
 *         price | 3 5 8 9 10 17 17 20
 */
public class CuttingRod {
	private Integer[] cache;
	
	public static void main(String[] args) throws IOException {
		CuttingRod problem= new CuttingRod();
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		String[] lines= reader.readLine().split(" ");
		int[] rods= new int[lines.length];
		for (int i = 0; i < lines.length; i++) {
			rods[i]= Integer.parseInt(lines[i]);
		}
		problem.cache= new Integer[rods.length];
		System.out.println(problem.maxPrice(rods, rods.length));
	}

	private Integer maxPrice(int[] rods, int length) {
		if(length<=0)
			return 0;
		int k= length-1;
		
		if(cache[k]!=null)
			return cache[k];
		
		int result=0;
		
		for (int i = k; i >=0; i--) {
			result= Math.max(result, rods[i]+maxPrice(rods, k-i));
		}
		cache[k]= result;
		return result;
	}
}
