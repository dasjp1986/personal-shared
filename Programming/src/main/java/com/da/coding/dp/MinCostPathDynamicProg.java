package com.da.coding.dp;

/**
 * Given a cost matrix cost[][] and a position (m, n) in cost[][], 
write a function that returns cost of minimum cost path to reach (m, n) from (0, 0). 
Each cell of the matrix represents a cost to traverse through that cell. 
Total cost of a path to reach (m, n) is sum of all the costs on that path (including both
 source and destination). You can only traverse down, right and diagonally lower cells 
 from a given cell, i.e., from a given cell (i, j), cells (i+1, j), (i, j+1) 
 and (i+1, j+1) can be traversed. You may assume that all costs are positive integers.
 */


public class MinCostPathDynamicProg {
	static int[][]cache;
	
	public static void main(String[] args) {
		MinCostPathDynamicProg mcpd= new MinCostPathDynamicProg();
		int[][] matrix={
				{1,2,2,5},
				{3,5,2,1},
				{5,1,3,1},
				{3,4,4,4}
				};
		cache= new int[matrix.length][matrix[0].length];
		int m=2, n=2;
		int cost= mcpd.minCost(matrix, m, n);
		System.out.println("Minimum cost from source[0,0]"+cost);
	}

	private int minCost(int[][] matrix, int m, int n) {
		
		if(cache[m][n]>0)
			return cache[m][n];
		
		int minCost=Short.MAX_VALUE;
		int leftCost=Short.MAX_VALUE;
		int topCost=Short.MAX_VALUE;
		int diagnolCost=Short.MAX_VALUE;
		
		if(m==0 && n>0){
			leftCost= minCost(matrix, m, n-1);
		}else if(n==0 && m>0){
			topCost= minCost(matrix, m-1, n);
		}else if(m>0 && n>0){
			diagnolCost=  minCost(matrix, m-1, n-1);
			leftCost= minCost(matrix, m, n-1);
			topCost= minCost(matrix, m-1, n);
		}else{
			minCost=0;
		}
		
		minCost= Math.min(minCost, diagnolCost);
		minCost= Math.min(minCost, topCost);
		minCost= Math.min(minCost, leftCost);
		
		//add the destionation cost
		minCost= minCost+matrix[m][n];
		System.out.println("Minimum Calculated : "+minCost +"  for m :"+m+ ", n :"+n);
		cache[m][n]= minCost;
		return minCost;
	}
}
