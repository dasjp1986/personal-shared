package com.da.coding.dp.fibonacci;
/**
 * 
 * @author Dharmendra
 * Fibonacci Series
 * 0,1,1,2,3,5
 */
public class FibonacciSimple {
	
	private static int r=0;
	public int getFiboNum(int n){
		System.out.println("recursed--"+(++r));
		if(n<=1)
			return n;
		return getFiboNum(n-1)+getFiboNum(n-2);
	}
	
	public static void main(String[] args) {
		FibonacciSimple fib= new FibonacciSimple();
		System.out.println(fib.getFiboNum(100));
	}
}
