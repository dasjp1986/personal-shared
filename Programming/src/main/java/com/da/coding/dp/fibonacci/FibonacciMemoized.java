package com.da.coding.dp.fibonacci;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Dharmendra
 * maintain a lookup table and look for precomputed result before computing it
 * top to down approach
 */
public class FibonacciMemoized {
	Map<Integer, Integer> mem= new HashMap<>();
	private static int r=0;
	
	public int getNum(int num){
		Integer fibNum = mem.get(num);
		if(fibNum!=null)
			return fibNum;
		System.out.println("recursed--"+(++r));
		if(num<=1)
			return num;
		int val= getNum(num-1)+getNum(num-2);
		mem.put(num, val);
		return val;
	}
	
	public static void main(String[] args) {
		FibonacciMemoized memoized= new FibonacciMemoized();
		System.out.println(memoized.getNum(100));
	}
}
