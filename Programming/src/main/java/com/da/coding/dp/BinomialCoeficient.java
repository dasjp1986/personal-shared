package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author Dharmendra Following are common definition of Binomial Coefficients.
 *         1) A binomial coefficient C(n, k) can be defined as the coefficient
 *         of X^k in the expansion of (1 + X)^n.
 * 
 *         2) A binomial coefficient C(n, k) also gives the number of ways,
 *         disregarding order, that k objects can be chosen from among n
 *         objects; more formally, the number of k-element subsets (or
 *         k-combinations) of an n-element set.
 * 
 *         The Problem Write a function that takes two parameters n and k and
 *         returns the value of Binomial Coefficient C(n, k). For example, your
 *         function should return 6 for n = 4 and k = 2, and it should return 10
 *         for n = 5 and k = 2.
 */
public class BinomialCoeficient {
	
	int[][] cache;
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(reader.readLine());
		int k = Integer.parseInt(reader.readLine());

		BinomialCoeficient bim = new BinomialCoeficient();
		bim.cache= new int[n][k];
		System.out.println(bim.coefficient(n, k));
	}

	/**
	 * 
	 * @param n
	 * @param k
	 * @return The value of C(n, k) can be recursively calculated using
	 *         following standard formula for Binomial Coefficients.
	 * 
	 *         C(n, k) = C(n-1, k-1) + C(n-1, k) C(n, 0) = C(n, n) = 1
	 *         its more like factorial(n)/(factorial(k)*factorial(n-k))
	 */
	private int coefficient(int n, int k) {
		if (k == 0 || n==k)
			return 1;
		
		int result= cache[n-1][k-1];
		if(result!=0)
			return result;

		result= coefficient(n-1, k-1)+coefficient(n-1, k);
		cache[n-1][k-1]=result;
		return result;
	}
}
