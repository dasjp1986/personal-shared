package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;

import com.da.coding.dp.utility.PrintUtility;

/**
 * 
 * @author Dharmendra The following is a description of the instance of this
 *         famous puzzle involving n=2 eggs and a building with k=36 floors.
 * 
 *         Suppose that we wish to know which stories in a 36-story building are
 *         safe to drop eggs from, and which will cause the eggs to break on
 *         landing. We make a few assumptions:
 * 
 *         …..An egg that survives a fall can be used again. …..A broken egg
 *         must be discarded. …..The effect of a fall is the same for all eggs.
 *         …..If an egg breaks when dropped, then it would break if dropped from
 *         a higher floor. …..If an egg survives a fall then it would survive a
 *         shorter fall. …..It is not ruled out that the first-floor windows
 *         break eggs, nor is it ruled out that the 36th-floor do not cause an
 *         egg to break. In this problem you have to find minimum trials to
 *         solve for n eggs and k floors. For more description on this problem
 *         (CLICK ON THIS LINK)
 * 
 *         Input: The first line contains an integer T, depicting total number
 *         of test cases. Then following T lines contains 2 integers n and k.
 * 
 *         Output: Print the minimum trials required to solve the problem.
 * 
 *         Constraints: 1<=T<=30 1<=n<=10 1<=k<=50

 *         Example: Input: 1 2 10
 *         Output: 4
 */
public class EggDroppingPuzzle {
	Integer[][] cache;
	public static void main(String[] args) throws NumberFormatException, IOException {
		EggDroppingPuzzle puzzle= new EggDroppingPuzzle();
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		int testCases= Integer.parseInt(reader.readLine());
		for (int i = 0; i < testCases; i++) {
			String[] inputs= reader.readLine().split(" ");
			int eggs= Integer.parseInt(inputs[0]);
			int floors=Integer.parseInt(inputs[1]);
			puzzle.cache= new Integer[eggs][floors];
			System.out.println(puzzle.minAttempts(eggs, floors));
			PrintUtility.<Integer>printMatrix(puzzle.cache);
		}
	}

	private int minAttempts(int eggs, int floors) {
		//test failed, no eggs left to test anymore
		if(eggs==0 && floors>0)
			return 0;
		
		//nothing left to test
		if(eggs==0 || floors==0)
			return 0;
		
		//you have only one way left to test, dropping from each floor on increasing order
		if(eggs==1)
			return floors;
		
		if(cache[eggs-1][floors-1]!=null)
			return cache[eggs-1][floors-1];
		
		int min=floors;
		for (int i = 1; i <= floors; i++) {
			int temp1= 1+minAttempts(eggs-1, i-1);
			int temp2= 1+minAttempts(eggs, floors-i);
			int max= Math.max(temp1, temp2);
			if(max<min){
				min= max;
				cache[eggs-1][floors-1]= min;
				System.out.println("min : at floor :"+min+" : "+i);
			}
		}
		return min;
	}
}
