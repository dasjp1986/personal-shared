package com.da.coding.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * @author Dharmendra You are given weights and values of N items, put these
 *         items in a knapsack of capacity W to get the maximum total value in
 *         the knapsack. Note that we have only one quantity of each item, In
 *         other words, given two integer arrays val[0..N-1] and wt[0..N-1]
 *         which represent values and weights associated with N items
 *         respectively. Also given an integer W which represents knapsack
 *         capacity, find out the maximum value subset of val[] such that sum of
 *         the weights of this subset is smaller than or equal to W. You cannot
 *         break an item, either pick the complete item, or don’t pick it (0-1
 *         property).
 * 
 *         Input:
 *         The first line of input contains an integer T denoting the number of
 *         test cases. Then T test cases follow. Each test case consists of four
 *         lines. The first line consists of N the number of items. The second
 *         line consists of W, the maximum capacity of the knapsack. In the next
 *         line are N space separated positive integers denoting the values of
 *         the N items and in the fourth line are N space separated positive
 *         integers denoting the weights of the corresponding items.
 * 
 *         Output:
 * 
 *         Print the maximum possible value you can get with the given
 *         conditions that you can obtain for each test case in a new line.
 * 
 *         Constraints:
 *         1≤T≤100
 *         1≤N≤100
 *         1≤W≤100
 *         1≤wt[i]≤100
 *         1≤v[i]≤100
 * 
 *         Example:
 * 
 *         Input: 
 *         1 -test cases
 *         3 -values/weights size
 *         4 -capacity
 *         1 2 3 -values
 *         4 5 1 -weights
 *         Output: 
 *         3
 */
public class KnapSack10Problem {
	static int recurse=0;
	int[][] cache;
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		KnapSack10Problem problem= new KnapSack10Problem();
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		int testCount= Integer.parseInt(reader.readLine());
		for (int i = 0; i < testCount; i++) {
			int size= Integer.parseInt(reader.readLine());
			int capacity= Integer.parseInt(reader.readLine());
			int[] values= new int[size];
			int[] weights= new int[size];
			String value= reader.readLine();
			String weight= reader.readLine();
			
			int j=0;
			for (String v: value.split(" ")) {
				values[j]= Integer.parseInt(v);
				j++;
			}
			
			j=0;
			for (String w: weight.split(" ")) {
				weights[j]= Integer.parseInt(w);
				j++;
			}
			problem.cache= new int[capacity+1][values.length];
			System.out.println(problem.findMaxValue(capacity, values.length-1, values, weights));
		}
	}

	private int findMaxValue(int capacity, int n, int[] values, int[] weights) {
		
		int max=0;
		
		if(capacity==0 || n<0)
			return 0;
		if(cache[capacity][n]!=0)
			return cache[capacity][n];
		
		recurse++;
		//System.out.println("Recursed...(capacity, n)-("+capacity+","+n+")"+recurse);
		//check if weight at n is greater than the capacity
		if(weights[n]>capacity)
			max= findMaxValue(capacity, n-1, values, weights);
		else{
			//when nth weight is included
			int temp1= values[n]+findMaxValue(capacity-weights[n], n-1, values, weights);
			
			//when nth weight is not included
			int temp2= findMaxValue(capacity, n-1, values, weights);
			max= Math.max(max, temp1);
			max= Math.max(max, temp2);
		}
		cache[capacity][n]= max;
		return max;
	}

}
	