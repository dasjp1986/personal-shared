package com.da.coding.tree;

public class Node {

	Node prev;
	int value;
	Node next;
	
	public Node(int value) {
		super();
		this.value = value;
	}

	public Node getPrev() {
		return prev;
	}

	public void setPrev(Node prev) {
		this.prev = prev;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	@Override
	public String toString() {
		return "TreeNode [prev=" + prev + ", value=" + value + ", next=" + next + "]";
	}
}
