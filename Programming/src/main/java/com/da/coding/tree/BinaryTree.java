package com.da.coding.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class BinaryTree {
	private Node root;
	private static int max=0;
	
	public Node getRoot() {
		return root;
	}

	public void addNode(Node node) {
		if (root == null)
			root = node;
		else {
			Queue<Node> queue = new LinkedList<>();
			Node temp = root;
			while (temp != null) {
				if (temp.getPrev() == null) {
					temp.setPrev(node);
					break;
				} else if (temp.getNext() == null) {
					temp.setNext(node);
					break;
				}
				queue.add(temp.getPrev());
				queue.add(temp.getNext());

				temp = queue.poll();
			}
		}
	}

	public void inOrderTraversal(Node node) {
		if (node != null) {
			inOrderTraversal(node.getPrev());
			System.out.println(node.getValue());
			inOrderTraversal(node.getNext());
		}
	}

	// traverse all possible routes from the root node
	/** Given a Binary tree, traverse all possible paths starting from root node
	 * and return max occurrences of any node in any of the path 
	 * 				4
	 * 			   / \
	 * 			  5   6
	 * 			 /   / \
	 *          4   1   6
	 *  all possible paths are
	 *  [4,5,4]= 2( 4 is repeated twice)
	 *  [4,6,1]= 1
	 *  [4,6,6]= 2(6 is repeated twice)  **/
	public void traverseAllPaths(Node node, List<Integer> parent) {
		if (parent == null) {
			parent = new ArrayList<>();
		}
		parent.add(node.getValue());
		List<Integer> left, right;
		if (node.getPrev() != null) {
			left= new ArrayList<>();
			left.addAll(parent);
			traverseAllPaths(node.getPrev(), left);
		}
		
		if(node.getNext()!=null){
			right= new ArrayList<>();
			right.addAll(parent);
			traverseAllPaths(node.getNext(), right);
		}
		
		if(node.getNext()==null && node.getPrev()==null){
			System.out.println(parent);
			int m= maxRecurrence(parent);
			if(m>max)
				max= m;
		}
	}

	private int maxRecurrence(List<Integer> parent) {
		int m=0;
		Map<Integer, Integer> map= new HashMap<>();
		for(Integer k: parent){
			Integer c= map.get(k);
			if(c==null)
				c=0;
			c++;
			if(c>m)
				m=c;
			map.put(k, c);
		}
		return m;
	}

	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();

		int[] input= {4,5,6,4,1,6,6,4};
		for (int i = 0; i < input.length; i++) {
			tree.addNode(new Node(input[i]));
			System.out.println("Added node.." + input[i]);
		}
		System.out.println("running in-order traversal");
		tree.inOrderTraversal(tree.getRoot());
		
		System.out.println("Printing All Paths");
		tree.traverseAllPaths(tree.getRoot(), null);
		
		System.out.println("Max Node recurrence "+max);
	}
}
