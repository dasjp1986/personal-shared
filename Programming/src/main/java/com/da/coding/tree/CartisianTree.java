package com.da.coding.tree;

/**
 * 
 * @author Dharmendra Build a cartisian tree given an in order traversal A
 *         Cartisian tree is a tree where - in order traversal will be same as
 *         given input set - it is either has max or min heap property
 */
public class CartisianTree {

	// Time Complexity O(n^2)
	public Node buildCartisianTree(int[] a, int start, int end) {

		if (start == end) {
			Node node = new Node(a[start]);
			return node;
		}

		int max = Integer.MIN_VALUE;
		int r = 0;
		for (int i = start; i <= end; i++) {
			if (a[i] > max) {
				max = a[i];
				r = i;
			}
		}
		Node temp = new Node(a[r]);
		if (r > start)
			temp.setPrev(buildCartisianTree(a, start, r - 1));
		if (r < end)
			temp.setNext(buildCartisianTree(a, r + 1, end));
		return temp;
	}

	public void preOrderTraversal(Node node) {
		if (node != null) {
			preOrderTraversal(node.getPrev());
			System.out.println(node.getValue());
			preOrderTraversal(node.getNext());
		}
	}

	public static void main(String[] args) {
		int[] input = {5,5,6,2,1};
		CartisianTree tree = new CartisianTree();
		Node root = tree.buildCartisianTree(input, 0, input.length - 1);
		tree.preOrderTraversal(root);
	}

	
}
