package com.da.coding.dynamic_connectivity;

import java.util.Arrays;

/**
 * 
 * @author dagraw1 index of the array represents the node and value at the index
 *         represents its parent
 * 
 *         Problem of tall trees are solved by using an approach where trees
 *         with smaller size are added to the root of
 */
public class QuickUnionWithHeights {
	int[] parent;
	int[] height;

	public QuickUnionWithHeights(int size) {
		parent = new int[size];
		height= new int[size];
		for (int i = 0; i < size; i++)
			parent[i] = i;
	}

	// Time Complexity O(logn)
	private int findRoot(int node) {
		if (parent[node] == node)
			return node;

		return findRoot(parent[node]);
	}

	// Time Complexity O(logn)= height of the tree
	public void quickUnion(int node1, int node2) {
		int parent1 = findRoot(node1);
		int parent2 = findRoot(node2);

		if (parent1 != parent2) {
			if (height[parent1] < height[parent2]) {
				parent[parent1] = parent2;
				height[parent2]= height[parent1]+1;
			} else{
				parent[parent2] = parent1;
				height[parent1]= height[parent2]+1;
			}
		}
	}

	// Time Complexity O(logn)
	public boolean isConnected(int node1, int node2) {
		if (findRoot(node1) == findRoot(node2))
			return true;

		return false;
	}

	public static void main(String[] args) {
		int n = 8;
		QuickUnionWithHeights quickFind = new QuickUnionWithHeights(n);
		System.out.println("1-2 " + quickFind.isConnected(1, 2));
		quickFind.quickUnion(1, 2);
		System.out.println("1-2 " + quickFind.isConnected(1, 2));
		System.out.println("Weighted array..."+Arrays.toString(quickFind.height));

		System.out.println("3-4 " + quickFind.isConnected(3, 4));
		quickFind.quickUnion(3, 4);
		System.out.println("3-4 " + quickFind.isConnected(3, 4));
		System.out.println("Weighted array..."+Arrays.toString(quickFind.height));

		System.out.println("5-6 " + quickFind.isConnected(5, 6));
		quickFind.quickUnion(5, 6);
		System.out.println("5-6 " + quickFind.isConnected(5, 6));
		System.out.println("Weighted array..."+Arrays.toString(quickFind.height));
		
		System.out.println("2-3 " + quickFind.isConnected(2, 3));
		quickFind.quickUnion(2, 3);
		System.out.println("2-3 " + quickFind.isConnected(2, 3));
		System.out.println("Weighted array..."+Arrays.toString(quickFind.height));
		
		System.out.println("4-6 " + quickFind.isConnected(4, 6));
		quickFind.quickUnion(4, 6);
		System.out.println("4-6 " + quickFind.isConnected(4, 6));
		System.out.println("Weighted array..."+Arrays.toString(quickFind.height));
		System.out.println("4-5 " + quickFind.isConnected(4, 5));
		System.out.println("2-6 " + quickFind.isConnected(2, 6));
		System.out.println("3-5 " + quickFind.isConnected(3, 5));
		
	}
}
