package com.da.coding.dynamic_connectivity;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 * A country has cities numbered from 1 to n
 * and there are m bidirectional routes between cities
 * 
 * Task is to find maximum no. of routes a traveler can access.
 * 
 * Input
 * 8 5
 * 1 2
 * 7 4
 * 7 3
 * 5 8
 * 1 3
 * 
 * n=8 and m=5
 * 
 * Output: 5
 * Number of stations can be visited, largest set of stations
 * {1,2,3,4,7}= 5
 * 
 */
public class MaximalTourismWithFlatTree {
	int[] parent;
	int[] weight;
	int max=0;

	public MaximalTourismWithFlatTree(int size) {
		parent = new int[size];
		weight = new int[size];
		for (int i = 0; i < size; i++) {
			parent[i] = i;
			weight[i] = 1;
		}
	}

	// Time Complexity O(logn)
	private int findRoot(int node) {
		while(node!=parent[node]){
			parent[node]= parent[parent[node]];
			node= parent[node];
		}
		
		return node;
	}

	// Time Complexity O(n)
	public void quickUnion(int node1, int node2) {
		int parent1 = findRoot(node1);
		int parent2 = findRoot(node2);

		if (parent1 != parent2) {
			if (weight[parent1] >= weight[parent2]) {
				parent[parent2] = parent1;
				weight[parent1] = weight[parent1] + weight[parent2];
				weight[parent2] = 1;
				
				if (max < weight[parent1])
					max = weight[parent1];
			} else {
				parent[parent1] = parent2;
				weight[parent2] = weight[parent1] + weight[parent2];
				weight[parent1] = 1;
				
				if (max < weight[parent2])
					max = weight[parent2];
			}
		}
	}

	// Time Complexity O(1)
	public boolean isConnected(int node1, int node2) {
		if (findRoot(node1) == findRoot(node2))
			return true;

		return false;
	}

	public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		int n= scn.nextInt();
		int m= scn.nextInt();
		
		MaximalTourismWithFlatTree quickFind = new MaximalTourismWithFlatTree(n+1);
		
		for(int i=0;i<m;i++){
			quickFind.quickUnion(scn.nextInt(), scn.nextInt());
			//System.out.println(Arrays.toString(quickFind.parent));
			//System.out.println(Arrays.toString(quickFind.weight));
		}
		scn.close();
		
		//System.out.println("Maximum stations visited : "+quickFind.max);
		long startTime= System.nanoTime();
		System.out.println(quickFind.max);
		//System.out.println("Time Taken : "+(System.nanoTime()-startTime));

	}
}
