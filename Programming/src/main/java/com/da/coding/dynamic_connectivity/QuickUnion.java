package com.da.coding.dynamic_connectivity;
/**
 * 
 * @author dagraw1
 * index of the array represents the node and value at the index represents its parent
 
 * Problem with this approach is you may end with tall trees and so is the complexity
 */
public class QuickUnion {
	int[] parent;
	
	public QuickUnion(int size) {
		parent= new int[size];
		for(int i=0;i<size;i++)
			parent[i]=i;
	}
	
	//Time Complexity O(n)
	private int findRoot(int node){
		if(parent[node]==node)
			return node;
		
		return findRoot(parent[node]);
	}
	
	//Time Complexity O(n)
	public void quickUnion(int node1, int node2){
		int parent1= findRoot(node1);
		int parent2= findRoot(node2);
		
		if(parent1!=parent2){
			parent[parent2]= parent1;
		}
	}
	
	//Time Complexity O(n)
	public boolean isConnected(int node1, int node2){
		if(findRoot(node1)==findRoot(node2))
			return true;
		
		return false;
	}
	
	public static void main(String[] args) {
		int n=5;
		QuickUnion quickFind= new QuickUnion(n);
		System.out.println("0-1 "+quickFind.isConnected(0, 1));
		quickFind.quickUnion(0, 1);
		System.out.println("0-1 "+quickFind.isConnected(0, 1));
		
		System.out.println("2-3 "+quickFind.isConnected(2, 3));
		quickFind.quickUnion(2, 3);
		System.out.println("2-3 "+quickFind.isConnected(2, 3));
		
		System.out.println("1-3 "+quickFind.isConnected(1, 3));
		quickFind.quickUnion(1, 3);
		System.out.println("1-2 "+quickFind.isConnected(1, 2));
		System.out.println("1-3 "+quickFind.isConnected(1, 3));
		System.out.println("0-3 "+quickFind.isConnected(0, 3));
	}
}
