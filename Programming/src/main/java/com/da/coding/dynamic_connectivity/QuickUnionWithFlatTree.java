package com.da.coding.dynamic_connectivity;

import java.util.Arrays;

/**
 * 
 * @author dagraw1 index of the array represents the node and value at the index
 *         represents its parent
 * 
 *         Problem of tall trees are solved by using an approach where trees
 *         with smaller size are added to the root of
 * 
 *         It can be even more improved by using flat trees where each node
 *         directly connects to its root node
 */
public class QuickUnionWithFlatTree {
	int[] parent;
	int[] weight;

	public QuickUnionWithFlatTree(int size) {
		parent = new int[size];
		weight = new int[size];
		for (int i = 0; i < size; i++) {
			parent[i] = i;
			weight[i] = 1;
		}
	}

	// Time Complexity O(1)
	private int findRoot(int node) {
		return parent[node];
	}

	// Time Complexity O(n)
	public void quickUnion(int node1, int node2) {
		int parent1 = findRoot(node1);
		int parent2 = findRoot(node2);

		if (parent1 != parent2) {
			if (weight[parent1] >= weight[parent2]) {
				for (int i = 1; i < parent.length; i++) {
					if (parent[i] == parent2) {
						parent[i] = parent1;
						weight[i] = 1;
						weight[parent1] = weight[parent1] + 1;
					}
				}
			} else {
				for (int i = 1; i < parent.length; i++) {
					if (parent[i] == parent1) {
						parent[i] = parent2;
						weight[i] = 1;
						weight[parent2] = weight[parent1] + 1;
					}
				}

			}
		}
	}

	// Time Complexity O(n)
	public boolean isConnected(int node1, int node2) {
		if (findRoot(node1) == findRoot(node2))
			return true;

		return false;
	}

	public static void main(String[] args) {
		int n = 8;
		QuickUnionWithFlatTree quickFind = new QuickUnionWithFlatTree(n);
		System.out.println("1-2 " + quickFind.isConnected(1, 2));
		quickFind.quickUnion(1, 2);
		System.out.println("1-2 " + quickFind.isConnected(1, 2));
		System.out.println("Weighted array..." + Arrays.toString(quickFind.weight));

		System.out.println("3-4 " + quickFind.isConnected(3, 4));
		quickFind.quickUnion(3, 4);
		System.out.println("3-4 " + quickFind.isConnected(3, 4));
		System.out.println("Weighted array..." + Arrays.toString(quickFind.weight));

		System.out.println("5-6 " + quickFind.isConnected(5, 6));
		quickFind.quickUnion(5, 6);
		System.out.println("5-6 " + quickFind.isConnected(5, 6));
		System.out.println("Weighted array..." + Arrays.toString(quickFind.weight));

		System.out.println("2-3 " + quickFind.isConnected(2, 3));
		quickFind.quickUnion(2, 3);
		System.out.println("2-3 " + quickFind.isConnected(2, 3));
		System.out.println("Weighted array..." + Arrays.toString(quickFind.weight));

		System.out.println("4-6 " + quickFind.isConnected(4, 6));
		quickFind.quickUnion(4, 6);
		System.out.println("4-6 " + quickFind.isConnected(4, 6));
		System.out.println("Weighted array..." + Arrays.toString(quickFind.weight));
		System.out.println("4-5 " + quickFind.isConnected(4, 5));
		System.out.println("2-6 " + quickFind.isConnected(2, 6));
		System.out.println("3-5 " + quickFind.isConnected(3, 5));

	}
}
