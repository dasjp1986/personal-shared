package com.da.coding.trie;

//assume trie DS here supports only a-z alphabets
public class TriedDS {
	private TNode root;
	private final int CHAR_START = 'a';
	private final int CHAR_LAST = 'z';

	public TriedDS() {
		root = new TNode('$');
	}

	//Time Complexity O(M)- M size of keyword, space complexity- O(N*M)- N number of keywords
	public void insert(String keyword) {
		char[] charArray = keyword.toCharArray();
		TNode tNode = null;
		TNode pNode = root;
		for (char ch : charArray) {
			if (ch < CHAR_START || ch > CHAR_LAST)
				throw new RuntimeException("Invalid character being inserted in TRIE");

			tNode = pNode.getNodes()[ch - CHAR_START];
			if (tNode == null){
				tNode = new TNode(ch);
				pNode.getNodes()[ch - CHAR_START]= tNode;
			}

			pNode = tNode;
		}
		pNode.setEndOfWord(true);
		System.out.println("inserted successfully, keyword-"+keyword);
	}

	//Time Complexity O(M)- M size of keyword
	public boolean isPresent(String keyword) {
		char[] charArray = keyword.toCharArray();
		TNode tNode = null;
		TNode pNode = root;
		for (char ch : charArray) {
			if (ch < CHAR_START || ch > CHAR_LAST)
				throw new RuntimeException("Invalid character being searched in TRIE");

			tNode = pNode.getNodes()[ch - CHAR_START];
			if (tNode == null)
				return false;

			pNode = tNode;
		}

		return pNode.isEndOfWord;
	}
	
	public static void main(String[] args) {
		TriedDS ds= new TriedDS();
		ds.insert("they");
		ds.insert("their");
		ds.insert("there");
		
		System.out.println(ds.isPresent("the"));
		System.out.println(ds.isPresent("their"));
		System.out.println(ds.isPresent("there"));
	}
}

class TNode {
	// this is needed only for visualizations
	char label;
	TNode[] nodes;
	boolean isEndOfWord;

	public TNode(char ch) {
		this.label = ch;
		nodes = new TNode[26];
		isEndOfWord = false;
	}

	public char getLabel() {
		return label;
	}

	public void setLabel(char label) {
		this.label = label;
	}

	public TNode[] getNodes() {
		return nodes;
	}

	public void setNodes(TNode[] nodes) {
		this.nodes = nodes;
	}

	public boolean isEndOfWord() {
		return isEndOfWord;
	}

	public void setEndOfWord(boolean isEndOfWord) {
		this.isEndOfWord = isEndOfWord;
	}

}
