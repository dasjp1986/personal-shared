package com.da.coding.backtracking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Dharmendra 
 * 		   Given a string, print all permutations of a given string.
 * 
 *         Input:
 * 
 *         The first line of input contains an integer T denoting the number of
 *         test cases. Each test case contains a single string in capital
 *         letter.
 * 
 *         Output:
 * 
 *         Print all permutations of a given string with single space and all
 *         permutations should be in lexicographically increasing order.
 * 
 *         Constraints:
 * 
 *         1 ≤ T ≤ 10 1 ≤ size of string ≤ 5
 * 
 *         Example:
 * 
 *         Input: 2 ABC
 * 
 *         ABSG
 * 
 *         Output: ABC ACB BAC BCA CAB CBA
 * 
 *         ABGS ABSG AGBS AGSB ASBG ASGB BAGS BASG BGAS BGSA BSAG BSGA GABS GASB
 *         GBAS GBSA GSAB GSBA SABG SAGB SBAG SBGA SGAB SGBA
 *
 */

public class StringPermutation {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = scanner.next();
		List<String> perms = new ArrayList<>();
		perms.add(str);
		perms = findPerms(perms, 0, str.length());
		Collections.sort(perms);
		for (String s : perms) {
			System.out.print(s);
			System.out.print(" ");
		}
		scanner.close();
	}

	private static List<String> findPerms(List<String> perms, int index, int length) {
		List<String> res = new ArrayList<>();
		if (perms == null || perms.isEmpty())
			return res;

		if (index >= length - 1)
			return perms;

		for (String str : perms) {
			res.add(str);
			for (int i = index + 1; i < str.length(); i++) {
				StringBuilder temp = new StringBuilder(str);
				char ch = temp.charAt(i);
				temp.setCharAt(i, temp.charAt(index));
				temp.setCharAt(index, ch);
				res.add(temp.toString());
			}
		}
		res = findPerms(res, index + 1, length);
		return res;
	}
}
