package com.da.coding.greedy;
/**
 * 
 * @author Dharmendra
 * find the highest product of three numbers
 * Ex- [5,1,4,2,7] Answer- 5*4*7= 140
 * Ex- [5,1,-4,2,-7] Answer- 5*-4*-7= 140
 *
 */
public class HighestProductOfThreeNums {
	public static void main(String[] args) {
		HighestProductOfThreeNums caller= new HighestProductOfThreeNums();
		int[] input1={5,1,4,2,7};
		int result1= caller.findHighestProduct(input1);
		System.out.println("Max product for input1 "+result1);
		
		int[] input2={5,1,-4,2,7};
		int result2= caller.findHighestProduct(input2);
		System.out.println("Max product for input2 "+result2);
		
		int[] input3={5,1,-4,2,-7};
		int result3= caller.findHighestProduct(input3);
		System.out.println("Max product for input3 "+result3);
		
		int[] input4={5,1,-4};
		int result4= caller.findHighestProduct(input4);
		System.out.println("Max product for input4 "+result4);
	}

	private int findHighestProduct(int[] input) {
		if(input==null || input.length<3)
			return 0;
		
		int highest=Math.max(input[0], input[1]);
		int lowest=Math.min(input[0], input[1]);
		int prodHighestOf2=input[0]*input[1];
		int prodLowestOf2=input[0]*input[1];
		int maxProduct=input[0]*input[1]*input[2];
		
		for (int i = 2; i < input.length; i++) {
			int current=input[i];
			maxProduct= findMax(maxProduct, current*prodHighestOf2, current*prodLowestOf2);
			prodHighestOf2=findMax(prodHighestOf2, current*highest, current*lowest);
			prodLowestOf2= findMin(prodLowestOf2, current*highest, current*lowest);
			highest= Math.max(highest, current);
			lowest=Math.min(lowest, current);
		}
		return maxProduct;
	}

	private int findMax(int a, int b, int c) {
		return Math.max(a, Math.max(b, c));
	}
	
	private int findMin(int a, int b, int c) {
		return Math.min(a, Math.min(b, c));
	}
}
