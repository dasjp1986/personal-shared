package com.da.coding.greedy;

/*
 * Condense a given array of meeting schedules wherever they overlap giving an
 * understanding at what point everybody is free
 * Ex- [(0, 1), (3, 5), (4, 8), (10, 12), (9, 10)]  
 * Answer [(0, 1), (3, 8), (9, 12)]
 */
public class OverlappingMeetings {
	public static void main(String[] args) {
		OverlappingMeetings caller= new OverlappingMeetings();
		int[][]meetings1= {{0,1},{3,5},{4,8},{10,12},{9,10}};
		System.out.println("Result for meetings1 ");
		caller.printResult(caller.findCondensed(meetings1, meetings1.length));
		
		int[][]meetings2= {{10,12},{9,10}};
		System.out.println("Result for meetings2 ");
		caller.printResult(caller.findCondensed(meetings2, meetings2.length));
		
		int[][]meetings3= {{1,12}};
		System.out.println("Result for meetings3 ");
		caller.printResult(caller.findCondensed(meetings3, meetings3.length));
		
		int[][]meetings4= {{0,1},{3,9},{1,3}};
		System.out.println("Result for meetings4 ");
		caller.printResult(caller.findCondensed(meetings4, meetings4.length));
		
		int[][]meetings5= {{50,101},{40,49},{1,30},{25,35},{10,45}};
		System.out.println("Result for meetings5 ");
		caller.printResult(caller.findCondensed(meetings5, meetings5.length));
	}
	
	private int[][] findCondensed(int[][]meetings, int length){
		if(meetings==null || length<2)
			return meetings;
		
		int[][] results = new int[length][2];
		results[0][0]=meetings[0][0];
		results[0][1]=meetings[0][1];
		int resultSize=1;
		
		for (int i = 1; i <length; i++) {
			//check for negative case meetings can not be condensed
			boolean isDiscrete=false;
			for (int j = 0; j < resultSize; j++) {
				if((results[j][0]<meetings[i][0] && results[j][1]<meetings[i][0]) ||
						(results[j][0]>meetings[i][1] && results[j][1]>meetings[i][1])){
					//adding a new discrete meeting time
					isDiscrete=true;
				}else{
					isDiscrete=false;
					results[j][0]=Math.min(results[j][0], meetings[i][0]);
					results[j][1]=Math.max(results[j][1], meetings[i][1]);
				}
				
			}	
			if(isDiscrete){
				results[resultSize][0]=meetings[i][0];
				results[resultSize][1]=meetings[i][1];
				resultSize++;
			}
		}
		
		//further recursion is required to look for more condensed results
		if(length>resultSize)
			results= findCondensed(results, resultSize);
		return results;
	}
	
	private void printResult(int[][]results){
		for (int i = 0; i < results.length; i++) {
			if(results[i][0]==0 && results[i][1]==0)
				continue;
			System.out.println(results[i][0]+" , "+results[i][1]);
		}
	}
}
