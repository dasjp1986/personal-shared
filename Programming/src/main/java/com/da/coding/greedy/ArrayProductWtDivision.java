package com.da.coding.greedy;
/**
 * 
 * @author Dharmendra
 * Replace every element with product of other elements except the current one
 * Use of Division is not allowed
 * for ex- [1,2,6,5,9]
 * [2*6*5*9,1*6*5*9,1*2*5*9,1*2*6*9,1*2*6*5]
 */
public class ArrayProductWtDivision {
	public static void main(String[] args) {
		ArrayProductWtDivision caller= new ArrayProductWtDivision();
		int[] input1= {1,2,6,5,9};
		System.out.println("elements product for input1: ");
		caller.printArray(caller.getProductArray(input1));
		
		int[] input2= {1,2};
		System.out.println("elements product for input2: ");
		caller.printArray(caller.getProductArray(input2));
	}
	
	
	
	int[] getProductArray(int[] input){
		int[] result= new int[input.length];
		
		if(input==null || input.length<2)
			return input;
		
		int front=1;
		
		result[0]=1;
		while(front<input.length){
			result[front]= result[front-1]*input[front-1];
			front++;
		}
		
		int rear=input.length-2;
		int rearProd=1;
		while(rear>-1){
			rearProd= rearProd*input[rear+1];
			result[rear]= result[rear]*rearProd;
			rear--;
		}
		return result;
	}
	
	public void printArray(int[] print){
		for (int i : print) {
			System.out.println(i);
		}
	}
}
