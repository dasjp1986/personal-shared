package com.da.coding.greedy;

/**
 * an array contains stock prices for yesterday ordered with increasing timestamp
 * need to find max profit for the day
 * for ex. [10, 7, 5, 8, 11, 9]
 * stock is bought at 5 and sold at 11, so max profit is 6
 */
public class StockPricesMaxProfit 
{
    public static void main( String[] args )
    {
    	StockPricesMaxProfit caller= new StockPricesMaxProfit();
    	
    	int[] day1Stocks= {10, 7, 5, 8, 11, 9};
    	System.out.println("Day1 max profit "+caller.maxProfit(day1Stocks));
    	
    	int[] day2Stocks= {10, 109, 1, 8, 11, 101};
    	System.out.println("Day2 max profit "+caller.maxProfit(day2Stocks));
    	
    	int[] day3Stocks= {10,8,6,4,3,2,1};
    	System.out.println("Day3 max profit "+caller.maxProfit(day3Stocks));
    	
    }
    
    public int maxProfit(int[] stocks){
    	int maxProfit=0;
    	int min=0;
    	if(stocks== null || stocks.length<2)
    		return 0;
    	
    	min= stocks[0];
    	for (int i = 1; i < stocks.length; i++) {
			if(stocks[i]-min>maxProfit)
				maxProfit= stocks[i]-min;
			
    		if(stocks[i]<min)
				min= stocks[i];
		}
    	
    	return maxProfit;
    }
}
