package com.da.coding.greedy;

import com.da.coding.utility.PrintUtility;
/**
 * 
 * @author Dharmendra
 * 	Activity Selection Problem
 * 	Consider the following 6 activities. 
     start[]  =  {1, 3, 0, 5, 8, 5};
     finish[] =  {2, 4, 6, 7, 9, 9};
	The maximum set of activities that can be executed 
	by a single person is {0, 1, 3, 4}, here value represent indexes
 */
public class ActivitySelectionProblem {

	private static PrintUtility printUtility= new PrintUtility();
	
	public static void main(String[] args) throws Exception {
		ActivitySelectionProblem caller= new ActivitySelectionProblem();
		
		/*int[] meetStart1= {1,3,0,5,8,5};
		int[] meetEnd1=   {2,4,6,7,9,9};*/
		
		int[] meetStart1={1,1};
		int[] meetEnd1={2,4};
		//TODO
		int[] indexes=new int[meetStart1.length];
		for (int i = 0; i < indexes.length; i++) {
			indexes[i]=i;
		}
		
		int[] acceptedMeetings = caller.getAcceptedMeetings(meetStart1, meetEnd1, meetStart1.length, indexes);
		System.out.println("Accepted meetings : ");
		printUtility.printArray(acceptedMeetings);
	}
	
	private int[] getAcceptedMeetings(int[] meetStart, int[] meetEnd, int size, int[] indexes) throws Exception{
		
		if(meetStart==null|| meetEnd==null || meetStart.length!=meetEnd.length)
			throw new Exception("Invalid Schedule Lists");
		
		int[] acceptedMeetings= new int[size];
		int[] tempStart= new int[size];
		int[] tempEnd= new int[size];
		
		if(size==1)
		{		acceptedMeetings= new int[1];
				acceptedMeetings[0]=0;
				return acceptedMeetings;
		}
		
		tempStart[0]= meetStart[0];
		tempEnd[0]= meetEnd[0];
		int tempSize=1;
		
		for (int i = 1; i < size; i++) {
			//check for discrete meetings
			boolean isDiscrete=false;
			for (int j = 0; j < tempSize; j++) {
				if((tempStart[j]<meetStart[i] && tempEnd[j]<=meetStart[i])||
						tempStart[j]>=meetEnd[i] && tempEnd[j]>meetEnd[i]){
					isDiscrete=true;
				}else{
					isDiscrete=false;
					if(tempEnd[j]-tempStart[j]>meetEnd[i]-meetStart[i]){
						tempStart[j]= meetStart[i];
						tempEnd[j]=meetEnd[i];
						
						acceptedMeetings[j]=indexes[i];
					}
				}
			}
			if(isDiscrete){
				tempStart[tempSize]=meetStart[i];
				tempEnd[tempSize]= meetEnd[i];
				acceptedMeetings[tempSize]=indexes[i];
				tempSize++;
			}
		}
		
		System.out.println("Accepted Meetings after first iteration :");
		printUtility.printArray(acceptedMeetings);
		if(meetStart.length>tempSize)
			acceptedMeetings= getAcceptedMeetings(tempStart, tempEnd,tempSize, acceptedMeetings);
		return acceptedMeetings;
	}
}
