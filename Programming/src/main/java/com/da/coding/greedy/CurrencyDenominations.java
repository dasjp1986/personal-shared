package com.da.coding.greedy;

/**
 * 
 * @author Dharmendra
 * find all possible ways amount n can be withdrawn given an array of denominations	
 * Ex- denominations{1,2,5,10}
 * if Amount n= 5
 * [{1,1,1,1,1},{1,2,2},{1,1,1,2},{5}] then answer is 4
 */
public class CurrencyDenominations {
	public static void main(String[] args) {
		CurrencyDenominations caller= new CurrencyDenominations();
		int []denoms={1,2,5,10};
		System.out.println("Possible ways to fetch Amount : 4 "+caller.getPossibiilities(4, denoms));
	}
	
	private int getPossibiilities(int amount, int[] denoms){
		int[] ways= new int[amount+1];
		ways[0]=1;
		
		for (int coin : denoms) {
			for (int i = coin; i <= amount; i++) {
				int r=i-coin;
				ways[i]=ways[i]+ways[r];
			}
		}
		
		return ways[amount]; 
	}
	
}
