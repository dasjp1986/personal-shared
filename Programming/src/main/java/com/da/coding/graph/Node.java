package com.da.coding.graph;

import java.util.Set;

public class Node {
	private int label;

	public Node(int label) {
		this.label = label;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	
	@Override
	public boolean equals(Object obj) {
		Node node = (Node) obj;
		return this.getLabel() == node.getLabel();
	}

	@Override
	public String toString() {
		return "GraphNode [label=" + label +"]";
	}

}
