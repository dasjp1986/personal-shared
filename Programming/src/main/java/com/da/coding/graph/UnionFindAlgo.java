package com.da.coding.graph;

/**
 * 
 * @author Dharmendra 
 * 		   Union-Find Algorithm | Set 1 (Detect Cycle in an
 *         Undirected Graph) A disjoint-set data structure is a data structure
 *         that keeps track of a set of elements partitioned into a number of
 *         disjoint (non-overlapping) subsets. A union-find algorithm is an
 *         algorithm that performs two useful operations on such a data
 *         structure:
 * 
 *         Find: Determine which subset a particular element is in. This can be
 *         used for determining if two elements are in the same subset.
 * 
 *         Union: Join two subsets into a single subset.
 * 
 *         In this post, we will discuss an application of Disjoint Set Data
 *         Structure. The application is to check whether a given graph contains
 *         a cycle or not.
 * 
 *         Union-Find Algorithm can be used to check whether an undirected graph
 *         contains cycle or not. Note that we have discussed an algorithm to
 *         detect cycle. This is another method based on Union-Find. This method
 *         assumes that graph doesn’t contain any self-loops. We can keeps track
 *         of the subsets in a 1D array, lets call it parent[].
 */
public class UnionFindAlgo {
	GraphAdjacencyList graph;
	
	
	public UnionFindAlgo(GraphAdjacencyList graph) {
		this.graph= graph;
	}
	
	public int find(int v){
		if(graph.getParent()[v]==-1 || graph.getParent()[v]==v)
			return v;
		return find(graph.getParent()[v]);
	}
	
	public void subset(int v, int p){
		graph.setParent(v, p);
	}
	
	public boolean isCycle(){
		for (int i = 0; i < graph.getEdges().length; i++) {
			int subset1= find(i);
			int subset2=-1;
			for (Node n:graph.getEdges()[i]) {
				subset2= find(n.getLabel());
				if(subset1==subset2)
					return true;
				subset(n.getLabel(), i);
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		GraphAdjacencyList undiGraph1= new GraphAdjacencyList(3);
		Node n00= new Node(0);
		Node n11= new Node(1);
		Node n22= new Node(2);
		
		undiGraph1.addEdge(n00, n11);
		undiGraph1.addEdge(n00, n22);
		
		//undiGraph1.addEdge(n11, n22);
		
		UnionFindAlgo algo= new UnionFindAlgo(undiGraph1);
		System.out.println("find cycle in undirected graph1 : "+algo.isCycle());
	}
}
