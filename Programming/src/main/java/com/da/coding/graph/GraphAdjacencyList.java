package com.da.coding.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class GraphAdjacencyList {
	//an array of list
	private int vertices;
	private LinkedList<Node>[] edges;
	private Set<Node> visited;
	private int[] parent;
	
	public GraphAdjacencyList(int vertices) {
		this.vertices= vertices;
		edges= new LinkedList[vertices];
		parent= new int[vertices];
		for (int i = 0; i < vertices; i++) {
			edges[i]= new LinkedList<>();
			parent[i]=-1;
		}
		visited= new HashSet<>();
	}
	
	public int[] getParent(){
		return parent;
	}
	
	public void setParent(int v, int p){
		parent[v]= p;
	}
	
	public LinkedList<Node>[] getEdges(){
		return edges;
	}
	public void addEdge(Node v, Node e){
		edges[v.getLabel()].add(e);
	}
	
	//complexity Time= O(V+E) and Space=O(V)
	public void bfsTraversal(Node v){
		visited.clear();
		System.out.println("Running BFSTraversal...");
		
		Queue<Node> queue= new LinkedList<>(); 
		queue.add(v);
	
		while (!queue.isEmpty()) {
			Node temp= queue.poll();
			if(!visited.contains(temp)){
				System.out.println(temp.getLabel());
				visited.add(temp);
				if(temp.getLabel()<vertices)
					queue.addAll(edges[temp.getLabel()]);
			}
		}
	}
	
	//complexity Time= O(V+E) and Space=O(V)
	public void dfsTraversal(Node v){
		visited.clear();
		Stack<Node> stack= new Stack<>();
		System.out.println("Running DFSTraversal...");
		
		stack.push(v);
		while (!stack.isEmpty()) {
			Node temp= stack.pop();
			if(!visited.contains(temp))	{
				System.out.println(temp.getLabel());
				visited.add(temp);
				if(temp.getLabel()<vertices)
					edges[temp.getLabel()].forEach(n->stack.push(n));;
				
			}
		}
	}
	
	//find a cycle in directed graph
	//complexity Time= O(V+E) and Space=O(V)
	public boolean isCycleDirectedGraphWithXtraSpace(Node v){
		System.out.println("Finding a cycle in directed graph, returns true if so...");
		visited.clear();
		Stack<Node> stack= new Stack<>();
		stack.push(v);
		
		while(!stack.isEmpty()){
			Node temp= stack.pop();
			if(visited.contains(temp))
				return true;
			else{
				visited.add(temp);
				if(v.getLabel()<vertices)
					edges[temp.getLabel()].forEach(n->stack.push(n));
			}
		}
		return false;
	}
	
	//find a cycle in directed graph
	//complexity Time= O(V+E)
	public boolean isCycleDirectedGraphWithRecursion(Node v){
		boolean isCycle=false;
		if(visited.contains(v))
			return true;
		else{
			visited.add(v);
			if(v.getLabel()<vertices){
				for (Node n:edges[v.getLabel()]) {
					if(isCycleDirectedGraphWithRecursion(n)){
						isCycle= true;
						break;
					}
				}
			}
		}
		return isCycle;
	}
	
	public static void main(String[] args) {
		//undirected graphs
		GraphAdjacencyList undiGraph1= new GraphAdjacencyList(5);
		Node n0= new Node(0);
		Node n1= new Node(1);
		Node n2= new Node(2);
		Node n3= new Node(3);
		Node n4= new Node(4);
		
		undiGraph1.addEdge(n0, n1);
		undiGraph1.addEdge(n0, n2);
		
		undiGraph1.addEdge(n1, n0);
		undiGraph1.addEdge(n1, n3);
		
		undiGraph1.addEdge(n2, n0);
		undiGraph1.addEdge(n2, n4);
		
		undiGraph1.addEdge(n3, n1);
		undiGraph1.addEdge(n3, n4);
		
		undiGraph1.addEdge(n4, n2);
		undiGraph1.addEdge(n4, n3);
		
		undiGraph1.bfsTraversal(n0);
		undiGraph1.dfsTraversal(n0);
		
		//adding another undirected graph
		GraphAdjacencyList undiGraph2= new GraphAdjacencyList(3);
		Node n00= new Node(0);
		Node n11= new Node(1);
		Node n22= new Node(2);
		
		undiGraph2.addEdge(n00, n11);
		undiGraph2.addEdge(n00, n22);
		undiGraph2.addEdge(n00, n00);
		
		undiGraph2.addEdge(n11, n00);
		undiGraph2.addEdge(n11, n22);
		
		undiGraph2.addEdge(n22, n11);
		undiGraph2.addEdge(n22, n00);
		
		System.out.println("Traversing another graph");
		undiGraph2.bfsTraversal(n00);
		undiGraph2.dfsTraversal(n00);
		
		GraphAdjacencyList diGraph1= new GraphAdjacencyList(3);
		Node din0= new Node(0);
		Node din1= new Node(1);
		Node din2= new Node(2);
		
		diGraph1.addEdge(din0, din1);
		diGraph1.addEdge(din0, din2);
		diGraph1.addEdge(din1, din2);
		
		System.out.println(diGraph1.isCycleDirectedGraphWithXtraSpace(din0));
		
		diGraph1.visited.clear();
		System.out.println("Detecting cycle using recursion : "+diGraph1.isCycleDirectedGraphWithRecursion(din0));
	}
}
