package com.da.coding.graph;

public class GraphAdjacencyMatrix {
	private int[][] matrix;
	private int[] visited;

	public GraphAdjacencyMatrix(int vertices) {
		matrix = new int[vertices][vertices];
		visited = new int[vertices];
	}

	public void addEdge(int v, int e) {
		matrix[v][e] = 1;
	}

	public void removeEdge(int v, int e) {
		matrix[v][e] = 0;
	}

	public void visit(int v) {
		visited[v] = 1;
	}

	public boolean isVisited(int v) {
		return visited[v] == 1;
	}
	
	public void unVisitAll(){
		for (int i = 0; i < visited.length; i++) {
			visited[i]=0;
		}
	}

	public void bfsTraversal() {
		System.out.println("Running BFSTraversal...");
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == 1) {
					if (!isVisited(i)) {
						System.out.println(i);
						visit(i);
					}
					if (!isVisited(j)) {
						System.out.println(j);
						visit(j);
					}
				}
			}

		}
		unVisitAll();
	}

	public void DFSTraversal(int i) {
		if (i >=matrix.length)
			return;
		for (int j = 0; j < matrix[i].length; j++) {
			if (matrix[i][j] == 1) {
				if (!isVisited(i)) {
					System.out.println(i);
					visit(i);
				}
				if (!isVisited(j)) {
					System.out.println(j);
					visit(j);
					DFSTraversal(j);
				}
			}
		}

	}

	public static void main(String[] args) {
		GraphAdjacencyMatrix graph = new GraphAdjacencyMatrix(5);
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);

		graph.addEdge(1, 0);
		graph.addEdge(1, 3);

		graph.addEdge(2, 0);
		graph.addEdge(2, 4);

		graph.addEdge(3, 1);
		graph.addEdge(3, 4);

		graph.addEdge(4, 2);
		graph.addEdge(4, 3);

		graph.bfsTraversal();

		System.out.println("Running dfs traversal...");
		graph.DFSTraversal(0);
	}
}
