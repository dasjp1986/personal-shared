package com.da.coding.geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 * Given an array, print k largest elements from the array.  The output elements should be printed in decreasing order.

Input:

The first line of input contains an integer T denoting the number of test cases.
The first line of each test case is N and k, N is the size of array and K is the largest elements to be returned.
The second line of each test case contains N input C[i].

Output:

Print the k largest element in descending order.

Constraints:

1 ≤ T ≤ 100
1 ≤ N ≤ 100
K ≤ N
1 ≤ C[i] ≤ 1000

Example:

Input:
2
5 2
12 5 787 1 23
7 3
1 23 12 9 30 2 50

Output:
787 23
50 30 23
 */
public class KLargestElements {
	int[] heap;
	int heapSize;
	
	public KLargestElements(int[] input) {
		heapSize= input.length;
		heap= input;
		
		buildHeap();
		printHeap();
	}
	
	public void findKElements(int k){
		for (int i = 0; i < k; i++) {
			System.out.println(extractMax());
		}
	}

	private void buildHeap() {
		for (int i = 0; i <heapSize/2 ; i++) {
			maxHeapify(i);
		}
	}

	private void maxHeapify(int p) {
		int left, right;
		left= 2*p+1;
		right= 2*p+2;
		
		int max= heap[p];
		int index= p;
		if(left<heapSize && heap[left]>max){
			max= heap[left];
			index= left;
		}
		
		if(right<heapSize && heap[right]>max){
			max= heap[right];
			index= right;
		}
		if(index!=p){
			swap(p, index);
			maxHeapify(p);
		}
	}

	private void swap(int index, int i) {
		int temp= heap[i];
		heap[i]=heap[index];
		heap[index]= temp;
	}
	
	private void printHeap() {
		for (int i = 0; i <=heapSize / 2; i++) {
			System.out.println("node-" + heap[i]);
			if (2 * i + 1 < heapSize) {
				System.out.print(" left-" + heap[2 * i + 1]);
			}

			if (2 * i + 2 < heapSize) {
				System.out.print(" right-" + heap[2 * i + 2]);
			}
			System.out.println();
		}
	}
	
	private int extractMax(){
		if(heapSize<1)
			throw new RuntimeException("Heap is empty");
		
		int max= heap[0];
		swap(0, heapSize-1);
		heapSize--;
		maxHeapify(0);
		return max;
	}
	
	public static void main(String[] args) throws IOException {
		BufferedReader scn= new BufferedReader(new InputStreamReader(System.in));
		int t =Integer.parseInt(scn.readLine());
		for(int i=0;i<t;i++){
			String[] lines= scn.readLine().split(" ");
			int n= Integer.parseInt(lines[0]);
			int k= Integer.parseInt(lines[1]);
			String[] nums= scn.readLine().split(" ");
			int[] input= new int[n];
			for(int j=0;j<n;j++){
				input[j]= Integer.parseInt(nums[j]);
			}
			KLargestElements test= new KLargestElements(input);
			System.out.println(Arrays.toString(test.heap));
			test.findKElements(k);
			
		}
		
		scn.close();
//		int[] input={12,5,787,16,23,90};
//		KLargestElements test= new KLargestElements(input);
//		System.out.println(Arrays.toString(test.heap));
//		test.findKElements(4);
	}
}
