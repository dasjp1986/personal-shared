package com.da.coding.visa.assignment;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CarSchedulingTest {

	@Test
	public void test01ZeroWaitTime() {
		String[] timeStrings = { "00:01", "00:01", "00:02", "00:02", "00:03", "00:03" };
		Result result = CarScheduling.scheduleCars(6, 3, 2, timeStrings);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);
		System.out.println(result);
	}

	@Test
	public void test02ZeroWaitTime() {
		String[] timeStrings = { "00:01", "00:02", "00:02", "00:02", "00:03", "00:03" };
		Result result = CarScheduling.scheduleCars(6, 3, 3, timeStrings);
		System.out.println(result);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);

	}

	@Test
	public void test03ZeroWaitTime() {
		String[] timeStrings = { "01:01", "01:01", "02:02", "02:02", "03:03", "03:03" };
		Result result = CarScheduling.scheduleCars(6, 3, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);
		System.out.println(result);
	}

	@Test
	public void test04ZeroWaitTime() {
		String[] timeStrings = { "00:01", "00:02", "00:03" };
		Result result = CarScheduling.scheduleCars(3, 5, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);
		System.out.println(result);
	}

	@Test
	public void test05ZeroWaitTime() {
		String[] timeStrings = { "00:01", "00:01", "00:01", "00:01", "00:01", "00:01" };
		Result result = CarScheduling.scheduleCars(6, 3, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);
		System.out.println(result);
	}

	@Test
	public void test06ZeroWaitTime() {
		String[] timeStrings = { "00:01" };
		Result result = CarScheduling.scheduleCars(1, 3, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);
		System.out.println(result);
	}

	@Test
	public void test07ZeroWaitTime() {
		String[] timeStrings = { "00:01", "00:02" };
		Result result = CarScheduling.scheduleCars(2, 3, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 0);
		assertEquals(result.getLongestWaitTime(), 0);
		System.out.println(result);
	}

	@Test
	public void test09WaitTime1() {
		String[] timeStrings = { "00:01", "00:02", "00:02", "00:02", "00:02", "00:03" };
		Result result = CarScheduling.scheduleCars(6, 3, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 1);
		assertEquals(result.getLongestWaitTime(), 1);
		System.out.println(result);
	}

	@Test
	public void test10WaitTime2() {
		String[] timeStrings = { "00:01", "00:02", "00:02", "00:02", "00:02", "00:03", "00:03", "00:04", "00:04",
				"00:04" };
		Result result = CarScheduling.scheduleCars(10, 3, 4, timeStrings);
		assertEquals(result.getTotalWaitTime(), 2);
		assertEquals(result.getLongestWaitTime(), 1);
		System.out.println(result);
	}

	@Test
	public void test11WaitTimeExactCapacity() {
		String[] timeStrings = { "00:01", "00:02", "00:03", "00:04", "00:05", "00:06" };
		Result result = CarScheduling.scheduleCars(6, 3, 2, timeStrings);
		assertEquals(result.getTotalWaitTime(), 3);
		assertEquals(result.getLongestWaitTime(), 1);
		System.out.println(result);
	}

	@Test
	public void test12WaitTime180() {
		String[] timeStrings = { "01:01", "02:01", "03:01", "04:01", "05:01", "06:01" };
		Result result = CarScheduling.scheduleCars(6, 3, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 180);
		assertEquals(result.getLongestWaitTime(), 60);
		System.out.println(result);
	}

	@Test
	public void test13WaitTime30() {
		String[] timeStrings = { "01:00", "01:30", "01:45", "02:45", "03:00" };
		Result result = CarScheduling.scheduleCars(5, 3, 2, timeStrings);
		assertEquals(result.getTotalWaitTime(), 30);
		assertEquals(result.getLongestWaitTime(), 15);
		System.out.println(result);
	}

	@Test
	public void test14WaitTime30() {
		String[] timeStrings = { "00:01", "00:2", "00:04", "00:06", "00:08" };
		Result result = CarScheduling.scheduleCars(5, 2, 3, timeStrings);
		assertEquals(result.getTotalWaitTime(), 7);
		assertEquals(result.getLongestWaitTime(), 3);
		System.out.println(result);
	}

	@Test(expected = RuntimeException.class)
	public void test13InsufficientCars() {
		String[] timeStrings = { "01:01", "02:01", "03:01", "04:01", "05:01", "06:01" };
		Result result = CarScheduling.scheduleCars(6, 2, 2, timeStrings);
	}

}
