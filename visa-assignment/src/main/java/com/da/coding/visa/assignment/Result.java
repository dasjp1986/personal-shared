package com.da.coding.visa.assignment;

import java.util.Arrays;

public class Result {

	private String[][] schedulePoints;
	private int[] totalWaitTimesPerSchedule;
	private int[] longestWaitTimesPerSchedule;
	private int totalWaitTime;
	private int longestWaitTime;

	public String[][] getSchedulePoints() {
		return schedulePoints;
	}

	public void setSchedulePoints(String[][] schedulePoints) {
		this.schedulePoints = schedulePoints;
	}

	public int[] getTotalWaitTimesPerSchedule() {
		return totalWaitTimesPerSchedule;
	}

	public void setTotalWaitTimesPerSchedule(int[] totalWaitTimesPerSchedule) {
		this.totalWaitTimesPerSchedule = totalWaitTimesPerSchedule;
	}

	public int[] getLongestWaitTimesPerSchedule() {
		return longestWaitTimesPerSchedule;
	}

	public void setLongestWaitTimesPerSchedule(int[] longestWaitTimesPerSchedule) {
		this.longestWaitTimesPerSchedule = longestWaitTimesPerSchedule;
	}

	public int getTotalWaitTime() {
		return totalWaitTime;
	}

	public void setTotalWaitTime(int totalWaitTime) {
		this.totalWaitTime = totalWaitTime;
	}

	public int getLongestWaitTime() {
		return longestWaitTime;
	}

	public void setLongestWaitTime(int longestWaitTime) {
		this.longestWaitTime = longestWaitTime;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < schedulePoints.length; i++) {
			stringBuilder.append("schedule : " + i + Arrays.toString(schedulePoints[i]));
		}
		String totalWaitTimeStr = String.format("%02d", totalWaitTime / 60) + "HH:"
				+ String.format("%02d", totalWaitTime % 60) + "MM";
		String longestWaitTimeStr = String.format("%02d", longestWaitTime / 60) + "HH:"
				+ String.format("%02d", longestWaitTime % 60) + "MM";

		return "Result [schedulePoints=" + stringBuilder.toString() + ", waitTimesPerSchedule="
				+ Arrays.toString(totalWaitTimesPerSchedule) + ", totalWaitTime=" + totalWaitTimeStr
				+ ", longestTimesPerSchedule=" + Arrays.toString(longestWaitTimesPerSchedule) + ", longestWaitTime="
				+ longestWaitTimeStr + "]";
	}

}
