package com.da.coding.visa.assignment;

import java.util.Arrays;
import java.util.Scanner;

/**
 * You are assigned to schedule cars for a group of student to travel from point
 * A to point B for a day. There are total N students, M cars, and each car can
 * hold up to C students. You know the arrival time of each student at Point A
 * ahead of time. Each car travels one way from A to B, and does NOT return.
 * Assume that M*C >= N, so that you can transfer all the students to the
 * destination. Also assume that all cars are ready for boarding at Point A, and
 * the boarding time for the students can be ignored.
 * 
 * 
 * 
 * Input: N, M, C (all integers, M*C>=N)
 * 
 * Student Arrival Time: t1, t2, …, tn (timestamps in the format hh:mm, hh
 * integer in between 00 and 23, mm integer in between 00 and 59)
 * 
 * 
 * 
 * Questions:
 * 
 * (1) How will you schedule the cars such that the longest wait time for any
 * student is minimal
 * 
 * (2) How will you schedule the cars such that the total wait time for all the
 * students is minimal
 * 
 * 
 * 
 * Please code in Java or C++, analyze the complexity in O(n) term, and design
 * test cases to verify the correctness of your program.
 *
 */
public class CarScheduling {

	// Please use CarSchedulingTest.java for running any test cases.

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		int m = scn.nextInt();
		int c = scn.nextInt();

		String[] timeInMins = new String[n];
		for (int i = 0; i < n; i++) {
			timeInMins[i] = scn.next();

		}

		Result result = scheduleCars(n, m, c, timeInMins);
		System.out.println(result);
		scn.close();

	}

	/**
	 * 
	 * @param studentsCount
	 * @param carsCount
	 * @param capacity
	 * @param timeStrings
	 * @return
	 */

	// n= studentsCount, m=carsCount, c=capacity
	// Time complexity- O(nlogn+n+m*c+n) = O(nlogn+m*c)
	public static Result scheduleCars(int studentsCount, int carsCount, int capacity, String[] timeStrings) {

		int[] arrivalTimes = parseToMins(timeStrings);
		// Time complexity O(nlogn)
		Arrays.sort(arrivalTimes);

		if (studentsCount > capacity * carsCount) {
			throw new RuntimeException("insufficient car capacity");
		}

		// declare car start schedule points and wait time
		carsCount = Math.min(carsCount, studentsCount);
		int[] schedulePoints = new int[carsCount + 1];
		int[] totalWaitTimesPerSchedule = new int[carsCount];
		int[] longestWaitTimesPerSchedule = new int[carsCount];
		int totalWaitTime = 0;

		Result result = new Result();

		// arrange all the student arrivals in groups-per car capacity from left to
		// right
		// Time complexity- O(n)
		totalWaitTime = divideIngroupsByCars(studentsCount, carsCount, capacity, arrivalTimes, schedulePoints,
				totalWaitTimesPerSchedule, longestWaitTimesPerSchedule);

		// if student count is same as total capacity, solution is already created
		if (studentsCount != carsCount * capacity) {
			// re arrange arrivals to get minimum total wait time and minimizing longest
			// wait time too
			// Time complexity- O(m*c)
			totalWaitTime = getMinTotalWaitTime(schedulePoints, totalWaitTimesPerSchedule, arrivalTimes, capacity,
					totalWaitTime, longestWaitTimesPerSchedule);
		}

		// mapping scheduler start points for readability purpose
		// Time complexity- O(n)
		String[][] schedulePointsTimes = mapSchedulePoints(schedulePoints, arrivalTimes);
		result.setSchedulePoints(schedulePointsTimes);
		result.setTotalWaitTimesPerSchedule(totalWaitTimesPerSchedule);
		result.setTotalWaitTime(totalWaitTime);
		result.setLongestWaitTimesPerSchedule(longestWaitTimesPerSchedule);
		result.setLongestWaitTime(getMaxValue(longestWaitTimesPerSchedule));

		return result;
	}
	
	// take all student arrivals in place them in arrival bucket fully upto capacity
	private static int divideIngroupsByCars(int studentsCount, int carsCount, int capacity, int[] arrivalTimes,
			int[] schedulePoints, int[] totalWaitTimesPerSchedule, int[] longestWaitTimesPerSchedule) {
		int j = 0;
		int totalWaitTime = 0;
		for (int i = 0; i < totalWaitTimesPerSchedule.length; i++) {
			schedulePoints[i] = j;
			int n = j + capacity;
			while (j < studentsCount && j < n) {
				j++;
			}
			totalWaitTimesPerSchedule[i] = getWaitTime(schedulePoints[i], j, arrivalTimes);
			longestWaitTimesPerSchedule[i] = getLongestWaitTime(schedulePoints[i], j, arrivalTimes);
			totalWaitTime = totalWaitTime + totalWaitTimesPerSchedule[i];
		}

		schedulePoints[carsCount] = studentsCount;
		return totalWaitTime;
	}
	
	// Time complexity- O(m*c)
	private static int getMinTotalWaitTime(int[] schedulePoints, int[] waitTimes, int[] arrivalTimes, int capacity,
			int totalWaitTime, int[] longestWaitTimesPerSchedule) {

		// re-arranging from end to start, if possible to accommodate any student
		// without
		// increasing total wait time
		for (int i = schedulePoints.length - 2; i > 0; i--) {
			while (schedulePoints[i + 1] - schedulePoints[i] < capacity) {
				int prevWaitTime = getWaitTime(schedulePoints[i - 1], schedulePoints[i] - 1, arrivalTimes);
				int currentWaitTime = getWaitTime(schedulePoints[i] - 1, schedulePoints[i + 1], arrivalTimes);

				int prevLongestWaitTime = Math.max(
						getLongestWaitTime(schedulePoints[i - 1], schedulePoints[i], arrivalTimes),
						getLongestWaitTime(schedulePoints[i], schedulePoints[i + 1], arrivalTimes));
				int newLongestTimeForPrev = getLongestWaitTime(schedulePoints[i - 1], schedulePoints[i] - 1,
						arrivalTimes);
				int newLongestTimeForCurrent = getLongestWaitTime(schedulePoints[i] - 1, schedulePoints[i + 1],
						arrivalTimes);
				int currentLongestWaitTime = Math.max(newLongestTimeForPrev, newLongestTimeForCurrent);

				int newTotalWaitTime = totalWaitTime - waitTimes[i - 1] - waitTimes[i] + prevWaitTime + currentWaitTime;

				if (newTotalWaitTime > totalWaitTime
						|| (newTotalWaitTime == totalWaitTime && currentLongestWaitTime > prevLongestWaitTime)) {
					break;
				}
				// || (newTotalWaitTime == totalWaitTime && schedulePoints[i + 1] -
				// schedulePoints[i] < capacity)
				schedulePoints[i] = schedulePoints[i] - 1;
				waitTimes[i - 1] = prevWaitTime;
				waitTimes[i] = currentWaitTime;
				longestWaitTimesPerSchedule[i - 1] = newLongestTimeForPrev;
				longestWaitTimesPerSchedule[i] = newLongestTimeForCurrent;
				totalWaitTime = newTotalWaitTime;
			}
		}

		return totalWaitTime;
	}

	private static int getMaxValue(int[] values) {

		int max = 0;
		for (int value : values) {
			max = Math.max(value, max);
		}
		return max;
	}



	// mapping schedule start points to hh:mm format from minutes
	private static String[][] mapSchedulePoints(int[] schedulePoints, int[] arrivalTimes) {
		String[][] schedulePointsTimes = new String[schedulePoints.length - 1][];

		for (int i = 0; i < schedulePointsTimes.length; i++) {
			String[] points = new String[schedulePoints[i + 1] - schedulePoints[i]];
			for (int j = 0; j < points.length; j++) {
				int time = arrivalTimes[schedulePoints[i] + j];
				int hour = time / 60;
				int mins = time % 60;
				points[j] = String.format("%02d", hour) + ":" + String.format("%02d", mins);
			}
			schedulePointsTimes[i] = points;
		}
		return schedulePointsTimes;
	}

	private static int[] parseToMins(String[] timeInMins) {
		int[] arrivalTimes = new int[timeInMins.length];
		for (int i = 0; i < timeInMins.length; i++) {
			String times[] = timeInMins[i].split(":");
			arrivalTimes[i] = Integer.parseInt(times[0]) * 60 + Integer.parseInt(times[1]);

		}
		return arrivalTimes;
	}



	private static int getLongestWaitTime(int i, int j, int[] arrivalTimes) {
		int minTime = Integer.MAX_VALUE, maxTime = Integer.MIN_VALUE;
		for (int k = i; k < j; k++) {
			int time = arrivalTimes[k];
			if (time < minTime) {
				minTime = time;
			}
			if (time > maxTime) {
				maxTime = time;
			}
		}
		return maxTime - minTime;
	}

	private static int getWaitTime(int i, int j, int[] arrivalTimes) {
		int total = 0;
		if (i >= j || i < 0 || j < 0) {
			return total;
		}

		int max = arrivalTimes[j - 1];
		while (i < j) {
			total += max - arrivalTimes[i];
			i++;
		}
		return total;
	}

}
