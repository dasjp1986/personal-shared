package com.da.coding.spring.social;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacebookSocialStarter 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(FacebookSocialStarter.class, args);
    }
}
