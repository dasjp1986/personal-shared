package com.da.coding.lembda.inbuilt.functions;

import java.util.Comparator;

import com.da.coding.lembda.examples.Car;

public class Comparators {
	public static void main(String[] args) {
		Comparator<Car> comparator=(c1,c2)->c1.getModel().compareTo(c2.getModel());
		Car car1= new Car("BMW-X1", 2015);
		Car car2= new Car("Audi-Q7", 2014);
		
		System.out.println(comparator.compare(car1, car2));
		System.out.println(comparator.reversed().compare(car1, car2));
	}
}
