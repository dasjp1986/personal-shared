package com.da.coding.lembda.examples;

import java.util.Arrays;
import java.util.List;

public class ConverterCaller {
	public static void main(String[] args) {
		//Converter<String, Integer> converter= (from)->Integer.valueOf(from);
		Converter<String, Integer> converter=Integer::valueOf; //:: static method references
		System.out.println(converter.convert("100"));
		
		Something something= new Something();
		Converter<String, String> strConverter= something::startsWith; //:: object method references
		System.out.println(strConverter.convert("Dharmendra"));
		
		List<Integer> list= Arrays.asList(2,5,2,1,3);
		//list.forEach(a->System.out.println(autoConvert(a)));            you can not call default method from lamdba
	}
}
