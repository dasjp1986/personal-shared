package com.da.coding.lembda.examples;

public class CarFactoryCaller {
	public static void main(String[] args) {
		CarFactory<Car> carFactory= Car::new; //:: constructor reference
		Car car= carFactory.create("Nexa-Baleno", 2016);
		System.out.println(car);
	}
	
}
