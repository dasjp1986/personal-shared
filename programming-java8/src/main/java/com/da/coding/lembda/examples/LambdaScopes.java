package com.da.coding.lembda.examples;

import java.util.Arrays;
import java.util.List;

public class LambdaScopes {
	int sum;
	
	void findSum(List<Integer> list){
		list.forEach(num->{
			System.out.println(num);
			sum+=num;   //instance variable is available in  lambda 
			}
		);
	}
	
	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(1,2,3,4,5);

		/*final int sum=0;
		list.forEach(num->{
			System.out.println(num);
			sum+=num;
			}
		);*/
		
		LambdaScopes scopes= new LambdaScopes();
		scopes.findSum(list);
		System.out.println(scopes.sum);
		
		int max= 1000;
		
		list.forEach(num->{
			System.out.println(num);
			System.out.println("max :"+max);    //non final variable max is available as it is implicitly final
			//max=10;  Local variable max defined in an enclosing scope must be final or effectively final
		});
	}
}
