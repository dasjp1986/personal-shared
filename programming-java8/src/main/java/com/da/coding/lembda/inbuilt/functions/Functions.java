package com.da.coding.lembda.inbuilt.functions;

import java.util.function.Function;

public class Functions {
	public static void main(String[] args) {
		Function<String, Integer> toInteger= Integer::valueOf;
		Integer intVal= toInteger.apply("123");
		System.out.println(intVal);
		
		Function<String, Object> toDouble= toInteger.andThen(Double::valueOf);
		Object doubleVal = toDouble.apply("456");
		System.out.println(doubleVal);
		
		Function<Integer, Integer> sqrFunction= a->a*a;
		System.out.println(sqrFunction.apply(4));
	}
}
