package com.da.coding.lembda.examples;

public interface CarFactory<C extends Car> {
	C create(String model, int year);
}
