package com.da.coding.lembda.inbuilt.functions;

import java.util.function.Supplier;

import com.da.coding.lembda.examples.Car;

public class Suppliers {
	public static void main(String[] args) {
		Supplier<Car> supplier= Car::new;
		Car car = supplier.get();
		System.out.println(car);
	}
}
