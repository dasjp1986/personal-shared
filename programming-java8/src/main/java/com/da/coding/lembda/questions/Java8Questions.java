package com.da.coding.lembda.questions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class Java8Questions {
	public static void main(String[] args) {
		//How will you print 10 random numbers using forEach of java 8?
		System.out.println("print 10 random numbers using forEach");
		Random ran= new Random();
		ran.ints().limit(10).forEach(r->System.out.println(r));
		//using method references
		ran.ints().limit(10).forEach(System.out::println);
		
		//How will you print unique squares of numbers in java 8?
		System.out.println("print unique squares of numbers");
		List<Integer> numbers= Arrays.asList(1,2,3,2,3);
		numbers.stream().distinct().map(num->num*num).forEach(System.out::println);
		
		//How will you print count of empty strings in java 8?
		System.out.println("print count of empty strings");
		List<String> strs= Arrays.asList("abc", "", "", "bcd");
		long count = strs.parallelStream().filter(str->str.isEmpty()).count();
		System.out.println(count);
		//collect all no empty strings
		List<String> collect = strs.parallelStream().filter(str->!str.isEmpty()).collect(Collectors.toList());
		System.out.println(collect);

		//find highest number in the list
		Optional<Integer> max = numbers.stream().max((a,b)->a.compareTo(b));
		System.out.println("printing max :");
		max.ifPresent(System.out::println);
		
		//find sum of all numbers
		IntSummaryStatistics statistics = numbers.parallelStream().mapToInt((num)->num).summaryStatistics();
		System.out.println("sum : "+statistics.getSum());
		System.out.println("max: "+statistics.getMax());
		
		//get current time with local date without time zone
		System.out.println("Date Today : "+LocalDate.now());
		System.out.println("Date Time now: "+LocalDateTime.now());
	}
}
