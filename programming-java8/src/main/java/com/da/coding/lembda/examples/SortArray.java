package com.da.coding.lembda.examples;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortArray {
	public static void main(String[] args) {
		List<String> list= Arrays.asList("zzz","ddd","ppp", "bbb");
		
		//sorting in older way
		Collections.sort(list);
		System.out.println("sorted in natural order :"+list);
		
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String a, String b) {
				return b.compareTo(a);
			}
		});
		
		System.out.println("sorted in reverse order : "+list);
		
		//sorting with lambda way
		Collections.sort(list, (a,b)-> a.compareTo(b));
		System.out.println("sorted in natural order :"+list);
	}
}
