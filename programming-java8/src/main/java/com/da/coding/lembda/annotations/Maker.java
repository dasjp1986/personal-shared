package com.da.coding.lembda.annotations;

import java.lang.annotation.Repeatable;

@Repeatable(value = Makers.class)
public @interface Maker {
	String value();
}
