package com.da.coding.lembda.inbuilt.functions;

import java.util.function.Predicate;

public class Predicates {
	public static void main(String[] args) {
		Predicate<String> predicate= s->s.startsWith("D");
		System.out.println(predicate.test("Dharmendra"));
		
		System.out.println(predicate.negate().test("Dharmendra"));
	}
}
