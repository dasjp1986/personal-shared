package com.da.coding.lembda.examples;

import java.util.HashMap;
import java.util.Map;

public class MapOperations {
	public static void main(String[] args) {
		Map<Integer, String> map= new HashMap<>();
		for (int i = 0; i < 10; i++) {
			map.put(i, "value-"+i);
		}
		
		map.forEach((k,v)->System.out.println(v));
		map.computeIfAbsent(11, key->"value-"+key);
		map.forEach((k,v)->System.out.println(v));
		
		map.computeIfPresent(4, (key,value)->"replaced-value-"+key);
		map.forEach((k,v)->System.out.println(v));

		//delete an entry only if it has passed value against it
		map.remove(11, "11");
		System.out.println("not removed :"+map.get(11));
		
		map.remove(11,"value-11");
		System.out.println("removed "+map.get(11));
		
		//get or default value
		System.out.println(map.getOrDefault(40, "not-found"));
		
		//merge the value
		map.merge(9, "existing", (value,newValue)->value.concat(newValue));
		System.out.println(map.get(9));
	}
}
