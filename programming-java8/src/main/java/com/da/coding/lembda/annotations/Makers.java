package com.da.coding.lembda.annotations;

public @interface Makers {
	Maker[] value();
}
