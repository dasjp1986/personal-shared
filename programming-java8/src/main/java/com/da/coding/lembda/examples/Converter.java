package com.da.coding.lembda.examples;

@FunctionalInterface
public interface Converter<F,T> {
	T convert(F fTemp);
	default double autoConvert(double fTemp){
		return fTemp/2;
	}
}
