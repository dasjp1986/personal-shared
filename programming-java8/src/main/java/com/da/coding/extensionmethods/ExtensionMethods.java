package com.da.coding.extensionmethods;

public interface ExtensionMethods {
	
	default String getInfo(){
		return "This is an extension method";
	}
	
	default String getMoreInfo(){
		return "This is an additional extension method";
	}
}
