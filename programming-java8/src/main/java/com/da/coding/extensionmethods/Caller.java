package com.da.coding.extensionmethods;

public class Caller implements ExtensionMethods{
	public static void main(String[] args) {
		ExtensionMethods caller= new Caller();
		System.out.println(caller.getInfo());
		System.out.println(caller.getMoreInfo());
	}
}
