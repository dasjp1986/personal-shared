package com.da.coding.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ParellelAndSequentialStreams {
	public static void main(String[] args) {
		List<String> strList= new ArrayList<>(10);
		for (int i = 0; i < 10; i++) {
			strList.add(UUID.randomUUID().toString());
		}
		
		System.out.println("Original Strings : "+strList);
		long startTime1= System.nanoTime();
		strList.stream().sorted((s1,s2)->s2.compareTo(s1));
		long endTime1= System.nanoTime();
		System.out.println("Single Stream Sorting Time "+(endTime1-startTime1));
		
		System.out.println("Original Strings : "+strList);
		long startTime2= System.nanoTime();
		strList.parallelStream().sorted((s1,s2)->s2.compareTo(s1));
		long endTime2= System.nanoTime();
		System.out.println("Parellel Stream Sorting Time "+(endTime2-startTime2));
		
	}
}
