package com.da.coding.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

//Streams are not applicable on Maps
public class CollectionStreams {
	public static void main(String[] args) {
		List<String> strList= Arrays.asList("cde","def","abc","bcd");
		strList
			.stream()
				.filter(s->s.contains("c"))
				.sorted((s1,s2)->s2.compareTo(s1))   //only view is sorted not the actual collection
				.map(String::toUpperCase)			//map is an intermediate operation	
				.forEach(System.out::println);
		
		//terminal operation match checks condition in streams and returns boolean end
		boolean anyMatch = strList
			.stream()
			.anyMatch(s->s.startsWith("d"));
		
		boolean allMatch = strList
			.stream()
			.allMatch(s->s.contains("c"));
		
		boolean noneMatch = strList
			.stream()
			.noneMatch(s->s.contains("z"));
		
		System.out.println(anyMatch+" : "+allMatch+" : "+noneMatch);
		
		//terminal operation count returns no. of counts in the stream
		
		long totalCount = strList.stream().count();
		long cCount= strList.stream().filter(s->s.contains("c")).count();
		
		System.out.println(totalCount+" : "+cCount);
		
		//terminal operation reduced returns and optional after applying the given function
		Optional<String> optional= strList.stream().reduce((s1,s2)->s1+" : "+s2);
		optional.ifPresent(System.out::println);
	}
}
