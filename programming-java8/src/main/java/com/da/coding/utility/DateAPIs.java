package com.da.coding.utility;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateAPIs {
	public static void main(String[] args) {
		Clock clock= Clock.systemDefaultZone();
		Instant instant= clock.instant();
		Date date= Date.from(instant);
		
		//Time Zones
		System.out.println(ZoneId.getAvailableZoneIds());
		ZoneId zoneIndia = ZoneId.of("Indian/Comoro");
		ZoneId america= ZoneId.of("America/Marigot");
		
		System.out.println(zoneIndia.getRules());
		
		//local time
		LocalTime indiaTime= LocalTime.now(zoneIndia);
		LocalTime americaTime= LocalTime.now(america);
		
		System.out.println(indiaTime+" ----- "+americaTime);
		System.out.println(americaTime.isBefore(indiaTime));
		long hours= ChronoUnit.HOURS.between(indiaTime, americaTime);
		long minutes= ChronoUnit.MINUTES.between(indiaTime, americaTime);
		
		System.out.println("hours : "+hours+" minutes : "+minutes);
		
	}
}
