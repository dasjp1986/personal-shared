package com.da.coding.utility;

import java.util.Optional;

import com.da.coding.lembda.examples.Car;

public class Optionals {
	public static void main(String[] args) {
		Optional<Car> optional= Optional.of(new Car("BMW-X1", 2015));
		System.out.println(optional.isPresent());
		optional.ifPresent(car->System.out.println(car));

		
		optional= Optional.ofNullable(null);
		optional.orElseThrow(IllegalArgumentException::new);
	}
}
