package com.da.coding.structural.facade;

public class ShippingReportGenerator {
	private ShippingDao shpDao;
	
	public ShippingReportGenerator(ShippingDao shpDao) {
		 this.shpDao= shpDao;
	}
	
	public String generateReport(String orderId){
		return shpDao.fetch(orderId);
	}
}
