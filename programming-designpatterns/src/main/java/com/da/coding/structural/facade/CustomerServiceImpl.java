package com.da.coding.structural.facade;

public class CustomerServiceImpl implements CustomerService {

	private ReceivingReportGenerator rcvReportGenerator;
	private ShippingReportGenerator shpReportGenerator;

	public CustomerServiceImpl() {
		rcvReportGenerator = new ReceivingReportGenerator(new ReceivingDao());
		shpReportGenerator = new ShippingReportGenerator(new ShippingDao());
	}

	@Override
	public String generateReport(String orderId, OrderStatus status) {
		String result;
		switch (status) {
		case SHIPPED:
			result= shpReportGenerator.generateReport(orderId);	
			break;
			
		case RECEIVED:
			result= rcvReportGenerator.generateReport(orderId);
			break;
		default:
			throw new RuntimeException("Invalid Order Status");
		}
		return result;
	}

}
