package com.da.coding.structural.facade;

import java.util.Date;

public class ShippingDao {
	public String fetch(String orderID){
		return "Order :"+orderID+" was shipped on "+new Date().toLocaleString();
	}
}
