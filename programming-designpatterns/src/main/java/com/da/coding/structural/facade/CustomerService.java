package com.da.coding.structural.facade;

public interface CustomerService {
	String generateReport(String orderId, OrderStatus status);
}
