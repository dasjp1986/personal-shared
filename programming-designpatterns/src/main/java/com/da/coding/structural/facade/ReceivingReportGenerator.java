package com.da.coding.structural.facade;

public class ReceivingReportGenerator {
	
	private ReceivingDao rcvDao;
	
	public ReceivingReportGenerator(ReceivingDao rcvDao) {
		this.rcvDao= rcvDao;
	}
	public String generateReport(String orderId){
		return rcvDao.fetch(orderId);
	}
}
