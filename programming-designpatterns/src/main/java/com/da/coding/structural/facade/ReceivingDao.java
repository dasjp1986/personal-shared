package com.da.coding.structural.facade;

import java.util.Date;

public class ReceivingDao {
	public String fetch(String orderID){
		return "Order :"+orderID+" was received on "+new Date().toLocaleString();
	}
}
