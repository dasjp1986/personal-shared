package com.da.coding.structural.facade;

public enum OrderStatus {
	RECEIVED, SHIPPED
}
