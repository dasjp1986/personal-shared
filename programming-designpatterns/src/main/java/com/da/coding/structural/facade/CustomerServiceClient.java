package com.da.coding.structural.facade;

public class CustomerServiceClient {
	CustomerService customerService;
	
	public CustomerServiceClient() {
		customerService= new CustomerServiceImpl();
	}
	
	public void accessReport(){
		System.out.println(customerService.generateReport("O111", OrderStatus.RECEIVED));
		System.out.println(customerService.generateReport("O1222", OrderStatus.SHIPPED));
	}
	
	public static void main(String[] args) {
		CustomerServiceClient client= new CustomerServiceClient();
		client.accessReport();
	}
}	
