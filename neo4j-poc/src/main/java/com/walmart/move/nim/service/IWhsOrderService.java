package com.walmart.move.nim.service;

import java.util.Set;

import com.walmart.move.nim.domain.WhsOrder;

public interface IWhsOrderService {

	WhsOrder create(WhsOrder whsOrder);

	Iterable<WhsOrder> getAll();

	WhsOrder get(String orderId);

	void createBulk(Set<WhsOrder> whsOrders);

	void statusAggregation(String whsOrderId);

}