package com.walmart.move.nim.loadtesting;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.walmart.move.nim.common.Constants;
import com.walmart.move.nim.domain.Container;
import com.walmart.move.nim.domain.EnrichOrder;
import com.walmart.move.nim.domain.Pick;
import com.walmart.move.nim.domain.WhsOrder;

public class UploadData {

	private static final int ORDERS_COUNT = 1;
	private static final int ENRICH_COUNT = 2;
	private static final int PICK_COUNT = 2;
	private static final int CONTAINER_COUNT = 5;
	private static int lastId = 1;

	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i < 5; i++) {
			executeUploads();
		}
		Thread.sleep(10000);

	}

	private static void executeUploads() {
		Set<WhsOrder> whsOrders = getWhsOrders();
		HttpEntity<Set<WhsOrder>> ordersEntity = new HttpEntity<Set<WhsOrder>>(whsOrders);

		Set<EnrichOrder> enrichOrders = getEnrichOrders(whsOrders);
		HttpEntity<Set<EnrichOrder>> enrichOrdersEntity = new HttpEntity<Set<EnrichOrder>>(enrichOrders);

		Set<Pick> picks = getPicks(enrichOrders);
		HttpEntity<Set<Pick>> picksEntity = new HttpEntity<>(picks);

		Set<Container> containers = getContainers();
		HttpEntity<Set<Container>> containersEntity = new HttpEntity<>(containers);

		RestTemplate restTemplate = new RestTemplate();

		new Thread(new Runnable() {

			@Override
			public void run() {
				restTemplate.exchange("http://localhost:8080/orders/createBulk", HttpMethod.POST, ordersEntity,
						String.class);
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				restTemplate.exchange("http://localhost:8080/enrichOrders/createBulk", HttpMethod.POST,
						enrichOrdersEntity, String.class);
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				restTemplate.exchange("http://localhost:8080/picks/createBulk", HttpMethod.POST, picksEntity,
						String.class);

			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				restTemplate.exchange("http://localhost:8080/containers/createBulk", HttpMethod.POST, containersEntity,
						String.class);
			}
		}).start();
	}

	private static Set<Container> getContainers() {
		Set<Container> containers = new HashSet<>();
		for (int i = 1; i <= CONTAINER_COUNT; i++) {
			Container container = new Container();
			container.setContainerId("c" + i);
			container.setLoadNbr(i);
			container.setStatus(Constants.STATUS.LOADED.getStatusCode());
			container.setTrailerNbr(i);

			containers.add(container);
		}
		return containers;
	}

	private static Set<Pick> getPicks(Set<EnrichOrder> enrichOrders) {
		Set<Pick> picks = new HashSet<>();
		Random random = new Random();
		for (EnrichOrder enrichOrder : enrichOrders) {
			for (int i = 1; i <= PICK_COUNT; i++) {
				Pick pick = new Pick();
				pick.setContainerId("c" + (random.nextInt(CONTAINER_COUNT) + 1));
				pick.setEnrichId(enrichOrder.getEnrichOrderId());
				pick.setItemNbr(i % 2 + 1);
				pick.setLoadNbr(i);
				pick.setOrderId(enrichOrder.getOrderId());
				pick.setPickId(enrichOrder.getEnrichOrderId() + "-p" + i);
				pick.setPickQty(enrichOrder.getEnrichQty() / PICK_COUNT);
				pick.setStatus(Constants.STATUS.PICKED.getStatusCode());

				picks.add(pick);
			}
		}
		return picks;
	}

	private static Set<EnrichOrder> getEnrichOrders(Set<WhsOrder> whsOrders) {
		Set<EnrichOrder> enrichOrders = new HashSet<>();
		for (WhsOrder whsOrder : whsOrders) {
			for (int i = 1; i <= ENRICH_COUNT; i++) {
				EnrichOrder enrichOrder = new EnrichOrder();
				enrichOrder.setOrderId(whsOrder.getOrderId());
				enrichOrder.setEnrichOrderId(whsOrder.getOrderId() + "-e" + i);
				enrichOrder.setEnrichQty(whsOrder.getOrderQty() / ENRICH_COUNT);
				enrichOrder.setDeliveryNbr(i % 2 + 1);
				enrichOrder.setStatus(Constants.STATUS.ENRICHED.getStatusCode());
				enrichOrder.setSubCenter(i % 2 + 1);

				enrichOrders.add(enrichOrder);
			}
		}
		return enrichOrders;
	}

	private static Set<WhsOrder> getWhsOrders() {
		Set<WhsOrder> whsOrders = new HashSet<>();
		for (int i = 1; i <= ORDERS_COUNT; i++, lastId++) {
			WhsOrder whsOrder = new WhsOrder();
			whsOrder.setOrderId("o" + lastId);
			whsOrder.setItemNbr(1);
			whsOrder.setOrderQty(10 * i);
			whsOrder.setDestination("s-" + i);
			whsOrder.setOrderRecordType(1);
			whsOrder.setStatus(Constants.STATUS.NEW.getStatusCode());

			whsOrders.add(whsOrder);
		}
		return whsOrders;
	}
}
