package com.walmart.move.nim.service;

import java.util.Set;

import com.walmart.move.nim.domain.EnrichOrder;

public interface IEnrichOrderService {

	EnrichOrder create(EnrichOrder enrichOrder);

	void createAll(Set<EnrichOrder> enrichOrders);

	Iterable<EnrichOrder> getAll();

	public void statusAggregation(String enrichOrderId);

	EnrichOrder get(String enrichOrderId);

}