package com.walmart.move.nim.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.walmart.move.nim.common.Constants;
import com.walmart.move.nim.domain.Container;
import com.walmart.move.nim.domain.EnrichOrder;
import com.walmart.move.nim.domain.Pick;
import com.walmart.move.nim.repository.ContainerRepository;

@Service
public class ContainerService implements IContainerService {
	@Autowired
	private ContainerRepository containerRepository;

	@Autowired
	private IEnrichOrderService enrichOrderService;

	public Container create(Container container) {
		Optional<Container> containerOpt = containerRepository.findById(container.getContainerId());
		if (containerOpt.isPresent()) {
			Container containerExst = containerOpt.get();
			if (containerExst.getStatus() > container.getStatus()) {
				container.setStatus(containerExst.getStatus());
			}
			container.getPicks().addAll(containerExst.getPicks());
			Collection<Pick> picks = container.getPicks();
			if (!CollectionUtils.isEmpty(picks)) {
				Set<String> enrichOrderIds = new HashSet<>();
				for (Pick pick : picks) {
					if (container.getStatus() > pick.getStatus()) {
						pick.setStatus(container.getStatus());
						pick.setContainer(container);
					}
					enrichOrderIds.add(pick.getEnrichId());
				}
				container.setPicks(new HashSet<>(picks));
				// check enrich order status before calling status aggregation
				for (String enrichOrderId : enrichOrderIds) {
					EnrichOrder enrichOrder = enrichOrderService.get(enrichOrderId);
					if (enrichOrder != null && enrichOrder.getStatus() >= Constants.STATUS.PICKED.getStatusCode())
						enrichOrderService.statusAggregation(enrichOrderId);
				}
			}
		}
		return containerRepository.save(container);
	}

	@Override
	public Iterable<Container> getAll() {
		return containerRepository.findAll();
	}

	@Override
	public void createAll(Set<Container> containers) {
		for (Container container : containers) {
			this.create(container);
		}
	}
}
