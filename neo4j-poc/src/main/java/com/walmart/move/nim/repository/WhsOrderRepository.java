package com.walmart.move.nim.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.walmart.move.nim.domain.WhsOrder;

public interface WhsOrderRepository extends PagingAndSortingRepository<WhsOrder, String> {

	public WhsOrder findByItemNbr(@Param("itemNbr") Integer itemNbr);

	@Query("Match (w:WhsOrder) RETURN w")
	public Collection<WhsOrder> findAll();

}
