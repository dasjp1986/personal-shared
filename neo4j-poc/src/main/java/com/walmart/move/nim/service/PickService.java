package com.walmart.move.nim.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walmart.move.nim.domain.Container;
import com.walmart.move.nim.domain.EnrichOrder;
import com.walmart.move.nim.domain.Pick;
import com.walmart.move.nim.repository.ContainerRepository;
import com.walmart.move.nim.repository.EnrichOrderRepository;
import com.walmart.move.nim.repository.PickRepository;

@Service
public class PickService implements IPickService {
	@Autowired
	private PickRepository pickRepository;

	@Autowired
	private ContainerRepository containerRepository;

	@Autowired
	private EnrichOrderRepository enrichOrderRepository;

	@Autowired
	private IEnrichOrderService enrichOrderService;

	@Override
	public Pick create(Pick pick) {

		Optional<Pick> pickOpt = pickRepository.findById(pick.getPickId());
		if (pickOpt.isPresent()) {
			Pick pickExst = pickOpt.get();
			if (pickExst.getStatus() > pick.getStatus()) {
				pick.setStatus(pickExst.getStatus());
			}
			pick.setContainer(pickExst.getContainer());
			pick.setEnrichOrder(pickExst.getEnrichOrder());
		} else {
			Optional<EnrichOrder> enrichOpt = enrichOrderRepository.findById(pick.getEnrichId());
			EnrichOrder enrichOrder = null;
			if (enrichOpt.isPresent()) {
				enrichOrder = enrichOpt.get();
			} else {
				enrichOrder = new EnrichOrder();
				enrichOrder.setEnrichOrderId(pick.getEnrichId());
				enrichOrder.setOrderId(pick.getOrderId());
			}

			pick.setEnrichOrder(enrichOrder);
			enrichOrder.getPicks().add(pick);

			Optional<Container> containerOpt = containerRepository.findById(pick.getContainerId());
			Container container = null;
			if (containerOpt.isPresent()) {
				container = containerOpt.get();
				pick.setStatus(container.getStatus());
			} else {
				container = new Container();
				container.setContainerId(pick.getContainerId());
			}

			pick.setContainer(container);
			container.getPicks().add(pick);
		}

		return pickRepository.save(pick);
	}

	@Override
	public Iterable<Pick> getAll() {
		return pickRepository.findAll();
	}

	@Override
	public void createAll(Set<Pick> picks) {
		Set<String> enrichIds = new HashSet<>();
		for (Pick pick : picks) {
			pick = this.create(pick);
			enrichIds.add(pick.getEnrichId());
		}
		for (String enrichId : enrichIds) {
			enrichOrderService.statusAggregation(enrichId);
		}

	}

}
