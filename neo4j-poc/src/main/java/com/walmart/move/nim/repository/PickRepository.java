package com.walmart.move.nim.repository;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.walmart.move.nim.domain.Pick;

public interface PickRepository extends PagingAndSortingRepository<Pick, String>{

	@Query("Match (p:Pick) WHERE p.loadNbr={loadNbr} AND p.containerId={containerId} RETURN p")
	Collection<Pick> findAllByLoadNbrAndContainerId(Integer loadNbr, String containerId);

}
