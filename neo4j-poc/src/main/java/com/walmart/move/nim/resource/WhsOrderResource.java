package com.walmart.move.nim.resource;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.move.nim.domain.WhsOrder;
import com.walmart.move.nim.service.IWhsOrderService;

@RestController
@RequestMapping(path = "/orders")
public class WhsOrderResource {

	@Autowired
	private IWhsOrderService whsOrderService;

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	public WhsOrder create(@RequestBody WhsOrder whsOrder) {
		return whsOrderService.create(whsOrder);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/createBulk")
	public void createBulk(@RequestBody Set<WhsOrder> whsOrders) {
		if (!CollectionUtils.isEmpty(whsOrders))
			whsOrderService.createBulk(whsOrders);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/show")
	public Iterable<WhsOrder> getAll() {
		return whsOrderService.getAll();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/get")
	public WhsOrder get(@Param("orderId") String orderId) {
		return whsOrderService.get(orderId);
	}

}
