package com.walmart.move.nim.common;

public class Constants {
	public static enum STATUS {
		NEW(0), ENRICHED(1), PICKED(2), LOADED(3), SHIPPED(4);

		private int statusCode;

		STATUS(int statusCode) {
			this.statusCode = statusCode;
		}

		public int getStatusCode() {
			return statusCode;
		}

		public String getStatusDesc(int statusCode) {
			return values()[statusCode].name();
		}

	}
}
