package com.walmart.move.nim.resource;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.move.nim.domain.EnrichOrder;
import com.walmart.move.nim.service.IEnrichOrderService;

@RestController
@RequestMapping(path = "/enrichOrders")
public class EnrichOrderResource {

	@Autowired
	private IEnrichOrderService enrichOrderService;

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public EnrichOrder create(@RequestBody EnrichOrder enrichOrder) {
		return enrichOrderService.create(enrichOrder);
	}

	@RequestMapping(path = "/createBulk", method = RequestMethod.POST)
	public void createBulk(@RequestBody Set<EnrichOrder> enrichOrders) {
		if (!CollectionUtils.isEmpty(enrichOrders))
			enrichOrderService.createAll(enrichOrders);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/show")
	public Iterable<EnrichOrder> getAll() {
		return enrichOrderService.getAll();
	}
}
