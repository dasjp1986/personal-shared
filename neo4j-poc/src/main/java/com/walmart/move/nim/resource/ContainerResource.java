package com.walmart.move.nim.resource;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.move.nim.domain.Container;
import com.walmart.move.nim.service.IContainerService;

@RestController
@RequestMapping(path = "/containers")
public class ContainerResource {
	@Autowired
	private IContainerService containerService;

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public Container create(@RequestBody Container container) {
		return containerService.create(container);
	}

	@RequestMapping(path = "/show", method = RequestMethod.GET)
	public Iterable<Container> getAll() {
		return containerService.getAll();
	}

	@RequestMapping(path = "/createBulk", method = RequestMethod.POST)
	public void create(@RequestBody Set<Container> containers) {
		if (!CollectionUtils.isEmpty(containers))
			containerService.createAll(containers);
	}
}
