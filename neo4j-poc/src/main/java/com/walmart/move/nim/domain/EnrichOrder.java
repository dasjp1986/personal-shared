package com.walmart.move.nim.domain;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.walmart.move.nim.common.Constants;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "enrichOrderId")
@NodeEntity(label = "EnrichOrder")
public class EnrichOrder {
	@Id
	private String enrichOrderId;
	private Integer enrichQty;
	private Integer subCenter;
	private Integer deliveryNbr;
	private String orderId;
	private Integer status;

	@Relationship(type = "ENRICHES", direction = Relationship.INCOMING)
	private WhsOrder order;

	@Relationship(type = "Picks", direction = Relationship.OUTGOING)
	private Set<Pick> picks;

	public EnrichOrder() {
		super();
		this.status= Constants.STATUS.ENRICHED.getStatusCode();
		this.picks= new HashSet<>();
	}

	public Integer getEnrichQty() {
		return enrichQty;
	}

	public void setEnrichQty(Integer enrichQty) {
		this.enrichQty = enrichQty;
	}

	public Integer getSubCenter() {
		return subCenter;
	}

	public void setSubCenter(Integer subCenter) {
		this.subCenter = subCenter;
	}

	public Integer getDeliveryNbr() {
		return deliveryNbr;
	}

	public void setDeliveryNbr(Integer deliveryNbr) {
		this.deliveryNbr = deliveryNbr;
	}

	public WhsOrder getOrder() {
		return order;
	}

	public String getEnrichOrderId() {
		return enrichOrderId;
	}

	public void setEnrichOrderId(String enrichOrderId) {
		this.enrichOrderId = enrichOrderId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Set<Pick> getPicks() {
		return picks;
	}

	public void setPicks(Set<Pick> picks) {
		this.picks = picks;
	}

	public void setOrder(WhsOrder order) {
		this.order = order;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enrichOrderId == null) ? 0 : enrichOrderId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnrichOrder other = (EnrichOrder) obj;
		if (enrichOrderId == null) {
			if (other.enrichOrderId != null)
				return false;
		} else if (!enrichOrderId.equals(other.enrichOrderId))
			return false;
		return true;
	}

}
