package com.walmart.move.nim.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.walmart.move.nim.domain.Container;

public interface ContainerRepository extends PagingAndSortingRepository<Container, String>{

}
