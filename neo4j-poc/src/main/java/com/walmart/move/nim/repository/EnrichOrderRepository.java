package com.walmart.move.nim.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.walmart.move.nim.domain.EnrichOrder;

public interface EnrichOrderRepository extends PagingAndSortingRepository<EnrichOrder, String> {

	//void update(String enrichId);

}
