package com.walmart.move.nim.service;

import java.util.Set;

import com.walmart.move.nim.domain.Container;

public interface IContainerService {

	Container create(Container container);

	Iterable<Container> getAll();

	void createAll(Set<Container> containers);

}