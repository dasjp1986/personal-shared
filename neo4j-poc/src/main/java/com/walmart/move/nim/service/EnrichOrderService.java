package com.walmart.move.nim.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.walmart.move.nim.domain.EnrichOrder;
import com.walmart.move.nim.domain.Pick;
import com.walmart.move.nim.domain.WhsOrder;
import com.walmart.move.nim.repository.EnrichOrderRepository;
import com.walmart.move.nim.repository.WhsOrderRepository;

@Service
public class EnrichOrderService implements IEnrichOrderService {
	@Autowired
	private EnrichOrderRepository enrichOrderRepository;

	@Autowired
	private WhsOrderRepository whsOrderRepository;

	@Autowired
	private IWhsOrderService whsOrderService;

	@Override
	public EnrichOrder create(EnrichOrder enrichOrder) {
		Optional<EnrichOrder> enrichOpt = enrichOrderRepository.findById(enrichOrder.getEnrichOrderId());
		if (enrichOpt.isPresent()) {
			EnrichOrder eOrderExst = enrichOpt.get();
			if (eOrderExst.getStatus() > enrichOrder.getStatus()) {
				enrichOrder.setStatus(eOrderExst.getStatus());
			}
			enrichOrder.setPicks(eOrderExst.getPicks());
			enrichOrder.setOrder(eOrderExst.getOrder());
		}

		if (enrichOrder.getOrder() == null) {
			Optional<WhsOrder> whsOrderOpt = whsOrderRepository.findById(enrichOrder.getOrderId());
			WhsOrder whsOrder = null;
			if (whsOrderOpt.isPresent()) {
				whsOrder = whsOrderOpt.get();
			} else {
				whsOrder = new WhsOrder();
				whsOrder.setOrderId(enrichOrder.getOrderId());
			}
			enrichOrder.setOrder(whsOrder);
			whsOrder.getEnrichOrders().add(enrichOrder);
		}

		return enrichOrderRepository.save(enrichOrder);

	}

	@Override
	public void createAll(Set<EnrichOrder> enrichOrders) {
		Set<String> whsOrderIds = new HashSet<>();
		for (EnrichOrder enrichOrder : enrichOrders) {
			enrichOrder = this.create(enrichOrder);
			whsOrderIds.add(enrichOrder.getOrderId());
		}
		for (String whsOrderId : whsOrderIds) {
			whsOrderService.statusAggregation(whsOrderId);
		}
	}

	@Override
	public Iterable<EnrichOrder> getAll() {
		return enrichOrderRepository.findAll();
	}

	@Override
	public void statusAggregation(String enrichOrderId) {
		EnrichOrder enrichOrder = enrichOrderRepository.findById(enrichOrderId).get();
		Set<Pick> picks = enrichOrder.getPicks();
		if (!CollectionUtils.isEmpty(picks)) {
			int minStatus = Integer.MAX_VALUE;
			for (Pick pick : picks) {
				if (pick.getStatus() < minStatus) {
					minStatus = pick.getStatus();
				}
			}
			if (minStatus > enrichOrder.getStatus()) {
				enrichOrder.setStatus(minStatus);
				enrichOrderRepository.save(enrichOrder);
				whsOrderService.statusAggregation(enrichOrder.getOrderId());
			}
		}
	}

	@Override
	public EnrichOrder get(String enrichOrderId) {
		return (enrichOrderRepository.findById(enrichOrderId).orElseGet(null));
	}
}
