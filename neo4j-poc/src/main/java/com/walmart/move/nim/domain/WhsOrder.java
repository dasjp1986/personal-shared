package com.walmart.move.nim.domain;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.walmart.move.nim.common.Constants;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "orderId")
@NodeEntity(label = "WhsOrder")
public class WhsOrder {
	@Id
	private String orderId;
	private Integer orderRecordType;
	private String destination;
	private Integer itemNbr;
	private Integer orderQty;
	private Integer status;

	@Relationship(type = "ENRICHES", direction = Relationship.OUTGOING)
	private Set<EnrichOrder> enrichOrders;

	public WhsOrder() {
		super();
		this.status= Constants.STATUS.NEW.getStatusCode();
		this.enrichOrders= new HashSet<>();
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getOrderRecordType() {
		return orderRecordType;
	}

	public void setOrderRecordType(Integer orderRecordType) {
		this.orderRecordType = orderRecordType;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getItemNbr() {
		return itemNbr;
	}

	public void setItemNbr(Integer itemNbr) {
		this.itemNbr = itemNbr;
	}

	public Integer getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(Integer orderQty) {
		this.orderQty = orderQty;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Set<EnrichOrder> getEnrichOrders() {
		return enrichOrders;
	}

	public void setEnrichOrders(Set<EnrichOrder> enrichOrders) {
		this.enrichOrders = enrichOrders;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WhsOrder other = (WhsOrder) obj;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		return true;
	}

}
