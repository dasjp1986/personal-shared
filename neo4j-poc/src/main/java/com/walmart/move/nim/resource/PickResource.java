package com.walmart.move.nim.resource;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.move.nim.domain.Pick;
import com.walmart.move.nim.service.IPickService;

@RestController
@RequestMapping(path = "/picks")
public class PickResource {
	@Autowired
	private IPickService pickService;

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public Pick create(@RequestBody Pick pick) {
		return pickService.create(pick);
	}

	@RequestMapping(path = "/createBulk", method = RequestMethod.POST)
	public void createAll(@RequestBody Set<Pick> picks) {
		if (!CollectionUtils.isEmpty(picks))
			pickService.createAll(picks);
	}

	@RequestMapping(path = "/show", method = RequestMethod.GET)
	public Iterable<Pick> getAll() {
		return pickService.getAll();
	}

}
