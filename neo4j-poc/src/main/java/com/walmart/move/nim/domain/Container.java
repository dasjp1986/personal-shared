package com.walmart.move.nim.domain;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.walmart.move.nim.common.Constants;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "containerId")
@NodeEntity(label = "Container")
public class Container {
	@Id
	private String containerId;
	private Integer loadNbr;
	private Integer trailerNbr;
	private Integer status;

	@Relationship(type = "CONTAINS", direction = Relationship.OUTGOING)
	Set<Pick> picks;

	public Container() {
		this.status = Constants.STATUS.PICKED.getStatusCode();
		this.picks= new HashSet<>();
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public Integer getLoadNbr() {
		return loadNbr;
	}

	public void setLoadNbr(Integer loadNbr) {
		this.loadNbr = loadNbr;
	}

	public Integer getTrailerNbr() {
		return trailerNbr;
	}

	public void setTrailerNbr(Integer trailerNbr) {
		this.trailerNbr = trailerNbr;
	}

	public Set<Pick> getPicks() {
		return picks;
	}

	public void setPicks(Set<Pick> picks) {
		this.picks = picks;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((containerId == null) ? 0 : containerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Container other = (Container) obj;
		if (containerId == null) {
			if (other.containerId != null)
				return false;
		} else if (!containerId.equals(other.containerId))
			return false;
		return true;
	}

}
