package com.walmart.move.nim.service;

import java.util.Set;

import com.walmart.move.nim.domain.Pick;

public interface IPickService {

	Pick create(Pick pick);

	Iterable<Pick> getAll();

	void createAll(Set<Pick> picks);

}