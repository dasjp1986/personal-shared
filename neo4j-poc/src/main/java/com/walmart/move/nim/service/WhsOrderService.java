package com.walmart.move.nim.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.walmart.move.nim.domain.EnrichOrder;
import com.walmart.move.nim.domain.WhsOrder;
import com.walmart.move.nim.repository.WhsOrderRepository;

@Service
public class WhsOrderService implements IWhsOrderService {
	@Autowired
	private WhsOrderRepository whsOrderRepository;

	@Override
	public WhsOrder create(WhsOrder whsOrder) {
		Optional<WhsOrder> optional = whsOrderRepository.findById(whsOrder.getOrderId());
		if (optional.isPresent()) {
			WhsOrder orderExst = optional.get();
			if (orderExst.getStatus() > whsOrder.getStatus()) {
				whsOrder.setStatus(orderExst.getStatus());
			}
			whsOrder.setEnrichOrders(orderExst.getEnrichOrders());
		}
		return whsOrderRepository.save(whsOrder);
	}

	@Override
	public Iterable<WhsOrder> getAll() {
		return whsOrderRepository.findAll();
	}

	@Override
	public WhsOrder get(String orderId) {
		return whsOrderRepository.findById(orderId).get();
	}

	@Override
	public void createBulk(Set<WhsOrder> whsOrders) {
		for (WhsOrder whsOrder : whsOrders) {
			this.create(whsOrder);
		}
	}

	@Override
	public void statusAggregation(String whsOrderId) {
		Optional<WhsOrder> whsOrderOpt = whsOrderRepository.findById(whsOrderId);
		if (whsOrderOpt.isPresent()) {
			WhsOrder whsOrder = whsOrderOpt.get();
			Set<EnrichOrder> enrichOrders = whsOrder.getEnrichOrders();
			if (!CollectionUtils.isEmpty(enrichOrders)) {
				int minStatus = Integer.MAX_VALUE;
				for (EnrichOrder enrichOrder : enrichOrders) {
					if (enrichOrder.getStatus() < minStatus) {
						minStatus = enrichOrder.getStatus();
					}
				}
				if (minStatus > whsOrder.getStatus()) {
					whsOrder.setStatus(minStatus);
					whsOrderRepository.save(whsOrder);
				}
			}
		}

	}
}
