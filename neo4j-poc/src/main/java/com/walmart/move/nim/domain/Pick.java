package com.walmart.move.nim.domain;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.walmart.move.nim.common.Constants;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "pickId")
@NodeEntity(label = "Pick")
public class Pick {
	@Id
	private String pickId;
	private Integer itemNbr;
	private Integer pickQty;
	private Integer status;
	private String containerId;
	private String enrichId;
	private Integer loadNbr;
	private String orderId;

	@Relationship(type = "CONTAINS", direction = Relationship.INCOMING)
	private Container container;

	@Relationship(type = "Picks", direction = Relationship.INCOMING)
	private EnrichOrder enrichOrder;

	public Pick() {
		this.status = Constants.STATUS.PICKED.getStatusCode();
	}

	public String getPickId() {
		return pickId;
	}

	public void setPickId(String pickId) {
		this.pickId = pickId;
	}

	public Integer getItemNbr() {
		return itemNbr;
	}

	public void setItemNbr(Integer itemNbr) {
		this.itemNbr = itemNbr;
	}

	public Integer getPickQty() {
		return pickQty;
	}

	public void setPickQty(Integer pickQty) {
		this.pickQty = pickQty;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public String getEnrichId() {
		return enrichId;
	}

	public void setEnrichId(String enrichId) {
		this.enrichId = enrichId;
	}

	public EnrichOrder getEnrichOrder() {
		return enrichOrder;
	}

	public void setEnrichOrder(EnrichOrder enrichOrder) {
		this.enrichOrder = enrichOrder;
	}

	public Integer getLoadNbr() {
		return loadNbr;
	}

	public void setLoadNbr(Integer loadNbr) {
		this.loadNbr = loadNbr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pickId == null) ? 0 : pickId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pick other = (Pick) obj;
		if (pickId == null) {
			if (other.pickId != null)
				return false;
		} else if (!pickId.equals(other.pickId))
			return false;
		return true;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
