package com.da.coding.capillary;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 
 * @author dagraw1
 *
 *         The Upload Server A new website to host video and music is built up
 *         by the company X. The engineers at X are facing a major issue in
 *         identifying whether the data they receive in one of their API is of
 *         video or music.
 * 
 *         Now, you will be given lines of data. Each line of data consists a
 *         list of strings. Each string either represents an integer or a name.
 *         For each line of data, you need to check whether that data
 *         corresponds to a music or a video.
 * 
 *         You need to follow the following rules to detect the data and store
 *         it:
 * 
 *         A Music data consists of a name and an integer that denote the
 *         bitrate and nothing else. A video data consists of a name and two
 *         integers that denote the resolution of the video and nothing else.
 *         Rest of the data which does not match any of the formats above is to
 *         be ignored. A music or a video name can consist of integers but it
 *         will contain at least one character of the English alphabets. An
 *         integer in the data will consist only of integers, and it will never
 *         start with 0. For each line of data, if it satisfies the constraints
 *         of a music then print M, if it satisfies constraints of a video print
 *         V, or else print N which means that the data has to be ignored.
 * 
 *         Input Format
 * 
 *         The first line contains an integer as input denoting the total
 *         numbers of lines in the input. Next lines contains either two strings
 *         or three strings separated by space.
 * 
 *         Output Format
 * 
 *         For each data, you need to print either of the three characters N, V
 *         or M.
 * 
 *         Sample Input 3 243 5399 a12 320 240 aviation 189 Sample Output N V M
 *         Explanation In the given sample. the first one does not correspond to
 *         either a video or a music as it contains two integers which do not
 *         match the format of any of the two types of data.
 * 
 *         The second data corresponds to a video as it contains one name and
 *         two integers.
 * 
 *         The third data corresponds to a music as it contains one name and one
 *         integer.
 * 
 * 
 */
public class TheUploadServer {

	/*
	 * You can declare the other variables or functions here
	 */
	public static void process_data(String a, String b) {
		if (checkName(a) && checkNum(b)) {
			System.out.println("M");
		} else {
			System.out.println("N");
		}
	}

	public static void process_data(String a, String b, String c) {
		if (checkName(a) && checkNum(b) && checkNum(c)) {
			System.out.println("V");
		} else {
			System.out.println("N");
		}
	}

	private static boolean checkNum(String str) {

		if (str.length() > 0 && str.charAt(0) < '1' && str.charAt(0) > '9') {
			return false;
		}
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) < '0' || str.charAt(i) > '9') {
				return false;
			}
		}
		return true;

	}

	private static boolean checkName(String str) {
		boolean isValid = false;

		for (int i = 0; i < str.length(); i++) {
			if ((str.charAt(i) >= 'a' && str.charAt(i) <= 'z') || (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z')) {
				isValid = true;
			}
		}
		return isValid;
	}

	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int length = Integer.parseInt(reader.readLine());
		String line = null;

		for (int i = 0; i < length; i++) {
			line = reader.readLine();
			// System.out.println(line);
			String[] tokens = line.split(" ");
			if (tokens.length == 2) {
				process_data(tokens[0], tokens[1]);
			} else if (tokens.length == 3) {
				process_data(tokens[0], tokens[1], tokens[2]);
			} else {
				System.out.println("N");
			}
		}

		reader.close();
	}

}
