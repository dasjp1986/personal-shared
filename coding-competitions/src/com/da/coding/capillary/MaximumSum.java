package com.da.coding.capillary;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * 
 * @author dagraw1
 *
 *         Maximum Sum Given an array of integers. Now, you have to output the
 *         sum of unique values of the maximum subarray sum of all the possible
 *         subarrays of the given array . Note: Subarray means contiguous
 *         elements with atleast one element in it.
 * 
 *         Input Format
 * 
 *         The first line of the input contains a single integer , the total
 *         number of elements in array . The next line of the input contains
 *         space-separated integers representing the elements of the array.
 * 
 *         Output Format
 * 
 *         The only single line of the output should contain a single integral
 *         value representing the answer to the problem.
 * 
 *         Constraints
 * 
 * 
 *         Sample Input 4 5 -2 7 -3 Sample Output 17
 *         
 *         
 */
public class MaximumSum {
	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);

		int n = scn.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < n; i++) {
			nums[i] = scn.nextInt();
		}
		System.out.println(solve(n, nums));
		scn.close();
	}

	private static int solve(int n, int[] nums) {
		Set<Integer> uniques = new HashSet<>();
		int[] maxArray = new int[nums.length];

		for (int i = 0; i < nums.length; i++) {
			maxArray[i] = nums[i];
			uniques.add(nums[i]);
			int sum = nums[i];
			for (int j = i + 1; j < nums.length; j++) {
				if (sum + nums[j] > nums[j]) {
					if (sum + nums[j] > maxArray[i]) {
						maxArray[i] = sum + nums[j];
						uniques.add(maxArray[i]);
					}
				} else {
					if (nums[j] > maxArray[i]) {
						maxArray[i] = nums[j];
						uniques.add(maxArray[i]);
					}
				}
				sum += nums[j];
			}
		}

		int total = 0;

		for (int num : uniques) {
			total += num;
		}
		return total;
	}
}
