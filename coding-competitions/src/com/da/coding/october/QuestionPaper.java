package com.da.coding.october;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class QuestionPaper {

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int testCases = scn.nextInt();
		for (int t = 0; t < testCases; t++) {
			int n = scn.nextInt();
			int a = scn.nextInt();
			int b = scn.nextInt();

			System.out.println(solve(n, a, b));
		}
		scn.close();
	}

	private static int solve(int n, int a, int b) {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(0, 1);
		map.put(a, 1);
		map.put(b, 1);

		List<Integer> list = new ArrayList<>();

		for (int i = 1; i < n; i++) {
			Set<Integer> keys = map.keySet();
			list.addAll(keys);
			for (Integer num : list) {
				map.put(num + a, 1);
				map.put(num + b, 1);
			}
		}

		return map.size();
	}

}
