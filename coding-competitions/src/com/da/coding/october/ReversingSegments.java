package com.da.coding.october;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         You are given an array of size and queries. For each query, you are
 *         given two indices of the array and . The subarray generated from to
 *         is reversed. Your task is to determine the maximum sum of the
 *         subarrays.
 * 
 *         Note: After each query is solved, the array comes to its initial
 *         states.
 * 
 *         Input format
 * 
 *         First line: Two space-separated integers and Next line:
 *         space-separated integers denoting the array elements. Next lines: Two
 *         space-separated integers in every line denoting the values of and
 *         Output format
 * 
 *         For each query, print the required answer in a new line.
 * 
 *         Constraints
 * 
 *         SAMPLE INPUT 5 2 3 -1 4 2 -1 3 4 1 2 SAMPLE OUTPUT 8 9 Explanation
 *         Given array is {3,-1,4,2,-1}.
 * 
 *         For first query L=3 and R=4. Now the array becomes {3,-1,2,4,-1}.
 * 
 *         Maximum sub-array sum is 8 and the sub-array is {3,-1,2,4}.
 * 
 *         For second query L=1 and R=2. Now the array becomes {-1,3,4,2,-1}.
 * 
 *         Maximum sub-array sum is 9 and the sub-array is {3,4,2}.
 */


//TLE Error
public class ReversingSegments {
	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		int q = scn.nextInt();
		int[] a = new int[n + 1];
		int[] sums = new int[n + 1];
		for (int i = 1; i <= n; i++) {
			a[i] = scn.nextInt();
			sums[i] = a[i];
		}

		int max = sums[1];
		int index = 0;
		for (int j = 2; j < sums.length; j++) {
			if (sums[j] + sums[j - 1] > sums[j]) {
				sums[j] = sums[j] + sums[j - 1];

				if (sums[j] > max) {
					max = sums[j];
					index = j;
				}
			}
		}

		for (int j = 0; j < q; j++) {
			System.out.println(solve(a, scn.nextInt(), scn.nextInt(), sums, index));
		}
		scn.close();
	}

	private static int solve(int[] a, int s, int e, int[] sums, int index) {
		if (index < s) {
			return sums[index];
		} else if (index > e) {
			return compute(s, e, index, a, sums);
		} else {
			return compute(s, e, a.length - 1, a, sums);
		}

	}

	private static int compute(int s, int e, int length, int[] a, int[] sums) {

		int temp = sums[s - 1];
		int max = sums[s - 1];

		int t = e;
		while (t >= s) {
			if (temp + a[t] > temp) {
				temp = temp + a[t];
				if (temp > max) {
					max = temp;
				}
			}
			t--;
		}

		t = e + 1;
		while (t <= length) {
			if (temp + a[t] > temp) {
				temp = temp + a[t];
				if (temp > max) {
					max = temp;
				}
			}
			t++;
		}
		return max;
	}
}
