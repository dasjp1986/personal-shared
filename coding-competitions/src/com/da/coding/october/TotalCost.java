package com.da.coding.october;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         A family consists of members. You are given the task to book flight
 *         tickets for these members. You are given the following information
 *         about the airline in which you have to book the tickets:
 * 
 *         : It denotes the cost of one ticket of the flight. : It denotes the
 *         number of total available seats in the flight. : If the numbers of
 *         available seats are less than or equal to , then the cost of the
 *         flight ticket increases to . : It denotes the new hiked cost.
 *         Determine the total cost to book the tickets for all the family
 *         members.
 * 
 *         Note: The tickets are booked one by one for all the family members.
 * 
 *         Input format
 * 
 *         First line: Five space-separated integers and respectively
 * 
 *         Output format
 * 
 *         Print the total cost to book the tickets for all the members of the
 *         family.
 * 
 *         Constraints
 * 
 * 
 * 
 * 
 * 
 *         SAMPLE INPUT 6000 10 5 6500 7 SAMPLE OUTPUT 43000 Explanation Monk
 *         books 5 tickets for price 6000 each and rest 2 for price 6500 each.
 *         Total expenditure = 6000*5+6500*2 = 43000
 */

//Working as expected
public class TotalCost {

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int p = scn.nextInt();
		int s = scn.nextInt();
		int t = scn.nextInt();
		int h = scn.nextInt();
		int x = scn.nextInt();

		System.out.println(solve(p, s, t, h, x));
		scn.close();
	}

	private static int solve(int p, int s, int t, int h, int x) {
		int total = Math.min(s - t, x) * p;
		if (x > s - t) {
			total += (x - s + t) * h;
		}
		return total;
	}

}
