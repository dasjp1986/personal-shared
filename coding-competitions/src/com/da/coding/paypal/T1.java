package com.da.coding.paypal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class T1 {

	public static void main(String[] args) {
		List<Integer> a = new ArrayList<>();
//		a.add(1);
//		a.add(2);
//		a.add(1);
//		a.add(3);
//		a.add(2);

		a.add(0);

		System.out.println(degreeOfArray(a));
	}

	public static int degreeOfArray1(List<Integer> arr) {

		Map<Integer, Integer> map = new HashMap<>();
		int num = 0;
		int max = 0;
		for (int i = 0; i < arr.size(); i++) {
			Integer count = map.get(arr.get(i));
			if (count == null) {
				count = 0;
			}
			count++;
			map.put(arr.get(i), count);
			if (count > max) {
				max = count;
				num = arr.get(i);
			}
		}

		int i = 0, j = arr.size() - 1;
		while (i < arr.size() && arr.get(i) != num) {
			i++;
		}

		while (j >= 0 && arr.get(j) != num) {
			j--;
		}

		return j - i + 1;

	}

	public static int degreeOfArray(List<Integer> arr) {

		Map<Integer, Integer> map = new HashMap<>();
		int num = 0;
		int max = 0;
		for (int i = 0; i < arr.size(); i++) {
			Integer count = map.get(arr.get(i));
			if (count == null) {
				count = 0;
			}
			count++;
			map.put(arr.get(i), count);
			if (count > max) {
				max = count;
				num = arr.get(i);
			}
		}

		int length = arr.size();
		for (Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() == max) {
				num = entry.getKey();
				int i = 0, j = arr.size() - 1;
				while (i < arr.size() && arr.get(i) != num) {
					i++;
				}

				while (j >= 0 && arr.get(j) != num) {
					j--;
				}

				length = Math.min(j - i + 1, length);
			}
		}
		
		return length;

	}
}
