package com.da.coding.paypal;

/*
 * Create the FoodFactory class and other required classes here.
 */

import java.util.Scanner;

abstract class Cuisine {
	public abstract Cuisine serveFood(String dish);
}

class UnservableCuisineRequestException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnservableCuisineRequestException(String message) {
		super(message);
	}
}

class Chinese extends Cuisine {
	private String dish;

	public Cuisine serveFood(String dish) {
		return this;
	}

	public String getDish() {
		return dish;
	}

	public void setDish(String dish) {
		this.dish = dish;
	}
}

class Italian extends Cuisine {
	private String dish;

	public Cuisine serveFood(String dish) {
		this.dish = dish;
		return this;
	}

	public String getDish() {
		return dish;
	}

	public void setDish(String dish) {
		this.dish = dish;
	}
}

class Japanese extends Cuisine {
	private String dish;

	public Cuisine serveFood(String dish) {
		this.dish = dish;
		return this;
	}

	public String getDish() {
		return dish;
	}

	public void setDish(String dish) {
		this.dish = dish;
	}
}

class Mexican extends Cuisine {
	private String dish;

	public Cuisine serveFood(String dish) {
		this.dish = dish;
		return this;
	}

	public String getDish() {
		return dish;
	}

	public void setDish(String dish) {
		this.dish = dish;
	}
}

class FoodFactory {
	private static volatile FoodFactory SINGLETON;
	private java.util.Map<String, Cuisine> cache;

	private FoodFactory() {
		super();
		cache = new java.util.concurrent.ConcurrentHashMap<>();
	}

	public static FoodFactory getFactory() {
		if (SINGLETON == null) {
			synchronized (FoodFactory.class) {
				if (SINGLETON == null) {
					SINGLETON = new FoodFactory();
				}
			}
		}
		return SINGLETON;
	}

	public void registerCuisine(String cuisineKey, Cuisine cuisine) {
		cache.put(cuisineKey, cuisine);
	}

	public Cuisine serveCuisine(String cuisineKey, String dish) throws UnservableCuisineRequestException {
		Cuisine cuisine = cache.get(cuisineKey);
		if (cuisine == null) {
			throw new UnservableCuisineRequestException("Unservable cusine " + cuisineKey + " for dish " + dish);
		}

		try {
			java.lang.reflect.Method method = cuisine.getClass().getMethod("setDish", new Class[] { String.class });
			method.invoke(cuisine, dish);

		} catch (Exception ex) {

		}

		return cuisine;
	}

}

public class Solution {
	private static final Scanner INPUT_READER = new Scanner(System.in);
	private static final FoodFactory FOOD_FACTORY = FoodFactory.getFactory();

	static {
		FoodFactory.getFactory().registerCuisine("Chinese", new Chinese());
		FoodFactory.getFactory().registerCuisine("Italian", new Italian());
		FoodFactory.getFactory().registerCuisine("Japanese", new Japanese());
		FoodFactory.getFactory().registerCuisine("Mexican", new Mexican());
	}

	public static void main(String[] args) {
		int totalNumberOfOrders = Integer.parseInt(INPUT_READER.nextLine());
		while (totalNumberOfOrders-- > 0) {
			String[] food = INPUT_READER.nextLine().split(" ");
			String cuisine = food[0];
			String dish = food[1];

			try {
				if (FOOD_FACTORY.equals(FoodFactory.getFactory())) {
					Cuisine servedFood = FOOD_FACTORY.serveCuisine(cuisine, dish);

					switch (cuisine) {
					case "Chinese":
						Chinese chineseDish = (Chinese) servedFood;
						System.out.println("Serving " + chineseDish.getDish() + "(Chinese)");
						break;
					case "Italian":
						Italian italianDish = (Italian) servedFood;
						System.out.println("Serving " + italianDish.getDish() + "(Italian)");
						break;
					case "Japanese":
						Japanese japaneseDish = (Japanese) servedFood;
						System.out.println("Serving " + japaneseDish.getDish() + "(Japanese)");
						break;
					case "Mexican":
						Mexican mexicanDish = (Mexican) servedFood;
						System.out.println("Serving " + mexicanDish.getDish() + "(Mexican)");
						break;
					default:
						break;
					}
				}
			} catch (UnservableCuisineRequestException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
}