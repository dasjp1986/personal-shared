package com.da.coding.global;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * 
 * @author dagraw1
 *
 *Almost Vowel

You are given with a string
. Your task is to remove atmost two substrings of any length from the given string 

such that the remaining string contains vowels('a','e','i','o','u') only. Your aim is the maximise the length of the remaining string. Output the length of remaining string after removal of atmost two substrings.
NOTE: The answer may be 0, i.e. removing the entire string.

INPUT FORMAT:

First line of input contains number of test cases
.
Each test case comprises of a single line corresponding to string

.

OUTPUT FORMAT:

Output a single integer denoting length of string containing only vowels after removal of atmost two substrings of any length.

CONSTRAINTS:

Sample Input

2
earthproblem
letsgosomewhere

Sample Output

3
2

Explanation

For the first test case, the maximum length possible is 3. String of length 3 with only vowels formed by removing atmost 2 substrings can be: "eao" or "eae".
For the second test case, the maximum length possible is 2. String of length 2 with only vowels formed by removing atmost 2 substrings can be: "ee" or "oe".
 */
public class Question2 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter wr = new PrintWriter(System.out);
		int T = Integer.parseInt(br.readLine().trim());
		for (int t_i = 0; t_i < T; t_i++) {
			String S = br.readLine();

			int out_ = solve(S);
			System.out.println(out_);
			System.out.println("");
		}

		wr.close();
		br.close();
	}

	static int solve(String s) {
		int left = 0;
		int right = 0;
		int i = 0, j = 0;

		int max = 0;
		while (i < s.length()) {
			right = 0;

			while (i < s.length() && !isVowel(s.charAt(i))) {
				i++;
			}
			j = i;

			while (j < s.length() && isVowel(s.charAt(j))) {
				j++;
			}
			right = j - i;

			max = Math.max(max, left + right);

			left = right;
			i = j;
		}

		return max;
	}

	public static boolean isVowel(char ch) {
		if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
			return true;
		} else {
			return false;
		}
	}

}
