package com.da.coding.global;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * 
 * @author dagraw1
 *
 *Root X

Given a tree with
 vertices and  edges. Every vertex has value associated with it which is given by array . Now you have to answer queries. For each query you are given and , aim is to find the numbers of nodes with value , in the path from vertex

to root.

Note - Root of the tree is 1 and given graph is a tree.

Input

First Line:
, no of vertices and

, no of queries

Second Line:
 space separated integers, where  integer represent the value of 

vertex. 

Next N - 1 lines:
and

, meaning there is an edge between vertex u and v.

Next Q lines: 
and

, defining query as explained.

Output

For each Query print answer in new line.

Constraints

Sample Input

8 5

7 9 8 5 10 8 6 11

1 2

1 3

3 4

4 5

4 8

2 7

2 6

7 6

1 7

5 8

8 11

3 6

Sample Output

1

1

3

4

0

Explanation

For query 1: V = 7 and X = 6, nodes from 7 to root are [7, 2, 1] having values [6, 9, 7]. No of values less than X are 1.

For query 2: V = 1 and X = 7, nodes from 1 to root are [1] having values [7]. No of values less than X are 1.

For query 3: V = 5 and X = 8, nodes from 5 to root are [5 4 3 1] having values [10 5 8 7]. No of values less than X are 3.

Similarly for query 4 and 5, ans 4 and 0 respectively.
 */
public class Question1 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter wr = new PrintWriter(System.out);
		String[] tmp = br.readLine().split(" ");
		int N = Integer.parseInt(tmp[0]);
		int Q = Integer.parseInt(tmp[1]);

		String[] arr_Val = br.readLine().split(" ");
		int[] Val = new int[N + 1];
		for (int i_Val = 0; i_Val < N; i_Val++) {
			Val[i_Val + 1] = Integer.parseInt(arr_Val[i_Val]);
		}

		int[] u = new int[N];
		int[] v = new int[N];

		for (int i_u = 0; i_u < N - 1; i_u++) {
			String[] tmp1 = br.readLine().split(" ");
			u[i_u] = Integer.parseInt(tmp1[0]);
			v[i_u] = Integer.parseInt(tmp1[1]);

		}
		int[] V = new int[Q];
		int[] X = new int[Q];
		for (int i_V = 0; i_V < Q; i_V++) {
			String[] tmp1 = br.readLine().split(" ");
			V[i_V] = Integer.parseInt(tmp1[0]);
			X[i_V] = Integer.parseInt(tmp1[1]);
		}

		int[] out_ = CountNodes(Val, v, N, Q, u, V, X);
		System.out.println(out_[0]);
		for (int i_out_ = 1; i_out_ < out_.length; i_out_++) {
			System.out.println(out_[i_out_]);
		}

		wr.close();
		br.close();
	}

	static int[] CountNodes(int[] Val, int[] v, int N, int Q, int[] u, int[] V, int[] X) {
		int[] parent = new int[Val.length];
		parent[1] = 0;

		for (int i = 0; i < u.length; i++) {
			parent[v[i]] = u[i];
		}

		int[] result = new int[Q];
		for (int i = 0; i < Q; i++) {
			result[i] = count(parent, V[i], X[i], Val);
		}

		return result;
	}

	private static int count(int[] parent, int v, int x, int[] val) {
		int index = v;
		int count = 0;
		while (index != 0) {
			if (val[index] <= x) {
				count++;
			}
			index = parent[index];
		}

		return count;
	}

}
