package com.da.coding.increff;

import java.util.Scanner;

public class Problem3 {

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);

		int n = scn.nextInt();
		int k = scn.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < n; i++) {
			nums[i] = scn.nextInt();
		}
		System.out.println(solve(n, k, nums));
		scn.close();
	}

	private static int solve(int n, int k, int[] nums) {
		System.out.println("n: " + n + ", k: " + k + ", nums" + nums);
		if (n <= k) {
			return 1;
		}

		int left = (nums[0] + nums[1] + nums[2]) / k;
		int right = (nums[n - k] + nums[n - k + 1] + nums[n - k + 2]) / k;

		int[] leftArray = new int[n - k + 1];
		leftArray[0] = left;
		for (int i = 1; i < leftArray.length; i++) {
			leftArray[i] = nums[k + i - 1];
		}

		int[] rightArray = new int[n - k + 1];
		rightArray[rightArray.length - 1] = right;
		for (int i = rightArray.length - 2; i >= 0; i--) {
			rightArray[i] = nums[i];
		}

		int max = Math.max(left * solve(leftArray.length, k, leftArray),
				right * solve(rightArray.length, k, rightArray));

		return max;
	}

}
