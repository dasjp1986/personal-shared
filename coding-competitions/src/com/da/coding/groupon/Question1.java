package com.da.coding.groupon;

import java.util.ArrayList;
import java.util.List;

public class Question1 {
	
	public static long listMax(int n, List<List<Integer>> operations) {
        long[] res= new long[n+1];
        long max=0;

        for(List<Integer> ops: operations){
            int a= ops.get(0);
            int b= ops.get(1);
            int k = ops.get(2);

            for(int i=a;i<=b;i++){
                res[i]= res[i]+k;
                if(res[i]>max){
                    max= res[i];
                }
            }
        }

        return max;

    }
	
	public static long newlistMax(int n, List<List<Integer>> operations) {
        long[] res= new long[n+2];
        long max=0;
        long sum=0;

        
        for(List<Integer> ops: operations){
            int a= ops.get(0);
            int b= ops.get(1);
            int k = ops.get(2);

            res[a]+= k;
            res[b+1]-=k;
            
            
        }
        
        for (int i = 1; i < res.length; i++) {
			sum+= res[i];
			max= Math.max(sum, max);
		}

        return max;

    }
	
	
	public static void main(String[] args) {
		Question1 q= new Question1();
		System.out.println(q);
	}
}
class Range{
	int start;
	int end;
	
	public Range(int start, int end){
		this.start= start;
		this.end= end;
	}
}
