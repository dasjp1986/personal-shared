package com.da.coding.american;

public class Task3 {

	public static int solution(int n) {

		long fib = 0;
		if (n < 2) {
			return n;
		}

		long a = 0;
		long b = 1;
		for (int i = 1; i <= n; i++) {
			fib = a + b;
			b = a;
			a = fib;
		}

		String str = Long.toString(fib);
		int result = 0;
		if (str.length() <= 6) {
			result = Integer.parseInt(str);
		} else {
			result = Integer.parseInt(str.substring(str.length() - 6));
		}

		return result;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			System.out.println(solution(i)+","+solution(i+60));
		}
	}
}
