package com.da.coding.american;

public class Task2 {
	public static void main(String args[]) throws Exception {
		/* Enter your code here. Read input from STDIN. Print output to STDOUT */
		System.out.println(count(5, new int[] { 1, 3, 5 }, 0));
	}

	public static int count(int n, int a[], int index) {
		if (n == 0) {
			return 1;
		}

		if (index >= a.length) {
			return 0;
		}

		int count = 0;

		if (n - a[index] >= 0) {
			count = count + count(n - a[index], a, index) + count(n, a, index + 1);
		}

		return count;
	}
}
