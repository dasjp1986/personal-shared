package com.da.coding.american;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Task1 {

	public static void main(String args[]) throws Exception {
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		String song = scn.next();

		System.out.println(solve(n, song));

		scn.close();
	}

	private static int solve(int n, String song) {
		int j = 0;
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			list.add(i);
		}
		Iterator<Integer> it = list.iterator();
		while (n != 1) {
			if (j == song.length()) {
				j = 0;
			}
			if (song.charAt(j) == 'a') {
				it.next();
				j++;
			} else if (song.charAt(j) == 'b') {
				it.next();
				it.remove();
				n--;
				j++;
			}
			if (!it.hasNext()) {
				it = list.iterator();
			}
		}
		return list.get(0) + 1;
	}

}
