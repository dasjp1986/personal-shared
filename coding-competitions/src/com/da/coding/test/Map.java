package com.da.coding.test;

public class Map {
	Node root = new Node(null);

	public void put(Integer val) {
		String binaryString = Integer.toBinaryString(val);

	}

}

class Node {
	private Boolean val;
	private Node left;
	private Node right;

	public Node(Boolean val) {
		super();
		this.val = val;
	}

	public Boolean getVal() {
		return val;
	}

	public void setVal(Boolean val) {
		this.val = val;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

}
