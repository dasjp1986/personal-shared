package com.da.coding.test;

public class T1 {
	
	int count= getCount();
	
	public T1() {
		System.out.println("running constructor");
	}
	
	static {
		System.out.println("running static block");
	}
	
	public static void main(String args[]) throws Exception {
		T1 t1= new T1();
		T1 t2= new T1();
	}

	private int getCount() {
		System.out.println("running count");
		return 100;
	}

}
