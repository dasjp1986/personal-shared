package com.da.coding.test;

import java.lang.instrument.Instrumentation;

public class Space2 {
	Temp2[] temp = new Temp2[100];

	public static void main(String[] args) throws InterruptedException {
		Runtime runtime = Runtime.getRuntime();
		Space1 sp1 = new Space1();
		runtime.gc();
		Thread.sleep(1000);
		long s1 = runtime.totalMemory() - runtime.freeMemory();
		System.out.println(s1);
		
		runtime.gc();
		Thread.sleep(1000);
		long s2 = runtime.totalMemory() - runtime.freeMemory();
		System.out.println(s2);

	}
}

class Temp2 {
	int attribute;
}
