package com.da.coding.test;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * 
 * @author dagraw1
 * 
 *         Find product of min and max elements in the given stream of elements
 *         Stream can push or pop elements by their value
 */

public class MinMaxProduct {
	public static void main(String[] args) {
		List<String> operations = new ArrayList<>();
		List<Integer> x = new ArrayList<>();

		operations.add("push");
		operations.add("push");
		operations.add("push");
		operations.add("pop");

		x.add(2);
		x.add(3);
		x.add(4);
		x.add(2);

		List<Long> res = maxMin(operations, x);
		System.out.println(res);

	}

	public static List<Long> maxMin(List<String> operations, List<Integer> x) {
		List<Long> results = new ArrayList<>();

		if (operations == null || operations.size() == 0) {
			return results;

		}

		Integer min = 0;
		Integer max = 0;
		TreeMap<Integer, Integer> map = new TreeMap<>();
		for (int i = 0; i < operations.size(); i++) {
			if (operations.get(i).equalsIgnoreCase("push")) {
				map.put(x.get(i), 1);
			}

			if (operations.get(i).equalsIgnoreCase("pop")) {
				map.remove(x.get(i));
			}
			min = map.firstKey();
			max = map.lastKey();
			results.add(new Long(min.longValue()*max.longValue()));
		}
		return results;
	}
}
