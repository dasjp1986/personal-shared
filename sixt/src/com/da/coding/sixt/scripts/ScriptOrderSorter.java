package com.da.coding.sixt.scripts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ScriptOrderSorter {

	public List<Integer> buildOrder(List<VulnerabilityScript> scripts) {

		List<Integer> orderList = new ArrayList<>();
		Set<Integer> nodes = new LinkedHashSet<>();
		Map<Integer, Set<Integer>> outgoing = new HashMap<>();
		Map<Integer, Set<Integer>> incoming = new HashMap<>();

		for (VulnerabilityScript script : scripts) {
			List<Integer> dependencies = script.getDependencies();
			if (dependencies != null) {
				for (Integer depend : dependencies) {
					addToMap(incoming, script.getScriptId(), depend);
					addToMap(outgoing, depend, script.getScriptId());
					nodes.add(depend);
				}
				nodes.add(script.getScriptId());
			}
		}

		while (!nodes.isEmpty()) {
			traverse(orderList, nodes, outgoing, incoming);
		}

		return orderList;

	}

	public static void main(String[] args) {
		ScriptOrderSorter sorter = new ScriptOrderSorter();
		List<VulnerabilityScript> scripts = new ArrayList<>();

		List<Integer> depend1 = new ArrayList<>();
		depend1.add(2);
		depend1.add(3);
		scripts.add(new VulnerabilityScript(1, depend1));

		List<Integer> depend2 = new ArrayList<>();
		depend2.add(4);
		scripts.add(new VulnerabilityScript(2, depend2));

		List<Integer> depend3 = new ArrayList<>();
		depend3.add(5);
		scripts.add(new VulnerabilityScript(3, depend3));

		List<Integer> result = sorter.buildOrder(scripts);
		System.out.println(result);

	}

	private void traverse(List<Integer> orderList, Set<Integer> nodes, Map<Integer, Set<Integer>> outgoing,
			Map<Integer, Set<Integer>> incoming) {
		Iterator<Integer> iterator = nodes.iterator();
		boolean isFound = false;
		while (iterator.hasNext()) {
			Integer node = iterator.next();
			Set<Integer> set = incoming.get(node);
			if (set == null || set.isEmpty()) {
				Set<Integer> childrens = outgoing.get(node);
				if (childrens != null) {
					for (Integer child : childrens) {
						incoming.get(child).remove(node);
					}
				}
				orderList.add(node);
				iterator.remove();
				isFound = true;
				break;
			}
		}
		if (!isFound)
			throw new RuntimeException("Invalid Build Order...");
	}

	private void addToMap(Map<Integer, Set<Integer>> map, Integer src, Integer dest) {
		Set<Integer> set = map.get(src);
		if (set == null) {
			set = new HashSet<>();
			map.put(src, set);
		}
		set.add(dest);
	}

}
