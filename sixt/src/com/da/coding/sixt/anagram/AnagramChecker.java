package com.da.coding.sixt.anagram;

import java.util.HashMap;
import java.util.Map;

public class AnagramChecker {
	//Time Complexity- O(2N)~ O(N)
	// Space Complexity- O(N)
	public boolean isAnagram(String first, String second) {

		if (first == null || second == null) {
			return false;
		}

		if (first.length() != second.length()) {
			return false;
		}

		Map<Character, Integer> countMap = new HashMap<>();

		for (int i = 0; i < first.length(); i++) {
			Integer count = countMap.get(first.charAt(i));
			if (count == null) {
				count = 0;
			}
			countMap.put(first.charAt(i), count + 1);
		}

		for (int i = 0; i < second.length(); i++) {
			Integer count = countMap.get(second.charAt(i));
			if (count == null) {
				return false;
			}
			count--;
			if (count == 0) {
				countMap.remove(second.charAt(i));
			} else {
				countMap.put(second.charAt(i), count);
			}
		}

		return true;
	}
	
	public static void main(String[] args) {
		AnagramChecker checker= new AnagramChecker();
		System.out.println(checker.isAnagram("anagram", "nagaram"));
		System.out.println(checker.isAnagram("aab", "aba"));
		System.out.println(checker.isAnagram("aab", "ab"));
		System.out.println(checker.isAnagram("aaba", "aba"));
		System.out.println(checker.isAnagram("a", ""));
	}
}
