package com.da.coding.stack;

import java.util.LinkedList;
import java.util.Queue;

/***
 * 
 * @author Dharmendra
 *
 *         Implement Stack using Queues The problem is opposite of this post. We
 *         are given a Queue data structure that supports standard operations
 *         like enqueue() and dequeue(). We need to implement a Stack data
 *         structure using only instances of Queue and queue operations allowed
 *         on the instances.
 */
public class StackFromQueue {
	Queue<Integer> queue1 = new LinkedList<>();
	Queue<Integer> queue2 = new LinkedList<>();

	// this approach push will make sure pushed item will be always be in front
	// of the queue so pop will be easiest
	class CostlyPush2Qs {
		public CostlyPush2Qs() {
			queue1.clear();
			queue2.clear();
		}

		//Time Complexity O(n) and space complexity O(n)
		public void push(int item) {
			queue2.add(item);
			while (!queue1.isEmpty()) {
				queue2.add(queue1.poll());
			}
			Queue<Integer> temp = queue1;
			queue1 = queue2;
			queue2 = temp;
		}
		
		//Time Complexity O(1)
		public int pop() {
			return queue1.poll();
		}
	}

	// this approach item will simply be pushed at the end of the queue, so push
	// is faster but pop requires all items to be removed first
	class CostlyPop2Qs {
		public CostlyPop2Qs() {
			queue1.clear();
			queue2.clear();
		}

		//Time Complexity O(1)
		public void push(int item) {
			queue1.add(item);
		}

		//Time Complexity O(n) and Space Complexity O(n)
		public int pop() {
			while (queue1.size() > 1) {
				queue2.add(queue1.poll());
			}
			Queue<Integer> temp = queue1;
			queue1 = queue2;
			queue2 = temp;
			return queue2.poll();
		}
	}

	
	class CostlyPush1Q{
		public CostlyPush1Q() {
			queue1.clear();
		}
		
		//this approach uses only one queue and use it as a stack
		//this makes sure every push will insert the element at the front of the queue
		//Time Complexity O(n) Space Complexity(1)
		public void push(int item){
			int size= queue1.size();
			queue1.add(item);
			for (int i = 0; i < size; i++) {
				queue1.add(queue1.poll());
			}
		}
		
		//Time Complexity O(1)
		public int pop(){
			return queue1.poll();
		}
	}
	
	
	public static void main(String[] args) {
		StackFromQueue stackFromQueue = new StackFromQueue();
		CostlyPush2Qs costlyPush = stackFromQueue.new CostlyPush2Qs();
		for (int i = 1; i <= 3; i++) {
			costlyPush.push(i);
		}
		for (int i = 1; i <= 3; i++) {
			System.out.println(costlyPush.pop());
		}

		CostlyPop2Qs costlyPop = stackFromQueue.new CostlyPop2Qs();
		for (int i = 1; i <= 3; i++) {
			costlyPop.push(5 + i);
		}
		for (int i = 1; i <= 3; i++) {
			System.out.println(costlyPop.pop());
		}
		
		CostlyPush1Q costlyPush1Q= stackFromQueue.new CostlyPush1Q();
		for (int i = 0; i < 3; i++) {
			costlyPush1Q.push(10+i);
		}
		
		for (int i = 0; i < 3; i++) {
			System.out.println(costlyPush1Q.pop());
		}
	}
}
