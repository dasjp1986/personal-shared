package com.da.coding.stack;

import java.util.Scanner;
import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 * Minimum number of bracket reversals needed to make an expression balanced
Given an expression with only ‘}’ and ‘{‘. The expression may not be balanced. Find minimum number of bracket reversals to make the expression balanced.

Examples:

Input:  exp = "}{"
Output: 2
We need to change '}' to '{' and '{' to
'}' so that the expression becomes balanced, 
the balanced expression is '{}'

Input:  exp = "{{{"
Output: Can't be made balanced using reversals

Input:  exp = "{{{{"
Output: 2 

Input:  exp = "{{{{}}"
Output: 1 

Input:  exp = "}{{}}{{{"
Output: 3
 */
public class BalancingExpWithReversals {
	private static final char OPEN='{';
	private static final char CLOSE='}';
	
	public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		String expression= scn.next();
		System.out.println(findMinReversals(expression));
		scn.close();
	}

	//Time Complexity O(n) and Space Complexity O(n)
	private static int findMinReversals(String expression) {
		int minCount=0;
		Stack<Character> stack= new Stack<>();
		
		//if string contains odd number of brackets, expression is invalid
		if(expression== null || expression.length()%2!=0)
			return minCount;
		
		for (int i = 0; i < expression.length(); i++) {
			if(expression.charAt(i)==CLOSE){
				if(stack.isEmpty()){
					minCount++;
					stack.push(OPEN);
				}else{
					stack.pop();
				}
			}else{
				stack.push(OPEN);
			}
		}
		//if stack is non empty then for every two brackets of type [, you need half no. of reversals
		minCount+= stack.size()/2;
		return minCount;
	}
}
