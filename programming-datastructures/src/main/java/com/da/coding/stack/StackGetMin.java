package com.da.coding.stack;

import java.util.Stack;

/**
 * 
 * @author Dharmendra
 *
 *         Design a stack that supports getMin() in O(1) time and O(1) extra
 *         space An approach that uses O(1) time and O(n) extra space
 *         
 *         Stack does not the contain the actual input values but calculated values
 */
public class StackGetMin {
	Stack<Integer> stack= new Stack<>();
	int min;
	
	public void push(int item){
		if(stack.isEmpty()){
			stack.add(item);
			min= item;
		}else{
			if(item>=min){
				stack.push(item);
			}else{
				stack.push(2*item-min);
				min= item;
			}
		}
	}
	
	public int pop(){
		int item= stack.pop();
		if(item<min){
			min= 2*min- item;
		}
		return item;
	}
	
	public int getMin(){
		return min;
	}
	
	public static void main(String[] args) {
		StackGetMin problem= new StackGetMin();
		problem.push(4);
		problem.push(3);
		problem.push(2);
		problem.push(1);
		System.out.println("Min: "+problem.getMin());
		
		System.out.println("popped..."+problem.pop());
		System.out.println("Min: "+problem.getMin());
		
		System.out.println("popped..."+problem.pop());
		System.out.println("Min: "+problem.getMin());
		
		System.out.println("popped..."+problem.pop());
		System.out.println("Min: "+problem.getMin());
	}
}
