package com.da.coding.stack;

import java.util.Arrays;

/**
 * 
 * @author Dharmendra
 *
 *         Implement two stacks in an array Create a data structure twoStacks
 *         that represents two stacks. Implementation of twoStacks should use
 *         only one array, i.e., both stacks should use the same array for
 *         storing elements. Following functions must be supported by twoStacks.
 * 
 *         push1(int x) �> pushes x to first stack push2(int x) �> pushes x to
 *         second stack
 * 
 *         pop1() �> pops an element from first stack and return the popped
 *         element pop2() �> pops an element from second stack and return the
 *         popped element
 * 
 *         Implementation of twoStack should be space efficient.
 * 
 *         http://www.geeksforgeeks.org/implement-two-stacks-in-an-array/
 */
public class TwoStacksFromAnArray {
	int s1;
	int s2;

	int[] array;

	public TwoStacksFromAnArray(int arrayLength) {
		this.array = new int[arrayLength];
		s1 = 0;
		s2 = arrayLength-1;
	}

	public void pushS1(int num) {
		if (s1 <= s2) {
			array[s1] = num;
			s1++;
		} else {
			System.out.println("Array is already full");
		}

	}

	public void pushS2(int num) {
		if (s2>= s1) {
			array[s2] = num;
			s2--;
		} else {
			System.out.println("Array is already full");
		}
	}

	public int popS1() {
		s1--;
		return array[s1];
	}

	public int popS2() {
		s2++;
		return array[s2];
	}

	public static void main(String[] args) {
		TwoStacksFromAnArray problem = new TwoStacksFromAnArray(4);
		problem.pushS1(1);
		problem.pushS2(4);
		problem.pushS1(3);
		problem.pushS1(2);
		System.out.println(Arrays.toString(problem.array));
		problem.pushS1(5);
		System.out.println(problem.popS1());
		System.out.println(Arrays.toString(problem.array));
		problem.pushS2(5);
		System.out.println(Arrays.toString(problem.array));
	}
}
