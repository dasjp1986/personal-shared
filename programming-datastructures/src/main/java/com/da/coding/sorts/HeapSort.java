package com.da.coding.sorts;

/**
 * 
 *  * it is based on the Heap Datastructure which can be represented in an array such that
 * parent node A(i)>=A(2i) and A(i)>=A(2i+1) where 2i and 2i+1 represents left and right child
 * 
 * Heap represents nearly complete binary tree
 */
import java.util.Arrays;

public class HeapSort {

	Integer[] heap;
	Integer heapSize;

	public HeapSort(Integer[] input) {
		heap = input;
		heapSize = input.length;
	}

	public static void main(String[] args) throws Exception {
		Integer[] input = { 10,15,6,1,9,11,18 };
		HeapSort sorting = new HeapSort(input);
		sorting.buildMaxHeap();
		System.out.println(Arrays.toString(sorting.heap));
		for (int i = 0; i < 3; i++) {
			System.out.println(sorting.extractMax());
		}
		
//		System.out.println(Arrays.toString(sorting.sort()));
//		sorting.increaseKey(sorting.heapSize - 1, 10);
//		sorting.buildMaxHeap();
//		System.out.println("Extract Max :" + sorting.extractMax());
//		System.out.println("Extract Max :" + sorting.extractMax());
//		System.out.println("Extract Max :" + sorting.extractMax());
//		System.out.println("Extract Max :" + sorting.extractMax());
		
	}

	// Time Complexity O(n)
	public Integer[] buildMaxHeap() {
		if (heap != null) {
			// all the elements post input.length/2 will be leaf nodes which are
			// max heap by default
			for (int parent = heapSize / 2; parent >= 0; parent--) {
				maxHeapify(parent, heapSize);
			}
		}
		return heap;
	}

	// Time Complexity O(Logn)
	private void maxHeapify(Integer parent, int length) {
		Integer maxIndex = parent;
		Integer left = 2 * parent + 1;
		Integer right = 2 * parent + 2;

		if (left < length && heap[left] > heap[maxIndex]) {
			maxIndex = left;
		}

		if (right < length && heap[right] > heap[maxIndex]) {
			maxIndex = right;
		}

		if (maxIndex != parent) {
			swap(parent, maxIndex);

			parent = parent / 2;
			maxHeapify(parent, length);
		}
	}

	private void swap(Integer parent, Integer maxIndex) {
		Integer temp = heap[maxIndex];
		heap[maxIndex] = heap[parent];
		heap[parent] = temp;
	}

	// Time Complexity O(nLogn)
	public Integer[] sort() {
		heap = buildMaxHeap();
		int n = heapSize - 1;

		while (n > 0) {
			swap(0, n);
			maxHeapify(0, n);
			n--;
		}
		return heap;
	}

	public Integer findMax() {
		return heap[0];
	}

	public Integer extractMax() throws Exception {
		if (heapSize <= 0)
			throw new Exception("Unable to extract max elements as heap is already empty...");
		int root = 0;
		int n = heapSize - 1;

		Integer max = heap[root];
		swap(n, root);
		heapSize--;
		maxHeapify(root, heapSize);
		return max;
	}

	public void increaseKey(Integer key, Integer value) throws Exception {
		if (key >= heapSize) {
			throw new Exception("Key is more than heapSize : " + heapSize);
		}
		heap[key] = heap[key] + value;
		maxHeapify(key, heapSize);
	}
	

}
