package com.da.coding.sorts;

import java.util.Arrays;

/**
 * 
 * @author dagraw1
 * Every swap starting from index 1 will make sure it is swapped with all previous elements until
 * it is sorted
 * 
 * Example- 3,4,1,5,2
 * iteration 1: 3,4,1,5,2
 * iteration 2: 1,3,4,5,2
 * iteration 3: 1,3,4,5,2
 * iteration 4: 1,2,3,4,5
 */

//Time Complexity O(n^2)
//Space Complexity O(1) only for swap
public class InsertionSort {
	public Integer[] sort(Integer[] input){
		if(input==null)
			return null;
		
		for (int i = 1; i < input.length; i++) {
			int j=i;
			while(j>0 && input[j]<input[j-1]){
				swap(input, j, j-1);
				j--;
			}
		}
		
		return input;
	}

	private void swap(Integer[] input, int next, int prev) {
		int temp= input[prev];
		input[prev]= input[next];
		input[next]= temp;
	}
	
	public static void main(String[] args) {
		InsertionSort insertionSorting= new InsertionSort();
		Integer[] input={3,4,1,5,2};
		System.out.println(Arrays.toString(insertionSorting.sort(input)));
	}
}
