package com.da.coding.sorts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingPerf {
	public static void main(String[] args) {

		int n=Short.MAX_VALUE+100000;
		System.out.println("Input Value : "+n);
		List<Integer> inputList= new ArrayList<>();
		for (int i = 1; i <= n; i++) {
			inputList.add(i);
		}
		
		Collections.shuffle(inputList);
		
		Integer[] inputArray= inputList.toArray(new Integer[inputList.size()]);
		
		//System.out.println("Initial Input : "+Arrays.toString(inputArray));
		System.out.println("-----------------Testing Merge Sort for n : "+n);
		
		MergeSort mergeSort= new MergeSort();
		long startTime= System.currentTimeMillis();
		Integer[] output= mergeSort.sort(inputArray, 0, inputArray.length-1);
		//System.out.println("Sorted "+Arrays.toString(output));
		System.out.println("n : "+n+" Time Taken " +(System.currentTimeMillis()-startTime)+ " ms");
		
		
		System.out.println("-----------------Testing Insertion Sort n : "+n);
		InsertionSort insertionSort= new InsertionSort();
		startTime= System.currentTimeMillis();
		output= insertionSort.sort(inputArray);
		//System.out.println("Sorted "+Arrays.toString(output));
		System.out.println("n : "+n+" Time Taken " +(System.currentTimeMillis()-startTime)+ " ms");
		
		System.out.println("-----------------Testing Heap Sort n : "+n);
		HeapSort heapSort= new HeapSort(inputArray);
		startTime= System.currentTimeMillis();
		output= heapSort.sort();
		//System.out.println("Sorted "+Arrays.toString(output));
		System.out.println("n : "+n+" Time Taken " +(System.currentTimeMillis()-startTime)+ " ms");
	
	}
}
