package com.da.coding.sorts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Dharmendra Time Complexity T(n)= nLog(n) Space Complexity O(n)
 */
public class MergeSort {
	public static void main(String[] args) {

		int n = Short.MAX_VALUE;

		List<Integer> inputList = new ArrayList<>();
		for (int i = 1; i <= n; i++) {
			inputList.add(i);
		}

		Collections.shuffle(inputList);

		Integer[] inputArray = inputList.toArray(new Integer[inputList.size()]);

		System.out.println("Initial Input : " + Arrays.toString(inputArray));
		MergeSort sorting = new MergeSort();
		long startTime = System.currentTimeMillis();
		Integer[] output = sorting.sort(inputArray, 0, inputArray.length - 1);
		System.out.println("Sorted " + Arrays.toString(output));
		System.out.println("n : " + n + " Time Taken " + (System.currentTimeMillis() - startTime) + " ms");

	}

	public Integer[] sort(Integer[] inputArray, int start, int end) {
		if (start == end)
			return new Integer[] { inputArray[start] };
		
		int mid = (start + end) / 2;

		Integer[] left = sort(inputArray, start, mid);
		Integer[] right = sort(inputArray, mid + 1, end);
		return merge(left, right);

	}

	private Integer[] merge(Integer[] left, Integer[] right) {
		Integer[] output = new Integer[left.length + right.length];
		int i = 0, j = 0, k = 0;
		while (i < left.length && j < right.length) {
			if (left[i] < right[j]) {
				output[k] = left[i];
				i++;
			} else {
				output[k] = right[j];
				j++;
			}
			k++;
		}

		// append the remaining left sub array
		while (i < left.length) {
			output[k] = left[i];
			i++;
			k++;
		}

		// append the remaining right sub array
		while (j < right.length) {
			output[k] = right[j];
			j++;
			k++;
		}
		return output;
	}
}
