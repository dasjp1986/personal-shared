package com.da.coding.questions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

/**
 * 
 * @author Dharmendra
 *
 *         Merge Overlapping Intervals Given a set of time intervals in any
 *         order, merge all overlapping intervals into one and output the result
 *         which should have only mutually exclusive intervals. Let the
 *         intervals be represented as pairs of integers for simplicity. For
 *         example, let the given set of intervals be {{1,3}, {2,4}, {5,7},
 *         {6,8} }. The intervals {1,3} and {2,4} overlap with each other, so
 *         they should be merged and become {1, 4}. Similarly {5, 7} and {6, 8}
 *         should be merged and become {5, 8}
 */
public class MergeOverlappingIntervals {

	//First we are sorting all the pairs based on the first index in each pairs
	// in single scan of all elements we can find overlapping pairs
	//Time Complexity O(nlogn+n)= O(nlogn)
	public List<Pair> overlappedIntervals(List<Pair> pairs) {
		pairs = sortPairs(pairs);
		ListIterator<Pair> listIterator = pairs.listIterator();
		Pair prev = listIterator.next();
		while (listIterator.hasNext()) {
			int start = 0, end = 0;
			Pair next = listIterator.next();
			if (prev.i <= next.i && prev.j >= next.i) {
				start = prev.i;
				if (prev.j < next.j) {
					end = next.j;
				} else {
					end = prev.j;
				}
				listIterator.remove();
				prev.i = start;
				prev.j = end;
			}else{
				prev= next;
			}
		}
		return pairs;
	}

	private List<Pair> sortPairs(List<Pair> pairs) {
		Collections.sort(pairs, new Comparator<Pair>() {

			@Override
			public int compare(Pair o1, Pair o2) {
				return o1.i.compareTo(o2.i);
			}
		});
		return pairs;
	}

	public static void main(String[] args) {
		MergeOverlappingIntervals problem = new MergeOverlappingIntervals();
		List<Pair> input = new ArrayList<>();
		input.add(problem.new Pair(1, 5));
		input.add(problem.new Pair(3, 4));
		input.add(problem.new Pair(2, 6));

		input = problem.overlappedIntervals(input);
		System.out.println(input);

	}

	class Pair {
		Integer i;
		Integer j;

		public Pair(Integer i, Integer j) {
			super();
			this.i = i;
			this.j = j;
		}

		@Override
		public String toString() {
			return "Pair [i=" + i + ", j=" + j + "]";
		}

	}
}
