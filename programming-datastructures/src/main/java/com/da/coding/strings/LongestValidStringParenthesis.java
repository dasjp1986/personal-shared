package com.da.coding.strings;

import java.util.Scanner;
import java.util.Stack;

/**
 * 
 * @author Dharmendra
 * 
 *         Length of the longest valid substring Given a string consisting of
 *         opening and closing parenthesis, find length of the longest valid
 *         parenthesis substring.
 */
public class LongestValidStringParenthesis {

	//Time Complexity O(n) and Space Complexity O(n)
	public int longestString(String str) {
		int count = 0;

		Stack<Character> stack = new Stack<>();
		int i = 0;
		while (i < str.length()) {
			if (str.charAt(i)== '(')
				stack.push(str.charAt(i));
			else if (str.charAt(i) == ')' && !stack.isEmpty()) {
				stack.pop();
				count += 2;
			}
			i++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		LongestValidStringParenthesis problem= new LongestValidStringParenthesis();
		Scanner scn= new Scanner(System.in);
		System.out.println("Enter a string : ");
		System.out.println(problem.longestString(scn.next()));
		scn.close();
	}
}
