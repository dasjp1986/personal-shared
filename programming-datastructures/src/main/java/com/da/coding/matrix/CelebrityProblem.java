package com.da.coding.matrix;

/**
 * 
 * @author Dharmendra
 *
 *
 *         The Celebrity Problem In a party of N people, only one person is
 *         known to everyone. Such a person may be present in the party, if yes,
 *         (s)he doesn�t know anyone in the party. We can only ask questions
 *         like �does A know B? �. Find the stranger (celebrity) in minimum
 *         number of questions.
 * 
 *         We can describe the problem input as an array of numbers/characters
 *         representing persons in the party. We also have a hypothetical
 *         function HaveAcquaintance(A, B) which returns true if A knows B,
 *         false otherwise. How can we solve the problem.
 * 
 *         hypothetical function HaveAcquaintance(A,B) can be represented as a
 *         lookup in n*n matrix where A can be row index and B can be column
 *         index
 * 
 *         for ex- 2 is celebrity here {{0,0,1,0}, {0,0,1,0}, {0,0,0,0},
 *         {0,0,1,0} }
 */
public class CelebrityProblem {
	int n = 4;
	int[][] matrix = { { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 0, 0, 0, 0 }, { 0, 0, 1, 0 } };

	boolean haveAcquaintance(int i, int j) {
		if (matrix[i][j] == 1)
			return true;
		return false;
	}

	public int findCelebrity() {
		int i = 0;
		int j = n - 1;
		// find the potential celebrity
		while (i < j) {
			// if i knows j then i can not be the celebrity
			if (haveAcquaintance(i, j)) {
				i++;
			} else {
				j--;
			}
		}
		int celebrity = i;
		i = 0;
		// check if fails to be a celebrity
		while (i < n) {
			// either he knows any of the other person or if there is at least
			// one person who doesn't know him
			if(i!=celebrity){
				if(!haveAcquaintance(i, celebrity) || haveAcquaintance(celebrity, i)){
					return -1;
				}
			}
			i++;
		}
		return celebrity;
	}
	
	public static void main(String[] args) {
		CelebrityProblem problem= new CelebrityProblem();
		System.out.println(problem.findCelebrity());
	}
}
