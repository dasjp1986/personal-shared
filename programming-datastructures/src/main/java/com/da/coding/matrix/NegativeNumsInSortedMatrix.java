package com.da.coding.matrix;
/**
 * 
 * @author Dharmendra
 * find no. of negative numbers in given matrix
 * each rows in matrix are sorted
 * each columns in matrix are sorted
 * | -3 -1 1 |
 * | -2  1 2 |
 *  answer= 3
 */
public class NegativeNumsInSortedMatrix {
	
	//Time Complexity O(m+n)
	public int findNegativeNums(int[][] matrix, int n, int m){
		int count=0;
		int i=0;
		int j=m-1;
		
		while(j>=0 && i<n){
			if(matrix[i][j]<0){
				count= count+ j+1;
				i++;
			}else{
				j--;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		NegativeNumsInSortedMatrix problem= new NegativeNumsInSortedMatrix();
		int n=2, m=3;
		int[][] matrix1= {{-3, -1, 1},
						  {-2,  1, 2}
						};
		System.out.println(problem.findNegativeNums(matrix1, n, m));
		
		n=2;
		m=3;
		int[][] matrix2= {{-12, -11, -10},
						  {-11, -10, -9}
						};
		System.out.println(problem.findNegativeNums(matrix2, n, m));
	}
}
