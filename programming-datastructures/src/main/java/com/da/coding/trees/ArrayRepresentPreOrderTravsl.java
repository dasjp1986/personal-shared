package com.da.coding.trees;

import java.util.Scanner;
import java.util.Stack;

/**
 * 
 * @author dagraw1
 *
 * Check if a given array can represent Preorder Traversal of Binary Search Tree
Given an array of numbers, return true if given array can represent preorder traversal of a Binary Search Tree, else return false. Expected time complexity is O(n).

Examples:

Input:  pre[] = {2, 4, 3}
Output: true
Given array can represent preorder traversal
of below tree
    2
     \
      4
     /
    3

Input:  pre[] = {2, 4, 1}
Output: false
Given array cannot represent preorder traversal
of a Binary Search Tree.

Input:  pre[] = {40, 30, 35, 80, 100}
Output: true
Given array can represent preorder traversal
of below tree
     40
   /   \
 30    80 
  \      \
  35     100 


Input:  pre[] = {40, 30, 35, 20, 80, 100}
Output: false
Given array cannot represent preorder traversal
of a Binary Search Tree.

 */
public class ArrayRepresentPreOrderTravsl {
	public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		
		int testCases= scn.nextInt();
		for (int i = 0; i < testCases; i++) {
			int n= scn.nextInt();
			int[] inputArray= new int[n];
			for (int j = 0; j < n; j++) {
				inputArray[j]= scn.nextInt();
			}
			System.out.println(isPreOrderTraversal(inputArray));
		}
		
		scn.close();
	}

	//Time Complexity O(n) and Space Complexity O(n)
	private static int isPreOrderTraversal(int[] inputArray) {
		int res=1;
		int root= Integer.MIN_VALUE;
		
		Stack<Integer> stack= new Stack<>();
		
		for (int i = 0; i < inputArray.length; i++) {
			if(inputArray[i]<root){
				return 0;
			}
			//you have found the right child so just previous to it was root
			while (!stack.isEmpty() && inputArray[i]>stack.peek()) {
				root= stack.pop();
			}
			//current node behave like root
			stack.push(inputArray[i]);
		}
		
		return res;
	}
}
