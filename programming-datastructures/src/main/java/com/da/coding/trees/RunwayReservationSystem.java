package com.da.coding.trees;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         Runway Reservation System 
 *         	• Airport with single (very busy) runway
 *         (Boston 6 → 1) • “Reservations” for future landings 
 *         • When plane lands, it is removed from set of pending events 
 *         • Reserve req specify “requested landing time” t 
 *         • Add t to the set if no other landings
 *         are scheduled within k minutes either way. Assume that k can vary. –
 *         else error, don’t schedule
 * 
 *         Example 
 *         37 41 46 49 56 time (mins) now x x x x 
 *         Figure 1: Runway Reservation System Example Let R denote the reserved landing times: R
 *         = (41, 46, 49, 56) and k = 3
 * 
 *         Request for time: 44 not allowed (46 ∈ R) 53 OK 20 not allowed
 *         (already past) | R |= n Goal: Run this system efficiently in O(lg n)
 *         time
 * 
 *         Can we do better? • Sorted list: Appending and sorting takes Θ(n lg
 *         n) time. However, it is possible to insert new time/plane rather than
 *         append and sort but insertion takes Θ(n) time. A k minute check can
 *         be done in O(1) once the insertion point is found. • Sorted array: It
 *         is possible to do binary search to find place to insert in O(lg n)
 *         time. Using binary search, we find the smallest i such that R[i] ≥ t,
 *         i.e., the next larger element. We then compare R[i] and R[i − 1]
 *         against t. Actual insertion however requires shifting elements which
 *         requires Θ(n) time. • Unsorted list/array: k minute check takes O(n)
 *         time. • Min-Heap: It is possible to insert in O(lg n) time. However,
 *         the k minute check will require O(n) time. • Dictionary or Python
 *         Set: Insertion is O(1) time. k minute check takes Ω(n) time
 *         
 *         
 */
public class RunwayReservationSystem {
	
	Node root;
	
	//Time Complexity T(n)= O(Logn)
	//But in case of skewed binary tree it may be O(n)
	//Better solution can use Balanced BST
	public boolean reserve(Double flightTime, Integer minuteCheck){
		Node node= new Node(null, flightTime);
		if(root== null){
			root= node;
		}else{
			Node temp= root;
			Node parent= root;
			boolean isLeft=false;
			while(temp!=null){
				parent= temp;
				if(flightTime<temp.time){
					if(flightTime+minuteCheck<=temp.time){
						temp= temp.left;
						isLeft= true;
					}
					else
						return false;
				}else{
					if(flightTime-minuteCheck>=temp.time){
						temp= temp.right;
						isLeft= false;
					}
					else
						return false;
				}
			}
			node.parent= parent;
			if(isLeft){
				updateRank(node.parent);
				parent.left= node;
			}
			else{
				updateRank(node.parent);
				parent.right= node;
			}
		}
		
		return true;
	}
	
	private void updateRank(Node parent) {
		if(parent!=null){
			parent.rank++;
			updateRank(parent.parent);
		}
	}



	public void printInOrder(Node node){
		if(node!=null){
			printInOrder(node.left);
			System.out.println(node);
			printInOrder(node.right);
		}
	}
	
	public static void main(String[] args) {
		RunwayReservationSystem reservationSystem= new RunwayReservationSystem();
		Scanner scn= new Scanner(System.in);
		System.out.println("Enter no. of schedules");
		int n= scn.nextInt();
		System.out.println("Enter K minute check between flights");
		int minuteCheck= scn.nextInt();
		System.out.println("Enter Schedules");
		Double flightTime;
		for (int i = 0; i < n; i++) {
			flightTime= scn.nextDouble();
			System.out.println(reservationSystem.reserve(flightTime, minuteCheck));
		}
		reservationSystem.printInOrder(reservationSystem.root);
		scn.close();
	}
	
	class Node{
		Node left;
		Node right;
		Node parent;
		Double time;
		Integer rank;
		
		public Node(Node parent, Double time) {
			this.parent= parent;
			this.time= time;
			this.rank=1;
		}

		@Override
		public String toString() {
			return "Node [time=" + time + ", rank=" + rank + "]";
		}

	}
}
