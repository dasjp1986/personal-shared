package com.da.coding.trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class BinaryTree {
	private Node root;

	public Node getRoot() {
		return root;
	}

	public void addNode(int value) {
		if (root == null) {
			root = new Node(value);
		} else {
			Node node = new Node(value);
			Queue<Node> queue = new LinkedList<>();
			queue.add(root);
			while (!queue.isEmpty()) {
				Node temp = queue.poll();
				if (temp.getLeft() == null) {
					temp.setLeft(node);
					break;
				} else if (temp.getRight() == null) {
					temp.setRight(node);
					break;
				} else {
					queue.add(temp.getLeft());
					queue.add(temp.getRight());
				}

			}
		}
	}

	public void bfsTrversal() {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			Node temp = queue.poll();
			System.out.print(temp.getValue() + " ");
			if (temp.getLeft() != null)
				queue.add(temp.getLeft());
			if (temp.getRight() != null)
				queue.add(temp.getRight());
		}
	}

	public void dfsTraversal(Node node) {
		if (node != null) {
			System.out.print(node.getValue() + " ");
			dfsTraversal(node.getLeft());
			dfsTraversal(node.getRight());
		}
	}

	// root node is visited first, then left subtree and then right sub tree
	public void preOrderRecursion(Node node) {
		if (node != null) {
			System.out.print(node.getValue() + " ");
			preOrderRecursion(node.getLeft());
			preOrderRecursion(node.getRight());
		}
	}

	// left sub tree is visited first, then root node and then right sub tree
	public void inOrderRecursion(Node node) {
		if (node != null) {
			inOrderRecursion(node.getLeft());
			System.out.print(node.getValue() + " ");
			inOrderRecursion(node.getRight());
		}
	}

	// left subtree is visited first and right subtree and then root node
	public void postOrderRecursion(Node node) {
		if (node != null) {
			postOrderRecursion(node.getLeft());
			postOrderRecursion(node.getRight());
			System.out.print(node.getValue() + " ");
		}
	}

	// iterative method uses stack instead of recursion
	public void preOrderIterative() {
		Stack<Node> stack = new Stack<>();
		stack.push(getRoot());

		while (!stack.isEmpty()) {
			Node node = stack.pop();
			System.out.print(node.getValue() + " ");
			if (node.getRight() != null)
				stack.push(node.getRight());
			if (node.getLeft() != null)
				stack.push(node.getLeft());
		}
	}

	// if we reverse the post order traversal and closely match with pre-order
	// traversal
	// we see there is only difference of right subtree being visited before
	// left subtree
	// pre-order iterative traversal can be reused by traversing right subtree
	// before left subtree
	public void postOrderIterative() {
		Stack<Node> stack = new Stack<>();
		Stack<Node> output = new Stack<>();

		stack.push(getRoot());
		while (!stack.isEmpty()) {
			Node node = stack.pop();
			output.push(node);

			if (node.getLeft() != null) {
				stack.push(node.getLeft());
			}

			if (node.getRight() != null) {
				stack.push(node.getRight());
			}
		}
		while (!output.isEmpty()) {
			System.out.print(output.pop() + " ");
		}
	}

	public void InOrderIterative() {
		Stack<Node> stack = new Stack<>();
		Node current = getRoot();
		boolean isRun = true;
		while (isRun) {
			while (current != null) {
				stack.push(current);
				current = current.getLeft();
			}
			if (current == null && !stack.isEmpty()) {
				Node node = stack.pop();
				System.out.print(node.getValue() + " ");
				current = node.getRight();
			}

			if (current == null && stack.isEmpty())
				isRun = false;
		}
	}

	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		for (int i = 0; i < n; i++) {
			tree.addNode(scn.nextInt());
		}
		scn.close();
		System.out.println("\nTraversing BFS...");
		tree.bfsTrversal();
		System.out.println("\nTraversing DFS...");
		tree.dfsTraversal(tree.getRoot());
		System.out.println("\nPre-Order Traversal...");
		tree.preOrderRecursion(tree.getRoot());
		System.out.println("\nPre-Order Traversal Iterative...");
		tree.preOrderIterative();
		System.out.println("\nIn-Order Traversal...");
		tree.inOrderRecursion(tree.getRoot());
		System.out.println("\nIn-Order Traversal Iterative...");
		tree.InOrderIterative();
		System.out.println("\nPost-Order Traversal...");
		tree.postOrderRecursion(tree.getRoot());
		System.out.println("\nPost-Order Traversal Iterative...");
		tree.postOrderIterative();
	}
}
