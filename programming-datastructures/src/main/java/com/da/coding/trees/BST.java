package com.da.coding.trees;

import java.util.Scanner;

public class BST {
	Node root;
	
	public BST() {

	}
	
	public void addNode(Node node){
		if(root==null)
			root= node;
		else{
			Node temp= root;
			Node parent= root;
			boolean isLeft=true;
			while(temp!=null){
				parent= temp;
				if(node.getValue()>=temp.getValue()){
					temp= temp.getRight();
					isLeft= false;
				}else{
					temp= temp.getLeft();
					isLeft= true;
				}
			}
			if(isLeft){
				parent.setLeft(node);
			}else{
				parent.setRight(node);
			}
		}
	}
	
	public void inOrderTraversal(Node node){
		if(node!=null){
			inOrderTraversal(node.getLeft());
			System.out.println(node.getValue());
			inOrderTraversal(node.getRight());
		}
	}
	
	public static void main(String[] args) {
		BST bst= new BST();
		Scanner scn= new Scanner(System.in);
		int n= scn.nextInt();
		for (int i = 0; i < n; i++) {
			bst.addNode(new Node(scn.nextInt()));
		}
		bst.inOrderTraversal(bst.root);
		scn.close();
	}
}
