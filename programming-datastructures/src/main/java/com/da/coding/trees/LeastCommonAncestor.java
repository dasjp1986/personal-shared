package com.da.coding.trees;

import java.util.Scanner;
import java.util.Stack;

public class LeastCommonAncestor {
    BinaryTree tree;
    
    public LeastCommonAncestor(BinaryTree tree) {
        this.tree= tree;
    }
    
    public int findLca(int v1, int v2){
        
        Stack<Integer> path1= findPath(v1, tree.getRoot());
        Stack<Integer> path2= findPath(v2, tree.getRoot());
        int leastNode=-1;
        if(path1!=null && path2!=null && !path1.isEmpty() && !path2.isEmpty()){
            for (int i = 0; i < path1.size() && i<path2.size(); i++) {
                int p1= path1.pop();
                int p2= path2.pop();
                if(p1== p2){
                    leastNode= p1;
                }else
                    break;
            }
        }
            
        return leastNode;
    }

    private Stack<Integer> findPath(int value, Node node) {
        Stack<Integer> path= null;
        if(node==null)
            return null;
        if(node.getValue()==value){
            path= new Stack<>();
            path.push(value);
            return path;
        }
        
        path= findPath(value, node.getLeft());
        if(path!=null){
            path.push(node.getValue());
            return path;
        }
        
        path= findPath(value, node.getRight());
        if(path!=null){
            path.push(node.getValue());
            return path;
        }
        
        return null;
    }
    
    public static void main(String[] args) {
        BinaryTree tree= new BinaryTree();
        Scanner scn= new Scanner(System.in);
        int n= scn.nextInt();
        for (int i = 0; i < n; i++) {
            tree.addNode(scn.nextInt());
        }
        LeastCommonAncestor problem= new LeastCommonAncestor(tree);
        System.out.println("Enter Nodes to find anchestor");
        System.out.println(problem.findLca(scn.nextInt(), scn.nextInt()));
        scn.close();
    }
}
