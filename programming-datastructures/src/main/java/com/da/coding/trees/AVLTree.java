package com.da.coding.trees;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 * 
 * 	************ WRONG CODE, NEED TO CORRECT *****************
 *
 *  AVL is a special Binary Search Tree which is balanced in such a way that
 *  |h(left_subtree)-h(right_subtree)| <=1
 *  here h represents height of the subtree- longest path from root to the leaf node
 *  
 *  An element can be inserted similar to insertion in BST, after that AVL violation can be checked up to the root node
 *  from last added element, if there is a violation- left or right rotation can be done until violations up to root node are
 *  resolved
 *  
 *  pre-order traversal must not change after left or right rotation
 *  
 *  Left Rotation
 *  	X
 *     / \
 *    A   Y
 *    	 / \
 *    	B   C
 *    
 *   pre-order traversal
 *   A X B Y C 
 *   Steps
 *   - move right child Y to the left and make it as parent node
 *   - make X as left child of new root Y
 *   - make left child of Y as right child of X
 *   - do not disturb the other nodes
 *   
 *   	Y
 *     / \
 *    X   C
 *   / \
 *  A   B
 *  
 *   pre-order traversal
 *   A X B Y C
 *   
 *  Right Rotation
 *  Steps
 *  - move left child X to the right and make it as parent node
 *  - make parent node Y of the left child X as right node
 *  - make right node B of node X as left child of Y 
 *  - do not disturb other nodes
 *  
 *   	X
 *     / \ 
 *    A   Y
 *    	 / \
 *    	B	C
 *    pre-order traversal
 *    A X B Y C
 */
public class AVLTree {
	
	Node root;
	
	public Node addNode(int value){
		Node node= new Node(value);
		if(root==null){
			root= node;
		}else{
			Node temp= root;
			Node parent= null;
			boolean isLeft= false;
			while(temp!=null){
				parent= temp;
				if(value<temp.value){
					temp= temp.left;
					isLeft= true;
				}else{
					temp= temp.right;
					isLeft= false;
				}
			}
			node.parent= parent;
			if(isLeft){
				parent.left= node;
			}else{
				parent.right= node;
			}
			updateHeight(node);
			
		}
		return node;
	}
	
	public void inOrderTraversal(Node node){
		if(node!=null){
			inOrderTraversal(node.left);
			System.out.println(node);
			inOrderTraversal(node.right);
		}
	}
	
	public void updateHeight(Node node){
		int leftHeight=-1, rightHeight=-1;
		Node temp= node;
		while(temp!=null){
			balanceTree(temp);
			if(temp.left!=null)
				leftHeight= temp.left.height;
			if(temp.right!=null)
				rightHeight= temp.right.height;
			temp.height= Math.max(leftHeight, rightHeight)+1;
			System.out.println("updating height..."+temp);
			temp= temp.parent;
		}
		
	}
	

	private void balanceTree(Node node) {

		if (node == null)
			return;

		Boolean isLeft = checkRotation(node);
		if (isLeft == null) {
			// no rotation
			return;
		} else if (isLeft) {
			leftRotation(node);
		} else
			rightRotate(node);

		int leftHeight = -1, rightHeight = -1;
		if (node.left != null)
			leftHeight = node.left.height;
		if (node.right != null)
			rightHeight = node.right.height;
		node.height = Math.max(leftHeight, rightHeight) + 1;
		System.out.println("updating height..." + node);

		if (checkRotation(node.parent) != null) {
			if (isLeft)
				rightRotate(node);
			else
				leftRotation(node);
		}
	}

	private Boolean checkRotation(Node node) {
		if(node==null)
			return null;
		
		int leftHeight=-1, rightHeight=-1;
		if(node.left!=null)
			leftHeight= node.left.height;
		if(node.right!=null)
			rightHeight= node.right.height;

		if(leftHeight-rightHeight>1){
			//right rotation is needed
			return false;
		}else if(rightHeight- leftHeight>1){
			//left rotation is needed
			return true;
		}else{
			return null;
		}
	}

	private void leftRotation(Node node) {
		System.out.println("Running left rotation at node : "+node);
		Node right= node.right;
		
		right.parent= node.parent;
		if(right.parent== null){
			root= right;
		}else{
			right.parent.right= right;
		}
		node.right= right.left;
		node.parent= right;
		right.left= node;
		
		
	}

	private void rightRotate(Node node) {
		System.out.println("Running right rotation at node : " + node);
		Node left = node.left;

		left.parent = node.parent;
		if (left.parent == null) {
			root = left;
		} else {
			left.parent.left = left;
		}
		node.left = left.right;
		node.parent = left;
		left.right = node;
	}
	
	public static void main(String[] args) {
		AVLTree tree= new AVLTree();
		Scanner scn= new Scanner(System.in);
		System.out.println("Enter no. of nodes");
		int n= scn.nextInt();
		for (int i = 0; i < n; i++) {
			tree.addNode(scn.nextInt());
		}
		tree.inOrderTraversal(tree.root);
		scn.close();
	}

	class Node{
		Node left;
		int value;
		Node right;
		Node parent;
		int height;
		
		Node(int value){
			this.value= value;
		}

		@Override
		public String toString() {
			return "Node [value=" + value + ", height=" + height + "]";
		}

		
		
	}
}
