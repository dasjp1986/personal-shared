package com.da.coding.arrays;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 * Find the sub array with maximum sum
 * array can contain both negative and positive numbers
 * Ex- [1,-3,2,1,-1]
 * ans- [2,1] =3
 */
public class SubArrayWithMaxSum {
    
    public static void main(String[] args) {
        SubArrayWithMaxSum problem= new SubArrayWithMaxSum();
        Scanner scanner= new Scanner(System.in);
        int n= scanner.nextInt();
        int[] array= new int[n];
        for (int i = 0; i < n; i++) {
            array[i]= scanner.nextInt();
        }
        scanner.close();
        problem.maxSubArray(array);
    } 
    
    //Time Complexity O(n)
    public void maxSubArray(int[] array){
        int start=0, end=0, max=array[0], gmax=array[0];
        for (int i = 1; i < array.length; i++) {
            
            if(max+array[i]>array[i]){
                max= max+ array[i];
                if(max>gmax){
                    gmax= max;
                    end= i;
                }
            }else{
                max= array[i];
                if(max>gmax){
                    gmax= max;
                    start= i;
                    end= i;
                }
            }
        }
        //print subarray
       while(start<=end){
           System.out.println(array[start]);
           start++;
       }
    }
}
