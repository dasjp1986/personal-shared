package com.da.coding.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

/**
 * 
 * @author Dharmendra
 *
 *         Next Greater Element Given an array, print the Next Greater Element
 *         (NGE) for every element. The Next greater Element for an element x is
 *         the first greater element on the right side of x in array. Elements
 *         for which no greater element exist, consider next greater element as
 *         -1.
 * 
 *         Examples: a) For any array, rightmost element always has next greater
 *         element as -1. b) For an array which is sorted in decreasing order,
 *         all elements have next greater element as -1. c) For the input array
 *         [4, 5, 2, 25}, the next greater elements for each element are as
 *         follows.
 * 
 *         Element NGE 4 --> 5 5 --> 25 2 --> 25 25 --> -1 d) For the input
 *         array [13, 7, 6, 12}, the next greater elements for each element are
 *         as follows.
 * 
 *         Element NGE 13 --> -1 7 --> 12 6 --> 12 12 --> -1
 */
public class NextGreaterElement {

	// Time Complexity O(n) and Space Complexity O(n)
	public int[] nextGreater(int[] inputArray) {
		if (inputArray == null || inputArray.length == 0)
			return inputArray;

		Stack<Integer> stack = new Stack<>();

		// pushing last value to the stack and setting array at last index to -1
		int lastIndex = inputArray.length - 1;
		stack.push(inputArray[lastIndex]);
		inputArray[lastIndex] = -1;
		lastIndex--;

		while (lastIndex >= 0) {
			int ngr = -1;
			if (!stack.isEmpty() && inputArray[lastIndex] < stack.peek()) {
				ngr = stack.peek();
			} else {
				while (!stack.isEmpty() && stack.peek() <= inputArray[lastIndex]) {
					stack.pop();
				}
				if (!stack.isEmpty())
					ngr = stack.peek();
			}
			stack.push(inputArray[lastIndex]);
			inputArray[lastIndex] = ngr;
			lastIndex--;
		}

		return inputArray;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		int testCases = Integer.parseInt(reader.readLine());
		for (int i = 0; i < testCases; i++) {
			int n = Integer.parseInt(reader.readLine());
			int[] inputArray = new int[n];
			String[] tokens= reader.readLine().split(" ");
			for (int j = 0; j < n; j++) {
				inputArray[j] = Integer.parseInt(tokens[j]);
			}
			NextGreaterElement ngr = new NextGreaterElement();
			inputArray= ngr.nextGreater(inputArray);
			ngr.printArray(inputArray);
		}
		reader.close();

	}

	private void printArray(int[] inputArray) {
		for (int i : inputArray) {
			System.out.print(i+" ");
		}
	}

}
