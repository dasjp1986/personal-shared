package com.da.coding.histogram;

import java.util.Scanner;
import java.util.Stack;

/**
 * 
 * @author Dharmendra
 * 
 *         Largest Rectangular Area in a Histogram | Set 2 Find the largest
 *         rectangular area possible in a given histogram where the largest
 *         rectangle can be made of a number of contiguous bars. For simplicity,
 *         assume that all bars have same width and the width is 1 unit.
 * 
 *         For example, consider the following histogram with 7 bars of heights
 *         {6, 2, 5, 4, 5, 2, 6}. The largest possible rectangle possible is 12
 *         (see the below figure, the max area rectangle is highlighted in red)
 * 
 *         http://www.geeksforgeeks.org/largest-rectangle-under-histogram/
 */
public class LongestRectangeHistogram {
	public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		int n= scn.nextInt();
		int[] histogram= new int[n];
		for (int i = 0; i < n; i++) {
			histogram[i]= scn.nextInt();
		}
		scn.close();
		System.out.println(findMax(histogram, n));
	}

	//Time Complexity O(n)
	private static int findMax(int[] histogram, int n) {
		int i=0;
		Stack<Integer> stack= new Stack<>();
		int max_area=0;
		while(i<n){
			//either stack is empty or there is a bar for taller height
			if(stack.isEmpty() || histogram[stack.peek()]<=histogram[i]){
				stack.push(i);
				i++;
			}else{
				max_area = calculateArea(histogram, i, stack, max_area);
			}
		}
		//if there are still some more items on the stack
		while (!stack.isEmpty()) {
			max_area= calculateArea(histogram, i, stack, max_area);
		}
		return max_area;
	}

	private static int calculateArea(int[] histogram, int i, Stack<Integer> stack, int max_area) {
		int top;
		int area;
		top= stack.pop();
		area= histogram[top]*(stack.isEmpty()?i: i-1-stack.peek());
		if(area>max_area)
			max_area= area;
		return max_area;
	}
}
