package com.da.coding.maths;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Dharmendra 
 * 		   Given an array of integers, write a function that returns
 *         true if there is a triplet (a, b, c) that satisfies a2 + b2 = c2.
 * 
 *         Input: The first line contains 'T' denoting the number of testcases.
 *         Then follows description of testcases. Each case begins with a single
 *         positive integer N denoting the size of array. The second line
 *         contains the N space separated positive integers denoting the
 *         elements of array A.
 * 
 *         Output: For each testcase, print "Yes" or "No" whtether it is
 *         Pythagorean Triplet or not.
 * 
 *         Constraints: 1<=T<=50 1<=N<=100 1<=A[i]<=100
 * 
 *         Example: Input: 1 5 3 2 4 6 5 Output: Yes
 */
public class PhythagoreanTriplet {
	static Map<Integer, Boolean> hash= new HashMap<>();
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
		
		int testCases= Integer.parseInt(reader.readLine());
		for (int i = 0; i < testCases; i++) {
			int n= Integer.parseInt(reader.readLine());
			int[] array= new int[n];
			String[] tokens= reader.readLine().split(" ");
			for (int j = 0; j < n; j++) {
				array[j]= Integer.parseInt(tokens[j]);
				array[j]=array[j]*array[j];
				hash.put(array[j], true);
			}
			System.out.println(findTriplet(array));
		}
		
		reader.close();
	}

	private static String findTriplet(int[] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = i+1; j < array.length; j++) {
				if(hash.get(array[i]+array[j])!=null){
					return "Yes";
				}
			}
		}
		return "No";
	}
	
	
}
