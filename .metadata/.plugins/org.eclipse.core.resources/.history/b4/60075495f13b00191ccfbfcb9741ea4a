package com.da.coding.heaps;

import java.util.Scanner;

public class MergeRopes {
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			int[] nums = new int[n];
			for (int i = 0; i < n; i++) {
				nums[i] = scn.nextInt();
			}
			System.out.println(solve(n, nums));
		}
		scn.close();
	}

	private static int solve(int n, int[] ropes) {
		if (n < 2) {
			return 0;
		}

		Heap heap = new Heap(n);
		int total = 0;

		for (int i = 0; i < ropes.length; i++) {
			heap.add(ropes[i]);
		}

		while (!heap.isEmpty()) {
			int joined = heap.extractTop() + heap.extractTop();
			total += joined;
			heap.add(joined);
		}

		return total;
	}
}

class Heap {
	int[] heap;
	int index;

	public Heap(int n) {
		this.heap = new int[n];
	}

	public boolean isEmpty() {
		return index == 0;
	}

	public void add(int val) {
		if (index < heap.length) {
			heap[index++] = val;
			heapify((index - 1) / 2);

		}
	}

	public void heapify(int parent) {
		int left = 2 * parent + 1;
		int right = 2 * parent + 2;

		int swapIndex = parent;
		if (left < index && heap[left] < heap[swapIndex]) {
			swapIndex = left;
		}

		if (right < index && heap[right] < heap[swapIndex]) {
			swapIndex = right;
		}

		if (swapIndex != parent) {
			int temp = heap[parent];
			heap[parent] = heap[swapIndex];
			heap[swapIndex] = temp;

			heapify(parent);
		}

	}

	public int extractTop() {
		int top = heap[0];
		index--;
		if (index >= 0) {
			heap[0] = heap[index];
			heapifyTopDown(0);
		}
		return top;
	}

	public void heapifyTopDown(int parent) {
		int left = 2 * parent + 1;
		int right = 2 * parent + 2;

		int swapIndex = parent;
		if (left < index && heap[left] < heap[swapIndex]) {
			swapIndex = left;
		}

		if (right < index && heap[right] < heap[swapIndex]) {
			swapIndex = right;
		}

		if (swapIndex != parent) {
			int temp = heap[parent];
			heap[parent] = heap[swapIndex];
			heap[swapIndex] = temp;

			heapify(swapIndex);
		}
	}

}
