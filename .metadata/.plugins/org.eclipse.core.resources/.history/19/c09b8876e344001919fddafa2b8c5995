package com.da.coding.arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MergeIntervals {

	public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
		if (intervals == null || intervals.size() < 2) {
			return intervals;
		}

		Collections.sort(intervals, (i1, i2) -> new Integer(i1.end).compareTo(i2.end));
		ArrayList<Interval> results = new ArrayList<>();
		Interval prev = intervals.get(0);
		for (int i = 1; i < intervals.size(); i++) {
			Interval current = intervals.get(i);

			if (prev.end >= current.start) {
				results.remove(prev);
				prev = new Interval(prev.start, current.end);
				results.add(prev);
			} else {
				prev = current;
			}
		}

		return results;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		ArrayList<Interval> intervals = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			Interval interval = new Interval(scanner.nextInt(), scanner.nextInt());
			intervals.add(interval);
		}
		ArrayList<Interval> results = new MergeIntervals().merge(intervals);
		System.out.println(results);
		scanner.close();
	}

}

class Interval {
	int start;
	int end;

	Interval() {
		start = 0;
		end = 0;
	}

	Interval(int s, int e) {
		start = s;
		end = e;
	}

	@Override
	public String toString() {
		return "Interval [start=" + start + ", end=" + end + "]";
	}

}
