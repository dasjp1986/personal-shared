package com.da.coding.heaps;

import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *         There are given N ropes of different lengths, we need to connect
 *         these ropes into one rope. The cost to connect two ropes is equal to
 *         sum of their lengths. The task is to connect the ropes with minimum
 *         cost.
 * 
 *         Input: The first line of input contains an integer T denoting the
 *         number of test cases. The first line of each test case is N where N
 *         is the number of ropes. The second line of each test case contains N
 *         input L[i],length of ropes.
 * 
 *         Output: For each testcase, print the minimum cost to connect all the
 *         ropes.
 * 
 *         Constraints: 1 ≤ T ≤ 100 1 ≤ N ≤ 106 1 ≤ L[i] ≤ 106
 * 
 *         Example: Input: 1 4 4 3 2 6
 * 
 *         Output: 29
 * 
 *         Explanation: For example if we are given 4 ropes of lengths 4, 3, 2
 *         and 6. We can connect the ropes in following ways. 1) First connect
 *         ropes of lengths 2 and 3. Now we have three ropes of lengths 4, 6 and
 *         5. 2) Now connect ropes of lengths 4 and 5. Now we have two ropes of
 *         lengths 6 and 9. 3) Finally connect the two ropes and all ropes have
 *         connected.
 * 
 *         Total cost for connecting all ropes is 5 + 9 + 15 = 29. This is the
 *         optimized cost for connecting ropes. Other ways of connecting ropes
 *         would always have same or more cost. For example, if we connect 4 and
 *         6 first (we get three strings of 3, 2 and 10), then connect 10 and 3
 *         (we get two strings of 13 and 2). Finally we connect 13 and 2. Total
 *         cost in this way is 10 + 13 + 15 = 38.
 */
public class MergeRopes {
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int ts = scn.nextInt();
		for (int t = 0; t < ts; t++) {
			int n = scn.nextInt();
			int[] nums = new int[n];
			for (int i = 0; i < n; i++) {
				nums[i] = scn.nextInt();
			}
			System.out.println(solve(n, nums));
		}
		scn.close();
	}

	private static long solve(int n, int[] ropes) {
		if (n < 2) {
			return 0;
		}

		Heap heap = new Heap(n);
		long total = 0;

		for (int i = 0; i < ropes.length; i++) {
			heap.add(ropes[i]);
		}

		while (true) {
			long joined = heap.extractTop() + heap.extractTop();
			total += joined;
			if (heap.isEmpty()) {
				break;
			}
			heap.add(joined);
		}

		return total;
	}
}

class Heap {

	private long[] heap;
	private int lastIndex;
	private int capacity;

	public Heap(int capacity) {
		this.capacity = capacity;
		this.heap = new long[capacity];
	}

	public boolean isEmpty() {
		return lastIndex == 0;
	}

	// Time complexity- O(logn)
	public void add(long num) {
		if (lastIndex == capacity) {
			throw new RuntimeException("Heap is already full");
		}
		heap[lastIndex++] = num;
		heapifyUp(lastIndex - 1);
	}

	// Time complexity- O(logn)
	private void heapifyUp(int index) {
		int parent = (index - 1) / 2;

		if (heap[index] < heap[parent]) {
			long temp = heap[parent];
			heap[parent] = heap[index];
			heap[index] = temp;
			heapifyUp(parent);
		}
	}

	// Time complexity- O(logn)
	private void heapifyDown(int parent) {
		int minIndex = parent;
		if ((2 * parent + 1) < lastIndex && heap[2 * parent + 1] < heap[minIndex]) {
			minIndex = 2 * parent + 1;
		}

		if ((2 * parent + 2) < lastIndex && heap[2 * parent + 2] < heap[minIndex]) {
			minIndex = 2 * parent + 2;
		}

		if (minIndex != parent) {
			long temp = heap[parent];
			heap[parent] = heap[minIndex];
			heap[minIndex] = temp;
			heapifyDown(minIndex);
		}
	}

	public long extractTop() {
		if (lastIndex == 0) {
			throw new RuntimeException("Heap is empty");
		}

		long top = heap[0];
		heap[0] = heap[lastIndex - 1];
		lastIndex--;
		heapifyDown(0);
		return top;
	}

}
