package com.da.coding.common;

public class TreeNode {

	private int data; // node data
	private TreeNode left;
	private TreeNode right; // left and right child's reference
	// Tree node constructor

	public TreeNode(int data) {
		this.data = data;
		left = right = null;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public TreeNode getLeft() {
		return left;
	}

	public void setLeft(TreeNode left) {
		this.left = left;
	}

	public TreeNode getRight() {
		return right;
	}

	public void setRight(TreeNode right) {
		this.right = right;
	}
	
	

}
