package com.da.coding.amazon;

import java.util.LinkedList;
import java.util.Queue;

import com.da.coding.common.TreeNode;

/**
 * 
 * @author dagraw1
 *
 *         Given a Binary Tree, print the diagnol traversal of the binary tree.
 * 
 *         Consider lines of slope -1 passing between nodes. Given a Binary
 *         Tree, print all diagonal elements in a binary tree belonging to same
 *         line.
 * 
 *         Input : Root of below tree unnamed
 * 
 *         Output : Diagonal Traversal of binary tree : 8 10 14 3 6 7 13 1 4
 * 
 * 
 *         Input: The task is to complete the method which takes 1 argument,
 *         root of Binary Tree. The struct Node has a data part which stores the
 *         data, pointer to left child and pointer to right child. There are
 *         multiple test cases. For each test case, this method will be called
 *         individually.
 * 
 *         Output: The function should print out the diagonal traversal of the
 *         binary tree.
 * 
 *         Constraints: 1 <=T<= 30 1 <= Number of nodes<= 100 1 <= Data of a
 *         node<= 1000
 * 
 *         Example:
 * 
 *         Input 2 2 1 2 R 1 3 L 4 10 20 L 10 30 R 20 40 L 20 60 R
 * 
 *         Output 1 2 3 10 30 20 60 40
 * 
 *         There are two test casess. First case represents a tree with 3 nodes
 *         and 2 edges where root is 1, left child of 1 is 3 and right child of
 *         1 is 2. Second test case represents a tree with 4 edges and 5 nodes.
 */
public class DiagonalBinTreeTraversal {

	public void diagonalPrint(TreeNode root) {
		Queue<TreeNode> q = new LinkedList<>();

		TreeNode node = root;
		while (node != null) {
			q.add(node);
			node = node.getRight();
		}
		q.add(null);

		int sum = 0;
		while (!q.isEmpty()) {
			node = q.remove();
			if (node == null) {
				System.out.println("sum : " + sum);
				if (!q.isEmpty()) {
					q.add(null);
				}
				sum = 0;
			} else {
				System.out.print(node.getData() + " ");
				sum += node.getData();
				node = node.getLeft();
				while (node != null) {
					q.add(node);
					node = node.getRight();
				}

			}
		}
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(10);
		root.setLeft(new TreeNode(20));
		root.setRight(new TreeNode(30));
		root.getLeft().setLeft(new TreeNode(40));
		root.getLeft().setRight(new TreeNode(60));
		root.getRight().setLeft(new TreeNode(50));

		DiagonalBinTreeTraversal traversal = new DiagonalBinTreeTraversal();
		traversal.diagonalPrint(root);
	}

}
