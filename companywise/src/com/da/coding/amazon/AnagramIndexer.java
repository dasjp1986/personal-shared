package com.da.coding.amazon;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author dagraw1
 *
 *1.given a large file with contents like
1.CAT
2.DOG
3.TAC
4.ACT
5.GOD
6.ODG
Re-arrange it such that every line has the index of all of its anagrams
Eg Output
1.CAT 1,3,4
2.DOG 2,5,6
3.TAC 1,3,4
4.ACT 1,3,4
5.GOD 2,5,6
6.ODG 2,5,6
 */
public class AnagramIndexer {
	
	public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		int n= scn.nextInt();
		String[] words= new String[n];
		for (int i = 0; i < n; i++) {
			words[i]= scn.next();
		}
		anagrams(words);
		scn.close();
	}
	public static void anagrams(String[] words) {
		Map<Long, List<String>> map= new LinkedHashMap<>();
		for(String str: words) {
			String[] ws= str.split("\\.");
			long code= getCode(ws[1]);
			List<String> list = map.get(code);
			if(list==null) {
				list= new ArrayList<>();
				map.put(code, list);
			}
			list.add(ws[0]);
		}
		
		for(String str: words) {
			String[] ws= str.split("\\.");
			long code= getCode(ws[1]);
			List<String> list = map.get(code);
			System.out.println(str+ " "+list);
		}
	}
	
	private static Long getCode(String str) {
		long code=0;
		for (int i = 0; i < str.length(); i++) {
			int val= str.charAt(i)-'A';
			code+= (long) (val*val);
		}
		return code;
	}
}
