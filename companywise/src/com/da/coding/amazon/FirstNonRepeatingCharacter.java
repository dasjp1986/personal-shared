package com.da.coding.amazon;

/**
 * 
 * @author dagraw1
 * 
 *         Given a string, find the first non-repeating character in it. For
 *         example, if the input string is “GeeksforGeeks”, then output should
 *         be ‘f’ and if input string is “GeeksQuiz”, then output should be ‘G’.
 *
 */
public class FirstNonRepeatingCharacter {
	
	//Time O(N) and space complexity- O(256)
	public static int find(String str) {

		Integer[] array = new Integer[256];

		for (int i = 0; i < str.length(); i++) {
			if (array[str.charAt(i)] == null) {
				array[str.charAt(i)] = i;
			} else {
				array[str.charAt(i)] = -1;
			}
		}

		for (int i = 0; i < array.length; i++) {
			if (array[i] != null && array[i] != -1) {
				return array[i];
			}
		}
		return -1;

	}

	public static void main(String[] args) {
		System.out.println(find("geeksforgeeks"));
		System.out.println(find("abca"));
	}
}
