package com.da.coding.kafka.sync;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;

public class ProducerCallback implements Callback {

	private final long startTime;
	private final int key;
	private final String message;

	public ProducerCallback(long startTime, int key, String message) {
		this.startTime = startTime;
		this.key = key;
		this.message = message;
	}

	/**
	 * A callback method the user can implement to provide asynchronous handling
	 * of request completion. This method will be called when the record sent to
	 * the server has been acknowledged. Exactly one of the arguments will be
	 * non-null.
	 */
	public void onCompletion(RecordMetadata recdMetaData, Exception ex) {
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("Message published successfully to server in time : " + elapsedTime);
		if (recdMetaData != null) {
			System.out.println("key : " + key);
			System.out.println("message: " + message);
			System.out.println("partition: " + recdMetaData.partition());
			System.out.println("offset: " + recdMetaData.offset());
		}
	}

}
