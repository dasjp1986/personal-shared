package com.da.coding.kafka.sync;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class ProducerThread extends Thread {

	private final KafkaProducer<Integer, String> producer;
	private final String topic;
	private final boolean isAsync;

	public ProducerThread(String topic, boolean isAsync) {
		Properties configs = new Properties();
		configs.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
				KafkaProperties.KAFKA_SERVER_URL + ":" + KafkaProperties.KAFKA_SERVER_PORT);
		configs.setProperty(ProducerConfig.CLIENT_ID_CONFIG, "DemoProducer");
		configs.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
		configs.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		this.topic = topic;
		this.isAsync = isAsync;
		this.producer = new KafkaProducer<Integer, String>(configs);
	}

	@Override
	public void run() {
		int messageNo = 1;
		while (true) {
			String messageStr = "Message_" + messageNo;
			long startTime = System.currentTimeMillis();
			if (isAsync) {
				producer.send(new ProducerRecord<Integer, String>(topic, messageNo, messageStr),
						new ProducerCallback(startTime, messageNo, messageStr));
			} else {
				// send synchronously, get waits for message to be delievered
				try {
					producer.send(new ProducerRecord<Integer, String>(topic, messageNo, messageStr)).get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				System.out.println("published.. Message #: " + messageNo + " Message: " + messageStr);
			}
			messageNo++;
		}
	}
}
