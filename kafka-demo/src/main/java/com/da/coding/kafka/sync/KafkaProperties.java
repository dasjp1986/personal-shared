package com.da.coding.kafka.sync;

public class KafkaProperties {
	public static final String TOPIC = "DA_Topic_1";
	public static final String KAFKA_SERVER_URL = "localhost";
	public static final int KAFKA_SERVER_PORT = 9092;
	public static final int KAFKA_PRODUCER_BUFFER_SIZE = 64 * 1024;
	public static final int CONNECTION_TIMEOUT = 100000;
	public static final String TOPIC2 = "DA_Topic_2";
	public static final String TOPIC3 = "DA_Topic_3";
	public static final String CLIENT_ID = "SimpleConsumerDemoClient";

	private KafkaProperties() {
	}
}
