package com.da.coding.kafka.sync;

public class ProducerConsumerSyncTest {
	public static void main(String[] args) {
		boolean isAsync = true;
		ProducerThread producerThread = new ProducerThread("DA_Topic_1", isAsync);
		producerThread.start();

		ConsumerThread consumerThread = new ConsumerThread("DA_Topic_1", false);
		consumerThread.start();
	}
}
