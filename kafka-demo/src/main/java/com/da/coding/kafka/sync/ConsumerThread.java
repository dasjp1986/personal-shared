package com.da.coding.kafka.sync;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import kafka.utils.ShutdownableThread;
import scala.util.parsing.combinator.testing.Str;

public class ConsumerThread extends ShutdownableThread {

	private final KafkaConsumer<Integer, String> consumer;
	private final String topic;

	public ConsumerThread(String topic, boolean bool) {
		super(topic, bool);
		this.topic = topic;
		Properties configs = new Properties();
		configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
				KafkaProperties.KAFKA_SERVER_URL + ":" + KafkaProperties.KAFKA_SERVER_PORT);
		configs.put(ConsumerConfig.GROUP_ID_CONFIG, "DemoConsumer");
		configs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
		configs.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		configs.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
		configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.IntegerDeserializer");
		configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");

		this.consumer = new KafkaConsumer<Integer, String>(configs);
	}

	@Override
	public void doWork() {
		consumer.subscribe(Collections.singletonList(this.topic));
		ConsumerRecords<Integer, String> consumerRecords = consumer.poll(1000);
		for (ConsumerRecord<Integer, String> crd : consumerRecords) {
			System.out.println("key: " + crd.key() + ", value: " + crd.value() + ", partition: " + crd.partition()
					+ ", offset: " + crd.offset());
		}
	}

}
