package com.da.coding.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class MyProducer {
	public static void main(String[] args) {
		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", "127.0.0.1:9092");
		properties.setProperty("key.serializer", StringSerializer.class.getName());
		properties.setProperty("value.serializer", StringSerializer.class.getName());
		//acknowledgment 1-received by leader but not by others
		properties.setProperty("acks", "1");
		//wait for records to be sent for 1 ms, kinda time based batching
		properties.setProperty("linger.ms", "1");
		//client retry if fails to send to broker
		properties.setProperty("retries", "3");

		Producer<String, String> producer = new KafkaProducer<String, String>(properties);
		ProducerRecord<String, String> producerRecd = new ProducerRecord<String, String>("DA_Topic_Java", "1",
				"Java produced-2");
		producer.send(producerRecd);
		producer.close();
	}
}
