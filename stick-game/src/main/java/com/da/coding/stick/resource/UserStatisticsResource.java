package com.da.coding.stick.resource;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.da.coding.stick.core.service.UserStatisticsService;
import com.da.coding.stick.infra.entity.GlobalStatistics;
import com.da.coding.stick.infra.entity.UserStatistics;

@RestController
@RequestMapping(path="/userStats")
public class UserStatisticsResource {
	
	@Autowired
	UserStatisticsService userStatisticsService;
	
	@RequestMapping(method=RequestMethod.GET, path="/{userId}")
	public UserStatistics get(@PathVariable(value="userId") String userId) throws Exception{
		return userStatisticsService.get(userId);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/create")
	public UserStatistics create(@RequestBody UserStatistics gameStatistics){
		return userStatisticsService.saveUpdate(gameStatistics);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/all")
	public Collection<UserStatistics> getAll(){
		return userStatisticsService.getAll();
	}
	
	@RequestMapping(method= RequestMethod.GET, path="/global")
	public GlobalStatistics getGlobalStats(){
		return userStatisticsService.getGlobatStats();
	}
	
}
