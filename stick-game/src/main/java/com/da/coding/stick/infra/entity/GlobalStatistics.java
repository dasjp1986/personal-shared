package com.da.coding.stick.infra.entity;

public class GlobalStatistics {
	private long totalWin;
	private long totalLost;
	private double winRatio;
	private double lostRatio;
	
	public GlobalStatistics(long totalWin, long totalLost, double winRatio, double lostRatio) {
		super();
		this.totalWin = totalWin;
		this.totalLost = totalLost;
		this.winRatio = winRatio;
		this.lostRatio = lostRatio;
	}

	public long getTotalWin() {
		return totalWin;
	}

	public void setTotalWin(long totalWin) {
		this.totalWin = totalWin;
	}

	public long getTotalLost() {
		return totalLost;
	}

	public void setTotalLost(long totalLost) {
		this.totalLost = totalLost;
	}

	public double getWinRatio() {
		return winRatio;
	}

	public void setWinRatio(double winRatio) {
		this.winRatio = winRatio;
	}

	public double getLostRatio() {
		return lostRatio;
	}

	public void setLostRatio(double lostRatio) {
		this.lostRatio = lostRatio;
	}
	
	
}
