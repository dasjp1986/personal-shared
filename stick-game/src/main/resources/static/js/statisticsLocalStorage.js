TSG = window.TSG || {};

TSG.StatisticsLocalStorage = function(options) {

    var _storageKey = 'TSG.stats';
    var _stats = { userID:undefined, name:undefined, won: 0, lost: 0 };
    var score=0;
    _loadStatistics();

    /**
     * Loads statistics.
     * @return {object} statistics like { won: 0, lost: 0 }
     */
    function _loadStatistics() {
    	_stats.userID= TSG.userID;
    	_stats.name=TSG.name;
    	$.ajax({
            url: "http://localhost:8080/userStats/"+TSG.userID,
            contentType: "application/json; charset=utf-8", 
            type: "GET",
            success: function (data) {
            	if(data !='' && data != undefined){
            		_stats.won= data.won;
        			_stats.lost= data.lost;
        			TSG.score=data.won;
        			_statsChanged();
            	}
            }
        });
    }
     
    /**
	 * Stores current statistics to local storage.
	 */
    function _saveStatistics() {
    	var json= JSON.stringify(_stats);
    	$.ajax({
            url: "http://localhost:8080/userStats/create",
            data: json,
            contentType: "application/json; charset=utf-8", 
            type: "POST",
            success: function (data) {
           
            }
        });
    }

	 function _statsChanged() {
		if (options.onChange) {
			options.onChange(_getStats());
		}
	}

    function _gameFinished(won) {
        if (won) {
            _stats.won++;
        } else {
            _stats.lost++;
        }
        _saveStatistics();
        _statsChanged();
    }

    function _getStats() {
        return { won: _stats.won, lost: _stats.lost, played: _stats.won + _stats.lost };
    }

    return {
        gameFinished: _gameFinished,
        getStats: _getStats
    };
};
