package com.da.coding.consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import com.da.coding.common.SharedBuffer;
import com.da.coding.utility.MergeSortUtility;

public class Consumer implements Runnable {

	//lock on shared sorted list
	private Lock sortedLock;
	//lock on shared unsorted list
	private Lock unSortedLock;
	//index to the unsorted list being sorted
	private AtomicInteger index;
	//number of child lists in unsorted list
	private int listsCount;
	// condition on producer
	private Condition produceCondition;
	
	/***
	 * 
	 * @param sortedLock
	 * @param unSortedLock
	 * @param index
	 * @param listsCount
	 * @param produceCondition
	 */

	public Consumer(Lock sortedLock, Lock unSortedLock, AtomicInteger index, int listsCount,
			Condition produceCondition) {
		this.sortedLock = sortedLock;
		this.unSortedLock = unSortedLock;
		this.index = index;
		this.listsCount = listsCount;
		this.produceCondition = produceCondition;
	}

	@Override
	public void run() {
		SharedBuffer sharedBuffer = SharedBuffer.getInstance();
		// this the ending condition to stop consumers
		while (sharedBuffer.getSortedList().size() < listsCount) {

			int currentIndex = findIndexToBeSorted(sharedBuffer);
			// no more index left for sorting
			if (currentIndex == -1) {
				return;
			}
			processLists(currentIndex);
		}
	}

	private void processLists(int currentIndex) {
		List<Integer> toBesortedElements = new ArrayList<>(
				SharedBuffer.getInstance().getUnsortedList().get(currentIndex));
		toBesortedElements = MergeSortUtility.sort(toBesortedElements);
		try {
			sortedLock.lock();
			//System.out.println("acquired sortedLock- Consumer : " + Thread.currentThread().getName());
			List<List<Integer>> sortedList = SharedBuffer.getInstance().getSortedList();
			int diff = currentIndex - sortedList.size();
			if (diff >= 0) {
				while (diff-- > 0) {
					sortedList.add(null);
				}
				sortedList.add(toBesortedElements);
			} else {

				sortedList.set(currentIndex, toBesortedElements);
			}
		} finally {
			sortedLock.unlock();
			System.out.println("released sortedLock- Consumer : " + Thread.currentThread().getName());
		}
	}

	private int findIndexToBeSorted(SharedBuffer sharedBuffer) {
		int currentIndex = 0;
		try {
			unSortedLock.lock();
			System.out.println("acquired unsortedLock- Consumer : " + Thread.currentThread().getName());

			while (sharedBuffer.getUnsortedList().isEmpty()
					|| index.intValue() >= sharedBuffer.getUnsortedList().size()) {

				if (index.get() == listsCount) {
					return -1;
				}
				System.out.println("waiting unsortedLock- Consumer : " + Thread.currentThread().getName() + " index : "
						+ index.get() + " list size : " + sharedBuffer.getUnsortedList().size());
				produceCondition.await();
			}
			currentIndex = index.incrementAndGet();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			unSortedLock.unlock();
			System.out.println("released unsortedLock- Consumer : " + Thread.currentThread().getName());
		}
		return currentIndex;
	}

}
