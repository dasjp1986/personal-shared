package com.da.coding.common;

import java.util.ArrayList;
import java.util.List;

public class SharedBuffer {
	//list holding all unsorted child lists
	private final List<List<Integer>> unsortedList;
	//list holding all sorted child lists
	private final List<List<Integer>> sortedList;
	private static volatile SharedBuffer SINGLETON;

	private SharedBuffer() {
		super();
		this.unsortedList = new ArrayList<>();
		this.sortedList = new ArrayList<>();
	}

	/**
	 * 
	 * @return SINGLETON instance of the Shared Buffer
	 */
	public static SharedBuffer getInstance() {
		if (SINGLETON == null) {
			synchronized (SharedBuffer.class) {
				if (SINGLETON == null) {
					SINGLETON = new SharedBuffer();
				}
			}
		}

		return SINGLETON;
	}

	public List<List<Integer>> getUnsortedList() {
		return unsortedList;
	}

	public List<List<Integer>> getSortedList() {
		return sortedList;
	}
	
	public void clear() {
		unsortedList.clear();
		sortedList.clear();
	}

}
