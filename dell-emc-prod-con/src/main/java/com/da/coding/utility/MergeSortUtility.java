package com.da.coding.utility;

import java.util.ArrayList;
import java.util.List;

public class MergeSortUtility {
	public static List<Integer> sort(List<Integer> list) {
		return mergeSortInt(0, list.size() - 1, list);
	}

	private static List<Integer> mergeSortInt(int start, int end, List<Integer> list) {
		ArrayList<Integer> result = new ArrayList<>();
		if (start >= end) {
			result.add(list.get(start));
			return result;
		}

		int mid = (start + end) / 2;

		List<Integer> leftList = mergeSortInt(start, mid, list);
		List<Integer> rightList = mergeSortInt(mid + 1, end, list);

		int l = 0, r = 0;

		while (l < leftList.size() && r < rightList.size()) {
			if (leftList.get(l) < rightList.get(r)) {
				result.add(leftList.get(l++));
			} else {
				result.add(rightList.get(r++));
			}
		}

		while (l < leftList.size()) {
			result.add(leftList.get(l++));
		}

		while (r < rightList.size()) {
			result.add(rightList.get(r++));
		}

		return result;
	}
	
	public static void main(String[] args) {
		List<Integer> list= new ArrayList<>();
		list.add(10);
		list.add(3);
		list.add(5);
		list.add(7);
		list.add(1);
		
		System.out.println(sort(list));
		
	}

}
