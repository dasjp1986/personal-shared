package com.da.coding.utility;

import java.util.List;

public class CompareListElements {
	public static boolean isEqualList(List<List<Integer>> sortedList, List<List<Integer>> unsortedList) {
		if (sortedList.size() != unsortedList.size()) {
			return false;
		}
		for (int i = 0; i < unsortedList.size(); i++) {
			List<Integer> unsortedChild = unsortedList.get(i);
			List<Integer> sortedChild = sortedList.get(i);
			if (unsortedChild.size() != sortedChild.size()) {
				return false;
			}
			unsortedChild = MergeSortUtility.sort(unsortedChild);
			for (int j = 0; j < unsortedChild.size(); j++) {
				if (unsortedChild.get(j) != sortedChild.get(j)) {
					return false;
				}
			}

		}
		return true;
	}
}
