package com.da.coding.producer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import com.da.coding.common.SharedBuffer;

public class Producer implements Runnable {

	//lock on shared unsorted list
	private Lock unsortedLock;
	//number of child lists in unsorted list
	private int listsCount;
	//number of elements in each child list
	private int elementsCount;
	//max value of the element in child list
	private int maxBound;
	// condition on producer
	private Condition produceCondition;

	/***
	 * 
	 * @param unsortedLock
	 * @param listsCount
	 * @param elementsCount
	 * @param maxBound
	 * @param produceCondition
	 */
	public Producer(Lock unsortedLock, int listsCount, int elementsCount, int maxBound, Condition produceCondition) {
		this.unsortedLock = unsortedLock;
		this.listsCount = listsCount;
		this.elementsCount = elementsCount;
		this.maxBound = maxBound;
		this.produceCondition = produceCondition;
	}

	@Override
	public void run() {
		List<List<Integer>> tempResult = new ArrayList<>();
		for (int i = 0; i < listsCount; i++) {
			Random random = new Random();
			List<Integer> list = new ArrayList<>();
			for (int j = 0; j < elementsCount; j++) {
				list.add(random.nextInt(maxBound));
			}
			tempResult.add(list);
		}
		try {
			unsortedLock.lock();
			SharedBuffer.getInstance().getUnsortedList().addAll(tempResult);
			System.out.println("acquired unsortedLock- Producer : " + Thread.currentThread().getName());
			produceCondition.signal();
		} finally {
			unsortedLock.unlock();
			System.out.println("released unsortedLock- Producer : " + Thread.currentThread().getName());
		}

	}

}
