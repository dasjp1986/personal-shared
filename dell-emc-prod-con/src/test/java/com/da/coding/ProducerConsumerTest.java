package com.da.coding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import com.da.coding.common.SharedBuffer;
import com.da.coding.consumer.Consumer;
import com.da.coding.producer.Producer;
import com.da.coding.utility.CompareListElements;

public class ProducerConsumerTest {
	Lock unSortedLock = new ReentrantLock();
	Lock sortedLock = new ReentrantLock();
	Condition produceCondition = unSortedLock.newCondition();

	int loopCount = 100;
	int numInEachLoop = 10;

	AtomicInteger index = new AtomicInteger(-1);

	int producersCount = 5;
	int consumersCount = 5;

	@Test
	public void testProduceThenConsume() {

		SharedBuffer.getInstance().clear();
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		List<Future<?>> futures = new ArrayList<>();

		// starting producers
		for (int i = 0; i < producersCount; i++) {
			futures.add(executorService
					.submit(new Producer(unSortedLock, loopCount, numInEachLoop, 100, produceCondition)));
		}

		// starting consumers
		for (int i = 0; i < consumersCount; i++) {
			futures.add(executorService.submit(
					new Consumer(sortedLock, unSortedLock, index, loopCount * producersCount, produceCondition)));
		}

		// waiting for all tasks to be completed
		boolean isAllDone = false;
		while (!isAllDone) {
			isAllDone = true;
			for (Future<?> future : futures) {
				isAllDone = isAllDone & future.isDone();
			}
		}

		List<List<Integer>> unsortedList = SharedBuffer.getInstance().getUnsortedList();
		List<List<Integer>> sortedList = SharedBuffer.getInstance().getSortedList();

		executorService.shutdown();

		assertNotNull(unsortedList);
		assertNotNull(sortedList);
		assertEquals(unsortedList.size(), sortedList.size());
		assertTrue(CompareListElements.isEqualList(sortedList, unsortedList));

	}

	@Test
	public void testConsumeThenProduce() throws InterruptedException {

		SharedBuffer.getInstance().clear();
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		List<Future<?>> futures = new ArrayList<>();

		// starting consumers
		for (int i = 0; i < consumersCount; i++) {
			futures.add(executorService.submit(
					new Consumer(sortedLock, unSortedLock, index, loopCount * producersCount, produceCondition)));
		}

		Thread.sleep(1000);
		// starting producers
		for (int i = 0; i < producersCount; i++) {
			futures.add(executorService
					.submit(new Producer(unSortedLock, loopCount, numInEachLoop, 100, produceCondition)));
		}

		// waiting for all tasks to be completed
		boolean isAllDone = false;
		while (!isAllDone) {
			isAllDone = true;
			for (Future<?> future : futures) {
				isAllDone = isAllDone & future.isDone();
			}
		}

		List<List<Integer>> unsortedList = SharedBuffer.getInstance().getUnsortedList();
		List<List<Integer>> sortedList = SharedBuffer.getInstance().getSortedList();

		executorService.shutdown();

		assertNotNull(unsortedList);
		assertNotNull(sortedList);
		assertEquals(unsortedList.size(), sortedList.size());
		assertTrue(CompareListElements.isEqualList(sortedList, unsortedList));

	}

	@Test
	public void testProducerConsumerIntleaved() {

		SharedBuffer.getInstance().clear();
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		List<Future<?>> futures = new ArrayList<>();

		// starting producers then consumers one by one
		for (int i = 0; i < producersCount; i++) {
			futures.add(executorService
					.submit(new Producer(unSortedLock, loopCount, numInEachLoop, 100, produceCondition)));
			futures.add(executorService.submit(
					new Consumer(sortedLock, unSortedLock, index, loopCount * producersCount, produceCondition)));

		}

		// waiting for all tasks to be completed
		boolean isAllDone = false;
		while (!isAllDone) {
			isAllDone = true;
			for (Future<?> future : futures) {
				isAllDone = isAllDone & future.isDone();
			}
		}

		List<List<Integer>> unsortedList = SharedBuffer.getInstance().getUnsortedList();
		List<List<Integer>> sortedList = SharedBuffer.getInstance().getSortedList();

		executorService.shutdown();

		assertNotNull(unsortedList);
		assertNotNull(sortedList);
		assertEquals(unsortedList.size(), sortedList.size());
		assertTrue(CompareListElements.isEqualList(sortedList, unsortedList));

	}

	@Test
	public void testConsumerProducerIntleaved() {

		SharedBuffer.getInstance().clear();
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		List<Future<?>> futures = new ArrayList<>();

		// starting consumers then producers one by one
		for (int i = 0; i < producersCount; i++) {
			futures.add(executorService.submit(
					new Consumer(sortedLock, unSortedLock, index, loopCount * producersCount, produceCondition)));

			futures.add(executorService
					.submit(new Producer(unSortedLock, loopCount, numInEachLoop, 100, produceCondition)));

		}

		// waiting for all tasks to be completed
		boolean isAllDone = false;
		while (!isAllDone) {
			isAllDone = true;
			for (Future<?> future : futures) {
				isAllDone = isAllDone & future.isDone();
			}
		}

		List<List<Integer>> unsortedList = SharedBuffer.getInstance().getUnsortedList();
		List<List<Integer>> sortedList = SharedBuffer.getInstance().getSortedList();

		executorService.shutdown();

		assertNotNull(unsortedList);
		assertNotNull(sortedList);
		assertEquals(unsortedList.size(), sortedList.size());
		assertTrue(CompareListElements.isEqualList(sortedList, unsortedList));

	}

}
